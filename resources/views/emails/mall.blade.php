<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
<style>@media only screen and (max-width: 600px) {
        .inner-body {
            width: 100% !important;
        }

        .footer {
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }</style>
<table class="wrapper" width="100%" cellpadding="0" cellspacing="0"
       style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
    <tr>
        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
            <table class="content" width="100%" cellpadding="0" cellspacing="0"
                   style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                <tr>
                    <td class="header"
                        style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
                        <a href="http://weibo.test"
                           style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">悦淘</a>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0"
                        style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                        <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0"
                               style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell"
                                    style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">
                                        各位领导：</h1>
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                        请查看昨天的数据报表。</p>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                           style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                        <tr>
                                            <td align="center"
                                                style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                商城
                                                <table border="2" cellpadding="10" cellspacing="0"
                                                       style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            供应商
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单量
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            净收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            优惠券金额
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            支出
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            盈利
                                                        </td>
                                                    </tr>
                                                    @foreach ($mall_num as $mall)
                                                        <tr>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['channel_name'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['order_num'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['actual_price'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['actual_price_pay'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['couponPrice'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['buy_price'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mall['reduce_price'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table class="subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-top: 1px solid #EDEFF2; margin-top: 25px; padding-top: 25px;">
                                        <tr>
                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; line-height: 1.5em; margin-top: 0; text-align: left; font-size: 12px;">
                                                    悦淘开发组
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                        <table class="footer" align="center" width="570" cellpadding="0"
                               cellspacing="0"
                               style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                            <tr>
                                <td class="content-cell" align="center"
                                    style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: center;">
                                        © 2019 悦淘. 版本所有。</p></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>