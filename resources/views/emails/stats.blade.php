<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
<style>@media only screen and (max-width: 600px) { .inner-body { width: 100% !important; } .footer { width: 100% !important; } } @media only screen and (max-width: 500px) { .button { width: 100% !important; } }</style>
<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
    <tr>
        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
            <table class="content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                <tr>
                    <td class="header" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
                        <a href="http://weibo.test" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">悦淘</a></td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                        <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">各位领导：</h1>
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">请查看昨天的数据报表。</p>
                                    <table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 30px auto; padding: 0; text-align: center; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                            注册用户
                                                            <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa; ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center;">
                                                                        注册来源
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        注册用户数
                                                                    </td>
                                                                </tr>
                                                                @foreach ($regist_num as $regist)
                                                                <tr>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $regist['name'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $regist['num'] }}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                            会员卡
                                                            <table border="2" cellpadding="2" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        会员卡
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        线上开卡
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        线下开卡
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        分公司直推
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $member_num['total_mem_num'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                       {{ $member_num['online_mem_num'] }}
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $member_num['downline_mem_num'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $member_num['branch_company_mem_num'] }}
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                            商城
                                                            <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        供应商
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        订单量
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        收入
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        净收入
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        优惠券金额
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        支出
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        盈利
                                                                    </td>
                                                                </tr>
                                                                @foreach ($mall_num as $mall)
                                                                <tr>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['channel_name'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['order_num'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['actual_price'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['actual_price_pay'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['couponPrice'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['buy_price'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $mall['reduce_price'] }}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                            悦淘399大礼包
                                                            <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        分类
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        名称
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        数量
                                                                    </td>
                                                                </tr>
                                                                @foreach ($sendGoods_num as $sendGoods)
                                                                    <tr>
                                                                        @if ($sendGoods['name'])
                                                                        <td style="font-family: Avenirtitle, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center" rowspan="{{ $sendGoods['count'] }}">
                                                                            {{ $sendGoods['name'] }}
                                                                        </td>
                                                                        @endif
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:600px;text-align:center">
                                                                            {{ $sendGoods['title'] }}
                                                                        </td>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $sendGoods['goodsNum'] }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                            悦店商城
                                                            <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        供应商
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        订单量
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        收入
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        支出
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        盈利
                                                                    </td>
                                                                </tr>
                                                                @foreach ($ydMall_num as $mall)
                                                                    <tr>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $mall['channel_name'] }}
                                                                        </td>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $mall['order_num'] }}
                                                                        </td>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $mall['actual_price'] }}
                                                                        </td>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $mall['buy_price'] }}
                                                                        </td>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $mall['reduce_price'] }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                            悦店大礼包
                                                            <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        分类
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        名称
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        数量
                                                                    </td>
                                                                </tr>
                                                                @foreach ($ydSendGoods_num as $sendGoods)
                                                                    <tr>
                                                                        @if ($sendGoods['name'])
                                                                            <td style="font-family: Avenirtitle, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center" rowspan="{{ $sendGoods['count'] }}">
                                                                                {{ $sendGoods['name'] }}
                                                                            </td>
                                                                        @endif
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:600px;text-align:center">
                                                                            {{ $sendGoods['title'] }}
                                                                        </td>
                                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                            {{ $sendGoods['goodsNum'] }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                            酒店
                                                            <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                                <tr style="background-color: #409eff;color: #FFFFFF">
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        供应商
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        订单量
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        收入
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        支出
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        盈利
                                                                    </td>
                                                                </tr>
                                                                @foreach ($hotel_num as $hotel)
                                                                <tr>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $hotel['channel_name'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $hotel['order_num'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $hotel['actual_price'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $hotel['buy_price'] }}
                                                                    </td>
                                                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                        {{ $hotel['reduce_price'] }}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                线路
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            供应商
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            兑换订单
                                                        </td>
                                                    </tr>
                                                    @foreach ($line_num as $line)
                                                    <tr>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $line['name'] }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $line['num'] }}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                三方权益(视频)
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            视频分类
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单量
                                                        </td>

                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            支出
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            盈利
                                                        </td>
                                                    </tr>
                                                    @foreach ($video_num as $video)
                                                        <tr>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $video->name }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $video->total }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $video->actual_price }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $video->buy_price }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{$video->reduce_price}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                三方权益(火车)
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        {{--<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">--}}
                                                            {{--班次--}}
                                                        {{--</td>--}}
                                                        {{--<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">--}}
                                                            {{--订单量--}}
                                                        {{--</td>--}}
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单总量
                                                        </td>

                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单成功出票
                                                        </td>

                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单退票数量
                                                        </td>

                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单取消数量
                                                        </td>


                                                    </tr>
                                                    @foreach ($train_num as $train)
                                                        <tr>
                                                            {{--<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">--}}
                                                                {{--{{ $train->name }}--}}
                                                            {{--</td>--}}
                                                            {{--<td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">--}}
                                                                {{--{{ $train->total }}--}}
                                                            {{--</td>--}}

                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $train['totalNum'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $train['successNum'] }}
                                                            </td>

                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $train['refundNum'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $train['cancelNum'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                三方权益（手机充值）
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            充值项
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            单数
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            支出
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            盈亏
                                                        </td>
                                                    </tr>
                                                    @foreach ($mobile_num as $mobile)
                                                        <tr>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:600px;text-align:center">
                                                                {{ $mobile['name'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $mobile['total'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:400px;text-align:center">
                                                                {{ $mobile['money'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:400px;text-align:center">
                                                                {{ $mobile['pay'] }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:400px;text-align:center">
                                                                {{ $mobile['income'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                三方权益（加油充值）
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            单数
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                             支出
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            车主邦补贴
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            悦淘补贴
                                                        </td>
                                                    </tr>

                                                        <tr>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $gas_num->total }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $gas_num->payAmount }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $gas_num->pay }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $gas_num->income }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $gas_num->bonus }}
                                                            </td>
                                                        </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                三方权益（曹操出行）
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            单数
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            应收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            实收入
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            待收入
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $car_num['number'] }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $car_num['totalMoney'] }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $car_num['money'] }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $car_num['pendingMoney'] }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                大人
                                                <table border="2" cellpadding="2" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            新增大人
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            授权人数
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            绑定手机
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $leader_num['infix_leader_num'] }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $leader_num['wxbind_num'] }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $leader_num['infix_phone_num'] }}
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                三方权益（机票）
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            单数
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            收入
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $flight_num->total }}
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            {{ $flight_num->income }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;padding-top: 40px ">
                                                积分兑换
                                                <table border="2" cellpadding="10" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;border-color:#f5f8fa ">
                                                    <tr style="background-color: #409eff;color: #FFFFFF">
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            产品
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            单价
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            订单量
                                                        </td>
                                                        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                            积分总量
                                                        </td>
                                                    </tr>
                                                    @foreach ($integral_num as $integral)
                                                        <tr>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:600px;text-align:center">
                                                                {{ $integral->goods_name }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:200px;text-align:center">
                                                                {{ $integral->price }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:400px;text-align:center">
                                                                {{ $integral->total }}
                                                            </td>
                                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;width:400px;text-align:center">
                                                                {{ $integral->total_price }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>

                                    </table>

                                    <table class="subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-top: 1px solid #EDEFF2; margin-top: 25px; padding-top: 25px;">
                                        <tr>
                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; line-height: 1.5em; margin-top: 0; text-align: left; font-size: 12px;">
                                                    悦淘开发组
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                            <tr>
                                <td class="content-cell" align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: center;">© 2019 悦淘. 版本所有。</p></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>