<?php

Route::group(["prefix" => "friend"], function (){
    Route::post("/list", "Community\FriendController@getList");
    Route::post("/getFriendRobotList", "Community\FriendController@getFriendRobotList");


});








Route::group(["prefix" => "info"], function (){
    Route::post("/list.json", "Member\MemberController@getList");
    Route::post("/detail.json", "Member\MemberController@getDetail");
    Route::post("/relations.json", "Member\MemberController@getRelations");
    Route::post("/commission.json", "Member\MemberController@getCommission");
    Route::post("/delCheckInfo", "Member\MemberController@delCheckInfo");
    Route::post("/analysis/list.json", "Member\MemberController@getMemberDataByDate");
    Route::post("/bonus/list.json", "Member\MemberController@getBonusList");
    Route::post("/cash/list.json", "Member\MemberController@getCashList");

    //后台新版本用户体现明细
    Route::post("/cash/newList.json", "Member\MemberController@getNewCashList");
    Route::post("/cash/downNewCashList.json", "Member\MemberController@downNewCashList");
    Route::post("/cash/passCash", "Member\MemberController@passCash");
    Route::post("/cash/passCashAll", "Member\MemberController@passCashAll");
    Route::post("/cash/noPassCash", "Member\MemberController@noPassCash");
    Route::post("/cash/noPassCashAll", "Member\MemberController@noPassCashAll");
    Route::post("/cash/upStatusToWithdrawal", "Member\MemberController@upStatusToWithdrawal");
});
Route::post("/getMemberIdentifys", "Member\MemberController@getMemberIdentifys");
Route::post("/getMemberIdentify", "Member\MemberController@getMemberIdentify");
Route::post("/upMemberIdentify", "Member\MemberController@upMemberIdentify");
Route::post("/memberDetail", "Member\MemberController@memberDetail");


