<?php

//管理员的添加
Route::post("/adminRegister","Admin\AdminRegisterController@addAdmin");
//获取部门信息
Route::post("/getSectioyn","Admin\SectioynController@allSectioynList");

Route::group(["prefix" => "service",'middleware'=>['AdminAuthVerification']], function (){
    //服务项目列表
    Route::post("/list","Admin\RobotServiceController@SerivceList");
    //添加服务项目
    Route::post("/add","Admin\RobotServiceController@addSerivce");
    //修改服务项目
    Route::post("/update","Admin\RobotServiceController@updateSerivce");
    //删除服务项目
    Route::post("/del","Admin\RobotServiceController@delService");
    //购买接入群订单展示
    Route::post("/getBuyGroupOrderList","Admin\RobotServiceController@getBuyGroupOrderList");
});


Route::group(["prefix" => "group"], function (){
    //ls
    //社群列表
    Route::post("/getAdminDataStatistics","Admin\GroupManageController@getAdminDataStatistics");
    //渠道列表
    Route::post("/getChannelList","Admin\GroupManageController@getChannelList");
    //社群列表
    Route::post("/getGroupList","Admin\GroupManageController@getCommunityList");
    //机器人列表
    Route::post("/getRobotList","Admin\GroupManageController@getRobotList");
    //小悦机器人管理总群数量
    Route::post("/getRobotGroupInfo","Admin\GroupManageController@getRobotGroupInfo");
    //机器人数量统计--折线图
    Route::post("/getRobotGroupInfoGraph","Admin\GroupManageController@getRobotGroupInfoGraph");
    //统计群主列表
    Route::post("/getHouTaiGroupOwnerList","Admin\GroupManageController@getHouTaiGroupOwnerList");
    //群主列表
    Route::post("/getGroupOwnerList","Admin\GroupManageController@getGroupOwnerList");
    //统计信息
    Route::post("/getStatisticsInfo","Admin\GroupManageController@getStatisticsInfo");
    //订单统计图
    Route::post("/getOrderGraph","Admin\GroupManageController@getOrderGraph")->middleware('AdminAuthVerification');;
    //消息列表
    Route::post("/getMessageList","Admin\GroupManageController@getMessageList");
   //发送到群
    Route::post("/getGroupSend","Admin\GroupManageController@getSendGroupNumByLinkType");
    //商品统计
    Route::post("/getProductStatistics","Admin\GroupManageController@getProductStatistics");
    Route::post('/getHouTaiUserListByName','Admin\AdminIndexController@getHouTaiUserListByName');
    //管理端  群主统计
    Route::post("/groupLeader","Admin\GroupManageController@getGroupLeaderList");

    //订单列表
    Route::post("/getOrderList","Admin\GroupManageController@getOrderGoodsList");

    //获取群自定义标签列表
    Route::post("/getGroupTagsList","Admin\GroupManageController@getGroupTagsList");

});






Route::group(["prefix" => "group",'middleware'=>['AdminAuthVerification']], function (){
    //审核群列表
    Route::post("/verifyList","Admin\GroupManageController@getGroupVerifyList");
    //群审核接口
    Route::post("/toExamineGroup","Admin\GroupManageController@toExamineGroup");
    //群审核接口
    Route::post("/verifyGroupInfo","Admin\GroupManageController@verifyGroupInfo");

});

Route::group(["prefix" => "user"], function (){
    //小悦机器人注册用户列表
    Route::post("/userList","Admin\UserController@getUserList");
    //小悦机器人注册用户统计
    Route::post("/userinfo","Admin\UserController@getUserinfo");
    //赠送群
    Route::post("/givUserGroup","Admin\UserController@givUserGroup");
    //专属机器人审核列表
    Route::post("/getExclusiveRobotList","Admin\UserController@getExclusiveRobotList");
    //专属机器人审核
    Route::post("/examineExclusiveRobot","Admin\UserController@examineExclusiveRobot");
    //小悦机器人注册用户列表
    Route::post("/getUserAdminList","Admin\UserController@getUserAdminList");   

});


Route::group(["prefix" => "live"], function (){
    //直播间排名
    Route::post("/getLiveRankInfo","Admin\GroupManageController@getLiveRankInfo");
    //每天悦淘直播主播人数
    Route::post("/getLive","Admin\GroupManageController@getLiveNumByTime");
    //每天悦淘直播主播人数--折线图
    Route::post("/getLiveGraph","Admin\GroupManageController@getLiveNumByTimeGraph");
});




Route::group(["prefix" => "power"], function (){
    //查询所有权限数据
    Route::post("/getPower","Admin\PowerController@allPowerList");
    //获取管理员  所拥有的权限
    Route::post("/adminPower","Admin\PowerController@getAdminPower");
    //增加管理员权限
    Route::post("/addAdminPower","Admin\PowerController@addAdminPower");
    //删除管理员权限
    Route::post("/deleteAdminPower","Admin\PowerController@deleteAdminPower");
});


//最新版本data 用户管理start
//管理员用户登录
Route::post('/userLogin','Admin\AdminLoginController@Login');
//管理员用户登录联合登陆
Route::post('/userLoginForMiddel','Admin\AdminLoginController@LoginForMiddel');
//更新密码
Route::any("updatePassword" , "Admin\AdminLoginController@updatePassword")->middleware('AdminAuthVerification');
Route::post('/uploadSpikeGoods','Mall\V1\SeckillController@uploadSpikeGoods');

Route::group(["prefix" => "admin",'middleware'=>['AdminAuthVerification']], function (){

    //订单群数量
    Route::post('/orderGroupNum','Admin\AdminIndexController@orderGroupNum');
    //订单群列表
    Route::post('/orderGroupList','Admin\AdminIndexController@orderGroupList');

//Route::group(["prefix" => "admin"], function (){
    //管理员用户添加
    Route::post('/addAdminUser','Admin\AdminUserController@addAdminUser');

    //根据用户昵称获取管理员列表
    Route::post('/getAdminListByName','Admin\AdminUserController@getAdminListByName');

    //管理员用户更新
    Route::post('/upAdminUser','Admin\AdminUserController@upAdminUser');

    //管理员用户信息
    Route::post('/getAdminUser','Admin\AdminUserController@getAdminUser');

    //管理员用户信息，判断当前用户的角色类型信息
    Route::post('/getAdminUserRole','Admin\AdminUserController@getAdminUserRole');

    //管理员用户列表
    Route::post('/getAdminUsers','Admin\AdminUserController@getAdminUsers');

    //管理员用户列表
    Route::post('/delAdminUser','Admin\AdminUserController@delAdminUser');

    //管理员角色列表
    Route::post('/getRoles','Admin\AdminUserController@getRoles');

    //管理员角色信息
    Route::post('/getRole','Admin\AdminUserController@getRole');

    //添加管理员角色信息
    Route::post('/addRole','Admin\AdminUserController@addRole');

    //更新管理员角色信息
    Route::post('/upRole','Admin\AdminUserController@upRole');

    //删除管理员角色信息
    Route::post('/delRole','Admin\AdminUserController@delRole');


    //权限列表
    Route::post('/getPowers','Admin\AdminUserController@getPowers');

    //权限信息
    Route::post('/getPower','Admin\AdminUserController@getPower');

    //添加权限
    Route::post('/addPower','Admin\AdminUserController@addPower');

    //更新权限
    Route::post('/upPower','Admin\AdminUserController@upPower');

    //删除权限
    Route::post('/delPower','Admin\AdminUserController@delPower');

    //管理员操作日志列表
    Route::post('/adminOperationList','Admin\AdminUserController@getAdminOperationList');






    //用户端权限列表
    Route::post('/getUserPowerList','Admin\AdminUserController@getUserPowerList');
    //用户端权限信息
    Route::post('/getUserPowerInfo','Admin\AdminUserController@getUserPowerInfo');
    //添加用户端权限
    Route::post('/addUserPower','Admin\AdminUserController@addUserPower');
    //更新用户端权限
    Route::post('/upUserPower','Admin\AdminUserController@upUserPower');
    //删除用户端权限
    Route::post('/delUserPower','Admin\AdminUserController@delUserPower');
    //管理员角色列表
    Route::post('/getUserRoles','Admin\AdminUserController@getUserRoles');
    //管理员角色信息
    Route::post('/getUserRole','Admin\AdminUserController@getUserRole');
    //添加管理员角色信息
    Route::post('/addUserRole','Admin\AdminUserController@addUserRole');
    //更新管理员角色信息
    Route::post('/upUserRole','Admin\AdminUserController@upUserRole');
    //删除管理员角色信息
    Route::post('/delUserRole','Admin\AdminUserController@delUserRole');
    //权限树状图
    Route::post('/getUserTreePowers','Admin\AdminUserController@getUserTreePowers');
    //权限等级下拉菜单
    Route::post('/getUserLevelPowers','Admin\AdminUserController@getUserLevelPowers');




    //权限树状图
    Route::post('/getTreePowers','Admin\AdminUserController@getTreePowers');


    //用户权限左侧列表
    Route::post('/getUserPowers','Admin\AdminUserController@getUserPowers');

    //权限等级下拉菜单
    Route::post('/getLevelPowers','Admin\AdminUserController@getLevelPowers');




    //部门列表
    Route::post('/getDepartments','Admin\AdminUserController@getDepartments');

    //部门信息
    Route::post('/getDepartment','Admin\AdminUserController@getDepartment');

    //添加部门信息
    Route::post('/addDepartment','Admin\AdminUserController@addDepartment');

    //更新部门信息
    Route::post('/upDepartment','Admin\AdminUserController@upDepartment');

    //删除部门信息
    Route::post('/delDepartment','Admin\AdminUserController@delDepartment');


    //职位列表
    Route::post('/getPositions','Admin\AdminUserController@getPositions');

    //职位信息
    Route::post('/getPosition','Admin\AdminUserController@getPosition');

    //添加职位信息
    Route::post('/addPosition','Admin\AdminUserController@addPosition');

    //更新职位信息
    Route::post('/upPosition','Admin\AdminUserController@upPosition');

    //删除职位信息
    Route::post('/delPosition','Admin\AdminUserController@delPosition');

    //部门下拉菜单
    Route::post('/getDepartmentsMenu','Admin\AdminUserController@getDepartmentsMenu');

    //职位下拉菜单
    Route::post('/getPositionsMenu','Admin\AdminUserController@getPositionsMenu');



    //最新版本data 用户管理end


    //二维码添加接口
    Route::post('/addQrCode', 'Mall\V1\QrCodeController@addQrCode');

    //二维码列表接口
    Route::post('/getQrCodes', 'Mall\V1\QrCodeController@getQrCodes');

    //二维码详情接口
    Route::post('/getQrCode', 'Mall\V1\QrCodeController@getQrCode');

    //二维码更新接口
    Route::post('/upQrCode', 'Mall\V1\QrCodeController@upQrCode');

    //推送
    Route::post('/pushMessage', 'Push\PushController@pushMessage');

    //推送分发
    Route::post('/pushMessageOneSwitch', 'Push\PushController@pushMessageOneSwitch');

    //推送分发
    Route::post('/pushMessageAllSwitch', 'Push\PushController@pushMessageAllSwitch');

    //个推 组推
    Route::post('/pushMessageType', 'Push\PushController@pushMessageType');

    //推送列表
    Route::post('/getPushs', 'Push\PushController@getPushs');

    //推送添加
    Route::post('/addPush', 'Push\PushController@addPush');

    //推送详情
    Route::post('/getPush', 'Push\PushController@getPush');

    //推送编辑
    Route::post('/upPush', 'Push\PushController@upPush');

    //推送删除
    Route::post('/delPush', 'Push\PushController@delPush');


    //素材圈列表
    Route::post('/getMaterialCircles', 'Mall\V1\MaterialCircleController@getMaterialCircles');

    //素材圈添加
    Route::post('/addMaterialCircle', 'Mall\V1\MaterialCircleController@addMaterialCircle');

    //素材圈详情
    Route::post('/getMaterialCircle', 'Mall\V1\MaterialCircleController@getMaterialCircle');

    //素材圈编辑
    Route::post('/upMaterialCircle', 'Mall\V1\MaterialCircleController@upMaterialCircle');

    //素材圈删除
    Route::post('/delMaterialCircle', 'Mall\V1\MaterialCircleController@delMaterialCircle');

    //添加分组用户推送
    Route::post('/addPushGroupUser', 'Push\PushController@addPushGroupUser');

    Route::post('/getPushGroupUsers', 'Push\PushController@getPushGroupUsers');

    Route::post('/getPushGroupUser', 'Push\PushController@getPushGroupUser');

    Route::post('/upPushGroupUser', 'Push\PushController@upPushGroupUser');

    //添加短信模板
    Route::post('/addShortMessage', 'Push\PushController@addShortMessage');

    Route::post('/getShortMessages', 'Push\PushController@getShortMessages');

    Route::post('/getShortMessage', 'Push\PushController@getShortMessage');

    Route::post('/upShortMessage', 'Push\PushController@upShortMessage');


    Route::post('/pushShortMessageType', 'Push\PushController@pushShortMessageType');


    Route::post('/getShortUrl', 'Push\PushController@getShortUrl');

    //售后订单列表列表
    Route::post('/getRefundOrders', 'Mall\V1\MallOrderController@getRefundOrders');
    Route::post('/getRefundOrderInfo', 'Mall\V1\MallOrderController@getRefundOrderInfo');
    //审核
    Route::post('/checkRefund', 'Mall\V1\MallOrderController@checkRefund');

    Route::post('/getRefundServiceTypes', 'Mall\V1\MallOrderController@getRefundServiceTypes');

    Route::post('/addRefundServiceType', 'Mall\V1\MallOrderController@addRefundServiceType');
    Route::post('/upRefundServiceType', 'Mall\V1\MallOrderController@upRefundServiceType');
    Route::post('/getRefundServiceType', 'Mall\V1\MallOrderController@getRefundServiceType');

    //根据子订单号查询退货地址
    Route::post('/getRefundAddress','Mall\V1\MallOrderController@getRefundAddress');
    //生成退款单
    Route::post('/refundOrder','Mall\V1\MallOrderController@createRefundOrder');
    //2019.12.28版售后退款
    Route::post('/getNewRefundOrders', 'Mall\V1\MallOrderController@getNewRefundOrders');
    Route::post('/getNewRefundOrderInfo', 'Mall\V1\MallOrderController@getNewRefundOrderInfo');
    Route::post('/getRefundAboutInfo', 'Mall\V1\MallOrderController@getRefundAboutInfo');

    //五百以上售后订单
    Route::post('/getMoreFiveHundredRefundOrders', 'Mall\V1\MallOrderController@getMoreFiveHundredRefundOrders');
    //五百以下售后订单
    Route::post('/getFiveHundredBelowRefundOrders', 'Mall\V1\MallOrderController@getFiveHundredBelowRefundOrders');

    //审核 上传物流单号 同意退款 拒绝退款
    Route::post('/checkNewRefund', 'Mall\V1\MallOrderController@checkNewRefund');
    Route::post('/upRefundOrderGoods', 'Mall\V1\MallOrderController@upRefundOrderGoods');
    //退款操作日志
    Route::post('/getRefundOperationLogList', 'Mall\V1\MallOrderController@getRefundOperationLogList');
    Route::post('/addRefundRemark', 'Mall\V1\MallOrderController@addRefundRemark');
    Route::post('/updateOrderGoodsRefundStatus', 'Mall\V1\MallOrderController@updateOrderGoodsRefundStatus');

    //自动退款2020.3.8号要上线版本

    //筛选出符合生成审核结算单的列表
    Route::post('/getEligibilityRefundOrders', 'Mall\V1\MallOrderController@getEligibilityRefundOrders');

    //生成审核结算单
    Route::post('/createRefundSettlement', 'Mall\V1\RefundSettlementController@createRefundSettlement');
    //退款结算单列表
    Route::post('/getRefundSettlementList', 'Mall\V1\RefundSettlementController@getRefundSettlementList');
    //退款结算单详情
    Route::post('/getRefundSettlement', 'Mall\V1\RefundSettlementController@getRefundSettlement');
    //审核一条退款结算单子单
    Route::post('/checkfundSettlement', 'Mall\V1\RefundSettlementController@checkfundSettlement');
    //审核多条退款结算单子单
    Route::post('/checkfundSettlementMore', 'Mall\V1\RefundSettlementController@checkfundSettlementMore');
    //退款结算单操作日志列表
    Route::post('/getRefundSetOpLogList', 'Mall\V1\RefundSettlementController@getRefundSetOpLogList');




    //兼职列表
    Route::post('/getPartTimeJobs', 'Mall\V1\MallOrderController@getPartTimeJobs');
    Route::post('/getPartTimeJobInfo', 'Mall\V1\MallOrderController@getPartTimeJobInfo');
    //审核
    Route::post('/checkPartTimeJob', 'Mall\V1\MallOrderController@checkPartTimeJob');


    // 手机充值订单列表
    Route::post('/getRechargeList', 'ThirdParty\OrderController@getRechargeList');
    // 每天明细
    Route::post('/getCurrentMonthRecharge', 'ThirdParty\OrderController@getCurrentMonthRecharge');
    // 用户明细
    Route::post('/getRechargeDetail', 'ThirdParty\OrderController@getRechargeDetail');

    // 视频充值列表
    Route::post('/getVideoRechargeList', 'ThirdParty\OrderController@getVideoRechargeList');
    // 每天明细
    Route::post('/getVideoMonthRecharge', 'ThirdParty\OrderController@getVideoMonthRecharge');
    // 用户明细
    Route::post('/getVideoRechargeDetail', 'ThirdParty\OrderController@getVideoRechargeDetail');

    // 加油充值列表
    Route::post('/getCarRechargeList', 'ThirdParty\OrderController@getCarRechargeList');
    // 每天明细
    Route::post('/getCarMonthRecharge', 'ThirdParty\OrderController@getCarMonthRecharge');
    // 用户明细
    Route::post('/getCarRechargeDetail', 'ThirdParty\OrderController@getCarRechargeDetail');

    // 火车票 每日明细
    Route::post('/getTrainMonthRecharge', 'ThirdParty\OrderController@getTrainMonthRecharge');
    // 火车票 用户明细
    Route::post('/getTrainRechargeDetail', 'ThirdParty\OrderController@getTrainRechargeDetail');

    //推荐商品
    Route::post('/getRecommendGoodsList', 'Mall\V1\GoodsRecommendController@getRecommendGoodsList');
    Route::post('/addRecommendGoods', 'Mall\V1\GoodsRecommendController@addRecommendGoods');
    Route::post('/upRecommendGoods', 'Mall\V1\GoodsRecommendController@upRecommendGoods');
    Route::post('/getRecommendGoodsOne', 'Mall\V1\GoodsRecommendController@getRecommendGoodsOne');
    Route::post('/delRecommendGoods', 'Mall\V1\GoodsRecommendController@delRecommendGoods');

    //视频带货
    Route::post('/addVideoTakeGoods', 'Mall\V1\VideoTakeGoodsController@addVideoTakeGoods');
    Route::post('/getVideoTakeGoodsList', 'Mall\V1\VideoTakeGoodsController@getVideoTakeGoodsList');
    Route::post('/delVideoTakeGoods', 'Mall\V1\VideoTakeGoodsController@delVideoTakeGoods');
    Route::post('/recVideoTakeGoods', 'Mall\V1\VideoTakeGoodsController@recVideoTakeGoods');
    Route::post('/getVideoTakeGoods', 'Mall\V1\VideoTakeGoodsController@getVideoTakeGoods');
    Route::post('/updVideoTakeGoods', 'Mall\V1\VideoTakeGoodsController@updVideoTakeGoods');
    //视频带货数据分析
    Route::post('/getVideoTakeGoodsData', 'Mall\V1\VideoTakeGoodsController@getVideoTakeGoodsData');

    //视频带货分类
    Route::post('/getVideoCategory', 'Mall\V1\VideoTakeGoodsController@getVideoCategory');

    //曹操订单
    Route::post('/addCaocaoOrder', 'Mall\V1\CaocaoOrderController@addCaocaoOrder');
    Route::post('/delCaocaoOrder', 'Mall\V1\CaocaoOrderController@delCaocaoOrder');
    Route::post('/upCaocaoOrder', 'Mall\V1\CaocaoOrderController@upCaocaoOrder');
    Route::post('/getCaocaoOrder', 'Mall\V1\CaocaoOrderController@getCaocaoOrder');
    Route::post('/getCaocaoOrderList', 'Mall\V1\CaocaoOrderController@getCaocaoOrderList');

    //当当后台
    Route::post('/addDdRecommendCategory', 'Mall\V1\ddAdminController@addDdRecommendCategory');
    Route::post('/delDdRecommendCategory', 'Mall\V1\ddAdminController@delDdRecommendCategory');
    Route::post('/upDdRecommendCategory', 'Mall\V1\ddAdminController@upDdRecommendCategory');
    Route::post('/getDdRecommendCategoryList', 'Mall\V1\ddAdminController@getDdRecommendCategoryList');
    Route::post('/getDdRecommendCategory', 'Mall\V1\ddAdminController@getDdRecommendCategory');

    Route::post('/addDdRecommendBook', 'Mall\V1\ddAdminController@addDdRecommendBook');
    Route::post('/delDdRecommendBook', 'Mall\V1\ddAdminController@delDdRecommendBook');
    Route::post('/upDdRecommendBook', 'Mall\V1\ddAdminController@upDdRecommendBook');
    Route::post('/getDdRecommendBookList', 'Mall\V1\ddAdminController@getDdRecommendBookList');
    Route::post('/getDdPurchases', 'Mall\V1\ddAdminController@getDdPurchases');
    //根据渠道和分类获取书籍列表
    Route::post('/getProductList', 'Mall\V1\ddAdminController@getProductList');
    Route::post('/upDdShowStatus', 'Mall\V1\ddAdminController@upDdShowStatus');

    Route::post('/getDdProductList', 'Mall\V1\ddAdminController@getDdProductList');


    //订单商品分佣
    Route::post('/getOrderGoodsBonus', 'Mall\V1\MallOrderController@getOrderGoodsBonus');


    Route::post('/upSendRemark', 'Mall\V1\MallOrderController@upSendRemark');



    //赠送优惠券
    Route::post('/sendCoupon','Mall\V1\CouponController@sendCoupon');
    //新增优惠券
    Route::post('/newAddCoupon','Mall\V1\CouponController@newAddCoupon');
    //开启或关闭优惠券
    Route::post('/switchCoupon','Mall\V1\CouponController@couponUpdate');
    //查看优惠券详情
    Route::post('/getCouponInfo','Mall\V1\CouponController@getCouponInfo');
    //分页查看优惠券
    Route::post('/selectCoupon','Mall\V1\CouponController@selectCoupon');
    //查看优惠券
    Route::post('/selectCoupons','Mall\V1\CouponController@selectCoupons');
    //开启或关闭优惠券
    Route::post('/newUpdateCoupon','Mall\V1\CouponController@newUpdateCoupon');
    //优惠券详情
    Route::get('/couponDetail','Mall\V1\CouponController@couponDetail');

    //优惠券专题列表
    Route::get('/couponZtList','Mall\V1\CouponZtController@selectCoupon');
    //新建优惠券专题
    Route::post('/couponZtAdd','Mall\V1\CouponZtController@addCouponZt');
    //查看优惠券专题
    Route::get('/couponZtDetail','Mall\V1\CouponZtController@couponDetail');
    //修改优惠券专题
    Route::post('/couponZtUp','Mall\V1\CouponZtController@updateCouponZt');
    //删除优惠券
    Route::post('/couponZtDel','Mall\V1\CouponZtController@deleteCoupon');

    //优惠券类型
    Route::post('/couponChannelType','Mall\V1\CouponZtController@couponChannelType');

    //优惠券标签
    Route::post('/getCouponTags','Mall\V1\CouponTagController@getCouponTags');
    Route::post('/getCouponTag','Mall\V1\CouponTagController@getCouponTag');
    Route::post('/addCouponTag','Mall\V1\CouponTagController@addCouponTag');
    Route::post('/upCouponTag','Mall\V1\CouponTagController@upCouponTag');
    Route::post('/delCouponTagProctor','Mall\V1\CouponTagController@delCouponTagProctor');


    //悦旅大学模块二维码
    Route::post('/getOrCodeImages','University\UniversityController@getOrCodeImages');
    Route::post('/getOrCodeImage','University\UniversityController@getOrCodeImage');
    Route::post('/upOrCodeImage','University\UniversityController@upOrCodeImage');


    //一分钱抽奖
    Route::post('/addLotteryDraw','Mall\V1\LotteryDrawController@addLotteryDraw');
    Route::post('/upLotteryDraw','Mall\V1\LotteryDrawController@upLotteryDraw');
    Route::post('/getLotteryDraws','Mall\V1\LotteryDrawController@getLotteryDraws');
    Route::post('/getLotteryDraw','Mall\V1\LotteryDrawController@getLotteryDraw');

    //一分钟抽奖商品管理
    Route::post('/getLotteryDrawProducts','Mall\V1\LotteryDrawController@getLotteryDrawProducts');
    Route::post('/getLotteryDrawProduct','Mall\V1\LotteryDrawController@getLotteryDrawProduct');
    Route::post('/addLotteryDrawProduct','Mall\V1\LotteryDrawController@addLotteryDrawProduct');
    Route::post('/upLotteryDrawProduct','Mall\V1\LotteryDrawController@upLotteryDrawProduct');
    Route::post('/delLotteryDrawProduct','Mall\V1\LotteryDrawController@delLotteryDrawProduct');
    Route::post('/upLotteryDrawProductOnline','Mall\V1\LotteryDrawController@upLotteryDrawProductOnline');
    //中奖人数
    Route::post('/getLotteryDrawPrizes','Mall\V1\LotteryDrawController@getLotteryDrawPrizes');
    Route::post('/upIsPrize','Mall\V1\LotteryDrawController@upIsPrize');
    Route::post('/delLotteryDraw','Mall\V1\LotteryDrawController@delLotteryDraw');


    //推荐活动管理
    Route::post('/addRecommendActivityInfo','Mall\V1\LotteryDrawController@addRecommendActivityInfo');
    Route::post('/upRecommendActivityInfo','Mall\V1\LotteryDrawController@upRecommendActivityInfo');
    Route::post('/getRecommendActivities','Mall\V1\LotteryDrawController@getRecommendActivities');
    Route::post('/getRecommendActivityInfo','Mall\V1\LotteryDrawController@getRecommendActivityInfo');


    //用户链路-优惠券和用户级别
    Route::post('/getMemberLinkCouponList','Mall\V1\LotteryDrawController@getMemberLinkCouponList');
    Route::post('/getMemberLinkCoupon','Mall\V1\LotteryDrawController@getMemberLinkCoupon');
    Route::post('/addMemberLinkCoupon','Mall\V1\LotteryDrawController@addMemberLinkCoupon');
    Route::post('/upMemberLinkCoupon','Mall\V1\LotteryDrawController@upMemberLinkCoupon');


    //社群 发送的资料
    Route::post('/getCommunitySendInfos','Mall\V1\LotteryDrawController@getCommunitySendInfos');
    Route::post('/getCommunitySendInfo','Mall\V1\LotteryDrawController@getCommunitySendInfo');
    Route::post('/addCommunitySendInfo','Mall\V1\LotteryDrawController@addCommunitySendInfo');
    Route::post('/upCommunitySendInfo','Mall\V1\LotteryDrawController@upCommunitySendInfo');
    Route::post('/delCommunitySendInfo','Mall\V1\LotteryDrawController@delCommunitySendInfo');

    //添加社群攻略分类
    Route::post('/addStrategyCategory','Community\StrategyCategoryController@addStrategyCategory');
    Route::post('/getStrategyCategoryList','Community\StrategyCategoryController@getStrategyCategoryList');
    //攻略文章
    Route::post('/addStrategyArticle','Community\StrategyArticleController@addStrategyArticle');
    Route::post('/getStrategyArticleList','Community\StrategyArticleController@getStrategyArticleList');
    Route::post('/getStrategyArticle','Community\StrategyArticleController@getStrategyArticle');
    Route::post('/updateStrategyArticle','Community\StrategyArticleController@updateStrategyArticle');

    //社群微信号审核
    Route::post('/getCommunityInvitationGroupList','Mall\V1\LotteryDrawController@getCommunityInvitationGroupList');
    Route::post('/getCommunityInvitationGroupInfo','Mall\V1\LotteryDrawController@getCommunityInvitationGroupInfo');
    Route::post('/checkCommunityInvitationGroup','Mall\V1\LotteryDrawController@checkCommunityInvitationGroup');

    //充值配置 表列表
    Route::post('/getBalanceList','Mall\V1\LotteryDrawController@getBalanceList');
    Route::post('/getBalanceInfo','Mall\V1\LotteryDrawController@getBalanceInfo');
    Route::post('/addBalance','Mall\V1\LotteryDrawController@addBalance');
    Route::post('/upBalance','Mall\V1\LotteryDrawController@upBalance');
    Route::post('/delBalance','Mall\V1\LotteryDrawController@delBalance');



});

//发送时机列表
Route::get('/couponZt/opportunityList','Mall\V1\CouponZtController@opportunityList');

Route::group(["prefix" => "flight",'middleware'=>['AdminAuthVerification']], function () {
//Route::group(["prefix" => "flight"], function (){// 机票订单
    // 订单状态
    Route::get('/getFlightOrderStatus','Flight\OrderController@orderStatus');
    // 订单列表
    Route::post('/getFlightOrderList','Flight\OrderController@orderList');

    //同意/拒绝订单
    Route::post('/checkRefund','Flight\OrderController@checkRefund');
});


Route::any('/getVipPriceIsZero', 'Push\PushController@getVipPriceIsZero');

Route::any('/upSearchByproductId', 'Push\PushController@upSearchByproductId');


Route::any('/getOrderPeopleAddress', 'Push\ExcelController@getOrderPeopleAddress');

Route::any('/getDownSuppliers', 'Push\ExcelController@getDownSuppliers');
Route::any('/getDownRefundOrders', 'Push\ExcelController@getDownRefundOrders');

Route::any('/getDownOrderGoods', 'Push\ExcelController@getDownOrderGoods');

//分类的树状结构
Route::post('/getDdCategoryList', 'Mall\V1\ddAdminController@getDdCategoryList');
//yuelvhui供应商excel导出
Route::any('/getYlhSuppliersExcel','Push\ExcelController@getYlhSuppliersExcel');

//yuelvhui供应商excel导入
Route::any('/upExcelYlhSupplierAddress','Push\ExcelController@upExcelYlhSupplierAddress');

Route::any('/uploadOrderAddress', 'Push\ExcelController@uploadOrderAddress');
Route::any('/getDownProduct', 'Push\ExcelController@getDownProduct');
//驳回给中台商品--中台停用供应商
Route::any('/yuetaoNoPassCenterGoods','Push\PushController@yuetaoNoPassCenterGoods');
//导出创客订单
Route::any('/exportOrderWorkList','Push\ExcelController@exportOrderWorkList');
//导出财务需要对账的退款单信息
Route::any('/exportRefundOrderGoodsList','Push\ExcelController@exportRefundOrderGoodsList');

Route::any('/getDownIsPrizes','Mall\V1\LotteryDrawController@getDownIsPrizes');

//渠道筛选列表
Route::post('/getSelectType','RobotOrder\SourceMaterialController@getSelectType');

/**
 * 新统计
 */
Route::group(["prefix"=>"statistics"],function(){
    Route::any('/getGroup','Statistics\StatisticsController@getGroup');
    Route::any('/getLeader','Statistics\StatisticsController@getLeader');
});

