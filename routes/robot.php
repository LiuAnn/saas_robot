<?php
/**
 * 展示用户 购买服务的路由
 */

Route::group(["prefix" => "service"], function () {
    // 服务展示 订单展示
    Route::post('/getRobotServiceList','RobotOrder\RobotOrderController@getBuyInfoByMid');
    //添加浏览记录 用户  mall  curl 请求
    Route::post('/addBrowerData','RobotOrder\RobotOrderController@addBrowerData');
    //根据mid 获取群标识
    Route::post('/getRoomId','RobotOrder\RobotOrderController@getRoomIdByMid');
    //根据mid 获取群标识
    Route::post('/getMidId','RobotOrder\RobotOrderController@getMidByRoomId');
    //判断当前用户是否购买过接入群的数据
    Route::post('/userIndex','RobotOrder\UseIndexBuyController@isOrderExists');
    //审核群列表
    Route::post("/verifyList","RobotOrder\GroupManageController@getGroupVerifyList");
    //二次升级列表
    Route::post("/twiceVerifyList","RobotOrder\GroupManageController@getTwiceGroupVerifyList");
    //群审核接口
    Route::post("/toExamineGroup","RobotOrder\GroupManageController@toExamineGroup");
    //二次群审核接口
    Route::post("/toTwiceExamineGroup","RobotOrder\GroupManageController@toTwiceExamineGroup");
   //群审核接口
    Route::post("/verifyGroupInfo","RobotOrder\GroupManageController@verifyGroupInfo");
    //获取我的机器人
    Route::post("/getRobotInfo","RobotOrder\UseIndexBuyController@getRobotInfo");
    //获取我的机器人
    Route::post("/getChannelData","RobotOrder\UseIndexBuyController@getChannelData");
    //添加新的机器人
    Route::post("/insertRobotInfo","RobotOrder\UseIndexBuyController@insertRobotInfo");
    //专属机器人列表
    Route::post("/getExclusiveRobotInfo","RobotOrder\UseIndexBuyController@getExclusiveRobotInfo");
    //获取我的机器人
    Route::post("/getRobotStatistics","RobotOrder\UseIndexBuyController@getRobotStatistics");
     //获取可选择的标签
    Route::post("/getTagData","RobotOrder\GroupManageController@getTagData");
     // 更新群标签
    Route::post("/changeGroupTag","RobotOrder\GroupManageController@changeGroupTag");
    // 更新发送顺序
    Route::post("/changeOrderBy","RobotOrder\GroupManageController@changeOrderBy");

    //优惠券数据统计
    Route::post("/getCouponListStatistics","RobotOrder\GroupManageController@getCouponListStatistics");
});



Route::get('/info','RobotOrder\RobotOrderController@test');


Route::group(["prefix" => "user",'middleware'=>['user.check.login']], function (){
     //用户端用户权限左侧列表
    Route::post('/getUserPowersList','Admin\AdminUserController@getUserPowersList');
     //角色列表
    Route::post('/getUserRoleList','RobotOrder\UseIndexBuyController@getUserRolesByMid');
      //添加角色
    Route::post('/addFrontRole','RobotOrder\UseIndexBuyController@addUserFrontRole');
      //编辑角色
    Route::post('/upFrontRole','RobotOrder\UseIndexBuyController@upUserFrontRole');
      //删除角色
    Route::post('/delFrontRole','RobotOrder\UseIndexBuyController@delUserFrontRole');
     // 获取角色详细
    Route::post('/getFrontRole','RobotOrder\UseIndexBuyController@getFrontRole');
    //创建用户
    Route::post('/createFrontUser','RobotOrder\UseIndexBuyController@createFrontUser');
   //获取待分配的机器人微信号
    Route::post('/robotWxAlias','RobotOrder\UseIndexBuyController@getRobotWxAliasList');
    //获取管理用户列表
    Route::post('/getUserList','RobotOrder\UseIndexBuyController@getUserListByMid');
    //获取员工明细
    Route::post('/getUserInfo','RobotOrder\UseIndexBuyController@getUserInfoId');
    //删除账号
    Route::post('/delFrontUser','RobotOrder\UseIndexBuyController@delFrontUser');
    //更新
    Route::post('/upFrontUser','RobotOrder\UseIndexBuyController@upFrontUser');
    //获取所有分组
    Route::post('/getGroupAllList','RobotOrder\UseIndexBuyController@getGroupAllList');
    //群主预估收入
    //Route::post('/getGrouoMemberMoneyList','Member\MemberController@getGrouoMemberMoneyList');
});

//记录关键词  ls
Route::group(["prefix" => "keyWord", 'middleware'=>['user.check.login']], function () {
    //用户关键词列表
    Route::post('/keyWordsList','Robot\KeyWordController@getKeyWordsList');
    //用户关键词详情
    Route::post('/keyWordsDetail','Robot\KeyWordController@keyWordsDetail');
    //用户关键词详情
    //Route::post('/keyWordsDetail','Robot\KeyWordController@getKeyWordsDetail');
    //用户关键词添加
    Route::post('/addKeyWords','Robot\KeyWordController@addKeyWords');
    //用户关键词修改
    //Route::post('/updateKeyWords','Robot\KeyWordController@updateKeyWords');
    //用户关键词删除
    Route::post('/deleteKeyWords','Robot\KeyWordController@deleteKeyWords');
});



//自动回复  ls
Route::group(["prefix" => "reply", 'middleware'=>['user.check.login']], function () {
    //用户关键词列表
    Route::post('/keyWordsList','Robot\ReplyController@getKeyWordsList');
    //用户关键词详情
    Route::post('/keyWordsDetail','Robot\ReplyController@getKeyWordsDetail');
    //用户关键词添加
    Route::post('/addKeyWords','Robot\ReplyController@addKeyWords');
    //用户关键词修改
    Route::post('/updateKeyWords','Robot\ReplyController@updateKeyWords');
    //用户关键词删除
    Route::post('/deleteKeyWords','Robot\ReplyController@deleteKeyWords');

    //用户关键词回复列表
    Route::post('/keyWordsReplyList','Robot\ReplyController@keyWordsReplyList');
    //用户关键词回复添加
    Route::post('/addKeyWordsReply','Robot\ReplyController@addKeyWordsReply');
    //用户关键词回复修改
    Route::post('/updateKeyWordsReply','Robot\ReplyController@updateKeyWordsReply');
    //用户关键词回复删除
    Route::post('/deleteKeyWordsReply','Robot\ReplyController@deleteKeyWordsReply');



    //新人进群回复消息更新标签
    Route::post('/updateIntoGroupReplyTag','Robot\IntoGroupReplyController@updateIntoGroupReplyTag');
    //新人进群回复消息样式列表
    Route::post('/intoGroupReplyStyleList','Robot\IntoGroupReplyController@intoGroupReplyStyleList');
    //新人进群回复消息类型
    Route::post('/intoGroupReplyTypeList','Robot\IntoGroupReplyController@intoGroupReplyTypeList');
    //新人进群回复列表
    Route::post('/intoGroupReplyList','Robot\IntoGroupReplyController@intoGroupReplyList');
    //新人进群回复添加
    Route::post('/addIntoGroupReply','Robot\IntoGroupReplyController@addIntoGroupReply');
    //新人进群回复修改
    Route::post('/updateIntoGroupReply','Robot\IntoGroupReplyController@updateIntoGroupReply');
    //新人进群回复删除
    Route::post('/deleteIntoGroupReply','Robot\IntoGroupReplyController@deleteIntoGroupReply');


    //新人进群问候语保存
    Route::post('/addIntoGroupGreetingInfo','Robot\IntoGroupReplyController@addIntoGroupGreetingInfo');
    //新人进群问候语显示
    Route::post('/getIntoGroupGreetingInfo','Robot\IntoGroupReplyController@getIntoGroupGreetingInfo');

});

//用户端首页  ls
Route::group(["prefix" => "info"], function () {
    //管理员操作日志列表
    Route::post('/userOperationList','Admin\AdminIndexController@getUserOperationList');
    //根据用户昵称获取用户列表
    Route::post('/getUserListByName','Admin\AdminIndexController@getUserListByName');
    //用户首页数据
    Route::post('/adminIndex','Admin\AdminIndexController@index');
    Route::post('/adminIndexTotal','Admin\AdminIndexController@adminIndexTotal');
    //用户首页统计图数据
    Route::post('/adminIndexGraph','Admin\AdminIndexController@adminIndexGraph');
    //数据统计
    Route::any('/dataStatistics','Admin\AdminIndexController@dataStatistics');
    //专属机器人群 数据统计
    Route::any('/dataExclusiveStatistics','Admin\AdminIndexController@dataExclusiveStatistics');
    //群机器人状态更改
    Route::post('/changeGroupRobotStatus','Admin\AdminIndexController@changeGroupRobotStatus');
    //群主数据统计
    Route::post('/groupAdminStatistics','Admin\AdminIndexController@groupAdminStatistics');
    //群主佣金明细数据统计
    Route::post('/getGroupAdminCommission','Admin\AdminIndexController@getGroupAdminCommission');
    //群主佣金明细数据统计
    Route::post('/getGroupAdminCommissionEveryDay','Admin\AdminIndexController@getGroupAdminCommissionEveryDay');
    //专属机器人群主统计
    Route::post('/groupExclusiveAdminStatistics','Admin\AdminIndexController@groupExclusiveAdminStatistics');
    //查看群成员
    Route::post('/groupMemberList','Admin\AdminIndexController@getGroupMemberList');
    //修改群成员积分
    Route::post('/updateGroupMember','Admin\AdminIndexController@updateGroupMember');
    //修改群签到开关
    Route::post('/updateGroupSignSwitch','Admin\AdminIndexController@updateGroupSignSwitch');
    //修改群签到列表
    Route::post('/groupSignConfList','Admin\AdminIndexController@groupSignConfList')->middleware('user.check.login');
    //修改群签到详情
    Route::post('/groupSignConfDetail','Admin\AdminIndexController@groupSignConfDetail')->middleware('user.check.login');
    //修改群签到配置
    Route::post('/insertGroupSignConf','Admin\AdminIndexController@insertGroupSignConf')->middleware('user.check.login');
    //删除群签到配置
    Route::post('/deleteGroupSignConf','Admin\AdminIndexController@deleteGroupSignConf')->middleware('user.check.login');
    //查找群成员
    Route::post('/searchGroupMember','Admin\AdminIndexController@searchGroupMember');
    //查看当前的绑定的关系
    Route::post('/groupMaster','Admin\AdminIndexController@seeNowBingData');
    //根据手机号查询信息
    Route::post('/getMemInfoByMobile','Admin\AdminIndexController@getMemInfoByMobile');
    //给群绑定群主
    Route::post('/bindGroupMid','Admin\AdminIndexController@bindGroupMid');
    //管理员列表
    Route::post("/getUserAdminList","Admin\UserController@getUserAdminList")->middleware('user.check.login');
    //商品统计
    Route::post("/getProductStatistics","Admin\GroupManageController@getProductStatistics")->middleware('user.check.login');
    //群活跃度统计列表
    Route::post("/getGroupActivity","Admin\GroupManageController@getGroupActivityStatistics")->middleware('user.check.login');


    //商品统计
    Route::post("/changeOrderGoods","Community\ProductController@changeOrderGoods");
});



//专属机器人  ls
Route::group(["prefix" => "exclusive"], function () {
    //申请记录展示
    Route::post('/applicationList','Robot\ExclusiveController@getExclusiveApplicationList');
    //删除
    Route::post('/delapplicationByid','Robot\ExclusiveController@delapplicationByid');
    //申请记录添加
    Route::post('/addApplication','Robot\ExclusiveController@addExclusiveApplication');
    //申请记录审核
    Route::post('/updateApplication','Robot\ExclusiveController@updateExclusiveApplication');
    //品牌列表获取
    Route::post('/getBrandData','Robot\ExclusiveController@getBrandDataApplication');

    //邻居团申请专属机器人数据
    Route::post('/addExclusiveRobot','Robot\ExclusiveController@addExclusiveRobot');
});

//攻略分类管理  ls
Route::group(["prefix" => "strategy"], function () {
    //添加社群攻略分类
    Route::post('/addCategory','Community\StrategyCategoryController@addStrategyCategory')->middleware('user.check.login');
    Route::post('/updateCategory','Community\StrategyCategoryController@updateStrategyCategory')->middleware('user.check.login');
    Route::post('/deleteCategory','Community\StrategyCategoryController@deleteStrategyCategory')->middleware('user.check.login');
    Route::post('/getCategoryList','Community\StrategyCategoryController@getStrategyCategoryList')->middleware('user.check.login');
    //攻略文章
    Route::post('/addArticle','Community\StrategyArticleController@addStrategyArticle')->middleware('user.check.login');
    Route::post('/moveArticle','Community\StrategyArticleController@moveStrategyArticle')->middleware('user.check.login');
    Route::post('/removeArticle','Community\StrategyArticleController@removeStrategyArticle')->middleware('user.check.login');
    Route::post('/getArticleList','Community\StrategyArticleController@getStrategyArticleList')->middleware('user.check.login');
    Route::post('/getArticle','Community\StrategyArticleController@getStrategyArticle');
    Route::post('/deleteArticle','Community\StrategyArticleController@deleteStrategyArticle')->middleware('user.check.login');
    Route::post('/updateArticle','Community\StrategyArticleController@updateStrategyArticle')->middleware('user.check.login');
});

//用户端订单 spz
Route::group(["prefix" => "order"], function () {
    // 服务展示 订单展示
    Route::post('/getOrder','RobotOrder\RobotOrderController@createOrder');
    //获取支付参数
    Route::post('/getPay','RobotOrder\RobotOrderController@getPayPargrams');
    //支付回调
    Route::post('/callback','RobotOrder\RobotOrderController@callback');
    //支付相关的
    Route::post('/getPayInfo','RobotOrder\RobotOrderController@getPayInfo');
});

//用户端 购买商品订单列表
Route::group(["prefix" => "orderGoods"], function () {
    // 服务展示 订单商品类型列表
    Route::any('/getOrderGoodsTypeList','RobotOrder\RobotOrderController@getOrderGoodsTypeList');
    // 服务展示 订单展示
    Route::any('/getOrderList','RobotOrder\RobotOrderController@getOrderGoodsList');

});


//社群 群订单列表
Route::group(["prefix" => "groupOrder"], function () {
    // 服务展示 订单展示
    Route::post('/getOrderList','RobotOrder\RobotOrderController@getGroupOrderList');

});


//用户端消息  spz
Route::group(["prefix" => "source"], function () {
    //发消息的群列表
    Route::post('/getRoomList','RobotOrder\SourceMaterialController@getMyRoomList');
    //列表
    Route::post('/fodderTypeList','RobotOrder\SourceMaterialController@fodderTypeList');
    //列表
    Route::post('/linkTypeList','RobotOrder\SourceMaterialController@linkTypeList');
    //渠道筛选列表
    Route::post('/getSelectType','RobotOrder\SourceMaterialController@getSelectType');
    //列表
    Route::post('/getList','RobotOrder\SourceMaterialController@getSourceMaterialList');

    Route::post('/getMiniAppList','RobotOrder\SourceMaterialController@getMiniAppList');
    //详细
    Route::post('/getInfo','RobotOrder\SourceMaterialController@getSourceMaterialInfo');

    //详细
    Route::post('/getInfoV2','RobotOrder\SourceMaterialController@getSourceMaterialInfoV2');

    //添加
    Route::post('/addInfo','RobotOrder\SourceMaterialController@addSourceMaterial');
    //更改
    Route::post('/upInfo', 'RobotOrder\SourceMaterialController@upSourceMaterialInfo');

    //更改
    Route::post('/upCommunitySendInfoV2', 'RobotOrder\SourceMaterialController@upCommunitySendInfoV2');

    //删除
    Route::post('/delInfo','RobotOrder\SourceMaterialController@delSourceMaterialInfoById');
    //获取可使用的标签
    Route::post('/canMyUseTag','RobotOrder\SourceMaterialController@getCanUseTag');
    //获取渠道
    Route::post('/getChannel','RobotOrder\SourceMaterialController@channelByMid');
    //前端正则匹配文本域链接文本分离
    Route::post('/getTextUrlSeparate','RobotOrder\SourceMaterialController@getTextUrlSeparate');




    //列表
    Route::post('/getListForExter','RobotOrder\SourceMaterialController@getSourceMaterialListV2');


});

Route::group(["prefix" => "group"], function () {
    //自动建群
    Route::any('/autoCreateGroup','Community\GroupController@autoCreateGroup')->middleware('user.check.login');
    //下级用户列表
    Route::any('/getLowerMemberList','Community\GroupController@getLowerMemberList');
    //机器人列表
    Route::any('/getRobotList','Community\GroupController@getRobotList')->middleware('user.check.login');
    //邀请好友进群
    Route::any('/friendsIntoGroup','Community\GroupController@friendsIntoGroup')->middleware('user.check.login');
    //邀请好友进待创建群
    Route::any('/planFriendsIntoGroup','Community\GroupController@planFriendsIntoGroup')->middleware('user.check.login');
    //准备建群的群列表
    Route::any('/toCreateGroupList','Community\GroupController@toCreateGroupList')->middleware('user.check.login');
    //建群
    Route::any('/createGroup','Community\GroupController@createGroup');
    //获取群列表
    Route::any('/groupList','Community\GroupController@groupList');
    //获取群信息
    Route::any('/groupDetail','Community\GroupController@groupDetail');
    //提交群审核
    Route::any('/applyGroup','Community\GroupController@applyGroup');

});


//群标签管理
Route::group(["prefix" => "tag",'middleware'=>['user.check.login']], function () {
    //群标签列表
    Route::post('/getGroupTagList','Community\GroupController@getGroupTagList');
    //添加群标签
    Route::post('/addTag','Community\GroupController@addGroupTag');
    //修改群标签
    Route::post('/updateGroupTag','Community\GroupController@updateGroupTag');
    //删除群标签
    Route::post('/delGroupTag','Community\GroupController@delGroupTag');

});


//优惠券管理
Route::group(["prefix" => "coupon",'middleware'=>['user.check.login']], function () {
    //优惠券列表
    Route::post('/getCouponList','Community\GroupController@getCouponList');
    //给群主发送的优惠券存入队列
    Route::post('/getSendCouponList','RobotOrder\SourceMaterialController@getSendCouponList');
    //从队列中取出优惠券发给群主
    Route::post('/sendCouponToGroupOwn','RobotOrder\SourceMaterialController@sendCouponToGroupOwn');

});

//商品池
Route::group(["prefix" => "product",'middleware'=>['user.check.login']], function () {
//Route::group(["prefix" => "product"], function () {
    //普通商品列表
    Route::post('/getProductList','Community\ProductController@getProductList');
    //自营商品分类列表
    Route::post('/getCategoryList','Community\ProductController@getChannelOneCategoryList');
    //京东商品分类列表
    Route::post('/getClassList','Community\ProductController@jdByClassData');
    //京东商品分类商品列表
    Route::post('/getJdClassGoods','Community\ProductController@jdByClassIdData');
    //获取短链
    Route::post('/getShortLinkUrl','Community\ProductController@getShortLinkUrl');
    //社群机器人发送商品列表
    Route::post('/getRobotSendGoodList','Community\ProductController@getRobotSendGoodList');
    //社群商品上下架
    Route::post('/updateRobotSendGoodStatus','Community\ProductController@updateRobotSendGoodStatus');
    //社群商品删除
    Route::post('/deleteRobotSendGood','Community\ProductController@deleteRobotSendGood');
    //活动商品列表
    Route::post('/getActiveList','Community\ProductController@getActiveList');
    //添加活动
    Route::post('/addActiveProduct','Community\ProductController@addActiveProduct');
    //删除活动
    Route::post('/delActiveInfo','Community\ProductController@delActiveInfo');
    //编辑活动
    Route::post('/updateActiveProduct','Community\ProductController@updateActiveProduct');
    //活动详情
    Route::post('/getActiveInfo','Community\ProductController@getActiveInfo');
});

Route::post('/getTbkLongUrl','Community\ProductController@getTbkLongUrl');

Route::group(["prefix" => "group",'middleware'=>['user.check.login']], function () {
    //分队列表
    Route::post('/getGroupList','Robot\RobotStaffManageController@getGroupListData');
    //获取带绑定的管理员
    Route::post('/getNotBindMemberData','Robot\RobotStaffManageController@getNotBindMemberData');
    //添加名称 绑定管理员
    Route::post('/addGroupList','Robot\RobotStaffManageController@addGroupList');
    //获取详细
    Route::post('/getGroupInfo','Robot\RobotStaffManageController@getGroupInfo');
    //更新
    Route::post('/upGroupInfo','Robot\RobotStaffManageController@upGroupInfo');
    //移除单个
    Route::post('/delGroupInfo','Robot\RobotStaffManageController@delGroupInfo');
    //解散小组
    Route::post('/disbandGroup','Robot\RobotStaffManageController@disbandGroup');
    //直播社群统计
    Route::post('/getLiveList','Robot\RobotStaffManageController@getLiveList');

});

//悦呗数据接口
Route::group(["prefix"=>"exter"],function(){
    //YCloud平台使用
    //社群列表
    Route::any('/getGroupList','Admin\AdminIndexController@getGroupList');
    //群操作删除恢复
    Route::any('/updateGroupStatus','Admin\AdminIndexController@updateGroupStatus');
    //获取社群ID集合
    Route::any('/getRoomIdsByCondition','Admin\AdminIndexController@getRoomIdsByCondition');
    //统计信息机器人统计
    Route::post("/getStatisticsInfoByRobot","Admin\GroupManageController@getStatisticsInfoByRobot");
    //统计信息群维度统计
    Route::post("/getStatisticsInfoByRoomId","Admin\GroupManageController@getStatisticsInfoByRoomId");
    //查看群成员
    Route::post('/getYCloudGroupMemberList','Admin\AdminIndexController@getYCloudGroupMemberList');
    //社群列表--小程序
    Route::any('/getMiniAppRobotList','Admin\AdminIndexController@getMiniAppRobotList');
    //移除群助理--小程序
    Route::any('/deleteGroupStatus','Admin\AdminIndexController@deleteGroupStatus');
    //社群列表
    Route::any('/bindYCloudGroupMid','Admin\AdminIndexController@bindYCloudGroupMid');
    //绑定机器人
    Route::any('/bindYCloudRobot','Admin\AdminIndexController@bindYCloudRobot');
    //机器人列表
    Route::any('/getRobotList','RobotOrder\UseIndexBuyController@getRobotList');
    //机器人状态修改
    Route::any('/updateRobotStatus','RobotOrder\UseIndexBuyController@updateRobotStatus');
    //群主数据统计
    Route::any('/groupYCAdminStatistics','Admin\AdminIndexController@groupYCAdminStatistics');


    //YCloud申请群助理
    Route::any('/applyYCGroupAssistant', 'Community\CommunityController@applyYCGroupAssistant');
    //YCloud申请群助理通过
    Route::any('/applyYCRobotPass', 'Admin\AdminIndexController@applyYCRobotPass');
    //YCloud申请群助理拒绝
    Route::any('/applyYCRobotRefuse', 'Admin\AdminIndexController@applyYCRobotRefuse');
    //申请群助理文案
    Route::any('/applyYCloudCopyWriting', 'Community\CommunityController@applyYCloudCopyWriting');
    //申请群助理文案--规则
    Route::any('/applyYCloudRules', 'Community\CommunityController@applyYCloudRules');
    //机器人申请提交判断装填-YCloud
    Route::any('/applyYCloudStatus', 'Community\CommunityController@applyYCloudStatus');

    //攻略文案
    //新人上手 数据库
    Route::any('/raidersYCloudNewList', 'Community\CommunityController@raidersYCloudNewList');
    //进阶学习
    Route::any('/raidersYCloudStudyArticle', 'Community\CommunityController@raidersYCloudStudyArticle');
    //邀请海报
    Route::any('/raidersYCloudInvitePost', 'Community\CommunityController@raidersYCloudInvitePost');
    //生成邀请海报
    Route::any('/raidersYCloudSharePoster', 'Community\CommunityController@raidersYCloudSharePoster');

    //数据中台
    //社群接口
    Route::post('/getGroupNumCount','Community\ExternalController@getGroupNumCount');
    //社群数量增长统计图
    Route::post('/getGroupNumCountGraph','Community\ExternalController@getGroupNumCountGraph');
    //360人的群占比统计图
    Route::post('/groupProportionGraph','Community\ExternalController@groupProportionGraph');
    //社群人数量
    Route::post('/groupMemberNum','Community\ExternalController@groupMemberNum');
    //社群人数量统计图数据
    Route::post('/groupMemberNumGraph','Community\ExternalController@groupMemberNumGraph');
    //社群人数占比统计
    Route::post('/groupMemberProportionGraph','Community\ExternalController@groupMemberProportionGraph');
    //社群人数性别占比统计
    Route::post('/sexProportionGraph','Community\ExternalController@sexProportionGraph');


    //社群交易
    Route::post('/orderGMVCount','Community\ExternalController@orderGMVCount');
    //社群交易统计--交易金额走势图
    Route::post('/orderGMVCountGraph','Community\ExternalController@orderGMVCountGraph');
    //社群交易--订单数量列表
    Route::post('/orderCountList','Community\ExternalController@orderCountList');
    //社群交易统计--订单数量走势图
    Route::post('/orderNumCountGraph','Community\ExternalController@orderNumCountGraph');
    //社群订单渠道构成统计图数据
    Route::post('/orderSourceProportionGraph','Community\ExternalController@orderSourceProportionGraph');


    //社群排行
    //社群排行--群排行Top30
    Route::post('/groupTopGraph','Community\ExternalController@groupTopGraph');
    //社群排行--群统计详情
    Route::post('/groupStatisticsDetail','Community\ExternalController@groupStatisticsDetail');
    //社群排行--群统计列表
    Route::post('/groupStatisticsList','Community\ExternalController@groupStatisticsList');
    //社群排行详情--交易金额走势图
    Route::post('/groupOrderGMVDetailGraph','Community\ExternalController@groupOrderGMVDetailGraph');
    //社群排行详情--订单走势图
    Route::post('/groupOrderNumDetailGraph','Community\ExternalController@groupOrderNumDetailGraph');
    //社群排行详情--商品销售排行列表
    Route::post('/getSellProductList','Community\ExternalController@getSellProductList');
    //社群排行详情--订单记录
    Route::post('/getGroupOrderList','Community\ExternalController@getGroupOrderList');

    //群主排行
    //群主排行--群主排行Top30
    Route::post('/groupAdminTopGraph','Community\ExternalController@groupAdminTopGraph');
    //群主排行--群主统计列表
    Route::post('/groupAdminStatisticsList','Community\ExternalController@groupAdminStatisticsList');

    //群主排行详情--群主详情信息
    Route::post('/groupAdminStatisticsDetail','Community\ExternalController@groupAdminStatisticsDetail');
    //群主排行详情--群主交易金额走势图
    Route::post('/groupAdminOrderGMVGraph','Community\ExternalController@groupAdminOrderGMVGraph');
    //群主排行详情--群主订单走势图
    Route::post('/groupAdminOrderDetailGraph','Community\ExternalController@groupAdminOrderDetailGraph');
    //群主排行详情--群主群列表
    Route::post('/groupAdminGroupList','Community\ExternalController@groupAdminGroupList');
    //群主排行详情--群主群订单记录
    Route::post('/getGroupAdminGroupOrderList','Community\ExternalController@getGroupAdminGroupOrderList');




    //社群机器人发送商品列表 悦地摊使用
    Route::post('/getRobotSendGoodList','Community\ExternalController@getRobotSendGoodList');
    //悦地摊首页爆款商品列表
    Route::post('/getFaddishGoodList','Community\ExternalController@getFaddishGoodList');

    //个人中心接口, 收益统计
    Route::post('/getMemberProfitCount','Member\MemberController@getMemberProfitCount');
    //个人中心接口, 群主收益列表
    Route::post('/getGroupAdminCommissionList','Member\MemberController@getGroupAdminCommissionList');

    //对接直订平台, 通过群主mid 查询群id
    Route::post('/getRoomIdsByGroupAdmin','Community\ExternalController@getRoomIdsByGroupAdmin');

    Route::post('/getGroupOrderData','Community\ExternalController@getGroupOrderData');

    Route::post('/getRobotNumData','Community\ExternalController@getRobotNumData');

    Route::post('/getExclusiveRobotList','Community\ExternalController@getExclusiveRobotList');

    Route::post('/getExclusiveGroupList','Community\ExternalController@getExclusiveGroupList');

    Route::post('/getRobotListForSearch','Community\ExternalController@getRobotListForSearch');

    Route::post('/getMemeberInfo','Member\MemberController@getMemeberInfoByMid');

    Route::post('/getMemeberCash','Member\MemberController@getMemeberCash');

    Route::post('/getMemeberBonusRecord','Member\MemberController@getMemeberBonusRecord');

    Route::post('/updateOrderStatus','Member\MemberController@userBonusBecommitted');

    Route::post('/getMemeberProfit','Member\MemberController@getMemeberProfit');

    Route::post('/getMemeberProfitV2','Member\MemberController@getMemeberProfitV2');

    Route::post('/sendUserFrozenMoney','Member\MemberController@sendUserFrozenMoney');

    Route::post('/getCommunityStatistics','Community\ExternalController@getCommunityStatistics');

    Route::post('/getGrowTrend','Community\ExternalController@getGrowTrend');

    Route::post('/updateReturnOrderList','Member\MemberController@updateReturnOrderList');

    Route::post('/getNotReceiveOrder','Member\MemberController@getNotReceiveOrder');

    Route::post('/getNotReceiveCpsOrder','Member\MemberController@getNotReceiveCpsOrder');

    Route::post('/getGrouoMemberMoneyList','Member\MemberController@getGrouoMemberMoneyList');
    
    Route::post('/getAssetManagement','Member\MemberController@getAssetManagement');
    
    Route::post('/updateZhidingOrderMobileData','Member\MemberController@updateZhidingOrderMobileData');
    
    Route::post('/insertLocalLifeOrderData','Member\MemberController@insertLocalLifeOrderData');

    Route::post('/getActiveInfo','Community\ProductController@getActiveInfoForH5');


    Route::post('/isStationmaster','Community\ExternalController@getStationmasterByMid');
});


Route::group(["prefix"=>"task"],function(){

    Route::post('/getTaskList','Community\GroupTaskController@getList');

    Route::post('/addTaskInfo','Community\GroupTaskController@addTask');

    Route::post('/getTaskInfo','Community\GroupTaskController@getTaskInfo');

    Route::post('/editTaskInfo','Community\GroupTaskController@editTask');

    Route::post('/delTask','Community\GroupTaskController@delTask');

    Route::post('/getTaskInfoForPl','Community\GroupTaskController@getTaskInfoForPl');

    Route::post('/taskExamine','Community\GroupTaskController@taskExamine');

    Route::post('/getTaskListForMid','Community\GroupTaskController@getTaskListForMid');

    Route::post('/addGroupTaskRelation','Community\GroupTaskController@addGroupTaskRelation');
    
    Route::post('/getGroupTaskListForTaskId','Community\GroupTaskController@getGroupTaskListForTaskId');

    Route::post('/updateGroupTaskStatus','Community\GroupTaskController@updateGroupTaskStatus');
    
    Route::post('/updateMoneySendStatus','Community\GroupTaskController@updateMoneySendStatus');
    
    Route::post('/test','Community\GroupTaskController@test');
});


Route::group(["prefix"=>"test"],function(){

    Route::post('/mongodb','RobotOrder\MongodbController@MongodbTest');

    Route::post('/getJdOrderList','Robot\RobotStaffManageController@getJdOrderList');
});


Route::group(["prefix"=>"yiyun"],function(){
    Route::any('/callBack','Platform\YiyunController@callBack');
    Route::any('/getStatus','Platform\YiyunController@getCallBackStatus');
    Route::any('/login','Platform\YiyunController@login');

    /**
     * 企业微信相关接口
     */
    Route::any('/enterprise/callBack','Platform\EnterpriseController@callBack');
    Route::any('/enterprise/login','Platform\EnterpriseController@login');
    Route::any('/enterprise/logOut','Platform\EnterpriseController@logOut');
    Route::any('/enterprise/getStatus','Platform\EnterpriseController@getCallBackStatus');
    Route::any('/enterprise/createCustomerGroup','Platform\EnterpriseController@createCustomerGroup');
    //获取我的客户列表
    Route::any('/enterprise/getMyCustomers','Platform\EnterpriseController@getMyCustomers');
    //获取保存在通讯录中群列表
    Route::any('/enterprise/getContactGroups','Platform\EnterpriseController@getContactGroups');
    //获取机器人开关状态接口
    Route::any('/enterprise/configs','Platform\EnterpriseController@configs');
    //测试消息发送
    Route::any('/enterprise/sendInfo','Platform\EnterpriseController@sendInfo');
});
