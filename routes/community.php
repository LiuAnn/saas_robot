<?php

//用于  测试  机器使用
Route::group(['prefix' => 'user'], function () {
    Route::post('/upload', 'Community\IndexController@uploadDataTest');
    Route::post('/userBehavior', 'Community\IndexController@userBehaviorTest');
});

//用于  线上  机器使用
Route::group(['prefix' => 'product'], function () {
    //探鲸易云机器人测试
    Route::post('/yiYunRobotTest', 'Community\IndexController@yiYunRobotTest');
    Route::post('/productUpload', 'Community\IndexController@uploadDataProduct');
    Route::post('/productBehavior', 'Community\IndexController@userBehaviorProduct');
    Route::post('/weixinInfo', 'Community\IndexController@deleteUselessData');
    Route::post('/updateCommonGroupNum', 'Community\IndexController@updateCommonGroupNum');
     //
    Route::post('/updatewhite', 'Community\IndexController@updateWhite');
    //查看redis
     Route::post('/getRedisData', 'Community\IndexController@updateRedisData');
     //查看大人接口数据getTest
    Route::post('/darenlink', 'Community\ProductController@getTest');
});

Route::group(['prefix' => 'group'], function () {
    //群激活首页
    Route::post('/activationIndex', 'Community\groupActivationController@groupHomeindexInfo');
    //群列表更多
    Route::post('/groupActivationList', 'Community\groupActivationController@groupHomeListByPage');
    //未激活详细页
    Route::post('/noActivationRoomById', 'Community\groupActivationController@getGroupNotActivationInfo');
    // 群成员列表
    Route::post('/queryMemberList', 'Community\ManagementController@queryMemberList');
    // 群详细信息
    Route::post('/queryGroupInfo', 'Community\ManagementController@queryGroupInfo');
    // 群分享
    Route::post('/queryShareInfo', 'Community\ManagementController@queryShareInfo');
    // 群列表
    Route::post('/queryGroupList', 'Community\ManagementController@queryGroupList');
});

// 订单部分
Route::group(['prefix' => 'order'], function () {
    // 群订单列表
    Route::post('/queryOrderList', 'Community\OrderController@queryOrderList');
    // 商品列表
    Route::post('/queryGoodsList', 'Community\OrderController@queryGoodsList');
});
/**
 * 前期规则
 */
Route::group(['prefix' => 'groupRule'], function () {

    //攻略分类   已调整
    Route::any('/raidersLabel', 'Community\CommunityController@raidersLabel');
    //新人上手 数据库  已调整
    Route::any('/raidersNewList', 'Community\CommunityController@raidersNewList');
    //新人上手 数据库
    Route::any('/raidersNewListV2', 'Community\CommunityController@raidersNewListV2');
    //邀请海报 数据库 已调整
    Route::any('/invitePosterList', 'Community\CommunityController@invitePosterList');
    //升级  已调整
    Route::any('/upgrade', 'Community\CommunityController@upgrade');
    //群开通审核 提交数据
    Route::any('/applyReview', 'Community\CommunityController@applyReview');
    //群二次审核 提交数据
    Route::any('/applyReviewV2', 'Community\CommunityController@applyReviewV2');
    //我的群列表
    Route::any('/groupList', 'Community\CommunityController@groupList');
    //展示三方的群数据
    Route::any('/groupListThrid', 'Community\CommunityController@groupListThridGroup');
    //申请群助理
    Route::any('/applyGroupAssistant', 'Community\CommunityController@applyGroupAssistant');
    //申请群助理
    Route::any('/applyGroupAssistantV2', 'Community\CommunityController@applyGroupAssistantV2');
    //获取群信息
//    Route::any('/groupInfo', 'Community\CommunityController@groupInfo');
//    //转链
//    Route::any('/conversionUrl', 'Community\CommunityController@conversionUrl');
//    //通讯录
//    Route::any('/addressBook', 'Community\CommunityController@addressBook');
//    //通讯录粉丝分页
//    Route::any('/fanList', 'Community\CommunityController@fanList');
//    //更改用户微信
//    Route::any('/updateWx', 'Community\CommunityController@updateWx');

    //分享小程序 已调整
    Route::any('/shareXcx', 'Community\CommunityController@shareXcx');
   //进阶学习 已调整
    Route::get('list/{typeId}/{pageSize}/{pageIndex}/{source}','Community\CommunityController@getListJinjie');
    //进阶学习 详细 已调整
    Route::get('listInfo/{typeId}/{id}/{source}','Community\CommunityController@listInfo');

//    //分享海报
    Route::any('/sharePoster', 'Community\CommunityController@sharePoster');

    //申请群助理文案
    Route::any('/applyCopyWriting', 'Community\CommunityController@applyCopyWriting');

    //新人上手 数据库
    Route::any('/videoUrl', 'Community\CommunityController@getVideoUrl');
});

/**
 * 悦淘专属机器人数据
 */
Route::group(['prefix' => 'dedrobot'], function () {

      //addSourceMaterialV2
       Route::post('/addSendInfo', 'RobotOrder\SourceMaterialController@addSourceMaterialV2'); 
      // 判断有没有专属机器人
      Route::post('/isdedicatedrobot', 'Community\ExclusiveMangerController@isDedicatedRobotCon');
      //判断是否微助理发送素材
      Route::post('/upsendstatus', 'Community\ExclusiveMangerController@upSendStatusCon');
      //新增素材数据
      Route::post('/addsenddata', 'Community\ExclusiveMangerController@addSendDataToRedis');
      //移除素材发送
      Route::post('/delsenddata', 'Community\ExclusiveMangerController@delSendDataRedis');
      //删除缓存
      Route::post('/delcache', 'Community\ExclusiveMangerController@delRedisCacheData');
});

