<?php


//登录
Route::any("login" , "User\LoginController@login");

//联合登录
Route::any("loginForMiddle" , "User\LoginController@loginForMiddle");

//注册
Route::any("register" , "User\LoginController@register");

//登出
Route::any("loginOut" , "User\LoginController@loginOut");

//发送验证码
Route::any("sendMessage" , "User\LoginController@sendMessage");

//更新密码
Route::any("updatePassword" , "User\LoginController@updatePassword");
