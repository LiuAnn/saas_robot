<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(["prefix" => "main"], function () {
    //小悦客户端首页
    Route::post("/getIndexInfo", "Client\IndexController@getIndexInfo");
    //根据群主自定义标签自动匹配商品
    Route::post("/getRecommendGoodsByGroupTags", "Client\IndexController@getRecommendGoodsByGroupTags");
});

Route::group(["prefix" => "localLife"], function () {
    //小悦客户端本地生活列表
    Route::any("/getList", "Client\LocalLifeController@index");
    //小悦客户端本地生活详情
    Route::any("/getDetail", "Client\LocalLifeController@getOne");
    Route::any("/share", "Client\LocalLifeController@share");
});

Route::group(["prefix" => "users"], function () {
    //小悦客户端用户信息
    Route::any("/detail", "User\UsersController@getOne");
    Route::any("/channel", "User\UsersController@getChannel");
    Route::any("/group", "User\UsersController@getGroupList");
    Route::any("/group/tag/set", "User\UsersController@setGroupTags");
    Route::any("/login", "User\UsersController@login");
});

# Open API
Route::group(["prefix" => "open-api"], function () {
    # 获取用户群信息
    Route::any("/getGroupList", "OpenAPI\GroupController@getGroupList");
    # 机器人信息
    Route::any("/robot/info", "OpenAPI\RobotController@getInfo");
    # 修改昵称
    Route::any("/robot/setNickname", "OpenAPI\RobotController@setNickname");
    # 修改头像
    Route::any("/robot/setAvatar", "OpenAPI\RobotController@setAvatar");
    # 添加好友
    Route::any("/robot/addFriends", "OpenAPI\RobotController@addFriends");
});