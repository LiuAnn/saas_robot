<?php

namespace App\Http\Middleware;

use App\Model\Mall\OperationLog;
use Illuminate\Support\Facades\Redis;
use Closure;
use Illuminate\Support\Facades\Auth;

class OperationLogMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->all(); //操作的内容

        $path = $request->path();  //操作的路由
        $method = $request->method();  //操作的方法
//        $ip = $request->ip();  //操作的IP

//        $usernum = $request->usernum;  //操作人(要自己获取)

        $header = $request->header('Authorization');

        if($header)
        {
            if($path!='admin/admin/getUserPowers')
            {
                $decodeStr = explode(' ', $header);

                list($flag, $key) = $decodeStr;

                $decodeStr  = explode('.', $key);

                list($id, $acceptSign) = $decodeStr;

//              self::writeLog($id,json_encode($header),$path,$method);
                self::writeLog($id,$input,$path,$method);
            }
        }

        return $next($request);

    }
    public  function writeLog($admin_user_id,$input,$path,$method){

        $log = new OperationLog();
        $log->setAttribute('admin_user_id', $admin_user_id);
        $log->setAttribute('path', $path);
        $log->setAttribute('method', $method);
        $log->setAttribute('input', json_encode($input, JSON_UNESCAPED_UNICODE));
//        $log->setAttribute('created_at', date("Y-m-d H:i:s",time()));//入库自动有时间
        $log->setAttribute('updated_at', null);
        $log->save();
    }
}
