<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthorizationException;
use App\Service\Admin\AdminUserService;
use Illuminate\Support\Facades\Redis;

use Illuminate\Http\Request;
use Closure;

/**
 * 后台接口验证登录
 */
class AdminAuthVerificationMiddleware {

    public function handle(Request $request, Closure $next) {

        $header = $request->header('Authorization');

        //判断header包含Authorization参数
        if(!$header){
            throw new AuthorizationException('Bearer');
        }

        $decodeStr = explode(' ', $header);


        if(count($decodeStr) != 2) {
            throw new AuthorizationException('Bearer');
        }

        list($flag, $key) = $decodeStr;


        //判断是否是系统验证
        if($flag !== 'Admin'){
            throw new AuthorizationException('Bearer');
        }

        //解析字符串,判断是否符合格式€
        $decodeStr  = explode('.', $key);
        $deCount    = count($decodeStr);

        if($deCount != 2 ) {
            throw new AuthorizationException('Bearer');
        }

        $memberToken = "";
        //获取序号ID,时间戳，接受的sign
        if($deCount == 2) {
            list($id, $acceptSign) = $decodeStr;

            // 验证用户的token是否正确
            $key  = "data_robot_admin_token_mid_". $id;;

            $memberToken = Redis::get($key);
        }

        if($acceptSign != $memberToken) {
            throw new AuthorizationException('Bearer');
        }

        // 获取用户数据
        $member = AdminUserService::getAdminUser(['adminId'=>$id]);

        if(empty($member['data']))
        {
            throw new AuthorizationException('用户不存在!');
        }
//        $url='http://'.$_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"];

        $request->attributes->add(['member'=>$member['data']]);//添加参数
//        $request->member = $member['data'];



        return $next($request);
    }
}
?>
