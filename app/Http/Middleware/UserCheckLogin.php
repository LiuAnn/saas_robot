<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthorizationException;
use App\Model\User\UserModel;
use App\Service\User\LoginService;
use Closure;

class UserCheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');

        //判断header包含Authorization参数
        if(!$header){
            throw new AuthorizationException('Bearer');
        }
        $decodeStr = explode(' ', $header);

        if(count($decodeStr) != 2) {
            throw new AuthorizationException('Bearer');
        }
        list($flag, $key) = $decodeStr;

        //判断是否是系统验证
        if($flag !== 'Admin'){
            throw new AuthorizationException('Bearer');
        }

        //解析字符串,判断是否符合格式€
        $decodeStr  = explode('.', $key);
        $deCount    = count($decodeStr);

        if($deCount != 2 ) {
            throw new AuthorizationException('Bearer');
        }

        $memberToken = "";
        //获取序号ID,时间戳，接受的sign
        if($deCount == 2) {
            list($id, $acceptSign) = $decodeStr;

            // 验证用户的token是否正确
            $memberToken = LoginService::getToken($id);
        }
        if($acceptSign != $memberToken) {
            throw new AuthorizationException('Bearer');
        }

        // 获取用户数据
        $member = UserModel::getLoginUser(['mid' => $id,'delete_at'=>0]);

        if (is_object($member)) {
            $member = json_encode($member);
            $member = json_decode($member, true);
        }
        $request->member = $member;//添加参数
        return $next($request);
    }
}
