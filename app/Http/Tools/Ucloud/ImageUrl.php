<?php
/**
*  
*/
class ImageUrl
{
	public static $img = array( "jpg", "jpeg", "gif", "png", "bmp" );
    const MILLISECOND_MULTIPLE = 10000;
    const FILETIME_DIFF = 11644473600000;


	function NewFileName($file)
    {
        $dotIdx = strrpos($file, ".");
        $ext = substr($file, $dotIdx);
        $type = substr($file, $dotIdx+1);
        if (in_array($type, self::$img))
        {
            $currenttimemillis = self::Get_millisecond();
            $time = (self::FILETIME_DIFF + $currenttimemillis) * self::MILLISECOND_MULTIPLE;
            $iRandom = rand(100, 999);
            $iRandom2 = rand(100, 999);
            return number_format($time,0,'','') . $iRandom . $iRandom2 . $ext;
        }
        return "";
    }

    function GetBuildFilePath($file)
    {
        $dotIdx = strrpos($file, ".");
        $ext = substr($file, $dotIdx);
        $type = substr($file, $dotIdx+1);
        if (in_array($type, self::$img))
        {
            $temfilename = substr($file, 0, $dotIdx-6);
            $_time = date('Y/m/d/H/i/',($temfilename/self::MILLISECOND_MULTIPLE-self::FILETIME_DIFF)/1000);
            return $_time;
        }
        return "";
    }

    function Get_millisecond() {
      list($msec, $sec) = explode(' ', microtime());
      return sprintf('%.0f',(floatval($msec)+floatval($sec))*1000);
    }
}
