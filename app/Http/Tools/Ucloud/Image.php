<?php

namespace App\Http\Tools\Ucloud;

defined('SUCESS_CODE') or define('SUCESS_CODE',100);

class Image
{
	
	private $ucloud_bucket = 'zdimg';  		 			//ucloud bucket
	//public $img_url = 'http://image.365zhiding.com/';  	//图片服务器地址
	public $img_url = 'https://image.yuelvhui.com/';  	//图片服务器地址
	public $dir = 'pubfile/';
	private $_allow;                          //允许文件类型
	private $_maxsize = 500; 						  //允许上传文件大500kb
	
	public function __construct()
	{
		$this->_allow = array('jpg','png','jpeg');
	}
	
	private function fileinfo($file,$filename)
	{
		$pos = strpos($file,'.');
		$fix = strtolower(substr($filename,$pos+1));
		$this->fix = $fix;
		$this->size = filesize($file);
	}
	
	public function uploadUcloud($file,$filename)
	{
		$this->fileinfo($file,$filename);
		
		$filename = $this->dir. date("Y").'/'.date("m").'/'.date("d")."/".$filename;
		$bucket = $this->ucloud_bucket;
		$info=array('error_code'=>'','msg'=>'');
		
		include('proxy.php');
		list($data, $err) = UCloud_MInit($bucket, $filename);
		if ($err)
		{
			$info['error_code']=$err->Code;
			$info['msg']=$err->ErrMsg;
			return $info;
		}
		$uploadId = $data['UploadId'];
		$blkSize  = $data['BlkSize'];
		list($etagList, $err) = UCloud_MUpload($bucket, $filename, $file, $uploadId, $blkSize);
		
		if ($err)
		{
			$info['error_code']=$err->Code;
			$info['msg']=$err->ErrMsg;
			return $info;
		}

		list($data, $err) = UCloud_MFinish($bucket, $filename, $uploadId, $etagList);
		
		if ($err) 
		{
			$info['error_code']=$err->Code;
			$info['msg']=$err->ErrMsg;
			return $info;
		}
		$info['error_code']=SUCESS_CODE;
		$info['msg']=$this->img_url.$filename;
		return $info;
	}

}
