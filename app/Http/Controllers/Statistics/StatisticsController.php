<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 2020/12/9
 * Time: 下午12:03
 * Describe:
 */

namespace App\Http\Controllers\Statistics;

use App\Http\Controllers\Controller;
use App\Service\Statistics\GroupService;
use Illuminate\Http\Request;
use App\Models\SyncGroup;

class StatisticsController extends Controller
{
    protected $groupService;

    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    /**
     * 获取群统计
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGroup(Request $request)
    {
        $param['page'] = $request->input('page', 1);
        $param['pageSize'] = $request->input('pageSize', 10);
        $param['platform'] = $request->input('platform', '');
        $param['platform_identity'] = $request->input('platformIdentity', '');
        $param['beginTime'] = $request->input('beginTime', '');
        $param['endTime'] = $request->input('endTime', '');
        $param['order_number'] = $request->input('orderNumber', 0);
        $param['order_amount'] = $request->input('orderAmount', 0);
        $data = $this->groupService->getGroup($param);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    /**
     * 获取群主统计
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLeader(Request $request)
    {
        $param['page'] = $request->input('page', 1);
        $param['pageSize'] = $request->input('pageSize', 10);
        $param['platform'] = $request->input('platform', '');
        $param['platform_identity'] = $request->input('platformIdentity', '');
        $param['phone'] = $request->input('phone', '');
        $param['group_count'] = $request->input('groupCount', 0);
        $param['order_amount'] = $request->input('orderAmount', 0);
        $param['order_number'] = $request->input('orderNumber', 0);
        $data = $this->groupService->getLeader($param);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }
}