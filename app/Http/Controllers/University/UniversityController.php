<?php

namespace App\Http\Controllers\University;

use App\Http\Controllers\Controller;
use App\Service\University\UniversityService;
use App\Service\University\UniversityServiceImpl;
use App\Service\University\UniversityTypeServiceImpl;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class UniversityController extends Controller
{


    public function getOrCodeImages(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return UniversityService::getOrCodeImages($param);
    }

    public function getOrCodeImage(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return UniversityService::getOrCodeImage($param);
    }


    public function upOrCodeImage(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return UniversityService::upOrCodeImage($param);
    }


    /**
     * 创建消息
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {
        $instance = UniversityServiceImpl::create([
            //标题
            'title'=>$request->get('title'),
            //概要
            'summary'=>$request->get('summary'),
            //详情
            'content'=>$request->get('content'),
            //图片详情
            'content_img'=>$request->get('content_img'),
            //类型
            'type_id'=>$request->get('type_id'),
            //作者
            'author' =>$request->get('author'),
            //图片
            'cover_pic'=>$request->get('cover_pic'),

            'sort'=>$request->get('sort'),
            'is_online'=>$request->get('is_online'),

            'cover_pic_all'=>$request->get('cover_pic_all'),

            'video_path'=>$request->get('video_path'),

            'jump'=>$request->get('jump'),

        ]);

        return ['code'=>200,'msg'=>'消息创建成功','data'=>[
            'aid'=>$instance->getAid()
        ]];
    }

    /**
     * 修改消息
     * @param Request $request
     * @return array
     */
    public function modify(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->get('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'消息不存在'];
        }

        $instance->setTypeId($request->get('type_id'));

        $request->has('title') ? $instance->setTitle($request->get('title')) : '';

        $request->has('sort') ? $instance->setSort($request->get('sort')) : '';

        $request->has('is_online') ? $instance->setIsOnline($request->get('is_online')) : '';

        $request->has('video_path') ? $instance->setVideoPath($request->get('video_path')) : '';

        $request->has('jump') ? $instance->setJump($request->get('jump')) : 0;

        $request->has('type_id') ? $instance->setTypeId($request->get('type_id')) : '';

        $request->has('summary') ? $instance->setSummary($request->get('summary')) : '';

        $request->has('content') ? $instance->setContent($request->get('content')) : '';

        $request->has('content_img') ? $instance->setContentImg($request->get('content_img')) : '';

        $request->has('author') ? $instance->setAuthor($request->get('author')) : '';

        $request->has('mid') ? $instance->setMid($request->get('mid')) : '';

        $request->has('cover_pic') ? $instance->setCoverPic($request->get('cover_pic')) : '';

        $request->has('cover_pic_all') ? $instance->setCoverPicAll($request->get('cover_pic_all')) : '';

        $instance->update();

        return ['code'=>200,'msg'=>'消息修改成功'];
    }

    /**
     * 获取列表
     * @param Request $request
     * @return array
     */
    public function getList(Request $request)
    {
        $pageIndex = $request->route('pageIndex');

        $pageSize = $request->route('pageSize');

        $typeId = $request->route('typeId') ? $request->route('typeId') : 0;

        $title = $request->route('title');

        $content = $request->route('content');

        $instances = UniversityServiceImpl::getInstancePage($pageIndex,$pageSize, [
            'title'=>$title,
            'content'=>$content,
            'typeId'=>$typeId
        ]);

        //判断当前栏目的bannerAid
        $universityTypeInstance = UniversityTypeServiceImpl::getInstance($typeId);
        $bannerAid = $universityTypeInstance ? $universityTypeInstance->getBannerAid() : 0;

        $instances['list'] = array_map(function($instance)use($bannerAid){
            $typeInfo      = UniversityTypeServiceImpl::getInstance($instance->getTypeId());
            $cover_pic_all = $instance->getCoverPicAll();
            return [
                'aid'=>$instance->getAid(),
                'title'=>$instance->getTitle(),
                'author'=>$instance->getAuthor(),
                'content'=>mb_substr($instance->getContent(),0,30),
                'typeId'=>$instance->getTypeId(),
                'typePid'=>!empty($typeInfo)?$typeInfo->getTypePid():'',
                'typeName'=>$instance->getTypeName(),
                'cover_pic'=>$instance->getCoverPic(),
                'cover_pic_all'=>!empty($cover_pic_all)?explode(",",$cover_pic_all):[],
                'is_banner'=>$bannerAid == $instance->getAid() ? 1 : 0,
                'created_at'=>$instance->getCreatedAt(),
                'sort'=>$instance->getSort(),
                'is_online'=>$instance->getIsOnline(),
                'down_num'=>$instance->getDownNum(),
                'like_num'=>$instance->getLikeNum()
            ];
        },$instances['list']);

        return ['code'=>200,'msg'=>'查询成功','data'=>$instances];
    }

    /**
     * 删除消息
     * @param Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->get('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'消息不存在'];
        }

        $instance->delete();

        return ['code'=>200,'msg'=>'消息删除成功'];
    }

    /**
     * 获取详情
     * @param Request $request
     * @return array
     */
    public function detail(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->route('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'消息不存在'];
        }

        return [
            'code'=>200,
            'data'=>[
                'title'=>$instance->getTitle(),
                'content'=>$instance->getContent(),
                'content_img'=>$instance->getContentImg(),
                'typeId'=>$instance->getTypeId(),
                'typeName'=>$instance->getTypeName(),
                'cover_pic'=>$instance->getCoverPic(),
                'cover_pic_all'=>$instance->getCoverPicAll(),
                'author'=>$instance->getAuthor(),
                'created_at'=>$instance->getCreatedAt(),
                'sort'=>$instance->getSort(),
                'is_online'=>$instance->getIsOnline(),
                'video_path'=>$instance->getVideoPath(),
                'jump'=>$instance->getJump(),
            ]
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function setTypeBanner(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->get('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'消息不存在'];
        }

        $instance->setBannerAid($request->get('type_id'));

        return ['code'=>200,'msg'=>'操作成功'];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function setArticleIsOnline(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->get('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'消息不存在'];
        }

        $instance->setArticleIsOnline($request->get('aid'),$request->get('is_online'));

        return ['code'=>200,'msg'=>'操作成功'];
    }

    public function setArticleLikeNum(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->get('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'数据不存在'];
        }

        $instance->setArticleLikeNum($request->get('aid'),$request->get('like_num'));

        return ['code'=>200,'msg'=>'操作成功'];
    }

    public function setArticleDownNum(Request $request)
    {
        $instance = UniversityServiceImpl::getInstance($request->get('aid'));

        if(!$instance){
            return ['code'=>400,'msg'=>'数据不存在'];
        }

        $instance->setArticleDownNum($request->get('aid'),$request->get('down_num'));

        return ['code'=>200,'msg'=>'操作成功'];
    }
}