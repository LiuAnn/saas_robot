<?php

namespace App\Http\Controllers\University;

use App\Http\Controllers\Controller;
use App\Service\University\UniversityTypeServiceImpl;
use Illuminate\Http\Request;

class UniversityTypeController extends Controller
{
    /**
     * 添加分类
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {
        $instance = UniversityTypeServiceImpl::create([
            'type_name' => $request->get('type_name'),
            'type_pid'  => $request->get('type_pid'),
            'classify'  => 0,
            'icon'      => $request->get('icon')
        ]);

        return is_object($instance) ? ['code'=>200,'msg'=>'创建成功','data'=>[
            'type_id'=>$instance->getTypeId()
        ]] : ['code'=>400,'msg'=>'创建失败'];
    }


    public function firstCreate(Request $request)
    {
        $instance = UniversityTypeServiceImpl::create([
            'type_name' => $request->get('type_name'),
            'type_pid'  => '10000',
            'classify'  => 0,
            'icon'      => $request->get('icon'),
            'subtitle'  => $request->get('subtitle'),
            'path'      => '0,10000'
        ]);

        return is_object($instance) ? ['code'=>200,'msg'=>'创建成功','data'=>[
            'type_id'=>$instance->getTypeId()
        ]] : ['code'=>400,'msg'=>'创建失败'];
    }


    /**
     * 修改分类
     * @param Request $request
     * @return array
     */
    public function modify(Request $request)
    {
        $instance = UniversityTypeServiceImpl::getInstance($request->get('type_id'));

        if(!$instance){
            return ['code'=>400,'msg'=>'分类不存在'];
        }

        $instance->setTypeName($request->get('type_name'));

        $instance->setIcon($request->get('icon', ''));
        $instance->setSubtitle($request->get('subtitle', ''));

        $instance->update();

        return ['code'=>200,'msg'=>'修改成功'];
    }

    /**
     * 分类删除
     * @param Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        $instance = UniversityTypeServiceImpl::getInstance($request->get('type_id'));

        if(!$instance){
            return ['code'=>400,'msg'=>'分类不存在'];
        }

        $instance->delete();

        return ['code'=>200,'msg'=>'分类删除成功'];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getLevelList(Request $request)
    {
        $classify = $request->route('classify');

        $instances = UniversityTypeServiceImpl::getTypeLevelListByClassify($classify);

        $data = array_map(function($instance){
            $level = substr_count($instance->getPath(),',')-1;
            return [
                'id'=>$instance->getTypeId(),
                'label'=>$instance->getTypeName(),
                'type_pid'=>$instance->getTypePid(),
                'path'=>$instance->getPath(),
                'label_level'=>$level >=0 ? str_repeat("　",$level).'|-'.$instance->getTypeName() : $instance->getTypeName(),
            ];
        },$instances);

        return ['code'=>200,'msg'=>'查询成功','data'=>$data];
    }

    /**
     * 分类管理
     * @param Request $request
     * @return array
     */
    public function getList(Request $request)
    {
        $typePid = $request->route('typePid');

        $typePid = $typePid ? $typePid : 10000;

        $classify = $request->get('classify');

        $instances = UniversityTypeServiceImpl::getTypeListByPid($typePid,$classify);

        $data = array_map(function($instance){
            return [
                'id'=>$instance->getTypeId(),
                'label'=>$instance->getTypeName(),
                'type_pid'=>$instance->getTypePid(),
                'classify'=>$instance->getClassify(),
                'article_num'=>$instance->getArticlesNum(),
                'icon'=>$instance->getIcon(),
                'display_order'=>$instance->getDisplayOrder(),
                'children'=>array_map(function($subInstance){
                    return [
                        'id'=>$subInstance->getTypeId(),
                        'label'=>$subInstance->getTypeName(),
                        'type_pid'=>$subInstance->getTypePid(),
                        'classify'=>$subInstance->getClassify(),
                        'article_num'=>$subInstance->getArticlesNum(),
                        'icon'=>$subInstance->getIcon(),
                        'display_order'=>$subInstance->getDisplayOrder(),
                        'index_show'=>$subInstance->getIndexShow()
                    ];
                },$instance->getSub())
            ];
        },$instances);

        return ['code'=>200,'msg'=>'查询成功','data'=>$data];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function detail(Request $request)
    {
        $instance = UniversityTypeServiceImpl::getInstance($request->route('typeId'));

        return $instance ? ['code'=>200, 'data'=>['label'=>$instance->getTypeName(), 'subtitle'=>$instance->getSubtitle(),'icon'=>$instance->getIcon(),'type_pid'=>$instance->getTypePid()]] :
            ['code'=>400,'msg'=>'类型不存在'];
    }

    /**
     * 更新排序
     * @param Request $request
     * @return array
     */
    public function updateDisplayOrder(Request $request)
    {
        $instance = UniversityTypeServiceImpl::getInstance($request->get('type_id'));

        if(!$instance){
            ['code'=>400,'msg'=>'类型不存在'];
        }

        $instance->setDisplayOrder($request->get('display_order'));
        $instance->update();

        return ['code'=>200,'msg'=>'操作成功'];
    }

    /**
     * 首页显示
     * @param Request $request
     * @return array
     */
    public function updateIndexShow(Request $request)
    {
        $instance = UniversityTypeServiceImpl::getInstance($request->get('type_id'));

        if(!$instance){
            ['code'=>400,'msg'=>'类型不存在'];
        }

        $instance->setIndexShow($request->get('index_show'));
        $instance->update();

        return ['code'=>200,'msg'=>'操作成功'];
    }

}
