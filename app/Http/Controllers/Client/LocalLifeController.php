<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/21
 * Time: 下午05:43
 * Describe: 本地生活
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Service\Client\LocalLifeService;
use Illuminate\Http\Request;

class LocalLifeController extends Controller
{
    protected $localLifeService;

    public function __construct(LocalLifeService $localLifeService)
    {
        $this->localLifeService = $localLifeService;
    }

    /**
     * 本地生活列表
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $platform_identity = $request->input('platformIdentity', '');
        if (empty($platform_identity)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->localLifeService->getList($platform_identity);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    /**
     * 本地生活详情
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOne(Request $request)
    {
        $id = $request->input('id', '');
        $platform_identity = $request->input('platformIdentity', '');
        if (empty($id) || empty($platform_identity)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->localLifeService->getOne($id, $platform_identity);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    public function share(Request $request)
    {
        $mid = $request->input('mid', '');
        $platform_identity = $request->input('platformIdentity', '');
        if (empty($mid) || empty($platform_identity)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->localLifeService->share($mid, $platform_identity);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }
}










