<?php
/**
 * Created by PhpStorm.
 * User:cxy    小月客户端首页数据
 * Date: 2020/12/26
 * Time: 16:07
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Service\Client\IndexService;
use Illuminate\Http\Request;

class IndexController extends Controller
{   


    //小悦客户端首页
    public function getIndexInfo(Request $request)
    {   
    	$param = $request->all();

        $data =  IndexService::getIndexInfo($param);

        return [
        	'code'=>200,
        	'msg'=>'success',
        	'data'=>$data
        ];
    }


    //小悦客户端当前渠道群信息
    public function getMemberChannelInfo(Request $request)
    {	
    	$param = $request->all();

    	$data = IndexService::getMemberChannelInfo($param);

        return [
        	'code'=>200,
        	'msg'=>'success',
        	'data'=>$data
        ];

    }

    //小悦客户端当前渠道订单图表信息
    public function getMyOrderMapInfo(Request $request)
    {	
    	$param = $request->all();

    	$data = IndexService::getMyOrderMapInfo();
    	
        return [
        	'code'=>200,
        	'msg'=>'success',
        	'data'=>$data
        ];

    }

    //小悦客户端当前渠道月收益图表信息
    public function getMyMonthlyIncome(Request $request)
    {	
    	$param = $request->all();
    	
    	$data = IndexService::getMyMonthlyIncome($param);
    	
        return [
        	'code'=>200,
        	'msg'=>'success',
        	'data'=>$data
        ];

    }


    //是否拥有专属机器人
    public function isExclusiveRobot(Request $request)
    {	
    	$param = $request->all();
    	
    	$data = IndexService::isExclusiveRobot($param);
    	
        return [
        	'code'=>200,
        	'msg'=>'success',
        	'data'=>$data
        ];

    }

    //根据群主自定义标签自动匹配商品
    public function getRecommendGoodsByGroupTags(){

    	return IndexService::getRecommendGoodsByGroupTags();
    }


}










