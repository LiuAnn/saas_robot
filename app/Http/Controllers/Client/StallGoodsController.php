<?php

namespace App\Http\Controllers\Client;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
use App\Service\Client\StallGoodsService;


class StallGoodsController extends Controller {

    private $params;
    protected $HomeService;

    public function __construct(Request $request,HomeService $HomeService)
    {
        $this->params = $request->all();

    }


    /**
     * 获取单个商品的推送状态
     */
    public function getPushStatusByGoodsId(){
        if(empty($this->params['goods_id']) || empty($this->params['mid'])){
            return self::responseJson('400','缺少参数',['data'=>[]]);
        }
        $result =StallGoodsService::getPushStatusByGoodsId($this->params);
        return self::responseJson('200','成功',['data'=>$result]);

    }

    /**
     * 获取助理推送列表
     * @access
     * @author lele
     * @version 1.0
     */
    public function getAssistantList()
    {
        if(!isset($this->params['mid']) || empty($this->params['mid']) || !isset($this->params['status'])){
            return responseJson(400 , '缺少参数');
        }
        $result = StallGoodsService::getAssistantList($this->params);
        return $result;
    }

    /**
     * 判断是否有专属机器人
     * @access
     * @author liuwenhao
     * @version 1.0
     * 返回值 state 0 为没有   1 为有
     */
    public function isExclusiveRobot(){

        if(!isset($this->params['mid']) || empty($this->params['mid'])){
            return responseJson(400 , 'mid必须存在且不能为空');
        }
        $result = StallGoodsService::isExclusiveRobot($this->params);  //  请求结果

        return $result;
    }


    /**
     * 关闭机器人推送
     * @access
     * @author liuwenhao
     * @version 1.0
     */
    public function closeAssistantPush(){

        if(!isset($this->params['mid']) || empty($this->params['mid'])){
            return responseJson(400 , 'mid必须存在且不能为空');
        }

        $result = StallGoodsService::closeAssistantPush($this->params);

        return $result;
    }


    /**
     * 开启机器人推送
     * @access
     * @author liuwenhao
     * @version 1.0
     */
    public function openAssistantPush(){

        if(!isset($this->params['mid']) || empty($this->params['mid'])){
            return responseJson(400 , 'mid必须存在且不能为空');
        }
        $result = StallGoodsService::openAssistantPush($this->params);  //  请求结果

        return $result;
    }

    /**
     * 向机器人推送商品
     * @access
     * @author liuwenhao
     * @version 1.0
     */
    public function addAssistantPush(){

        if(!isset($this->params['mid']) || empty($this->params['mid'])){
            return responseJson(400 , 'mid必须存在且不能为空');
        }

        if(!isset($this->params['goods_type']) || empty($this->params['goods_type'])){
            return responseJson(400 , 'goods_type必须存在且不能为空');
        }

        if(!isset($this->params['goodsId']) || empty($this->params['goodsId'])){
            return responseJson(400 , 'goodsId必须存在且不能为空');
        }

        $result = StallGoodsService::addAssistantPush($this->params);

        return $result;
    }


    /**
     * 删除机器人推送商品
     * @access
     * @author liuwenhao
     * @version 1.0
     */
    public function delAssistantPush(){

        if(!isset($this->params['mid']) || empty($this->params['mid'])){
            return responseJson(400 , 'mid必须存在且不能为空');
        }

        if(!isset($this->params['id']) || empty($this->params['id'])){
            return responseJson(400 , 'id必须存在且不能为空');
        }

        if(!isset($this->params['goodsId']) || empty($this->params['goodsId'])){
            return responseJson(400 , 'goodsId必须存在且不能为空');
        }

        $result = StallGoodsService::delAssistantPush($this->params);

        return $result;
    }


    /**
     * 推送商品回调
     * @access
     * @param $params
     * @return array
     * @author liuwenhao
     * @version 1.0
     */
    public function callbackAssistantPush(){

        if(!isset($this->params['source']) || empty($this->params['source'])){
            return responseJson(400 , '站站你不传source，我咋知道是回调我的～');
        }

        if($this->params['source'] != config("stall.robot_source")){
            return responseJson(400 , '站站～你回调错地方了，这不是悦地摊的推送～～～');
        }

        if(!isset($this->params['flag']) || empty($this->params['flag'])){
            return responseJson(400 , '站站～flag不传，我咋改状态！！！');
        }

        $result = StallGoodsService::callbackAssistantPush($this->params);

        return $result;

    }

    /**
     * 判断是否加入助理
     * @access
     * @param $params
     * @return array
     * @author liuwenhao
     * @version 1.0
     */
    public function isAddAssistant(){

        $result = StallGoodsService::isAddAssistant($this->params);

        return $result;
    }

    /**
     * 获取好券商品 and 好物推荐
     */
    public function CouponGoodsList(){
       $type = isset($this->params['type']) ? $this->params['type'] : 0;
        $param['keyword']   = "家居用品";
        if($type == 1){
            $param['keyword']   = "食品生鲜";
        }
        $param['channelId'] = 100;
        $param['isCoupon']  = 1;
        $param['pageSize']  = 10;
        $param['page']      = $this->params['page'];
        $param['sorttype']  = 1;
        $param['sort']      = 0;
        $goodsList =SearchService::querySearchNewEndSearchData($param,0);
        foreach ($goodsList['data']['doc'] as $k=>$v){
            if(isset($params['mid']) && !empty($params['mid'])){ //如果登陆了，判断是否加入助理
                if(!isset($v['skuId'])){
                    $v['goods_sku_id'] = 0;
                }
                $key = "goods_id=" . $v['productId'] . "&mid=" . $params['mid'] . "&sku_id=" . $v['skuId'];
                $assistant = Redis::get($key);
                if(!isset($assistant) || empty($assistant)){
                    $assistant = 0;
                }
                $goodsList['data']['doc'][$k]['isAddAssistant'] = $assistant; // 0 没有加助理 1 添加了助理
            }else{
                $goodsList['data']['doc'][$k]['isAddAssistant'] = 0; // 0 没有加助理 1 添加了助理
            }
        }
        return ['code' => 200,'msg' => 'success','data' => $goodsList['data']['doc']];
    }

    /**
     * 唯品会
     */
    public function channelGoodsList(){
        $param['mid'] = empty($this->params['mid']) ? 0 : $this->params['mid'];
        $param['channelId'] = empty($this->params['channelId']) ? 101 : $this->params['channelId'];
        $param['keyword']   = "衣服";
//        $param['keyword']   =$this->params['keyword'];
        switch ($param['channelId']){
            case 98:
                $param['keyword']   = "日常家居";
                break;
            case 6:
                $param['keyword']   = "家居生活";
                break;
            case 11:
                $param['keyword']   = "家居生活";
                break;
        }
        $param['isCoupon']  = 0;
        $param['pageSize']  = 10;
        $param['page']      = $this->params['page'];
        $param['sorttype']  = 1;
        $param['sort']      = 0;
        $goodsList =SearchService::querySearchNewEndSearchData($param,0);
        foreach ($goodsList['data']['doc'] as $k=>$v){
            if(isset($params['mid']) && !empty($params['mid'])){ //如果登陆了，判断是否加入助理
                if(!isset($v['skuId'])){
                    $v['goods_sku_id'] = 0;
                }
                $key = "goods_id=" . $v['productId'] . "&mid=" . $params['mid'] . "&sku_id=" . $v['skuId'];
                $assistant = Redis::get($key);
                if(!isset($assistant) || empty($assistant)){
                    $assistant = 0;
                }
                $goodsList['data']['doc'][$k]['isAddAssistant'] = $assistant; // 0 没有加助理 1 添加了助理
            }else{
                $goodsList['data']['doc'][$k]['isAddAssistant'] = 0; // 0 没有加助理 1 添加了助理
            }

            if(isset($v['buyPrice'])){
                $buyPrice = round($v['buyPrice'] / 100 , 2);
                if($v['vipPrice'] - $buyPrice > 0){
                    $goodsList['data']['doc'][$k]['femalesPrice'] = round(($v['vipPrice'] - $buyPrice) / 3.99 , 2); //佣金
                }
            }

        }
        return ['code' => 200,'msg' => 'success','data' => $goodsList['data']['doc']];
    }
    /**
     * 831顶部分类二级页
     */
    public function secondaryPage(){
//        $id = isset($this->params['id']) ? $this->params['id'] : 0;
        return StallGoodsService::secondaryPage($this->params);
    }
    /**
     * 社群专区
     */
    public function CommunityGoods(){
        if(!isset($this->params['platform_identity']) || !isset($this->params['goods_status'])){
            return responseJson(400 , '缺少参数');
        }
        $result = StallGoodsService::CommunityGoods($this->params);
        return $result;

    }
}