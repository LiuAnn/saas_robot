<?php

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Service\User\LoginService;
use App\Service\User\PhoneMessageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller {

    /**
     * UseIndexBuyController constructor.
     */
    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [ 'updatePassword','loginOut']]);
    }
    /**
     * @time     2020/4/14 12:43
     * @method   method   [登录]
     * @param Request $request
     * @return
     */
    public function login(Request $request){

        $data = $request->all();
        $result = LoginService::login($data);

        return $result;
    }

    /**
     * @time     2020/8/17 12:43
     * @method   method   [联合登录]
     * @param Request $request
     * @return
     */
    public function loginForMiddle(Request $request){

        $data = $request->all();
        $result = LoginService::loginForMiddle($data);

        return $result;
    }

    /**
     * @param Request $request
     * @return false|string
     * @author   liuwenhao
     * @time     2020/4/14 12:44
     * @method   method   [登出]
     */
    public function loginOut(Request $request){
        $member =  $request->member;
        return LoginService::loginOut($member);
    }

    /**
     * @param Request $request
     * @return false|string
     * @throws \Exception
     * @author   liuwenhao
     * @time     2020/4/14 12:47
     * @method   method   [注册]
     */
    public function register(Request $request){

        $data = $request->all();

        $result = LoginService::register($data);

        return $result;
    }


    /**
     * @param Request $request
     * @return array
     * @author   liuwenhao
     * @time     2020/4/15 12:35
     * @method   method   [发送短信验证码]
     */
    public function sendMessage(Request $request){

        $param  = $request->all();

         //判断滑块token
        if(!isset($param['imgToken']) || empty($param['imgToken'])){
            return ['code' => 400 , 'msg' => "请滑动滑块"];
        }

       //滑块验证
        $imgRes = LoginService::changeImageToken($param['imgToken']);

        if($imgRes['code'] != 200){
            return ['code' => 400 , "msg" => '滑块已过期请刷新重试'];
        }

        $mobile = empty($param["mobile"]) ? 0 : trim($param["mobile"]);

        Log::info("send params: ", $param);

        if (empty($mobile)) {
            return ['code' => 400, 'msg' => '手机号不能为空!'];
        }

        $areaCode   = array_key_exists("areaCode", $param) ? $param["areaCode"] : 86;
        $returnData = PhoneMessageService::isBeyondLimit($mobile, $areaCode);

        $returnData = [];

        if (empty($returnData)) {

            return PhoneMessageService::sendMessage($mobile, $areaCode);
        } else {
            return $returnData;
        }
    }


    /**
     * @param Request $request
     * @return
     * @author   liuwenhao
     * @time     2020/4/15 0:13
     * @method   method   [验证短信]
     */
    public function registerVerify(Request $request)
    {
        $param  = $request->All();
        $mobile = trim($param["mobile"]);
        $code   = trim($param["code"]);
        return LoginService::registerVerifyCode($mobile, $code);
    }

    /**
     * 更新密码
     * @param Request $request
     * @return mixed
     */
    public function updatePassword(Request $request)
    {
        $param  = $request->All();
        $member =  $request->member;
        return LoginService::updatePassword($param, $member);
    }

}
