<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/23
 * Time: 上午05:43
 * Describe: 用户相关
 */

namespace App\Http\Controllers\User;

use Exception;
use App\Http\Controllers\Controller;
use App\Service\User\UsersService;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $userService;

    public function __construct(UsersService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * 用户渠道
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChannel(Request $request)
    {
        $token = $request->input('token', '');
        $mobile = $request->input('mobile', '');
        if (empty($token)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->userService->getChannel($token);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    /**
     * 获取用户信息
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOne(Request $request)
    {
        $token = $request->input('token', '');
        $mobile = $request->input('mobile', '');
        if (empty($token)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->userService->getOne($token);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    /**
     * 获取用户群信息
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGroupList(Request $request)
    {
        $token = $request->input('token', '');
        if (empty($token)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->userService->getGroupList($token);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    public function setGroupTags(Request $request)
    {
        $token = $request->input('token', '');
        $group_id = $request->input('group_id', '');
        $add = $request->input('add', '');
        $del = $request->input('del', '');
        if (empty($group_id) || empty($token)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = $this->userService->setGroupTags($group_id, $add, $del, $token);
        return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
    }

    public function login(Request $request)
    {
        try {
            $code = $request->input('code', '');
            $encryptedData = $request->input('encryptedData', '');
            $replaces = array(' ' => '+');
            $encryptedData = strtr($encryptedData, $replaces);
            $iv = $request->input('iv', '');
            $data = $this->userService->login($code, $encryptedData, $iv);
            return response()->json(array('code' => 200, 'message' => 'success', 'data' => $data))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }
}










