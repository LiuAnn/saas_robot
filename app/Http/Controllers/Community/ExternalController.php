<?php


namespace App\Http\Controllers\Community;

use App\Http\Controllers\Controller;
use App\Service\Admin\AdminIndexService;
use App\Service\Admin\UserService;
use App\Service\Community\GroupService;
use Illuminate\Http\Request;
use  App\Service\Community\ExternalService;

class ExternalController extends Controller
{

    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [
            'index', 'dataStatistics', 'adminIndexGraph', 'getGroupMemberList', 'groupExclusiveAdminStatistics', 'dataExclusiveStatistics',
            'adminIndexTotal', 'getUserOperationList', 'getUserListByName', 'groupAdminStatistics',
            'seeNowBingData', 'getMemInfoByMobile', 'bindGroupMid', 'getUserAdminList'
        ]]);
    }


    /*
    * 数据中台社群接口
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getGroupNumCount(Request $request)
    {

        $param = $request->all();
        return ExternalService::getGroupNumCount($param);
    }


    /*
    * 社群人数量
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupMemberNum(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupMemberNum($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群交易
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function orderGMVCount(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::orderGMVCount($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 订单数量列表
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function orderCountList(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::orderCountList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群数量增长统计图
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getGroupNumCountGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::getGroupNumCountGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群数量增长统计图
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupProportionGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupProportionGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群人数占比统计
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupMemberProportionGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupMemberProportionGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群人数性别占比统计
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function sexProportionGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::sexProportionGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群增长统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupMemberNumGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupMemberNumGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群增长统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function orderGMVCountGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::orderGMVCountGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群增长统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupOrderGMVDetailGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupOrderGMVDetailGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群增长统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupAdminOrderGMVGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupAdminOrderGMVGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
    * 订单数量走势图
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function orderNumCountGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::orderNumCountGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群排行详情--订单走势图
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupOrderNumDetailGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupOrderNumDetailGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群排行详情--订单走势图
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupAdminOrderDetailGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupAdminOrderDetailGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
    * 社群订单渠道构成统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function orderSourceProportionGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::orderSourceProportionGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupTopGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupTopGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupAdminTopGraph(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::groupAdminTopGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupStatisticsList(Request $request)
    {

        $param = $request->all();
        $param['sourceZT'] = 1;
        $data = ExternalService::getDataStatistics($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 群主排行详情--群主群列表
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupAdminGroupList(Request $request)
    {

        $param = $request->all();
        if (!isset($param['groupAdminMid']) || empty($param['groupAdminMid'])) {
            return response()->fail(400, '缺少群主ID');
        }
        $data = ExternalService::getDataStatistics($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getSellProductList(Request $request)
    {

        $param = $request->all();
        $param['sourceZT'] = 1;
        $data = ExternalService::getSellProductList($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getGroupOrderList(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::getGroupOrderList($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getGroupAdminGroupOrderList(Request $request)
    {

        $param = $request->all();
        $data = ExternalService::getGroupAdminGroupOrderList($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 社群排行--群统计详情
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupStatisticsDetail(Request $request)
    {

        $param = $request->all();
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return response()->fail(400, '缺少群ID');
        }
        $data = ExternalService::groupStatisticsDetail($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 渠道群主排行统计图数据
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupAdminStatisticsList(Request $request)
    {

        $param = $request->all();
        $param['sourceZT'] = 1;
        $data = ExternalService::groupAdminStatistics($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * 群主排行详情--群主详情信息
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function groupAdminStatisticsDetail(Request $request)
    {

        $param = $request->all();
        $param['sourceZT'] = 1;
        $data = ExternalService::groupAdminStatisticsDetail($param);;
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
    * 管理员统计
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getRobotSendGoodList(Request $request)
    {

        $param = $request->all();

        //显示上架状态的商品
        $param['goods_status'] = 1;
        return ExternalService::getRobotSendGoodList($param);
    }

    /*
    * 悦地摊展示C店的商品列表
    * @param Request $request
    * @throws ServicesException
    * @return array
    */
    public function getFaddishGoodList(Request $request)
    {
        $param = $request->all();
        $test = $param['test'] ?? 0;
        $num = $param['num'] ?? 1;
        if ($test) {
            $returnData = [];
            $goods = [
                'product_id' => 2095870,
                'sku_id' => 13997503,
                'product_name' => '多乐（duole）榨汁机多功能料理机家用水果打炸果汁机婴儿辅食奶昔搅拌机电动研磨机DL-BL18PP 银黑色',
                'product_vipPrice' => 198,
                'small_goods_img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-27/13/yuelvhuiqfmIyYmVbi1606454228.jpg',
                'end_time' => '2020/12/16 00:00:00',
            ];
            for ($i = 1; $i <= $num; $i++) {
                $returnData[] = $goods;
            }
        } else {
            $returnData = [
                [
                    'product_id' => 2095870,
                    'sku_id' => 13997503,
                    'product_name' => '多乐（duole）榨汁机多功能料理机家用水果打炸果汁机婴儿辅食奶昔搅拌机电动研磨机DL-BL18PP 银黑色',
                    'product_vipPrice' => 198,
                    'small_goods_img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-27/13/yuelvhuiqfmIyYmVbi1606454228.jpg',
                    'end_time' => '2020/12/16 00:00:00',
                ]
            ];
            //结束时间2020/12/13
            if (time() > 1608048000) {
                $returnData = [];
            }
        }
        return response()->success($returnData);
    }

    /*
    * 管理员统计
    * @param Request $request
    * @throws ServicesException
    * @retuen array
    */
    public function getRoomIdsByGroupAdmin(Request $request)
    {

        $param = $request->all();

        return ExternalService::getRoomIdsByGroupAdmin($param);
    }

    /*
    * 管理员统计
    * @param Request $request
    * @throws ServicesException
    * @retuen array
    */
    public function getExclusiveRobotList(Request $request)
    {

        $param = $request->all();

        return ExternalService::getExclusiveRobotList($param);
    }


    /*
    * 机器人搜索
    * @param Request $request
    * @throws ServicesException
    * @retuen array
    */
    public function getRobotListForSearch(Request $request)
    {

        $param = $request->all();

        return ExternalService::getRobotListForSearch($param);
    }


    public function getRobotNumData(Request $request)
    {

        $param = $request->all();

        return ExternalService::getRobotNumData($param);
    }


    public function getGroupOrderData(Request $request)
    {

        $param = $request->all();

        return ExternalService::getGroupOrderData($param);

    }

    public function getExclusiveGroupList(Request $request)
    {

        $param = $request->all();

        return ExternalService::getExclusiveGroupList($param);

    }

    public function getCommunityStatistics(Request $request)
    {
        $param = $request->all();

        $param['limitNum'] = '360';
        return ExternalService::getGroupNumCount($param);
    }

    public function getGrowTrend(Request $request)
    {
        $param = $request->all();
        $param['limitNum'] = '360';
        $data = ExternalService::getGroupNumCountGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 通过mid获取是否是站长
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStationmasterByMid(Request $request)
    {
        $mid = $request->input('mid', '');
        if (empty($mid)) {
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        $data = GroupService::getStationmasterByMid($mid);
        if (empty($data)){
            return response()->json(array('code' => 400, 'message' => '参数错误', 'data' => []))->setCallback();
        }
        return response()->json(array('code' => 200, 'message' => '', 'data' => [$data]))->setCallback();
    }


    

}