<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/8
 * Time: 17:44
 */

namespace App\Http\Controllers\Community;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Community\StrategyCategoryService;

class StrategyCategoryController extends Controller
{
    public function addStrategyCategory(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;

        return StrategyCategoryService::addStrategyCategory($memberInfo, $params);
    }

    public function updateStrategyCategory(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return StrategyCategoryService::updateStrategyCategory($memberInfo, $params);
    }

    public function getStrategyCategoryList(Request $request)
    {
        $params = $request->all();
        if (isset($params['source']) && !empty($params['source'])) {
            $params['platform_identity'] = $params['source'];
        } elseif ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        return StrategyCategoryService::getStrategyCategoryList($params);
    }

    /**
     * 攻略分类删除
     * @param Request $request
     * @return array
     */
    public function deleteStrategyCategory(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return StrategyCategoryService::deleteStrategyCategory($memberInfo, $params);
    }



}