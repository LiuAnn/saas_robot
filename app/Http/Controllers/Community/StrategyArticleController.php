<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/8
 * Time: 19:48
 */

namespace App\Http\Controllers\Community;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Community\StrategyArticleService;

class StrategyArticleController extends Controller
{
    /**
     * 新增攻略文章
     */
    public function addStrategyArticle(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        return StrategyArticleService::addStrategyArticle($memberInfo, $params);
    }

    /**
     * 攻略文章移动
     */
    public function moveStrategyArticle(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->fail(200001, '用户无权限');
        }
        $memberInfo = $request->member;
        return StrategyArticleService::moveStrategyArticle($memberInfo, $params, 'move');
    }

    /**
     * 攻略文章移除
     */
    public function removeStrategyArticle(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->fail(200001, '用户无权限');
        }
        $memberInfo = $request->member;
        return StrategyArticleService::moveStrategyArticle($memberInfo, $params);
    }

    /**
     * 攻略文章列表
     */
    public function getStrategyArticleList(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        return StrategyArticleService::getStrategyArticleList($params);
    }

    /**
     * 攻略文章详情
     */
    public function getStrategyArticle(Request $request)
    {
        $params = $request->all();
        return StrategyArticleService::getStrategyArticle($params);
    }

    /**
     * 攻略文章删除
     */
    public function deleteStrategyArticle(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return StrategyArticleService::deleteStrategyArticle($memberInfo, $params);
    }

    /**
     * 攻略文章编辑
     */
    public function updateStrategyArticle(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return StrategyArticleService::updateStrategyArticle($memberInfo, $params);
    }
}