<?php
/**
 * Created by PhpStorm.
 * User:spz    社群首页数据
 * Date: 2020/3/25
 * Time: 18:24
 */
namespace App\Http\Controllers\Community;

use App\Http\Controllers\Controller;
use App\Service\Community\YiYunRobotService;
use Illuminate\Http\Request;
use App\Service\Community\IndexService;
use App\Service\Community\UploadService;

class IndexController extends Controller
{

    protected $params;
    public function __construct(Request $request)
    {
        $this->params = $request->all();
    }

    /**
     * 社群用户的  测试   文件上传
     */
    public  function  uploadDataTest()
    {
        $data =$this->params;
        $file = $_FILES;
        if (empty($file)||empty($data)) {
            exit(0);
        }
        UploadService::uploadData($data,$file);
    }

    /**
     * 社群用户行为  测试    的操作
     */
     public function  userBehaviorTest(){
         $body =$this->params;
         if (empty($body)) {
             exit(0);
         }
         IndexService::pathInlog($body);
     }
    /**
     * 社群用户的  测试   文件上传
     */
    public function  uploadDataProduct()
    {
        $data =$this->params;
        $file = $_FILES;
        if (empty($file)||empty($data)) {
            exit(0);
        }
        UploadService::uploadData($data,$file);
    }
    /**
     * 社群用户行为  测试    的操作
     */
    public function  userBehaviorProduct()
    {
        $body =$this->params;
        if (empty($body)) {
            exit(0);
        }
        IndexService::pathInlog($body);
    }

    /**
     * 社群用户行为  测试    的操作
     */
    public function  yiYunRobotTest()
    {
        $body =$this->params;
        if (empty($body)) {
            exit(0);
        }
        YiYunRobotService::sendMegByRobot($body);
    }

    //删除重复的好友
    public   function  deleteUselessData(){
        IndexService:: deleteUselessData();
    }

    //更新共同的群数量
    public   function  updateCommonGroupNum(){
        $body =$this->params;

        IndexService:: updateCommonGroupNum($body);
    }

    public  function  updateWhite(){

        IndexService:: updateWhiteBindMidNum();
    }

    public  function  updateRedisData(){
        $body =$this->params;
        IndexService:: updateRedisData($body);
    }




}










