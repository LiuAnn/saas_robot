<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2021/1/4
 * Time: 11:45
 */

namespace App\Http\Controllers\Community;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Community\GroupTaskService;
class GroupTaskController   extends Controller
{


    public function __construct(Request $request)
    {
        $this->middleware('user.check.login', ['only' => ['getList','addTask','editTask','delTask','taskExamine','getGroupTaskListForTaskId']]);

    }

    /**
     * 获取任务列表
     * @param Request $request
     * @return mixed
     */
    public function getList(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::getTaskList($param,$member);
    }

    /**
     * 添加任务
     * @param Request $request
     * @return mixed
     */
    public function addTask(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::addTask($param,$member);
    }

    /**
     * 添加任务
     * @param Request $request
     * @return mixed
     */
    public function editTask(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::editTask($param,$member);
    }

    /**
     * 删除任务
     * @param Request $request
     * @return mixed
     */
    public function delTask(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::delTask($param,$member);
    }


    /**
     * 获取任务详情
     * @param Request $request
     * @return mixed
     */
    public function getTaskInfo(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        return GroupTaskService::getTaskInfo($param);
    }

    /**
     * 获取任务列表对外输出
     * @param Request $request
     * @return mixed
     */
    public function getTaskInfoForPl(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        return GroupTaskService::getTaskInfoForPl($param);
    }

    /**
     * 获取任务列表对外输出
     * @param Request $request
     * @return mixed
     */
    public function getTaskListForMid(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        return GroupTaskService::getTaskListForMid($param);
    }

    /**
     * 任务审核接口
     * @param Request $request
     * @return mixed
     */
    public function taskExamine(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::taskExamine($param,$member);
    }

    /**
     * 团长任务完成度列表接口
     * @param Request $request
     * @return mixed
     */
    public function getGroupTaskListForTaskId(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::getGroupTaskListForTaskId($param,$member);
    }

    /**
     * 团长任务同步团长进度
     * @param Request $request
     * @return mixed
     */
    public function addGroupTaskRelation(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        return GroupTaskService::addGroupTaskRelation($param,$member);
    }


    public function test(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        return GroupTaskService::test($param);
    }

    /**
     * 定时同步任务状态
     * @param Request $request
     * @return mixed
     */
    public function updateGroupTaskStatus(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        return GroupTaskService::updateGroupTaskStatus($param);
    }

    /**
     * 定时同步奖励金发送状态
     * @param Request $request
     * @return mixed
     */
    public function updateMoneySendStatus(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        return GroupTaskService::updateMoneySendStatus($param);
    }

    

}