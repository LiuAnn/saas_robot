<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/31
 * Time: 21:19
 */

namespace App\Http\Controllers\Community;

use App\Http\Controllers\Controller;
use App\Service\Community\GroupService;
use Illuminate\Http\Request;
use App\Service\Community\GroupActivationService;
use Illuminate\Support\Facades\Validator;

class GroupController extends Controller
{
    protected $params;
    public function __construct(Request $request)
    {
        $this->params = $request->all();
    }

    /**
     * 用户端后台自动创建群
     * @return mixed
     */
    public function autoCreateGroup(Request $request)
    {
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        $data = GroupService::autoCreateGroup($memberInfo, $this->params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 获取下级用户列表
     * @return mixed
     */
    public function getLowerMemberList(Request $request)
    {
        $data = GroupService::getLowerMemberList($this->params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 机器人列表
     * @return mixed
     */
    public function getRobotList(Request $request)
    {
        $memberInfo = $request->member;
        $data = GroupService::getRobotList($memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 邀请好友进群
     * @return mixed
     */
    public function friendsIntoGroup(Request $request)
    {
        $memberInfo = $request->member;
        $data = GroupService::friendsIntoGroup($memberInfo, $this->params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 待创建群的好友邀请
     * 邀请好友进群
     * @return mixed
     */
    public function planFriendsIntoGroup(Request $request)
    {
        $memberInfo = $request->member;
        $data = GroupService::planFriendsIntoGroup($memberInfo, $this->params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 待创建的群列表
     * @return mixed
     */
    public function toCreateGroupList(Request $request)
    {
        $memberInfo = $request->member;
        $this->params['platform_identity'] = $memberInfo['platform_identity'];
        $data = GroupService::toCreateGroupList($this->params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 创建群
     * @return mixed
     */
    public function createGroup()
    {
        $res = GroupService::createGroup($this->params);

        return $res;
    }


    /**
     * 根据订单查询用户群列表
     * @return mixed
     */
    public function groupList()
    {
        $data = GroupService::getAllGroupListByMidAndOrderSn($this->params);

        return ['code' => 200, 'msg' => '成功', 'data' => $data];
    }


    /**
     * 获取用户群详情
     * @return mixed
     */
    public function groupDetail()
    {
        $mid = $this->params['groupId'];

        return GroupService::getGroupDetailByGroupId($mid);
    }

    /**
     * 群提交审核
     * @return mixed
     */
    public function applyGroup()
    {
        $params = $this->params;

        // 参数验证
        $validator = Validator::make($params, [
            'groupId' => 'bail|required',
            'images' => 'bail|required',
        ], [
            'required'  => '参数缺失',
        ]);
        if ($validator->fails()) {
            $error_msg = $validator->messages()->first();
            return self::responseJson('400', $error_msg);
        }

        //更新数据
        $update = GroupService::updateDataById($params['groupId'], ['images' => $params['images'], 'examine_status' => 1]);

        if ($update) {
            return self::responseJson(200, '成功');
        }

        return self::responseJson(400, '提交数据失败，请重新申请');
    }


    /**
     * 群标签列表
     * @return mixed
     */
    public function getGroupTagList(Request $request)
    {

        $memberInfo = $request->member; 
              
        return  GroupService::getGroupTagList($this->params,$memberInfo);
    }

    /**
     * 添加群标签
     * @return mixed
     */
    public function addGroupTag(Request $request)
    {

        $memberInfo = $request->member; 
              
        return  GroupService::addGroupTag($this->params,$memberInfo);
    }

    /**
     * 修改群标签
     * @return mixed
     */
    public function updateGroupTag(Request $request)
    {

        $memberInfo = $request->member; 

        return  GroupService::updateGroupTag($this->params,$memberInfo);
    }

    /**
     * 删除群标签
     * @return mixed
     */
    public function delGroupTag(Request $request)
    {

        $memberInfo = $request->member; 

        return  GroupService::delGroupTag($this->params,$memberInfo);
    }

    /**
     * 绑定群标签
     * @return mixed
     */
    public function bindGroupTag(Request $request)
    {

        $memberInfo = $request->member; 

        return  GroupService::bindGroupTag($this->params,$memberInfo);
    }

    /**
     * 优惠券列表
     * @return mixed
     */
    public function getCouponList(Request $request)
    {
        $memberInfo = $request->member; 

        return  GroupService::getCouponList($this->params,$memberInfo);

    }
}