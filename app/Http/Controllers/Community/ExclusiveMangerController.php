<?php

namespace App\Http\Controllers\Community;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Community\ExclusiveMangerService;

class ExclusiveMangerController extends Controller
{

    protected $params;
    public function __construct(Request $request)
    {
        $this->params = $request->all();
    }
    /**
     * 判断是否为专属机器人
     */
    public  function  isDedicatedRobotCon(){

        $this->params['mid'] =  empty($this->params['mid']) ?0 : $this->params['mid'] ;
        $this->params['source'] =  empty($this->params['source']) ?0 : $this->params['source'] ;

        $returnData =  ExclusiveMangerService::isDedicatedRobot($this->params);

        return ['code'=>empty($returnData)?400:200, 'msg'=>'成功','data'=>$returnData ];

    }
    /**
     * 更新专属机器人发送状态
     * @return array
     */
    public function upSendStatusCon(){

        $this->params['mid'] =  empty($this->params['mid']) ?0 : $this->params['mid'] ;
        $this->params['source'] =  empty($this->params['source']) ?0 : $this->params['source'] ;
        $this->params['send_status'] =  empty($this->params['send_status']) ?0 : $this->params['send_status'] ;

        $returnData =  ExclusiveMangerService::upSendStatusCon($this->params);

        return self::responseJson(empty($returnData)?400:200, '成功',$returnData);

    }

    /**
     * 添加专属机器人素材数据
     */
    public function  addSendDataToRedis(){

        $this->params['link_url'] =  empty($this->params['link_url']) ?'' : $this->params['link_url'] ;
        $this->params['pic_url'] =  empty($this->params['pic_url']) ?1 : $this->params['pic_url'] ;
        $this->params['content'] =    empty($this->params['content']) ?'' : $this->params['content'] ;
        $this->params['mid'] =     empty($this->params['mid']) ?0 : $this->params['mid'] ;
        $this->params['source'] =  empty($this->params['source']) ?0 : $this->params['source'] ;
        $this->params['send_time'] =  empty($this->params['send_time']) ?0 : $this->params['send_time'] ;
        $this->params['product_id'] =  empty($this->params['product_id']) ?0 : $this->params['product_id'] ;

        if(empty($this->params['link_url'])||empty($this->params['pic_url'])||empty($this->params['send_time'])||$this->params['send_time']<time()){
            return self::responseJson(400, '缺少参数,或者时间有误',[]);
        }
        $returnData =  ExclusiveMangerService::addSendDataToRedis($this->params);

        return self::responseJson(empty($returnData)?400:200, '成功',$returnData);

    }

    /**
     * 移除单个素材
     */
    public function delSendDataRedis()
    {   
        $this->params['mid'] = empty($this->params['mid']) ? 0 : $this->params['mid'];
        $this->params['source'] = empty($this->params['source']) ? '' : $this->params['source'];
        $this->params['random_str'] = empty($this->params['random_str']) ? '' : $this->params['random_str'];
        $this->params['send_time'] = empty($this->params['send_time']) ? 0 : $this->params['send_time'];

        if (empty($this->params['random_str'])) {
            return self::responseJson(400, '缺少参数', []);
        }

        $returnData = ExclusiveMangerService::delSendDataRedis($this->params);

        return ['code'=>empty($returnData['flag'])?400:200, 'msg'=>'成功','data'=>$returnData ];
    }


     public function  delRedisCacheData()
     {
         $this->params['page'] = empty($this->params['page']) ? 1 : $this->params['page'];
         $this->params['pageSize'] = empty($this->params['pageSize']) ? 20 : $this->params['pageSize'];
         $returnData=[];
          if(!empty( $this->params['page'])&&!empty($this->params['pageSize'] )){
              $returnData = ExclusiveMangerService:: delRedisRandomStrCache(['offset'=>( $this->params['page']-1)*$this->params['pageSize'] ,'display'=>  $this->params['pageSize']   ]);
          }
         return ['code'=>200, 'msg'=>'成功','data'=>$returnData ];
     }

}