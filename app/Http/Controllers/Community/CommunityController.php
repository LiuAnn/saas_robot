<?php
/**
 * Created by PhpStorm.
 * User:spz    社群首页数据
 * Date: 2020/3/25
 * Time: 18:24
 */
namespace App\Http\Controllers\Community;

use App\Model\Admin\UserModel;
use function config;
use function dd;
use function dump;
use Illuminate\Http\Request;
use App\Model\Member\MemberModel;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Service\Community\CommunityService;
use App\Model\Community\CommunityInvitationGroupModel;
class CommunityController extends Controller
{

    protected $params;
    public function __construct(Request $request)
    {
        $this->params = $request->all();
    }

    /**
     * 攻略标签
     * @return mixed
     */
    public function raidersLabel(Request $request) {
        return self::responseJson(200, '成功', CommunityService::getRaidersLabel($this->params['source']));
    }

    /**
     * 新人上手
     * @param Request $request
     * @return mixed
     */
    public function raidersNew(Request $request)
    {
        $mid = $request->member->getModel()['mid'];

        return self::responseJson(200, 'msg', CommunityService::raidersNew($mid));
    }

    /**
     * 邀请海报
     * @return mixed
     */
    public function invitePoster()
    {
        return self::responseJson(200, 'msg', CommunityService::invitePoster());
    }

    /**
     * 升级时生成的信息
     * @param Request $request
     * @return mixed
     */
    public function upgrade(Request $request)
    {
        $mid = $request->mid;

        $type = $request->source;
        //来源
        $source = $request->source;

        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            $countNum = config('community.daRenCountNum');
            //文案
            $upgradeContent = config('community.daRenUpgradeContent');
        } else {
            $countNum = config('community.countNum');
            //文案
            $upgradeContent = config('community.upgradeContent');
        }

        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            //大人截图示例
            $exampleImages = config('community.daRenExampleImages');
        } else {
            //截图示例
            $exampleImages = config('community.exampleImages');
        }

        if($source!=1&&!empty($source)){
            $type = config('community.groupType')[$source];//对应的type类型
        }
        //查询是否有审核中的数据
        $community = CommunityService::getGroupInfoByMidAndStatus($mid, 1, $type);

        if ($community) {
            return self::responseJson(400, '有审核中的群，请等待结果后再次申请');
        }

        //查询是否有数据
        $communityInfo = CommunityService::getGroupInfoByMidAndStatus($mid, 0, $type);

        //有数据取原来的数据不重新生成
        if ($communityInfo) {

            $data = [
                'groupId' => $communityInfo->id,
                'groupNo' => $communityInfo->group_number,
                'groupName' => $communityInfo->group_name,
                'tutorWx' => $communityInfo->tutor_wx,
                'exampleImages' => $exampleImages,
                'conutNum' => $countNum,
                'upgradeContent' => $upgradeContent,
            ];

            return self::responseJson(200, '成功', $data);
        }

        //获取分配的群编号
        $groupNo = CommunityService::getRandNumber();

        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            $groupName = config('community.daRenGroupName');
        } else {
            $groupName = config('community.groupName');
        }

        $data = [
            //群编号
            'groupNo' => $groupNo,
            //群名称
            'groupName' => $source==1? $groupName.$groupNo:config('community.groupNameData')[$source].$groupNo,
            //导师微信
            'tutorWX' =>   $source==1? config('community.tutorInfo')[0]['tutorWx']:config('community.tutorWx')[$source][0]['tutorWx'],
            //群人数要求
            'conutNum' => $countNum,
            //截图示例
            'exampleImages' => $exampleImages,
            //文案
            'upgradeContent' => $upgradeContent,
        ];
        //入库信息
        $createData = [
            'mid' => $mid,
            'group_number' => $data['groupNo'],
            'group_name' => $data['groupName'],
            'tutor_wx' => $data['tutorWX'],
            'type' => $type,
            'platform_identity' => $source==1?'7323ff1a23f0bf1cda41f690d4089353':$source,
            'addtime' => time(),
        ];

        try{
            //插入数据
            $createId = CommunityService::createData($createData);

            //主键id
            $data['groupId'] = $createId;

            if ($createId) {
                return self::responseJson(200, '成功', $data);
            }

        }catch (\Exception $e) {
            //保证数据不重复
            Log::info('user_upgrade:'.$e->getMessage());
        }

        return self::responseJson(400, '申请失败，请重试');


    }

    /**
     * 申请审核
     * @param Request $request
     * @return mixed
     */
    public function applyReview(Request $request)
    {
        $params = $this->params;

        if(isset($params['source']) && $params['source'] != '7323ff1a23f0bf1cda41f690d4089353' && $params['source'] != '75f712af51d952af3ab4c591213dea13') {
            // 参数验证
            $validator = Validator::make($params, [
                'groupId' => 'bail|required',
                'images' => 'bail|required',
            ], [
                'groupId.required' => '参数缺失',
                'images.required' => '请上传图片'
            ]);
            if ($validator->fails()) {
                $error_msg = $validator->messages()->first();
                return self::responseJson('400', $error_msg);
            }
        }
        // 1 待审核 2审核通过 3审核不通过'
        $examine_status = 1;
        if(isset($params['source'])){
            //判断当前source真实性
            $isSet = CommunityService::changeSourceIsSet($params['source']);
           if(empty($isSet)){
               return self::responseJson(400, '提交数据失败，请重新申请');
           }
        }
        $examine_step = isset($params['examine_step']) ? $params['examine_step'] : 0;

        if ($examine_step == 3) {
            $examine_status = 2;
        }
        $phone = UserModel::getMemberMobile($params['mid']) ?: '';
        $nickname = UserModel::getMemberNickname($params['mid']) ?: '';
        $image = isset($params['images']) ? $params['images'] : '';
        $updateData = [
            'images' => $image,
            'examine_step' => $examine_step,
            'examine_status' => $examine_status,
            'phone' => $phone,
            'nickname' => $nickname
        ];
        if (isset($params['groupId']) && !empty($params['groupId'])) {
            //更新数据
            $update = CommunityService::updateDataByIdMid($params['groupId'],$params['mid'], $updateData);
        } else {
            $update = CommunityService::updateDataByIdMidParam($params, $updateData);
        }

        if ($update !== false) {
            return self::responseJson(200, '成功');
        } else {
            return self::responseJson(400, '提交数据失败，请重新申请');
        }

    }

    /**
     * 申请升级
     * @param Request $request
     * @return mixed
     */
    public function applyReviewV2(Request $request)
    {
        $params = $this->params;

        return CommunityService::applyReviewV2($params);
    }

    /**
     * 获取我的群列表
     * @param Request $request
     * @return mixed
     */
    public function groupList(Request $request)
    {
        $params = $request->all();

        if (!isset($params['mid']) || empty($params['mid']) || !isset($params['source']) || empty($params['source'])) {
            return self::responseJson(400, '缺少参数');
        }
        $params['mid'] = $this->params['mid'];

        $params['type'] = $this->params['type'] ?? 1;  //区分直推群和间推群 1直推群 2间推群
        $params['page'] = $this->params['page'] ?? 1;
        $params['pageSize'] = $this->params['pageSize'] ?? 10;
        if (isset($params['group_id']) && !empty($params['group_id'])) {
            //查询是否有审核中的数据
            $community = CommunityService::getExamineStepByGroupId($params) ?: 0;
            $data = [
                'examine_step' => $community
            ];
            return self::responseJson(200, '成功', $data);
        }
        $data = CommunityService::getGroupListByMid($params);
        return self::responseJson(200, '成功', $data);
    }

    /**
     * 三方群数据
     * @param Request $request
     * @return array
     */
    public function groupListThridGroup(Request $request){
        $params = $request->all();

        if (!isset($params['mid']) || empty($params['mid']) || !isset($params['source']) || empty($params['source'])) {
            return self::responseJson(400, '缺少参数');
        }
        $data = CommunityService::getGroupListByMidBySource($params);
        return self::responseJson(200, '成功', $data);
    }

    /**
     * 根据群id获取群信息
     * @return mixed
     */
    public function groupInfo()
    {
        $params = $this->params;

        // 参数验证
        $validator = Validator::make($params, [
            'groupId' => 'bail|required',
        ], [
            'required'  => '参数缺失',
        ]);
        if ($validator->fails()) {
            $error_msg = $validator->messages()->first();
            return self::responseJson('400', $error_msg);
        }

        $info = CommunityService::getGroupInfoByGroupId($params['groupId']);

        $info->images = explode(',', $info->images);

        return self::responseJson(200, '成功', $info);
    }

    /**
     * 申请群助理
     * @return array|mixed
     */
    public function applyGroupAssistant()
    {
        $params = $this->params;

        // 参数验证
        $validator = Validator::make($params, [
            'groupId' => 'bail|required',
        ], [
            'required'  => '参数缺失',
        ]);
        if ($validator->fails()) {
            $error_msg = $validator->messages()->first();
            return self::responseJson('400', $error_msg);
        }

        $res = CommunityService::applyGroupAssistant($params);

        return $res;

    }

    /**
     * 申请群助理
     * @return array|mixed
     */
    public function applyGroupAssistantV2()
    {
        $params = $this->params;

        $res = CommunityService::applyGroupAssistantV2($params);

        return $res;

    }

    /**
     * YCloud申请群助理
     * @return array|mixed
     */
    public function applyYCGroupAssistant()
    {
        $params = $this->params;

        $returnData = CommunityService::applyYCGroupAssistant($params);

        if (isset($returnData['code']) && $returnData['code'] != 200) {
            $returnData['msg'] = $returnData['msg'] ?? '';
            return response()->fail($returnData['code'], $returnData['msg']);
        }
        return response()->success($returnData);

    }

    /**
     * YCloud申请群助理
     * @return array|mixed
     */
    public function applyYCloudStatus()
    {
        $params = $this->params;

        $returnData = CommunityService::applyYCloudStatus($params);

        if (isset($returnData['code']) && $returnData['code'] != 200) {
            $returnData['msg'] = $returnData['msg'] ?? '';
            return response()->fail($returnData['code'], $returnData['msg']);
        }
        return response()->success($returnData);

    }

    /**
     * 转链
     * @param Request $request
     * @return mixed
     *  妙飞（milkfly）马苏里拉芝士碎450g（奶酪碎 披萨拉丝奶酪 焗饭 烘焙材料）
    【在售价】23.5元
    【我买的价】23.5元

    【免费下载悦淘APP再省1.6元】
    【抢购地址】https://share.yuelvhui.com/ENJfye
    【邀请码】F26548518
     */
    public function conversionUrl(Request $request)
    {
        $this->params['mid'] = $request->member->getModel()['mid'];

        // 参数验证
        $validator = Validator::make($this->params, [
            'urlStr' => 'bail|required',
        ], [
            'required'  => '参数缺失',
        ]);
        if ($validator->fails()) {
            $error_msg = $validator->messages()->first();
            return self::responseJson('400', $error_msg);
        }

        $res = CommunityService::conversionUrl($this->params);

        if ($res) {
            return self::responseJson(200, '成功', ['content' => $res]);
        }

        return self::responseJson(400, '转链失败');

    }

    /**
     * 通讯录
     * @param Request $request
     * @return mixed
     */
    public function addressBook(Request $request)
    {
        $this->params['mid'] = $request->member->getModel()['mid'];

        $this->params['page'] = $this->params['page'] ?? 1;
        $this->params['pageSize'] = $this->params['pageSize'] ?? 10;

        return self::responseJson(200, '成功', CommunityService::getAddressBook($this->params));
    }

    /**
     * 粉丝列表分页
     * @return mixed
     */
    public function fanList(Request $request)
    {
        $this->params['mid'] = $request->member->getModel()['mid'];

        $this->params['page'] = $this->params['page'] ?? 2;
        $this->params['pageSize'] = $this->params['pageSize'] ?? 10;

        return self::responseJson(200, '成功', CommunityService::fanList($this->params));
    }

    /**
     * 修改微信号
     * @param Request $request
     * @return mixed
     */
    public function updateWx(Request $request)
    {
        $mid = $request->member->getModel()['mid'];

        // 参数验证
        $validator = Validator::make($this->params, [
            'wechat' => 'bail|required',
        ], [
            'required'  => '参数缺失',
        ]);
        if ($validator->fails()) {
            $error_msg = $validator->messages()->first();
            return self::responseJson('400', $error_msg);
        }

        $updateData = [
            'mid' => $mid,
            'wechat' => $this->params['wechat'],
        ];

        //修改用户微信号
        $update = MemberModel::updateMemberInfo($updateData);

        if ($update) {
            return self::responseJson(200, '修改成功');
        }

        return self::responseJson(400, '修改失败');
    }

    /**
     * 申请群助理的步骤
     * @return mixed
     */
    public function applyCopyWriting()
    {
        $data = CommunityService::applyCopyWriting($this->params);

        return self::responseJson(200, 'msg', $data);
    }


    /**
     * YCloud申请群助理的步骤
     * @return mixed
     */
    public function applyYCloudCopyWriting()
    {
        $data = CommunityService::applyYCloudCopyWriting($this->params);

        return self::responseJson(200, 'msg', $data);
    }

    /**
     * YCloud申请群助理的步骤
     * @return mixed
     */
    public function applyYCloudRules()
    {
        $data = CommunityService::applyYCloudRules($this->params);

        return self::responseJson(200, 'msg', $data);
    }

    /**
     * 分享小程序
     * @param Request $request
     * @return mixed
     */
    public function shareXcx(Request $request)
    {
        $mid = $request->mid;
        $source = $request->source;
        $data = CommunityService::shareXcx($mid,$source);
        return self::responseJson(200, 'msg', $data);

    }

    /**
     * 进阶学习
     * @param $request
     * @return mixed
     */
    public function getListJinjie(Request $request){

        $typeId = $request->route('typeId');

        $pageSize = $request->route('pageSize');

        $pageIndex = $request->route('pageIndex');

        $source = $request->route('source');

        $source = '89dafaaf53c4eb68337ad79103b36aff';
        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            if ($typeId == 2) {
                //大人小店的进阶学习
                $typeId = 17;
            }
        }

        $data['code'] = 200;

        $data['msg'] = '查询成功';

        $data['data'] = CommunityInvitationGroupModel::getArticleListByTypeIds($typeId,$pageIndex,$pageSize,$source);

        return $data;
    }

    public function listInfo(Request $request){
        $typeId = $request->route('typeId');
        $id = $request->route('id');
        $source = $request->route('source');
        $data['code'] = 200;
        $data['msg'] = '查询成功';
        $data['data'] = CommunityInvitationGroupModel::getArticleInfoId($typeId,$id,$source);

        return $data;

    }



    /**
     * 分享海报
     * @param Request $request
     * @return array
     */
    public function sharePoster(Request $request)
    {
        $mid = $request->mid;
        $source = $request->source;
        $data = CommunityService::sharePoster($mid, $this->params['posterImg'],$source);

        return self::responseJson(200, 'msg', $data);


    }

    /**
     * 新人上手
     * @param Request $request
     * @return mixed
     */
    public function raidersNewList(Request $request)
    {
        $mid = $request->mid;
        $source = $request->source;
        $type= $request->type;

        return self::responseJson(200, 'msg', CommunityService::raidersNewList($mid, $source,$type));
    }

    /**
     * 新人上手
     * @param Request $request
     * @return mixed
     */
    public function raidersNewListV2(Request $request)
    {
        $mid = $request->mid;
        $source = $request->source;
        $result = CommunityService::raidersNewListV2($mid, $source);
        if (isset($result['code'])) {
            $result['msg'] = isset($result['msg']) ? $result['msg'] : '参数错误';
            return self::responseJson(200, $result['msg'], []);
        }
        return self::responseJson(200, 'msg', $result);
    }


    /**
     * 新人上手
     * @param Request $request
     * @return mixed
     */
    public function raidersYCloudNewList(Request $request)
    {
        $result = CommunityService::raidersYCloudNewList();
        if (isset($result['code'])) {
            $result['msg'] = isset($result['msg']) ? $result['msg'] : '参数错误';
            return self::responseJson(200, $result['msg'], []);
        }
        return self::responseJson(200, 'msg', $result);
    }

    /**
     * 进阶学习
     * @param Request $request
     * @return mixed
     */
    public function raidersYCloudStudyArticle(Request $request)
    {
        $param = $request->all();
        $result = CommunityService::raidersYCloudStudyArticle($param);
        if (isset($result['code']) && $result['code'] != 200) {
            $result['msg'] = isset($result['msg']) ? $result['msg'] : '参数错误';
            return self::responseJson(200, $result['msg'], []);
        }
        return self::responseJson(200, 'msg', $result);
    }
    
    /**
     * 进阶学习
     * @param Request $request
     * @return mixed
     */
    public function raidersYCloudInvitePost(Request $request)
    {
        $source = $request->source;
        $type = $request->type;
        $source = '89dafaaf53c4eb68337ad79103b36aff';
        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            if ($type == 3) {
                //大人小店的邀请海报
                $type = 19;
            }
        }
        $param = $request->all();
        $result = CommunityService::raidersYCloudInvitePost($param);
        if (isset($result['code']) && $result['code'] != 200) {
            $result['msg'] = isset($result['msg']) ? $result['msg'] : '参数错误';
            return self::responseJson(400, $result['msg'], []);
        }
        return self::responseJson(200, 'msg', $result);
    }



    /**
     * 生成邀请海报
     * @param Request $request
     * @return mixed
     */
    public function raidersYCloudSharePoster(Request $request)
    {
        $param = $request->all();
        $result = CommunityService::raidersYCloudSharePoster($param, $this->params['posterImg']);
        if (isset($result['code']) && $result['code'] != 200) {
            $result['msg'] = isset($result['msg']) ? $result['msg'] : '参数错误';
            return self::responseJson(400, $result['msg'], []);
        }
        return self::responseJson(200, 'msg', $result);
    }


    /**
     * 邀请海报
     * @return mixed
     */
    public function invitePosterList(Request $request)
    {
        $source = $request->source;
        $type = $request->type;
        $source = '89dafaaf53c4eb68337ad79103b36aff';
        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            if ($type == 3) {
                //大人小店的邀请海报
                $type = 19;
            }
        }
        return self::responseJson(200, 'msg', CommunityService::invitePosterList($source,$type));
    }

    //获取视频url
    public function getVideoUrl(Request $request){
        $data['code'] = 200;

        $data['msg'] = '查询成功';

        $data['data'] = ['url'=>'https://image.yuetao.vip/ditan/gonglue.mp4'];

        return $data;
    }
}


