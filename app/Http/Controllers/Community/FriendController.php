<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/5/7
 * Time: 11:45
 */

namespace App\Http\Controllers\Community;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Community\FriendService;
class FriendController   extends Controller
{


    public function __construct(Request $request)
    {
        $this->middleware('user.check.login', ['only' => [ 'getList', 'getFriendRobotList']]);

    }

    /**
     * 获取好友列表
     * @param Request $request
     * @return mixed
     */
    public function getList(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return FriendService::getRobotUserList($param,$member);
    }


    /**
     * 获取好友列表的机器人列表
     * @param Request $request
     * @return mixed
     */
    public function getFriendRobotList(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return FriendService::getFriendRobotList($param,$member);
    }


}