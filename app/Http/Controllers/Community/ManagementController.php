<?php

namespace App\Http\Controllers\Community;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Community\ManagementService;

class ManagementController extends Controller
{
    /**
     * 群会员列表
     * @param Request $request
     * @return array
     * @author  liujiaheng
     * @created 2020/4/7 6:35 下午
     */
    public function queryMemberList(Request $request)
    {
        $data = $request->all();
        // 验证规则
        $rules = [
            'groupId'  => 'required|integer',
            'page'     => '',
            'pageSize' => '',
            'sort'     => '',
            'isPlus'   => '',
        ];
        // 提示信息
        $messages = [
            'required' => ':attribute必须存在',
            'string'   => ':attribute必须为字符串',
        ];
        // 字段别名
        $attributes = [
            'groupId'  => '群ID',
            'sort'     => '排序',
            'isPlus'   => '是否plus会员',
            'page'     => '页码',
            'pageSize' => '数据量',
        ];

        $validator = \Validator::make($data, $rules, $messages, $attributes);

        //验证参数
        if ($validator->fails()) {
            return responseJson(400, $validator->errors()->first());
        }

        return responseJson(200, 'success', ManagementService::queryMemberList($data));
    }

    /**
     * 群详细信息
     * @param Request $request
     * @return array
     * @author  liujiaheng
     * @created 2020/4/8 11:29 上午
     */
    public function queryGroupInfo(Request $request)
    {
        $data = $request->all();
        // 验证规则
        $rules = [
            'groupId' => 'required|integer',
        ];
        // 提示信息
        $messages = [
            'required' => ':attribute必须存在',
            'string'   => ':attribute必须为字符串',
        ];
        // 字段别名
        $attributes = [
            'groupId' => '群ID',
        ];

        $validator = \Validator::make($data, $rules, $messages, $attributes);

        //验证参数
        if ($validator->fails()) {
            return responseJson(400, $validator->errors()->first());
        }

        return responseJson(200, 'success', ManagementService::queryGroupInfo($data));
    }

    /**
     * 群分享详情
     * @param Request $request
     * @return array
     * @author  liujiaheng
     * @created 2020/4/8 3:28 下午
     */
    public function queryShareInfo(Request $request)
    {
        $data = $request->all();
        // 验证规则
        $rules = [
            'groupId'   => 'required|integer',
            'startDate' => 'required|string',
            'endDate'   => 'required|string',
        ];
        // 提示信息
        $messages = [
            'required' => ':attribute必须存在',
            'string'   => ':attribute必须为字符串',
        ];
        // 字段别名
        $attributes = [
            'groupId'   => '群ID',
            'startDate' => '开始时间',
            'endDate'   => '结束时间',
        ];

        $validator = \Validator::make($data, $rules, $messages, $attributes);

        //验证参数
        if ($validator->fails()) {
            return responseJson(400, $validator->errors()->first());
        }

        return responseJson(200, 'success', ManagementService::queryShareInfo($data));
    }

    /**
     * 群列表
     * @param Request $request
     * @return array
     * @author  liujiaheng
     * @created 2020/4/8 4:13 下午
     */
    public function queryGroupList(Request $request)
    {
        $data = $request->all();
        // 验证规则
        $rules = [
            'mid' => 'required|integer',
        ];
        // 提示信息
        $messages = [
            'required' => ':attribute必须存在',
            'string'   => ':attribute必须为字符串',
        ];
        // 字段别名
        $attributes = [
            'mid' => '用户ID',
        ];

        $validator = \Validator::make($data, $rules, $messages, $attributes);

        //验证参数
        if ($validator->fails()) {
            return responseJson(400, $validator->errors()->first());
        }

        return responseJson(200, 'success', ManagementService::queryGroupList($data));
    }

}
