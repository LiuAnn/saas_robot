<?php
/**
 * Created by PhpStorm.
 * User:spz    社群首页数据
 * Date: 2020/3/25
 * Time: 18:24
 */

namespace App\Http\Controllers\Community;

use App\Http\Controllers\Controller;
use App\Service\Community\ExternalService;
use Illuminate\Http\Request;
use App\Service\Community\ProductService;

class ProductController extends Controller
{   


    const PRODUCT_TYPE = [5,6,7,8,9,11,12,13,14,15,16];

    public function __construct()
    {
    }

    //商品池商品列表
    public function getProductList(Request $request)
    {   
        //type 产品类型  4:自营 5:京东 6:拼多多 7:淘宝 8:唯品会
        //接收数据  数据验证
        $param = $request->All();
        $member = $request->member;
        $param['page'] = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize'] = isset($param['pageSize']) ? $param['pageSize'] : 20;
        $param['goodName'] = isset($param['goodName']) ? $param['goodName'] :'';
        $param['maxPrice'] = isset($param['maxPrice']) ? $param['maxPrice'] :'';
        $param['minPrice'] = isset($param['minPrice']) ? $param['minPrice'] :'';
        $param['bandId'] = isset($param['bandId']) ? $param['bandId'] :0;
        $param['sorttype'] = isset($param['sorttype']) ? $param['sorttype'] :1; // 1  佣金金额
        $param['sort']      = isset($param['sort']) ? $param['sort'] :2;        //  1  升序  2  降序
        $param['type']   = isset($param['type'])?$param['type']:4;
        $param['bandId'] = ($param['type']==7&&empty($param['goodName'])&&$param['bandId']<1)?3786:$param['bandId'];
        $param['cityName'] = isset($param['cityName'])?$param['cityName']:'北京';
        $param['startTime'] = isset($param['startTime'])?$param['startTime']:date("Y-m-d",time());
        $param['endTime'] = isset($param['endTime'])?$param['endTime']:date("Y-m-d",strtotime("+1 day"));

        if($member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353'){
            if ($param['type'] == 4) {
                return ProductService::getProductList($param, $member);
            } else if (in_array($param['type'], self::PRODUCT_TYPE)) {
                $classId = empty($param['bandId']) ? 0 : $param['bandId'];
                $eliteId = empty($param['ModelId']) ? 0 : $param['ModelId'];

                    if ($param['type'] == 5&&$eliteId>0) {
                        $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId,  $param['page'],  $param['pageSize'], $param['sorttype'],$param['sort']  );
                        return $result;
                    } else if ($param['type'] == 6&&$classId>0) {
                        $result = ProductService::getPddOusideGoodsByCategoryId($classId, $param['page'],  $param['pageSize'],$param['sorttype'],$param['sort'] );
                        return $result;
                    } else if ($param['type'] == 7&&$classId>0){
                        $result = ProductService::gettaobkOusideGoodsByCategoryId($classId, $param['page'],  $param['pageSize']);
                        return $result;
                    } else if($param['type'] == 8&&($param['goodName']!=''||$param['maxPrice']!=''||$param['minPrice']!='')){
                        $result = ProductService::getwphkOusideGoodsByCategoryId($param,$param['page'],$param['pageSize']);
                        return $result;
                    }
                return ProductService::querySearchNewEndSearchData($param, $member);
            }
        }else if($member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff'){
            if($param['type'] == 4){
                return ProductService::querySearchDarenSearchData($param, $member);
            } else if ($param['type'] == 5 || $param['type'] == 6) {
                $classId = empty($param['bandId']) ? 0 : $param['bandId'];
                $eliteId = empty($param['ModelId']) ? 0 : $param['ModelId'];
                    if ($param['type'] == 5&&$eliteId>0) {
                        $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId,  $param['page'],  $param['pageSize'],$param['sorttype'],$param['sort'] );
                        return $result;
                    } else if ($param['type'] == 6&&$classId>0) {
                        $result = ProductService::getPddOusideGoodsByCategoryId($classId, $param['page'],  $param['pageSize'],$param['sorttype'],$param['sort'] );
                        return $result;
                    }
                return ProductService::querySearchNewEndSearchData($param, $member);
            }
        }else  if($member['platform_identity']=='75f712af51d952af3ab4c591213dea13'){
            if($param['type'] == 4){
                return ProductService::querySearchZhiyingSearchData($param, $member);
            } elseif ($param['type'] == 5 || $param['type'] == 6 || $param['type'] == 15 || $param['type'] == 11|| $param['type'] == 16|| $param['type'] == 17) {
                $classId = empty($param['bandId']) ? 0 : $param['bandId'];
                $eliteId = empty($param['ModelId']) ? 0 : $param['ModelId'];
                if ($param['type'] == 5 && $eliteId > 0) {
                    $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId, $param['page'], $param['pageSize'], $param['sorttype'], $param['sort']);
                    return $result;
                } else if ($param['type'] == 6 && $classId > 0) {
                    $result = ProductService::getPddOusideGoodsByCategoryId($classId, $param['page'], $param['pageSize'], $param['sorttype'], $param['sort']);
                    return $result;
                }
                return ProductService::querySearchNewEndSearchData($param, $member);
            }
        }else if($member['platform_identity']=='183ade9bc5f1c4b52c16072362bb21d1'){
          if ($param['type'] == 5 || $param['type'] == 6) {
                $classId = empty($param['bandId']) ? 0 : $param['bandId'];
                $eliteId = empty($param['ModelId']) ? 0 : $param['ModelId'];
                if ($param['type'] == 5 && $eliteId > 0) {
                    $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId, $param['page'], $param['pageSize'], $param['sorttype'], $param['sort']);
                    return $result;
                } else if ($param['type'] == 6 && $classId > 0) {
                    $result = ProductService::getPddOusideGoodsByCategoryId($classId, $param['page'], $param['pageSize'], $param['sorttype'], $param['sort']);
                    return $result;
                }
            }
            return ProductService::querySearchNewEndSearchData($param, $member);
        }else if($member['platform_identity']=='c23ca372624d226e713fe1d2cd44be35'){
            if($param['type'] == 4){
                return ProductService::querySearchLinJuTuanSearchData($param, $member);
            }            
        }


    }

    /**
     * 自营商品分类
     * @return array
     */
    public function getChannelOneCategoryList(Request $request){
        $member = $request->member;
        //悦淘
        if($member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353' && $member['user_type']>=2){
            return ProductService::getChannelOneCategoryList();
            //大人
        }else if($member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff' && $member['user_type']>=2){
            return [
                'code' => 200,
                'data' => [],
                'msg'  => '查询成功'
            ];
        }else if($member['platform_identity']=='75f712af51d952af3ab4c591213dea13'){
            return ProductService::getZhidingCategoryList();
        }else if($member['platform_identity']=='183ade9bc5f1c4b52c16072362bb21d1'){
            return ProductService::getMaituCategoryList();
        }else{
            return [
                'code' => 200,
                'data' => [],
                'msg'  => '查询成功'
            ];
        }
    }

    /**
     * 京东类目数据 频道
     */
    public function jdByClassData(Request $request)
    {
        //接收数据  数据验证
        $param = $request->All();
        $member = $request->member;

        $result=[];
        if($member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353' && $member['user_type']>=2){
            if ($param['type'] == 4) {
                return ProductService::getProductList($param, $member);
            } else if ($param['type'] == 5) {
                $result = ProductService::getJdClassAllData($param, $member);
            } else if ($param['type'] == 6) {
                $result = ProductService::getPddClassAllData($param, $member);
            } else if ($param['type'] == 7) {
                $result = ProductService::getTaobkClassAllData($param, $member);
            } else if ($param['type'] == 8) {
                $result = ProductService::getWphCategory(1);
            } else if ($param['type'] == 9) {
                $result = ProductService::getSuNingCategory();
            }
            //大人
        }else if($member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff' && $member['user_type']>=2){
            if ($param['type'] == 5) {
                $result = ProductService::getJdClassAllData($param, $member);
            } else if ($param['type'] == 6) {
                $result = ProductService::getPddClassAllData($param, $member);
            }
        }else if($member['platform_identity']=='75f712af51d952af3ab4c591213dea13' && $member['user_type']>=2){
            if ($param['type'] == 5) {
                $result = ProductService::getJdClassAllData($param, $member);
            } else if ($param['type'] == 6) {
                $result = ProductService::getPddClassAllData($param, $member);
            }
        }else if($member['platform_identity']=='183ade9bc5f1c4b52c16072362bb21d1' && $member['user_type']>=2){
            if ($param['type'] == 5) {
                $result = ProductService::getJdClassAllData($param, $member);
            } else if ($param['type'] == 6) {
                $result = ProductService::getPddClassAllData($param, $member);
            }else if ($param['type'] == 8) {
                $result = ProductService::getWphCategory(1);
            }
        }
        return self::responseJson('200', '成功', $result);
    }
    /**
     * 京东 品类 频道
     */
    public function jdByClassIdData(Request $request)
    {
        $param = $request->All();
        $member = $request->member;
        $type = empty($param['type']) ? 0 : $param['type'];
        $classId = empty($param['bandId']) ? 0 : $param['bandId'];
        $eliteId = empty($param['ModelId']) ? 0 : $param['ModelId'];
        $pageNum = empty($param['page']) ? 1 : $param['page'];
        $pageSize = empty($param['pageSize']) ? 10 : $param['pageSize'];
        if (empty($classId) && empty($eliteId)) {
            return self::responseJson('200', '成功', ['data' => []]);
        } else {
            $result=[];
            if($member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353' && $member['user_type']>=2){
                if ($type == 5) {
                    $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId, $pageNum, $pageSize);
                } else if ($type == 6) {
                    $result = ProductService::getPddOusideGoodsByCategoryId($classId, $pageNum, $pageSize);
                }
            }else if($member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff' && $member['user_type']>=2){
                if ($type == 5) {
                    $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId, $pageNum, $pageSize);
                } else if ($type == 6) {
                    $result = ProductService::getPddOusideGoodsByCategoryId($classId, $pageNum, $pageSize);
                }
            }else if($member['platform_identity']=='75f712af51d952af3ab4c591213dea13' && $member['user_type']>=2){
                if ($type == 5) {
                    $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId, $pageNum, $pageSize);
                }
            }else if($member['platform_identity']=='183ade9bc5f1c4b52c16072362bb21d1' && $member['user_type']>=2){
                if ($type == 5) {
                    $result = ProductService::getJdOusideGoodsByCategoryId($classId, $eliteId, $pageNum, $pageSize);
                } else if ($type == 6) {
                    $result = ProductService::getPddOusideGoodsByCategoryId($classId, $pageNum, $pageSize);
                }
            }
            return self::responseJson('200', '成功', [$result]);
        }
    }

    /*
    * 管理员统计
    * @param Request $request
    * @throws ServicesException
    * @retuen array
    */
    public function getRobotSendGoodList(Request $request)
    {

        $param = $request->all();
        if ($request->member['platform_identity']) {
            $param['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->fail(200001, '用户无权限');
        }
        return ExternalService::getRobotSendGoodList($param);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function updateRobotSendGoodStatus(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->fail(200001, '用户无权限');
        }
        $memberInfo = $request->member;
        return ProductService::updateRobotSendGoodStatus($memberInfo, $params);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function deleteRobotSendGood(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->fail(200001, '用户无权限');
        }
        $memberInfo = $request->member;
        return ProductService::deleteRobotSendGood($memberInfo, $params);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getShortLinkUrl(Request $request)
    {
        $param = $request->All();
        $member = $request->member;
        $result='';
        if($member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353' && $member['user_type']>=2){
//            if (!in_array($param['type'], [11,12,13,14]))
//            ProductService::addAutoSendData($param, $member);
            $result = ProductService::getProductShortLink($param, $member);
        }else if($member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff' && $member['user_type']>=2){
            $result = ProductService::getDarenProductShortLink($param, $member);
        }else if($member['platform_identity']=='75f712af51d952af3ab4c591213dea13' && $member['user_type']>=2){
            $result = ProductService::getZhidingProductShortLink($param, $member);
        }else if($member['platform_identity']=='183ade9bc5f1c4b52c16072362bb21d1' && $member['user_type']>=2){
            $result = ProductService::getMaituProductShortLink($param, $member);
        }
        if (isset($result['code']) && $result['code'] != 200) {
            $result['msg'] = $result['msg'] ?? '';
            return self::responseJson($result['code'], $result['msg'], []);
        }
        return self::responseJson('200', '成功', ['shortLink' => $result]);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getTbkLongUrl(Request $request)
    {
        $param = $request->All();

        return ProductService::getTbkLongUrl($param);
    }

    /**
     * @param Request $request
     */
    public function changeOrderGoods(Request $request)
    {
        $param = $request->All();
        $param['good_type'] = empty($param['good_type']) ? 2 : $param['good_type'];
        $param['page'] = empty($param['page']) ? 1 : $param['page'];
        $param['pageSize'] = empty($param['pageSize']) ? 10 : $param['pageSize'];
        return ProductService::changeOrderGoods($param);
    }


    public function getTest(){
        $body =$this->params;
        ProductService::getTest($body);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getActiveList(Request $request)
    {
        $param = $request->All();
        $member = $request->member;

        return ProductService::getActiveList($param,$member);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function addActiveProduct(Request $request)
    {
        $param = $request->All();
        $member = $request->member;

        return ProductService::addActiveProduct($param,$member);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getActiveInfoForH5(Request $request)
    {
        $param = $request->All();

        return ProductService::getActiveInfoForH5($param);
    }

    

    /**
     * @param Request $request
     * @return array
     */
    public function delActiveInfo(Request $request)
    {
        $param = $request->All();

        $member = $request->member;

        return ProductService::delActiveInfo($param,$member);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateActiveProduct(Request $request)
    {
        $param = $request->All();
        $member = $request->member;

        return ProductService::updateActiveProduct($param,$member);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getActiveInfo(Request $request)
    {
        $param = $request->All();

        return ProductService::getActiveInfo($param);
    }

}










