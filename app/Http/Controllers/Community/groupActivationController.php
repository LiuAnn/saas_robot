<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/31
 * Time: 21:19
 */

namespace App\Http\Controllers\Community;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\Community\GroupActivationService;

class groupActivationController   extends Controller
{
    protected $params;
    public function __construct(Request $request)
    {
        $this->params = $request->all();
    }
    /**
     * 群激活首页接口
     * @return array
     */
    public function  groupHomeindexInfo()
    {
        $uid =  empty($this->params['uid']) ?0 : $this->params['uid'] ;
        $page =  empty($this->params['page']) ?1 : $this->params['page'] ;
        $pageSize =  empty($this->params['page']) ?10 : $this->params['pageSize'] ;
//        $text=  empty($this->params['text']) ?10 : $this->params['text'] ;
//         $encode =   GroupActivationService:: UnicodeEncode($text);
//         $decode =   GroupActivationService:: unicodeDecode($encode);
        return  GroupActivationService::getGroupInfoByMid($uid,$page,$pageSize);
    }
    /**
     * 群激活未激活列表
     * @return mixed
     */
    public function groupHomeListByPage()
    {
        $uid =  empty($this->params['uid']) ?0 : $this->params['uid'] ;
        $type =  empty($this->params['type']) ?0 : $this->params['type'] ;
        $page =  empty($this->params['page']) ?1 : $this->params['page'] ;
        $pageSize =  empty($this->params['page']) ?10 : $this->params['pageSize'] ;
        return  GroupActivationService::getGroupListByPage($uid,$type,$page,$pageSize);
    }
    /**
     * 群未激活的详细信息
     */
    public function getGroupNotActivationInfo()
    {
        $room_id =  empty($this->params['room_id']) ?0 : $this->params['room_id'] ;
        $uid =  empty($this->params['uid']) ?0 : $this->params['uid'] ;
        return  GroupActivationService::getGroupNotActivatioInfoByRoomId($uid,$room_id);
    }
}