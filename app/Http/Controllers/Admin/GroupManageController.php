<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\GroupManageService;
use App\Service\Admin\UserService;
use Illuminate\Http\Request;
use App\Service\Robot\UserOperationLogService;
use App\Service\OrderService\RobotOrderService;

class GroupManageController extends Controller
{
    private $validate = [
        'service_name'      => 'required',//服务项目名称
        'service_price'     => 'required|min:1',//服务项目价格
        'service_unit'      => 'required|max:1',//服务项目单位 1、年 2、月 3、日
        'group_number'      => 'required|min:1',//群数量
        'sort'              => 'required|int',//排序
    ];
    /*
     * 查看审核群列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */

    public function getGroupVerifyList(Request $request)
    {

        $param = $request->all();

        $memberInfo = $request->member;
        $param['platform_identity'] = $memberInfo['platform_identity'];
        return GroupManageService::getGroupVerifyList($param);        
    }



    /*
     * 审核群
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function toExamineGroup(Request $request)
    {
        $param = $request->all();

        return GroupManageService::toExamineGroup($param);

    }
    /*
     * 群审核详情
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function verifyGroupInfo(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getGroupVerifyInfo($param);

    }


    /*
     * 机器人列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getRobotList(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getRobotList($param);

    }


    /*
     * 数据统计
     */
    public static function getChannelList()
    {
        $data = GroupManageService::getChannelList();
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }
    /*
     * 数据统计
     */
    public static function getAdminDataStatistics(Request $request)
    {
        $param = $request->all();
        $data = GroupManageService::getAdminDataStatistics($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }



    /*
     * 用户后台首页统计图表数据展示
     */
    public function getOrderGraph(Request $request)
    {
        $param = $request->all();
        //$memberInfo = $request->attributes->get('member');
        //当前登录用户的用户信息
        //$param['mid'] = $memberInfo['adminUserId'];
        $data = $adminUserInstance = GroupManageService::getOrderGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 社群列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getCommunityList(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getCommunityList($param);

    }

    /*
     * 群主列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getGroupOwnerList(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getGroupOwnerList($param);

    }

    /*
    * 群主列表
    * @param Request $request
    * @throws ServicesException
    * @retuen array
    */
    public function getHouTaiGroupOwnerList(Request $request)
    {
        $param = $request->all();
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);
            $param['user_type'] = $memberInfo->user_type ?? 1;
            $param['sup_id'] = $memberInfo->sup_id ?? 0;
        }
        $data = GroupManageService::getHouTaiGroupOwnerList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);

    }

    /*
     * 消息列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getMessageList(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getMessageList($param);

    }

    /*
     * 统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getStatisticsInfo(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getStatisticsInfo($param);

    }


    /*
     * 统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getStatisticsInfoByRobot(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getStatisticsInfo($param);

    }

    /*
     * 统计信息群维度统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getStatisticsInfoByRoomId(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getStatisticsInfo($param);

    }


    /*
     * 直播订单排名统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getLiveRankInfo(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getLiveRankInfo($param);

    }

    /*
     * 获取直播人数根据时间
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getLiveNumByTime(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getLiveNumByTime($param);

    }


    /*
     * 获取直播人数根据时间
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getLiveNumByTimeGraph(Request $request)
    {
        $param = $request->all();

        $data = GroupManageService::getLiveNumByTimeGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);

    }



    /*
     * 获取机器人群统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function getRobotGroupInfo(Request $request)
    {
        $param = $request->all();

        return GroupManageService::getRobotGroupInfo($param);

    }


    /*
     * 获取机器人群统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getRobotGroupInfoGraph(Request $request)
    {
        $param = $request->all();

        $data = GroupManageService::getRobotGroupInfoGraph($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);

    }

    /**
     * 统计消息发送的群的数量
     * @param Request $request
     * @return mixed
     */
    public  function  getSendGroupNumByLinkType(Request $request){
        $param = $request -> All();
        $result = UserOperationLogService::getSendGroupNumByLinkType( $param);
        return response()->success($result);

    }

    /*
     * 获取商品统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */  
    public function getProductStatistics(Request $request)
    {
        $param = $request->all();
        $memberInfo = $request->member;
        return GroupManageService::getProductStatistics($param,$memberInfo);
    }

    /*
     * 获取群主统计数据
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function  getGroupLeaderList(Request $request)
    {
        $param = $request->all();
        return GroupManageService::getGroupLeaderList($param);
    }

    /**
     * 群活跃度统计
     * @param Request $request
     * @return mixed
     */
    public function getGroupActivityStatistics(Request $request)
    {
        $param = $request->all();
        $memberInfo = $request->member;
        return GroupManageService::getGroupActivityStatistics($param, $memberInfo);
    }

    /**
     * 购买的上品订单列表
     */
    public  static  function getOrderGoodsList(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        $getOrderGoodsData = RobotOrderService::getOrderGoodsList($param, $member);
        return [
            "code" => 200,
            "msg" => "success",
            "data" => $getOrderGoodsData,
        ];
    }

    /**
     * 获取群自定义标签列表
     * @param Request $request
     * @return mixed
     */
    public static function getGroupTagsList(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        $getGroupTagsData = GroupManageService::getGroupTagsList($param, $member);
        return [
            "code" => 200,
            "msg" => "success",
            "data" => $getGroupTagsData,
        ];

    }


    /**
     * 添加自定义标签
     * @param Request $request
     * @return mixed
     */
    public static function addGroupTags(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::addGroupTags($param, $member);
    }

}