<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\SectioynService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SectioynController extends Controller
{
    /*
     * 获取部门信息
     */
    public function allSectioynList()
    {
        //获取数据
        return SectioynService::getSectioyn();
        
    }
}