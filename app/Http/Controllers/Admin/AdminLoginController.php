<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\AdminLoginService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class AdminLoginController extends Controller
{



    /*
     * 管理员登录
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function Login(Request $request)
    {
        $param = $request->all();
        //查询  用户名和密码是否正确
        $adminUserInstance = AdminLoginService::getInstanceByNameAndPassword($param);

        return $adminUserInstance;
    }


    /*
     * 管理员登录联合登录
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function LoginForMiddel(Request $request)
    {
        $param = $request->all();
        //查询  用户名和密码是否正确
        $adminUserInstance = AdminLoginService::getInstanceByNameFormiddle($param);

        return $adminUserInstance;
    }

    /**
     * 更新密码
     * @param Request $request
     * @return mixed
     */
    public function updatePassword(Request $request)
    {
        $param  = $request->All();
        $member = $request->get('member');
        return AdminLoginService::updatePassword($param, $member);
    }
}