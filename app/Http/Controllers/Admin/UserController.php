<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\GroupManageService;
use App\Service\Admin\UserService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    /*
     * 查看用户列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */

    public function getUserList(Request $request)
    {

        $param = $request->all();

        return UserService::getUserList($param);        
    }

    /*
     * 查看用户统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */

    public function getUserinfo(Request $request)
    {

        $param = $request->all();

        return UserService::getUserinfo($param);        
    }

    /*
     * 赠送用户群数量
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */

    public function givUserGroup(Request $request)
    {

        $param = $request->all();

        $memberInfo = $request->get('member');

        return UserService::givUserGroup($memberInfo,$param);       
    }

    /*
     * 专属机器人列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */

    public function getExclusiveRobotList(Request $request)
    {

        $param = $request->all();

        return UserService::getExclusiveRobotList($param);       
    }

    /*
     * 专属机器人审核
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function examineExclusiveRobot(Request $request)
    {

        $param = $request->all();

        $memberInfo = $request->get('member');

        return UserService::examineExclusiveRobot($memberInfo,$param);   
    }


     /*
     * 管理员统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getUserAdminList(Request $request)
    {

        $param = $request->all();

        $memberInfo = $request->member;

        return UserService::getUserAdminList($memberInfo,$param);   
    }   

}