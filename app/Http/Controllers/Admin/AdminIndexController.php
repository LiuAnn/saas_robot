<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\GroupMember;
use App\Service\Admin\AdminIndexService;
use App\Service\Admin\GroupManageService;
use App\Service\Admin\UserService;
use App\Service\Robot\UserOperationLogService;
use Illuminate\Http\Request;

class AdminIndexController  extends Controller
{

    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [
            'index', 'dataStatistics', 'adminIndexGraph', 'getGroupMemberList','groupExclusiveAdminStatistics','dataExclusiveStatistics',
            'adminIndexTotal', 'getUserOperationList', 'getUserListByName', 'groupAdminStatistics', 'updateGroupMember',
            'seeNowBingData','getMemInfoByMobile','bindGroupMid','getUserAdminList','getGroupAdminCommission','getGroupAdminCommissionEveryDay'
        ]]);
    }

     /*
     * 管理员统计
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getUserAdminList(Request $request)
    {

        $param = $request->all();

        $memberInfo = $request->get('member');

        return UserService::getUserAdminList($memberInfo,$param);   
    }   
    /*
     * 用户后台首页数据展示
     */
    public function index(Request $request)
    {
        $param = $request->all();
        //当前登录用户的用户信息
        //$memberInfo = $request->member;
        if (!isset($param['mid']) || empty($param['mid'])) {
            $param['mid'] = $request->member['mid'];
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
            $param['platform_identity'] = $request->member['platform_identity'];
        } else {
            $memberInfo = UserService::getUserInfoById($param['mid']);
            //$param['mid'] = 12334;
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['mid'] = $memberInfo->mid ?? 0;
            $param['user_type'] = $memberInfo->user_type ?? 1;
            $param['sup_id'] = $memberInfo->sup_id ?? 0;
            $param['platform_identity'] = $memberInfo->platform_identity ?? '';
        }
        //$param['user_type'] = 2;
        $data = $adminUserInstance = AdminIndexService::getIndexStatistics($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 用户后台首页总数据展示
     */
    public function adminIndexTotal(Request $request)
    {
        $param = $request->all();
        //当前登录用户的用户信息
        if (!isset($param['mid']) || empty($param['mid'])) {
            $param['mid'] = $request->member['mid'];
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
            $param['platform_identity'] = $request->member['platform_identity'];
        } else {
            $memberInfo = UserService::getUserInfoById($param['mid']);
            //$param['mid'] = 12334;
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['user_type'] = $memberInfo->user_type ?? 1;
            $param['sup_id'] = $memberInfo->sup_id ?? 0;
            $param['platform_identity'] = $memberInfo->platform_identity ?? '';
        }
        $data = $adminUserInstance = AdminIndexService::getIndexStatisticsTotal($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
     * 用户后台首页统计图表数据展示
     */
    public function adminIndexGraph(Request $request)
    {
        $param = $request->all();
        //当前登录用户的用户信息
        if (!isset($param['mid']) || empty($param['mid'])) {
            $param['mid'] = $request->member['mid'];
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
            $param['platform_identity'] = $request->member['platform_identity'];
        } else {
            $memberInfo = UserService::getUserInfoById($param['mid']);
            //$param['mid'] = 12334;
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['user_type'] = $memberInfo->user_type ?? 1;
            $param['sup_id'] = $memberInfo->sup_id ?? 0;
            $param['platform_identity'] = $memberInfo->platform_identity ?? '';
        }
        $data = $adminUserInstance = AdminIndexService::getIndexGraphStatistics($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
    * 用户后台首页订单群数量
    */
    public function orderGroupNum(Request $request)
    {
        $param = $request->all();
        //当前登录用户的用户信息
        //$memberInfo = $request->member;
        $param['mid'] = $request->member['mid'];
        //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人数量
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $param['sup_id'] = $request->member['sup_id'] ?? 0;
        $data = $adminUserInstance = AdminIndexService::orderGroupNum($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
    * 用户后台首页订单群列表
    */
    public function orderGroupList(Request $request)
    {
        $param = $request->all();
        //当前登录用户的用户信息
        //$memberInfo = $request->member;
        $param['mid'] = $request->member['mid'];
        //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人数量
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $param['sup_id'] = $request->member['sup_id'] ?? 0;
        $data = $adminUserInstance = AdminIndexService::orderGroupList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
     * 用户后台数据统计
     */
    public function dataStatistics(Request $request)
    {
        $param = $request->all();
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);
//            //$param['mid'] = 12334;
            if($request->member['mid']==$param['mid']){
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] = $memberInfo->sup_id ?? 0;
            }else{
                $param['mid'] =$param['mid'];
                //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] =  $memberInfo->sup_id ?? 0;
            }
//            //用户类型进行查找数据 1 普通用户  2 社群管理人员

        } else {
            $param['mid'] = $request->member['mid'];
            //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
        $data = AdminIndexService::getDataStatistics($param,$request->member);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 社群列表
     * @param Request $request
     * @return mixed
     */
    public function getRoomIdsByCondition(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::getRoomIdsByCondition($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 社群列表
     * @param Request $request
     * @return mixed
     */
    public function getGroupList(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::getGroupList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 我的机器人列表
     *
     */
    public function updateGroupStatus(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::updateGroupStatus($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 社群列表--YCloud小程序端
     */
    public function getMiniAppRobotList(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::getMiniAppRobotList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
     * 社群列表--YCloud小程序端
     */
    public function deleteGroupStatus(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::deleteGroupStatus($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }



    /*
     * 绑定群主
     */
    public function bindYCloudGroupMid(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::bindYCloudGroupMid($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 绑定YCloud机器人
     */
    public function bindYCloudRobot(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::bindYCloudRobot($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 绑定YCloud机器人审核通过
     */
    public function applyYCRobotPass(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::applyYCRobotPass($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 绑定YCloud机器人审核拒绝
     */
    public function applyYCRobotRefuse(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::applyYCRobotRefuse($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 专属机器人群统计
     * @param Request $request
     * @return mixed
     */
    public function  dataExclusiveStatistics(Request $request){
        $param = $request->all();
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);
//            //$param['mid'] = 12334;
            if($request->member['mid']==$param['mid']){
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] = $memberInfo->sup_id ?? 0;
            }else{
                $param['mid'] =$param['mid'];
                //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] =  $memberInfo->sup_id ?? 0;
            }
//            //用户类型进行查找数据 1 普通用户  2 社群管理人员

        } else {
            $param['mid'] = $request->member['mid'];
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['user_type'] = $request->member['user_type'] ?? 2;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
//        $param['mid'] =167624;
        $data = AdminIndexService::getExclusiveDataStatistics($param,$request->member);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }





    /**
     * 查看当前的绑定的群主关系
     * @param Request $request
     */
    public  function seeNowBingData(Request $request){

        $param = $request->all();
        $param['mid'] = $request->member['mid'];
        $param['user_type'] = $request->member['user_type'];
        $param['sup_id'] = $request->member['sup_id'] ;
        if( $param['user_type']==2 && $request->member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff' ){
            $data = AdminIndexService::getDarenBingdData($param);
        }else if($param['user_type']==2&&$request->member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353'){
            $data = AdminIndexService::getBingdData($param);
        }else if($param['user_type']==2&&$request->member['platform_identity']=='75f712af51d952af3ab4c591213dea13'){
            $data = AdminIndexService::getZhidingBingdData($param);
        }else if($param['user_type']==2&&$request->member['platform_identity']=='183ade9bc5f1c4b52c16072362bb21d1'){
            $data = AdminIndexService::getMaituBingdData($param);
        }else if($param['user_type']==2){
            $data = AdminIndexService::getUniversalBingdData($request->member['platform_identity'],$param);
        }else{
            $data=[];
        }
        return response()->success($data);
    }

    /**
     * 根据手机号获取信息
     * @param Request $request
     */
    public function getMemInfoByMobile(Request $request)
    {
        $param = $request->all();
        $param['mid'] = $request->member['mid'];
        $param['user_type'] = $request->member['user_type'];
        $param['sup_id'] = $request->member['sup_id'] ;

        if ($param['user_type'] == 2 && $request->member['platform_identity'] == '89dafaaf53c4eb68337ad79103b36aff') {
            $data = AdminIndexService::getDarenMemInfoByMobile($param);
        } else if ($param['user_type'] == 2 && $request->member['platform_identity'] == '7323ff1a23f0bf1cda41f690d4089353' && isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getMemInfoByMobile($param);
        } else if ($param['user_type'] == 2 && $request->member['platform_identity'] == '75f712af51d952af3ab4c591213dea13'&& isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getZhidingMemInfoByMobile($param);
        } else if ($param['user_type'] == 2 && $request->member['platform_identity'] == '183ade9bc5f1c4b52c16072362bb21d1'&& isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getMaituMemInfoByMobile($param);
        } else if ($param['user_type'] == 2 && isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getUniversalInfoByMobile($request->member['platform_identity'],$param);
        }else {
            $data = [];
        }
        return response()->success($data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function  bindGroupMid(Request $request){
        $param = $request->all();
        $param['mid'] = $request->member['mid'];
        $param['user_type'] = $request->member['user_type'];
        $param['sup_id'] = $request->member['sup_id'] ;
        if( $param['user_type']==2 && $request->member['platform_identity']=='89dafaaf53c4eb68337ad79103b36aff' ){
            $data = AdminIndexService::bindDarenGroupMid($param);
        }else if( $param['user_type']==2 &&$request->member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353'&& isset($param['mobile'])&&strlen($param['mobile'])==11&&$param['id']>0){
            $data = AdminIndexService::bindGroupMid($param);
        }else if ($param['user_type'] == 2 && $request->member['platform_identity'] == '75f712af51d952af3ab4c591213dea13'&& isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getZhidingBindByMobile($param);
        }else if ($param['user_type'] == 2 && $request->member['platform_identity'] == '183ade9bc5f1c4b52c16072362bb21d1'&& isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getMaituBindByMobile($param);
        }else if ($param['user_type'] == 2 &&  isset($param['mobile']) && strlen($param['mobile']) == 11) {
            $data = AdminIndexService::getUniversalBindByMobile($request->member['platform_identity'] ,$param);
        }else{
            $data=[];
        }
        return response()->success($data);

    }


    public function changeGroupRobotStatus(Request $request)
    {
        $param = $request->all();
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);
            //$param['mid'] = 12334;
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['user_type'] = $memberInfo->user_type ?? 1;
            $param['sup_id'] = $memberInfo->sup_id ?? 0;
        } else {
            $param['mid'] = $request->member['mid'];
            //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
        $memberInfo = $request->member;
        $data = AdminIndexService::changeGroupRobotStatus($memberInfo, $param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
    * 用户后台群主数据统计
    */
    public function groupAdminStatistics(Request $request)
    {
        $param = $request->all();
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);

            if($request->member['mid'] != $param['mid']){
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] = $memberInfo->sup_id ?? 0;
            }else{
                $param['mid'] = $request->member['mid'];
                //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
                $param['user_type'] = $request->member['user_type'] ?? 1;
                $param['sup_id'] = $request->member['sup_id'] ?? 0;
            }
        } else {
            $param['mid'] = $request->member['mid'];
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
        
        $param['platform_identity'] = $request->member['platform_identity'];

        $data = AdminIndexService::groupAdminStatistics($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /*
    * YCloud管理后台群主列表
    */
    public function groupYCAdminStatistics(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::groupYCAdminStatistics($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }
    /**
     * 专属机器人群统计
     * @param Request $request
     * @return mixed
     */
    public function groupExclusiveAdminStatistics(Request $request)
    {
        $param = $request->all();
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);

            if($request->member['mid'] != $param['mid']){
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] = $memberInfo->sup_id ?? 0;
            }else{
                $param['mid'] = $request->member['mid'];
                //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
                $param['user_type'] = $request->member['user_type'] ?? 1;
                $param['sup_id'] = $request->member['sup_id'] ?? 0;
            }
        } else {
            $param['mid'] = $request->member['mid'];
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
        $param['platform_identity'] = $request->member['platform_identity'];
        $data = AdminIndexService::groupExclusiveAdminStatistics($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 查看群成员
     * @param Request $request
     * @return mixed
     */
    public function getGroupMemberList(Request $request)
    {
        $param = $request->all();
        $param['mid'] = $request->member['mid'];
        //用户类型进行查找数据 1 普通用户  2 社群管理人员
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $data = AdminIndexService::getGroupMemberList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 查看群成员
     * @param Request $request
     * @return mixed
     */
    public function getYCloudGroupMemberList(Request $request)
    {
        $param = $request->all();
        $data = AdminIndexService::getGroupMemberList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 修改群成员
     * @param Request $request
     * @return mixed
     */
    public function updateGroupMember(Request $request)
    {
        $param = $request->all();
        $memberInfo = $request->member;
        $data = AdminIndexService::updateGroupMember($param, $memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }



    /**
     * 修改群签到开关
     * @param Request $request
     * @return mixed
     */
    public function updateGroupSignSwitch(Request $request)
    {
        $param = $request->all();
        $param['mid'] = $request->member['mid'];
        //用户类型进行查找数据 1 普通用户  2 社群管理人员
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $data = AdminIndexService::updateGroupSignSwitch($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 群签到配置列表
     * @param Request $request
     * @return mixed
     */
    public function groupSignConfList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        $data = AdminIndexService::groupSignConfList($param, $memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 群签到配置列表
     * @param Request $request
     * @return mixed
     */
    public function groupSignConfDetail(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        $data = AdminIndexService::groupSignConfDetail($param, $memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 群签到配置添加
     * @param Request $request
     * @return mixed
     */
    public function insertGroupSignConf(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        $data = AdminIndexService::insertGroupSignConf($param, $memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 群签到配置删除
     * @param Request $request
     * @return mixed
     */
    public function deleteGroupSignConf(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        $data = AdminIndexService::deleteGroupSignConf($param, $memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }



    /**
     * 查找群成员
     * @param Request $request
     * @return mixed
     */
    public function searchGroupMember(Request $request)
    {
        $param = $request->all();
        $param['mid'] = $request->member['mid'];
        //用户类型进行查找数据 1 普通用户  2 社群管理人员
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $data = AdminIndexService::searchGroupMember($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    public function getUserOperationList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        //当前登录用户的用户信息
        //$memberInfo = $request->member;
        //$param['mid'] = 12334;
        //用户类型进行查找数据 1 普通用户  2 社群管理人员
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $param['sup_id'] = $request->member['sup_id'] ?? 0;
        $result = UserOperationLogService::getUserOperationList($memberInfo, $param);
        return response()->success($result);
    }


    public function getUserListByName(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        $result = UserOperationLogService::getUserListByName($memberInfo, $param);
        return response()->success($result);
    }

    public function getHouTaiUserListByName(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $result = UserOperationLogService::getHouTaiUserListByName($param);
        return response()->success($result);
    }

    /**
     * 获取群主分佣明细
     * @param Request $request
     * @return mixed
     */
    public function getGroupAdminCommission(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        return GroupManageService::getGroupAdminCommission($param,$memberInfo);
    }

    /**
     * 获取群主分佣明细
     * @param Request $request
     * @return mixed
     */
    public function getGroupAdminCommissionEveryDay(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        return GroupManageService::getGroupAdminCommissionEveryDay($param,$memberInfo);
    }


}