<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\AddAdminService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AdminRegisterController extends Controller
{
    private $validate = [
        'admin_name'      => 'required',//管理员名称
        'admin_email'     => 'required|email|unique:statistics.ylh_admin',//管理员邮箱
        'admin_password'  => 'required|min:6',//管理员密码
        'admin_type'      => 'required|max:1',//管理员类型   1、超级管理员   2、部门长    3、普通管理员
        'section_id'      => 'required|int',//所属部门
        'admin_login_type'=> 'required|max:1'//是否可以登录    1、可以登录     2、禁止登录
    ];

    /*
     * 管理员的添加
     */
    public function addAdmin(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $validator = Validator::make($param, $this->validate);

        //判断参数是否有误
        if($validator->fails()){
            return ['code' => 400, 'msg' => $this->formateError($validator->messages()->toArray())];
        }

        return AddAdminService::addAdmin($param);
    }

    private function formateError($error)
    {
        $keyName = array_keys($error)[0];
        return  $error[$keyName][0];
    }
}