<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\AdminUserService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    /*
     * 管理员的添加
     */
    public function addAdminUser(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->get('member');

        return AdminUserService::addAdminUser($memberInfo, $param);
    }

    public function getAdminUsers(Request $request)
    {
        $param = $request->all();

        return AdminUserService::getAdminUsers($param);
    }

    public function getAdminUser(Request $request)
    {
        $param = $request->all();

        return AdminUserService::getAdminUser($param);
    }

    public function getAdminListByName(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
        $result = AdminUserService::getAdminListByName($memberInfo, $param);
        return response()->success($result);
    }


    public function upAdminUser(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::upAdminUser($memberInfo, $param);
    }

    public function delAdminUser(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::delAdminUser($param);
    }




    public function getRoles(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getRoles($param);
    }

    public function getRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getRole($param);
    }

    public function addRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->get('member');
        return AdminUserService::addRole($memberInfo, $param);
    }

    public function upRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::upRole($param);
    }

    public function delRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::delRole($memberInfo, $param);
    }



    public function getUserRoles(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getUserRoles($param);
    }

    public function getUserRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getUserRole($param);
    }

    public function addUserRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::addUserRole($memberInfo, $param);
    }

    public function upUserRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::upUserRole($memberInfo, $param);
    }

    public function delUserRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::delUserRole($memberInfo, $param);
    }



    public function getPowers(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getPowers($param);
    }

    public function getPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getPower($param);
    }

    public function getUserPowerList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getUserPowerList($param);
    }

    public function getUserPowerInfo(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getUserPowerInfo($param);
    }

    public function addPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::addPower($param);
    }

    public function addUserPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::addUserPower($memberInfo, $param);
    }


    public function upPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();


        return AdminUserService::upPower($param);
    }

    public function delPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::delPower($memberInfo, $param);
    }

    public function getAdminOperationList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $result = AdminUserService::getAdminOperationList($param);
        return response()->success($result);
    }

    public function upUserPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::upUserPower($memberInfo, $param);
    }

    public function delUserPower(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::delUserPower($memberInfo, $param);
    }

    public function getTreePowers(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getTreePowers($param);
    }

    public function getUserTreePowers(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getUserTreePowers($param);
    }

    public function getUserPowers(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');

        return AdminUserService::getUserPowers($memberInfo,$param);
    }

    public function getUserPowersList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo = $request->member;
         if($memberInfo['sup_id']==0){
             return AdminUserService::getUserPowersList($memberInfo,$param);
         }else{
             return AdminUserService::getUserFrontPowersList($memberInfo,$param);
         }


    }

    public function getLevelPowers(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getLevelPowers($param);
    }

    public function getUserLevelPowers(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getUserLevelPowers($param);
    }


    public function getDepartments(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getDepartments($param);
    }

    public function getDepartment(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getDepartment($param);
    }

    public function addDepartment(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::addDepartment($memberInfo, $param);
    }

    public function upDepartment(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::upDepartment($memberInfo, $param);
    }

    public function delDepartment(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::delDepartment($memberInfo, $param);
    }



    public function getPositions(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getPositions($param);
    }

    public function getPosition(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getPosition($param);
    }

    public function addPosition(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::addPosition($memberInfo, $param);
    }

    public function upPosition(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::upPosition($memberInfo, $param);
    }

    public function delPosition(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo = $request->get('member');
        return AdminUserService::delPosition($memberInfo, $param);
    }

    public function getDepartmentsMenu(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getDepartmentsMenu($param);
    }


    public function getPositionsMenu(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return AdminUserService::getPositionsMenu($param);
    }

    /**
     * @Notes: 获取登陆人的角色类型信息
     * @Author: Yzk
     * @Date:2020/3/7 17:45
     * @param Request $request
     * @return array
     */
    public function getAdminUserRole(Request $request)
    {
        $param      = $request->all();
        $memberInfo = $request->get('member');

        return AdminUserService::getAdminUserRole($memberInfo,$param);
    }




















}