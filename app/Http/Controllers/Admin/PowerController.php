<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\PowerService;
use App\Service\Admin\AdminPowerNodeService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PowerController extends Controller
{
    /*
     * 获取所有权限列表
     */
    public function allPowerList()
    {
        return PowerService::getAllPower();
    }

    /*
     * 获取管理员的权限
     */
    public function getAdminPower()
    {
        //获取管理员的id
        $adminId = 1;

        return AdminPowerNodeService::getAdminPower($adminId);
    }

    /*
     * 添加管理员的权限
     */
    public function addAdminPower(Request $request)
    {
        //接收数据
        $param = $request -> All();

        //管理员权限添加
        return AdminPowerNodeService::addAdminPowerNode($param);
    }

    /*
     * 删除管理员的权限
     */
    public function deleteAdminPower(Request $request){
        //接收数据
        $param = $request -> All();

        return AdminPowerNodeService::deleteAdminPowerNode($param);
    }
}
