<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service\Admin\RobotServiceItemsService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RobotServiceController extends Controller
{
    private $validate = [
        'service_name'      => 'required',//服务项目名称
        'service_price'     => 'required|min:1',//服务项目价格
        'service_num'       => 'required|min:1',//购买时长
        'service_unit'      => 'required|max:1',//服务项目单位 1、年 2、月
        'group_number'      => 'required|min:1',//群数量
        'sort'              => 'required|int',//排序
    ];
    /*
     * 添加服务项目
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function addSerivce(Request $request)
    {
        $param = $request->all();

        $validator = Validator::make($param, $this->validate);

        //判断参数是否有误
        if($validator->fails()){
            return ['code' => 400, 'msg' => $this->formateError($validator->messages()->toArray())];
        }

        $memberInfo = $request->get('member');

        return RobotServiceItemsService::addSerivce($param,$memberInfo);
    }

    /*
     * 修改服务项目
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function updateSerivce(Request $request)
    {

        $param = $request->all();

        $validator = Validator::make($param, $this->validate);

        //判断参数是否有误
        if($validator->fails()){
            return ['code' => 400, 'msg' => $this->formateError($validator->messages()->toArray())];
        }

        $memberInfo = $request->get('member');

        return RobotServiceItemsService::updateSerivce($param,$memberInfo);
    }

    /*
     * 服务项目列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function SerivceList(Request $request)
    {
        $param = $request->all();

        $serivceList = RobotServiceItemsService::SerivceList($param);

        return $serivceList;
    }


    /*
     * 删除服务项目
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function delService(Request $request)
    {
        $param = $request->all();
        
        $serivceList = RobotServiceItemsService::delService($param);

        return $serivceList;
    }


     /*
     * 购买接入群订单展示
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getBuyGroupOrderList(Request $request)
    {
        $param = $request->all();
        
        $getBuyGroupOrderList = RobotServiceItemsService::getBuyGroupOrderList($param);

        return $getBuyGroupOrderList;
    }   


    private function formateError($error)
    {
        $keyName = array_keys($error)[0];
        return  $error[$keyName][0];
    }

}