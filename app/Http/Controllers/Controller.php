<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected static function responseJson($status = 400, $msg = "暂无数据", $data = []) {

        $retArr = ['code' => $status];
        $retData = ['msg', 'data', 'total', 'starttime', 'endtime'];
        foreach ($retData as $v) {
            if (isset($$v)) {
                $retArr[$v] = $$v;
            }
        }
        return $retArr;
    }
}
