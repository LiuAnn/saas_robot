<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/6/7
 * Time: 11:37
 */

namespace App\Http\Controllers\RobotOrder;

use App\Http\Controllers\Controller;
use App\Service\OrderService\MongondbService;
use Illuminate\Http\Request;
class MongodbController   extends Controller
{

    public function __construct()
    {

    }

    public   function MongodbTest(Request $request){

          MongondbService::mongoAdd($request);
//          MongondbService::mongoEdit($request);
           $res = MongondbService::mongoList($request);

           $returnData = json_decode(json_encode($res),true);

          return ['code'=>200,'mag'=>'success','data'=>$returnData];
    }



}