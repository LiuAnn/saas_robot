<?php
namespace App\Http\Controllers\RobotOrder;

use App\Http\Controllers\Controller;
use App\Service\Admin\UserService;
use App\Service\OrderService\GroupManageService;
use Illuminate\Http\Request;

class GroupManageController extends Controller
{
    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [
            'getGroupVerifyList','toExamineGroup','toTwiceExamineGroup','verifyGroupInfo', 'getTwiceGroupVerifyList','getTagData'
            ,'changeGroupTag','getCouponListStatistics','changeOrderBy'
        ]]);
    }
    /*
     * 查看审核群列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function getGroupVerifyList(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        //当前登录用户的用户信息
        //$memberInfo = $request->member;
        //$param['mid'] = 12334;
        //用户类型进行查找数据 1 普通用户  2 社群管理人员
        $param['user_type'] = $request->member['user_type'] ?? 1;
        $param['sup_id'] = $request->member['sup_id'] ?? 0;
        $param['platform_identity'] = $member['platform_identity'];
        return GroupManageService::getGroupVerifyList($param,$member);
    }

    /*
     * 查看二次升级审核群列表
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */

    public function getTwiceGroupVerifyList(Request $request)
    {

        $param = $request->all();

        $member =  $request->member;
        return GroupManageService::getTwiceGroupVerifyList($param, $member);
    }

    /*
     * 审核群
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function toExamineGroup(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::toExamineGroup($param,$member);

    }

    /**
     * 获取 标签内容  id
     * @return array
     */
    public function  getTagData(Request $request){
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::getTagData($param,$member);

    }


    /*
     * 二次审核群
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */
    public function toTwiceExamineGroup(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::toTwiceExamineGroup($param,$member);

    }


    /*
     * 群审核详情
     * @param Request $request
     * @throws ServicesException
     * @retuen array
     */    
    public function verifyGroupInfo(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::getGroupVerifyInfo($param,$member);

    }

    /**
     * 群标签更改
     */
    public function changeGroupTag(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::changeGroupTag($param,$member);
    }

    /**
     * 更新发送顺序
     */
    public function changeOrderBy(Request $request){
        $param = $request->all();
        $member =  $request->member;
        return GroupManageService::changeOrderBy($param,$member);
    }

    /**
     * 优惠券数据统计
     * @param Request $request
     */
    public  function    getCouponListStatistics(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        if (isset($param['mid']) && !empty($param['mid'])) {
            $memberInfo = UserService::getUserInfoById($param['mid']);

            if($request->member['mid'] != $param['mid']){
                $param['user_type'] = $memberInfo->user_type ?? 1;
                $param['sup_id'] = $memberInfo->sup_id ?? 0;
            }else{
                $param['mid'] = $request->member['mid'];
                //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
                $param['user_type'] = $request->member['user_type'] ?? 1;
                $param['sup_id'] = $request->member['sup_id'] ?? 0;
            }
        }
//        elseif (isset($param['phone']) && !empty($param['phone'])) {
//            $memberInfo = UserService::getUserInfoByPhone($param['phone']);
//            $param['mid'] = $memberInfo->mid ?? 0;
//            //用户类型进行查找数据 1 普通用户  2 社群管理人员
//            $param['user_type'] = $memberInfo->user_type ?? 1;
//            $param['sup_id'] = $memberInfo->sup_id ?? 0;
//        }
        else {
            $param['mid'] = $request->member['mid'];
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
        return GroupManageService::getCouponListStatisticsData($param,$member);
    }




}