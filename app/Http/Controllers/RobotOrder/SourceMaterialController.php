<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/15
 * Time: 20:50
 */

namespace App\Http\Controllers\RobotOrder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Service\Source\LotteryDrawService;

class SourceMaterialController  extends Controller
{

    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [
            'channelByMid','getCanUseTag','getMyRoomList',
            'getSourceMaterialList','getSourceMaterialInfo','getSourceMaterialInfo',
            'addSourceMaterial','upSourceMaterialInfo','delSourceMaterialInfoById',
            'fodderTypeList','linkTypeList','getTextUrlSeparate','getMiniAppList'
        ]]);
    }
    /**
     * 获取我的所有管理管理的群
     * @param Request $request
     * @return mixed
     */
    public function getMyRoomList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::getMyRoomListByMid($param,$member);
    }

    //社群发送的资料 类型列表
    public function fodderTypeList(Request $request)
    {
        $member= $request ->member;
        $data = LotteryDrawService::fodderTypeList($member);
        return response()->success($data);
    }


    //社群发送的资料 链接类型列表
    public function linkTypeList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request ->member;
        $data = LotteryDrawService::linkTypeList($member, $param);
        return response()->success($data);
    }

    //渠道筛选列表
    public function getSelectType(Request $request)
    {
        //接收数据  数据验证
        $data = LotteryDrawService::getSelectType();
        return response()->success($data);
    }


    //社群发送的资料 列表
    public function getSourceMaterialList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::getSourceMaterialList($param,$member);
    }


    //社群发送的资料 列表
    public function getSourceMaterialListV2(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        return LotteryDrawService::getSourceMaterialListV2($param);
    }

    //社群发送的小程序的模板列表
    public function getMiniAppList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::getMiniAppTemplateIdList();
    }

    //社群发送的资料  详情
    public function getSourceMaterialInfo(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::getSourceMaterialInfo($param,$member);
    }

    //社群发送的资料  详情
    public function getSourceMaterialInfoV2(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::getSourceMaterialInfoV2($param,$member);
    }


    //社群发送的资料  添加
    public function addSourceMaterial(Request $request)
    {
        //接收数据  数据验证
        $member= $request -> member;
        $param = $request -> All();
        return LotteryDrawService::addgetSourceMaterial($param,$member);
    }

    //社群发送的资料  添加
    public function addSourceMaterialV2(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        return LotteryDrawService::addgetSourceMaterialV2($param);
    }


    //社群发送的资料 修改
    public function upSourceMaterialInfo(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::upCommunitySendInfo($param,$member);
    }



    //社群发送的资料 修改
    public function upCommunitySendInfoV2(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $member= $request -> member;
        return LotteryDrawService::upCommunitySendInfoV2($param,$member);
    }


    public function delSourceMaterialInfoById(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request -> member;
        return LotteryDrawService::delCommunitySendInfo($param,$memberInfo);
    }


    /**
     * 获取可使用的标签
     * @param Request $request
     * @return array
     */
    public function getCanUseTag(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request ->member;
        return LotteryDrawService::getCanUseTag($param,$memberInfo);
    }


    public function getSendCouponList(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request ->member;
        return LotteryDrawService::getSendCouponList($param,$memberInfo);

    }

    public function sendCouponToGroupOwn(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request ->member;
        return LotteryDrawService::sendCouponToGroupOwn($param,$memberInfo);
    }

    /**
     * @param Request $request
     * @return
     */
    public function  channelByMid(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request ->member;

        return LotteryDrawService::getChannelData($memberInfo);

    }

    /**
     * 前端文本和url分离
     * @param Request $request
     * @return array
     */
    public function getTextUrlSeparate(Request $request){
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request ->member;
        return LotteryDrawService::getTextUrlSeparate($memberInfo,$param);

    }

}