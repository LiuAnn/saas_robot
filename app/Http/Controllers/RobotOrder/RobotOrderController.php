<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/14
 * Time: 21:02
 */

namespace App\Http\Controllers\RobotOrder;
use App\Http\Controllers\Controller;
use App\Service\OrderService\RobotOrderService;
use App\Service\OrderService\OrderActionService;
use App\Service\Community\GroupActivationService;

use Illuminate\Http\Request;
class RobotOrderController  extends Controller
{

    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [ 'getBuyInfoByMid','createOrder','getPayPargrams','getPayInfo','getOrderGoodsList']]);
    }
    public function test(){
        phpinfo();
    }
    /**
     * 用户登录以后 展示购买的信息
     * @param Request $request
     * @return mixed
     */
    public  function   getBuyInfoByMid(Request $request)
    {
        $param = $request->all();
        //查询
        $member =  $request->member;
        $returnData =   RobotOrderService::getServiceList($param,$member);

        return  self::responseJson(200,'获取成功',$returnData);
    }

    /**
     * @param Request $request
     */
    public function  addBrowerData(Request $request){

        $param = $request->all();
        //查询  用户名和密码是否正确
        GroupActivationService::addGoodsInfo($param);
    }

    /**
     *
     */
    public  function  getRoomIdByMid(Request $request){
        $param = $request->all();
        //查询  用户名和密码是否正确
       return    RobotOrderService::getRoomMid($param);
    }

    /**
     *
     */
    public  function  getMidByRoomId(Request $request){
        $param = $request->all();
        //查询  用户名和密码是否正确
        return    RobotOrderService::getMid($param);
    }

    /**
     * 用户购买订单
     * @param Request $request
     * @return mixed
     */
    public function createOrder(Request $request)
    {
        $param = $request->all();
        //查询  用户名和密码是否正确
        if (empty($param['mid']) || empty($param['service_id']) || empty($param['pay_amount'])) {
            return self::responseJson(400, '网络错误', []);
        }
        //创建订单
        $returnData = OrderActionService::robotInitialization($param);

        return  $returnData;
    }

    /**
     * 用户购买订单
     * @param Request $request
     */
    public  function  getPayPargrams(Request $request)
    {
        $param = $request->all();
        //查询  用户名和密码是否正确
        if (empty($param['mid']) || empty($param['order_sn'])) {
            return self::responseJson(400, '网络错误', []);
        }
        //创建订单
        $returnData = OrderActionService::getPayPargrams($param);

        return [
            "code"    => 200,
            "msg"     => "可支付订单",
            "ordersn" => $param['order_sn'],
            "pay"     => (array)$returnData
        ];

       // return  self::responseJson(200,'可支付订单',$returnData);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getPayInfo(Request $request)
    {
        $param = $request->all();
        //查询  用户名和密码是否正确
        if ( empty($param['order_sn'])) {
            return self::responseJson(400, '网络错误', []);
        }
        //创建订单
        $payStatus = RobotOrderService::getPaySatus($param['order_sn']);
        return [
            "code" => 200,
            "msg" => "success",
            "status" => $payStatus,
        ];

    }



    /**
     * 支付回调
     * @param Request $request
     * @author  liujiaheng
     * @created 2020/4/17 4:25 上午
     */
    public function callback(Request $request)
    {
        $param = $request->all();

        // 记录初始回调参数
        \Log::info('robot callback first params', [$param]);

        $mchId = $param["mchId"];
        $notifyType = $param["notifyType"];
        $body = $param["body"];

        $data = [
            "mchId" => $mchId,
            "notifyType" => $notifyType,
            "body" => $body
        ];
        $result = RobotOrderService::analysis($data);

        \Log::info("mall notify:" . json_encode($result));

        if ($result["notifyType"] == "PAY") {
            RobotOrderService::updateOrderStatus($result);
        }
    }


    /**
     * 购买的订单商品类型列表
     */
    public  static  function getOrderGoodsTypeList()
    {
        $data = RobotOrderService::getOrderGoodsTypeList();
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 购买的上品订单列表
     */
    public  static  function getOrderGoodsList(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        $getOrderGoodsData = RobotOrderService::getOrderGoodsList($param, $member);
        return [
            "code" => 200,
            "msg" => "success",
            "data" => $getOrderGoodsData,
        ];
    }



    /**
     * 获取群内购买商品的订单列表
     */
    public static function getGroupOrderList(Request $request)
    {
        $param = $request->all();
        return RobotOrderService::getGroupOrderList($param);

    }

}
