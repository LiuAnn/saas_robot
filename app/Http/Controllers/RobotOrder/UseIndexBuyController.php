<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/19
 * Time: 20:24
 */

namespace App\Http\Controllers\RobotOrder;

use App\Http\Controllers\Controller;
use App\Models\ChannelData;
use App\Service\Admin\UserService;
use Illuminate\Http\Request;
use  App\Service\Community\UseIndexService;

class UseIndexBuyController   extends Controller
{
    /**
     * UseIndexBuyController constructor.
     */
    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [ 'isOrderExists','getRobotInfo','getRobotStatistics','getExclusiveRobotInfo',
            'getUserRolesByMid','addUserFrontRole','upUserFrontRole','delUserFrontRole','getFrontRole','createFrontUser','getRobotWxAliasList','getGroupAllList']]);
    }
    /**
     * 判断用户是否购买 接入群的数据
     * @param Request $request
     */
    public  function  isOrderExists(Request $request)
    {
       return  UseIndexService::changeIsBuyService($request->member);
    }

    /**
     * 获取消息分类
     */
    public function  getSourceCategory(Request $request)
    {
        $page    = !empty($request["page"]) ? $request["page"] : 1;
        $pageSize = !empty($request["pageSize"]) ? $request["pageSize"] : 20;
        return  UseIndexService::getSourceListByPage($request->member,$page,$pageSize);
    }
    /**
     * 我的机器人列表
     *
     */
     public function getRobotInfo(Request $request)
     {
         $param = $request->all();
         $member =  $request->member;
         //当前登录用户的用户信息
         //$memberInfo = $request->member;
         //$param['mid'] = 12334;
         //用户类型进行查找数据 1 普通用户  2 社群管理人员
         if (isset($param['mid']) && !empty($param['mid'])) {
             $member = UserService::getUserInfoById($param['mid']);
             //$param['mid'] = 12334;
             //用户类型进行查找数据 1 普通用户  2 社群管理人员
             $param['user_type'] = $member->user_type ?? 1;
             $param['sup_id'] = $member->sup_id ?? 0;
             $member = json_decode(json_encode($member), true);
         } else {
             $param['mid'] = $request->member['mid'];
             //用户类型进行查找数据 1 普通用户  2 社群管理人员 3 专属机器人
             $param['user_type'] = $request->member['user_type'] ?? 1;
             $param['sup_id'] = $request->member['sup_id'] ?? 0;
         }
         return  UseIndexService::getRoBotInfo($param,$member);
     }

    /**
     * 我的机器人列表
     *
     */
    public function getRobotList(Request $request)
    {
        $param = $request->all();
        $data = UseIndexService::getRobotList($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 我的机器人列表
     *
     */
    public function updateRobotStatus(Request $request)
    {
        $param = $request->all();
        $data = UseIndexService::updateRobotStatus($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    /**
     * 平台标识列表
     * @param Request $request
     * @return mixed
     */
    public function getChannelData(Request $request)
    {
        $data = UseIndexService::getChannelDataList();
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /**
     * 添加新的机器人信息
     * @param Request $request
     * @return mixed
     */
    public function insertRobotInfo(Request $request)
    {
        $params = $request->all();
        $data = UseIndexService::insertRobotInfo($params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

     //专属机器人列表
    public function getExclusiveRobotInfo(Request $request)
    {
        $param = $request->all();
        $member =  $request->member;
        if (isset($param['mid']) && !empty($param['mid'])) {
            $member = UserService::getUserInfoById($param['mid']);
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['user_type'] = $member->user_type ?? 1;
            $param['sup_id'] = $member->sup_id ?? 0;
            $member = json_decode(json_encode($member), true);
        } else {
            $param['mid'] = $request->member['mid'];
            //用户类型进行查找数据 1 普通用户  2 社群管理人员
            $param['user_type'] = $request->member['user_type'] ?? 1;
            $param['sup_id'] = $request->member['sup_id'] ?? 0;
        }
        return  UseIndexService::getExclusiveRoBotInfo($param,$member);
    }
    /**
     * 统计数据
     * @param Request $request
     */
     public  function  getRobotStatistics(Request $request)
     {
         $param = $request->all();
         $member =  $request->member;
         return  UseIndexService::getRobotStatisticsByRobotId($param,$member);

     }
     // 获取角色列表
    public function getUserRolesByMid(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $member =   $request -> member;

        return UseIndexService::getUserRoles($param,$member);
    }


    public function addUserFrontRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request -> member;

        return UseIndexService::addUserRoleFront($memberInfo, $param);
    }


    public function upUserFrontRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo= $request -> member;
        return UseIndexService::upUserRoleFront($memberInfo, $param);
    }


    public function delUserFrontRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();

        $memberInfo= $request -> member;
        return UseIndexService::delUserRoleFront($memberInfo, $param);
    }


    public function getFrontRole(Request $request)
    {
        //接收数据  数据验证
        $param = $request -> All();
        $memberInfo= $request -> member;
        return UseIndexService::getFrontRole($param,$memberInfo);
    }


    /**
      创建用户
     */
    public function createFrontUser(Request $request){
        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::createFrontUser($data,$memberInfo);
        return $result;
    }

   //获取待绑定的微信号
    public function getRobotWxAliasList(Request $request)
    {
        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::getRobotWxAliasList($data,$memberInfo);
        return $result;
    }

    /**
     * 获取管理列表
     * @param Request $request
     * @return false|string
     */
    public function getUserListByMid(Request $request)
    {
        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::getUserListByMid($data,$memberInfo);
        return $result;
    }


    public function delFrontUser(Request $request){

        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::delFrontUser($data,$memberInfo);
        return $result;
    }
    /**
     * 获取详细
     * @param Request $request
     * @return array|false|string
     */
    public function getUserInfoId(Request $request){
        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::getFrontUserInfo($data,$memberInfo);
        return $result;
    }


    public function upFrontUser(Request $request){

        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::upFrontUserData($data,$memberInfo);
        return $result;
    }

    /**
     * 获取所有的群组
     * @param Request $request
     * @return array|false|string
     */
    public static  function  getGroupAllList(Request $request){
        $data = $request->all();
        $memberInfo= $request -> member;
        $result = UseIndexService::getGroupAllList($data,$memberInfo);
        return $result;
    }

}