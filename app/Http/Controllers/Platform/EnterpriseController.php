<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/11/12
 * Time: 下午05:43
 * Describe: 易云平台企业微信
 */

namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use App\Service\Admin\UserService;
use App\Service\Platform\EnterpriseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Models\YiyunCallback;
use Exception;

class EnterpriseController extends Controller
{
    //商家秘钥
    protected $SECRET = '6psqdiILsT3dxRc3zbvOkwNUvgTVCU3Q';

    /**
     * 回调接口
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callBack(Request $request)
    {
        error_log('请求内容：' . json_encode($_POST) . "\n", 3, storage_path('logs/enterprise_callback.log'));

        //回调状态
        $nType = $request->input('type', '');
        $strSign = $request->input('strSign', '');
        if (empty($nType)) {
            return response()->json(['code' => 401, 'msg' => '', 'data' => []]);
        }

        //回调内容
        $strContext = $request->input('strContext', '');
        $data = json_decode($strContext, true);

        //验证签名
//        $is_sign = self::checkSign($strSign, $strContext);
//        if (empty($is_sign)) {
//            error_log('签名验证失败：' . json_encode($_POST) . "\n", 3, storage_path('logs/callback-error.log'));
//            return response()->json(['code' => 402, 'msg' => '', 'data' => []]);
//        }

        //操作编号
        $vcSerialNo = empty($data['serial_no']) ? '' : $data['serial_no'];
        //商家编号
        $merchant_no = empty($data['merchant_no']) ? '' : $data['merchant_no'];
        //机器人编号
        $robot_no = empty($data['robot_serial_no']) ? '' : $data['robot_serial_no'];

        $result = false;
        switch ($nType) {
            //登陆成功
            case 500002:
                $result = EnterpriseService::loginSuccess($data);
                break;
            //创建外部群
            case 300026:
                error_log('创建外部群回调' . json_encode($data) . "\n", 3, storage_path('logs/enterprise_callback.log'));
                break;
            //获取我的客户列表
            case 20001:
                $result = EnterpriseService::setMyCustomers($data);
                break;
            //获取保存在通讯录中群列表
            case 300007:
                # code...
                break;
            //机器人被踢出群
            case 300024:
                # code
                break;
            //新成员入群
            case 300029:
                $result = EnterpriseService::intoGroup($data);
                break;
            //群成员退群
            case 300030:
                $result = EnterpriseService::exitGroup($data);
                break;
            default :
                # code...
                break;
        }

        if (!$result) {
            $insert = [
                'wxid' => '',
                'nType' => $nType,
                'merchant_no' => $merchant_no,
                'robot_no' => $robot_no,
                'serial_no' => $vcSerialNo,
                'platform' => 1,
                'data' => json_encode($data),
                'status' => 0
            ];
        } else {
            $insert = [
                'wxid' => '',
                'nType' => $nType,
                'merchant_no' => $merchant_no,
                'robot_no' => $robot_no,
                'serial_no' => $vcSerialNo,
                'platform' => 1,
                'data' => json_encode($data),
            ];
        }
        //插入回调记录表
        YiyunCallback::insert($insert);
        return response()->json(['code' => 200, 'message' => '', 'data' => []]);
    }

    /**
     * 商家机器人登陆
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            //渠道标识
            $platform_identity = $request->member['platform_identity'];
            $mid = $request->input('mid', '');
            if (empty($mid)) {
                throw new Exception('mid is null');
            }
            if (empty($platform_identity)) {
                $memberInfo = UserService::getUserInfoById($mid);
            }
            $platform_identity = $memberInfo->platform_identity ?? '';
            if (!$platform_identity) {
                throw new Exception ('params is null');
            }

            $login_info = EnterpriseService::login($platform_identity);
            return response()->json(array('code' => 200, 'message' => '登陆成功', 'data' => ['serial_no' => $login_info['serial_no']]))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }

    /**
     * 机器人登出
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logOut(Request $request)
    {
        try {
            $wx_alias = $request->input('wx_alias', '');//微信号
            if (!$wx_alias) {
                throw new Exception ('params is null', 400);
            }

            $login_info = EnterpriseService::logOut($wx_alias);
            return response()->json(array('code' => 200, 'message' => '登出成功', 'data' => ['serial_no' => $login_info['serial_no']]))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }

    /**
     * 获取登陆回调的最新状态
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCallBackStatus(Request $request)
    {
        //操作编号`
        $serial_no = $request->input('serial_no', '');
        $type = $request->input('type', '');
        if (empty($serial_no)) {
            return response()->json(['code' => 1, 'message' => '参数错误', 'data' => ['status' => '']]);
        }
        $data = YiyunCallback::where('serial_no', $serial_no)
            ->where('merchant_no', '!=', '')
            ->orderBy('created_at', 'DESC')
            ->first();
        if (empty($data)) {
            return response()->json(['code' => 200, 'message' => '', 'data' => []]);
        }
        $retData = [
            'status' => $data['nType'],
            'serial_no' => $data['serial_no'],
            'data' => json_decode($data['data']),
        ];

        return response()->json(['code' => 200, 'message' => '', 'data' => $retData]);
    }


    /**
     * 创建外部群
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCustomerGroup(Request $request)
    {
        try {
            $wx_alias = $request->input('wx_alias', '');//微信号
            $message = $request->input('message', '');//微信号
            if (!$wx_alias) {
                return response()->json(['code' => 1, 'message' => 'wx_alias不能为空', 'data' => []])->setCallback();
            }

            $result_info = EnterpriseService::CreateCustomerGroup($wx_alias, $message);
            return response()->json(array('code' => 200, 'message' => '创建成功', 'data' => $result_info))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }

    /**
     * 获取我的客户列表
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyCustomers(Request $request)
    {
        try {
            $wx_alias = $request->input('wx_alias', '');//微信号
            if (!$wx_alias) {
                return response()->json(['code' => 1, 'message' => 'wx_alias不能为空', 'data' => []])->setCallback();
            }

            $result_info = EnterpriseService::getMyCustomers($wx_alias);
            return response()->json(array('code' => 200, 'message' => '获取成功', 'data' => $result_info))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }

    /**
     * 获取保存在通讯录中群列表
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getContactGroups(Request $request)
    {
        try {
            $wx_alias = $request->input('wx_alias', '');//微信号
            if (!$wx_alias) {
                return response()->json(['code' => 1, 'message' => 'wx_alias不能为空', 'data' => []])->setCallback();
            }

            $result_info = EnterpriseService::getContactGroups($wx_alias);
            return response()->json(array('code' => 200, 'message' => '获取成功', 'data' => $result_info))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }

    /**
     * 获取机器人开关状态接口
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRobotConfigs(Request $request)
    {
        try {
            $wx_alias = $request->input('wx_alias', '');//微信号
            if (!$wx_alias) {
                return response()->json(['code' => 1, 'message' => 'wx_alias不能为空', 'data' => []])->setCallback();
            }

            $result_info = EnterpriseService::getRobotConfigs($wx_alias);
            return response()->json(array('code' => 200, 'message' => '获取成功', 'data' => $result_info))->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('code' => $exCode, 'message' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }


    /**
     * 测试消息发送
     *
     * @param Request $request
     */
    public function sendInfo(Request $request)
    {
        $robot_serial_no = $request->input('robot_serial_no', '');//机器人编号
        $group_serial_no = $request->input('group_serial_no', '');//群编号
        $message = $request->input('message', '');//消息内容
        $data = [
            'msg_num' => 1,
            'msg_type' => 2001,
            'msg_content' => $message,
            'voice_time' => 0,
            'href' => '',
            'title' => '',
            'desc' => ''
        ];
        $foo = EnterpriseService::sendInfo($robot_serial_no, $group_serial_no, $data);
        print_r($foo);
    }
}










