<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/08/31
 * Time: 上午11:43
 * Describe: 易云平台
 */
namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use App\Service\Platform\YiyunService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Models\YiyunCallback;
use Exception;

class YiyunController extends Controller
{
    //access_token
    protected $access_token = 'yiyun:access:token';
    //授权地址
    protected $AUTHORIZED_URL = 'https://merchant.auth.tusepaas.com:8091/';
    //服务地址
    protected $SERVICE_URL = 'https://merchant.wapi.tusepaas.com:8091/';
    //商家编号
    protected $MERCHANT = '202010280000026';
    //商家秘钥
    protected $SECRET = 'b0a91e0471034c358aad3bed6f32ef5b';

    /**
     * 回调接口
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callBack(Request $request)
    {
        error_log('请求内容：' . json_encode($_POST) . "\n", 3, storage_path('logs/callback.log'));
        //回调状态
        $nType = $request->input('nType', '');
        $strSign = $request->input('strSign', '');
        if (empty($nType)) {
            return response()->json(['code' => 401, 'msg' => '', 'data' => []]);
        }

        //回调内容
        $strContext = $request->input('strContext', '');
        $data = json_decode($strContext, true);

        //验证签名
        $is_sign = self::checkSign($strSign, $strContext);
        if (empty($is_sign)) {
            error_log('签名验证失败：' . json_encode($_POST) . "\n", 3, storage_path('logs/callback-error.log'));
            return response()->json(['code' => 402, 'msg' => '', 'data' => []]);
        }

        //操作编号
        $vcSerialNo = empty($data['vcSerialNo']) ? '' : $data['vcSerialNo'];
        //wxid
        $vcRobotWxId = empty($data['vcRobotWxId']) ? '' : $data['vcRobotWxId'];
        //商家编号
        $merchant_no = empty($data['vcMerchantNo']) ? '' : $data['vcMerchantNo'];
        //机器人编号
        $robot_no = empty($data['vcRobotSerialNo']) ? '' : $data['vcRobotSerialNo'];

        $result = false;
        switch ($nType) {
            //登陆成功
            case 1003:
                $result = YiyunService::loginSuccess($data);
                break;
            //登出成功
            case 1005:
                $result = YiyunService::logout($data);
                break;
            //请求添加机器人好友回调
            case 3003:
                $result = YiyunService::adoptFriend($data);
                break;
            //上报机器人好友列表的同时上报群信息
            case 3022:
                $result = YiyunService::setFriendAndGroupList($data, $vcRobotWxId);
                break;
            //群成员信息获取
            case 4501:
                $result = YiyunService::getGroupUserInfo($data);
                break;
            //新成员入群
            case 4502:
                $result = YiyunService::getIntoGroup($data);
                break;
            //群成员退群
            case 4503:
                $result = YiyunService::exitGroup($data);
                break;
            //机器人被好友拉群
            case 4505:
                $result = YiyunService::robotIntoGroup($data);
                break;
            //机器人被好友拉群（需要手动通过场景）
            case 4506:
                $result = YiyunService::robotPullGroupAdopt($data);
                break;
            //机器人踢出群
            case 4507:
                $result = YiyunService::kickOutGroup($data);
                break;
            //创建群 （暂时没有机器人创建群需求，也许后去会有）
            case 4508:
                break;
            //群内实时消息回调
            case 5003:
                $result = YiyunService::reportMessageMsg($data);
                break;
            default:
                break;
        }


        if (!$result) {
            $insert = [
                'wxid' => $vcRobotWxId,
                'nType' => $nType,
                'merchant_no' => $merchant_no,
                'robot_no' => $robot_no,
                'serial_no' => $vcSerialNo,
                'data' => json_encode($data),
                'status' => 0
            ];
        } else {
            $insert = [
                'wxid' => $vcRobotWxId,
                'nType' => $nType,
                'merchant_no' => $merchant_no,
                'robot_no' => $robot_no,
                'serial_no' => $vcSerialNo,
                'data' => json_encode($data),
            ];
        }
        //插入回调记录表
        YiyunCallback::insert($insert);
        return response()->json(['code' => 200, 'msg' => '', 'data' => $data['Data']]);
    }


    /**
     * 商家机器人登陆
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $wx_alias = $request->input('wx_alias', '');//微信号
            $nRegionCode = $request->input('regionCode', 110000);//扫码设备的地区编码:
            if (!$wx_alias) {
                throw new Exception ('params is null');
            }

            $login_info = YiyunService::login($wx_alias, $nRegionCode);
            return response()->json(array('error_code' => 0, 'error_reason' => '登陆成功', 'data' => ['serial_no' => $login_info['vcSerialNo']]))->setCallback();

        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(array('error_code' => $exCode, 'error_reason' => $e->getMessage(), 'data' => []))->setCallback();
        }
    }

    /**
     * 获取登陆回调的最新状态
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCallBackStatus(Request $request)
    {
        //操作编号
        $serial_no = $request->input('serial_no', '');
        if (empty($serial_no)) {
            return response()->json(['error_code' => 1, 'error_reason' => '参数错误', 'data' => ['status' => '']]);
        }
        $data = YiyunCallback::where('serial_no', $serial_no)->orderBy('created_at', 'DESC')->first();
        if (empty($data)) {
            return response()->json(['error_code' => 0, 'error_reason' => '', 'data' => ['status' => '']]);
        }
        $retData = [
            'status' => $data['nType'],
            'serial_no' => $data['serial_no'],
            'data' => json_decode($data['data']),
        ];

        return response()->json(['error_code' => 0, 'error_reason' => '', 'data' => $retData]);
    }

    /**
     * 验证回调签名，确保数据来源合法
     *
     * @param $sign
     * @param $data
     * @return bool
     */
    public function checkSign($sign, $data)
    {
        $md5_encryption = md5($data . $this->SECRET);
        if ($sign != $md5_encryption) {
            return false;
        }
        return true;
    }

    public function getCity()
    {
        //获取城市列表
    }

    /**
     * 获取用户好友和群信息（好友信息&&群列表信息 写了暂时没用到，后面也许会用）
     *
     */
    public function getUserInfoDetail(Request $request)
    {
        $merchantNo = $this->MERCHANT;
        $robot_serinal_no = $request->input('robot_serinal_no');
        if (empty($robot_serinal_no)) {
            return response()->json(['error_code' => 1, 'error_reason' => '参数错误', 'data' => []]);
        }

        $user_group_data = YiyunService::getGroup($merchantNo, $robot_serinal_no, '', 0);


        $user_friend_list = YiyunService::getMemberList($merchantNo, $robot_serinal_no, '', 0);

        $retData['group_list'] = $user_group_data;
        $retData['friend_list'] = $user_friend_list;

        return response()->json(['error_code' => 0, 'error_reason' => '', 'data' => $retData]);
    }


}










