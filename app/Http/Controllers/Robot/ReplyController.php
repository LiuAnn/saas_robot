<?php

namespace App\Http\Controllers\Robot;

use App\Service\Robot\ReplyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReplyController extends Controller
{
    public function getKeyWordsList(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        return ReplyService::getKeyWordsList($params);
    }

    public function getKeyWordsDetail(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        return ReplyService::getKeyWordsDetail($params);
    }

    public function addKeyWords(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        return ReplyService::addKeyWords($memberInfo, $params);
    }

    public function addKeyWordsReply(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        return ReplyService::addKeyWordsReply($memberInfo, $params);
    }

    public function updateKeyWords(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return ReplyService::updateKeyWords($memberInfo, $params);
    }


    public function updateKeyWordsReply(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return ReplyService::updateKeyWordsReply($memberInfo, $params);
    }

    public function deleteKeyWords(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return ReplyService::deleteKeyWords($memberInfo, $params);
    }
    public function deleteKeyWordsReply(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return ReplyService::deleteKeyWordsReply($memberInfo, $params);
    }


    public function keyWordsReplyList(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        return ReplyService::keyWordsReplyList($params);
    }
}
