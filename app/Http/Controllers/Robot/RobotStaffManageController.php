<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/7/8
 * Time: 14:57
 */

namespace App\Http\Controllers\Robot;


use App\Service\Robot\UnitManageService;
use App\Service\Community\PullTaskRedisDataService;
use App\Service\Member\MemberService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RobotStaffManageController   extends Controller
{

    protected $params;
    public function __construct(Request $request)
    {
        $this->params = $request->all();
        $this->member = $request->member;
    }
    /**
     * 分队列表
     */
    public  function  getGroupListData(Request $request){
      $memberInfo =  $request->member;
      return    UnitManageService::getGroupListData( $this->params,$memberInfo);
    }
    /**
     * 获取待绑定的管理员
     * @param Request $request
     */
    public function  getNotBindMemberData(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::getNotBindMemberData( $this->params,$memberInfo);
    }
    /**
     * 添加
     * @param Request $request
     * @return mixed
     */
    public function addGroupList(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::addData( $this->params,$memberInfo);
    }
    /**
     * 获取详细
     */
    public function getGroupInfo(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::getGroupInfoById( $this->params,$memberInfo);
    }

    /**
     * 更新
     */
    public function upGroupInfo(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::upGroupInfo( $this->params,$memberInfo);
    }
    /**
     * 删除
     */
    public function delGroupInfo(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::delGroupInfo( $this->params,$memberInfo);
    }

    /**
     * 解散
     */
    public  function  disbandGroup(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::disbandGroup( $this->params,$memberInfo);
    }

    /**
     * 直播社群统计
     */
    public function getLiveList(Request $request){
        $memberInfo =  $request->member;
        return    UnitManageService::getLiveList( $this->params,$memberInfo);        
    }


    public function getJdOrderList(){

        PullTaskRedisDataService::insertTripartiteOrderData(2,9);
      //MemberService::userBonusBecommitted(['ordersn'=>'11715997645808973','order_status'=>1]);
    }

}