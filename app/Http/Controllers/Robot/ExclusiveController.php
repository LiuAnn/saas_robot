<?php

namespace App\Http\Controllers\Robot;

use App\Service\Robot\ExclusiveApplicationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExclusiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('user.check.login', ['only' => [
            'addExclusiveApplication', 'getExclusiveApplicationList','delapplicationByid'
        ]]);
    }

    public function getExclusiveApplicationList(Request $request)
    {
        $params = $request->all();
        $params['mid'] = $request->member['mid'];
        $applicationList = ExclusiveApplicationService::getExclusiveApplicationList($params);
        return response()->success($applicationList);
    }

    public function addExclusiveApplication(Request $request)
    {
        $params = $request->all();
        $params['mid'] = $request->member['mid'];
        $params['platform_identity'] =  $request->member['platform_identity'];
        $params['manage_phone'] =  $request->member['manage_phone'];
        return ExclusiveApplicationService::addExclusiveApplication($params);
    }

    public function updateExclusiveApplication(Request $request)
    {
        $params = $request->all();
        return ExclusiveApplicationService::updateExclusiveApplication($params);
    }

    public function delapplicationByid(Request $request){
        $params = $request->all();
        $params['mid'] = $request->member['mid'];
        return ExclusiveApplicationService::updateExclusiveApplication($params);
    }

    public function getBrandDataApplication(Request $request){
        $params = $request->all();
        return ExclusiveApplicationService::getBrandDataApplication($params);
    }

    /**
     * 专属机器人添加
     * @param Request $request
     * @return array
     */
    public function  addExclusiveRobot(Request $request){
        $params = $request->all();
        $params['platform_identity'] =  isset($params['source']) ?$params['source'] :'';
        $params['user_wx'] =  isset($params['user_wx']) ?$params['user_wx'] :'';
        $params['phone'] =  isset($params['phone']) ?$params['phone'] :'';
        $params['mid'] =  isset($params['mid']) ?$params['mid'] :'';
        $params['manage_phone'] =  isset($params['manage_phone']) ?$params['manage_phone'] :'';
        if(empty($params['mid'])||empty( $params['source'])||empty( $params['user_wx'])){
            return array('code'=>400,'msg'=>'必传参数来源微信号','data'=>[]);
        }else{
            return ExclusiveApplicationService::addExclusiveRobot($params);
        }
    }


}
