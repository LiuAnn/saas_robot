<?php

namespace App\Http\Controllers\Robot;

use App\Service\Robot\KeyWordService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KeyWordController extends Controller
{
    public function getKeyWordsList(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        $data = KeyWordService::getKeyWordsList($params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    public function keyWordsDetail(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        } else {
            return response()->success([]);
        }
        $data = KeyWordService::keyWordsDetail($params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    public function addKeyWords(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        return KeyWordService::addKeyWords($memberInfo, $params);
    }


    /**
     *
     * @param Request $request
     * @return array
     */
    public function deleteKeyWords(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        return KeyWordService::deleteKeyWords($memberInfo, $params);
    }
}
