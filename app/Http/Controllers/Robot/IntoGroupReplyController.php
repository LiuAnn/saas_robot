<?php

namespace App\Http\Controllers\Robot;

use App\Model\Robot\IntoGroupReplyModel;
use App\Service\Robot\IntoGroupReplyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IntoGroupReplyController extends Controller
{


    public function intoGroupReplyStyleList()
    {
        return response()->success(IntoGroupReplyModel::STYLE_LIST);
    }


    public function intoGroupReplyTypeList()
    {
        return response()->success(IntoGroupReplyModel::TYPE_LIST);
    }

    public function updateIntoGroupReplyTag(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        $data = IntoGroupReplyService::updateIntoGroupReplyTag($memberInfo, $params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    public function intoGroupReplyList(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        $data = IntoGroupReplyService::intoGroupReplyList($params,$memberInfo);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    public function addIntoGroupReply(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        $data = IntoGroupReplyService::addIntoGroupReply($memberInfo, $params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    public function deleteIntoGroupReply(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        $data = IntoGroupReplyService::deleteIntoGroupReply($memberInfo, $params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    public function updateIntoGroupReply(Request $request)
    {
        $params = $request->all();

        $memberInfo = $request->member;
        $data = IntoGroupReplyService::updateIntoGroupReply($memberInfo, $params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }



    public function addIntoGroupGreetingInfo(Request $request)
    {
        $params = $request->all();
        if ($request->member['platform_identity']) {
            $params['platform_identity'] = $request->member['platform_identity'];
        }
        $memberInfo = $request->member;
        $data = IntoGroupReplyService::addIntoGroupGreetingInfo($memberInfo, $params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }


    public function getIntoGroupGreetingInfo(Request $request)
    {
        $params = $request->all();
        $memberInfo = $request->member;
        $data = IntoGroupReplyService::getIntoGroupGreetingInfo($memberInfo, $params);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }
}
