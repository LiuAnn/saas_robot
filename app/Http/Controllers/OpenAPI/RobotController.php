<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 2021/1/21
 * Time: 下午5:09
 * Describe: 机器人开放接口-机器人相关
 */

namespace App\Http\Controllers\OpenAPI;

use App\Service\OpenAPI\Robot\RobotService;
use Exception;
use Illuminate\Http\Request;

class RobotController
{
    /**
     * 获取机器人信息
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getInfo(Request $request)
    {
        try {
            $robotSerialNo = $request->input('robotSerialNo', '');
            $retData = RobotService::getInfo($robotSerialNo);
            return response()->json(['code' => 200, 'msg' => 'success', 'data' => $retData])->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(['code' => $exCode, 'msg' => $e->getMessage(), 'data' => []])->setCallback();
        }
    }

    /**
     * 修改昵称
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function setNickname(Request $request)
    {
        try {
            $robotSerialNo = $request->input('robotSerialNo', '');
            $nickname = $request->input('nickname', '');
            $retData = RobotService::setNickname($robotSerialNo, $nickname);
            return response()->json(['code' => 200, 'msg' => 'success', 'data' => $retData])->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(['code' => $exCode, 'msg' => $e->getMessage(), 'data' => []])->setCallback();
        }
    }

    /**
     * 修改头像
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function setAvatar(Request $request)
    {
        try {
            $robotSerialNo = $request->input('robotSerialNo', '');
            $imageUrl = $request->input('imageUrl', '');
            $retData = RobotService::setAvatar($robotSerialNo, $imageUrl);
            return response()->json(['code' => 200, 'msg' => 'success', 'data' => $retData])->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(['code' => $exCode, 'msg' => $e->getMessage(), 'data' => []])->setCallback();
        }
    }

    public static function addFriends(Request $request)
    {
        try {
            $robotSerialNo = $request->input('robotSerialNo', '');
            $account = $request->input('account', '');
            $message = $request->input('message', '');
            $retData = RobotService::addFriends($robotSerialNo, $account, $message);
            return response()->json(['code' => 200, 'msg' => 'success', 'data' => $retData])->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(['code' => $exCode, 'msg' => $e->getMessage(), 'data' => []])->setCallback();
        }
    }


}