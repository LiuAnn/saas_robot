<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 2021/1/13
 * Time: 下午8:12
 * Describe: 机器人开放接口-群相关
 */

namespace App\Http\Controllers\OpenAPI;

use App\Service\OpenAPI\Group\GroupService;
use Exception;
use Illuminate\Http\Request;

class GroupController
{
    public static function getGroupList(Request $request)
    {
        try {
            $param = $request->all();
            $retData = GroupService::getGroupList($param);
            return response()->json(['code' => 200, 'msg' => 'success', 'data' => $retData])->setCallback();
        } catch (Exception $e) {
            $exCode = $e->getCode();
            return response()->json(['code' => $exCode, 'msg' => $e->getMessage(), 'data' => []])->setCallback();
        }
    }
}