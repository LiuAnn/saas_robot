<?php
/**
 * Created by PhpStorm.
 * User: chuck
 * Date: 2019/8/1
 * Time: 3:47 PM
 */

namespace App\Http\Controllers\Stats;


use App\Http\Controllers\Controller;
use App\Service\Mall\OperateSerivice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StatsController extends Controller
{
    public function index(Request $request)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 0);
        $params    = $request->all();
        $yesterDay = time() - 86400;
        $start     = empty($params['s']) ? strtotime(date('Y-m-d', $yesterDay)) : strtotime($params['s']);
        $end       = empty($params['e']) ? strtotime(date('Y-m-d')) : strtotime($params['e'] . ' 23:59:59');

        $statsData = OperateSerivice::statsData($start, $end);
        return view('emails.stats', $statsData);
    }

    public function mall(Request $request)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 0);
        Log::info('test change logs');


        echo "test change logs\n";
//        $params    = $request->all();
//        $yesterDay = time() - 86400;
//        $start     = empty($params['s']) ? strtotime(date('Y-m-d', $yesterDay)) : strtotime($params['s']);
//        $end       = empty($params['e']) ? strtotime(date('Y-m-d')) : strtotime($params['e'] . ' 23:59:59');
//
//        $mall_num  = OperateSerivice::getMallNum($start, $end);
//        $statsData = ['mall_num' => $mall_num];
//        return view('emails.mall', $statsData);
    }

}