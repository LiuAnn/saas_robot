<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2018/12/17
 * Time: 下午7:49
 */

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Service\Member\MemberService;
use App\Service\Community\PullTaskRedisDataService;
use Illuminate\Http\Request;

class MemberController extends Controller {

    /**
     * 会员列表页
     *
     */
    public function getList(Request $request) {
        $param = $request->all();
        return MemberService::getList($param);
    }

    /**
     * 会员详情
     */
    public function getDetail(Request $request) {
        $param = $request->All();
        return MemberService::getDetail($param);
    }

    /**
     * @Notes: 获取会员的关系
     * @Date:2019/12/26 15:57
     * @param Request $request
     * @return mixed
     */
    public function getRelations(Request $request) {
        $param = $request->All();
        return MemberService::getRelations($param);
    }

    /**
     * @Notes: 获取会员佣金分配
     * @Date:2019/12/26 15:57
     * @param Request $request
     * @return mixed
     */
    public function getCommission(Request $request) {
        $param = $request->All();
        return MemberService::getCommission($param);
    }

    /**
     * 根据日期统计日常注册等数据
     */
    public function getMemberDataByDate(Request $request) {
        $param = $request->All();
        return MemberService::getMemberDataByDate($param);
    }

    /**
     * 用户的奖金列表
     */
     public function getBonusList(Request $request) {
        $param = $request->All();
        return MemberService::getBonusList($param);
     }

     /**
      * 提现列表
      */
     public function getCashList(Request $request) {
        $param = $request->All();
        return MemberService::getCashList($param);
     }

    /**
     * 新版本提现列表
     */
    public function getNewCashList(Request $request) {
        $param = $request->All();
        return MemberService::getNewCashList($param);
    }

    /**
     * 实名认证失败列表
     */
    public function getMemberIdentifys(Request $request) {
        $param = $request->All();
        return MemberService::getMemberIdentifys($param);
    }

    /**
     * 实名认证失败详情
     */
    public function getMemberIdentify(Request $request) {
        $param = $request->All();
        return MemberService::getMemberIdentify($param);
    }


    /**
     * 通过实名认证
     */
    public function upMemberIdentify(Request $request) {
        $param = $request->All();
        return MemberService::upMemberIdentify($param);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function memberDetail(Request $request)
    {
        $param = $request->All();
        return MemberService::memberDetail($param);
    }

    public function downNewCashList(Request $request)
    {
        $param = $request->All();
        return MemberService::downNewCashList($param);
    }


    public function passCash(Request $request)
    {
        $param = $request->All();
        return MemberService::passCash($param);
    }

    public function passCashAll(Request $request)
    {
        $param = $request->All();
        return MemberService::passCashAll($param);
    }

    public function noPassCash(Request $request)
    {
        $param = $request->All();
        return MemberService::noPassCash($param);
    }

    public function noPassCashAll(Request $request)
    {
        $param = $request->All();
        return MemberService::noPassCashAll($param);
    }

    public function delCheckInfo(Request $request)
    {
        $param = $request->All();
        return MemberService::delCheckInfo($param);
    }

    public function getMemeberInfoByMid(Request $request)
    {
        $param = $request->All();
        return MemberService::getMemeberInfoByMid($param);
    }


    public function getMemeberCash(Request $request)
    {
        $param = $request->All();
        return MemberService::getMemeberCash($param);
    }

    public function getMemeberBonusRecord(Request $request)
    {
        $param = $request->All();
        return MemberService::getMemeberBonusRecord($param);
    }

    public function userBonusBecommitted(Request $request)
    {
        $param = $request->All();
        return MemberService::userBonusBecommitted($param);
    }


    public function getMemeberProfit(Request $request)
    {
        $param = $request->All();
        return MemberService::getMemeberProfit($param);
    }


    public function getMemberProfitCount(Request $request)
    {
        $param = $request->All();
        $data = MemberService::getMemberProfitCount($param);
        if (isset($data['code'])) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    /*
     * 悦地摊群主收益明细
     */
    public function getGroupAdminCommissionList(Request $request)
    {
        $param = $request->All();
        $data = MemberService::getGroupAdminCommissionList($param);
        if (isset($data['code']) && $data['code'] != 200) {
            $data['msg'] = $data['msg'] ?? '';
            return response()->fail($data['code'], $data['msg']);
        }
        return response()->success($data);
    }

    public function getMemeberProfitV2(Request $request)
    {
        $param = $request->All();
        return MemberService::getMemeberProfitV2($param);
    }
    

    public function sendUserFrozenMoney(Request $request)
    {
        $param = $request->All();
        return MemberService::sendUserFrozenMoney($param);
    }


    public function updateReturnOrderList(Request $request)
    {
        $param = $request->All();
        return MemberService::updateReturnOrderList();
    }


    public function getNotReceiveOrder(Request $request)
    {
        $param = $request->All();
        return MemberService::getNotReceiveOrder();
    }
    
    public function getNotReceiveCpsOrder(Request $request)
    {
        $param = $request->All();
        return MemberService::getNotReceiveCpsOrder();
    }

    public function getGrouoMemberMoneyList(Request $request)
    {

        $param = $request->All();
        return MemberService::getGrouoMemberMoneyList($param);        
    }

    public function getAssetManagement(Request $request)
    {

        $param = $request->All();
        return MemberService::getAssetManagement($param);        
    }

    public function updateZhidingOrderMobileData(Request $request)
    {

        $param = $request->All();
        return MemberService::updateZhidingOrderMobileData($param);        
    }

    public function insertLocalLifeOrderData(Request $request)
    {

        $param = $request->All();
        return PullTaskRedisDataService::insertHotelOrderData($param);  
    }

}