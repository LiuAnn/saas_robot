<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendRoomReload extends Model
{
    protected $table = 'send_room_reload';
    protected $guarded = [];
    public $timestamps = false;
}
