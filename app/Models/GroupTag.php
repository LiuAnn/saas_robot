<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/25
 * Time: 下午06:09
 * Describe: 群标签
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupTag extends Model
{
    protected $table = 'group_tag';
    public $timestamps = false;
    protected $guarded = [];
}
