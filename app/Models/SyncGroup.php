<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SyncGroup extends Model
{
    protected $table = 'sync_group';
    protected $guarded = [];

    public static $robot_platform = [
        0 => 'wehub',
        1 => '探鲸',
        2 => '企业微信',
    ];

    public function getTagInfo()
    {
        return $this->hasOne(Tag::class, 'id', 'group_tag_id');
    }
}
