<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsBrowse extends Model
{
    protected $table = 'goods_browse';

    protected $guarded = [];
}
