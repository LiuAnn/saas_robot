<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteListWx extends Model
{
    protected $table = 'white_list_wx';

    public $timestamps = false;

    /**
     * 获取机器人发送总素材条数
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getCountSendReload()
    {
        return $this->hasMany(SendReload::class);
    }
}
