<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/25
 * Time: 下午06:09
 * Describe: 第三方App 信息
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Developers extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
