<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MallOrderGoods extends Model
{
    protected $table = 'mall_order_goods';
}
