<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ChannelData extends Model
{
    protected $table = 'channel_data';

    public $timestamps = false;

    /*
    * 获取渠道名字平台标识列表
    */
    public static function getChannelDataList($fields = ['platform_identity','channel_name'], $where = []) {
        $channel_data = self::select('platform_identity','channel_name')
            ->where('is_delete',0)
            ->get();
        return $channel_data ? $channel_data->toArray() : [];
    }
}
