<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/16
 * Time: 下午07:19
 * Describe: 消息推送的Redis记录
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendInfoRedis extends Model
{
    protected $table = 'send_info_redis';
    public $timestamps = false;

    /**
     * 不补发的code类型
     * @var int[]
     */
    public static $codeType = [
        1003,//当前机器人不在线
        4502,//机器人不在群内
        5023,//当日被踢限制次数上限 10次
        9001,//参数错误
        5009,//群不存在
    ];

}
