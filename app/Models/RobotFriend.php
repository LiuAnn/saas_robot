<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RobotFriend extends Model
{
    protected $table = 'robot_friend';

    protected $guarded = [];

    public $timestamps = false;
}
