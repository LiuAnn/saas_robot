<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/29
 * Time: 下午03:09
 * Describe: 推荐商品
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecommendGoods extends Model
{
    protected $table = 'recommend_goods';
    public $timestamps = false;
    protected $guarded = [];
}
