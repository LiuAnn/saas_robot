<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendMiniApp extends Model
{
    protected $table = 'send_mini_app';
    public $num = 1;

    public $timestamps = false;

    /*
    * 获取渠道名字平台标识列表
    */
    public static function getMiniAppList($fields = ['name','app_xml'], $where = []) {
        $channel_data = self::select($fields)
            ->where('deleted_at',0)
            ->get();
        return $channel_data ? $channel_data->toArray() : [];
    }


    /*
    * 获取小程序XMl模板
    */
    public static function getMiniAppXMLInfoById($id) {
        $appXMLInfo = self::where(['id' => $id])
            ->value('app_xml');
        return $appXMLInfo;
    }


    /*
    * 获取小程序XMl模板列表
    */
    public static function getMiniAppTemplateIdList() {
        $TemplateIdList = self::select('id', 'name')
            ->where('deleted_at',0)
            ->get();
        return $TemplateIdList ? $TemplateIdList->toArray() : [];
    }


    public static function arrayToXml($arr, $eIsArray = FALSE)
    {
        $xml = new \XmlWriter();
        if (!$eIsArray) {
            $xml->openMemory();
            $xml->startDocument(1.0, 'UTF-8');
            $xml->startElement('msg');
        }
        foreach($arr as $key => $value){
        if(is_array($value)){
            $xml->startElement($key);
            self::arrayToXml($value, TRUE);
            $xml->endElement();
            continue;
            }
            $xml->writeElement($key, $value);
        }
        if(!$eIsArray){
        $xml->endElement();
        return $xml->outputMemory(true);
        }

    }
}
