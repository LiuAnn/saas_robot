<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyInfo extends Model
{
    protected $table = 'buy_info';
}
