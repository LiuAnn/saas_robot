<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/25
 * Time: 下午06:09
 * Describe: 群标签 关联表
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupTagRelation extends Model
{
    protected $table = 'group_tag_relation';
    public $timestamps = false;
    protected $guarded = [];
}
