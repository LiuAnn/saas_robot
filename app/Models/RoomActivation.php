<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomActivation extends Model
{
    protected $table = 'room_activation';

    protected $guarded = [];
}
