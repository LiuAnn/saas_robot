<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IntoGroupReply extends Model
{
    protected $table = 'into_group_reply';
}
