<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/25
 * Time: 下午06:09
 * Describe: 用户openid绑定表
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersWechat extends Model
{
    protected $table = 'users_wechat';
    public $timestamps = false;
    protected $guarded = [];
}
