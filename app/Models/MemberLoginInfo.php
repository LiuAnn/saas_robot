<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberLoginInfo extends Model
{
    protected $table = 'member_login_info';
}
