<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/25
 * Time: 下午06:09
 * Describe: 敏感词
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CensorHelper extends Model
{
    protected $table = 'censor_helper';
    public $timestamps = false;
    protected $guarded = [];
}
