<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendInfo extends Model
{
    protected $table = 'send_info';

    protected $guarded = [];

    public $timestamps = false;

    /**
     * 推送类型对应易云的类型
     *
     * @var array
     * 1: 文字
     * 2: 图片
     * 3: 视频
     * 4: 链接
     * 5:
     *
     * 2001：文字
     * 2002：图片
     * 2003：语音
     * 2004：视频
     * 2005：链接
     * 2006：好友名片
     * 2010：文件
     * 2013：小程序
     * 2016：音乐
     */
    public static $type = [
        1 => 2001,
        2 => 2002,
        3 => 2004,
        4 => 2001,
        5 => 2001,
        6 => 2001,
        7 => 2001,
        8 => 2013
    ];

    public static $enterprise_type = [
        1 => 2001,
        2 => 2002,
        3 => 2004,
        4 => 2001,
        5 => 2001,
        6 => 2001,
        7 => 2001,
        8 => 2013
    ];


}
