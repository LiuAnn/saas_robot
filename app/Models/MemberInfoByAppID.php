<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberInfoByAppID extends Model
{
    protected $table = 'member_info_by_appid';
    protected $guarded = [];

    /**
     * 订单表总数
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getCountOrderGoods()
    {
        return $this->hasMany(MallOrderGoods::class, 'room_id', 'group.id');
    }

    /**
     * 订单表总金额
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getSumActualPrice()
    {
        return $this->hasMany(MallOrderGoods::class, 'room_id', 'group.id');
    }
}
