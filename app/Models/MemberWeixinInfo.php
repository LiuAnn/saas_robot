<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberWeixinInfo extends Model
{
    protected $table = 'member_weixin_info';
    protected $guarded = [];
    public $timestamps = false;
}
