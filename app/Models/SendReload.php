<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendReload extends Model
{
    protected $table = 'send_reload';
    protected $guarded = [];
    public $timestamps = false;
}
