<?php
namespace App\Service\University;

use App\Model\University\UniversityModel;
use Illuminate\Support\Facades\Log;


class UniversityService{

    /**
     * 获取列表
     * @param Array
     * @return Array
     */
    public static function getOrCodeImages($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = UniversityModel::getOrCodeImages($param);

        $count     = UniversityModel::getListCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatImages($result)
            ],
        ];
    }


    public static function formatImages($data=[])
    {
        $list = [];

        if($data)
        {
            foreach ($data as $item)
            {
                $list[] = [
                    "id"           => $item->id,
                    "name"         => $item->name . '',
                    "image"        => $item->image . '',
                    "backgroundColor"=> $item->background_color . '',
                    "createdAt"    => !empty($item->created_at)?$item->created_at . '':'',
                    "updatedAt"    => !empty($item->updated_at)?$item->updated_at:'',
                ];
            }
        }
        return $list;
    }


    public static function formatImage($data)
    {
        $list= [
            "id"           => $data->id,
            "name"         => $data->name . '',
            "image"        => $data->image . '',
            "backgroundColor"=> $data->background_color . '',
            "createdAt"    => !empty($data->created_at)?$data->created_at . '':'',
            "updatedAt"    => !empty($data->updated_at)?$data->updated_at:'',
        ];
        return $list;
    }

    public static function getOrCodeImage($param)
    {
        if(!isset($param['id'])||empty($param['id']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = UniversityModel::getOrCodeImage($param);

        if(empty($result))
        {
            return [ "code" => 200, "data" => ''];
        }


        return [
            "code" => 200,
            "data" => self::formatImage($result),
        ];
    }



    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }


    public static function checkInput($param = [])
    {
        if(!isset($param['name'])||empty($param['name']))
        {
            return ['code' => 400, 'msg' => '名称不能为空！'];
        }

        if(!isset($param['image'])||empty($param['image']))
        {
            return ['code' => 400, 'msg' => '图片不能为空！'];
        }

        if(!isset($param['backgroundColor'])||empty($param['backgroundColor']))
        {
            return ['code' => 400, 'msg' => '背景图颜色不能为空！'];
        }
    }

    public static function upOrCodeImage($param = [])
    {
        //验证
        $check_input = self::checkInput($param);
        if($check_input){
            return $check_input;
        }

        if(!isset($param['id'])||empty($param['id']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        //当前时间戳
        $timestamp              =   time();

        $updated_at             =   self::formatTimeStamp($timestamp);

        //数据拼接
        $data['name']       =   $param['name'];
        $data['image']      =   $param['image'];
        $data['background_color']=   $param['backgroundColor'];
        $data['updated_at'] =   $updated_at;

        $where = ['id'=>$param['id']];

        $result = UniversityModel::upOrCodeImage($where,$data);

        if($result)
        {
            return ['code' => 200 , 'msg' => '更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '更新失败！'];
        }
    }





}