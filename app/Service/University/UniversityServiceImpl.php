<?php
namespace App\Service\University;

use App\Model\Article\ArticleModel;
use App\Model\Article\ArticleTypeModel;

class UniversityServiceImpl
{
    //标题默认
    const DEFAULT_TITLE = '';

    const DEFAULT_TYPE_ID = 0;

    //内容默认
    const DEFAULT_SUMMARY = '';

    const DEFAULT_CONTENT = '';

    const DEFAULT_CONTENT_IMG = [];

    const DEFAULT_AUTHOR = '';

    //用户ID默认
    const DEFAULT_MID = 0;

    //封面图
    const DEFAULT_COVER_PIC = '';

    //悦旅大学文章分类
    const CLASSIFY = 0;

    //缓存库
    protected static $instances = [];

    protected $aid = 0;

    protected $title = '';

    protected $sort = 0;

    protected $is_online = 1;

    protected $video_path = '';

    protected $jump = 0;

    protected $like_num = 0;

    protected $down_num = 1;

    protected $summary = '';

    protected $content = '';

    protected $contentImg = '';

    protected $author = '';

    protected $mid = 0;

    protected $typeId = 0;

    protected $productId = 0;

    protected $coverPic = '';

    protected $coverPicAll = '';

    protected $createdAt = null;

    protected $updatedAt = null;

    protected $model = null;

    /**
     * ArticleServiceImpl constructor.
     * @param ArticleModel $model
     */
    protected function __construct(ArticleModel $model)
    {
        $this->aid = $model->aid;

        $this->title = $model->title;

        $this->summary = $model->summary;

        $this->content = $model->content;

        $this->contentImg = $model->content_img;

        $this->author = $model->author;

        $this->mid = $model->mid;

        $this->typeId = $model->type_id;

        $this->productId = $model->product_id;

        $this->coverPic = $model->cover_pic;

        $this->coverPicAll = $model->cover_pic_all;

        $this->createdAt = $model->created_at;

        $this->updatedAt = $model->updated_at;

        $this->sort = $model->sort;

        $this->is_online = $model->is_online;

        $this->video_path = $model->video_path;

        $this->jump = $model->jump;

        $this->down_num = $model->down_num;

        $this->like_num = $model->like_num;

        $this->model = $model;

        self::$instances[$this->aid] = $this;

        return $this;
    }

    /**
     * @return int
     */
    public function getAid()
    {
        return $this->aid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
        $this->model->sort = $sort;
    }

    /**
     * @return string
     */
    public function getIsOnline()
    {
        return $this->is_online;
    }

    /**
     * @return string
     */
    public function getVideoPath()
    {
        return $this->video_path;
    }


    /**
     * @return string
     */
    public function getJump()
    {
        return $this->jump;
    }


    /**
     * @return string
     */
    public function getDownNum()
    {
        return $this->down_num;
    }

    /**
     * @return string
     */
    public function getLikeNum()
    {
        return $this->like_num;
    }

    /**
     * @param string $is_online
     */
    public function setIsOnline($is_online)
    {
        $this->is_online = $is_online;
        $this->model->is_online = $is_online;
    }

    /**
     * @param string $is_online
     */
    public function setVideoPath($video_path)
    {
        $this->video_path = $video_path;
        $this->model->video_path = $video_path;
    }


    /**
     * @param string $is_online
     */
    public function setJump($jump)
    {
        $this->jump = $jump;
        $this->model->jump = $jump;
    }




    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->model->title = $title;
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
        $this->model->summary = $summary;
    }

    /**
     * @return int
     */
    public function getMid()
    {
        return $this->mid;
    }

    /**
     * @param int $mid
     */
    public function setMid($mid)
    {
        $this->mid = $mid;
        $this->model->mid = $mid;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * 获取类型名称
     */
    public function getTypeName()
    {
        $model = ArticleTypeModel::find($this->typeId);

        return $model ? $model->type_name : '';
    }

    /**
     * @param $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
        $this->model->type_id = $typeId;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->model->content = $content;
        $this->content = $content;
    }

    /**
     * @return mixed|string
     */
    public function getContentImg()
    {
        return $this->contentImg;
    }

    /**
     * @param mixed|string $contentImg
     */
    public function setContentImg($contentImg)
    {
        $this->model->content_img = $contentImg;
        $this->contentImg = $contentImg;
    }

    /**
     * @return mixed|string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed|string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return int|mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int|mixed $productId
     */
    public function setProductId($productId)
    {
        $this->model->product_id = $productId;
        $this->productId = $productId;
    }

    /**
     * @return mixed|string
     */
    public function getCoverPic()
    {
        return $this->coverPic;
    }

    /**
     * @return mixed|string
     */
    public function getCoverPicAll()
    {
        return $this->coverPicAll;
    }


    /**
     * @param $coverPic
     */
    public function setCoverPic($coverPic)
    {
        $this->model->cover_pic = $coverPic;
        $this->coverPic = $coverPic;
    }

    /**
     * @param $coverPic
     */
    public function setCoverPicAll($coverPicAll)
    {
        $this->model->cover_pic_all = $coverPicAll;
        $this->coverPicAll = $coverPicAll;
    }

    /**
     * @return mixed|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * 更新
     */
    public function update()
    {
        $this->model->save();
    }

    /**
     * 写入消息
     * @param array $data
     * @return UniversityServiceImpl
     */
    public static function create(array $data)
    {
        $model = ArticleModel::create([
            'title'     => $data['title'],
            'sort'        => empty($data['sort']) ? 0 : $data['sort'],
            'is_online'   => $data['is_online'],
            'type_id'   => $data['type_id'],
            'summary'   => array_get($data,'summary')   ? $data['summary']  : self::DEFAULT_SUMMARY,
            'content'   => array_get($data,'content')   ? $data['content'] : self::DEFAULT_CONTENT,
            'content_img'=> array_get($data,'content_img') ? $data['content_img'] : self::DEFAULT_CONTENT_IMG,
            'author'    => array_get($data,'author' )   ? $data['author'] : self::DEFAULT_AUTHOR,
            'cover_pic' => array_get($data,'cover_pic') ? $data['cover_pic'] : self::DEFAULT_COVER_PIC,
            'cover_pic_all' => array_get($data,'cover_pic_all') ? $data['cover_pic_all'] : '',

        ]);

        return new self($model);
    }

    /**
     * 删除消息
     */
    public function delete()
    {
        array_forget(self::$instances,$this->aid);

        $this->model->delete();
    }

    /**
     * 获取消息
     * @param $aid
     * @return UniversityServiceImpl|mixed|null
     */
    public static function getInstance($aid)
    {
        foreach (self::$instances as $instance){
            if($instance->getAid() == $aid){
                return $instance;
            }
        }

        $model = ArticleModel::find($aid);

        return $model ? new self($model) : null;
    }

    /**
     * 获取列表
     * @param $pageIndex
     * @param $pageSize
     * @param array $condition
     * @return mixed
     */
    public static function getInstancePage($pageIndex,$pageSize,array $condition)
    {
        $query = ArticleModel::query();

        //查询类型列表
        $allTypeIds = ArticleTypeModel::where('classify',self::CLASSIFY)->pluck('type_id')->toArray();
        //如果没有类型，则查询全部
        $query->whereIn('type_id',($condition['typeId'] ? [$condition['typeId']] : $allTypeIds));

        array_get($condition,'title') ?
            $query->where('title','like',"%{$condition['title']}%")  : '';

        array_get($condition,'summary') ?
            $query->where('summary','like',"%{$condition['summary']}%")  : '';

        array_get($condition,'content') ?
            $query->where('content','like',"%{$condition['content']}%")  : '';

        $tempQuery = clone $query;
        $data['totalRaw'] = $tempQuery->count();

        $data['totalPage'] = ceil($data['totalRaw']/$pageSize);

        $data['pageIndex'] = $pageIndex;

        $data['pageSize'] = $pageSize;

        $tempQuery = clone $query;
        $models = $tempQuery->skip(($pageIndex-1)*$pageSize)
            ->orderBy('sort','desc')
            ->orderBy('aid','desc')
            ->take($pageSize)
            ->get();

        $data['list'] = $models->isEmpty() ? [] : $models->map(function($model){
            return new self($model);
        })->toArray();

        return $data;
    }

    /**
     * 设置当前当前文章为banner
     * @param $typeId
     * @return bool
     */
    public function setBannerAid($typeId)
    {
        $model = ArticleTypeModel::find($typeId);

        if(!$model){
            return false;
        }

        $model->banner_aid = $model->banner_aid == $this->aid ? 0 : $this->aid;

        $model->save();

        return true;
    }

    /**
     * 文章 上下架
     * @param $is_online
     * @return bool
     */
    public function setArticleIsOnline($aid, $is_online)
    {
        $model = ArticleModel::find($aid);

        if(!$model){
            return false;
        }

        $model->is_online = $is_online;

        $model->save();

        return true;
    }


    /**
     * 修改点赞数
     * @param $is_online
     * @return bool
     */
    public function setArticleLikeNum($aid, $likeNum)
    {
        $model = ArticleModel::find($aid);

        if(!$model){
            return false;
        }

        $model->like_num = $likeNum;

        $model->save();

        return true;
    }

    /**
     * 修改点赞数
     * @param $is_online
     * @return bool
     */
    public function setArticleDownNum($aid, $downNum)
    {
        $model = ArticleModel::find($aid);

        if(!$model){
            return false;
        }

        $model->down_num = $downNum;

        $model->save();

        return true;
    }
}
