<?php
namespace App\Service\University;

use App\Model\Article\ArticleModel;
use App\Model\Article\ArticleTypeModel;
use Illuminate\Support\Facades\DB;

class UniversityTypeServiceImpl
{
    //属性名称
    const TYPE_NAME = '';

    const DEFAULT_ICON = '';

    //是否首页显示
    const INDEX_SHOW_NO = 0; //首页不显示
    const INDEX_SHOW_YES = 1;//首页显示

    //素材类型ID
    const CLASSIFY = 0; //普通栏目
    const CLASSIFY_META = 4; //素材栏目

    //缓存库
    protected static $instances = [];

    //类型ID
    protected $typeId = 0;

    //父级类型ID
    protected $typePid = '';

    //类型名称
    protected $typeName = '';

    //绝对路径
    protected $path = '';

    //类型图标
    protected $icon = '';

    protected $subtitle = '';

    //分类
    protected $classify = 0;

    //banner文章ID
    protected $bannerAid = 0;

    //模型
    protected $model = null;

    //排序
    protected $displayOrder = 0;

    //是否首页显示
    protected $indexShow = 0;

    /**
     * ArticleServiceImpl constructor.
     * @param ArticleTypeModel $model
     */
    protected function __construct(ArticleTypeModel $model)
    {
        $this->typeId = $model->type_id;

        $this->typePid= $model->type_pid;

        $this->typeName = $model->type_name;

        $this->path = $model->path;

        $this->icon = $model->icon;

        $this->subtitle = $model->subtitle;

        $this->classify = $model->classify;

        $this->bannerAid = $model->banner_aid;

        $this->displayOrder = $model->display_order;

        $this->indexShow = $model->index_show;

        $this->model = $model;

        self::$instances[$this->typeId] = $this;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @return string
     */
    public function getTypePid()
    {
        return $this->typePid;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * @param string $typeName
     */
    public function setTypeName($typeName)
    {
        $this->typeName = $typeName;
        $this->model->type_name = $typeName;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed|string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        $this->model->icon = $icon;
    }

    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        $this->model->subtitle = $subtitle;
    }



    /**
     * @return int|mixed
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * @param int|mixed $displayOrder
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        $this->model->display_order = $displayOrder;
    }

    /**
     * @return int|mixed
     */
    public function getIndexShow()
    {
        return $this->indexShow;
    }

    /**
     * @param int|mixed $indexShow
     */
    public function setIndexShow($indexShow)
    {
        $this->indexShow = $indexShow;

        $this->model->index_show = $indexShow;
    }

    /**
     * @return int|mixed
     */
    public function getClassify()
    {
        return $this->classify;
    }

    /**
     * @param int|mixed $classify
     */
    public function setClassify($classify)
    {
        $this->classify = $classify;

        $this->model->classify = $classify;
    }

    /**
     * 更新
     */
    public function update()
    {
        $this->model->save();
    }

    /**
     * 删除
     */
    public function delete()
    {
        DB::transaction(function(){
            //删除后代分类
            $subModels = ArticleTypeModel::whereRaw("FIND_IN_SET({$this->typeId},path)")->get();
            $subModels->map(function($model){
                array_forget(self::$instances,$model->type_id);
                $model->delete();
            });

            //删除本身
            $this->model->delete();
            array_forget(self::$instances,$this->typeId);
        });
    }

    /**
     * @param array $data
     * @return UniversityTypeServiceImpl|null
     */
    public static function create(array $data)
    {
        if($data['type_pid']!=0){
            //如果不是顶级分类
            $instance = self::getInstance($data['type_pid']);

            if(!$instance){
                return null;
            }

            $typePid = $instance->getTypeId();
            $path = $instance->getPath().','.$instance->getTypeId();
        }else{
            //如果是顶级分类
            $typePid = 0;
            $path = 0;
        }

        $model = ArticleTypeModel::create([
            'type_name'=>$data['type_name'],
            'type_pid' =>$typePid,
            'path'     =>$path,
            'icon'     => array_get($data,'icon') ? $data['icon'] : self::DEFAULT_ICON,
            'classify' => $data['classify'],
            'index_show'=>self::INDEX_SHOW_YES
        ]);

        return new self($model);
    }

    /**
     * @param $typeId
     * @return UniversityTypeServiceImpl|mixed|null
     */
    public static function getInstance($typeId)
    {
        foreach (self::$instances as $instance){
            if($instance->getTypeId() == $typeId){
                return $instance;
            }
        }

        $model = ArticleTypeModel::find($typeId);

        return $model ? new self($model) : null;
    }

    /**
     * @param $typePid
     * @param int $classify
     * @return mixed
     */
    public static function getTypeListByPid($typePid,$classify=0)
    {
        $classify = $classify ? [$classify] : [self::CLASSIFY,self::CLASSIFY_META];

        $models = ArticleTypeModel::where('type_pid',$typePid)
            ->orderBy('display_order','asc')
            ->whereIn('classify',$classify)
            ->get();

        return $models->isEmpty() ? [] : $models->map(function($model){
            return new self($model);
        })->toArray();
    }

    /**
     * 获取当前分类文章数量[包含后代分类下面的文章数量]
     */
    public function getArticlesNum()
    {
        $typeIds = ArticleTypeModel::whereRaw("FIND_IN_SET({$this->typeId},path)")->where('deleted_at',null)->pluck('type_id')->toArray();

        $typeIds = array_merge($typeIds,[$this->typeId]);

        return ArticleModel::whereIn('type_id',$typeIds)->where('deleted_at',null)->orderBy('aid','desc')->count();
    }

    /**
     * @param int $classify
     * @return mixed
     */
    public static function getTypeLevelListByClassify($classify=0)
    {
        $models = ArticleTypeModel::where('classify', $classify)
            ->where('path','<>','0')
            ->orderBy(DB::raw("CONCAT(path,',',type_id)"), "asc")
            ->get();

        return $models->isEmpty() ? [] : $models->map(function($model){
            return new self($model);
        })->toArray();
    }

    /**
     * @return array
     */
    public function getSub()
    {
        $models = ArticleTypeModel::where('type_pid',$this->typeId)
//            ->where('deleted_at',null)
            ->orderBy('display_order', "asc")
            ->orderBy('created_at','desc')
            ->get();

        return $models->isEmpty() ? [] : $models->map(function($model){
            return new self($model);
        })->toArray();
    }

    /**
     * 获取当前栏目的banner文章ID
     */
    public function getBannerAid()
    {
        return $this->bannerAid;
    }

}
