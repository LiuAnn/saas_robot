<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2018/12/18
 * Time: 下午3:10
 */

namespace App\Service\Member;

use App\Service\Community\UseIndexService;
use App\Model\Community\PullTaskRedisModel;
use App\Model\Member\MemberModel;
use App\Model\Admin\GroupModel;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class MemberService
{

    //社群用户分佣规则
    const USER_IDENTITY_RULE = [1=>0.4,2=>0.6,3=>0.8]; //每个角色的分佣比例
    const USER_IDENTITY_DESC = [0=>'普通用户',1=>'掌柜',2=>'大掌柜',3=>'超级掌柜']; //角色称谓
    const USER_IDENTITY_MONEY = [1=>39900,2=>398000,3=>998000]; //升级所需GMV 单位 分
    const USER_IDENTITY_BONUS = [1=>0,2=>39900,3=>100000,4=>50000];//升级后奖励金额 单位 分
    const SPECIAL_COMMODITIES = [2095870];
    /**
     * 会员列表
     * @param Array
     * @return Array
     */
    public static function getList($param)
    {
        $display = 20;
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $offset = ($page - 1) * $display;

        $param["offset"] = $offset;
        $param["display"] = $display;

        $result = MemberModel::getList($param);
        $count = MemberModel::getListCount($param);
        $listCount = MemberModel::getWhereListCount($param);

        return [
            "code" => 200,
            "msg" => "来啦~ 老弟",
            "data" => [
                "listCount" => $listCount,
                "page" => $page,
                "total" => $count,
                "list" => self::formatList($result),
                "hotelOrder" => []
            ]
        ];
    }

    /**
     * 用户信息
     * @param Array
     * @return Array
     */
    public static function getDetail($param)
    {
        $memberId = $param["memberId"];
        $result = MemberModel::getDetail($memberId);
        return [
            "code" => 200,
            "msg" => "来啦~ 老弟",
            "data" => [
                "baseInfo" => self::formatBaseInfo($result),
                "coupon" => self::getMemberCoupon($memberId),
                "hotel" => self::getHotelOrderListByMemberid($memberId),
                "company" => self::getCompnayByMemeberId($memberId)
            ]
        ];
    }

    /**
     * @Notes: 获取用户关系层级
     * @Date:2019/12/26 19:27
     * @param $param
     * @return array
     */
    public static function getRelations($param)
    {
        $memberId = $param["memberId"];
        $resultPath = MemberModel::getRelationsPath($memberId);
        if (empty($resultPath)) {
            return ["code" => 400, "msg" => "当前用户不存在用户关系"];
        }
        $path = substr($resultPath->path, 2);
        if (empty($path)) {
            return [
                "code" => 200,
                "msg" => "来啦~ 老弟",
                "data" => [
                    "memberRelations" => [],
                ]
            ];
        }
        $result = MemberModel::getRelationList($path);
        return [
            "code" => 200,
            "msg" => "来啦~ 老弟",
            "data" => [
                "memberRelations" => self::formatMemberRelations($result),
            ]
        ];
    }

    /**
     * @Notes: 获取佣金分配
     * @Date:2019/12/26 19:28
     * @param $param
     * @return array
     */
    public static function getCommission($param)
    {
        $memberId = $param["memberId"];
        $result = MemberModel::getBrokerage($memberId);

        return [
            "code" => 200,
            "msg" => "来啦~ 老弟",
            "data" => [
                "commission" => self::formatCommission($result),
            ]
        ];
    }

    /**
     * 根据日期获取统计数据
     * @param Aarray
     * @return Array
     */
    public static function getMemberDataByDate($param)
    {
        $page = !empty($param["page"]) ? $param["page"] : 1;

        $display = 50;
        $offset = ($page - 1) * $display;

        $result = MemberModel::getMemberDataByDate($offset, $display);
        $list = [];
        foreach ($result as $item) {
            $onlineCard = self::getOlineCardCount($item->date1);
            $downCard = self::getDownCardCount($item->date1);
            $list[] = [
                "date" => $item->date1,
                "regCount" => $item->count,
                "cardCount" => $onlineCard + $downCard,
                "onlineCard" => $onlineCard,
                "offlineCard" => $downCard,
                "vCount" => $item->vCount,
                "yCount" => $item->yCount,
                "newHotelCount" => 0,
                "newMallCount" => 0,
                "hotelUseCount" => 0,
                "mallUseCount" => 0,
            ];
        }

        return [
            "code" => 200,
            "msg" => "来啦~~ 老弟",
            "data" => [
                "page" => $page,
                "total" => 1000,
                "list" => $list,
            ]
        ];
    }

    /**
     * 获取用户的优惠券
     * @param int
     * @return Array
     */
    public static function getMemberCoupon($memberId)
    {
        $result = MemberModel::getMemberCoupon($memberId);
        $list = [];
        foreach ($result as $item) {
            $list[] = [
                "cid" => $item->cid,
                "name" => $item->name,
                "amount" => $item->amount,
                "total" => $item->total,
                "useNum" => $item->usenum,
                "unNum" => $item->unnum,
                "endDate" => self::formatTimeStamp($item->end_dates)
            ];
        }
        return $list;
    }

    /**
     * 获取用户所属分公司
     * @param
     */

    /**
     * 酒店订单
     * @param Array
     * @return Array
     */
    public static function getHotelOrderListByMemberid($memberId)
    {
        $result = MemberModel::getHotelOrderListByMemberid($memberId);
        $list = [];
        foreach ($result as $item) {
            $list[] = [
                "orderNo" => $item->ordersn,
                "payFormNo" => $item->payFormNo,
                "hotelName" => $item->hotel_name,
                "price" => intval($item->price),
                "sale" => $item->sale,
                "cMoney" => $item->cmoney,
                "act" => intval($item->act),
                "arrivalDate" => self::formatTimeStamp($item->arrival_date),
                "payTime" => self::formatTimeStamp($item->paytime),
                "hotelId" => $item->hotel_id
            ];
        }
        return $list;
    }

    /**
     * 获取用户所属分公司
     * @param int
     * @return Array
     */
    public static function getCompnayByMemeberId($memberId)
    {
        $result = MemberModel::getCompanyByMemeberId($memberId);
        return $result;
    }

    /**
     * 用户的奖金列表
     */
    public static function getBonusList($param)
    {
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $display = 50;

        $offset = ($page - 1) * $display;
        $count = MemberModel::getBonusListCount($param);
        $list = MemberModel::getBonusList($param, $offset, $display);
        return [
            "code" => 200,
            "msg" => "",
            "data" => [
                "list" => $list,
                "count" => $count,
                "page" => $page,
                "totalPage" => ceil($count / $display)
            ]
        ];
    }

    /**
     * 提现申请列表
     */
    public static function getCashList($param)
    {
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $display = 50;
        $offset = ($page - 1) * $display;
        $count = MemberModel::getCashListCount();
        $list = MemberModel::getCashList($offset, $display);
        $tempList = [];

        foreach ($list as $item) {
            $tempList[] = [
                "mid" => $item->mid,
                "billNo" => $item->billNo,
                "truename" => $item->truename,
                "mobile" => $item->mobile,
                "cardId" => $item->cardid,
                "money" => $item->money,
                "bonusMount" => $item->bMount,
                "cashMount" => $item->amount,
                "isCheck" => $item->is_identify_check == 1 ? "已实名" : "未实名",
                "createTime" => $item->createTime
            ];
        }
        return [
            "code" => 200,
            "msg" => "",
            "data" => [
                "count" => $count,
                "page" => $page,
                "totalPage" => ceil($count / $display),
                "list" => $tempList,
            ]
        ];
    }


    //新版本提现申请列表
    public static function getNewCashList($param)
    {
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 50;

        $param["offset"] = ($page - 1) * $display;
        $param["display"] = $display;

        $count = MemberModel::getNewCashListCount($param);
        $result = MemberModel::getNewCashList($param);

        return [
            "code" => 200,
            "data" => [
                "count" => $count,
                "page" => $page,
                "pageSize" => $display,
                "totalPage" => ceil($count / $display),
                "list" => self::formatNewCashList($result)
            ],
        ];
    }

    public static function formatNewCashList($data)
    {
        $list = [];

        if (!empty($data)) {
            foreach ($data as $item) {
                $cardResult = MemberModel::getCardResult(['mid' => $item->mid]);

                $cardId = !empty($cardResult) ? \Crypt::decrypt($cardResult->id_card) : '';
//            $cardId     = !empty($cardResult)?$cardResult->id_card:'';

                $bankResult = MemberModel::getBank(['mid' => $item->mid]);

                $bankCard = !empty($bankResult) ? \Crypt::decrypt($bankResult->bank_card) : '';
//            $bankCard   = !empty($bankResult)?$bankResult->bank_card:'';

                if (strtotime($item->createTime) > strtotime("2019-11-15")) {
                    if ($item->card_type > 0) {
                        $mcCashMakeMoney = $item->mcCash * 0.98;
                    } else {
                        $mcCashMakeMoney = $item->mcCash * 0.84;
                    }

                } else {
                    $mcCashMakeMoney = $item->mcCash * 0.9;
                }


                $list[] = [
                    'id' => $item->id,
                    'mid' => $item->mid,
                    'cardType' => $item->card_type > 0 ? '会员' : '非会员',
                    'uname' => !empty($cardResult) ? $cardResult->uname : '',
                    'cardId' => $cardId,
                    'bankCard' => $bankCard,
                    'status' => $item->mcStatus,
                    'money' => $item->money,//账户总额
                    'balance' => $item->balance,//账户余额
                    'cash' => $item->cash,//账户到账
                    'mcCash' => $item->mcCash,//本次体现
                    'mcCashMakeMoney' => number_format($mcCashMakeMoney, 2),//本次应打款
//                    'mcCashService'=>$item->mcCash*0.1,//本次体现手续费
                    'amount' => $item->amount,//提现金额
                    'mcStatus' => self::getMcStatus($item->mcStatus),//打款状态
                    'bonusMount' => !empty($item->bMount) ? $item->bMount : 0,//佣金
//                'cash'      =>$item->cash,
                    'mobile' => $item->mobile,
                    'createTime' => $item->createTime,
                ];
            }
        }
        return $list;
    }


    /**
     * 根据某个日期获取线上开卡量
     */
    public static function getOlineCardCount($date)
    {
        return MemberModel::getOlineCardCount($date);
    }

    /**
     * 根据某个日期获取线下开卡量
     */
    public static function getDownCardCount($date)
    {
        return MemberModel::getDownCardCount($date);
    }

    /**
     * 格式化列表信息
     */
    private static function formatList($data)
    {
        $list = [];
        foreach ($data as $item) {
            $list[] = self::formatBaseInfo($item);
        }
        return $list;
    }

    /**
     * 格式化用户基础信息
     * @param Array
     * @return Array
     */
    private static function formatBaseInfo($item)
    {
        return [
            "mid" => $item->mid,
            "mobile" => $item->mobile,
            "nickName" => $item->nickname,
            "trueName" => $item->truename,
            "sex" => $item->sex,
            "cardType" => self::getCardType($item->card_type),
            "joinTime" => self::formatTimeStamp($item->jointime),
            "cardDate" => self::formatTimeStamp($item->card_date),
            "oAmount" => $item->occupied_amount,
            "rAmount" => $item->rollouted_occupied_amount,
            "cardNo" => self::getObject($item, "cardid"),
            "province" => self::getObject($item, "province"),
            "city" => self::getObject($item, "city"),
            "district" => self::getObject($item, "district"),
            "address" => self::getObject($item, "address"),
            "integral" => [
                "total" => self::nullConverNumber($item->total),
                "available" => self::nullConverNumber($item->available),
                "consum" => self::nullConverNumber($item->consum),
                "expire" => self::nullConverNumber($item->expire)
            ],
            "parentMobile" => $item->pMobile,
            "parentName" => $item->pName
        ];
    }

    /**
     * @Notes: 格式化用户关系
     * @Date:2019/12/26 16:55
     * @param $data
     * @return array
     */
    private static function formatMemberRelations($data)
    {
        $list = [];
        if ($data) {
            foreach ($data as $key => $item) {
                $list[$key]['mobile'] = $item->mobile;
                $list[$key]['truename'] = $item->truename;
                $list[$key]['stockNumber'] = $item->stock_number;
                $list[$key]['firstNumber'] = $item->first_number;
                $list[$key]['type'] = $item->type;
            }

        }
        return $list;
    }

    /**
     * @Notes: 格式化佣金分成
     * @Date:2019/12/26 16:55
     * @param $data
     * @return array
     */
    private static function formatCommission($data)
    {
        $list = [];
        if ($data) {
            foreach ($data as $key => $item) {
                $list[$key]['amount'] = $item->amount;
                $list[$key]['identityType'] = self::getIdentityType($item->identity_type);
                $list[$key]['type'] = $item->type;
                $list[$key]['mobile'] = $item->mobile;
                $list[$key]['truename'] = $item->truename;
                $list[$key]['stockNumber'] = $item->stock_number;
                $list[$key]['firstNumber'] = $item->first_number;
            }

        }
        return $list;
    }

    public static function getIdentityType($identityType)
    {
        $string = "c用户 ";
        //1:首次小创客 2:首次大创客 3:二次小创客且有库存 4:二次大创客且有库存
        // 5:二次小创客无库存 6:二次大创客无库存 7:分公司
        switch ($identityType) {
            case 1 :
                $string = "首次小创客";
                break;
            case 2 :
                $string = "首次大创客";
                break;
            case 3 :
                $string = "二次小创客且有库存";
                break;
            case 4 :
                $string = "二次大创客且有库存";
                break;
            case 5 :
                $string = "二次小创客无库存";
                break;
            case 6 :
                $string = "二次大创客无库存";
                break;
            case 7 :
                $string = "分公司";
                break;
        }
        return $string;
    }

    /**
     * 获取会员类型
     */
    private static function getCardType($type)
    {
        $string = "未购卡";
        switch ($type) {
            case 1 :
                $string = "精英卡";
                break;
            case 2 :
                $string = "悦旅卡(早期)";
                break;
            case 3 :
                $string = "领袖卡(早期)";
                break;
        }
        return $string;
    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /**
     * null 转 0
     */
    private static function nullConverNumber($value)
    {
        return isset($value) ? $value : 0;
    }

    /**
     * null 转 string
     */
    private static function nullConverString($value)
    {
        return isset($value) ? $value : "暂无";
    }

    /**
     * 判断object 是否存在
     */
    private static function getObject($obj, $key)
    {
        return property_exists($obj, $key) ? $obj->$key : "暂无";
    }

    /**
     * 实名认证失败列表
     * @param Array
     * @return Array
     */
    public static function getMemberIdentifys($param)
    {
        $display = 20;
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $offset = ($page - 1) * $display;

        $param["offset"] = $offset;
        $param["display"] = $display;

        $result = MemberModel::getMemberIdentifyList($param);
        $count = MemberModel::getMemberIdentifyListCount($param);

        return [
            "code" => 200,
            "msg" => "来啦~ 老弟",
            "data" => [
                "page" => $page,
                "total" => $count,
                "list" => self::formatMemberIdentifys($result),
            ]
        ];
    }

    public static function formatMemberIdentifys($data)
    {
        $list = [];
        if ($data) {
            foreach ($data as $key => $item) {
                $list[$key]['id'] = $item->id;
                $list[$key]['mid'] = $item->mid;
                $list[$key]['mobile'] = $item->mobile;
                $list[$key]['nickName'] = $item->nickname;
                $list[$key]['trueName'] = $item->truename;
                $list[$key]['cardId'] = $item->cardid;
                $list[$key]['cardidIdentify'] = $item->cardid_identify;
                $list[$key]['status'] = $item->status;

            }

        }
        return $list;
    }

    public static function formatMemberIdentify($data)
    {
        $list = [];
        if ($data) {
            $list = [
                'id' => $data->id,
                'mid' => $data->mid,
                'mobile' => $data->nickname,
                'nickName' => $data->nickname,
                'trueName' => $data->truename,
                'cardId' => $data->cardid,
                'frontImg' => self::imgStr($data->front_img),
                'backImg' => self::imgStr($data->back_img),
                'thirdImg' => self::imgStr($data->third_img),
                'a' => 1
            ];

        }
        return $list;
    }

    public static function imgStr($img = '')
    {


//
        if (@fopen($img, 'r')) //        if(file_get_contents($img,0,null,0,1))

        {
            $img = $img;
        } else {
            $img = $img . "/";

        }
        return $img;
    }


    /**
     * 信息
     * @param Array
     * @return Array
     */
    public static function getMemberIdentify($param)
    {
        $result = MemberModel::getMemberIdentify(['id' => $param['id']]);
        return [
            "code" => 200,
            "msg" => "来啦~ 老弟",
            "data" => self::formatMemberIdentify($result)
        ];
    }

    /**
     * 信息
     * @param Array
     * @return Array
     */
    public static function upMemberIdentify($paramData)
    {
        $param = $paramData['info'];

        $where['mid'] = $param['mid'];

        $data['is_identify_check'] = 1;
        $data['truename_identify'] = $param['trueName'];
        $data['cardid'] = $param['cardId'];
        $data['cardid_identify'] = $param['cardId'];

        $result = MemberModel::updateMember($where, $data);

        $identifyWhere['id'] = $param['id'];
        $identifyData['status'] = 1;

        $idResult = MemberModel::upMemberIdentify($identifyWhere, $identifyData);

        if ($result && $idResult) {
            self::delRedisMemberModel($param['mid']);
            return [
                "code" => 200,
                "msg" => "认证成功",
            ];
        } else {
            return [
                "code" => 400,
                "msg" => "认证失败",
            ];
        }
    }

    /**
     * 从缓存中，清除用户的实例
     */
    public static function delRedisMemberModel($mid)
    {
        $key = "member_model_{$mid}";
        Redis::del($key);
    }


    public static function getMcStatus($type)
    {
        $string = "";
        //1:待确定 2:提现中 3:提现成功 4:提现失败'
        switch ($type) {
            case 1 :
                $string = "待确定";
                break;
            case 2 :
                $string = "提现中";
                break;
            case 3 :
                $string = "提现成功";
                break;
            case 4 :
                $string = "提现失败";
                break;
        }
        return $string;
    }

    public static function memberDetail($param)
    {
        $memberInstance = MemberModel::getMemberInfo(['mid' => $param['mid']]);

        if (!$memberInstance) {
            return ['code' => 400, 'msg' => '用户不存在'];
        }

        $memberIdentifyInfo = MemberModel::getMemberIdentifyInfo(['mid' => $param['mid'], 'status' => 1]);
//        sline_member_identify

        return [
            'code' => 200,
            'msg' => '',
            'data' => [
                'mobile' => $memberInstance->mobile,
                'nickName' => !empty($memberInstance->nickName) ? $memberInstance->nickName : '',
                'trueName' => !empty($memberInstance->truename) ? $memberInstance->truename : '',
                'cardId' => substr_replace($memberInstance->cardid_identify, str_pad('', 13, '*'), 2, 13),
                'joinTime' => $memberInstance->jointime,
                'identifyInfo' => $memberInstance->card_type > 0 ? '会员' : '注册用户',
                //认证时间
                'identifyTime' => !empty($memberIdentifyInfo->createTime) ? $memberIdentifyInfo->createTime : '未知',
                //正面照片
                'frontImg' => !empty($memberIdentifyInfo->front_img) ? $memberIdentifyInfo->front_img : '',
                //反面照片
                'backImg' => !empty($memberIdentifyInfo->back_img) ? $memberIdentifyInfo->back_img : '',
                //手持照片
                'thirdImg' => !empty($memberIdentifyInfo->third_img) ? $memberIdentifyInfo->third_img : '',
            ]

        ];
    }

    public static function downNewCashList($param)
    {
        $limit = 200;

        $count = MemberModel::getNewCashListCount($param);

        if ($count == 0) {
            return ['code' => 400, 'msg' => '无数据!!'];
        }

//        if($count>=700)
//        {
//            return ['code' => 400, 'msg' => '数据太多,请分段下载!!','num'=>$count];
//        }

        $totalPage = ceil($count / $limit);

        $resultData = [];

        $j = 0;

        for ($i = 1; $i <= $totalPage; $i++) {
            $cashList = [];
            $param["offset"] = ($i - 1) * $limit;
            $param["display"] = $limit;
            $result = MemberModel::getNewCashList($param);

            $cashList = self::formatNewCashList($result);

            foreach ($cashList as $v) {
                $resultData[$j]['id'] = $v['id'];
                $resultData[$j]['mid'] = $v['mid'];
                $resultData[$j]['uname'] = $v['uname'];
                $resultData[$j]['cardId'] = $v['cardId'];
                $resultData[$j]['bankCard'] = $v['bankCard'];
                $resultData[$j]['money'] = $v['money'];
                $resultData[$j]['balance'] = $v['balance'];
                $resultData[$j]['cash'] = $v['cash'];
                $resultData[$j]['mcCash'] = $v['mcCash'];
                $resultData[$j]['mcCashMakeMoney'] = $v['mcCashMakeMoney'];
                $resultData[$j]['amount'] = $v['amount'];
                $resultData[$j]['cardType'] = $v['cardType'];
                $resultData[$j]['mcStatus'] = $v['mcStatus'];
                $resultData[$j]['bonusMount'] = $v['bonusMount'];
                $resultData[$j]['mobile'] = $v['mobile'];
                $resultData[$j]['createTime'] = $v['createTime'];
                $j++;
            }
        }

        return ["code" => 200, "data" => $resultData];
    }


    //新版本提现申请通过
    public static function passCash($param)
    {
        $id = !empty($param["id"]) ? $param["id"] : 0;
        if ($id <= 0) {
            return ["code" => 400, "msg" => '参数错误'];
        }

        $cashInfo = MemberModel::getNewCashInfo($param);

        if (empty($cashInfo)) {
            return ["code" => 400, "msg" => '没有这条数据'];
        }

        if ($cashInfo->status != 2) {
            return ["code" => 400, "msg" => '此数据不能操作,不是提现中数据'];
        }

        DB::beginTransaction();

        $cashWhere['id'] = $id;
        $upCash['status'] = 3;

        $cashResult = MemberModel::upMemberCash($cashWhere, $upCash);


        $memberInfo = MemberModel::getMemberInfo(['mid' => $cashInfo->mid]);

        if (empty($memberInfo)) {
            DB::rollBack();
            return ["code" => 400, "msg" => '无用户数据'];
        }


        $memberWhere['mid'] = $memberInfo->mid;

        $upMember['cash'] = $memberInfo->cash + $cashInfo->amount;

        $memberResult = MemberModel::updateMember($memberWhere, $upMember);

        if ($cashResult && $memberResult) {
            DB::commit();
            return ["code" => 200, 'msg' => '操作成功'];
        } else {
            DB::rollBack();
            return ["code" => 400, 'msg' => '操作失败'];
        }
    }


    //新版本提现申请不通过
    public static function noPassCash($param)
    {
        $id = !empty($param["id"]) ? $param["id"] : 0;
        if ($id <= 0) {
            return ["code" => 400, "msg" => '参数错误'];
        }

        $cashInfo = MemberModel::getNewCashInfo($param);

        if (empty($cashInfo)) {
            return ["code" => 400, "msg" => '没有这条数据'];
        }

        if ($cashInfo->status != 2) {
            return ["code" => 400, "msg" => '此数据不能操作,不是提现中数据'];
        }

        DB::beginTransaction();

        $cashWhere['id'] = $id;
        $upCash['status'] = 4;

        $cashResult = MemberModel::upMemberCash($cashWhere, $upCash);


        $memberInfo = MemberModel::getMemberInfo(['mid' => $cashInfo->mid]);

        if (empty($memberInfo)) {
            DB::rollBack();
            return ["code" => 400, "msg" => '无用户数据'];
        }


        $memberWhere['mid'] = $memberInfo->mid;

        $upMember['balance'] = $memberInfo->balance + $cashInfo->amount;

        $memberResult = MemberModel::updateMember($memberWhere, $upMember);

        if ($cashResult && $memberResult) {
            DB::commit();
            return ["code" => 200, 'msg' => '操作成功'];
        } else {
            DB::rollBack();
            return ["code" => 400, 'msg' => '操作失败'];
        }


    }


    //新版本提现申请通过
    public static function passCashAll($param)
    {
        $idStr = !empty($param["idArr"]) ? $param["idArr"] : '';

        $idArr = explode("\n", $idStr);

        $cashResultError = [];
        $cashResultSuccess = [];

        if (!empty($idArr)) {
            $i = 0;
            $j = 0;
            foreach ($idArr as $value) {
                if ($value) {
                    $passCashRes = self::passCash(['id' => $value]);

                    if ($passCashRes['code'] == 400) {
                        $cashResultError[$i]['cashId'] = $value;
                        $cashResultError[$i]['cashMsg'] = $passCashRes['msg'];
                        $i++;
                    } else {
                        $cashResultSuccess[$i]['cashId'] = $value;
                        $cashResultSuccess[$i]['cashMsg'] = '提现成功';
                        $j++;
                    }
                }

            }
        }

        $cashResult = array_merge($cashResultError, $cashResultSuccess);

        return ["code" => 200, 'data' => $cashResult, 'msg' => '操作成功'];
    }


    //新版本提现申请不通过
    public static function noPassCashAll($param)
    {
        $idStr = !empty($param["idArr"]) ? $param["idArr"] : '';

        $idArr = explode("\n", $idStr);

        $cashResultError = [];
        $cashResultSuccess = [];

        if (!empty($idArr)) {
            $i = 0;
            $j = 0;
            foreach ($idArr as $value) {
                if ($value) {
                    $passCashRes = self::noPassCash(['id' => $value]);

                    if ($passCashRes['code'] == 400) {
                        $cashResultError[$i]['cashId'] = $value;
                        $cashResultError[$i]['cashMsg'] = $passCashRes['msg'];
                        $i++;
                    } else {
                        $cashResultSuccess[$i]['cashId'] = $value;
                        $cashResultSuccess[$i]['cashMsg'] = $passCashRes['msg'];;
                        $j++;
                    }
                }

            }
        }

        $cashResult = array_merge($cashResultError, $cashResultSuccess);

        return ["code" => 200, 'data' => $cashResult, 'msg' => '操作成功'];
    }


    //清除认证信息
    public static function delCheckInfo($param)
    {
        $mid = !empty($param["mid"]) ? $param["mid"] : 0;
        if ($mid <= 0) {
            return ["code" => 400, "msg" => '参数错误'];
        }

        $memberInfo = MemberModel::getMemberInfo($param);

        if (empty($memberInfo)) {
            return ["code" => 400, "msg" => '没有这条数据'];
        }


        DB::beginTransaction();

        $extWhere['mid'] = $mid;
        //delete from sline_member_identify_ext where mid

        $extResult = MemberModel::delIdentifyExt($extWhere);

        if (empty($extResult)) {
            DB::rollBack();
            return ["code" => 400, "msg" => '无身份证信息'];
        }

        //delete from sline_member_bankCard_ext where mid =

        $bankWhere['mid'] = $mid;

        $bankResult = MemberModel::delBankCardExt($bankWhere);


        if (empty($bankResult)) {
            DB::rollBack();
            return ["code" => 400, "msg" => '无银行卡信息'];
        }

        if ($extResult && $bankResult) {
            DB::commit();
            return ["code" => 200, 'msg' => '操作成功'];
        } else {
            DB::rollBack();
            return ["code" => 400, 'msg' => '操作失败'];
        }


    }

    //提现状态改为提现中
    public static function upStatusToWithdrawal($param)
    {
        $id = !empty($param["id"]) ? trim($param["id"]) : 0;
        if ($id <= 0 || !is_numeric($id)) {
            return ["code" => 400, "msg" => '参数错误'];
        }

        $cashInfo = MemberModel::getNewCashInfo($param);

        if (empty($cashInfo)) {
            return ["code" => 400, "msg" => '没有这条数据'];
        }

        if ($cashInfo->status != 3) {
            return ["code" => 400, "msg" => '此数据不能操作,不是提现成功数据'];
        }

        DB::beginTransaction();

        $cashWhere['id'] = $id;
        $upCash['status'] = 2;

        $cashResult = MemberModel::upMemberCash($cashWhere, $upCash);


        $memberInfo = MemberModel::getMemberInfo(['mid' => $cashInfo->mid]);

        if (empty($memberInfo)) {
            DB::rollBack();
            return ["code" => 400, "msg" => '无用户数据'];
        }


        $memberWhere['mid'] = $memberInfo->mid;

        $upMember['cash'] = $memberInfo->cash - $cashInfo->amount;

        $memberResult = MemberModel::updateMember($memberWhere, $upMember);

        if ($cashResult && $memberResult) {
            DB::commit();
            return ["code" => 200, 'msg' => '操作成功'];
        } else {
            DB::rollBack();
            return ["code" => 400, 'msg' => '操作失败'];
        }
    }

    /**
     * 是否为符合规则的群
     * @param $param
     * @return array
     */
    public static function sendIsRuleGroup($room_id = 0, $type)
    {

        $groupInfo = MemberModel::getGroupData($room_id);


        if (empty($groupInfo) || $groupInfo->mid < 1 || $groupInfo->mid == '') {

            return ["code" => 400, 'msg' => '操作失败'];
        }

        self::updateGroupUserInfo($groupInfo->mid, $room_id, $type);

        // $lastBind =  MemberModel::getBindgroupInfo($room_id,$groupInfo->mid);

        // //减少普通群或减少有效群数量
        // if(in_array($type,[4,5,6,8])&&!empty($lastBind)){

        //     if($lastBind['change_type']==1){

        //         self::updateGroupUserInfo($groupInfo->mid,$room_id,$type);
        //     }else{

        //         if($lastBind['change_reason']==8&&$type!=8){

        //             self::updateGroupUserInfo($groupInfo->mid,$room_id,$type);
        //         }
        //     }

        //     return true;
        // }

        // //增加普通群或增加有效群数量
        // if(in_array($type,[1,2,3,7])){


        //     if($lastBind['change_type']==2&&$type!=7){

        //         self::updateGroupUserInfo($groupInfo->mid,$room_id,$type);
        //     }else{

        //         if($lastBind['change_reason']!=7&&$type==7){

        //             self::updateGroupUserInfo($groupInfo->mid,$room_id,$type);
        //         }
        //     }

        //     return true;

        // }


    }


    /**
     * 编辑群主信息
     * @param $param
     * @return array
     */
    public static function updateGroupUserInfo($mid = 0, $room_id = 0, $type, $form = 0)
    {

        if ($mid == 0 || $room_id == 0) {

            return ["code" => 400, 'msg' => '操作失败'];
        }


        $groupInfo = GroupModel::getGroupData($room_id);

        if (empty($groupInfo)) {

            return ["code" => 400, 'msg' => '操作失败'];
        }

        $groupData = self::getNewGroupData($type, $groupInfo->member_count);

        $memberInfo = MemberModel::getGroupMemeberInfo($mid);

        $ivalid_group_number = MemberModel::getIvalidGroupNumber($mid);

        if (empty($memberInfo)) {

            $resData = MemberModel::getMemberInfoByMid(['mid' => $mid]);

            Log::info('updateGroupUserInfo获取用户注册信息' . json_encode($resData));

            if (empty($resData)) {

                return ["code" => 400, 'msg' => '用户注册信息不全'];
            }

            $data = [];

            $data['mid'] = $mid;
            $data['nick_name'] = $resData->nickname;
            $data['mobile'] = $resData->mobile;
            $data['type'] = 1;
            $data['parent_id'] = $resData->parent_id;
            $data['group_number'] = $groupData['group_number'] < 0 ? 0 : $groupData['group_number'];
            $data['ivalid_group_number'] = $ivalid_group_number;
            $data['leader_num'] = 0;
            $data['ivalid_leader_num'] = 0;

            $id = MemberModel::insertMemberInfo($data);
        } else {

            $data = [];

            $data['mid'] = $mid;
            $data['group_number'] = $memberInfo['group_number'] + $groupData['group_number'] < 0 ? 0 : $memberInfo['group_number'] + $groupData['group_number'];
            $data['ivalid_group_number'] = $ivalid_group_number;

            $id = MemberModel::updateGroupMemeberInfo($data);
        }

        if ($groupData['group_number'] != 0) {

            MemberModel::insertBindgroupRecode(['mid' => $mid, 'room_id' => $room_id, 'group_type' => 1, 'change_type' => $groupData['change_type'], 'change_reason' => $type, 'change_reason_remak' => $groupData['reason']]);
        }

        if ($data['ivalid_group_number'] != 0) {

            MemberModel::insertBindgroupRecode(['mid' => $mid, 'room_id' => $room_id, 'group_type' => 2, 'change_type' => $groupData['change_type'], 'change_reason' => $type, 'change_reason_remak' => $groupData['reason']]);
        }

        //处理群主奖金
        self::sendGroupMemberBonus($mid);

        $parent_id = (isset($memberInfo['parent_id']) && $memberInfo['parent_id'] > 0) ? $memberInfo['parent_id'] : (isset($resData->parent_id) && $resData->parent_id > 0 ? $resData->parent_id : 0);

        self::updateZhanZhangUserInfo($parent_id);

    }

    /**
     * 编辑上级信息
     * @param $param
     * @return array
     */
    public static function updateZhanZhangUserInfo($mid = 0)
    {


        if ($mid == 0) {

            return ["code" => 400, 'msg' => '操作失败'];
        }

        $memberInfo = MemberModel::getGroupMemeberInfo($mid);


        $ivalid_leader_num = MemberModel::getIvalidLeaderNum($mid, 1);
        $leader_num = MemberModel::getIvalidLeaderNum($mid);

        if (empty($memberInfo)) {


            $resData = MemberModel::getMemberInfoByMid(['mid' => $mid]);

            $data = [];

            $data['mid'] = $mid;
            $data['nick_name'] = $resData->nickname;
            $data['mobile'] = $resData->mobile;
            $data['type'] = $ivalid_leader_num > 2 ? 2 : 1;
            $data['parent_id'] = $resData->parent_id;
            $data['group_number'] = 0;
            $data['ivalid_group_number'] = 0;
            $data['leader_num'] = $leader_num;
            $data['ivalid_leader_num'] = $ivalid_leader_num;
            if ($ivalid_leader_num > 2) {

                $data['created_time_zanzhang'] = date("Y-m-d H:i:s", time());
            }

            $id = MemberModel::insertMemberInfo($data);
        } else {

            $data['mid'] = $mid;
            $data['type'] = $ivalid_leader_num > 0 ? 2 : 1;
            $data['leader_num'] = $leader_num;
            $data['ivalid_leader_num'] = $ivalid_leader_num;
            if ($ivalid_leader_num > 2) {

                $data['created_time_zanzhang'] = date("Y-m-d H:i:s", time());
            }

            $id = MemberModel::updateGroupMemeberInfo($data);

        }

        //处理站长奖金
        self::sendZhanZhangBonus($mid);
    }


    /**
     * 发放群主奖金
     * @param $param
     * @return array
     */
    public static function sendGroupMemberBonus($mid)
    {

        //获取群主用户信息
        $memberInfo = MemberModel::getGroupMemeberInfo($mid);

        $ivalid_group_number = $memberInfo['ivalid_group_number'];

        $lastInfo = MemberModel::getLastMemberBonus($mid, 1);

        $data = [];

        if (empty($lastInfo)) {

            if ($ivalid_group_number > 1) {

                $data['mid'] = $mid;
                $data['type'] = 1;
                $data['bonus_type'] = 1;
                $data['group_type'] = 1;
                $data['amount'] = 500;

                $id = MemberModel::insertMemberBonus($data);

                if ($id) {

                    $member['mid'] = $mid;
                    $member['total_money'] = $memberInfo['total_money'] + 500;
                    $member['frozen_money'] = $memberInfo['frozen_money'] + 500;

                    MemberModel::updateGroupMemeberInfo($member);
                }

            }

            return true;
        }

        $sendBonusFormat = self::sendBonusFormat($lastInfo->bonus_type);

        //有冻结奖金并且有效群数量小于需求数量消除冻结奖金
        if ($lastInfo->type == 1 && $ivalid_group_number < $sendBonusFormat['needGroupNum']) {


            $data['mid'] = $mid;
            $data['type'] = 3;
            $data['bonus_type'] = $lastInfo->bonus_type;
            $data['group_type'] = $lastInfo->group_type;
            $data['amount'] = $lastInfo->amount;

            $id = MemberModel::insertMemberBonus($data);

            if ($id) {

                $member['mid'] = $mid;
                $member['total_money'] = $memberInfo['total_money'] - $lastInfo->amount < 0 ? 0 : $memberInfo['total_money'] - $lastInfo->amount;
                $member['frozen_money'] = $memberInfo['frozen_money'] - $lastInfo->amount < 0 ? 0 : $memberInfo['frozen_money'] - $lastInfo->amount;

                MemberModel::updateGroupMemeberInfo($member);
            }

            return true;
        }


        //冻结奖金到账判断下一级奖励是否符合规则
        if ($lastInfo->type == 2 && $lastInfo->bonus_type < 5) {

            $sendBonusFormatV1 = self::sendBonusFormat($lastInfo->bonus_type + 1);

            if ($ivalid_group_number >= $sendBonusFormatV1['needGroupNum']) {

                $data['mid'] = $mid;
                $data['type'] = 1;
                $data['bonus_type'] = $lastInfo->bonus_type + 1;
                $data['group_type'] = $lastInfo->group_type;
                $data['amount'] = $sendBonusFormatV1['needBonusMoney'];

                $id = MemberModel::insertMemberBonus($data);


                if ($id) {

                    $member['mid'] = $mid;
                    $member['total_money'] = $memberInfo['total_money'] + $sendBonusFormatV1['needBonusMoney'];
                    $member['frozen_money'] = $memberInfo['frozen_money'] + $sendBonusFormatV1['needBonusMoney'];

                    MemberModel::updateGroupMemeberInfo($member);
                }

                return true;

            }
        }


        //有消除冻结奖金并且有效群数量大于等于需求数量消除冻结奖金
        if ($lastInfo->type == 3 && $ivalid_group_number >= $sendBonusFormat['needGroupNum']) {


            $data['mid'] = $mid;
            $data['type'] = 1;
            $data['bonus_type'] = $lastInfo->bonus_type;
            $data['group_type'] = $lastInfo->group_type;
            $data['amount'] = $lastInfo->amount;

            $id = MemberModel::insertMemberBonus($data);

            if ($id) {

                $member['mid'] = $mid;
                $member['total_money'] = $memberInfo['total_money'] + $lastInfo->amount;
                $member['frozen_money'] = $memberInfo['frozen_money'] + $lastInfo->amount;

                MemberModel::updateGroupMemeberInfo($member);
            }

            return true;
        }


    }


    /**
     * 发放站长奖金
     * @param $param
     * @return array
     */
    public static function sendZhanZhangBonus($mid = 0)
    {


        if ($mid == 0) {


            return ["code" => 400, 'msg' => '操作失败'];
        }

        //获取群主用户信息
        $memberInfo = MemberModel::getGroupMemeberInfo($mid);

        $ivalid_group_number = $memberInfo['ivalid_leader_num'];

        $lastInfo = MemberModel::getLastMemberBonus($mid, 2);

        $data = [];

        if (empty($lastInfo)) {

            if ($ivalid_group_number > 0) {

                $data['mid'] = $mid;
                $data['type'] = 1;
                $data['bonus_type'] = 1;
                $data['group_type'] = 2;
                $data['amount'] = 1000;

                $id = MemberModel::insertMemberBonus($data);

                if ($id) {

                    $member['mid'] = $mid;
                    $member['total_money'] = $memberInfo['total_money'] + 1000;
                    $member['frozen_money'] = $memberInfo['frozen_money'] + 1000;

                    MemberModel::updateGroupMemeberInfo($member);
                }
            }

            return true;

        }

        $sendBonusFormat = self::sendZhanZhangBonusFormat($lastInfo->bonus_type);

        //有冻结奖金并且有效群数量小于需求数量消除冻结奖金
        if ($lastInfo->type == 1 && $ivalid_group_number < $sendBonusFormat['needGroupNum']) {


            $data['mid'] = $mid;
            $data['type'] = 3;
            $data['bonus_type'] = $lastInfo->bonus_type;
            $data['group_type'] = $lastInfo->group_type;
            $data['amount'] = $lastInfo->amount;

            $id = MemberModel::insertMemberBonus($data);

            if ($id) {

                $member['mid'] = $mid;
                $member['total_money'] = $memberInfo['total_money'] - $lastInfo->amount < 0 ? 0 : $memberInfo['total_money'] - $lastInfo->amount;
                $member['frozen_money'] = $memberInfo['frozen_money'] - $lastInfo->amount < 0 ? 0 : $memberInfo['frozen_money'] - $lastInfo->amount;

                MemberModel::updateGroupMemeberInfo($member);
            }

            return true;
        }


        //冻结奖金到账判断下一级奖励是否符合规则
        if ($lastInfo->type == 2 && $lastInfo->bonus_type < 5) {

            $sendBonusFormatV1 = self::sendBonusFormat($lastInfo->bonus_type + 1);

            if ($ivalid_group_number >= $sendBonusFormatV1['needGroupNum']) {

                $data['mid'] = $mid;
                $data['type'] = 1;
                $data['bonus_type'] = $lastInfo->bonus_type + 1;
                $data['group_type'] = $lastInfo->group_type;
                $data['amount'] = $sendBonusFormatV1['needBonusMoney'];

                $id = MemberModel::insertMemberBonus($data);


                if ($id) {

                    $member['mid'] = $mid;
                    $member['total_money'] = $memberInfo['total_money'] + $sendBonusFormatV1['needBonusMoney'];
                    $member['frozen_money'] = $memberInfo['frozen_money'] + $sendBonusFormatV1['needBonusMoney'];

                    MemberModel::updateGroupMemeberInfo($member);
                }

                return true;

            }
        }


        //有消除冻结奖金并且有效群数量小于需求数量消除冻结奖金
        if ($lastInfo->type == 3 && $ivalid_group_number > $sendBonusFormat['needGroupNum']) {


            $data['mid'] = $mid;
            $data['type'] = 1;
            $data['bonus_type'] = $lastInfo->bonus_type;
            $data['group_type'] = $lastInfo->group_type;
            $data['amount'] = $lastInfo->amount;

            $id = MemberModel::insertMemberBonus($data);

            if ($id) {

                $member['mid'] = $mid;
                $member['total_money'] = $memberInfo['total_money'] + $lastInfo->amount;
                $member['frozen_money'] = $memberInfo['frozen_money'] + $lastInfo->amount;

                MemberModel::updateGroupMemeberInfo($member);
            }

            return true;
        }

    }


    /**
     * 发放群主奖金规则
     * @param $param mid 群主id
     * @param $param type  群主类型 1群主 2 站长
     * @return array
     */
    public static function sendBonusFormat($type)
    {

        $getBonusType = [1 => 2, 2 => 5, 3 => 10, 4 => 15, 5 => 30];
        $getBonusMoney = [2 => 500, 5 => 1000, 10 => 3000, 15 => 5000, 30 => 9900];

        $needGroupNum = 0;
        $needBonusMoney = 0;

        if (isset($getBonusType[$type])) {

            $needGroupNum = $getBonusType[$type];
            $needBonusMoney = $getBonusMoney[$needGroupNum];

        }

        $last_info['needGroupNum'] = $needGroupNum;
        $last_info['needBonusMoney'] = $needBonusMoney;


        return $last_info;
    }


    /**
     * 发放站长奖金规则
     * @param $param mid 群主id
     * @param $param type  群主类型 1群主 2 站长
     * @return array
     */
    public static function sendZhanZhangBonusFormat($type)
    {

        $getBonusType = [1 => 1, 2 => 3, 3 => 5, 4 => 10, 5 => 15];
        $getBonusMoney = [1 => 1000, 3 => 2000, 5 => 5000, 10 => 8000, 15 => 12900];

        $needGroupNum = 0;
        $needBonusMoney = 0;

        if (isset($getBonusType[$type])) {

            $needGroupNum = $getBonusType[$type];
            $needBonusMoney = $getBonusMoney[$needGroupNum];

        }

        $last_info['needGroupNum'] = $needGroupNum;
        $last_info['needBonusMoney'] = $needBonusMoney;


        return $last_info;
    }


    public static function getNewGroupData($type, $count)
    {

        $group_number = 0;
        $ivalid_group_number = 0;
        $reason = "";
        $change_type = 0;

        switch ($type) {
            case 1:

                $group_number = 1;
                $ivalid_group_number = $count >= 80 ? 1 : 0;
                $reason = "后台绑定群主";
                $change_type = 1;

                break;
            case 2:

                $group_number = 1;
                $ivalid_group_number = $count >= 80 ? 1 : 0;
                $reason = "社群申请群主";
                $change_type = 1;

                break;

            case 3:

                $group_number = 1;
                $ivalid_group_number = $count >= 80 ? 1 : 0;
                $reason = "机器人进群";
                $change_type = 1;

                break;

            case 4:

                $group_number = -1;
                $ivalid_group_number = $count >= 80 ? -1 : 0;
                $reason = "机器人退群";
                $change_type = 2;
                break;

            case 5:

                $group_number = -1;
                $ivalid_group_number = $count >= 80 ? -1 : 0;
                $reason = "群解散";
                $change_type = 2;

                break;

            case 6:

                $group_number = 0;
                $ivalid_group_number = $count >= 80 ? -1 : 0;
                $reason = "群成员不满80人";
                $change_type = 2;
                break;

            case 7:

                $group_number = 0;
                $ivalid_group_number = $count >= 80 ? 1 : 0;
                $reason = "群成员满80人";
                $change_type = 1;

                break;

            case 8:

                $group_number = -1;
                $ivalid_group_number = $count >= 80 ? -1 : 0;
                $reason = "后台解绑群主";
                $change_type = 2;

                break;

            default:
                # code...
                break;
        }


        return ['group_number' => $group_number, 'ivalid_group_number' => $ivalid_group_number, 'reason' => $reason, 'change_type' => $change_type];
    }

    /**
     * 获取群主信息
     * @param $param mid 群主id
     * @return array
     */
    public static function getMemeberInfoByMid($param)
    {

        if (!isset($param['mid']) || empty($param['mid'])) {

            return ['code' => 400, 'meesage' => '用户ID不能为空', 'data' => []];
        }

        $mid = $param['mid'];

        $memberInfo = MemberModel::getGroupMemeberInfo($mid);

        $resData = MemberModel::getMemberInfoByMid(['mid' => $mid]);


        $getBonusType = [1 => 2, 2 => 5, 3 => 10, 4 => 15, 5 => 30];
        $getBonusMoney = [2 => 500, 5 => 1000, 10 => 3000, 15 => 5000, 30 => 9900];

        $userLevel = 0;

        if (!empty($memberInfo)) {

            $lastInfo = MemberModel::getLastMemberBonus($mid, 1);

            if (!empty($lastInfo)) {

                switch ($lastInfo->type) {
                    case 1:

                        $userLevel = $lastInfo->bonus_type;
                        break;
                    case 2:
                        $userLevel = $lastInfo->bonus_type;
                        break;
                    case 3:
                        $userLevel = $lastInfo->bonus_type - 1 < 0 ? 0 : $lastInfo->bonus_type;
                        break;
                    default:
                        $userLevel = 0;
                        break;
                }

            }
        }

        $isgRroupMember = MemberModel::isgRroupMember($mid);

        $isgRroupMember = empty($isgRroupMember) ? 0 : 1;


        $rule = [];

        foreach ($getBonusType as $key => $value) {

            $rule[$key - 1]['number'] = $getBonusType[$key];
            $rule[$key - 1]['money'] = $getBonusMoney[$value] / 100;
            $rule[$key - 1]['is_now'] = $userLevel == $key ? 1 : 0;
        }


        $ivalid_group_number = empty($memberInfo) ? 0 : $memberInfo['ivalid_group_number'];

        $little_group_number = $getBonusType[$userLevel + 1] - $ivalid_group_number < 0 ? 0 : $getBonusType[$userLevel + 1] - $ivalid_group_number;

        $next_money_number = $getBonusMoney[$getBonusType[$userLevel + 1]] / 100;
        //已提现金额
        $get_cash = MemberModel::getGetCashNumber($mid);
        $withdraw_number = MemberModel::getWithdrawNumber($mid);

        $user_identity = (empty($memberInfo)||$memberInfo['user_identity']==0) ? $isgRroupMember : $memberInfo['user_identity'];
        $user_identity_desc = self::USER_IDENTITY_DESC;
        $user_identity_name = $user_identity_desc[$user_identity];
        $isGroupMember = MemberModel::isGroupMember($mid);

        $data = [

            'mid' => $mid,
            'nick_name' => $resData->nickname,
            'mobile' => $resData->mobile,
            'codeNumber' => $resData->code_number,
            'litpic' => $resData->litpic,
            'peding_account_money' => empty($memberInfo) ? 0 : $memberInfo['peding_account_money'] / 100,
            'frozen_money' => empty($memberInfo) ? 0 : $memberInfo['frozen_money'] / 100,
            'cash_money' => empty($memberInfo) ? 0 : $memberInfo['cash_money'] / 100,
            'all_money' => empty($memberInfo) ? 0 : ($memberInfo['cash_money'] + $memberInfo['drawn_money']) / 100,
            'get_cash' => $get_cash,//TODO 获取现金
            'withdraw_number' => $withdraw_number,//TODO 已提现金额
            'ivalid_group_number' => $ivalid_group_number,
            'little_group_number' => $little_group_number,
            'next_money_number' => $next_money_number,
            'more_money' => 500,//群主每月赚的钱×5 当小于100时直接展示500
            'type' => $user_identity,
            'isGroupMember'=> empty($isGroupMember)?0:1,
            'user_identity_name'=>$user_identity_name,
            'rule' => $rule
        ];


        return ['code' => 200, 'meesage' => 'success', 'data' => $data];
    }


    /**
     * 悦地摊用户提现
     * @param $param mid 群主id
     * @return array
     */
    public static function getMemeberCash($param)
    {

        Log::info('悦地摊用户提现' . json_encode($param));

        if (isset($param['mid']) && $param['mid'] != '') {

            $memberInfo = MemberModel::getGroupMemeberInfo($param['mid']);


            if (empty($memberInfo)) {

                return ['code' => 400, 'message' => '用户不存在，无法提现'];
            }

            $cashInfo = MemberModel::getMemeberCashInfo($param['billNo']);

            Log::info('社群赚用户提现' . $param['mid'] . json_encode($param));

            if (!empty($cashInfo)) {


                $id = MemberModel::updateMemeberCash($param);

                if ($param['status'] == 3 && $id) {

                    $data = [
                        'mid' => $param['mid'],
                        'drawn_money' => $memberInfo['drawn_money'] + $param['amount'] * 100,
                        'total_money' => $memberInfo['total_money'] - $param['amount'] * 100,
                        'frozen_money' => $memberInfo['frozen_money'] - $param['amount'] * 100,
                    ];


                    $result = MemberModel::updateGroupMemeberInfo($data);

                    if ($result) {

                        return ['code' => 200, 'message' => '操作成功'];
                    }

                }

                if ($param['status'] == 4 && $id) {

                    $data = [
                        'mid' => $param['mid'],
                        'cash_money' => $memberInfo['cash_money'] + $param['amount'] * 100,
                        'frozen_money' => $memberInfo['frozen_money'] - $param['amount'] * 100,
                    ];


                    $result = MemberModel::updateGroupMemeberInfo($data);

                    if ($result) {

                        return ['code' => 200, 'message' => '操作成功'];
                    }
                }


                if ($param['status'] == 2 && $id) {

                    return ['code' => 200, 'message' => '操作成功'];
                }

                return ['code' => 400, 'message' => '操作失败1'];

            } else {

                log::info('社群赚用户提现用户信息' . $param['mid'] . json_encode($memberInfo));
 
                $amount = $param['amount']*100; 

                if ($memberInfo['cash_money'] < $amount) {

                    return ['code' => 400, 'message' => '可提现金额不足','data'=>['memberInfo'=>$memberInfo['cash_money'],'amount'=>$param['amount']*100]];
                }

                $id = MemberModel::insertMemeberCash($param);

                if ($id) {

                    $data = [
                        'mid' => $param['mid'],
                        'cash_money' => $memberInfo['cash_money'] - $param['amount'] * 100,
                        'frozen_money' => $memberInfo['frozen_money'] + $param['amount'] * 100,
                    ];

                }

                $result = MemberModel::updateGroupMemeberInfo($data);

                if ($result) {

                    return ['code' => 200, 'message' => '操作成功'];
                }

                return ['code' => 400, 'message' => '操作失败2'];

            }
        } else {


            return ['code' => 400, 'message' => '操作失败3'];
        }


    }


    /**
     * 获取用户收益  此接口待完善
     * @param $param mid 群主id
     * @return array
     */
    public static function getMemeberProfit($param)
    {

        if (!isset($param['mid']) || empty($param['mid'])) {

            return ['code' => 400, 'message' => '用户ID不能为空'];
        }

        $type = isset($param['type']) ? $param['type'] : 1;

        switch ($type) {
            case 1:
                //获取本月收益
                $param['startTime'] = date('Y-m-01 00:00:00', strtotime('-1 month'));
                $param['endTime'] = date('Y-m-d H:i:s', time());
                # code...
                break;
            case 2:
                //获取上个月收益
                $param['startTime'] = date('Y-m-01 00:00:00', strtotime('-1 month'));
                $param['endTime'] = date('Y-m-01  00:00:00', strtotime(date("Y-m-d")));
                # code...
                break;

            case 3:
                //获取近30天收益
                $param['startTime'] = date('Y-m-d H:i:s', strtotime('-130 days'));
                $param['endTime'] = date('Y-m-d H:i:s', time());
                break;

            default:
                # code...
                break;
        }

        $resData = MemberModel::getMemeberProfit($param);

        return ['code' => 200, 'message' => 'success', 'data' => ['totalProfit' => $resData, 'PendProfit' => $resData]];

    }


    /**
     * 获取用户收益  此接口待完善
     * @param $param mid 群主id
     * @return array
     */
    public static function getMemberProfitCount($param)
    {

        if (!isset($param['mid']) || empty($param['mid'])) {

            return ['code' => 400, 'msg' => '用户ID不能为空'];
        }

        $type = isset($param['type']) ? $param['type'] : 1;
        //获取今日推广收益
        $param['startTime'] = date('Y-m-d 00:00:00', time());
        $param['endTime'] = date('Y-m-d H:i:s', time());
        $todayProfit = MemberModel::getMemeberProfit($param, $type);
        //获取本月推广收益
        $param['startTime'] = date('Y-m-01 00:00:00', strtotime('-1 month'));
        $param['endTime'] = date('Y-m-d H:i:s', time());
        $monthProfit = MemberModel::getMemeberProfit($param, $type);
        //历史推广收益
        $param['startTime'] = date('Y-m-01 00:00:00', strtotime('-1 month'));
        $param['endTime'] = date('Y-m-d H:i:s', time());
        unset($param['startTime'], $param['endTime']);
        $historyProfit = MemberModel::getMemeberProfit($param, $type);
        //可提现推广收益
        $param['status'] = 1;
        $catchProfit = MemberModel::getMemeberProfit($param, $type);

        return [
            'todayProfit' => $todayProfit,
            'monthProfit' => $monthProfit,
            'historyProfit' => $historyProfit,
            'catchProfit' => $catchProfit,
        ];
    }



    /**
     * 获取用户收益  此接口待完善
     * @param $param mid 群主id
     * @return array
     */
    public static function getGroupAdminCommissionList($param = [])
    {

        $param['page'] = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize'] = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $param['member_id'] = isset($param['member_id']) ? $param['member_id'] : 0;
        if (empty($param['member_id'])) {
            return ["code" => 400, "msg" => '用户ID不能为空', "data" => []];
        }
        $result = GroupModel::getGroupAdminCommissionEveryDayForYT($param);
        $count = GroupModel::getGroupAdminCommissionEveryDayForYTCount($param);
        $data = [];
        foreach ($result as $key => $value) {
            $list = $value;
            $list['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list['channel'] = getChannelNameChannel($value['channel']);
            $list['lirun'] = $value['lirun']/100;
            $list['true_amount'] = $value['status'] > 0 ? $value['lirun']/100 : 0;
            $data[] = $list;
        }
        return $returnData = [
            'total' => $count,
            'page' => $param['page'],
            'pageSize' => $param['pageSize'],
            'list' => $data
        ];
    }


    /**
     * 获取用户收益  此接口待完善
     * @param $param mid 群主id
     * @return array
     */
    public static function getMemeberProfitV2($param)
    {

        if (!isset($param['mid']) || empty($param['mid'])) {

            return ['code' => 400, 'message' => '用户ID不能为空'];
        }

        $type = isset($param['type']) ? $param['type'] : 1;

        switch ($type) {
            case 1:
                //获取本月收益
                $param['startTime'] = date('Y-m-01 00:00:00', time());
                $param['endTime'] = date('Y-m-d H:i:s', time());
                # code...
                break;
            case 2:
                //获取上个月收益
                $param['startTime'] = date('Y-m-01 00:00:00', strtotime('-1 month'));
                $param['endTime'] = date('Y-m-01  00:00:00', strtotime(date("Y-m-d")));
                # code...
                break;

            case 3:
                //获取近30天收益
                $param['startTime'] = date('Y-m-d H:i:s', strtotime('-30 days'));
                $param['endTime'] = date('Y-m-d H:i:s', time());
                break;

            default:
                # code...
                break;
        }

        $memberInfo = MemberModel::getGroupMemeberInfo($param['mid']);


        $getBonusType = [1 => 2, 2 => 5, 3 => 10, 4 => 15, 5 => 30];
        $getBonusMoney = [2 => 500, 5 => 1000, 10 => 3000, 15 => 5000, 30 => 9900];

        $userLevel = 0;

        if (!empty($memberInfo)) {

            $lastInfo = MemberModel::getLastMemberBonus($param['mid'], 1);

            if (!empty($lastInfo)) {

                switch ($lastInfo->type) {
                    case 1:

                        $userLevel = $lastInfo->bonus_type;
                        break;
                    case 2:
                        $userLevel = $lastInfo->bonus_type;
                        break;
                    case 3:
                        $userLevel = $lastInfo->bonus_type - 1 < 0 ? 0 : $lastInfo->bonus_type;
                        break;
                    default:
                        $userLevel = 0;
                        break;
                }

            }
        }


        $ivalid_group_number = empty($memberInfo) ? 0 : $memberInfo['ivalid_group_number'];

        $little_group_number = $getBonusType[$userLevel + 1] - $ivalid_group_number < 0 ? 0 : $getBonusType[$userLevel + 1] - $ivalid_group_number;

        $next_money_number = $getBonusMoney[$getBonusType[$userLevel + 1]] / 100;


        $resData = MemberModel::getMemeberProfit($param);

        $data = [
            'pendProfit' => MemberModel::getMemeberProfit($param, 1),   //预估推广收益
            'shareProfit' => MemberModel::getMemeberProfit($param, 3),//预估分享收益
            'zhanProfit' => MemberModel::getMemeberProfit($param, 2),//预估站长分享收益
            'channelSubsidy' => MemberModel::getMemeberProfit($param, 4),//预估群主平台补贴
            'rateProfit' => 0,//本月预估返利收益
            'platformProfit' => 0,//本月预估平台奖励
            'createGroupProfit' => MemberModel::getMemeberCreateGroupProfit($param),//本月预估建群奖励
            'newUserSelfProfit' => 0,//本月预估拉新收益
            'newUserProfit' => 0,//本月预估拉新奖励
            'newFinanceProfit' => 0,//本月预估金融拉新推广收益
            'newSendFinanceProfit' => 0,//本月预估金融拉新奖励收益
            'liveProfit' => 0,//本月预估生活服务类收益
            'livePlatFormProfit' => 0,//本月预估生活服务平台奖励
            'subsidyProfit' => 0,//本月预估推广补贴
            'ivalidGroupNumber' => $ivalid_group_number,//有效群数量
            'littleGroupNumber' => $little_group_number,//缺少群数量
            'nextMoneyNumber' => $next_money_number,//下阶段奖励金额
            'recommendRule' => '3-10-15-25-999',//当月推荐办卡人数
            'commissionRule' => '20-100-200-400-15/个',//奖励公司推广佣金
        ];

        $data['totalProfit'] = $resData + $data['createGroupProfit'];

        return ['code' => 200, 'message' => 'success', 'data' => $data];

    }


    /**
     * 获取用户提现记录  此接口待完善
     * @param $param mid 群主id
     * @return array
     */
    public static function getMemeberBonusRecord($param)
    {

        if (!isset($param['mid']) || empty($param['mid'])) {

            return ['code' => 400, 'message' => '用户ID不能为空'];
        }

        $mid = $param['mid'];

        $type = isset($param['type']) ? $param['type'] : 1;

        $data = [];

        $total = 0;
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        switch ($type) {
            case 1:
                //用户奖金明细

                $list = MemberModel::getUserBonusList($mid, $page, $pageSize);

                $total = MemberModel::getUserBonusCount($mid);

                foreach ($list as $key => $item) {

                    $data[$key]['mid'] = $item['mid'];
                    $data[$key]['type'] = $item['type'];
                    $data[$key]['bonus_type'] = $item['bonus_type'];
                    $data[$key]['group_type'] = $item['group_type'];
                    $data[$key]['amount'] = $item['amount'] / 100;
                    $data[$key]['created_time'] = $item['created_time'];
                }

                break;

            case 2:
                //用户佣金明细
                # code...
                $list = MemberModel::getUserBecommittedList($mid, $page, $pageSize);

                $total = MemberModel::getUserBecommittedCount($mid);

                foreach ($list as $key => $item) {

                    $data[$key]['mid'] = $item['mid'];
                    $data[$key]['type'] = $item['type'];
                    $data[$key]['bonus_type'] = $item['bonus_type'];
                    $data[$key]['group_type'] = $item['group_type'];
                    $data[$key]['amount'] = $item['amount'] / 100;
                    $data[$key]['created_time'] = $item['created_at'];
                }
                break;

            default:
                # code...
                break;
        }


        return ['code' => 200, 'message' => 'success', 'data' => ['total' => $total, 'page' => $page, 'data' => $data]];


    }

    /**
     * 获取悦淘7天前未结算佣金订单
     * @return array
     */
    public static function getNotReceiveOrder()
    {


        $list = MemberModel::getNotReceiveOrder();

        if (!empty($list)) {

            Log::info('获取悦淘已结算未分佣订单');

            foreach ($list as $key => $value) {

                $orderInfo = MemberModel::getYtOrderInfo($value['order_no']);

                if (!empty($orderInfo) && $orderInfo['receiving_time'] > 0) {


                    self::userBonusBecommitted(['ordersn' => $value['order_no'], 'order_status' => 3]);
                }
            }

        } else {

            Log::info('没有未分佣已结算订单');
        }
    }


    /**
     * 拉取cps缺失订单
     * @return array
     */
    public static function getNotReceiveCpsOrder()
    {


        self::userBonusBecommitted(['ordersn' => '11715916752859422', 'order_status' => 1]);

        //self::newSendIsRuleGroup(6830);
        

        // $list = MemberModel::getNotReceiveCpsOrder();

        // if (!empty($list)) {

        //     foreach ($list as $key => $value) {

        //         self::userBonusBecommitted(['ordersn' => $value['ordersn'], 'order_status' => 1]);
        //     }
        // }

    }


    /**
     * 变更商品佣金状态
     * @param $param mid 群主id
     * @return array
     */
    public static function userBonusBecommitted($param)
    {

        if (!isset($param['ordersn']) || empty($param['ordersn'])) {

            return ['code' => 400, 'message' => '订单号不能为空'];
        }

        if (!isset($param['order_status']) || empty($param['order_status'])) {

            return ['code' => 400, 'message' => '订单状态不能为空'];
        }

        $orderInfo = MemberModel::getOrderInfoByOrdersn($param['ordersn']);

        if (!empty($orderInfo)) {

            switch ($param['order_status']) {
                case 3:

                    //订单状态3 已收货 开始分佣
                    Log::info('用户点击确认收货订单完成开始分佣' . json_encode($param));
                    // $bonus_status = MemberModel::getBecommittedInfo()
                    if ($orderInfo['order_status'] != 3) {

                        $data = [];

                        $data['order_status'] = 3;
                        $data['ordersn'] = $param['ordersn'];
                        $data['receiving_time'] = isset($param['receiving_time']) ? $param['receiving_time'] : time();//确认收货时间
                        $data['updated_at'] = time();

                        $updateOrderStatus = MemberModel::updateOrderStatus($data);

                        if ($updateOrderStatus) {

                            self::updateBonusBecommittedStatus($param['ordersn'], 3);//实现预估分佣到账步骤
                            self::insertTotalUpgradeMoney($param['ordersn']);
                        }
                    } else {

                        return ['code' => 400, 'message' => '订单状态修改失败-v1'];
                    }


                    # code...
                    break;

                case 2:
                    //订单状态2 订单退款 
                    Log::info('用户订单退款开始' . json_encode($param));

                    if ($orderInfo['order_status'] != 2) {

                        $data = [];

                        $data['order_status'] = 2;
                        $data['ordersn'] = $param['ordersn'];
                        $data['cancel_time'] = isset($param['cancel_time']) ? $param['cancel_time'] : time();//订单退款时间
                        $data['updated_at'] = time();

                        $updateOrderStatus = MemberModel::updateOrderStatus($data);

                        if ($updateOrderStatus) {

                            self::updateBonusBecommittedStatus($param['ordersn'], 2);
                        }
                    } else {

                        return ['code' => 400, 'message' => '订单状态修改失败-v2'];
                    }
                    # code...
                    break;

                case 1:

                    self::updateBonusBecommittedStatus($param['ordersn'], 1);

                    break;

                default:
                    # code...
                    break;
            }

        } else {

            return ['code' => 400, 'message' => '订单不存在'];
        }

    }


    /**
     * 修改订单分佣到账状态
     * @param $param mid 群主id
     * @return array
     */
    public static function updateBonusBecommittedStatus($ordersn = 0, $type = 0)
    {

        if ($ordersn == 0 || $type == 0) {

            return ['code' => 400, 'message' => '参数不正确'];
        }

        switch ($type) {
            case 1:
                //存入预估分佣
                $orderInfo = MemberModel::getOrderInfoByOrdersn($ordersn);


                $list = MemberModel::getBonusBecommittedStatusByOrdersn($ordersn);


                //悦淘普通商品分佣

                if (!empty($orderInfo) && empty($list) && $orderInfo['room_id'] > 1 && $orderInfo['order_three_partner'] != 4 && !in_array($orderInfo['goods_id'], self::SPECIAL_COMMODITIES)) {


                    self::newCommssionRule($ordersn,1);

                    // $sendTotalMoney = 0;//毛利润

                    // switch ($orderInfo['good_type']) {
                    //     case 1:

                    //         $growth_value = MemberModel::getProductGrowthValue($orderInfo['goods_id']);


                    //         if ($growth_value > 0) {

                    //             //当前商品成长值大于0时 总分佣 = 成长值/2
                    //             $sendTotalMoney = $growth_value / 2 * 100;

                    //         } else {

                    //             //当前商品成长值等于0时 总分佣 = 支付价-商品进货价*商品数量-优惠价价格
                    //             $sendTotalMoney = $orderInfo['actual_price'] - $orderInfo['goods_price_buy'] * $orderInfo['goods_num'] - $orderInfo['goods_coupon_average'] * 100;

                    //         }


                    //         break;

                    //     default:

                    //         $sendTotalMoney = $orderInfo['commission'];

                    //         break;
                    // }

                    // if ($sendTotalMoney <= 0) {

                    //     return ['code' => 400, 'message' => '利润不足'];
                    // }

                    // $groupInfo = GroupModel::getGroupData($orderInfo['room_id']);

                    // if (!empty($groupInfo) && $groupInfo->mid > 0) {

                    //     $memberInfo = MemberModel::getGroupMemeberInfo($groupInfo->mid);


                    //     ///群主分佣开始

                    //     $sendGroupMemberMoney = $sendTotalMoney * 0.6;

                    //     $resData = MemberModel::getMemberInfoByMid(['mid' => $groupInfo->mid]);

                    //     $data = [];

                    //     $data['mid'] = $groupInfo->mid;
                    //     $data['room_id'] = $orderInfo['room_id'];
                    //     $data['amount'] = $sendGroupMemberMoney;
                    //     $data['product_type'] = $orderInfo['good_type'];
                    //     $data['product_id'] = $orderInfo['goods_id'];
                    //     $data['product_name'] = $orderInfo['goods_name'];
                    //     $data['content'] = $orderInfo['ordersn'] . "--群主分佣";
                    //     $data['channel'] = $orderInfo['channel'];
                    //     $data['order_no'] = $orderInfo['ordersn'];
                    //     $data['status'] = 0;
                    //     $data['identity'] = 1;

                    //     MemberModel::insertBonusBecommitted($data);


                    //     if (!empty($memberInfo)) {

                    //         MemberModel::updateGroupMemeberInfoV1($groupInfo->mid, 'peding_account_money', $sendGroupMemberMoney, 1);

                    //     } else {

                    //         $member = [];

                    //         $member['mid'] = $groupInfo->mid;
                    //         $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                    //         $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                    //         $member['type'] = 1;
                    //         $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                    //         $member['group_number'] = 0;
                    //         $member['ivalid_group_number'] = 0;
                    //         $member['leader_num'] = 0;
                    //         $member['ivalid_leader_num'] = 0;
                    //         $member['peding_account_money'] = $sendGroupMemberMoney;

                    //         MemberModel::insertMemberInfo($member);

                    //     }

                    //     ///群主分佣结束


                    //     ///站长分佣开始

                    //     $sendZhanZhangMoney = $sendTotalMoney * 0.1;

                    //     if (isset($resData->parent_id) && $resData->parent_id > 0) {

                    //         $zhanZhangMemberInfo = MemberModel::getGroupMemeberInfo($resData->parent_id);

                    //         if (!empty($zhanZhangMemberInfo) && $zhanZhangMemberInfo['type'] == 2) {

                    //             $data['mid'] = $resData->parent_id;
                    //             $data['amount'] = $sendZhanZhangMoney;
                    //             $data['content'] = $orderInfo['ordersn'] . "--站长分佣";
                    //             $data['identity'] = 2;

                    //             MemberModel::insertBonusBecommitted($data);

                    //             MemberModel::updateGroupMemeberInfoV1($resData->parent_id, 'peding_account_money', $sendZhanZhangMoney, 1);
                    //         }


                    //     }
                    //     ///站长分佣结束


                    //     ///分享者分佣开始

                    //     if ($orderInfo['share_mid'] > 0) {

                    //         $sendShareUserMoney = $sendTotalMoney * 0.1;

                    //         $data['mid'] = $orderInfo['share_mid'];
                    //         $data['amount'] = $sendShareUserMoney;
                    //         $data['content'] = $orderInfo['ordersn'] . "--分享者分佣";
                    //         $data['identity'] = 3;

                    //         MemberModel::insertBonusBecommitted($data);

                    //         $shareMemberInfo = MemberModel::getGroupMemeberInfo($orderInfo['share_mid']);

                    //         if (!empty($shareMemberInfo)) {

                    //             MemberModel::updateGroupMemeberInfoV1($orderInfo['share_mid'], 'peding_account_money', $sendShareUserMoney, 1);

                    //         } else {

                    //             $resData = MemberModel::getMemberInfoByMid(['mid' => $orderInfo['share_mid']]);

                    //             $member = [];

                    //             $member['mid'] = $orderInfo['share_mid'];
                    //             $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                    //             $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                    //             $member['type'] = 1;
                    //             $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                    //             $member['group_number'] = 0;
                    //             $member['ivalid_group_number'] = 0;
                    //             $member['leader_num'] = 0;
                    //             $member['ivalid_leader_num'] = 0;
                    //             $member['peding_account_money'] = $sendShareUserMoney;

                    //             MemberModel::insertMemberInfo($member);

                    //         }
                    //     }


                    // }

                    // return ['code' => 200, 'message' => '操作成功'];
                }

                //C店商品分佣

                if (!empty($orderInfo) && empty($list) && $orderInfo['room_id'] > 1 && $orderInfo['order_three_partner'] == 4 && !in_array($orderInfo['goods_id'], self::SPECIAL_COMMODITIES)) {

                    Log::info('C店商品开始分佣' . $orderInfo['ordersn']);

                    if ($orderInfo['c_commission_role'] == 0 || $orderInfo['c_commission_money'] == 0) {

                        return ['code' => 400, 'message' => '不参与分佣'];
                    }

                    $c_commission_money = $orderInfo['c_commission_money'];//分佣总金额

                    $c_commission_role = explode(",", $orderInfo['c_commission_role']);

                    if (isset($c_commission_role[0]) && $c_commission_role[0] == 1) {

                        $sendShareUserMoney = $c_commission_money * ($c_commission_role[2] / $c_commission_role[1]) * 100; //分享者佣金

                        $sendZhanZhangMoney = $c_commission_money * ($c_commission_role[3] / $c_commission_role[1]) * 100; //群主佣金

                        $sendGroupMemberMoney = $c_commission_money * ($c_commission_role[4] / $c_commission_role[1]) * 100; //站长佣金

                    } elseif (isset($c_commission_role[0]) && $c_commission_role[0] == 2) {

                        $sendShareUserMoney = $c_commission_role[2] * 100 * $orderInfo['goods_num']; //分享者佣金

                        $sendZhanZhangMoney = $c_commission_role[3] * 100 * $orderInfo['goods_num']; //群主佣金

                        $sendGroupMemberMoney = $c_commission_role[4] * 100 * $orderInfo['goods_num']; //站长佣金

                    } else {

                        return ['code' => 400, 'message' => '不参与分佣'];

                    }


                    $data = [];

                    $data['room_id'] = $orderInfo['room_id'];
                    $data['product_type'] = $orderInfo['good_type'];
                    $data['product_id'] = $orderInfo['goods_id'];
                    $data['product_name'] = $orderInfo['goods_name'];
                    $data['channel'] = $orderInfo['channel'];
                    $data['order_no'] = $orderInfo['ordersn'];
                    $data['status'] = 0;


                    $returnData = [];

                    ///分享者分佣开始

                    if ($orderInfo['share_mid'] > 0) {

                        $data['mid'] = $orderInfo['share_mid'];
                        $data['amount'] = $sendShareUserMoney;
                        $data['content'] = $orderInfo['ordersn'] . "--分享者分佣";
                        $data['identity'] = 3;

                        MemberModel::insertBonusBecommitted($data);

                        Log::info('C店商品分享者开始分佣' . $orderInfo['ordersn']);

                        $returnData['c_channel_share']['mid'] = $orderInfo['share_mid'];
                        $returnData['c_channel_share']['amount'] = $sendShareUserMoney;

                        $shareMemberInfo = MemberModel::getGroupMemeberInfo($orderInfo['share_mid']);

                        if (!empty($shareMemberInfo)) {

                            MemberModel::updateGroupMemeberInfoV1($orderInfo['share_mid'], 'peding_account_money', $sendShareUserMoney, 1);

                        } else {

                            $resData = MemberModel::getMemberInfoByMid(['mid' => $orderInfo['share_mid']]);

                            $member = [];

                            $member['mid'] = $orderInfo['share_mid'];
                            $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                            $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                            $member['type'] = 1;
                            $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                            $member['group_number'] = 0;
                            $member['ivalid_group_number'] = 0;
                            $member['leader_num'] = 0;
                            $member['ivalid_leader_num'] = 0;
                            $member['peding_account_money'] = $sendShareUserMoney;

                            MemberModel::insertMemberInfo($member);

                        }

                        $sendShareUserMoney = 0;
                    }

                    ///分享者分佣结束

                    $groupInfo = GroupModel::getGroupData($orderInfo['room_id']);

                    if (!empty($groupInfo) && $groupInfo->mid > 0) {

                        $memberInfo = MemberModel::getGroupMemeberInfo($groupInfo->mid);


                        $resData = MemberModel::getMemberInfoByMid(['mid' => $groupInfo->mid]);


                        ///站长分佣开始

                        if (isset($resData->parent_id) && $resData->parent_id > 0) {

                            $zhanZhangMemberInfo = MemberModel::getGroupMemeberInfo($resData->parent_id);

                            if (!empty($zhanZhangMemberInfo) && $zhanZhangMemberInfo['type'] == 2) {

                                $data['mid'] = $resData->parent_id;
                                $data['amount'] = $sendZhanZhangMoney;
                                $data['content'] = $orderInfo['ordersn'] . "--站长分佣";
                                $data['identity'] = 2;

                                MemberModel::insertBonusBecommitted($data);

                                Log::info('C店商品站长开始分佣' . $orderInfo['ordersn']);

                                $returnData['c_channel_leader']['mid'] = $resData->parent_id;
                                $returnData['c_channel_leader']['amount'] = $sendZhanZhangMoney;

                                MemberModel::updateGroupMemeberInfoV1($resData->parent_id, 'peding_account_money', $sendZhanZhangMoney, 1);

                                $sendZhanZhangMoney = 0;
                            }
                        }
                        ///站长分佣结束


                        ///群主分佣开始
                        $sendGroupMemberMoney = $sendZhanZhangMoney + $sendGroupMemberMoney + $sendShareUserMoney;

                        $data['mid'] = $groupInfo->mid;
                        $data['room_id'] = $orderInfo['room_id'];
                        $data['amount'] = $sendGroupMemberMoney;
                        $data['content'] = $orderInfo['ordersn'] . "--群主分佣";
                        $data['identity'] = 1;

                        MemberModel::insertBonusBecommitted($data);

                        Log::info('C店商品群主开始分佣' . $orderInfo['ordersn']);


                        $returnData['c_channel_group']['mid'] = $groupInfo->mid;
                        $returnData['c_channel_group']['amount'] = $sendGroupMemberMoney;


                        if (!empty($memberInfo)) {

                            MemberModel::updateGroupMemeberInfoV1($groupInfo->mid, 'peding_account_money', $sendGroupMemberMoney, 1);

                        } else {

                            $member = [];

                            $member['mid'] = $groupInfo->mid;
                            $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                            $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                            $member['type'] = 1;
                            $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                            $member['group_number'] = 0;
                            $member['ivalid_group_number'] = 0;
                            $member['leader_num'] = 0;
                            $member['ivalid_leader_num'] = 0;
                            $member['peding_account_money'] = $sendGroupMemberMoney;

                            MemberModel::insertMemberInfo($member);

                        }

                        ///群主分佣结束


                        //平台补贴开始
                        if (isset($orderInfo['c_platform_subsidy']) && $orderInfo['c_platform_subsidy'] > 0) {

                            $data['amount'] = $orderInfo['c_platform_subsidy'] * 100;
                            $data['content'] = $orderInfo['ordersn'] . "--平台补贴";
                            $data['identity'] = 4;

                            $returnData['c_platform_subsidy']['mid'] = $groupInfo->mid;
                            $returnData['c_platform_subsidy']['amount'] = $data['amount'];

                            MemberModel::insertBonusBecommitted($data);

                            MemberModel::updateGroupMemeberInfoV1($groupInfo->mid, 'peding_account_money', $data['amount'], 1);

                        }
                        //平台补贴结束

                    }


                    $commission_info = curlPost('https://mallapi.daren.tech/mall/bookOrderCommission', ['ordersn' => $orderInfo['ordersn'], 'commission_info' => json_encode($returnData)]);

                    Log::info('C店商品分佣推单' . $orderInfo['ordersn'] . $commission_info);

                    return ['code' => 200, 'message' => '操作成功'];


                }


                if (!empty($orderInfo) && empty($list) && $orderInfo['room_id'] == 1 && $orderInfo['order_three_partner'] != 4 && !in_array($orderInfo['goods_id'], self::SPECIAL_COMMODITIES)) {

                    self::newCommssionRule($ordersn,1);
                    // 不是C店
                    // $sendTotalMoney = 0;//毛利润

                    // switch ($orderInfo['good_type']) {
                    //     case 1:

                    //         $growth_value = MemberModel::getProductGrowthValue($orderInfo['goods_id']);

                    //         if ($growth_value > 0) {

                    //             //当前商品成长值大于0时 总分佣 = 成长值/2
                    //             $sendTotalMoney = $growth_value / 2 * 100;

                    //         } else {

                    //             //当前商品成长值等于0时 总分佣 = 支付价-商品进货价*商品数量-优惠价价格
                    //             $sendTotalMoney = $orderInfo['actual_price'] - $orderInfo['goods_price_buy'] * $orderInfo['goods_num'] - $orderInfo['goods_coupon_average'] * 100;

                    //         }

                    //         break;

                    //     default:

                    //         $sendTotalMoney = $orderInfo['commission'];

                    //         break;
                    // }

                    // if ($sendTotalMoney <= 0) {

                    //     return ['code' => 400, 'message' => '利润不足'];
                    // }

                    // $data['product_type'] = $orderInfo['good_type'];
                    // $data['product_id'] = $orderInfo['goods_id'];
                    // $data['product_name'] = $orderInfo['goods_name'];
                    // $data['channel'] = $orderInfo['channel'];
                    // $data['order_no'] = $orderInfo['ordersn'];
                    // $data['status'] = 0;

                    // //分享者分佣开始
                    //     if ($orderInfo['share_mid'] > 0) {
                    //         $sendShareUserMoney = $sendTotalMoney * 0.1;

                    //         $data['mid'] = $orderInfo['share_mid'];
                    //         $data['amount'] = $sendShareUserMoney;
                    //         $data['content'] = $orderInfo['ordersn'] . "--分享者分佣";
                    //         $data['identity'] = 3;

                    //         MemberModel::insertBonusBecommitted($data);

                    //         $shareMemberInfo = MemberModel::getGroupMemeberInfo($orderInfo['share_mid']);

                    //         if (!empty($shareMemberInfo)) {

                    //             MemberModel::updateGroupMemeberInfoV1($orderInfo['share_mid'], 'peding_account_money', $sendShareUserMoney, 1);

                    //         } else {

                    //             $resData = MemberModel::getMemberInfoByMid(['mid' => $orderInfo['share_mid']]);

                    //             $member = [];

                    //             $member['mid'] = $orderInfo['share_mid'];
                    //             $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                    //             $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                    //             $member['type'] = 1;
                    //             $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                    //             $member['group_number'] = 0;
                    //             $member['ivalid_group_number'] = 0;
                    //             $member['leader_num'] = 0;
                    //             $member['ivalid_leader_num'] = 0;
                    //             $member['peding_account_money'] = $sendShareUserMoney;

                    //             MemberModel::insertMemberInfo($member);
                    //         }
                    //     }

                    //     //不是群主，上级拿60%分佣
                    //     $parent_user_info = MemberModel::geParentIdByMid($orderInfo['member_id']);
                    //     if ($parent_user_info) {
                    //         $sendShareUserMoney = $sendTotalMoney * 0.6;
                    //         $data['mid'] = $parent_user_info->parent_id;
                    //         $data['amount'] = $sendShareUserMoney;
                    //         $data['content'] = $orderInfo['ordersn'] . "--上级分佣";
                    //         $data['identity'] = 5;

                    //         MemberModel::insertBonusBecommitted($data);

                    //         $shareMemberInfo = MemberModel::getGroupMemeberInfo($data['mid']);

                    //         if (!empty($shareMemberInfo)) {

                    //             MemberModel::updateGroupMemeberInfoV1($data['mid'], 'peding_account_money', $sendShareUserMoney, 1);

                    //         } else {

                    //             $resData = MemberModel::getMemberInfoByMid(['mid' => $data['mid']]);

                    //             $member = [];

                    //             $member['mid'] = $data['mid'];
                    //             $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                    //             $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                    //             $member['type'] = 1;
                    //             $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                    //             $member['group_number'] = 0;
                    //             $member['ivalid_group_number'] = 0;
                    //             $member['leader_num'] = 0;
                    //             $member['ivalid_leader_num'] = 0;
                    //             $member['peding_account_money'] = $sendShareUserMoney;

                    //             MemberModel::insertMemberInfo($member);

                    //         }
                    //     }
                    


                    // return ['code' => 200, 'message' => '操作成功'];
                }

                if (!empty($orderInfo) && empty($list) && $orderInfo['room_id'] == 1 && $orderInfo['order_three_partner'] == 4 && !in_array($orderInfo['goods_id'], self::SPECIAL_COMMODITIES)) {
                    Log::info('C店商品开始分佣' . $orderInfo['ordersn']);

                    if ($orderInfo['c_commission_role'] == 0 || $orderInfo['c_commission_money'] == 0) {

                        return ['code' => 400, 'message' => '不参与分佣'];
                    }

                    $c_commission_money = $orderInfo['c_commission_money'];//分佣总金额

                    $c_commission_role = explode(",", $orderInfo['c_commission_role']);

                    if (isset($c_commission_role[0]) && $c_commission_role[0] == 1) {

                        $sendShareUserMoney = $c_commission_money * ($c_commission_role[2] / $c_commission_role[1]) * 100; //分享者佣金

                        $sendZhanZhangMoney = $c_commission_money * ($c_commission_role[3] / $c_commission_role[1]) * 100; //群主佣金

                        $sendGroupMemberMoney = $c_commission_money * ($c_commission_role[4] / $c_commission_role[1]) * 100; //站长佣金

                    } elseif (isset($c_commission_role[0]) && $c_commission_role[0] == 2) {

                        $sendShareUserMoney = $c_commission_role[2] * 100 * $orderInfo['goods_num']; //分享者佣金

                        $sendZhanZhangMoney = $c_commission_role[3] * 100 * $orderInfo['goods_num']; //群主佣金

                        $sendGroupMemberMoney = $c_commission_role[4] * 100 * $orderInfo['goods_num']; //站长佣金

                    } else {

                        return ['code' => 400, 'message' => '不参与分佣'];

                    }


                    $data = [];

                    $data['room_id'] = $orderInfo['room_id'];
                    $data['product_type'] = $orderInfo['good_type'];
                    $data['product_id'] = $orderInfo['goods_id'];
                    $data['product_name'] = $orderInfo['goods_name'];
                    $data['channel'] = $orderInfo['channel'];
                    $data['order_no'] = $orderInfo['ordersn'];
                    $data['status'] = 0;


                    $returnData = [];

                        //分享者分佣开始
                        if ($orderInfo['share_mid'] > 0) {
                            $data['mid'] = $orderInfo['share_mid'];
                            $data['amount'] = $sendShareUserMoney;
                            $data['content'] = $orderInfo['ordersn'] . "--分享者分佣";
                            $data['identity'] = 3;

                            MemberModel::insertBonusBecommitted($data);

                            Log::info('C店商品分享者开始分佣' . $orderInfo['ordersn']);

                            $returnData['c_channel_share']['mid'] = $orderInfo['share_mid'];
                            $returnData['c_channel_share']['amount'] = $sendShareUserMoney;

                            $shareMemberInfo = MemberModel::getGroupMemeberInfo($orderInfo['share_mid']);

                            if (!empty($shareMemberInfo)) {

                                MemberModel::updateGroupMemeberInfoV1($orderInfo['share_mid'], 'peding_account_money', $sendShareUserMoney, 1);

                            } else {

                                $resData = MemberModel::getMemberInfoByMid(['mid' => $orderInfo['share_mid']]);

                                $member = [];

                                $member['mid'] = $orderInfo['share_mid'];
                                $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                                $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                                $member['type'] = 1;
                                $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                                $member['group_number'] = 0;
                                $member['ivalid_group_number'] = 0;
                                $member['leader_num'] = 0;
                                $member['ivalid_leader_num'] = 0;
                                $member['peding_account_money'] = $sendShareUserMoney;

                                MemberModel::insertMemberInfo($member);

                                $sendShareUserMoney = 0;
                            }

                        }
                        //不是群主，上级拿60%分佣
                        $parent_user_info = MemberModel::geParentIdByMid($orderInfo['member_id']);
                        if ($parent_user_info) {
                            $sendShareUserMoney = $sendZhanZhangMoney;
                            $data['mid'] = $parent_user_info->parent_id;
                            $data['amount'] = $sendShareUserMoney;
                            $data['content'] = $orderInfo['ordersn'] . "--上级分佣";
                            $data['identity'] = 5;

                            MemberModel::insertBonusBecommitted($data);

                            $shareMemberInfo = MemberModel::getGroupMemeberInfo($data['mid']);

                            $returnData['c_channel_group']['mid'] = $data['mid'];
                            $returnData['c_channel_group']['amount'] = $sendGroupMemberMoney;
                            if (!empty($shareMemberInfo)) {
                                MemberModel::updateGroupMemeberInfoV1($data['mid'], 'peding_account_money', $sendShareUserMoney, 1);
                            } else {
                                $resData = MemberModel::getMemberInfoByMid(['mid' => $data['mid']]);

                                $member = [];

                                $member['mid'] = $data['mid'];
                                $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                                $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                                $member['type'] = 1;
                                $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                                $member['group_number'] = 0;
                                $member['ivalid_group_number'] = 0;
                                $member['leader_num'] = 0;
                                $member['ivalid_leader_num'] = 0;
                                $member['peding_account_money'] = $sendShareUserMoney;

                                MemberModel::insertMemberInfo($member);

                            }

                            //平台补贴开始
                            if (isset($orderInfo['c_platform_subsidy']) && $orderInfo['c_platform_subsidy'] > 0) {

                                $data['amount'] = $orderInfo['c_platform_subsidy'] * 100;
                                $data['content'] = $orderInfo['ordersn'] . "--平台补贴";
                                $data['identity'] = 4;

                                $returnData['c_platform_subsidy']['mid'] = $parent_user_info->parent_id;
                                $returnData['c_platform_subsidy']['amount'] = $data['amount'];

                                MemberModel::insertBonusBecommitted($data);

                                MemberModel::updateGroupMemeberInfoV1($parent_user_info->parent_id, 'peding_account_money', $data['amount'], 1);

                            }
                            //平台补贴结束
                        }
                    

                    $commission_info = curlPost('https://mallapi.daren.tech/mall/bookOrderCommission', ['ordersn' => $orderInfo['ordersn'], 'commission_info' => json_encode($returnData)]);

                    Log::info('C店商品分佣推单' . $orderInfo['ordersn'] . $commission_info);

                    return ['code' => 200, 'message' => '操作成功'];
                }


                if(!empty($orderInfo) && empty($list) && $orderInfo['order_three_partner'] != 4 && in_array($orderInfo['goods_id'], self::SPECIAL_COMMODITIES)){

                    Log::info('爆款商品开始分佣' . $orderInfo['ordersn']);                    

                    $buyer_identity = MemberModel::isWork($orderInfo['member_id']);

                    if($buyer_identity>0){

                        $data = [];

                        $data['room_id'] = $orderInfo['room_id'];
                        $data['product_type'] = $orderInfo['good_type'];
                        $data['product_id'] = $orderInfo['goods_id'];
                        $data['product_name'] = $orderInfo['goods_name'];
                        $data['channel'] = $orderInfo['channel'];
                        $data['order_no'] = $orderInfo['ordersn'];
                        $data['status'] = 0;
                        $data['mid'] = $orderInfo['member_id'];
                        $data['amount'] = 6000;
                        $data['content'] = $orderInfo['ordersn'] . "--爆款商品创客自购分佣";
                        $data['identity'] = 5;

                        MemberModel::insertBonusBecommitted($data);

                        $parentMemberInfo = MemberModel::getGroupMemeberInfo($data['mid']);

                        if (!empty($parentMemberInfo)) {
                            
                            MemberModel::updateGroupMemeberInfoV1($data['mid'], 'peding_account_money',6000, 1);

                        }else{

                                $resData = MemberModel::getMemberInfoByMid(['mid' => $data['mid']]);

                                $member = [];

                                $member['mid'] = $data['mid'];
                                $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                                $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                                $member['type'] = 1;
                                $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                                $member['group_number'] = 0;
                                $member['ivalid_group_number'] = 0;
                                $member['leader_num'] = 0;
                                $member['ivalid_leader_num'] = 0;
                                $member['peding_account_money'] = $sendShareUserMoney;

                                MemberModel::insertMemberInfo($member);


                        }


                        Log::info('爆款商品分佣结束' . $orderInfo['ordersn']);

                        return ['code' => 200, 'message' => '操作成功'];

                        break;

                    }

                    $parent_user_info = MemberModel::geParentIdByMid($orderInfo['member_id']);

                    $data = [];

                    $data['room_id'] = $orderInfo['room_id'];
                    $data['product_type'] = $orderInfo['good_type'];
                    $data['product_id'] = $orderInfo['goods_id'];
                    $data['product_name'] = $orderInfo['goods_name'];
                    $data['channel'] = $orderInfo['channel'];
                    $data['order_no'] = $orderInfo['ordersn'];
                    $data['status'] = 0;

                    if ($parent_user_info&&$parent_user_info->parent_id>0) {
                        
                        $parent_identity = MemberModel::isWork($parent_user_info->parent_id);

                        $sendShareUserMoney = $parent_identity?6000:4000;
                        $data['mid'] = $parent_user_info->parent_id;
                        $data['amount'] = $sendShareUserMoney;
                        $data['content'] = $orderInfo['ordersn'] . "--上级分佣";
                        $data['identity'] = 5;

                        MemberModel::insertBonusBecommitted($data);

                        $parentMemberInfo = MemberModel::getGroupMemeberInfo($data['mid']);

                        if (!empty($parentMemberInfo)) {
                            
                            MemberModel::updateGroupMemeberInfoV1($data['mid'], 'peding_account_money', $sendShareUserMoney, 1);

                        }else{

                                $resData = MemberModel::getMemberInfoByMid(['mid' => $data['mid']]);

                                $member = [];

                                $member['mid'] = $data['mid'];
                                $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                                $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                                $member['type'] = 1;
                                $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                                $member['group_number'] = 0;
                                $member['ivalid_group_number'] = 0;
                                $member['leader_num'] = 0;
                                $member['ivalid_leader_num'] = 0;
                                $member['peding_account_money'] = $sendShareUserMoney;

                                MemberModel::insertMemberInfo($member);


                        }

                        $parent_v1_mid = self::findChuangKeMid($data['mid']);

                        if ($parent_identity==0&&$parent_v1_mid>0) {
                            
                            $sendShareUserMoneyV1 = 2000;
                            $data['mid'] = $parent_v1_mid;
                            $data['amount'] = $sendShareUserMoneyV1;
                            $data['content'] = $orderInfo['ordersn'] . "--上级的上级分佣";
                            $data['identity'] = 5;

                            MemberModel::insertBonusBecommitted($data);

                            $parentMemberInfoV1 = MemberModel::getGroupMemeberInfo($data['mid']);

                            if (!empty($parentMemberInfoV1)) {
                                
                                MemberModel::updateGroupMemeberInfoV1($data['mid'], 'peding_account_money', $sendShareUserMoneyV1, 1);

                            }else{

                                    $resData = MemberModel::getMemberInfoByMid(['mid' => $data['mid']]);

                                    $member = [];

                                    $member['mid'] = $data['mid'];
                                    $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                                    $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                                    $member['type'] = 1;
                                    $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                                    $member['group_number'] = 0;
                                    $member['ivalid_group_number'] = 0;
                                    $member['leader_num'] = 0;
                                    $member['ivalid_leader_num'] = 0;
                                    $member['peding_account_money'] = $sendShareUserMoneyV1;

                                    MemberModel::insertMemberInfo($member);


                            }


                            $parent_user_info_v1 = MemberModel::geParentIdByMid($data['mid']);

                        }

                    }


                    Log::info('爆款商品分佣结束' . $orderInfo['ordersn']);

                    return ['code' => 200, 'message' => '操作成功'];
                }

                # code...
                break;

            case 2:
                //订单退款，处理预估分佣 或实际到账分佣
                $list = MemberModel::getBonusBecommittedStatusByOrdersn($ordersn);

                if (!empty($list)) {

                    foreach ($list as $key => $item) {

                        if ($item['status'] == 0) {


                            $data = [];

                            $data['id'] = $item['id'];
                            $data['status'] = 2;

                            $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                            if ($updateStauts) {

                                MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $item['amount'], 2);

                            }

                        }


                        if ($item['status'] == 1) {


                            $data = [];

                            $data['id'] = $item['id'];
                            $data['status'] = 2;

                            $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                            if ($updateStauts) {

                                MemberModel::updateGroupMemeberInfoV1($item['mid'], 'cash_money', $item['amount'], 2);

                                MemberModel::updateGroupMemeberInfoV1($item['mid'], 'total_money', $item['amount'], 2);

                            }
                        }


                    }

                    return ['code' => 200, 'message' => '操作成功'];

                }

                break;
            case 3:
                //订单已收货 预估分佣到账

                $list = MemberModel::getBonusBecommittedStatusByOrdersn($ordersn);

                if (!empty($list)) {

                    foreach ($list as $key => $item) {

                        if ($item['status'] == 0) {


                            $data = [];

                            $data['id'] = $item['id'];
                            $data['status'] = 1;

                            $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                            if ($updateStauts) {

                                MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $item['amount'], 2);

                                MemberModel::updateGroupMemeberInfoV1($item['mid'], 'cash_money', $item['amount'], 1);

                                MemberModel::updateGroupMemeberInfoV1($item['mid'], 'total_money', $item['amount'], 1);
                            }

                        }


                    }

                    return ['code' => 200, 'message' => '操作成功'];

                }
                # code...
                break;

            default:
                # code...
                break;
        }


        return ['code' => 400, 'message' => '更改失败'];

    }

    /**
     * 发放用户冻结奖金
     * @param $param mid 群主id
     * @return array
     */
    public static function sendUserFrozenMoney()
    {

        $list = MemberModel::sendUserFrozenMoney();

        if (empty($list)) {

            return [];
        }


        $times = strtotime("-7 days");

        foreach ($list as $key => $item) {

            $created_time = strtotime($item['created_time']);

            if ($item['type'] == 1 && $created_time < $times) {


                $data['mid'] = $item['mid'];
                $data['type'] = 2;
                $data['bonus_type'] = $item['bonus_type'];
                $data['group_type'] = $item['group_type'];
                $data['amount'] = $item['amount'];

                $id = MemberModel::insertMemberBonus($data);


                if ($id) {

                    MemberModel::updateGroupMemeberInfoV1($item['mid'], 'frozen_money', $item['amount'], 2);
                    MemberModel::updateGroupMemeberInfoV1($item['mid'], 'cash_money', $item['amount'], 1);
                }

                Log::info('用户奖金发放' . $item['mid'] . json_encode($item));

            }
        }
    }


    /**
     * 回滚C店订单分佣
     * @param $param mid 群主id
     * @return array
     */
    public static function updateReturnOrderList()
    {


        $Orderlist = MemberModel::getReturnOrderList();

        if (empty($Orderlist)) {

            return ['code' => 400, 'message' => '数据为空'];
        }

        foreach ($Orderlist as $key => $value) {

            $list = MemberModel::getBonusBecommittedStatusByOrdersn($value['order_no']);

            if (!empty($list)) {

                foreach ($list as $k => $item) {

                    if ($item['status'] == 0 && $item['identity'] == 1) {


                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $item['amount'], 2);

                        Log::info('回滚群主分佣' . $item['mid'] . json_encode($item));

                    }

                    if ($item['status'] == 0 && $item['identity'] == 2) {


                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $item['amount'], 1);

                        Log::info('增加站长订单分佣' . $item['mid'] . json_encode($item));

                    }


                }


            }

        }

        return ['code' => 200, 'message' => '操作成功'];

    }


    /**
     * 群主预估收入
     * @param array $condition
     * @return mixed
     */
    public static function getGrouoMemberMoneyList($param)
    {
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $pageSize = !empty($param["pageSize"]) ? $param["pageSize"] : 10;

        $list = MemberModel::getGrouoMemberMoneyList($param, $page, $pageSize);
        $total = MemberModel::getGrouoMemberMoneyCount($param, $page, $pageSize);

        $data = [];

        foreach ($list as $key => $value) {

            $data[$key]['nickName'] = $value['nickName'];
            $data[$key]['groupNum'] = empty($value['groupNum']) ? 0 : $value['groupNum'];
            $data[$key]['bonusAmount'] = empty($value['bonusAmount']) ? 0 : $value['bonusAmount'] / 100;
            $data[$key]['TgOrderCount'] = empty($value['TgOrderCount']) ? 0 : $value['TgOrderCount'];
            $data[$key]['TgOrderAmount'] = empty($value['TgOrderAmount']) ? 0 : $value['TgOrderAmount'] / 100;
            $data[$key]['ZgOrderCount'] = empty($value['ZgOrderCount']) ? 0 : $value['ZgOrderCount'];
            $data[$key]['cashMoney'] = empty($value['cashMoney']) ? 0 : $value['cashMoney'] / 100;
            $data[$key]['drawnMoney'] = empty($value['drawnMoney']) ? 0 : $value['drawnMoney'] / 100;
            $data[$key]['pedingAccountMoney'] = empty($value['pedingAccountMoney']) ? 0 : $value['pedingAccountMoney'] / 100;

        }

        return ['code' => 200, 'message' => 'success', 'data' => ['total' => $total, 'page' => $page, 'data' => $data]];
    }

    /**
     * 资产管理
     * @param array $condition
     * @return mixed
     */
    public static function getAssetManagement($param)
    {

        $data = [];

        $data['groupNum'] = MemberModel::getGrouoTotalCount($param);
        $data['groupMemberNum'] = MemberModel::getGrouoMemberTotalCount($param);
        $data['grouoManagNum'] = MemberModel::getGrouoManageTotalCount($param);

        $orderInfo = MemberModel::getGroupOrderCount($param);

        $data['orderNum'] = isset($orderInfo['count_1']) ? $orderInfo['count_1'] : 0;
        $data['orderGmv'] = isset($orderInfo['actual_price']) ? $orderInfo['actual_price'] : 0;
        $data['jiangliMoney'] = 0;
        $data['shouYiMoney'] = 0;

        return ['code' => 200, 'message' => 'success', 'data' => $data];
    }

    /**
     * 错误分佣订单回滚重新分佣
     * @param array $condition
     * @return mixed
     */
    public static function callbackOrderBonus($param)
    {
        $list = MemberModel::callbackOrderBonus();

        foreach ($list as $key => $value) {

            $growth_value = MemberModel::getProductGrowthValue($value['goods_id']);

            if ($growth_value > 0) {

                //当前商品成长值大于0时 总分佣 = 成长值/2
                $sendTotalMoney = $growth_value / 2 * 100;

            } else {

                //当前商品成长值等于0时 总分佣 = 支付价-商品进货价*商品数量-优惠价价格
                $sendTotalMoney = $value['actual_price'] - $value['goods_price_buy'] * $value['goods_num'] - $value['goods_coupon_average'] * 100;

            }

            if ($sendTotalMoney < 0) {

                $sendTotalMoney = 0;
            }

            $bonusList = MemberModel::getBonusBecommittedStatusByOrdersn($value['ordersn']);

            if (!empty($bonusList)) {

                foreach ($bonusList as $k => $item) {

                    if ($item['identity'] == 1) {

                        $sendGroupMemberMoney = $sendTotalMoney * 0.6;

                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $sendGroupMemberMoney, 1);

                        $data = [];

                        $data['id'] = $item['id'];
                        $data['amount'] = $sendGroupMemberMoney;

                        $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                        Log::info('重新分佣' . $k . "_" . $data['amount'] . "_" . json_encode($item));
                    }

                    if ($item['identity'] == 2) {

                        $sendGroupMemberMoney2 = $sendTotalMoney * 0.1;

                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $sendGroupMemberMoney2, 1);

                        $data = [];

                        $data['id'] = $item['id'];
                        $data['amount'] = $sendGroupMemberMoney2;

                        $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                        Log::info('重新分佣' . $k . "_" . $data['amount'] . "_" . json_encode($item));

                    }

                    if ($item['identity'] == 3) {

                        $sendGroupMemberMoney3 = $sendTotalMoney * 0.1;

                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $sendGroupMemberMoney3, 1);

                        $data = [];

                        $data['id'] = $item['id'];
                        $data['amount'] = $sendGroupMemberMoney3;

                        $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                        Log::info('重新分佣' . $k . "_" . $data['amount'] . "_" . json_encode($item));
                    }

                }

            }


        }


    }


    /**
     * 修正订单数据
     * @param array $condition
     * @return mixed
     */
    public static function updateZhidingOrderMobileData($param)
    {
    
        $orderList = MemberModel::getZhidingOrderList();


        foreach ($orderList as $key => $v) {
            
           $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' =>$v['member_id']]);

           $data = [];

           $data['buyer_phone'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
           $data['buyer_nickname'] = isset($zd_memInfo['nickName']) ? $zd_memInfo['nickName'] : '';

           $result = MemberModel::updateZhidingOrderMobile($v['id'],$data);
        }

    }

    /**
     * 新的分佣规则
     * @param array $condition
     * @return mixed
     */
    public static function newCommssionRule($ordersn,$type){

        if ($ordersn == 0 || $type == 0) {

            return ['code' => 400, 'message' => '参数不正确'];
        }

        switch ($type) {

                    case 1://存入预估佣金


                        $orderInfo = MemberModel::getOrderInfoByOrdersn($ordersn);

                        if($orderInfo['member_id']==0||$orderInfo['member_id']==''){


                            return ['code' => 400, 'message' => '不参与分佣'];
                        }


                        $list = MemberModel::getBonusBecommittedStatusByOrdersn($ordersn);


                        //悦淘普通商品分佣

                        if (!empty($orderInfo) && empty($list) && $orderInfo['room_id'] > 0 && $orderInfo['order_three_partner'] != 4) {


                            $sendTotalMoney = 0;//毛利润

                            switch ($orderInfo['good_type']) {
                                case 1:

                                    $growth_value = MemberModel::getProductGrowthValue($orderInfo['goods_id']);


                                    if ($growth_value > 0) {

                                        //当前商品成长值大于0时 总分佣 = 成长值/2
                                        $sendTotalMoney = $growth_value / 2 * 100;

                                    } else {

                                        //当前商品成长值等于0时 总分佣 = 支付价-商品进货价*商品数量-优惠价价格
                                        $sendTotalMoney = $orderInfo['actual_price'] - $orderInfo['goods_price_buy'] * $orderInfo['goods_num'] - $orderInfo['goods_coupon_average'] * 100;

                                    }


                                    break;

                                default:

                                    $sendTotalMoney = $orderInfo['commission'];

                                    break;
                            }


                            if ($sendTotalMoney <= 0) {

                                return ['code' => 400, 'message' => '利润不足'];
                            }

                            $isCommissionForGroupMember = MemberModel::isRuleGroup($orderInfo['room_id']);

                            //判断该群是否为三天之内新建群 若是群主分佣100%
                            if(!empty($isCommissionForGroupMember)&&$isCommissionForGroupMember['mid']>0){

                                $memberInfo = MemberModel::getGroupMemeberInfo($isCommissionForGroupMember['mid']);

                                $resData = MemberModel::getMemberInfoByMid(['mid' => $isCommissionForGroupMember['mid']]);

                                $data = [];

                                $data['mid'] = $isCommissionForGroupMember['mid'];
                                $data['room_id'] = $orderInfo['room_id'];
                                $data['amount'] = $sendTotalMoney;
                                $data['product_type'] = $orderInfo['good_type'];
                                $data['product_id'] = $orderInfo['goods_id'];
                                $data['product_name'] = $orderInfo['goods_name'];
                                $data['content'] = $orderInfo['ordersn'] . "--群主新建群分佣";
                                $data['channel'] = $orderInfo['channel'];
                                $data['order_no'] = $orderInfo['ordersn'];
                                $data['status'] = 0;
                                $data['identity'] = 1;

                                MemberModel::insertBonusBecommitted($data);

                                if (!empty($memberInfo)) {

                                    MemberModel::updateGroupMemeberInfoV1($isCommissionForGroupMember['mid'], 'peding_account_money', $sendTotalMoney, 1);

                                } else {

                                    $member = [];

                                    $member['mid'] = $isCommissionForGroupMember['mid'];
                                    $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                                    $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                                    $member['type'] = 1;
                                    $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                                    $member['group_number'] = 0;
                                    $member['ivalid_group_number'] = 0;
                                    $member['leader_num'] = 0;
                                    $member['ivalid_leader_num'] = 0;
                                    $member['peding_account_money'] = $sendTotalMoney;
                                    $member['user_identity'] = 1;

                                    MemberModel::insertMemberInfo($member);

                                }

                            }else{

                                $userIndenty = self::getUserIndenty($orderInfo['member_id']);


                                if($userIndenty>0){
                                    //用户角色不是普通用户时 自购返佣
                                    $userIndentyRule = self::USER_IDENTITY_RULE;

                                    $userIndentyDesc = self::USER_IDENTITY_DESC;

                                    $sendUserMoney = $sendTotalMoney*$userIndentyRule[$userIndenty];

                                    $data = [];

                                    $data['mid'] = $orderInfo['member_id'];
                                    $data['room_id'] = $orderInfo['room_id'];
                                    $data['amount'] = $sendUserMoney;
                                    $data['product_type'] = $orderInfo['good_type'];
                                    $data['product_id'] = $orderInfo['goods_id'];
                                    $data['product_name'] = $orderInfo['goods_name'];
                                    $data['content'] = $orderInfo['ordersn'] . "--".$userIndentyDesc[$userIndenty]."自购分佣";
                                    $data['channel'] = $orderInfo['channel'];
                                    $data['order_no'] = $orderInfo['ordersn'];
                                    $data['status'] = 0;
                                    $data['identity'] = 0;
                                    $data['new_identity'] = $userIndenty;

                                    MemberModel::insertBonusBecommitted($data);

                                    MemberModel::updateGroupMemeberInfoV1($orderInfo['member_id'], 'peding_account_money', $sendUserMoney, 1);


                                }else{

                                    //用户角色为普通用户时 返佣给上级
                                    $resData = MemberModel::getMemberInfoByMid(['mid' => $orderInfo['member_id']]);

                                    $parentUserIndenty = self::getUserIndenty($resData->parent_id);

                                    if($parentUserIndenty>0){
                                        
                                        $userIndentyRule = self::USER_IDENTITY_RULE;

                                        $userIndentyDesc = self::USER_IDENTITY_DESC;

                                        $sendUserMoney = $sendTotalMoney*$userIndentyRule[$parentUserIndenty];


                                        $data = [];

                                        $data['mid'] =  $resData->parent_id;
                                        $data['room_id'] = $orderInfo['room_id'];
                                        $data['amount'] = $sendUserMoney;
                                        $data['product_type'] = $orderInfo['good_type'];
                                        $data['product_id'] = $orderInfo['goods_id'];
                                        $data['product_name'] = $orderInfo['goods_name'];
                                        $data['content'] = $orderInfo['ordersn'] . "--".$userIndentyDesc[$parentUserIndenty]."上级分佣";
                                        $data['channel'] = $orderInfo['channel'];
                                        $data['order_no'] = $orderInfo['ordersn'];
                                        $data['status'] = 0;
                                        $data['identity'] = 0;
                                        $data['new_identity'] = $parentUserIndenty;

                                        MemberModel::insertBonusBecommitted($data);                                    

                                        MemberModel::updateGroupMemeberInfoV1($resData->parent_id, 'peding_account_money', $sendUserMoney, 1);
                                    }


                                }


                            }

                            return ['code' => 200, 'message' => '操作成功'];
                        }


                        break;
                    case 2: //佣金退款
                        # code...
                        break;
                    case 3: //佣金到账



                        //订单已收货 预估分佣到账

                        $list = MemberModel::getBonusBecommittedStatusByOrdersn($ordersn);

                        if (!empty($list)) {

                            foreach ($list as $key => $item) {

                                if ($item['status'] == 0) {


                                    $data = [];

                                    $data['id'] = $item['id'];
                                    $data['status'] = 1;

                                    $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                                    if ($updateStauts) {

                                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $item['amount'], 2);

                                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'cash_money', $item['amount'], 1);

                                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'total_money', $item['amount'], 1);
                                    }

                                }


                            }

                            return ['code' => 200, 'message' => '操作成功'];

                        }

                        break;
                    
                    default:
                        # code...
                        break;
                }        

    }

    /**
     * 获取用户身份
     * @param member_id
     * @return mixed
     */
    public static function getUserIndenty($member_id=0){

        $memberInfo = MemberModel::getGroupMemeberInfo($member_id);

        if(!empty($memberInfo)){

            if($memberInfo['user_identity']==0){

                //查看该用户是否为五十人群群主
                $isGroupMember = MemberModel::isGroupMemberV1($member_id);

                if(empty($isGroupMember)){

                    return $memberInfo['user_identity'];
                }else{

                    $data = [];

                    $data['mid'] = $member_id;
                    $data['user_identity'] = 1;

                    $result = MemberModel::updateGroupMemeberInfo($data);

                    if($result){

                        return $data['user_identity'];
                    }

                }

            }else{

                return $memberInfo['user_identity'];
            }

        }else{

            //查看该用户是否为五十人群群主
            $isGroupMember = MemberModel::isGroupMemberV1($member_id);

            $resData = MemberModel::getMemberInfoByMid(['mid' => $member_id]);

            $data = [];

            $data['mid'] = $member_id;
            $data['nick_name'] = $resData->nickname;
            $data['mobile'] = $resData->mobile;
            $data['type'] = empty($isGroupMember)?0:1;
            $data['parent_id'] = $resData->parent_id;
            $data['group_number'] = 0;
            $data['ivalid_group_number'] = 0;
            $data['leader_num'] = 0;
            $data['ivalid_leader_num'] = 0;
            $data['user_identity'] = empty($isGroupMember)?0:1;

            $result = MemberModel::insertMemberInfo($data);

            if($result){

                return $data['user_identity'];
            }


        }
    }

    /**
     * 增加角色总的GMV
     * @param ordersn
     * @return mixed
     */
    private static function insertTotalUpgradeMoney($ordersn,$type=1){

        $orderInfo = MemberModel::getOrderInfoByOrdersnV2($ordersn);

        if(empty($orderInfo)){

            return ['code'=>400,'msg'=>'订单不存在'];
        }

        $memberInfo = MemberModel::getGroupMemeberInfo($orderInfo['member_id']);

        if($type==2){

            $userId = $memberInfo['parent_id'];

            $memberInfo = MemberModel::getGroupMemeberInfo($memberInfo['parent_id']);

        }

        if(!empty($memberInfo)){

            MemberModel::updateGroupMemeberInfoV1($memberInfo['mid'], 'upgrade_money', $orderInfo['actual_price'], 1);

            self::userIdentityUp($memberInfo['mid']);

            if($type==1&&$memberInfo['parent_id']>0){

                $parent_info = MemberModel::getGroupMemeberInfo($memberInfo['parent_id']);

                if(empty($parent_info)){

                    $parentIsGroupMember = MemberModel::isGroupMemberV1($memberInfo['parent_id']);

                    $parentUserIndenty = empty($parentIsGroupMember)?0:1;

                }else{

                    $parentUserIndenty = $parent_info['user_identity'];

                }

                if($parentUserIndenty>=$memberInfo['user_identity']){

                    self::insertTotalUpgradeMoney($ordersn,2);
                }

            }


        }else{

            if($type==1){

              $mid = $orderInfo['member_id'];

            }else{

              $mid = $userId;
            }

            //查看该用户是否为五十人群群主
            $isGroupMember = MemberModel::isGroupMemberV1($mid);

            $resData = MemberModel::getMemberInfoByMid(['mid' => $mid]);

            $data = [];

            $data['mid'] = $mid;
            $data['nick_name'] = $resData->nickname;
            $data['mobile'] = $resData->mobile;
            $data['type'] = empty($isGroupMember)?0:1;
            $data['parent_id'] = $resData->parent_id;
            $data['group_number'] = 0;
            $data['ivalid_group_number'] = 0;
            $data['leader_num'] = 0;
            $data['ivalid_leader_num'] = 0;
            $data['user_identity'] = empty($isGroupMember)?0:1;
            $data['upgrade_money'] = $orderInfo['actual_price'];

            $result = MemberModel::insertMemberInfo($data);

            self::userIdentityUp($mid);

            if($type==1&&$resData->parent_id>0){

                $parent_info = MemberModel::getGroupMemeberInfo($resData->parent_id);

                if(empty($parent_info)){

                    $parentIsGroupMember = MemberModel::isGroupMemberV1($resData->parent_id);

                    $parentUserIndenty = empty($parentIsGroupMember)?0:1;

                }else{

                    $parentUserIndenty = $parent_info['user_identity'];

                }

                if($parentUserIndenty>=$data['user_identity']){

                    self::insertTotalUpgradeMoney($ordersn,2);
                }

            }


        }

    }


    /**
     * 角色升级
     * @param ordersn
     * @return mixed
     */
    private static function userIdentityUp($mid){


        $memberInfo = MemberModel::getGroupMemeberInfo($mid);

        $userIdentityMoney = self::USER_IDENTITY_MONEY;
        
        $userIndentyBonus = self::USER_IDENTITY_BONUS;

        $userIndentyDesc = self::USER_IDENTITY_DESC;


        if(!empty($memberInfo)){

            $next_indentity_money = $memberInfo['user_identity']>2?$userIdentityMoney[3]:$userIdentityMoney[$memberInfo['user_identity']+1];

            $getMyUserNumber = MemberModel::getMyUserNumber($mid);

            switch ($memberInfo['user_identity']) {
                case 1:
                    $is_allow_up = $getMyUserNumber>99?1:0;
                    break;
                case 2:
                    $is_allow_up = $getMyUserNumber>199?1:0;
                    break;
                case 3:
                     $number = MemberModel::getMyUserNumberForTop($mid);
                     $needMyUserNumber = $number*200==0?199:$number*200-1;
                     $is_allow_up = $getMyUserNumber>$needMyUserNumber?1:0;
                    break;
                default:
                    $is_allow_up = 0;
                    break;
            }

            if(($memberInfo['upgrade_money']>=$next_indentity_money)||$is_allow_up==1){

                $new_user_identity = $memberInfo['user_identity']>2?3:$memberInfo['user_identity']+1;

                $user_bonus = $memberInfo['user_identity']>2?$userIndentyBonus[4]:$userIndentyBonus[$memberInfo['user_identity']+1];
            
                $data = [];

                $data['mid'] = $mid;
                $data['room_id'] = 0;
                $data['amount'] = $user_bonus;
                $data['product_type'] = 0;
                $data['product_id'] = 0;
                $data['product_name'] = 0;
                $data['content'] = "升级为".$userIndentyDesc[$new_user_identity]."奖金";
                $data['channel'] = 1;
                $data['order_no'] = 0;
                $data['status'] = 0;
                $data['identity'] = 6;
                $data['new_identity'] = $new_user_identity;

                MemberModel::insertBonusBecommitted($data);

                $member_data = [];

                $member_data['mid'] = $mid;
                $member_data['user_identity'] = $new_user_identity;
                $member_data['peding_account_money'] = $memberInfo['peding_account_money']+$user_bonus;

                if($is_allow_up==1&&$memberInfo['upgrade_money']<$next_indentity_money){

                    $member_data['upgrade_money'] = $memberInfo['upgrade_money']+$user_bonus;

                }else{

                    $member_data['upgrade_money'] = $memberInfo['upgrade_money']-$next_indentity_money+$user_bonus;
                }

                $result = MemberModel::updateGroupMemeberInfo($member_data);

                if($memberInfo['parent_id']>0){

                    $parent_info = MemberModel::getGroupMemeberInfo($memberInfo['parent_id']);

                    if(!empty($parent_info)&&$parent_info['user_identity']>=$new_user_identity){

                        MemberModel::updateGroupMemeberInfoV1($parent_info['mid'],'upgrade_money',$user_bonus,1);

                        self::userIdentityUp($parent_info['mid']);
                    }

                }

            }

        }

    }


    /**
     * 发放角色升级时获得的奖励
     * @param ordersn
     * @return mixed
     */
    public static function sendUserIdentityUpBonus(){

        $list = MemberModel::getUserIdentityUpBonusList();

        if (!empty($list)) {

            foreach ($list as $key => $item) {

                if ($item['status'] == 0) {


                    $data = [];

                    $data['id'] = $item['id'];
                    $data['status'] = 1;

                    $updateStauts = MemberModel::updateBonusBecommittedStatus($data);

                    if ($updateStauts) {

                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'peding_account_money', $item['amount'], 2);

                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'cash_money', $item['amount'], 1);

                        MemberModel::updateGroupMemeberInfoV1($item['mid'], 'total_money', $item['amount'], 1);
                    }

                }


            }

            return ['code' => 200, 'message' => '操作成功'];

        }

    }


    /**
     * 新的建群是否为符合规则的群
     * @param $param
     * @return array
     */
    public static function newSendIsRuleGroup($room_id = 0)
    {

        $groupInfo = MemberModel::getGroupDataV1($room_id);

        if(!empty($groupInfo)){

            $bonus_status = MemberModel::getRoomBonusInfo($room_id);

            if(!empty($bonus_status)){

                if($bonus_status['status']==0&&$groupInfo['member_count']<50){

                    $data['id'] = $bonus_status['id'];
                    $data['status'] = 2;

                    $updateStauts = MemberModel::updateBonusBecommittedStatus($data); 

                }

            }else{

                if($groupInfo['member_count']>49){

                    $sendBonusList = MemberModel::getRoomBonusInfoV1($groupInfo['mid']);

                    $isNotGet = 0;
                    $sendBonusCount = 0;

                    foreach ($sendBonusList as $key => $value) {
                       
                       if($value['status']==1){

                            $sendBonusCount = $sendBonusCount+1;

                       }else{

                            $isNotGet = 1;
                       }

                    }

                    if($isNotGet==0){

                        if($sendBonusCount<2){

                            $sendBonus = 500;

                        }else if($sendBonusCount>1&&$sendBonusCount<5){

                            $sendBonus = 1000;

                        }else if($sendBonusCount>4){

                            $sendBonus = 2000;
                        }

                        $data = [];

                        $data['mid'] = $groupInfo['mid'];
                        $data['room_id'] = $room_id;
                        $data['amount'] = $sendBonus;
                        $data['product_type'] = 0;
                        $data['product_id'] = 0;
                        $data['product_name'] = 0;
                        $data['content'] = $room_id."群群主建群奖金";
                        $data['channel'] = 1;
                        $data['order_no'] = 0;
                        $data['status'] = 0;
                        $data['identity'] = 7;
                        $data['new_identity'] = 0;

                        MemberModel::insertBonusBecommitted($data);

                        $memberInfo = MemberModel::getGroupMemeberInfo($groupInfo['mid']);

                        $resData = MemberModel::getMemberInfoByMid(['mid' => $groupInfo['mid']]);

                        if (!empty($memberInfo)) {

                            MemberModel::updateGroupMemeberInfoV1($groupInfo['mid'], 'peding_account_money', $sendBonus, 1);

                        } else {

                            $member = [];

                            $member['mid'] = $groupInfo['mid'];
                            $member['nick_name'] = isset($resData->nickname) ? $resData->nickname : '';
                            $member['mobile'] = isset($resData->mobile) ? $resData->mobile : 0;
                            $member['type'] = 1;
                            $member['parent_id'] = isset($resData->parent_id) ? $resData->parent_id : 0;
                            $member['group_number'] = 0;
                            $member['ivalid_group_number'] = 0;
                            $member['leader_num'] = 0;
                            $member['ivalid_leader_num'] = 0;
                            $member['user_identity'] = 1;
                            $member['peding_account_money'] = $sendBonus;

                            MemberModel::insertMemberInfo($member);

                        }


                    }

                }

            }


        }
    }    


    /**
     * 查找用户最近的创客
     * @param $param
     * @return int
     */
    public static function findChuangKeMid($mid=0){

        $pathObj = MemberModel::getShipByMemberId($mid);
        $pathArr = [];
        $g1Id = 0;
        if (!empty($pathObj->path)) {
            $pathArr = explode(',', $pathObj->path);
            $pathArr = array_filter($pathArr);
        }

        if (!empty($pathArr)) {
            $path = implode(',', $pathArr);
            //间推
            $g1Arr = MemberModel::getAllWork($path);
            if (!empty($g1Arr)) {
                //取最近节点人
                $g1Obj          = array_pop($g1Arr);
                $g1Id           = $g1Obj->mid;
            }
        }

        return $g1Id;

    }

}