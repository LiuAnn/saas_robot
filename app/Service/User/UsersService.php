<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/21
 * Time: 下午05:43
 * Describe: 本地生活
 */

namespace App\Service\User;

use Exception;
use App\Models\GroupTag;
use App\Models\GroupTagRelation;
use App\Models\MemberInfoByAppID;
use App\Models\RecommendGoods;
use App\Models\Users;
use App\Models\UsersWechat;
use App\Service\Platform\MiniProgramService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UsersService
{
    // 机器人小程序用户登录信息Redis Key
    private static $user_token_redis = 'robot:mini:program:user:info:';

    public static function getChannel()
    {
        $data = [
            'channel_type' => [
                [
                    'channel_name' => '大人',
                    'channel_images' => 'http://shop.yuetao.tech/images/logo.jpg'
                ]
            ],
            'list' => [
                [
                    'channel_name' => '悦淘掌柜',
                    'profit_total' => 580,
                    'group_number' => 3,
                    'group_gmv' => '159, 980.89'
                ],
                [
                    'channel_name' => '直订掌柜',
                    'profit_total' => 580,
                    'group_number' => 3,
                    'group_gmv' => '159, 980.89'
                ],
            ]

        ];
        return $data;
    }

    /**
     * 用户信息
     *
     * @param string $token
     * @return array
     */
    public static function getOne($token = '')
    {
        if (empty($token)) {
            return [];
        }
        $user_info = self::getUserInfo($token);
        if (empty($user_info)) {
            return [];
        }
        $data = Users::select('id', 'username', 'nickname', 'mobile', 'avatar', 'created_at')->find($user_info['user_id']);
        return $data ?? [];
    }


    /**
     * 获取用户群
     *
     * @param string $token
     * @return array
     */
    public static function getGroupList($token = '')
    {
        if (empty($token)) {
            return [];
        }
        $user_info = self::getUserInfo($token);
        if (empty($user_info)) {
            return [];
        }
        $data = MemberInfoByAppID::select('id', 'name', 'head_img')
            ->where('mobile', $user_info['mobile'])
            ->where('is_delete', 0)
            ->get()
            ->toArray();
        if (empty($data)) {
            return [];
        }
        foreach ($data as $key => $val) {
            $data[$key]['tag_list'] = GroupTagRelation::from('group_tag_relation as tag_relation')
                ->select('tag_relation.id', 'tag.name')
                ->leftJoin('group_tag as tag', function ($query) {
                    $query->on('tag_relation.tag_id', 'tag.id')->where('is_delete', 0);
                })
                ->where('tag_relation.group_id', $val['id'])
                ->where('tag_relation.deleted_at', 0)->get()->toArray();
        }

        return $data ?? [];
    }

    /**
     * 设置群标签
     *
     * @param $group_id
     * @param string $add
     * @param string $del
     * @param string $token
     * @return array
     */
    public static function setGroupTags($group_id, $add = '', $del = '', $token)
    {
        //不是本人的群不能修改
        $user_info = self::getUserInfo($token);
        if (empty($user_info)) {
            return [];
        }
        $group_info = MemberInfoByAppID::where('mobile', $user_info['mobile'])
            ->where('is_delete', 0)
            ->where('id', $group_id)
            ->first();
        if (empty($group_info)) {
            return [];
        }
        if ($add) {
            $add_array = explode(',', $add);
            $tag_info = [];
            foreach ($add_array as $key => $val) {
                $tags = GroupTag::where('id', $val)->where('is_delete', 0)->first();
                if (empty($tags)) {
                    unset($add_array[$key]);
                    continue;
                }
                $tag_relation = GroupTagRelation::where('group_id', $group_id)->where('tag_id', $val)->where('deleted_at', 0)->first();
                if ($tag_relation) {
                    unset($add_array[$key]);
                    continue;
                }
                $tag_info[$key]['group_id'] = $group_id;
                $tag_info[$key]['tag_id'] = $val;
                $tag_info[$key]['created_at'] = time();
            }
            GroupTagRelation::where('group_id', $group_id)->insert($tag_info);
        }
        if ($del) {
            $del = explode(',', $del);
            GroupTagRelation::whereIn('tag_id', $del)->update(['deleted_at' => time()]);
        }
        return [];
    }

    public static function login($code = '', $encryptedData = '', $iv = '')
    {
        if (empty($code) || empty($encryptedData) || empty($iv)) {
            throw new Exception('params is error', 400);
        }
        // GET 解密
        $miniProgramInfo = new MiniProgramService(1);

        //$user_info = $miniProgramInfo->getSessionKeyByCode($code);
//        print_r($user_info);
        // GET 用户信息
//        $user = file_get_contents('https://api.weixin.qq.com/sns/jscode2session?appid=' . $app_id . '&secret=' . $developers->app_secret . '&js_code=' . $code . '&grant_type=authorization_code');
//        $user_info = json_decode($user, true);
        $user_info = [
            'errcode' => 0,
            'unionid' => '',
            'openid' => 'o62H10AaiOxwkq5LHm8RQojDQ3i4',
            'session_key' => 'tiihtNczf5v6AKRyjwEUhQ==',
        ];
        if ($user_info['errcode'] != 0) {
            throw new Exception('getSessionKeyFail', $user_info['errcode']);
        }
        $mobile = '18888888888';

        // GET 解密后的用户数据
        $decryptData = $miniProgramInfo->decryptData($encryptedData, $iv, $user_info['session_key']);
        if ($decryptData['code'] != 200) {
            throw new Exception('decryptDataFail', $decryptData['code']);
        }

        $users = Users::select('id')
            ->where('unionid', $decryptData['data']->unionId)
            ->first();
        if (empty($users)) {
            Users::insert([
                'username' => $mobile,
                'mobile' => $mobile,
                'nickname' => $decryptData['data']->nickName,
                'avatar' => $decryptData['data']->avatarUrl,
                'gender' => $decryptData['data']->gender,
                'country' => $decryptData['data']->country,
                'province' => $decryptData['data']->province,
                'city' => $decryptData['data']->city,
                'unionid' => $decryptData['data']->unionId,
            ]);
            $user_id = DB::getPdo()->lastInsertId();
        } else {
            Users::where('id', $users->id)
                ->update([
                    'nickname' => $decryptData['data']->nickName,
                    'avatar' => $decryptData['data']->avatarUrl,
                    'gender' => $decryptData['data']->gender,
                    'country' => $decryptData['data']->country,
                    'province' => $decryptData['data']->province,
                    'city' => $decryptData['data']->city,
                    'last_login_time' => time()
                ]);
            $user_id = $users->id;
        }

        // GET openId info
        $wechat_info = UsersWechat::select('uid')
            ->where('app_id', $decryptData['data']->watermark->appid)
            ->where('openid', $decryptData['data']->openId)
            ->first();
        if (empty($wechat_info)) {
            UsersWechat::insert([
                'uid' => $user_id,
                'app_id' => $decryptData['data']->watermark->appid,
                'openid' => $decryptData['data']->openId,
            ]);
        }

        // SET Token
        $token = self::getToken();
        $data = array(
            'token' => $token,
            'user_id' => $user_id,
            'unionid' => $decryptData['data']->unionId,
            'openid' => $decryptData['data']->openId,
            'nickname' => $decryptData['data']->nickName,
            'avatar' => $decryptData['data']->avatarUrl,
            'mobile' => $mobile
        );
        Redis::set(self::$user_token_redis . $token, json_encode($data));
        Redis::expire(self::$user_token_redis . $token, 86400 * (empty($wechat_info) ? 1 : 30));

        return [
            'token' => $token,
            'openid' => $decryptData['data']->openId,
            'nickname' => $decryptData['data']->nickName,
            'avatar' => $decryptData['data']->avatarUrl,
            'mid' => 0,
            'code_number' => '',
            'user_id' => $user_id
        ];
    }

    protected static function getToken($uid = '')
    {
        $time = time();
        $chars = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
        $hash = '';
        $max = strlen($chars) - 1;
        mt_srand();
        for ($i = 0; $i < 32; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
        return md5($uid . $time . $hash);
    }

    /**
     * 获取推荐商品详情接口  后面要移到其他地方
     */
    public static function getDetail()
    {
        $id = 1;
        return RecommendGoods::where('product_id', $id)
            ->frist();
    }


    /**
     * 从Redis获取用户信息
     *
     * @param string $token
     * @return array|mixed
     */
    private static function getUserInfo($token = '')
    {
        if (empty($token)) {
            return [];
        }
        $user_info = Redis::get(self::$user_token_redis . $token);
        if (empty($user_info)) {
            return [];
        }
        return json_decode($user_info, true);
    }
}