<?php

namespace App\Service\User;
use App\Model\User\LotteryChanceModel;
use Illuminate\Support\Facades\DB;
use App\Model\User\LotteryChanceLogModel;

class LotteryChanceServiceImpl
{
    protected $chanceId = 0;

    protected $mid = 0;

    protected $totalNum = 0;

    protected $useNum = 0;

    protected $restNum = 0;

    protected $periods = '';

    protected $createdAt = null;

    protected $updatedAt = null;

    protected $deletedAt = null;

    protected $model = null;

    protected static $instances = [];

    protected function __construct(LotteryChanceModel $model)
    {
        $this->chanceId = $model->chance_id;

        $this->mid = $model->mid;

        $this->totalNum = $model->total_num;

        $this->useNum = $model->use_num;

        $this->restNum = $model->rest_num;

        $this->periods = $model->periods;

        $this->createdAt = $model->created_at;

        $this->updatedAt = $model->updated_at;

        $this->model = $model;

        self::$instances[$this->chanceId] = $this;

        return $this;
    }

    /**
     * @return int|mixed
     */
    public function getChanceId()
    {
        return $this->chanceId;
    }

    /**
     * @return int|mixed
     */
    public function getMid()
    {
        return $this->mid;
    }

    /**
     * @return int|mixed
     */
    public function getTotalNum()
    {
        return $this->totalNum;
    }

    /**
     * @return int|mixed
     */
    public function getUseNum()
    {
        return $this->useNum;
    }

    /**
     * @return int|mixed
     */
    public function getRestNum()
    {
        return $this->restNum;
    }

    /**
     * @return mixed|string
     */
    public function getPeriods()
    {
        return $this->periods;
    }

    /**
     * @return mixed|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param array $data
     * @return LotteryChanceServiceImpl
     */
    protected static function create(array $data)
    {
        $model = LotteryChanceModel::create([
            'mid'=>$data['mid'],
            'total_num'=>$data['totalNum'],
            'rest_num'=>$data['restNum'],
            'periods'=>$data['periods'],
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ]);

        return new self($model);
    }

    /**
     * 减少可用次数
     * @param $mid
     * @param $num
     * @param $periods
     * @return mixed
     */
    public static function decrease($mid, $num, $periods)
    {
        $model = LotteryChanceModel::where('mid',$mid)
            ->where('periods',$periods)
            ->first();

        //如果记录存在
        if($model && ($model->rest_num-$num)>=0){
            $model->rest_num-=$num;
            $model->use_num+=$num;
            return $model->save();
        }
    }

    /**
     * 增加抽奖次数
     * @param $mid
     * @param $num
     * @param $periods
     * @return mixed
     * @throws \Throwable
     */
    public static function increase($mid, $num, $periods)
    {
        $model = LotteryChanceModel::where('mid',$mid)
            ->where('periods',$periods)
            ->first();

        /**
         * 如果记录不存在
         */
        if(!$model){
            //创建记录
            DB::transaction(function()use($mid,$num,$periods){
                self::create(['mid'=>$mid,'totalNum'=>$num,'restNum'=>$num,'periods'=>$periods]);
                self::createLotteryChancesLog($mid,$num);
            });

            return;
        }

        /***
         * 如果记录存在
         */
        //查询当天已经获取的抽奖次数
        $todayChancesNum = self::getTotalChancesNumByDate($mid,date('Y-m-d H:i:s'));

        //如果当天已经获取10次，则终止
        if($todayChancesNum >= 10){
            return;
        }

        if(($todayChancesNum + $num) >10){
            DB::transaction(function()use($model,$todayChancesNum,$mid){
                $model->rest_num+=(10-$todayChancesNum);
                $model->total_num+=(10-$todayChancesNum);
                $model->save();
                self::createLotteryChancesLog($mid,(10-$todayChancesNum));
            });
            return;
        }

        if(($todayChancesNum + $num) <=10){
            DB::transaction(function()use($model,$num,$mid){
                $model->rest_num+=$num;
                $model->total_num+=$num;
                $model->save();
                self::createLotteryChancesLog($mid,$num);
            });
            return;
        }
    }

    /**
     * @param $mid
     * @param $periods
     * @return LotteryChanceServiceImpl|null
     */
    public static function getInstanceByMidAndPeriods($mid, $periods)
    {
        $model = LotteryChanceModel::where('mid',$mid)
            ->where('periods',$periods)
            ->first();

        return $model ? new self($model) : null;
    }

    /**
     * 获取指定用户，指定期数，指定日期的剩余抽奖次数
     * @param $mid
     * @param $periods
     * @param $date
     * @return int
     */
    public static function getInstanceByMidAndPeriodsAt($mid, $periods,$date)
    {
        //判断剩余次数
        $model = LotteryChanceModel::where('mid',$mid)
            ->where('periods',$periods)
            ->first();

        if($model === null || $model->rest_num == 0){
            return 0;
        }

        //判断今天的抽奖次数是否超出10次
        $hadUseNum = LotteryModel::where('mid',$mid)->where('periods',$periods)
            ->where('created_at','>=',Carbon::parse($date)->startOfDay()->toDateTimeString())
            ->where('created_at','<=',Carbon::parse($date)->endOfDay()->toDateTimeString())
            ->count();

        if($hadUseNum >= 10){
            return 0;
        }

        //判断剩余抽奖次数是否剩下的限制次数
        return (10-$hadUseNum) <= $model->rest_num ? (10-$hadUseNum) : $model->rest_num;
    }


    /**
     * 增加抽奖机会记录
     * @param $mid
     * @param $num
     */
    protected static function createLotteryChancesLog($mid,$num)
    {
        LotteryChanceLogModel::create([
            'mid'=>$mid,
            'num'=>$num,
            'created_at'=>time()
        ]);
    }

    /**
     * 获取指定用户某天的抽奖机会的获取记录
     * @param $mid
     * @param $date
     * @return int
     */
    protected static function getTotalChancesNumByDate($mid,$date)
    {
        $startTime = self::parse($date)->startOfDay()->timestamp;
        $endTime = self::parse($date)->endOfDay()->timestamp;

        return LotteryChanceLogModel::where('mid',$mid)
            ->where('created_at','>=',$startTime)
            ->where('created_at','<=',$endTime)
            ->sum('num');
    }


    public static function parse($time = null, $tz = null)
    {
        return new static($time, $tz);
    }

    /**
     * 定时任务，每天给用户发放三次抽奖机会
     */
    public static function Job()
    {
        $result = MemberModel::getMidByAll();
        foreach ($result as $item) {
            self::increase($item->mid,3,LotteryServiceImpl::PERIODS20190107);
            Log::info("send member ottery chances by mid:" . $item->mid);
        }
    }

    /**
     * 定时减去前一天的免费机会
     */
    public static function JobDecreaseFreeChances()
    {
        $models = LotteryChanceModel::where('periods',LotteryServiceImpl::PERIODS20190107)->get();

        DB::transaction(function()use($models){
            $models->map(function($model){
                $num = LotteryModel::selectRaw('count(mid) as num,mid')
                    ->where('periods',LotteryServiceImpl::PERIODS20190107)
                    ->where('created_at','>=',Carbon::now()->subDay()->startOfDay()->toDateTimeString())
                    ->where('created_at','<=',Carbon::now()->subDay()->endOfDay()->toDateTimeString())
                    ->groupBy('mid')
                    ->count();
                if($num <= 3){
                    $cutNum = 3-$num;
                    //减去总次数
                    $model->total_num >= $cutNum ? $model->total_num-=$cutNum : 0;
                    //减去剩余次数
                    $model->rest_num >= $cutNum ? $model->rest_num-=$cutNum : 0;
                    $model->save();
                    //添加记录
                    LotteryChanceLogModel::create([$model->mid,-$cutNum]);
                }
            });
        });

    }

}
