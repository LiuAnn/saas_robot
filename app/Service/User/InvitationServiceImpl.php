<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 18/4/11
 * Time: 上午1:09
 */

namespace App\Service\User;

use App\Model\User\InvitationModel;
use App\Service\User\SendsmsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class InvitationServiceImpl
{

    //邀请是否已经解锁
    const UNLOCK_YES = 1;
    const UNLOCK_NO = 0;
    const SPECIAL_INVITE_KEY = 'special_invite_key_';

    /**
     * 新增推荐码
     */
    public static function insertInvit($data)
    {

        $date       = strtotime('2020-02-28');
        $codeNumber = "F" . mt_rand(1000, 9999) . mt_rand(1000, 9999);
        if ($date <= time()) {
            $time = date('H');
            if ($time >= 12) {
                $nowDate           = date('Y_m_d');
                $specialKey        = self::SPECIAL_INVITE_KEY . $nowDate;
                $specialCodeNumber = Redis::rpop($specialKey);
                if (!empty($specialCodeNumber)) {
                    $data["codeNumber"] = $specialCodeNumber;
                    SendsmsService::messageSend(18519373245, 301011, [$data['memberId'], '该用户是F2020开头用户', $specialCodeNumber]);
                }
            }
        }
        if (!empty($data["codeNumber"])) {
            $codeNumber = $data["codeNumber"];
        }

        $invData = [
            "mid"          => $data["memberId"],
            "code_number"  => $codeNumber,
            "total_number" => 10,
            "use_number"   => 0,
            "create_time"  => date("Y-m-d H:i:s", time()),
        ];
        return InvitationModel::insertInvit($invData);
    }

    /*
     * 检查邀请码是否合法
     * @parm string
     * */
    public static function verifyCodeNumber($codeNumber)
    {
        if (empty($codeNumber)) {
            return 0;
        }

        $key   = "invit_{$codeNumber}";
        $invit = Redis::get($key);
        if (empty($invit)) {
            $invit = InvitationModel::getInvitCode($codeNumber);
            Redis::set($key, json_encode($invit));
        } else {
            $invit = json_decode($invit);
        }

        if (empty($invit)) {
            return false;
        }

        return $invit->mid;
    }

    /**
     * 修改推荐码的次数 + 1
     */
    public static function updateCodeNumber($codeNumber)
    {
        //return InvitationModel::updateCodeNumber($codeNumber);

        DB::transaction(function () use ($codeNumber) {

            $model = InvitationModel::where('code_number', $codeNumber)->first();

            if (!$model) {
                return;
            }

            $model->use_number += 1;

            //推荐次数少于10次，值修改次数，不解锁
            $model->save();

            //判断推荐码使用次数未超过10次，不做后续操作
            if ($model->use_number < 10) {
                return;
            }

            //获取推荐码属主
            $memberInstance = MemberServiceImpl::getInstance($model->mid);
            //如果属主不存在，不做后续操作
            if (!$memberInstance) {
                return;
            }

            //如果已经解锁
            if ($memberInstance->isInviteUnLock()) {
                return;
            }

            //判断从属主注册之日起与当前时间的时间差是否超过30天
            if (Carbon::parse($model->create_time)->gt(Carbon::now()->subDay(30)->startOfDay())) {
                //解锁
                $memberInstance->inviteUnLock();

                //增加50积分
                $memberInstance->deductionCoin(-Config('CacheKeyVariable.invitationJiFenNumber'));

                //添加消费记录
                $memberInstance->createJiFenLog("推荐码使用10次，获得" . Config('CacheKeyVariable.invitationJiFenNumber') . "积分",
                    time(), MemberJiFenServiceImpl::TYPE_IN,
                    Config('CacheKeyVariable.invitationJiFenNumber')
                );
            }

        });
    }

    /**
     * 根据用户获取该用户的推荐码
     */
    public static function getByMember($member)
    {
        $result = InvitationModel::getByMember($member["mid"]);
        if (empty($result)) {
            $code = Config("errors.com_http_code.service_error");
            return ["code" => $code, "msg" => "没有推荐码"];
        }

        return [
            "code"       => Config("errors.com_http_code.success"),
            "codeNumber" => $result->code_number,
            "total"      => $result->total_number,
            "use"        => $result->use_number,
            "unlock"     => MemberServiceImpl::getInstance($member['mid'])->isInviteUnLock() ?
                MemberServiceImpl::UNLOCK_YES : MemberServiceImpl::UNLOCK_NO
        ];
    }

    public static function updateMemberCode($codeNumber, $memberId)
    {
        return InvitationModel::updateMemberCode($codeNumber, $memberId);
    }

    public static function getInvitationCode($memberId)
    {
        $result = InvitationModel::getByMember($memberId);
        return !empty($result) ? $result->code_number : "";
    }

    /**
     * 判断这个用户是否有推荐码
     * 如果有执行修改
     * 如果没有执行新增
     */
    public static function operInvitationCode($data)
    {
        $invita = self::getInvitationCode($data["memberId"]);
        if (empty($invita)) {
            /**
             * 用户没有邀请码，执行新增
             */
            self::insertInvit($data);
        }
    }

    //
    public static function listByMember($member, $page)
    {
        $memberId      = $member["mid"];
        $oneLevelTotal = MemberModel::getMemberChildCountByMemberId($memberId);
        $twoLevelTotal = MemberModel::getTwoChildCount($memberId);
        $total         = $oneLevelTotal + $twoLevelTotal;

        $display = 20;
        $offset  = ($page - 1) * $display;
        $result  = MemberModel::getMemberChildListByMemberId($memberId, $offset, $display);
        $list    = [];

        $totalPage = ceil($oneLevelTotal / $display);
        foreach ($result as $item) {
            $tmpMember = [
                "nickname" => $item->nickname,
                "mobile"   => $item->mobile,
            ];
            $list[]    = [
                "mid"      => $item->mid,
                "name"     => MemberUnitService::getNickName($tmpMember),
                "avatar"   => MemberUnitService::getAvatar($item->litpic),
                "mobile"   => MemberUnitService::getSubMobile($item->mobile),
                "joinTime" => date("Y-m-d H:i:s", $item->jointime)
            ];
        }
        $data = [
            "code" => 200,
            "msg"  => "ok...",
            "data" => [
                "totalPage" => $totalPage,
                "member"    => [
                    "avatar" => MemberUnitService::getAvatar($member["litpic"]),
                    "name"   => MemberUnitService::getNickName($member),
                ],
                "team"      => [
                    "total"     => $total,
                    "oneLevel"  => $oneLevelTotal,
                    "twoLevelB" => $twoLevelTotal,
                ],
                "list"      => $list
            ]
        ];
        return $data;
    }

    public static function newListByMember($member, $page)
    {
        $memberId      = $member["mid"];
        $oneLevelTotal = MemberModel::getMemberChildCountByMemberId($memberId);
        $twoLevelTotal = MemberModel::getTwoChildCount($memberId);
        $total         = $oneLevelTotal + $twoLevelTotal;

        $oneLevel = MemberModel::getVipMemberChildCountByMemberId($memberId);
        $twoLevel = MemberModel::getVipTwoChildCount($memberId);
        $possible = $total - $oneLevel - $twoLevel;
        $display  = 20;
        $offset   = ($page - 1) * $display;
        $result   = MemberModel::getMemberChildListByMemberId($memberId, $offset, $display);
        $list     = [];

        $totalPage = ceil($oneLevelTotal / $display);
        foreach ($result as $item) {
            $tmpMember = [
                "nickname" => $item->nickname,
                "mobile"   => $item->mobile,
            ];
            $list[]    = [
                "mid"      => $item->mid,
                "name"     => MemberUnitService::getNickName($tmpMember),
                "avatar"   => MemberUnitService::getAvatar($item->litpic),
                "mobile"   => MemberUnitService::getSubMobile($item->mobile),
                "joinTime" => date("Y-m-d H:i:s", $item->jointime)
            ];
        }
        $data = [
            "code" => 200,
            "msg"  => "ok...",
            "data" => [
                "totalPage" => $totalPage,
                "member"    => [
                    "avatar" => MemberUnitService::getAvatar($member["litpic"]),
                    "name"   => MemberUnitService::getNickName($member),
                ],
                "team"      => [
                    "total"     => $possible,
                    "oneLevel"  => $oneLevel,
                    "twoLevelB" => $twoLevel,
                ],
                "newteam"   => [
                    "total"   => $oneLevelTotal,
                    "shopNum" => $oneLevel,
                ],
                "list"      => $list
            ]
        ];
        return $data;
    }

    public static function childListByMember($memberId, $page)
    {
        $member = MemberServiceImpl::getInstance($memberId);
        if (empty($member)) {
            return [
                "code" => 400,
                "msg"  => "失败",
                "data" => []
            ];
        }
        $member = $member->getModel();

        $total = MemberModel::getMemberChildCountByMemberId($memberId);

        $display = 20;
        $offset  = ($page - 1) * $display;
        $result  = MemberModel::getMemberChildListByMemberId($memberId, $offset, $display);
        $list    = [];

        $totalPage = ceil($total / $display);

        foreach ($result as $item) {
            $tmpMember = [
                "nickname" => $item->nickname,
                "mobile"   => $item->mobile,
            ];
            $list[]    = [
                "mid"      => $item->mid,
                "name"     => MemberUnitService::getNickName($tmpMember),
                "avatar"   => MemberUnitService::getAvatar($item->litpic),
                "mobile"   => MemberUnitService::getSubMobile($item->mobile),
                "joinTime" => date("Y-m-d H:i:s", $item->jointime)
            ];
        }

        $data = [
            "code" => 200,
            "msg"  => "ok...",
            "data" => [
                "totalPage" => $totalPage,
                "member"    => [
                    "avatar" => MemberUnitService::getAvatar($member["litpic"]),
                    "name"   => MemberUnitService::getNickName($member),
                ],
                "team"      => [
                    "total" => $total,
                ],
                "list"      => $list
            ]
        ];

        return $data;
    }

    public static function getcompanyByNumber($codeNumber)
    {
        return InvitationModel::getcompanyByNumber($codeNumber);
    }

    public static function getMemberByNumber($codeNumber)
    {
        return InvitationModel::getMemberByNumber($codeNumber);
    }

    /**
     * 根据邀请码获取用户信息
     * @param $codeNumber
     * @return array
     */
    public static function getMemberInfoByCodeNumber($codeNumber)
    {
        $model = MemberModel::leftJoin('invitation_code', 'member.mid', '=', 'invitation_code.mid')
            ->where('invitation_code.code_number', $codeNumber)
            ->first();

        return $model ? $model->toArray() : [];
    }

    /**
     * 悦店列表
     */
    public static function ydListByMember($member, $page)
    {
        $memberId      = $member["mid"];
        $oneLevelTotal = ThirdPartyMemberModel::getMemberTotalByParentId($memberId, 119);
        $twoLevelTotal = MemberModel::getTwoChildCount($memberId);
        $total         = $oneLevelTotal + $twoLevelTotal;

        $oneLevel = MemberModel::getVipMemberChildCountByMemberId($memberId);
        $twoLevel = MemberModel::getVipTwoChildCount($memberId);
        $possible = $total - $oneLevel - $twoLevel;
        $display  = 20;
        $offset   = ($page - 1) * $display;
        $result   = ThirdPartyMemberModel::inviListByParentId($memberId, $offset, $display, "119, 120, 125");
        $list     = [];

        $totalPage = ceil($oneLevelTotal / $display);
        foreach ($result as $item) {
            $tmpMember = [
                "nickname" => $item->nickname,
                "mobile"   => $item->mobile,
            ];
            $list[]    = [
                "mid"      => $item->mid,
                "name"     => MemberUnitService::getNickName($tmpMember),
                "avatar"   => MemberUnitService::getAvatar($item->litpic),
                "mobile"   => MemberUnitService::getSubMobile($item->mobile),
                "joinTime" => $item->created_at
            ];
        }
        $data = [
            "code" => 200,
            "msg"  => "ok...",
            "data" => [
                "totalPage" => $totalPage,
                "member"    => [
                    "avatar" => MemberUnitService::getAvatar($member["litpic"]),
                    "name"   => MemberUnitService::getNickName($member),
                ],
                "team"      => [
                    "total"     => $possible,
                    "oneLevel"  => $oneLevel,
                    "twoLevelB" => $twoLevel,
                ],
                "list"      => $list
            ]
        ];
        return $data;
    }
}
