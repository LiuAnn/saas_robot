<?php

namespace App\Service\User;

use App\Http\Controllers\Controller;
use App\Model\User\CompanyModel;
use App\Model\User\InvitationCodeModel;
use App\Model\User\MemberBonusModel;
use App\Model\User\UserClientModel;
use App\Model\User\UserModel;
use App\Service\User\InvitationServiceImpl;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class LoginClientService extends Controller {

    /**
     * @param $params
     * @return bool
     * @author   liuwenhao
     * @time     2020/4/14 15:39
     * @method   method   [使用悦淘账号登录]
     */
    public static function Login($params){

        $userData = UserClientModel::getLoginUser(['mobile' => $params['mobile']]);

        if(empty($userData)){
            return false;
        }

//        $password = $params['password'];
        $userPwdData = UserClientModel::getLoginUser(['mobile' => $params['mobile']]);

        if(empty($userPwdData)){
            return false;
        }


        //查询codeNumber
        $userInfo = UserClientModel::getCodeNumber(['mid' => $userPwdData->mid]);
        Log::info('userInfo logData'.json_encode([$params,$userInfo]));

        if (!$userInfo) {
            return false;
        }

        $codeNumber = $userInfo->codeNumber;

        //密码加密
//        $password = self::userEncrypt($params['password']);
        $addData = [
            'mobile'      => $userPwdData->mobile,
            'mid'         => $userPwdData->mid,
            'password'    => $params['password'],
            'code_number' => $codeNumber,
            'nickname'    => $userPwdData->nickname
        ];

        //同步用户
        $res = UserModel::addUser($addData);

        if($res){
            return $addData;
        }

        return false;
    }

    /**
     * @param $params
     * @return bool
     * @author   liuwenhao
     * @time     2020/4/14 16:17
     * @method   method   [获取用户信息]
     */
    public static function getClientUser($params){

        $userData = UserClientModel::getLoginUser(['mobile' => $params['mobile']]);

        if(empty($userData)){
            return false;
        }

        //查询codeNumber
        $codeNumber = UserClientModel::getCodeNumber(['mid' => $userData->mid])->codeNumber;

        $userData->codeNumber = $codeNumber;

        return $userData;
    }

    /**
     * 手机号注册
     * @param $data
     * @return array
     * @throws \Throwable
     */
    public static function register($data)
    {
        $mobile   = $data["mobile"];
        $code     =array_key_exists("mobileCode", $data) ? $data["mobileCode"] :'';
        $areaCode = array_key_exists("areaCode", $data) ? $data["areaCode"] : 86;
        $country  = array_key_exists("country", $data) ? $data["country"] : "中国";

        $verifyMobile = $mobile;
        $templateId   = "300005";
        if ($areaCode != 86) {
            $verifyMobile = "{$areaCode}{$mobile}";
            $templateId   = "300006";
        }

        // 手机是否注册
        if (self::verifiMobile(['mobile' => $mobile]) == true) {
            return ["code" => 400, "msg" => "手机号已经注册"];
        }

        $codeNumber = empty($data["codeNumber"]) ? 0 : $data["codeNumber"];

        $virtual = 1;
        //***************************************************
        if ($codeNumber) {
            $parentId = InvitationServiceImpl::verifyCodeNumber($codeNumber);
            if (!$parentId) {
                return ['code' => 400, "msg" => "邀请码错误"];
            }

            $memberInstance = MemberServiceImpl::getInstance($parentId);
            if (!$memberInstance) {
                return ['code' => 400, "msg" => "邀请码数据异常"];
            }
            $companyId = $memberInstance->getCompanyId();
        } else {

            $res = self::userAttribute($mobile, 1);
            if ($res['code'] != 200) {
                return $res;
            }

            $virtual   = 2;
            $parentId  = $res['data']['companyId'];
            $companyId = $parentId == Config("CacheKeyVariable.default.parentId") ? Config("CacheKeyVariable.default.companyId") : $parentId;
        }

        //***************************************************
        $midMobile = substr($mobile, 3, 4);
        $nickName  = str_replace($midMobile, "****", $mobile);
        $password  = substr($mobile, 5, 6);

        $data = [
            "parent_id"     => $parentId,
            "mobile"        => $mobile,
            "password"      => md5($password),
            "nickname"      => $nickName,
            "areaCode"      => $areaCode,
            "country"       => $country,
            "jifen"         => 0,
            "money"         => 0,
            "balance"       => 0,
            "jointime"      => time(),
            "companyId"     => $companyId,
            "virtual"       => $virtual,
            "applicationId" => array_get($data, 'applicationId') ? $data['applicationId'] : 0
        ];

        Log::info("register by: {$mobile}, params:" . json_encode($data));

        //事物处理
//        try {
            $memberId = DB::transaction(function () use ($data, $codeNumber, $parentId) {
                //注册
                $memberId = UserClientModel::insertMember($data);

                // 给新用户生成推荐码
                InvitationServiceImpl::insertInvit(["memberId" => $memberId]);
                // 赠送优惠卷
                MemberCouponServicelmpl::giveMemberRegCoupon($memberId);
                self::registerBonus($memberId);
                return $memberId;
            });

//        $token = self::makeToken($memberId);
        if ($memberId > 0) {
            $code = Config("errors.com_http_code.success");
            Log::info('pushMessage444', [$memberId]);

            //注册成功短信通知

            $userData = UserClientModel::getLoginUser(['mid' => $memberId]);

            return $userData;
        } else {
            $code = 400;
            return ["code" => $code, "id" => 0, "integral" => 0, "msg" => "注册失败"];
        }
    }



    /**
     * 注册就跟用户赠送50现金
     */
    public static function registerBonus($memberId)
    {
        return false;
        // 奖金记录
        $bonusData = [
            "mid"        => $memberId,
            "fid"        => $memberId,
            "level"      => 1,
            "amount"     => Config("CacheKeyVariable.app.registerBonus"),
            "type"       => 6,
            "productaId" => 0,
        ];
        MemberBonusModel::insert($bonusData);
    }

    /**
     * 检查$mobile是否被注册
     * @param string
     * @return bool|int
     */
    public static function verifiMobile($mobile)
    {
        if (empty($mobile)) {
            return false;
        }
        return UserClientModel::getLoginUser(['mobile' => $mobile]);
    }

    /**
     * 获取用户的归属地
     * @param $sign
     * @param $type
     * @return array
     */
    public static function userAttribute($sign, $type)
    {
        $companyId = 0;
        $name      = "环球悦旅会";
        if ($type == 1) {
            // 判断手机号是否注册
            if (self::verifiMobile($sign) == true) {
                // 判断是否完成微信绑定
                return ["code" => 400, "msg" => "该手机号已经注册，请登录~"];
            } else {
                $companyId = Config("CacheKeyVariable.default.parentId");
                $name      = Config("CacheKeyVariable.default.companyName");
            }
        } else {
            // 根据邀请码获取分公司
            $tmpCompanyId = self::getcompanyByNumber($sign);
            if ($tmpCompanyId > 0) {
                $company = self::getCompanyByCompanyId($tmpCompanyId);
                if (!empty($company)) {
                    $companyId = $company->company_id;
                    $name      = $company->companyName;
                }
            }
        }

        return [
            "code" => 200,
            "data" => [
                "companyId" => $companyId,
                "avatar"    => "http://images.yueshang.store/pubfile/2018/11/20/line_1542694330.png",
                "name"      => $name
            ]
        ];
    }



    public static function getcompanyByNumber($codeNumber)
    {
        return InvitationCodeModel::getcompanyByNumber($codeNumber);
    }

    /**
     * 根据ID获取分公司信息
     */
    public static function getCompanyByCompanyId($companyId) {
        return CompanyModel::getCompanyByCompanyId($companyId);
    }


    /**
     * @param $memberId
     * @return string
     * @author   liuwenhao
     * @time     2020/4/14 14:14
     * @method   method   [生成token]
     */
    public static function makeToken($memberId)
    {
        $key        = "data_admin_user_token_mid_". $memberId;
        $token      = self::getToken($memberId);

        if(!empty($token)) {
            $expire  = 3600*3;
            Redis::expire($key, $expire);
            return $token;
        }

        $time       = time();
        $tokenStr   = "{$memberId}.{$time}";
        $token      = md5($tokenStr);
        $expire     = 3600*3;
        Redis::set($key, $token);
        Redis::expire($key, $expire);
        return $token;
    }

    /**
     * @param $memberId
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 14:15
     * @method   method   [获取token]
     */
    public static function getToken($memberId)
    {
        $key  = "data_admin_user_token_mid_". $memberId;
        return Redis::get($key);
    }

    /**
     * @param $password
     * @return string
     * @author   liuwenhao
     * @time     2020/4/14 16:07
     * @method   method   [密码加密]
     */
    protected static function getEncrypt($password){
        return md5($password);
    }

    /**
     * @param $password
     * @return array
     * @author   liuwenhao
     * @time     2020/4/14 14:31
     * @method   method   [密码加密]
     */
    public static function userEncrypt($password){

        $saltVal = substr(md5($password) , 0 , 4);

        //拼接上盐值进行加密
        $pwd = md5($saltVal . $password);

        return $pwd;
    }
}
