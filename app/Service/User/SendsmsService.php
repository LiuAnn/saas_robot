<?php

namespace App\Service\User;

use Illuminate\Support\Facades\Log;

class SendsmsService {

    private static $send_message_url = 'http://msg.yuelvhui.com';
    private static $send_message_sysid = '12001';
    private static $send_message_syspwd = '773abc161bd84083a5e9535eeb5c8dab';
    /**
     * 发送订单等短信
     */
    public static  function messageSend($mobile, $templateId, $param = []) {
        $data = [
            "templateId"	=> $templateId,
            "costKey"		=> "yuelvhui_c",
            "receiver"		=> $mobile,
            "contentParams"	=> $param
        ];

        $url 	= self::$send_message_url . "/send";
        $param 	= json_encode($data);
        Log::info(" send info:",[$data]);
        return self::send($url, $param);
    }


    /**
     * 新版短息通道,请求发送接口
     */
    public static function send($url, $data) {
        $sysId 	= self::$send_message_sysid;
        $sysPwd = self::$send_message_syspwd;

        // 获取毫秒
        $microtime 	= self::getMicrotime();

        // 接口中的sign
        $str 		=  "{$sysId}.{$microtime}.{$sysPwd}";
        $sign 		= md5($str);
        $headers 	= [
            "Content-Type: application/json",
            "Authorization:  Sys {$sysId}.{$microtime}.{$sign}"
        ];

        $ch     = curl_init();
        // 提交地址
        curl_setopt($ch, CURLOPT_URL, $url);

        // 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // 设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // POST
        curl_setopt($ch, CURLOPT_POST, 1);

        // headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);
        $data 	= json_decode($output);
        Log::info('send info:',[$output]);
        return $data;
    }

    /**
     * 新版短息通道
     * 获取微妙时间
     * @return float
     */
    private static function getMicrotime() {
        list($msec, $sec) = explode(' ', microtime());
        $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }
}
