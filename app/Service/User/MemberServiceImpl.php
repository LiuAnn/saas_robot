<?php

namespace App\Service\User;

use App\Model\User\UserClientModel;
use Illuminate\Support\Facades\Redis;

class MemberServiceImpl {
    /**
     * 获取实例
     * @param $mid
     * @return MemberServiceImpl|mixed|null
     */
    public static function getInstance($mid)
    {
        $model = UserClientModel::getLoginUser(['mid' => $mid]);
        return $model ? new self($model) : null;

        $key = "member_model_{$mid}";

        $model = Redis::get($key);
        if(!empty($model)){
            $model = unserialize($model);
            return new self($model);
        }else {
            $model = UserClientModel::getLoginUser(['mid' => $mid]);
            Redis::set($key, serialize($model));
            Redis::expire($key, 300);
            return new self($model);
        }

        // return $model ? new self($model) : null;
    }
}
