<?php

namespace App\Service\User;

use App\Model\User\CouponModel;
use Illuminate\Support\Facades\DB;
class MemberCouponServicelmpl {

    /**
     * 新注册用户，赠送酒店优惠券, 1张
     * @param $memberId
     * @throws \Throwable
     */
    public static function giveMemberRegCoupon($memberId)
    {
        self::sendRegisterCoupon($memberId);
    }

    /**
     * 注册优惠券
     * @param int $memberId
     * @throws \Throwable
     */
    public static function sendRegisterCoupon($memberId = 0)
    {
        MemberCouponServicelmpl::sendCoupon($memberId, 907, 1, 30);
        MemberCouponServicelmpl::sendCoupon($memberId, 908, 2, 7);
    }

    /**
     * 发送优惠券
     * @param $memberId
     * @param $cid
     * @param $number
     * @param $valid
     * @return bool
     * @throws \Throwable
     */
    public static function sendCoupon($memberId = 0, $cid = 0, $number = 0, $valid = 30)
    {
        $couponInfo = CouponModel::getCouponInfoById($cid);
        //优惠券不存在
        if (empty($couponInfo)) {
            return false;
        }
        //库存不足
        if ($couponInfo->totalnumber < $number) {
            //TODO
            return false;
        }
        if ($couponInfo->isnever == 1) {
            $startTime = $couponInfo->starttime;
            $endTime   = $couponInfo->endtime;
        } else if ($couponInfo->isnever == 2) {
            $startTime = strtotime(Date('Y-m-d'));
            $endTime   = $startTime + $valid * 24 * 60 * 60;
        } else {
            return false;
        }

        $arr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        shuffle($arr);
        $item   = array_chunk($arr, 4);
        $item   = implode($item[0]);
        $code   = $item . substr(time(), -4) . mt_rand(1000, 9999);
        $coupon = [];
        $time   = time();
        for ($i = 0; $i < $number; $i++) {
            $coupon[] = [
                "mid"        => $memberId,
                "cid"        => $cid,
                "totalnum"   => 1,
                "usenum"     => 0,
                "addtime"    => $time,
                "is_give"    => 1,
                "givetime"   => $time,
                "start_date" => $startTime,
                "end_dates"  => $endTime,
                'code'       => $code
            ];
        }
        DB::transaction(function () use ($coupon, $cid, $number) {
            self::insertCoupon($coupon);
            CouponModel::updateCouponTotalNum($cid, $number);
        });
    }


    public static function insertCoupon($data)
    {
        CouponModel::insertCoupon($data);
    }

}
