<?php

namespace App\Service\User;

use App\Model\User\MsgTemplateModel;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

/*
 * 发送短信类
 * */
class PhoneMessageService {

    private static $new_send_message_sysid = '230';
    private static $new_send_message_syspwd = 'dc1f571fb9e2c528d3edeef865d16008';
    private static $new_send_message_url = 'https://gateway.yueshang.shop/message/v1';
    //旧版短信通道参数
    private static $username    = 'zdwwl';  // 发送不是营销性质的短信
    private static $pwd         = 'zdw888';
    private static $username2   = "zdwyx";  // 发送营销性质的短信
    private static $pwd2        = "ylh123456";

    private static $url         = "http://115.28.112.245:8082/SendMT/SendMessage?";

    /**
     * @param $mobile
     * @param $areaCode
     * @return array
     * @author   liuwenhao
     * @time     2020/4/15 13:01
     * @method   method   [根据当前手机号判断是否已超时]
     */
    public static function isBeyondLimit($mobile, $areaCode){

        $ReallyIp     = $_SERVER['REMOTE_ADDR'];
//        $ReallyIp     = $_SERVER['HTTP_ALI_CDN_REAL_IP'];  //使用CDN后获取终端ip

        $key          = 'verificationCode' . md5($ReallyIp . $mobile . $areaCode);

        $sendCount    = Redis::get($key);
        $maxCountData = Config("cache.send_max_number");

        if (empty($sendCount)) {
            Redis::expire($key, $maxCountData['time_limit']);
        }

        if (empty($sendCount) || $sendCount < $maxCountData['send_number']) {

            Redis::set($key, $sendCount + 1);
            return array();
        } else {
            $code = Config("errors.com_http_code.service_error");
            return array('msg' => $maxCountData['send_number_msg'], 'code' => $code);
        }
    }

    /**
     * @param $mobile
     * @param $areaCode
     * @return array
     * @author   liuwenhao
     * @time     2020/4/15 12:31
     * @method   method   [发送短信验证码]
     */
    public static function sendMessage($mobile , $areaCode){

        $code = Config("errors.com_http_code.service_error");
        $key  = Config("CacheKeyVariable.app.mobelSendKey") . $mobile;

        $oldTime  = Redis::get($key);

        $currTime = time();

        //判断是否频繁发送
        if (!empty($oldTime) && ($currTime - $oldTime < 60)) {
           // return ["code" => $code, "msg" => "验证码发送频率过快，1分钟后重试"];
        }

        //将发送时间重新保存
        Redis::set($key, $currTime);

        $templateId = "2300002";
        if ($areaCode != 86) {
            $mobile     = "{$areaCode}{$mobile}";
            $templateId = "300006";
        }

        // 这个key记录短信从哪一个平台发送出去的
        $key    = "msg_channel_{$mobile}";
        $result = 0;
//        if ($areaCode == 86) {
//            $result = self::sendVerificationCode($mobile);
//        }

//        if ($result == 1) {
//            Redis::set($key, 1);
//            $code = Config("errors.com_http_code.success");
//            return ["code" => $code, "msg" => "验证发送成功"];
//        } else {
            $result = self::verificationNewSend($mobile, $templateId);

            if (!empty($result->return_code)) {
                if ($result->return_code == "SUCCESS") {
                    Redis::set($key, 2);
                    $code = Config("errors.com_http_code.success");
                    return ["code" => $code, "msg" => "验证发送成功"];
                }
            }
//        }

        return ["code" => $code, "msg" => "验证发送失败"];
    }


    /**
     * @param $mobile
     * @param int $templateId
     * @return mixed
     * @author   liuwenhao
     * @time     2020/4/15 14:09
     * @method   method   [新短信重新发送]
     */
    public static function verificationNewSend($mobile, $templateId = 300005)
    {
        Log::info("new verificationSend mobile: {$mobile}");

        $microtime  = self::getMicrotime() + 400000;
        $microtime  = number_format($microtime, 0, '', '');

        $data = [
            "templateId"  => $templateId,
            "costKey"   => "yuelvhui_c",
            "receiver"    => $mobile,
            "expireTime" =>$microtime,
            "length"=> 6,
            "sysFlag"=> "yuelvhui"
        ];

        $url  = self::$new_send_message_url . "/verification/send";
        $param = json_encode($data);
        return self::newSend($url, $param);
    }


    /**
     * @param $url
     * @param $data
     * @return mixed
     * @author   liuwenhao
     * @time     2020/4/15 14:10
     * @method   method   [新短信通道]
     */
    public static function newSend($url, $data) {
        $sysId  = self::$new_send_message_sysid;
        $sysPwd = self::$new_send_message_syspwd;

        // 获取毫秒
        $microtime  = self::getMicrotime();
        $microtime  = number_format($microtime, 0, '', '');

        // 接口中的sign
        $str      =  "{$sysId}.{$microtime}.{$sysPwd}";
        $sign     = md5($str);
        $headers  = [
            "Content-Type: application/json",
            "Authorization:  Sys {$sysId}.{$microtime}.{$sign}"
        ];

        $ch     = curl_init();
        // 提交地址
        curl_setopt($ch, CURLOPT_URL, $url);

        // 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // 设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // POST
        curl_setopt($ch, CURLOPT_POST, 1);

        // headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);
        $data   = json_decode($output);
        Log::info('send info:',[$output]);
        return $data;
    }

    /**
     * 新版短息通道
     * 获取微妙时间
     * @return float
     */
    private static function getMicrotime() {
        list($msec, $sec) = explode(' ', microtime());
        $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }

    /**
     * 发送短信验证码
     * @param $mobile
     * @code
     */
    public static function sendVerificationCode($mobile) {

        $key    = "msg_{$mobile}";
        $code = Redis::get($key);

        if (!$code) {
            $code   = mt_rand(100, 999) . mt_rand(100, 999);
            Redis::set($key, $code);
            Redis::expire($key, 600);
        }

        $text   = self::getTemplate("301044", [$code]);

        $status = self::sendMsgByOldChannel($mobile, $text);
        return $status;
    }

    /**
     * 短信通道
     * @param $mobile
     * @param $content
     * @return int
     */
    public static function sendMsgByOldChannel($mobile, $content, $type = 1)
    {
        if($type == 1) {
            $url = self::createOldChannelUrl($mobile, $content);
        }else {
            $url = self::createMarketingChannelUrl($mobile, $content);
        }

        Log::info("sendMsgByOldChannel: {$url}");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        curl_close($ch);

        // 返回状态码
        $code   = explode(",", $output);
        $status = 2;
        if($code[0] == "03" || $code[0] == "00") {
            $status = 1;
        }
        Log::info("send msg by mobile: {$mobile}, status: {$status}, content:{$content}");
        return $status;
    }


    /**
     * 生成的短息通道url
     * @param $mobile
     * @param $content
     * @return string
     */
    protected static function createOldChannelUrl($mobile, $content)
    {
        return sprintf(self::$url."UserName=%s&UserPass=%s&Mobile=%s&Content=%s",
            self::$username,self::$pwd,$mobile, urlencode($content));
    }


    /**
     * 发送营销短信
     * @param $mobile
     * @param $content
     * @return string
     */
    protected static function createMarketingChannelUrl($mobile, $content) {
        return sprintf(self::$url."UserName=%s&UserPass=%s&Mobile=%s&Content=%s",
            self::$username2,self::$pwd2, $mobile, urlencode($content));
    }

    /**
     * @param $templateId
     * @param array $param
     * @return string
     * @author   liuwenhao
     * @time     2020/4/15 13:29
     * @method   method   [根据模板ID，获取短信内容]
     */
    public static function getTemplate($templateId, $param = []) {
        $key = "message_temp_{$templateId}";
        $result = Redis::get($key);
        if(empty($result)) {
            $result = MsgTemplateModel::getMessageTemplate($templateId);
            Redis::set($key, json_encode($result));
        }else {
            $result = json_decode($result);
        }

        $text   = "";
        if(!empty($result)) {
            $text = vsprintf($result->content, $param);
            $text = $result->sign . $text;
        }
        return $text;
    }

    /**
     * @param $code
     * @param $res
     * @param $data
     * @return false|string
     * @author   liuwenhao
     * @time     2020/4/14 12:56
     * @method   method   [返回json格式]
     */
    private static function responseJson($code , $res , $data = []){
        $result = ['code' => $code , 'res' => $res , 'data' => $data];
        return json_encode($result);
    }
}
