<?php

namespace App\Service\User;

use App\Model\User\UserModel;
use App\Service\Robot\UserOperationLogService;
use App\User;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;
use App\Tools\Dingxiang\CaptchaClient;
use Illuminate\Support\Facades\Log;

class LoginService {

    private static $send_message_url = 'https://gateway.yueshang.shop/message/v1';

    /**
     * @param $params
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 12:48
     * @method   method   [登录]
     */
    public static function login($params){

        if(!isset($params['mobile']) || empty($params['mobile'])){
            return self::responseJson(200 , '手机号不能为空');
        }

        if(!isset($params['password']) || empty($params['password'])){
            return self::responseJson(200 , '密码不能为空');
        }

        //查询数据库判断
        $userInfo = UserModel::getLoginUser(['mobile' => $params['mobile']]);

        if(empty($userInfo)){

            $res = LoginClientService::Login($params);

            if(!$res){
                return self::responseJson(400 , '用户不存在');
            }

            $userPwdInfo = UserModel::getLoginUser(['mobile' => $res['mobile'] , 'mid' => $res['mid']]);

        }else{

            //密码加密
            $password = $params['password'];

            //判断密码是否正确
            $userPwdInfo = UserModel::getLoginUser(['mobile' => $params['mobile'] , 'password' => $password]);

            if(empty($userPwdInfo)){
                return self::responseJson(400 , '用户名或密码不正确');
            }
        }

        //取出验证通过的   管理员名称
        $memberId = $userPwdInfo->mid;
        $userName = empty($userPwdInfo->nickname) ? $userPwdInfo->mobile : $userPwdInfo->nickname;

        $data=[
            'mobile'    => $userPwdInfo->mobile,
            'nickname'  => $userName,
            'mid'       => $userPwdInfo->mid
        ];
        $token = self::makeToken($memberId);

        UserModel::addLoginLog($data,$token);
        $result = [
            'token' => $token,
            'data'  => $data
        ];

        $logMessage = '登录';
        UserOperationLogService::insertAdminOperation(
            $memberId, 4, json_encode($data), $logMessage, $memberId
        );
        return self::responseJson(200 , '登陆成功' , $result);
    }


    /**
     * @param $params
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 12:48
     * @method   method   [登录]
     */
    public static function loginForMiddle($params){


        $token = isset($params['token'])?$params['token']:'';
        //验证  参数是否存在
        if($token == '')
        {
            return ['code' => 400 , 'msg' => 'token不能为空'];
        }


        $postData['token'] = $token;
        $url               = "http://cidms.yuetao.vip/admin/getCidmsUserInfo";
        $userInfo          = curlPost($url,$postData);
        $arrUserInfo       = json_decode($userInfo, true);

        if ($arrUserInfo['code'] != 200) {
            return ['code' => 400, 'msg' => $arrUserInfo['msg']];
        }
       $params['mobile'] = $arrUserInfo['data']['managerResDTO']['mobile'];
       

        //查询数据库判断
        $userPwdInfo = UserModel::getLoginUser(['mobile' => $params['mobile']]);
        
        if(empty($userPwdInfo)){

            return self::responseJson(400 , '用户不存在');
            
        }

        //取出验证通过的   管理员名称
        $memberId = $userPwdInfo->mid;
        $userName = empty($userPwdInfo->nickname) ? $userPwdInfo->mobile : $userPwdInfo->nickname;

        $data=[
            'mobile'    => $userPwdInfo->mobile,
            'nickname'  => $userName,
            'mid'       => $userPwdInfo->mid
        ];
        $token = self::makeToken($memberId);

        UserModel::addLoginLog($data,$token);
        $result = [
            'token' => $token,
            'data'  => $data
        ];

        $logMessage = '登录';
        UserOperationLogService::insertAdminOperation(
            $memberId, 4, json_encode($data), $logMessage, $memberId
        );
        return self::responseJson(200 , '登陆成功' , $result);
    }


    /**
     * @param $params
     * @author   liuwenhao
     * @time     2020/4/14 12:49
     * 删除token
     * @method   method   [登出]
     */
    public static function loginOut($params){

        $memberId = $params['mid'];
        $key  = "data_admin_user_token_mid_". $memberId;
        $data=[
            'nickname'       => $params['nickname'],
            'mid'       => $params['mid'],
            'mobile'       => $params['mobile'],
            'type'       => 2
        ];
        UserModel::addLoginLog($data,Redis::get($key));

        Redis::del($key);

        $res = Redis::get($key);

        if(empty($res)){
            return self::responseJson(200 , '登出成功');
        }

        return self::responseJson(400 , '登出失败了');
    }

    /**
     * @param $params
     * @return false|string
     * @throws \Throwable
     * @author   liuwenhao
     * @time     2020/4/14 12:49
     * @method   method   [注册]
     */
    public static function register($params){

        if(!isset($params['mobile']) || empty($params['mobile'])){
            return self::responseJson(400 , '手机号不能为空');
        }

        if(!isset($params['mobileCode']) || empty($params['mobileCode'])){
            return self::responseJson(400 , '验证码不能为空');
        }

        //验证用户是否存在
        $userData = UserModel::getLoginUser(['mobile' => $params['mobile']]);

        if(!empty($userData)){
            return self::responseJson(400 , '您的手机号已注册过账号');
        }

        //验证验证码

        $codeRes = self::verificationCode($params['mobile'] , $params['mobileCode'], '2300002');


        Log::info("sendMessage mobile Code : {$params['mobileCode']}  mobile {$params['mobile']} ");
        $codeRes = json_decode(json_encode($codeRes), true);

        if ($codeRes['return_code'] !== 'SUCCESS') {
            return self::responseJson(400, '验证码已失效');
        }

        //验证密码
        if(!isset($params['password']) || empty($params['password'])){
            return self::responseJson(400 , '密码不能为空');
        }

        $addData = [
            'mobile'   => $params['mobile'],
            'password' => $params['password'],
        ];

        DB::beginTransaction();


        //查询悦淘用户是否存在
        $clientUserData = LoginClientService::getClientUser(['mobile' => $params['mobile']]);

        if(empty($clientUserData)){ //不存在，请求注册

            //不存在,同步注册
            $addClientRes = LoginClientService::register($params);

            if(!$addClientRes){
                return self::responseJson(400 , '注册失败');
            }
        }

        //重新获取用户信息
        $clientUserData = LoginClientService::getClientUser(['mobile' => $params['mobile']]);

        $addData['mid']         = $clientUserData->mid;
        $addData['nickname']    = $clientUserData->nickname;
        $addData['code_number']  = $clientUserData->codeNumber;
        $addData['created_time'] = date("Y-m-d H:i:s");

        //添加用户信息
        $addRes = UserModel::addUser($addData);

        if(!$addData){

            DB::rollBack();
            return self::responseJson(400 , '注册失败');
        }

        DB::commit();

        $token = self::makeToken($addData['mid']);

        $returnData = [
            'token' => $token,
            'date'  => [
                'mobile'   => $addData['mobile'],
                'nickname' => $addData['nickname'],
                'mid'      => $addData['mid']
            ]
        ];

        return self::responseJson(200 , '注册成功' , $returnData);
    }

    /**
     * 新版验证验证码
     * @param $mobile
     * @param $code
     * @param string $templateId
     * @return object
     * @author  liujiaheng
     * @created 2020/4/17 4:00 上午
     */
    public static function verificationCode($mobile, $code, $templateId = "300005")
    {
        $key = "msg_channel_{$mobile}";

        $channel = Redis::get($key);
        $channel = intval($channel);

        $result = (object)[];
        if($channel == 1) {
            $sendKey = "msg_{$mobile}";
            $rCode = Redis::get($sendKey);
            if($rCode == $code) {
                $result->return_code = "SUCCESS";
            }else {
                $result->code = "12002004";
                $result->return_code = "FAIL";
            }
        }

        if($channel == 2) {
            $data = [
                "templateId" => $templateId,
                "receiver" => $mobile,
                "code" => $code
            ];
            $url = self::$send_message_url . "/verification/verify";
            $param = json_encode($data);
            $result = PhoneMessageService::newSend($url, $param);
        }
        return $result;
    }


    /**
     * 滑块验证token是否正确
     * @param $token
     * @return array
     */
    public static function changeImageToken($token)
    {
        $code = Config("errors.com_http_code.service_error");
        /**构造入参为appId和appSecret
         * appId和前端验证码的appId保持一致，appId可公开
         * appSecret为秘钥，请勿公开
         * token在前端完成验证后可以获取到，随业务请求发送到后台，token有效期为两分钟**/
        $appId = "22670a97d0ee5e5621e76ad732effce9";

        $appSecret = "54e8b18630c0215945e11e3f825d285f";

        $client = new CaptchaClient($appId, $appSecret);
        $client->setTimeOut(20);      //设置超时时间，默认2秒
        //特殊情况可以额外指定服务器，默认情况下不需要设置
        $response = $client->verifyToken($token);  //token指的是前端传递的值，即验证码验证成功颁发的token

        Log::info('dx response'.json_encode($response));
        if ($response->result) {
            /**token验证通过，继续其他流程**/
            return ["code" => 200, "msg" => "发送成功"];
        } else {
            return ["code" => $code, "msg" => "网络错误"];
            /**token验证失败**/
        }
    }

    /**
     * @param $password
     * @return array
     * @author   liuwenhao
     * @time     2020/4/14 14:31
     * @method   method   [密码加密]
     */
    public static function getEncrypt($password){

        $saltVal = substr(md5($password) , 0 , 4);

        //拼接上盐值进行加密
        $pwd = md5($saltVal . $password);

        return $pwd;
    }

    /**
     * @param $memberId
     * @return string
     * @author   liuwenhao
     * @time     2020/4/14 14:14
     * @method   method   [生成token]
     */
    public static function makeToken($memberId)
    {
        $key        = "data_robot_user_token_mid_". $memberId;
        $token      = self::getToken($memberId);

        if(!empty($token)) {
            $expire  = 3600*3;
            Redis::expire($key, $expire);
            return $token;
        }

        $time       = time();
        $tokenStr   = "{$memberId}.{$time}";
        $token      = md5($tokenStr);
        $expire     = 3600*3;
        Redis::set($key, $token);
        Redis::expire($key, $expire);
        return $token;
    }

    /**
     * @param $memberId
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 14:15
     * @method   method   [获取token]
     */
    public static function getToken($memberId)
    {
        $key  = "data_robot_user_token_mid_". $memberId;
        return Redis::get($key);
    }

    /**
     * @param $param
     * @param $member
     */
    public static function updatePassword($params, $member)
    {
        if(!isset($params['mobile']) || empty($params['mobile'])){
            return self::responseJson(400 , '手机号不能为空');
        }

        if(!isset($params['mobileCode']) || empty($params['mobileCode'])){
            return self::responseJson(400 , '验证码不能为空');
        }
        if(empty($member)){
            return self::responseJson(400, '参数有误');
        }

        $codeRes = self::verificationCode($params['mobile'] , $params['mobileCode'], '2300002');

        $codeRes = json_decode(json_encode($codeRes), true);

        if (empty($codeRes)||$codeRes['return_code'] !== 'SUCCESS') {
            return self::responseJson(400, '验证码已失效');
        }

        if($params['newPassword']==$params['confirmPassword']){
            $addRes = UserModel::updateUser(['mid'=>$member['mid'],'password'=>$params['newPassword']]);
            if($addRes){
                $logMessage = '修改密码';
                UserOperationLogService::insertAdminOperation(
                    $member['mid'], 2, json_encode(['mid'=>$member['mid'],'password'=>$params['newPassword']]), $logMessage, $member['mid']
                );
                return self::responseJson(200, '密码修改成功');
            }else{
                return self::responseJson(200, '密码修改成功');
            }
        }else{
            return self::responseJson(400, '确认密码和新密码不一致');
        }

    }



    /**
     * @param $code
     * @param $res
     * @param $data
     * @return false|string
     * @author   liuwenhao
     * @time     2020/4/14 12:56
     * @method   method   [返回json格式]
     */
    private static function responseJson($code , $res , $data = []){
        $result = ['code' => $code , 'msg' => $res , 'data' => $data];
        return json_encode($result);
    }
}
