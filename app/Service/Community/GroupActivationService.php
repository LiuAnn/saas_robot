<?php
/**
 * 群激活
 * spz
 */

namespace App\Service\Community;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use  App\Model\Community\CommunityModel;
use  App\Model\Community\GroupActivationModel;
use  App\Model\Community\GroupGoodsDataModel;
use  App\Model\RobotOrder\RobotModel;

class GroupActivationService
{

    /**
     * 获取社群首页
     * @param $mid
     * @return array
     */
    public static function getGroupInfoByMid($mid, $page, $pageSize)
    {
        $userInfo = self::getMemberBriefInfo($mid);
        //激活的数量
        $actiNum = GroupActivationModel::getActivationNum($mid, 1);
        //未激活的数量
        $notActiNum = GroupActivationModel::getActivationNum($mid, 0);

        //初始化数据
        $shareData=$ActivationGroupData = $groupSaleGoodsData = $NotActivationGroupData = array();

        //社团首页展示的商品
        $groupSaleGoodsData = GroupGoodsDataModel:: ActivationGroupGoodsDataIndex();
        //未激活的的群数据
        $page = ($page - 1) * $pageSize;
        $NotActivationGroupData = GroupActivationModel::getActivationGroupData($mid, 0, $page, $pageSize);
        if (!empty($NotActivationGroupData)) {
            foreach ($NotActivationGroupData as $v) {
                $broweseNum = GroupActivationModel::browseCountNum($v->room_id);
                $v->browese_num = !empty($broweseNum) ? '今日'.$broweseNum .'人看过': '';
//                $key = "member_count_{$v->room_wxid}";
//                $newCountNum = Redis::get($key);
//                if ($newCountNum) {
//                    $v->new_friend = $newCountNum;
//                } else {
//                    $v->new_friend = 0;
//                }
//                $v->new_num = 0;
                $bad_num = GroupActivationModel::getActivationNumberCount($v->room_id);
                $bad_num = is_numeric($bad_num) ? $bad_num : 0;
                $v->bad_num = (10-$bad_num)>0 ?'还差'.(10-$bad_num).'人激活':'';

            }
        }
        if (!empty($actiNum)) {  //激活的群数据
            $ActivationGroupData = GroupActivationModel::getActivationGroupData($mid, 1, $page, $pageSize);
            if (!empty($ActivationGroupData)) {
                foreach ($ActivationGroupData as $v) {
                    $broweseNum = GroupActivationModel::browseCountNum($v->room_id);
                    $v->browese_num = !empty($broweseNum) ? '今日'.$broweseNum .'人看过': '';
//                    $key = "member_count_{$v->room_wxid}";
//                    $newCountNum = Redis::get($key);
//                    if ($newCountNum) {
//                        $v->new_num = '今日'.$newCountNum.'人看过';
//                    } else {
//                        $v->new_num = 0;
//                    }
                    $v->bad_num='';
                }
            }
        } else {
            $ActivationGroupData = [];
        }
        //首页分享的数据
//        if($notActiNum==1){
//            $shareData=$NotActivationGroupData[0];
//        }
        $returnData = [
            'raceLamp' => '分享链接到群，有10个你的会员点击就可激活该群，赚群管理奖励！',
            'actiNum' => $actiNum,
            'notActiNum ' => $notActiNum,
//            'shareData' => $shareData,
            'userInfo' => $userInfo,
            'groupSaleGoodsData' => $groupSaleGoodsData,
            'actionGroupData' => $ActivationGroupData,
            'notActionGroupData' => $NotActivationGroupData,
        ];
        return responseJson(200, '获取成功',$returnData);
    }
    /**
     * 列表展示 激活群  非激活群
     * @param $uid
     * @param $type
     * @param $page
     * @param $pageSize
     */
    public static function  getGroupListByPage($mid,$type,$page,$pageSize)
    {
        $newPage = ($page - 1) * $pageSize;
        $ActivationGroupData = GroupActivationModel::getActivationGroupData($mid, $type, $newPage, $pageSize);
        if($type==0){
            foreach($ActivationGroupData as $v)
            {
                $broweseNum = GroupActivationModel::browseCountNum($v->room_id);
                $v->browese_num = !empty($broweseNum) ? '今日'.$broweseNum .'人看过': '';
                $bad_num = GroupActivationModel::getActivationNumberCount($v->room_id);
                $bad_num = is_numeric($bad_num) ? $bad_num : 0;
                $v->bad_num = (10-$bad_num)>0 ?'还差'.(10-$bad_num).'人激活':'';
            }
        }else if($type==1){
            foreach($ActivationGroupData as $v)
            {
                $broweseNum = GroupActivationModel::browseCountNum($v->room_id);
                $v->browese_num = !empty($broweseNum) ? '今日'.$broweseNum .'人看过': '';
                $v->bad_num='';
            }
        }
        return responseJson(200, '获取成功', $ActivationGroupData);
    }
    /**
     * 获取默认头像
     */
    public static function getAvatar($litpic = '')
    {
        return empty($litpic) ? "http://images.yueshang.store/pubfile/2018/07/17/line_1531821979.png" : $litpic;
    }

    /**
     * 获取用户的基本信息
     * @param $uid
     * @return array
     */
    public static function getMemberBriefInfo($uid)
    {
        $key = "getMemberBriefInfoGroup_{$uid}";
        $rest = json_decode(Redis::get($key));
//        if (empty($userInfo)) {
            $rest = [
                'litpic' => self::getAvatar(),
                'nickName' => '',
                'memberType' => '',
                'memberImg' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/22/yuelvhuipxIdlaom2X1586355260.png',
            ];
            $uid=20;
            $key = "getMemberBriefInfoGroup_{$uid}";
            $member =     GroupActivationModel::getBriefInfo($uid);
            $memberInfo = GroupActivationModel::getMemberLevelInfo($uid);

            if (empty($member)) {
                return $rest;
            }
            $rest['litpic'] = $member['litpic'];
            $rest['nickName'] = $member['nickname'];
            $rest['cardType'] = (0 >= $member['card_type']) ? 0 : $member['card_type'];
            $rest['memberType'] = empty($member['card_type']) ? '普通会员' : '精英会员';

            if (!empty($memberInfo)) {
                $rest['memberName'] = $memberInfo[0]->name;
                $rest['memberImg'] = $memberInfo[0]->icon;
            } else {
                if ($rest['cardType']) {
                    $rest['memberName'] = '精英会员';
                    $rest['memberImg'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/22/yuelvhuiw5Hy7u82171586355292.png';
                }
            }
            Redis::set($key, json_encode($rest));
            Redis::expire($key, 1800);
//        }
        return $rest;
    }
    /**
     * 群未激活详情
     */
    public static  function getGroupNotActivatioInfoByRoomId($mid,$roomId)
    {
        $groupInfo =$midInfo = $goodsData=$haveActivation= $copyWriting = array();

        //统计群激活的的链接
        $countNum =  GroupActivationModel::getActivationNumberCount($roomId);
        $groupType=0;
        //社团首页展示的商品
        $goodsData = GroupGoodsDataModel:: ActivationGroupGoodsDataIndex();
        if(!empty($mid)&&!empty($roomId)){
            $groupInfo = GroupActivationModel::getGroupInfoByRoomId($mid,$roomId,$field="scm.id as room_id,scm.room_wxid,scm.name,scm.head_img,scm.member_count,scm.is_activation");
        }
        if(!empty($countNum)&&!empty($groupInfo)){
            $member = GroupActivationModel::getBriefInfo($mid);
            if($countNum>=10){
                $groupType=2;
                $midInfo=[
                    'myGroupDesc'=>'我的“'.$groupInfo->name.'”群已经激活成功，邀请更多好友进群享受群特权吧~！',
                    'nickName'=>isset($member['nickname'])?$member['nickname']:'',
                    'headPic'=>isset($member['litpic'])?$member['litpic']:'http://images.yueshang.store/pubfile/2018/07/17/line_1531821979.png',
                ];
            }else{
                $groupType=1;
                $haveActivation=[
                    'bgImage'=>'',
                    'myGroupDesc'=>'我的“'.$groupInfo->name.'”群还未激活，赶快邀请你的粉丝帮你激活吧！',
                    'nickName'=>isset($member['nickname'])?$member['nickname']:'',
                    'headPic'=>isset($member['litpic'])?$member['litpic']:'http://images.yueshang.store/pubfile/2018/07/17/line_1531821979.png',
                    'activationData'=> GroupActivationModel::getActivationData($roomId),
                ];
            }
        }else{
            $copyWriting=[
                'title'=>'激活你的微信群',
                'titleDesc'=>'享群福利 赚群管理奖励',
                'rule'=>['你还没有激活群','赶快点击右下角“分享”分享到自己群','邀请10位会员帮你激活群吧'],
            ];
        }
        $returnData=[
            'groupType'=>$groupType,
            'midInfo'=>$midInfo,
            'goodsData'=>$goodsData,
            'groupInfo'=>$groupInfo,
            'copyWriting'=>$copyWriting,
            'haveActivation'=>$haveActivation,
        ];
        return responseJson(200, '获取成功',$returnData);
    }
    /**
     * @param $param
     */
    public  static  function addGoodsInfo($param){
        Log::info('addReload' .  json_encode($param));
        if(!empty($param['goodsId'])&& !empty($param['room_id'])){
            $insertData =[
                'mid'=>empty($param['mid'])?0:$param['mid'],
                'goods_id'=>empty($param['goodsId'])?0:$param['goodsId'],
                'romm_id'=>empty($param['room_id'])?0:$param['room_id'],
                'robot_send'=>isset($param['robot_send']) && !empty($param['robot_send'])?1:0,
                'sku_id'=>empty($param['sku_id'])?0:$param['sku_id'],
                'created_at'=> date('Y-m-d H:i:s',time()),
                'channel'=>isset($param['channel'])?$param['channel']:1,
                'goods_type'=>isset($param['goods_type'])?$param['goods_type']:1,
            ];
             RobotModel::addOrderInfo('room_activation',$insertData);
            $res = RobotModel::getChakanMid($param['mid'],$param['room_id']);
            /**  用户商品记录 $userRes */
            $userRes = RobotModel::getUserChakanMid($param['mid'],$param['room_id'],$param['goodsId'],$param['channel'],$param['goods_type'],$param['robot_send']);
            if(empty($userRes)){
                $insertDataGoods=[
                    'mid'=>empty($param['mid'])?0:$param['mid'],
                    'romm_id'=>empty($param['room_id'])?0:$param['room_id'],
                    'robot_send'=>isset($param['robot_send']) && !empty($param['robot_send'])?1:0,
                    'goods_id'=>empty($param['goodsId'])?0:$param['goodsId'],
                    'sku_id'=>empty($param['sku_id'])?0:$param['sku_id'],
                    'created_at'=> date('Y-m-d H:i:s',time()),
                    'count_num'=>1,
                    'channel'=>isset($param['channel'])?$param['channel']:1,
                    'goods_type'=>isset($param['goods_type'])?$param['goods_type']:1,
                ];
                RobotModel::addOrderInfo('goods_browse',$insertDataGoods);
            }else{
                $data['count_num']=$userRes->count_num+1;
                $data['id']=$userRes->id;
                RobotModel::updateSee('goods_browse','id',$data);
            }
            /**  用户商品记录 $userRes */
            if(empty($res)){
                $insertData1=[
                    'mid'=>empty($param['mid'])?0:$param['mid'],
                    'romm_id'=>empty($param['room_id'])?0:$param['room_id'],
                    'robot_send'=>isset($param['robot_send']) && !empty($param['robot_send'])?1:0,
                    'channel'=>isset($param['channel'])?$param['channel']:1,
                    'created_at'=> date('Y-m-d H:i:s',time()),
                    'count_num'=> 0,
                ];
                RobotModel::addOrderInfo('room_browse',$insertData1);
            }else{
                $data['count_num']=$res->count_num+1;
                $data['id']=$res->id;
                RobotModel::updateSee('room_browse','id',$data);
            }
        }
    }


}