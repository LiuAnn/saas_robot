<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/8
 * Time: 19:49
 */

namespace App\Service\Community;

use App\Model\Community\StrategyArticleModel;
use App\Model\Community\StrategyCategoryModel;
use App\Service\Robot\UserOperationLogService;

class StrategyArticleService
{
    public static function addStrategyArticle($memberInfo, $param = [])
    {
        //检测参数
        $checkParam = self::checkStrategyArticleParams($param,'add');

        if (!empty($checkParam))
        {
            return ['code' => 400, 'msg' => $checkParam['msg']];
        }

        $param['created_at'] = date('Y-m-d H:i:s',time());

        $strategyArticle = StrategyArticleModel::create($param);

        if ($strategyArticle)
        {
            $logMessage = '新增攻略文章';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $strategyArticle
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function updateStrategyArticle($memberInfo, $param = [])
    {
        //检测参数
        $checkParam = self::checkStrategyArticleParams($param,'update');

        if (!empty($checkParam))
        {
            return ['code' => 400, 'msg' => $checkParam['msg']];
        }

        $article_result = StrategyArticleModel::where('deleted_at',0)->find($param['id']);
        if (empty($article_result))
        {
            return ['code' => 400, 'msg' => '文章信息不存在或已删除'];
        }

        $param['updated_at'] = date('Y-m-d H:i:s',time());
        if (isset($param['text_content'])) {
            unset($param['text_content']);
        }

        $strategyArticle = $article_result->update($param);

        if ($strategyArticle)
        {
            $logMessage = '修改攻略文章';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    public static function moveStrategyArticle($memberInfo, $param = [], $status = '')
    {
        if ($status == 'move') {
            if (empty($param['strategy_category_id']) || !is_numeric($param['strategy_category_id']))
            {
                return ['code' => 400, 'msg' => '分类ID不能为空'];
            }
            $strategy_category_id = $param['strategy_category_id'];
        } else {
            $strategy_category_id = 0;
        }
        if (empty($param['id']) || !is_array($param['id']))
        {
            return ['code' => 400, 'msg' => '文章ID不能为空'];
        }

        $update  = [
            'strategy_category_id' => $strategy_category_id
        ];
        $result = StrategyArticleModel::moveStrategyArticle($update, $param);

        $changeId = implode(',', $param['id']);
        if ($result !== false)
        {
            $logMessage = '修改文章所属分类';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($update), $logMessage, $changeId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function checkStrategyArticleParams($param = [],$checkType = 'update')
    {
        if ($checkType == 'update')
        {
            if (empty($param['id']))
            {
                return ['code' => 400, 'msg' => 'ID不能为空'];
            }
        }

        if (empty($param['strategy_category_id']))
        {
            return ['code' => 400, 'msg' => '分类ID不能为空'];
        }

        if (empty($param['title']))
        {
            return ['code' => 400, 'msg' => '标题不能为空'];
        }

        if (empty($param['content']))
        {
            return ['code' => 400, 'msg' => '内容不能为空'];
        }

        if (!empty($param['is_top']) && $param['is_top']==1)
        {
            if (empty($param['top_img']))
            {
                return ['code' => 400, 'msg' => '置顶封面图不能为空'];
            }

            if (empty($param['top_content_img']))
            {
                return ['code' => 400, 'msg' => '置顶详情图不能为空'];
            }
        }

        if (!empty($param['invite_img']))
        {
            if (empty($param['invite_exclusive_img']))
            {
                return ['code' => 400, 'msg' => '邀请专属图不能为空'];
            }
        }

        if (!empty($param['is_video']) && $param['is_video']==1)
        {
            if (empty($param['video_url']))
            {
                return ['code' => 400, 'msg' => '视频不能为空'];
            }
        }
    }

    public static function getStrategyArticleList($param = [])
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result = StrategyArticleModel::getStrategyArticleList($param);
        $count  = StrategyArticleModel::getStrategyArticleListCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatStrategyArticleList($result)
            ],
        ];
    }

    public static function formatStrategyArticleList($data=[])
    {
        $list = [];

        if($data)
        {
            $num = 1;
            foreach ($data as $key => $item)
            {
                if ($key >= 1) {
                    if ($data[$key-1]->strategy_category_id == $item->strategy_category_id) {
                        $num ++;
                    } else {
                        $num = 1;
                    }
                }
                $list[] = [
                    "id"                   => $item->id,
                    'num'                  => $num,
                    "strategy_category_id" => $item->strategy_category_id . '',
                    "is_top"               => $item->is_top . '',
                    "top_img"              => $item->top_img . '',
                    "top_content_img"      => $item->top_content_img . '',
                    "invite_img"           => $item->invite_img . '',
                    "invite_exclusive_img" => $item->invite_exclusive_img . '',
                    "title"                => $item->title . '',
                    "desc"                 => $item->desc . '',
                    "content"              => $item->content . '',
                    "button_type"          => self::getButtonType($item->button_type) . '',
                    "is_video"             => $item->is_video . '',
                    "video_url"            => $item->video_url . '',
                    "sort"                 => $item->sort . '',
                    "created_time"         => !empty($item->created_at) ? $item->created_at . '' : '',
                ];
            }
        }
        return $list;
    }

    public static function getButtonType($buttonType)
    {
        $string = "";
        //1:去升级,2:复制微信号,3:去邀请
        switch ($buttonType) {
            case 1 :
                $string = "去升级";
                break;
            case 2 :
                $string = "复制微信号";
                break;
            case 3 :
                $string = "去邀请";
                break;
        }
        return $string;
    }

    public static function getStrategyArticle($param = [])
    {
        if (empty($param['id']))
        {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }

        $article_result = StrategyArticleModel::where('deleted_at',0)->find($param['id']);
        if (empty($article_result))
        {
            return ['code' => 400, 'msg' => '文章信息不存在或已删除'];
        }

        return [
            "code" => 200,
            "data" => self::formatStrategyArticle($article_result)
        ];
    }



    public static function deleteStrategyArticle($memberInfo, $param = [])
    {
        if (empty($param['id']))
        {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        $where = ['deleted_at' => time()];
        $article_result = StrategyArticleModel::where('id',$param['id'])->update($where);
        if ($article_result)
        {
            $logMessage = '删除攻略文章';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($where), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function formatStrategyArticle($data)
    {
        $array = [];
        if (!empty($data))
        {
            $category_result = StrategyCategoryModel::find($data->strategy_category_id);

            //TODO  暂时富文本转化  纯文本
            $content = htmlspecialchars_decode($data->content);
            //$content = str_replace(" ", "", $content);
            $content = strip_tags($content);

            $array = [
                "id"                   => $data->id,
                "strategy_category_id" => $data->strategy_category_id,
                "is_top"               => $data->is_top,
                "top_img"              => $data->top_img . '',
                "top_content_img"      => $data->top_content_img . '',
                "invite_img"           => $data->invite_img . '',
                "invite_exclusive_img" => $data->invite_exclusive_img . '',
                "title"                => $data->title . '',
                "desc"                 => $data->desc . '',
                "text_content"         => $data->content . '',
                "content"              => $content . '',
                "button_type"          => $data->button_type,
                "is_video"             => $data->is_video,
                "video_url"            => $data->video_url . '',
                "sort"                 => $data->sort . '',
                "created_at"           => !empty($data->created_at) ? $data->created_at . '' : '',
                'name'                 => !empty($category_result->name) ? $category_result->name : '',
            ];
        }

        return $array;
    }


}