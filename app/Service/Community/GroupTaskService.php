<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/5/7
 * Time: 11:46
 */

namespace App\Service\Community;

use Illuminate\Support\Facades\Redis;
use App\Service\VoucherBag\CouponService;
use App\Model\Community\GroupTaskModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use  App\Model\Source\SendInfoModel;
use App\Model\Admin\GroupModel;
use App\Service\Community\UseIndexService;
use  App\Service\Community\PullTaskRedisDataService;
use Exception;

class GroupTaskService
{   
    const TASKSTATUS = [0=>'进行中',1=>'已完成',2=>'已超时',3=>'未开始'];
    const ISSENDMONEY = [0=>'未发送',1=>'已发送'];
    private static $linJuTuanUrl = "http://neighbour.yuetao.vip";//邻居团接口地址

    /**
     * 获取任务列表
     */
    public static function getTaskList($param,$member){

        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $pageSize = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param['platform_identity'] = $member['platform_identity'];
        $param['page'] = $page;
        $param['pageSize'] = $pageSize;
        $result    = GroupTaskModel::getTaskList($param);
        $count     = GroupTaskModel::getTaskCount($param);
        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => self::formatTaskInfo($result,$param)
            ],
        ];
    }

    /**
     * 格式化任务列表
     */
    public static function formatTaskInfo($result,$param){

        if(empty($result)){

            return [];
        }

        $list = [];

        foreach ($result as $key => $v) {

            $list[$key]['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list[$key]['id'] = $v['id'];//任务ID
            $list[$key]['taskName'] = $v['task_name'];
            $list[$key]['beginTime'] = date("Y-m-d H:i:s",$v['begin_time']);//开始时间
            $list[$key]['endTime'] =  date("Y-m-d H:i:s",$v['end_time']);//结束时间
            $list[$key]['goodsNum'] = $v['goods_num'];//商品数量
            $list[$key]['lowGoodsNum'] = $v['low_goods_num'];//下级商品数量
            $list[$key]['completedPeople'] = $v['completed_people'];//完成人数
            $list[$key]['bonusMoney'] = $v['bonus_money']/100;//奖励金
            $list[$key]['adminName'] = $v['admin_name'];//添加人
            $list[$key]['isExamine'] = $v['is_examine'];//是否需要审核
            $list[$key]['examineStatus'] = $v['examine'];//审核状态0审核中1以通过
            $list[$key]['examineImage'] = $v['examine_image'];
        }

        return $list;

    }

    /**
     * 获取任务列表
     */
    public static function getGroupTaskListForTaskId($param,$member){

        if(!isset($param['taskId'])||empty($param['taskId'])){

            return ['code'=>400,'msg'=>'任务标识不能为空'];
        }

        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $pageSize = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param['platform_identity'] = $member['platform_identity'];
        $param['page'] = $page;
        $param['pageSize'] = $pageSize;
        $result    = GroupTaskModel::getGroupTaskList($param);
        $count     = GroupTaskModel::getGroupTaskCount($param);
        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => self::formatGroupTaskList($result,$param)
            ],
        ];
    }

    /**
     * 格式化对外输出任务列表
     */
    public static function formatGroupTaskList($result,$param){

        if(empty($result)){

            return [];
        }

        $list = [];

        foreach ($result as $key => $v) {

            $list[$key]['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list[$key]['name'] = $v['nick_name'];
            $list[$key]['mobile'] = $v['mobile'];
            $list[$key]['taskStatusId'] = $v['task_status'];
            $list[$key]['taskStatus'] = self::TASKSTATUS[$v['task_status']];//任务状态
            $list[$key]['isSendMoney'] =  self::ISSENDMONEY[$v['task_status']];//任务状态


            $taskGoodsList = GroupTaskModel::getTaskGoodsInfo(['task_id'=>$v['task_id']]);

            $taskGoodsData = [];

            foreach ($taskGoodsList as $k => $value) {

                $taskGoodsData[$k]['key'] = $value['id'];
                $taskGoodsData[$k]['goodId'] = $value['good_id'];
                $taskGoodsData[$k]['goodName'] = $value['good_name'];
                $taskGoodsData[$k]['goodImg'] = $value['good_img'];
                $taskGoodsData[$k]['price'] = $value['vip_price']/100;
                $taskGoodsData[$k]['commission'] = $value['commission'];
                $getGroupTaskGoodsInfoByGoodsId = GroupTaskModel::getGroupTaskGoodsInfoByGoodsId(['task_id'=>$v['task_id'],'mid'=>$v['mid'],'goods_id'=>$value['good_id']]);
                if(empty($getGroupTaskGoodsInfoByGoodsId)){
                    $taskGoodsData[$k]['saleNum'] = 0;
                    $taskGoodsData[$k]['havingSaleNum'] = $value['sale_num'];
                }else{
                    $taskGoodsData[$k]['saleNum'] = $getGroupTaskGoodsInfoByGoodsId['sale_num'];
                    $taskGoodsData[$k]['havingSaleNum'] = $value['sale_num']-$getGroupTaskGoodsInfoByGoodsId['sale_num']>0?$value['sale_num']-$getGroupTaskGoodsInfoByGoodsId['sale_num']:0;
                }
                
            }

            $list[$key]['goodsList'] = $taskGoodsData;            

        }

        return $list;

    }

    /**
     * 添加任务
     */
    public static function addTask($param,$member){



        if(!isset($param['goodsData'])||empty($param['goodsData'])){

            return ['code'=>400,'msg'=>'商品数据不能为空'];
        }

        if(!isset($param['bonusMoney'])||empty($param['bonusMoney'])){

            return ['code'=>400,'msg'=>'奖励金不能为空'];
        }

        if(!isset($param['taskName'])||empty($param['taskName'])){

            return ['code'=>400,'msg'=>'任务标题不能为空'];
        }

        $insertGoodData = [];
        $goodsIds = [];
        foreach ($param['goodsData'] as $key => $value) {

            $data = [];
            
            if(empty($value['goodId'])){

                return ['code'=>400,'msg'=>'商品数据不全goodId'];
            }

            if(empty($value['goodName'])){

                return ['code'=>400,'msg'=>'商品数据不全goodName'];
            }

            if(empty($value['goodImg'])){

                return ['code'=>400,'msg'=>'商品数据不全goodImg'];
            } 
            
            if(empty($value['goodType'])){

                return ['code'=>400,'msg'=>'商品数据不全goodType'];
            }

            if(empty($value['vipPrice'])){

                return ['code'=>400,'msg'=>'商品数据不全vipPrice'];
            }

            if(empty($value['saleNum'])){

                return ['code'=>400,'msg'=>'商品数据不全saleNum'];
            }

            $findGoodsFromTime = self::findGoodsFromTime(strtotime($param['beginTime']),strtotime($param['endTime']),$value['goodId'],0,$member['platform_identity']);

            if($findGoodsFromTime==true){

                return ['code'=>400,'msg'=>'相同时间段内的活动已有'.$value['goodName']];
            }

            $data['good_id'] = $value['goodId'];
            $data['good_sku_id'] = $value['goodSkuId']?$value['goodSkuId']:0;
            $data['middle_good_id'] = $value['middleGoodId']?$value['middleGoodId']:0;
            $data['middle_sku_id'] = $value['middleSkuId']?$value['middleSkuId']:0;
            $data['good_name'] = $value['goodName'];
            $data['good_img'] = $value['goodImg'];
            $data['good_type'] = $value['goodType'];
            $data['vip_price'] = $value['vipPrice']*100;
            $data['original_price'] = !empty($value['originalPrice'])?$value['originalPrice']*100:0;
            $data['buy_price'] = !empty($value['buyPrice'])?$value['buyPrice']*100:0;
            $data['sale_num'] = $value['saleNum'];
            $data['commission'] = !empty($value['commission'])?$value['commission']:0;

            $goodsIds[$key]['goodsId'] = $value['goodId'];
            $goodsIds[$key]['num'] = $value['saleNum'];

            $insertGoodData[$key] = $data;
        }

        $insertTaskData = [];

        $priceProductIdsData = [];

        switch ($member['platform_identity']) {
            case 'c23ca372624d226e713fe1d2cd44be35':
                $priceProductIdsData = json_encode(['bonusMoney'=>$param['bonusMoney'],'data'=>$goodsIds]);
                $result = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/priceProductIds", ['product_ids'=>$priceProductIdsData]),true);
                break;
            
            default:
                # code...
                break;
        }

        $isExamine = 0; 
        $examineReson = "";
        if(isset($result['status'])&&$result['status']==200){

            if($result['data']['is_use']==false){
                $isExamine = 1;//需要审核
                $examineReson = "该任务奖励金额存在风险，需要特批";
                $insertTaskData['is_examine'] = 1;//需要审核
                $insertTaskData['examine'] = 0;//0 审核中 1 审核成功
            }else{

                $insertTaskData['is_examine'] = 0;//需要审核
                $insertTaskData['examine'] = 1;//0 审核中 1 审核成功               
            }

        }else{

            return ['code'=>400,'msg'=>'预警失败'];
        }

        

        $insertTaskData['goods_num'] = count($insertGoodData);
        $insertTaskData['begin_time'] = strtotime($param['beginTime']);
        $insertTaskData['end_time'] = strtotime($param['endTime']);
        $insertTaskData['bonus_money'] = $param['bonusMoney']*100;//奖励金
        $insertTaskData['task_name'] = $param['taskName'];//任务名称
        $insertTaskData['platform_identity'] = $member['platform_identity'];//平台标识
        $insertTaskData['admin_id'] = $member['mid'];
        $insertTaskData['admin_name'] = $member['nickname'];



        DB::beginTransaction();

        try {

            $task_id = GroupTaskModel::insertTaskData($insertTaskData);

            if(!$task_id){

                return ['code'=>400,'msg'=>'添加失败'];
            }

            foreach ($insertGoodData as $k => $v) {
                
                $insertGoodData[$k]['task_id'] = $task_id;
            }


            $result = GroupTaskModel::insertGoodData($insertGoodData);

            DB::commit();

            return ['code'=>200,'msg'=>'添加成功','data'=>['isExamine'=>$isExamine,'examineReson'=>$examineReson]];

        } catch (Exception $e) {

            Db::rollback();

            return ['code'=>400,'msg'=>'活动添加失败'];
        }        
    }


    /**
     * 获取任务详情
     */
    public static function getTaskInfo($param){

        if(!isset($param['taskId'])||empty($param['taskId'])){

            return ['code'=>400,'msg'=>'任务ID不能为空'];
        }

        $taskInfo = GroupTaskModel::getTaskInfo(['id'=>$param['taskId']]);

        if(empty($taskInfo)){

            return ['code'=>400,'msg'=>'任务不存在'];
        }

        $data = [];

        $data['taskId'] = $taskInfo->id;
        $data['beginTime'] = date("Y-m-d H:i:s",$taskInfo->begin_time);
        $data['endTime'] = date("Y-m-d H:i:s",$taskInfo->end_time);
        $data['bonusMoney'] = $taskInfo->bonus_money/100;
        $data['taskName'] = $taskInfo->task_name;

        $taskGoodsList = GroupTaskModel::getTaskGoodsInfo(['task_id'=>$param['taskId']]);

        $taskGoodsData = [];

        foreach ($taskGoodsList as $key => $value) {
            
            $taskGoodsData[$key]['key'] = $value['id'];
            $taskGoodsData[$key]['goodId'] = $value['good_id'];
            $taskGoodsData[$key]['goodSkuId'] = $value['good_sku_id'];
            $taskGoodsData[$key]['middleGoodId'] = $value['middle_good_id'];
            $taskGoodsData[$key]['middleSkuId'] = $value['middle_sku_id'];
            $taskGoodsData[$key]['goodName'] = $value['good_name'];
            $taskGoodsData[$key]['goodImg'] = $value['good_img'];
            $taskGoodsData[$key]['goodType'] = $value['good_type'];
            $taskGoodsData[$key]['vipPrice'] = $value['vip_price']/100;
            $taskGoodsData[$key]['originalPrice'] = $value['original_price']/100;
            $taskGoodsData[$key]['buyPrice'] = $value['buy_price']/100;
            $taskGoodsData[$key]['saleNum'] = $value['sale_num'];
            $taskGoodsData[$key]['commission'] = $value['commission'];
        }

        $data['taskGoodsData'] = $taskGoodsData;

        return [
            "code" => 200,
            "data" => $data
        ];        

    }



    /**
     * 编辑团长任务
     */
    public static function editTask($param,$member){


         if(!isset($param['taskId'])||empty($param['taskId'])){

            return ['code'=>400,'msg'=>'任务ID不能为空'];
        }


        $taskInfo = GroupTaskModel::getTaskInfo(['id'=>$param['taskId']]);

        if(empty($taskInfo)){

            return ['code'=>400,'msg'=>'任务不存在'];
        }

        if($taskInfo->task_status==2){

            return self::addTask($param,$member);
        }


        if(!isset($param['taskName'])||empty($param['taskName'])){

            return ['code'=>400,'msg'=>'任务标题不能为空'];
        }


        if(!isset($param['goodsData'])||empty($param['goodsData'])){

            return ['code'=>400,'msg'=>'商品数据不能为空'];
        }

        if(!isset($param['bonusMoney'])||empty($param['bonusMoney'])){

            return ['code'=>400,'msg'=>'奖励金不能为空'];
        }

        $insertGoodData = [];
        $goodsIds = [];
        foreach ($param['goodsData'] as $key => $value) {

            $data = [];
            
            if(empty($value['goodId'])){

                return ['code'=>400,'msg'=>'商品数据不全goodId'];
            }

            if(empty($value['goodName'])){

                return ['code'=>400,'msg'=>'商品数据不全goodName'];
            }

            if(empty($value['goodImg'])){

                return ['code'=>400,'msg'=>'商品数据不全goodImg'];
            } 
            
            if(empty($value['goodType'])){

                return ['code'=>400,'msg'=>'商品数据不全goodType'];
            }

            if(empty($value['vipPrice'])){

                return ['code'=>400,'msg'=>'商品数据不全vipPrice'];
            }

            if(empty($value['saleNum'])){

                return ['code'=>400,'msg'=>'商品数据不全saleNum'];
            }


            $findGoodsFromTime = self::findGoodsFromTime(strtotime($param['beginTime']),strtotime($param['endTime']),$value['goodId'],0,$member['platform_identity']);

            if($findGoodsFromTime==true){

                return ['code'=>400,'msg'=>'相同时间段内的活动已有'.$value['goodName']];
            }

            $data['id'] = $value['key'];
            $data['good_id'] = $value['goodId'];
            $data['good_sku_id'] = $value['goodSkuId']?$value['goodSkuId']:0;
            $data['middle_good_id'] = $value['middleGoodId']?$value['middleGoodId']:0;
            $data['middle_sku_id'] = $value['middleSkuId']?$value['middleSkuId']:0;
            $data['good_name'] = $value['goodName'];
            $data['good_img'] = $value['goodImg'];
            $data['good_type'] = $value['goodType'];
            $data['vip_price'] = $value['vipPrice']*100;
            $data['original_price'] = !empty($value['originalPrice'])?$value['originalPrice']*100:0;
            $data['buy_price'] = !empty($value['buyPrice'])?$value['buyPrice']*100:0;
            $data['sale_num'] = $value['saleNum'];
            $data['commission'] = !empty($value['commission'])?$value['commission']:0;

            $goodsIds[$key]['goodsId'] = $value['goodId'];
            $goodsIds[$key]['num'] = $value['saleNum'];

            $insertGoodData[$key] = $data;
        }

        $insertTaskData = [];

        $priceProductIdsData = [];

        switch ($member['platform_identity']) {
            case 'c23ca372624d226e713fe1d2cd44be35':
                $priceProductIdsData = json_encode(['bonusMoney'=>$param['bonusMoney'],'data'=>$goodsIds]);
                $result = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/priceProductIds", ['product_ids'=>$priceProductIdsData]),true);
                break;
            
            default:
                # code...
                break;
        }

        $isExamine = 0; 
        $examineReson = "";

        if(isset($result['status'])&&$result['status']==200){

            if($result['data']['is_use']==false){
                $isExamine = 1;//需要审核
                $examineReson = "该任务奖励金额存在风险，需要特批";
                $insertTaskData['is_examine'] = 1;//需要审核
                $insertTaskData['examine'] = 0;//0 审核中 1 审核成功
            }else{

                $insertTaskData['is_examine'] = 0;//需要审核
                $insertTaskData['examine'] = 1;//0 审核中 1 审核成功               
            }

        }else{

            return ['code'=>400,'msg'=>'预警失败'];
        }


        $insertTaskData['id'] = $param['taskId'];
        $insertTaskData['goods_num'] = count($insertGoodData);
        $insertTaskData['begin_time'] = strtotime($param['beginTime']);
        $insertTaskData['end_time'] = strtotime($param['endTime']);
        $insertTaskData['bonus_money'] = $param['bonusMoney']*100;//奖励金
        $insertTaskData['task_name'] = $param['taskName'];
        $insertTaskData['platform_identity'] = $member['platform_identity'];//平台标识
        $insertTaskData['admin_id'] = $member['mid'];
        $insertTaskData['admin_name'] = $member['nickname'];
        $insertTaskData['updated_time'] = time();


        DB::beginTransaction();

        try {

            $task_id = GroupTaskModel::updateTaskData($insertTaskData);

            if(!$task_id){

                return ['code'=>400,'msg'=>'编辑失败'];
            }

            GroupTaskModel::updateGoodDataForTaskId(['task_id'=>$param['taskId'],'is_del'=>1,'update_time'=>time(),'deleted_time'=>time()]);

            foreach ($insertGoodData as $k => $v) {
                
                $insertGoodData[$k]['task_id'] = $param['taskId'];
                if($insertGoodData[$k]['id']>0){

                    $insertGoodData[$k]['update_time'] = time();
                    $insertGoodData[$k]['is_del'] = 0;
                    GroupTaskModel::updateGoodData($insertGoodData[$k]);

                }else{

                    unset($insertGoodData[$k]['id']);

                    GroupTaskModel::insertGoodData($insertGoodData[$k]);
                }
            }

            DB::commit();

            return ['code'=>200,'msg'=>'编辑成功','data'=>['isExamine'=>$isExamine,'examineReson'=>$examineReson]];

        } catch (Exception $e) {

            Db::rollback();

            return ['code'=>400,'msg'=>'活动编辑失败'];
        }        
    }

    /**
     * 删除团长任务
     */
    public static function delTask($param,$member){


        if(!isset($param['taskId'])||empty($param['taskId'])){

            return ['code'=>400,'msg'=>'任务ID不能为空'];
        }


        $taskInfo = GroupTaskModel::getTaskInfo(['id'=>$param['taskId']]);

        if(empty($taskInfo)){

            return ['code'=>400,'msg'=>'任务不存在或已删除'];
        }

        $insertTaskData = [];

        $insertTaskData['id'] = $param['taskId'];
        $insertTaskData['platform_identity'] = $member['platform_identity'];//平台标识
        $insertTaskData['admin_id'] = $member['mid'];
        $insertTaskData['admin_name'] = $member['nickname'];
        $insertTaskData['updated_time'] = time();
        $insertTaskData['is_del'] = 1;
        $insertTaskData['deleted_time'] = time();

        $task_id = GroupTaskModel::updateTaskData($insertTaskData);

        if($task_id){

            return ['code'=>200,'msg'=>'删除成功'];
        }else{

            return ['code'=>200,'msg'=>'删除失败'];
        }

    }

    /**
     * 团长任务对外输出接口
     */
    public static function getTaskInfoForPl($param){

        if(!isset($param['platform_identity'])||empty($param['platform_identity'])){

            return ['code'=>400,'msg'=>'平台标识不能为空'];
        }

        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $pageSize = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param['platform_identity'] = $param['platform_identity'];
        $param['page'] = $page;
        $param['pageSize'] = $pageSize;
        $param['examine'] = 1;
        $result    = GroupTaskModel::getTaskList($param);
        $count     = GroupTaskModel::getTaskCount($param);
        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => self::formatTaskInfoForPl($result,$param)
            ],
        ];

    }


    /**
     * 格式化对外输出任务列表
     */
    public static function formatTaskInfoForPl($result,$param){

        if(empty($result)){

            return [];
        }

        $list = [];

        foreach ($result as $key => $v) {

            $list[$key]['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list[$key]['id'] = $v['id'];//任务ID
            $list[$key]['name'] = $v['task_name'];
            $list[$key]['beginTime'] = date("Y-m-d H:i:s",$v['begin_time']);//开始时间
            $list[$key]['endTime'] =  date("Y-m-d H:i:s",$v['end_time']);//结束时间
            $list[$key]['goodsNum'] = $v['goods_num'];//商品数量
            $list[$key]['completedPeople'] = $v['completed_people'];//完成人数
            $list[$key]['bonusMoney'] = $v['bonus_money']/100;//奖励金

            $taskGoodsList = GroupTaskModel::getTaskGoodsInfo(['task_id'=>$v['id']]);

            $taskGoodsData = [];

            foreach ($taskGoodsList as $k => $value) {
                
                $taskGoodsData[$k]['key'] = $value['id'];
                $taskGoodsData[$k]['goodId'] = $value['good_id'];
                $taskGoodsData[$k]['goodName'] = $value['good_name'];
                $taskGoodsData[$k]['goodImg'] = $value['good_img'];
                $taskGoodsData[$k]['price'] = $value['vip_price']/100;
                $taskGoodsData[$k]['buyPrice'] = $value['buy_price']/100;
                $taskGoodsData[$k]['saleNum'] = $value['sale_num'];
                $taskGoodsData[$k]['commission'] = $value['commission'];
            }

            $list[$key]['goodsList'] = $taskGoodsData;            

        }

        return $list;

    }


    /**
     * 团长任务对外输出接口
     */
    public static function getTaskListForMid($param){

        if(!isset($param['platform_identity'])||empty($param['platform_identity'])){

            return ['code'=>400,'msg'=>'平台标识不能为空'];
        }
        if(!isset($param['mid'])||empty($param['mid'])){

            return ['code'=>400,'msg'=>'用户标识不能为空'];
        }
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $pageSize = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param['platform_identity'] = $param['platform_identity'];
        $param['page'] = $page;
        $param['pageSize'] = $pageSize;
        $param['examine'] = 1;
        $param['updatetaskStatus'] = 1;
        $result    = GroupTaskModel::getTaskListForMid($param);

        $count     = GroupTaskModel::getTaskCount($param);
        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => self::formatTaskListForMid($result,$param)
            ],
        ];

    }


    /**
     * 格式化对外输出任务列表
     */
    public static function formatTaskListForMid($result,$param){

        if(empty($result)){

            return [];
        }

        $list = [];

        foreach ($result as $key => $v) {

            $list[$key]['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list[$key]['id'] = $v['id'];//任务ID
            $list[$key]['name'] = $v['task_name'];
            $list[$key]['beginTime'] = date("Y-m-d H:i:s",$v['begin_time']);//开始时间
            $list[$key]['endTime'] =  date("Y-m-d H:i:s",$v['end_time']);//结束时间
            $list[$key]['goodsNum'] = $v['goods_num'];//商品数量
            $list[$key]['bonusMoney'] = $v['bonus_money']/100;//奖励金
            $groupTaskRelation = GroupTaskModel::groupTaskRelation(['task_id'=>$v['id'],'mid'=>$param['mid']]);
            if(empty($groupTaskRelation)){

                $list[$key]['taskStatusId'] = $v['task_status'];

                if($v['task_status']==0){
                    $list[$key]['taskStatusId']=3;
                }

                if($v['task_status']==1){
                    $list[$key]['taskStatusId']=0;
                }
         
                $list[$key]['taskStatus'] = self::TASKSTATUS[$list[$key]['taskStatusId']];

            }else{

                $list[$key]['taskStatusId'] = $groupTaskRelation['task_status'];

                if($v['task_status']==2&&$groupTaskRelation['task_status']==0){

                    $list[$key]['taskStatusId']=2;
                }

                $list[$key]['taskStatus'] = self::TASKSTATUS[$list[$key]['taskStatusId']];//任务状态
            }
            

            $taskGoodsList = GroupTaskModel::getTaskGoodsInfo(['task_id'=>$v['id']]);

            $taskGoodsData = [];

            foreach ($taskGoodsList as $k => $value) {
                
                $taskGoodsData[$k]['key'] = $value['id'];
                $taskGoodsData[$k]['goodId'] = $value['good_id'];
                $taskGoodsData[$k]['goodName'] = $value['good_name'];
                $taskGoodsData[$k]['goodImg'] = $value['good_img'];
                $taskGoodsData[$k]['price'] = $value['vip_price']/100;
                $taskGoodsData[$k]['buyPrice'] = $value['buy_price']/100;
                $taskGoodsData[$k]['havingSaleNum'] = $value['sale_num'];
                $taskGoodsData[$k]['commission'] = $value['commission'];
                $getGroupTaskGoodsInfoByGoodsId = GroupTaskModel::getGroupTaskGoodsInfoByGoodsId(['task_id'=>$v['id'],'mid'=>$param['mid'],'goods_id'=>$value['good_id']]);

                if(empty($getGroupTaskGoodsInfoByGoodsId)){
                    $taskGoodsData[$k]['saleNum'] = 0;
                }else{
                    $taskGoodsData[$k]['saleNum'] = $getGroupTaskGoodsInfoByGoodsId['sale_num'];
                }

            }

            $list[$key]['goodsList'] = $taskGoodsData;            

        }

        return $list;

    }


    /**
     * 任务审核接口
     */
    public static function taskExamine($param,$member){

         if(!isset($param['taskId'])||empty($param['taskId'])){

            return ['code'=>400,'msg'=>'任务ID不能为空'];
        }


         if(!isset($param['examineImage'])||empty($param['examineImage'])){

            return ['code'=>400,'msg'=>'图片不能为空'];
        }

        $taskInfo = GroupTaskModel::getTaskInfo(['id'=>$param['taskId']]);

        if(empty($taskInfo)){

            return ['code'=>400,'msg'=>'任务不存在'];
        }

        $insertTaskData = [];

        $insertTaskData['id'] = $param['taskId'];
        $insertTaskData['platform_identity'] = $member['platform_identity'];//平台标识
        $insertTaskData['examine_mid'] = $member['mid'];
        $insertTaskData['updated_time'] = time();
        $insertTaskData['examine_time'] = time();
        $insertTaskData['examine_image'] = $param['examineImage'];

    
        DB::beginTransaction();

        try {

            $task_id = GroupTaskModel::updateTaskData($insertTaskData);

            if(!$task_id){

                return ['code'=>400,'msg'=>'操作失败'];
            }

            DB::commit();

            return ['code'=>200,'msg'=>'操作成功'];

        } catch (Exception $e) {

            Db::rollback();

            return ['code'=>400,'msg'=>'活动添加失败'];
        }   



    }




    /**
     * add group_task_relation 
     */
    public static function addGroupTaskRelation($param){

        $param['platform_identity'] = $param['platform_identity'];
        $param['updatetaskStatus'] = 1;//进行中的任务
        $param['examine'] = 1;//审核通过任务
        $count  = GroupTaskModel::getTaskCount($param);

        $pageSize = 5;
        $pageNum = ceil($count / $pageSize) + 1;

        for($i=1;$i<$pageNum;$i++){

            $param['page'] = 1;
            $param['pageSize'] = $pageSize;

            $result = GroupTaskModel::getTaskList($param);

            if(!empty($result)){
                    
                foreach ($result as $key => $value) {
                    sleep(1);
                    $finshPeople = 0;
                    $getProductIds = GroupTaskModel::getProductIdsbyTaskId($value['id']);
                    $ids= implode($getProductIds, ",");
                    $task['task_time_begin'] = date("Y-m-d H:i:s",$value['begin_time']);
                    $task['task_time_end'] = date("Y-m-d H:i:s",$value['end_time']);
                    $task['product_id_arr'] = $ids;

                    $tzTaskOrder = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/tzTaskOrder", $task),true);

                    if(isset($tzTaskOrder['data']['list'])&&!empty($tzTaskOrder['data']['list'])){

                        foreach ($tzTaskOrder['data']['list'] as $k => $v) {
                            
                            $task_status = 1;
                            $alwwaysDelivery = 1;

                            if(!empty($v['goodsList'])){

                                foreach ($v['goodsList'] as $keys => $val) {
                                    
                                    $getGoodDataForTaskId = GroupTaskModel::getGoodDataForTaskId(['task_id'=>$value['id'],'good_id'=>$val['product_id']]);
                                        
                                    $getGroupTaskGoodsRelation = GroupTaskModel::getGroupTaskGoodsInfoByGoodsId(['task_id'=>$value['id'],'goods_id'=>$val['product_id'],'mid'=>$v['mid']]);
                                    
                                    $finshGoodsNum = 0;

                                    $finshTime  = strtotime("-7 days");

                                    foreach ($val['order_list'] as $ke => $values) {
                                        
                                        if($values['status']==2&&$values['take_delivery_time']<$finshTime){

                                            $finshGoodsNum = $finshGoodsNum+$values['product_num'];
                                        }

                                    }

                                    if(!empty($getGoodDataForTaskId)){

                                        $insertGoodsTaskRelationData = [];

                                        $insertGoodsTaskRelationData['task_id'] = $value['id'];
                                        $insertGoodsTaskRelationData['mid'] = $v['mid'];
                                        $insertGoodsTaskRelationData['goods_id'] = $val['product_id'];
                                        $insertGoodsTaskRelationData['goods_name'] = $val['store_name'];
                                        $insertGoodsTaskRelationData['goods_image'] = $val['image'];
                                        $insertGoodsTaskRelationData['goods_price'] = $val['price']*100;
                                        $insertGoodsTaskRelationData['sale_num'] = $val['product_num'];

                                        if(!empty($getGroupTaskGoodsRelation)){

                                            $insertGoodsTaskRelationData['id'] = $getGroupTaskGoodsRelation['id'];

                                            $insertGoodsTaskRelationResult = GroupTaskModel::updateGroupTaskGoodsInfo($insertGoodsTaskRelationData);

                                        }else{

                                            $insertGoodsTaskRelationResult = GroupTaskModel::insertGoodsTaskRelationData($insertGoodsTaskRelationData);
                                        }

                                        if($finshGoodsNum<$getGoodDataForTaskId['sale_num']){

                                            $alwwaysDelivery = 0;
                                        }

                                        if($insertGoodsTaskRelationData['sale_num']<$getGoodDataForTaskId['sale_num']){
                                            $task_status = 0;
                                        }
                                    }
                                }
                            }else{

                                $task_status = 0;
                            }

                            if($value['goods_num']>count($v['goodsList'])){

                                $task_status = 0;
                            }

                            $insertTaskRelation = [];

                            $insertTaskRelation['task_id'] = $value['id'];
                            $insertTaskRelation['mid'] = $v['mid'];
                            $insertTaskRelation['nick_name'] = empty($v['real_name'])?$v['real_name']:$v['nickname'];
                            $insertTaskRelation['mobile'] = $v['phone'];
                            $insertTaskRelation['platform_identity'] = $param['platform_identity'];
                            $insertTaskRelation['is_send_money'] = $v['is_send_bonusMoney'];
                            $insertTaskRelation['task_status'] = $task_status;



                            $groupTaskRelation = GroupTaskModel::groupTaskRelation(['mid'=>$v['mid'],'task_id'=>$value['id']]);

                            if(empty($groupTaskRelation)){

                                $insertTaskRelationDataId = GroupTaskModel::insertTaskRelationData($insertTaskRelation);

                            }else{

                                // if($groupTaskRelation['is_send_money']==0&&$task_status==1){

                                //     $result = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/tzTaskMoney", ['task_id'=>$value['id'],'mid'=>$v['mid'],'bonusMoney'=>$value['bonus_money']/100]),true);
                                // }

                                $insertTaskRelation['id'] = $groupTaskRelation['id'];
                                $insertTaskRelationDataId = GroupTaskModel::updateTaskRelationInfo($insertTaskRelation);
                            }

                            Log::info('insertTaskRelation data:' .json_encode($insertTaskRelation));

                            if($alwwaysDelivery==1&&$value['task_status']==2&&$insertTaskRelation['is_send_money']==1){

                                $result = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/tzTaskMoneyUpdate", ['task_id'=>$value['id'],'mid'=>$v['mid'],'status'=>2]),true);
                            }



                            if($alwwaysDelivery==0&&$insertTaskRelation['task_status']==0&&$insertTaskRelation['is_send_money']==1){

                                $result = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/tzTaskMoneyUpdate", ['task_id'=>$value['id'],'mid'=>$v['mid'],'status'=>3]),true);
                            }


                            if($insertTaskRelation['task_status']==1){

                                $finshPeople = $finshPeople+1;
                            }                           

                        }
                    }

                    $insertTaskData=[];
                    $insertTaskData['id'] = $value['id'];
                    $insertTaskData['completed_people'] = $finshPeople;
                    $insertTaskData['updated_time'] = time();

                    $task_id = GroupTaskModel::updateTaskData($insertTaskData);                    

                }
            }

        }

    }

    /**
     * 判断该时间段内有没有相同的商品
     */
    public static function findGoodsFromTime($startTime,$endTime,$goodsId,$task_id=0,$platform_identity){

        $goodsList = GroupTaskModel::getGoodsListByGoodsId($goodsId,$platform_identity);

        if(!empty($goodsList)){

            foreach ($goodsList as $key => $value) {
                
                $result = self::is_time_cross($startTime,$endTime,$value['begin_time'],$value['end_time']);

                if($task_id>0&&$task_id!=$value['id']&&$result==true){

                    return true;
                }
            }

            return false;

        }else{

            return false;
        }
    }


    /**
     * updateGroupTaskStatus 
     */
    public static function updateGroupTaskStatus($param){

        $param['platform_identity'] = $param['platform_identity'];

        $updateGroupTaskStatusV1 = GroupTaskModel::updateGroupTaskStatus($param['platform_identity'],0);//任务从未开始到进行中变更
        $updateGroupTaskStatusV1 = GroupTaskModel::updateGroupTaskStatus($param['platform_identity'],1);//任务从进行中到已结束变更
    }


    /**
     * updateGroupTaskStatus 
     */
    public static function updateMoneySendStatus($param){


        $param['platform_identity'] = $param['platform_identity'];
        $param['examine'] = 1;//审核通过任务
        $param['isSendMoney'] = 0;//未发放奖励金的任务
        $param['taskStatus'] = 2;//已结束的任务

        $count  = GroupTaskModel::getTaskCount($param);  

        $pageSize = 5;
        $pageNum = ceil($count / $pageSize) + 1;

        for($i=1;$i<$pageNum;$i++){

            $param['page'] = 1;
            $param['pageSize'] = $pageSize;

            $result = GroupTaskModel::getTaskList($param);

            if(!empty($result)){

                foreach ($result as $key => $value) {

                    $param['taskId'] = $value['id'];
                    $param['platform_identity'] = $param['platform_identity'];
                    $param['task_status'] = 1;
                    $param['is_send_money'] = 0;
                    $groupTaskRelation   = GroupTaskModel::getGroupTaskListV1($param);

                    if(!empty($groupTaskRelation)){

                        foreach ($groupTaskRelation as $k => $v) {
                            sleep(1);
                            Log::info('send bonus Money begin' .json_encode($v));
                            $result = json_decode(curlPostCommon(self::$linJuTuanUrl."/api/tasks/tzTaskMoney", ['task_id'=>$value['id'],'mid'=>$v['mid'],'bonusMoney'=>$value['bonus_money']/100]),true);
                            Log::info('send bonus Money begin' .json_encode($result));                      
                        }
                    }
                }


            }

            $insertTaskData=[];
            $insertTaskData['id'] = $value['id'];
            $insertTaskData['is_send_money'] = 1;
            $insertTaskData['updated_time'] = time();

            $task_id = GroupTaskModel::updateTaskData($insertTaskData);

        }

    }


    /**
     * 计算两个时间段是否有交集
     *
     * @param string $beginTime1 开始时间1
     * @param string $endTime1 结束时间1
     * @param string $beginTime2 开始时间2
     * @param string $endTime2 结束时间2
     * @return bool
     */
    public static function is_time_cross($beginTime1 = '', $endTime1 = '', $beginTime2 = '', $endTime2 = '') {
      $status = $beginTime2 - $beginTime1;
      if ($status > 0) {
        $status2 = $beginTime2 - $endTime1;
        if ($status2 >= 0) {
          return false;
        } else {
          return true;
        }
      } else {
        $status2 = $endTime2 - $beginTime1;
        if ($status2 > 0) {
          return true;
        } else {
          return false;
        }
      }
    }



    public static function test($param){

        //310890072068
        //return UseIndexService::getZhidingMidData(['mobile' => 13730102139]);

        $channel = $param['channel'];
        $type = $param['type'];
        $beginTime = $param['beginTime'];
        $endTime = $param['endTime'];
        $goods_type = $param['goods_type'];

        if($goods_type==2){

            return PullTaskRedisDataService::insertTripartiteOrderData($type,$channel,$beginTime);

        }else{

            return PullTaskRedisDataService::insertHotelOrderData(['channel_id'=>$param['channel'],'startTime'=>$beginTime,'endTime'=>$endTime]);
        }
    }



}