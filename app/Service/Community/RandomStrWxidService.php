<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/6/23
 * Time: 14:52
 */

namespace App\Service\Community;

use App\Model\Community\PullTaskRedisModel;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
class RandomStrWxidService
{
    public  static function redisRandomWxidHandle()
    {
        Log::info( "Worker is started 2");
        $workerNum =2; //设定开启进程数
        $pool = new \Swoole\Process\Pool($workerNum);
        $pool->on("WorkerStart", function ($pool, $workerId) {
//            Log::info( "Worker#{$workerId} is started\n");
//            $redisKeyRandom =  config('community.insert_to_redis_random');
            $redisKeyRandom =  config('community.insert_to_redis_random');
            // 从redis队列里弹出一条数据
            $count = Redis::llen($redisKeyRandom);

            $falg = $count > 0 ? true : false;
            while ($falg) {
//                $count1 =count(Redis::lrange($redisKeyRandom, 0, -1));
//                Log::info( "Worker# WorkerStopinsert_to_redis_to_room  is wxid" .$redisKeyRandom.$count.$count1);
                $para = Redis::lpop($redisKeyRandom);
                // 处理这个请求
                if(isset($para) && !empty($para)){
                    Log::info( "Worker# WorkerStopinsert_to_redis_to_room  is paradata " .json_encode($para));
                    $para = json_decode($para, true);
                    self::SetRandomRoom($para);
                    unset($para);
                }
            }
            // 队列没有任务等待5秒
            sleep(5);
        });
        $pool->on("WorkerStop", function ($pool, $workerId) {
            Log::info( "Worker#{$workerId} is stopped\n");
        });
        $pool->start();
    }


    /**
     *   根据素材标识  把素材  和  机器人关系存redis
     *   ['mid' => $mid, 'random_str' => $str, 'tag_ids' => !empty($value['tag_ids']) ? $value['tag_ids'] : 0,'randomWxids'=>$wxAliasString]
     */
  public static  function  SetRandomRoom($randomWxidData){
      Log::info( "SetRandomRoom  is to_room".json_encode($randomWxidData) );
      if(!empty($randomWxidData['randomWxids'])){
           $random_str = $randomWxidData['random_str'];
           $tagIds     = $randomWxidData['tag_ids'];
          foreach($randomWxidData['randomWxids'] as $v){
              $sendMsgCount  =  PullTaskRedisModel::getPullToRoomMsgCount($random_str);//查看素材可以发送的数量
              if(!empty($sendMsgCount)){
                   $sendToRoom  =  PullTaskRedisModel::GetRoomByTagIdWxid($v,$tagIds);//查看素材可以发送的群
                   $redisToRoomKey = config('community.insert_to_redis_to_room');
                   $sendToRoomData =   json_decode(json_encode($sendToRoom),true);
//                   Log::info( "Worker# WorkerStopinsert_to_redis_to_room  is to_room".$sendMsgCount .json_encode(['random_str'=>$random_str,'wxid'=> $v,'room_count'=>count($sendToRoomData)   ,'roomData'=>$sendToRoomData]));
                   $id =Redis::rpush($redisToRoomKey, json_encode(['random_str'=>$random_str,'wxid'=> $v,'room_count'=>count($sendToRoomData)   ,'roomData'=>$sendToRoomData]));
                   Log::info( "Worker# WorkerStopinsert_to_redis_to_room  is to_room".json_encode(['random_str'=>$random_str,'redis'=>$id,'wxid'=> $v,'room_count'=>count($sendToRoomData)   ,'roomData'=>$sendToRoomData]));

              }
          }
      }
  }
  /**
    * 将要发送的素材存redis
  */
  public static function GetSendDataRoomToRedis(){
      Log::info( "Worker is started 2222");
      $workerNum = 2; //设定开启进程数
      $pool = new \Swoole\Process\Pool($workerNum);
      $pool->on("WorkerStart", function ($pool, $workerId) {
          $redisToRoomKey = config('community.insert_to_redis_to_room');
          // 从redis队列里弹出一条数据
          $count = Redis::llen($redisToRoomKey);
//          Log::info( "GetSendDataRoomToRedis  insert_to_redis_to_room  is wxid" .$redisToRoomKey.$count);
          $falg = $count > 0 ? true : false;
          while ($falg) {
              $para = Redis::lpop($redisToRoomKey);
              $count1 =count(Redis::lrange($redisToRoomKey, 0, -1));
//              Log::info( "GetSendDataRoomToRedis insert_to_redis_to_room  is wxid" .$redisToRoomKey.$count.$count1);
              // 处理这个请求
              if(isset($para) && !empty($para)){
                  $para = json_decode($para, true);
                  self::getDataToRoomRedis($para);
                  unset($para);
              }
          }
          // 队列没有任务等待5秒
          sleep(5);
      });
      $pool->on("WorkerStop", function ($pool, $workerId) {
          Log::info( "Worker# WorkerStop  GetSendDataRoomToRedis  {$workerId} is stopped\n");
      });
      $pool->start();
  }

    /**
     * 把素材存redis
     * @param $randomRoomData
     */
    public static function getDataToRoomRedis($randomRoomData)
    {
        $randomStr = $randomRoomData['random_str'];
        $wxid = $randomRoomData['wxid'];
        $redisNewKey = $wxid . $randomStr .config('community.redisNewKeystr');
        $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
        if ($tobeCount > 0) {
            for ($i = 1; $i <= $tobeCount; $i++) {
                Redis::lpop($redisNewKey);
            }
            PullTaskRedisModel::getDeleteReload($randomStr,$wxid);
        }
         if(!empty($randomRoomData['roomData'])){
             foreach ($randomRoomData['roomData'] as $room_data) {
                 $codeNumber = empty($room_data['code_number']) ? config('community.codeNumber') : $room_data['code_number'];
                 $tagIds = isset($room_data['tag_id'])?$room_data['tag_id']:'';
                 $pullData = PullTaskRedisModel::getPullToRoomData($randomRoomData['random_str'],$tagIds);
                 $msg_list = PullTaskRedisModel::getSendData($pullData,$codeNumber,$room_data['id'],$room_data['mid']);
                 if (!empty($msg_list)) {
                     $rpush = ['room_wxid' => $room_data['room_wxid'], 'room_id' => $room_data['id'], 'msg_list' => $msg_list];
                     if (!empty($rpush)) {
                         $is = Redis::rpush($redisNewKey, json_encode($rpush));
                         Log::info( 'tag_id '.$is .'save  data  to  redis tag_id getDataToRoomRedis'.$randomStr.$wxid.json_encode($rpush));
                     }
                 }
             }
             $res  = PullTaskRedisModel::updateSendInfoByRandomStr($randomStr);
             Log::info( 'tag_idave  data  to  redis tag_id'.$randomStr.$wxid.json_encode($res));
         }
    }

}