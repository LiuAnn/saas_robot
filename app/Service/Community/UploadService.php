<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/27
 * Time: 18:20
 */

namespace App\Service\Community;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Service\Common\UcloudServiceImpl;
use  App\Model\Community\CommunityModel;
class uploadService
{
    /**
     * 文件上传
     * @param $body
     * @throws \Exception
     */
    public static function uploadData($body,$fileData='')
    {
        Log::info('uploaduserBehavior0330log' . json_encode($body).json_encode($fileData));
        $time = date("Y-m-d H:i", time());
        $file = empty($body['file']) ? '' : $body['file'];
        $path_info = CommunityModel::getOrderListByMemberId($body['file_index']);
        if (!empty($path_info)) {
            $res = (new UcloudServiceImpl())->uploadImg($file);
            if ($res['code'] == 200) {
                CommunityModel::updateOrder(['file_index' => $body['file_index'], 'path_info' => $res['url']]);
            }
            Log::info('uploaduserBehavior032714log' . $time . json_encode($body) . json_encode($res));
            self::return_success_result($body['file_index']);
        }else{
            self::return_error_result();
        }
    }
    /**
     * 错误接口返回
     * @param array $params
     */
    public static function return_error_result($params = ['error_reason' => '未知错误'])
    {
        $base_result = [
            'error_code' => 1,
            'error_reason' => $params['error_reason']
        ];
        echo json_encode(array_merge($base_result, $params));
        exit;
    }
    /**
     * 成功正确接口返回
     * @param array $params
     */
    public static function return_success_result($file_index)
    {
        $base_result = [
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'upload_file_ack',
            'file_index' => $file_index
        ];
        Log::info('useruploadBehaviorSuccess' . date('Y-m-d H:s:t', time()) . json_encode(array_merge($base_result)));
        echo json_encode($base_result);
        exit;
    }

}