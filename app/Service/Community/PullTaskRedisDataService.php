<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/5/13
 * Time: 20:07
 */

namespace App\Service\Community;

use App\Model\Community\AutoGroupCreateModel;
use App\Model\Community\PullTaskRedisModel;
use App\Model\Community\GroupManageModel;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
use App\Service\Community\ExclusiveMangerService;
use App\Model\Source\SendInfoModel;
use App\Service\Community\UseIndexService;
use App\Service\Member\MemberService;

class PullTaskRedisDataService
{

    static $jdDomain = 'https://gateway.yueshang.shop/private/v1/index.php/general/third/order/cps/list';
    static $pddDomain = 'https://gateway.yueshang.shop/private/v1/pdd/v1/getOrderList';
    static $drDomain = 'https://gateway.daren.tech/open/v1/app/tour/getGoodsList';
    static $YtOrderAmount = 'https://gateway.yuetao.vip/center/v1/app/getStationOrderAmount';
    static $DrOrderAmount = 'https://mallapi.daren.tech/mall/getAllBalanceRecord';
    static $ZDorderUrl = 'http://zhidingapi.zhiding365.com/goods/robot/getOrderList';
    static $LocalLifeUrl = 'http://location.yuetao.vip/general/output/order';
    static $MovieUrl = 'https://gateway.yueshang.shop/private/v1/cinema/qianzhu/pagedCommunityQuerys';

    /**
     * 获取没有存redis 的消息 并且 存redis
     */
    public static function getNoRedisSendInfoSave()
    {
        PullTaskRedisModel::getNoRedisSendInfo();
    }

    /**
     * 数据提交
     */
    public static function WhiteListDataTask()
    {

        PullTaskRedisModel::WhiteListDataTask();
    }

    /**
     * 更新
     */
    public static function CommonGroupNumRobotChange()
    {

        PullTaskRedisModel::CommonGroupNumRobotChange();
    }

    /**
     * 获取悦淘自营订单
     * @Author yangjianhua
     * @Date 2020-05-27
     */
    public static function insertYtOrderData()
    {
        $communityOrderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => 1]);
        $recordId = !empty($communityOrderInfo) ? $communityOrderInfo['record_id'] : 0;

        $orderCount = PullTaskRedisModel::getYtOrderTotal($recordId);
        if (empty($orderCount)) {
            echo '没有悦淘订单数据可插入';
            exit;
        }
        $pageSize = 20;
        $pageNum = ceil($orderCount / $pageSize) + 1;
        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getYtOrderLists($i, $pageSize, $recordId, 1);
            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;
            foreach ($result['orderList'] as $k => $v) {
                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => 1, 'ordersn' => $v['ordersn'],'record_id'=>$v['record_id']]);
                if (!empty($orderInfo)) {
                    PullTaskRedisModel:: updateOrderData($v['ordersn'], ['goods_id' => $v['goods_id']]);
                    continue;
                }
                $live_data = PullTaskRedisModel::getYtZhiboOrderId($v['ordersn']);
                $live_id = !empty($live_data) ? $live_data['live_people_id'] : 0;
                $order_source = !empty($live_data) ? $live_data['order_source'] : 0;

                $memInfo = GroupManageModel::getUserParentIdByMidNew($v['member_id']);
                $data = [];
                $data['ext1'] = '';
                $data['live_id'] = $live_id;
                $data['order_source'] = $order_source;
                $data['record_id'] = $v['record_id'];
                $data['member_id'] = $v['member_id'];

                $data['parent_id'] = !empty($memInfo) ? $memInfo['parent_id'] : 0;
                $data['buyer_phone'] = !empty($memInfo) ? $memInfo['mobile'] : '';
                $data['buyer_nickname'] = !empty($memInfo) ? $memInfo['nickname'] : '';

                $data['ordersn_son'] = $v['ordersn_son'];
                $data['ordersn'] = $v['ordersn'];
                $data['ordersn_jd'] = $v['ordersn_jd'];
                $data['ordersn_son_jd'] = $v['ordersn_son_jd'];
                $data['goods_id'] = $v['goods_id'];
                $data['goods_name'] = $v['goods_name'];
                $data['goods_num'] = $v['goods_num'];
                $data['goods_price_deduc'] = $v['goods_price_deduc'];
                $data['goods_price_member'] = $v['goods_price_member'] * 100;
                $data['goods_type'] = $v['goods_type'];
                $data['goods_price_buy'] = $v['goods_price_buy'] * 100;
                $data['goods_price_orig'] = $v['goods_price_orig'] * 100;
                $data['goods_freight'] = $v['goods_freight'];
                $data['goods_total'] = $v['goods_total'] * 100;
                $data['goods_cover_image'] = $v['goods_cover_image'];
                $data['goods_category'] = $v['goods_category'];
                $data['is_send'] = $v['is_send'];
                $data['pay_status'] = $v['pay_status'];
                $data['pay_type'] = $v['pay_type'];
                $data['pay_form_no'] = $v['pay_form_no'];
                $data['pay_time'] = $v['pay_time'];
                $data['pay_info'] = $v['pay_info'];
                $data['goods_is_flash'] = $v['goods_is_flash'];
                $data['order_status'] = $v['order_status'];
                $data['cid'] = $v['cid'];
                $data['cancel_type'] = $v['cancel_type'];
                $data['cancel_reason'] = $v['cancel_reason'];
                $data['cancel_time'] = $v['cancel_time'];
                $data['share_mid'] = $v['share_mid'];
                $data['actual_price'] = $v['actual_price'];
                $data['created_at'] = $v['created_at'];
                $data['updated_at'] = $v['updated_at'];
                $data['deleted_at'] = $v['deleted_at'];
                $data['receiving_time'] = $v['receiving_time'];
                $data['order_three_partner'] = $v['order_three_partner'];
                $data['order_partner_skuId'] = $v['order_partner_skuId'];
                $data['goods_spec'] = $v['goods_spec'];
                $data['goods_order_push'] = $v['goods_order_push'];
                $data['goods_sku_order'] = $v['goods_sku_order'];
                $data['refund_status'] = $v['refund_status'];
                $data['push_count'] = $v['push_count'];
                $data['update_skuOrder_end'] = $v['update_skuOrder_end'];
                $data['goods_classify'] = $v['goods_classify'];
                $data['rewards'] = $v['rewards'];
                $data['order_actual_price'] = $v['order_actual_price'];
                $data['leader_id'] = $v['leader_id'];
                $data['leader_push'] = $v['leader_push'];
                $data['share_id'] = $v['share_id'];
                $data['goods_coupon_average'] = $v['goods_coupon_average'];
                $data['goods_balance_average'] = $v['goods_balance_average'];
                $data['is_use_coupon'] = $v['is_use_coupon'];
                $data['sku_id'] = $v['sku_id'];
                $data['order_red_money'] = $v['order_red_money'];
                $data['remark'] = $v['remark'];
                $data['version'] = $v['version'];
                $data['order_type'] = $v['order_type'];
                $data['is_remind'] = $v['is_remind'];
                $data['spl_id'] = $v['spl_id'];
                $data['activity_id'] = $v['activity_id'];
                $data['draw_id'] = $v['draw_id'];
                $data['is_member_goods'] = $v['is_member_goods'];
                $data['member_level'] = $v['member_level'];
                $data['is_comment'] = $v['is_comment'];
                $data['is_send_goods'] = $v['is_send_goods'];
                $data['kl_order_status'] = $v['kl_order_status'];
                $data['middle_order_code'] = $v['middle_order_code'];
                $data['middle_order_son_code'] = $v['middle_order_son_code'];
                $data['middle_product_id'] = $v['middle_product_id'];
                $data['middle_product_sku_id'] = $v['middle_product_sku_id'];
                $data['hotel_code'] = $v['hotel_code'];
                $data['price_type'] = $v['price_type'];
                $data['is_change'] = $v['is_change'];
                $data['is_export'] = $v['is_export'];
                $data['refunds'] = $v['refunds'];
                $data['penalty'] = $v['penalty'];
                $data['start_refund_time'] = $v['start_refund_time'];
                $data['end_refund_time'] = $v['end_refund_time'];
                $data['refund_form_no'] = $v['refund_form_no'];
                $data['refund_data'] = $v['refund_data'];
                $data['online_transaction_no'] = $v['online_transaction_no'];
                $data['comfire_refund_address_time'] = $v['comfire_refund_address_time'];
                $data['refund_amount'] = $v['refund_amount'];
                $data['room_id'] = $v['room_id'];
                //是否为机器人发送 日期:09-24
                $data['robot_send'] = isset($v['robot_send']) ? $v['robot_send'] : 0;
                $data['growth_value'] = $v['growth_value'];
                $data['order_sign'] = $v['order_sign'];
                $data['good_type'] = 1;
                $data['order_from'] = $v['share_from'] == 1 ? 2 : 1;
                $data['insert_time'] = time();
                $data['c_commission_role'] = $v['c_commission_role'];
                $data['c_commission_money'] = $v['c_commission_money'];
                $data['c_platform_subsidy'] = $v['c_platform_subsidy'];

                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $data);
                if (!$inserOrderId) {
                    $desc = 0;
                } else {

                    MemberService::userBonusBecommitted(['ordersn'=>$v['ordersn'],'order_status'=>1]);
                    
                }
            }
            $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);

        }
    }

    /**
     * 获取悦淘订单列表
     * @Author yangjianhua
     * @Date 2020-05-27
     */
    private static function _getYtOrderLists($page = 1, $pageSize = 20, $recordId = 0, $synMethod = 1)
    {

        $offset = ($page - 1) * $pageSize;
        $orderYtOrderList = PullTaskRedisModel::getYtOrderList($recordId, $offset, $pageSize);

        $orderList = json_decode($orderYtOrderList, true);

        $insertLogId = self::_insertOrderLog(['page' => $page, 'pageSize' => $pageSize, 'recordId' => $recordId], 1, $orderList, $synMethod);

        $data = [];
        $data['orderList'] = $orderList;
        $data['insertLogId'] = $insertLogId;
        return $data;
    }

    /**
     * 获取京东/拼多多订单
     * @param int $type 订单类型 0:拼多多 1:京东 2:唯品会 3:苏宁 4:饿了么果蔬 5:美团 6:亚马逊 7:淘宝 8 9:海威淘券 10:大人订单 11:叮当快药 12:饿了么外卖 13:信用卡 14:贷超 15:曹操出行H5
     * @param int $channelId 平台ID
     * @param string $date 自定义拉取订单的开始时间（最好不要超过当前时间7天，格式：2020-10-13）
     */
    public static function insertTripartiteOrderData($type = 1, $channelId = 0, $date = '')
    {
        $total = 400;
        $pageSize = 20;
        $pageNum = ceil($total / $pageSize) + 1;

        $filter = 1;
        //订单类型  看清楚再写    看清楚再写   看清楚再写
//        $communityOrderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => $type]);
//        $recordId = !empty($communityOrderInfo) ? $communityOrderInfo['record_id'] : 0;
        $recordId = 0;
        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getTripartiteOrderLists($i, $pageSize, $channelId, $recordId, $filter, 1, $type, $date);

            //改版前   orderList
            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;

            $channel = self::getOrderChannelByChannelId($channelId);

            foreach ($result['orderList'] as $k => $v) {


                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $channel, 'ordersn' => $v['order_sn'],'record_id'=>$v['id']]);
                if ($v['room_id'] < 1) {
                    continue;
                }


                if (empty($v['mid']) || !empty($orderInfo)||(!empty($orderInfo)&&$orderInfo['buyer_phone']==0)) {
                    if($channel==4){

                        $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['mid']]);
                        $memInfo = [];
                        $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                        $memInfo['buyer_phone'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                        $memInfo['buyer_nickname'] = isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                        PullTaskRedisModel:: updateOrderData($v['order_sn'],$memInfo);
                    }else{
                        PullTaskRedisModel:: updateOrderData($v['order_sn'], ['goods_id' => $v['goods_id']]);
                    }
                    
                    continue;
                }

                switch ($channel) {
                    case 4:

                        $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['mid']]);

                        $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                        $memInfo['mobile'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                        $memInfo['nickname'] = isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                        break;

                    case 3:

                        $mt_memInfo = UseIndexService::getMaituMidData(['number' => $v['mid']]);

                        $memInfo['parent_id'] = 0;
                        $memInfo['mobile'] = $mt_memInfo['mobile'];
                        $memInfo['nickname'] = $mt_memInfo['nickName'];

                        break;

                    default:

                        $memInfo = GroupManageModel::getUserParentIdByMidNew($v['mid']);

                        break;
                }

                $insertData = [];
                $insertData['live_id'] = $v['live_id'] ?? 0;
                $insertData['ext1'] = $v['custom_parameters'];
                $insertData['record_id'] = $v['id'];
                $insertData['ordersn'] = $v['order_sn'];
                $insertData['member_id'] = $v['mid'];
                $insertData['parent_id'] = !empty($memInfo) ? $memInfo['parent_id'] : 0;
                $insertData['buyer_phone'] = !empty($memInfo) ? $memInfo['mobile'] : '';
                $insertData['buyer_nickname'] = !empty($memInfo) ? $memInfo['nickname'] : '';
                $insertData['goods_id'] = $v['goods_id'];
                $insertData['goods_name'] = $v['goods_name'];
                $insertData['goods_price_orig'] = $v['goods_price'];
                $insertData['goods_num'] = $v['goods_quantity'];
                $insertData['goods_cover_image'] = $v['goods_thumbnail_url'];
                $insertData['actual_price'] = empty($v['order_amount']) ? $insertData['goods_price_orig'] * $v['goods_quantity'] : $v['order_amount'];
                $insertData['updated_at'] = empty($v['third_modify_time']) ? 0 : $v['third_modify_time'];
                $insertData['pay_time'] = $v['third_create_time'];
                $insertData['created_at'] = $v['third_create_time'];
                $insertData['room_id'] = $v['room_id'];
                $insertData['robot_send'] = isset($v['tmp_key']) && $v['tmp_key'] == 11 ? 1 : 0;

                if(!empty($v['tmp_key'])){

                    $str = explode("_", $v['tmp_key']);

                    if(isset($str[0])&&$str[0]=='s'){

                        $insertData['share_mid'] = isset($str[1])?$str[1]:0;
                    }
                }

                $insertData['pay_status'] = 1;
                $insertData['channel'] = $channel;
                $insertData['good_type'] = $type;
                $insertData['is_syn'] = empty($v['order_amount']) || empty($v['order_pay_time']) ? '1' : '0';
                $insertData['order_from'] = 1;
                $insertData['insert_time'] = time();
                $insertData['commission'] = $v['promotion_amount'];
                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                if (!$inserOrderId) {
                    $desc = 0;
                } else {
                    if ($channel == 1) {
                      MemberService::userBonusBecommitted(['ordersn'=>$v['order_sn'],'order_status'=>1]);
                    }
                }
                $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);
            }

        }


    }


    /**
     * 获取京东/拼多多订单
     * @param int $type 订单类型 0:拼多多 1:京东 2:唯品会 3:苏宁 4:饿了么果蔬 5:美团 6:亚马逊 7:淘宝 8 9:海威淘券 10:大人订单 11:叮当快药 12:饿了么外卖 13:信用卡 14:贷超 15:曹操出行H5
     * @param int $channelId 平台ID
     */
    public static function insertTripartiteOrderDataProfit($type = 1, $channelId = 0)
    {
        $total = 400;
        $pageSize = 20;
        $pageNum = ceil($total / $pageSize) + 1;

        $filter = 1;
        //订单类型  看清楚再写    看清楚再写   看清楚再写
//        $communityOrderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => $type]);
//        $recordId = !empty($communityOrderInfo) ? $communityOrderInfo['record_id'] : 0;
        $recordId = 0;
        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getTripartiteOrderListsV2($i, $pageSize, $channelId, $recordId, $filter, 1, $type);

            //改版前   orderList
            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;

            $channel = self::getOrderChannelByChannelId($channelId);

            foreach ($result['orderList'] as $k => $v) {


                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $channel, 'ordersn' => $v['order_sn']]);

                if (!empty($orderInfo)) {

                    switch ($v['order_status']) {
                        case 1:
                            MemberService::userBonusBecommitted(['ordersn' => $v['order_sn'], 'order_status' => 2]);

                            break;
                        case 5:
                            MemberService::userBonusBecommitted(['ordersn' => $v['order_sn'], 'order_status' => 3]);
                            break;

                        default:
                            # code...
                            break;
                    }


                }
            }

        }


    }


    /**
     * 返回三方订单列表
     * @Author yangjianhua
     * @Date 2020-05-27
     * @param  int type 2 京东 3拼多多
     */
    private static function _getTripartiteOrderListsV2($page = 1, $pageSize = 20, $channelId = 0, $recordId = 0, $filter = 1, $synMethod = 1, $type = 2)
    {

        // 业务参数
        $params = [];
        $params['page'] = $page;
        $params['pageSize'] = $pageSize;
        $params['channelId'] = $channelId;
        $params['recordId'] = $recordId;

        if ($synMethod == 1) {

            $params['orderStatus'][0] = 1;

            $params['orderStatus'][1] = 5;

            $params['thirdCreateTimeStart'] = strtotime("2020-10-15 00:00:00");

            $params['updateTimeStart'] = strtotime('-3 hour');

            $params['updateTimeEnd'] = time();

        }

        $params['orderSource'] = self::getOrderSourceByType($type);
        $params['filter'] = 1;

        $result = curlPost(self::$jdDomain, $params);

        if (false == $result) {
            \Log::error("接口请求异常");
            return false;
        }
        $result = json_decode($result, true);

        $insertLogId = self::_insertOrderLog($params, $type, $result['data'], $synMethod);


        $data = [];
        $data['orderList'] = $result['data'];
        $data['insertLogId'] = $insertLogId;

        return $data;
    }


    /**
     * 返回三方订单列表
     *
     * @param int $page
     * @param int $pageSize
     * @param int $channelId
     * @param int $recordId
     * @param int $filter
     * @param int $synMethod
     * @param int $type 1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9品牌折扣
     * @param string $date
     * @return array|bool
     */
    private static function _getTripartiteOrderLists($page = 1, $pageSize = 20, $channelId = 0, $recordId = 0, $filter = 1, $synMethod = 1, $type = 2, $date = '')
    {

        // 业务参数
        $params = [];
        $params['page'] = $page;
        $params['pageSize'] = $pageSize;
        $params['channelId'] = $channelId;
        $params['recordId'] = $recordId;
        $params['filter'] = $filter;


        if ($synMethod == 1) {

            $params['orderStatus'][0] = 2;

            $params['thirdCreateTimeStart'] = strtotime('-2 hour');

            if ($type == 5) {

                $params['orderStatus'][1] = 5;
                $params['thirdCreateTimeStart'] = strtotime('-2 days');
            }

            $params['thirdCreateTimeEnd'] = time();
        } else if ($synMethod == 2) {
            $params['orderStatus'][] = 2;
            $params['thirdCreateTimeStart'] = strtotime('-2 days');
            $params['thirdCreateTimeEnd'] = time();
        } else if ($synMethod == 3) {
            $params['thirdCreateTimeStart'] = strtotime('-1 month');
            $params['thirdCreateTimeEnd'] = time();
        } else {
            $params['updateTimeStart'] = strtotime('-7 days');
            $params['updateTimeEnd'] = time();
        }

        if (!empty($date)) {
            $params['thirdCreateTimeStart'] = strtotime($date);
        }

        if($channelId==1){
            $params['thirdCreateTimeStart'] = strtotime('-4 month');
        }

        $params['orderSource'] = self::getOrderSourceByType($type);
        $params['filter'] = 1;

        $result = curlPost(self::$jdDomain, $params);

        if (false == $result) {
            \Log::error("接口请求异常");
            return false;
        }
        $result = json_decode($result, true);

        $insertLogId = self::_insertOrderLog($params, $type, $result['data'], $synMethod);


        $data = [];
        $data['orderList'] = $result['data'];
        $data['insertLogId'] = $insertLogId;

        return $data;
    }

    /**
     * 机器人订单类型转中台订单类型
     * @param $type
     */
    public static function getOrderSourceByType($type)
    {
        //社群商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9品牌折扣
        $getOrderSourceData = [2 => 1, 3 => 0, 4 => 2, 5 => 5, 8 => 3, 9 => 9, 6 => 7];
        if (isset($getOrderSourceData[$type])) {
            return $getOrderSourceData[$type];
        } else {
            return 0;
        }
    }


    /**
     * 机器人订单类型转中台订单类型
     * @param $type
     */
    public static function getOrderChannelByChannelId($type)
    {
        //社群商品类型  1悦淘 2大人 3迈图 4直订
        $getOrderSourceData = [0 => 1,1=>2,3 => 3,9 => 4];
        if (isset($getOrderSourceData[$type])) {
            return $getOrderSourceData[$type];
        } else {
            return 0;
        }
    }


    /**
     * 同步订单日志
     * @Author yangjianhua
     * @Date 2020-05-27
     * @param int type 1 悦淘 2 京东 3 拼多多
     */
    private static function _insertOrderLog($param = [], $type = 1, $dataList = [], $synMethod = 1, $channel = 1)
    {
        $insertOrderLog = [];
        $insertOrderLog['param'] = json_encode($param);
        $insertOrderLog['channel'] = $channel;
        $insertOrderLog['type'] = $type;
        $insertOrderLog['data'] = json_encode($dataList);
        $insertOrderLog['desc'] = !empty($dataList) ? '' : '没有要同步的订单';
        $insertOrderLog['syn_method'] = $synMethod;
        $insertOrderLog['create_time'] = time();
        $insertLogId = PullTaskRedisModel::insertOrderData('order_log', $insertOrderLog);

        return $insertLogId;
    }

    /*
     * 悦淘补单
     * @Author yangjianhua
     *
     */
    public static function supplementYtOrderData()
    {
        $startTime = strtotime(date("Y-m-d", strtotime("-1 day")));
        $endTime = time();
        $communityOrderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => 1, ['insert_time', '<', $startTime]]);
        $recordId = !empty($communityOrderInfo) ? $communityOrderInfo['record_id'] : 0;

        $orderCount = PullTaskRedisModel::getYtOrderTotal($recordId);

        if (empty($orderCount)) {
            echo '没有悦淘订单数据可插入';
            exit;
        }
        $pageSize = 40;
        $pageNum = ceil($orderCount / $pageSize) + 1;
        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getYtOrderLists($i, $pageSize, $recordId, 2);
            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;
            foreach ($result['orderList'] as $k => $v) {
                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => 1, 'ordersn' => $v['ordersn'],'record_id'=>$v['record_id']]);
                if (!empty($orderInfo)) {
                    PullTaskRedisModel:: updateOrderData($v['ordersn'], ['goods_id' => $v['goods_id']]);
                    continue;
                }
                $live_data = PullTaskRedisModel::getYtZhiboOrderId($v['ordersn']);
                $live_id = !empty($live_data) ? $live_data['live_people_id'] : 0;
                $order_source = !empty($live_data) ? $live_data['order_source'] : 0;
                $data = [];
                $memInfo = GroupManageModel::getUserParentIdByMidNew($v['member_id']);

                $data['ext1'] = '';
                $data['live_id'] = $live_id;
                $data['order_source'] = $order_source;
                $data['record_id'] = $v['record_id'];
                $data['member_id'] = $v['member_id'];
                $data['parent_id'] = !empty($memInfo) ? $memInfo['parent_id'] : 0;
                $data['buyer_phone'] = !empty($memInfo) ? $memInfo['mobile'] : '';
                $data['buyer_nickname'] = !empty($memInfo) ? $memInfo['nickname'] : '';

                $data['ordersn_son'] = $v['ordersn_son'];
                $data['ordersn'] = $v['ordersn'];
                $data['ordersn_jd'] = $v['ordersn_jd'];
                $data['ordersn_son_jd'] = $v['ordersn_son_jd'];
                $data['goods_id'] = $v['goods_id'];
                $data['goods_name'] = $v['goods_name'];
                $data['goods_num'] = $v['goods_num'];
                $data['goods_price_deduc'] = $v['goods_price_deduc'];
                $data['goods_price_member'] = $v['goods_price_member'];
                $data['goods_type'] = $v['goods_type'];
                $data['goods_price_buy'] = $v['goods_price_buy'];
                $data['goods_price_orig'] = $v['goods_price_orig'];
                $data['goods_freight'] = $v['goods_freight'];
                $data['goods_total'] = $v['goods_total'];
                $data['goods_cover_image'] = $v['goods_cover_image'];
                $data['goods_category'] = $v['goods_category'];
                $data['is_send'] = $v['is_send'];
                $data['pay_status'] = $v['pay_status'];
                $data['pay_type'] = $v['pay_type'];
                $data['pay_form_no'] = $v['pay_form_no'];
                $data['pay_time'] = $v['pay_time'];
                $data['pay_info'] = $v['pay_info'];
                $data['goods_is_flash'] = $v['goods_is_flash'];
                $data['order_status'] = $v['order_status'];
                $data['cid'] = $v['cid'];
                $data['cancel_type'] = $v['cancel_type'];
                $data['cancel_reason'] = $v['cancel_reason'];
                $data['cancel_time'] = $v['cancel_time'];
                $data['share_mid'] = $v['share_mid'];
                $data['actual_price'] = $v['actual_price'];
                $data['created_at'] = $v['created_at'];
                $data['updated_at'] = $v['updated_at'];
                $data['deleted_at'] = $v['deleted_at'];
                $data['receiving_time'] = $v['receiving_time'];
                $data['order_three_partner'] = $v['order_three_partner'];
                $data['order_partner_skuId'] = $v['order_partner_skuId'];
                $data['goods_spec'] = $v['goods_spec'];
                $data['goods_order_push'] = $v['goods_order_push'];
                $data['goods_sku_order'] = $v['goods_sku_order'];
                $data['refund_status'] = $v['refund_status'];
                $data['push_count'] = $v['push_count'];
                $data['update_skuOrder_end'] = $v['update_skuOrder_end'];
                $data['goods_classify'] = $v['goods_classify'];
                $data['rewards'] = $v['rewards'];
                $data['order_actual_price'] = $v['order_actual_price'];
                $data['leader_id'] = $v['leader_id'];
                $data['leader_push'] = $v['leader_push'];
                $data['share_id'] = $v['share_id'];
                $data['goods_coupon_average'] = $v['goods_coupon_average'];
                $data['goods_balance_average'] = $v['goods_balance_average'];
                $data['is_use_coupon'] = $v['is_use_coupon'];
                $data['sku_id'] = $v['sku_id'];
                $data['order_red_money'] = $v['order_red_money'];
                $data['remark'] = $v['remark'];
                $data['version'] = $v['version'];
                $data['order_type'] = $v['order_type'];
                $data['is_remind'] = $v['is_remind'];
                $data['spl_id'] = $v['spl_id'];
                $data['activity_id'] = $v['activity_id'];
                $data['draw_id'] = $v['draw_id'];
                $data['is_member_goods'] = $v['is_member_goods'];
                $data['member_level'] = $v['member_level'];
                $data['is_comment'] = $v['is_comment'];
                $data['is_send_goods'] = $v['is_send_goods'];
                $data['kl_order_status'] = $v['kl_order_status'];
                $data['middle_order_code'] = $v['middle_order_code'];
                $data['middle_order_son_code'] = $v['middle_order_son_code'];
                $data['middle_product_id'] = $v['middle_product_id'];
                $data['middle_product_sku_id'] = $v['middle_product_sku_id'];
                $data['hotel_code'] = $v['hotel_code'];
                $data['price_type'] = $v['price_type'];
                $data['is_change'] = $v['is_change'];
                $data['is_export'] = $v['is_export'];
                $data['refunds'] = $v['refunds'];
                $data['penalty'] = $v['penalty'];
                $data['start_refund_time'] = $v['start_refund_time'];
                $data['end_refund_time'] = $v['end_refund_time'];
                $data['refund_form_no'] = $v['refund_form_no'];
                $data['refund_data'] = $v['refund_data'];
                $data['online_transaction_no'] = $v['online_transaction_no'];
                $data['comfire_refund_address_time'] = $v['comfire_refund_address_time'];
                $data['refund_amount'] = $v['refund_amount'];
                $data['room_id'] = $v['room_id'];

                $data['robot_send'] = isset($v['robot_send']) ? $v['robot_send'] : 0;
                $data['growth_value'] = $v['growth_value'];
                $data['order_sign'] = $v['order_sign'];
                $data['good_type'] = 1;
                $data['order_from'] = $v['share_from'] == 1 ? 2 : 1;
                $data['insert_time'] = time();
                $data['supplement_time'] = time();
                $data['c_commission_role'] = $v['c_commission_role'];
                $data['c_commission_money'] = $v['c_commission_money'];
                $data['c_platform_subsidy'] = $v['c_platform_subsidy'];

                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $data);
                if (!$inserOrderId) {
                    $desc = 0;
                } else {

                    MemberService::userBonusBecommitted(['ordersn'=>$v['ordersn'],'order_status'=>1]);
                    
                }
            }
            $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '补单数据有失败' : '补单数据成功']);

        }
    }

    /*
     * 京东、拼多多补单
     * @Author yangjianhua
     *
     */
    public static function supplementTripartiteOrderData($type = 2)
    {
        $total = 10000;
        $pageSize = 50;
        $pageNum = ceil($total / $pageSize) + 1;
        $channelId = 0;
        $filter = 1;

        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getTripartiteOrderLists($i, $pageSize, $channelId, 0, $filter, 2, $type);

            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;

            foreach ($result['orderList'] as $k => $v) {
                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'ordersn' => $v['order_sn'],'record_id'=>$v['id']]);

                if ($v['room_id'] < 1) {

                    continue;
                }

                if (empty($v['mid']) || !empty($orderInfo)) {
                    PullTaskRedisModel:: updateOrderData($v['order_sn'], ['goods_id' => $v['goods_id']]);
                    continue;
                }
                $memInfo = GroupManageModel::getUserParentIdByMidNew($v['mid']);
                $insertData = [];
                $insertData['live_id'] = $v['live_id'];
                $insertData['ext1'] = $v['custom_parameters'];
                $insertData['record_id'] = $v['id'];
                $insertData['ordersn'] = $v['order_sn'];
                $insertData['member_id'] = $v['mid'];
                $insertData['parent_id'] = !empty($memInfo) ? $memInfo['parent_id'] : 0;
                $insertData['buyer_phone'] = !empty($memInfo) ? $memInfo['mobile'] : '';
                $insertData['buyer_nickname'] = !empty($memInfo) ? $memInfo['nickname'] : '';
                $insertData['goods_id'] = $v['goods_id'];
                $insertData['goods_name'] = $v['goods_name'];
                $insertData['goods_price_orig'] = $v['goods_price'];
                $insertData['goods_num'] = $v['goods_quantity'];
                $insertData['goods_cover_image'] = $v['goods_thumbnail_url'];
                $insertData['actual_price'] = empty($v['order_amount']) ? $insertData['goods_price_orig'] * $v['goods_quantity'] : $v['order_amount'];
                $insertData['updated_at'] = empty($v['third_modify_time']) ? 0 : $v['third_modify_time'];
                $insertData['pay_time'] = $v['third_create_time'];
                $insertData['created_at'] = $v['third_create_time'];
                $insertData['room_id'] = $v['room_id'];
                if(!empty($v['tmp_key'])){

                    $str = explode("_", $v['tmp_key']);

                    if(isset($str[0])&&$str[0]=='s'){

                        $insertData['share_mid'] = isset($str[1])?$str[1]:0;
                    }
                }
                $insertData['pay_status'] = 1;
                $insertData['good_type'] = $type;
                $insertData['is_syn'] = empty($v['order_amount']) || empty($v['order_pay_time']) ? '1' : '0';
                $insertData['order_from'] = 1;
                $insertData['insert_time'] = time();
                $insertData['commission'] = $v['promotion_amount'];
                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                if (!$inserOrderId) {
                    $desc = 0;
                } else {
                    if ($channelId == 1) {
                      MemberService::userBonusBecommitted(['ordersn'=>$v['order_sn'],'order_status'=>1]);
                    }
                }
                $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '补单数据有失败' : '补单数据成功']);
            }

        }
    }

    /*
     * 悦淘京东、拼多多等三方订单退款订单拉取
     * @Author yangjianhua
     *
     */
    public static function insertCancelOrderData($type = 2)
    {

        $total = 10000;
        $pageSize = 100;
        $pageNum = ceil($total / $pageSize) + 1;
        $channelId = 0;
        $filter = 1;

        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);

            $result = self::_getTripartiteOrderLists($i, $pageSize, $channelId, 0, $filter, 4, $type);

            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;

            foreach ($result['orderList'] as $k => $v) {

                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'ordersn' => $v['order_sn']]);

                if ($v['room_id'] > 2 && !in_array($v['order_status'], [2, 5]) && !empty($orderInfo) && $orderInfo['order_status'] != 2) {

                    $data['order_status'] = 2;
                    $data['end_refund_time'] = strtotime($v['update_time']);

                    PullTaskRedisModel:: updateOrderData($v['order_sn'], $data);
                    $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => $v['order_sn'] . '订单退款成功']);
                }


            }

        }
    }


    /*
     * 同步京东订单定期同步价格
     * @Author yangjianhua
     *
     */
    public static function synTripartiteOrderPriceData($type = 2)
    {
        $total = 2000;
        $pageSize = 50;
        $pageNum = ceil($total / $pageSize) + 1;
        $channelId = 0;
        $filter = 1;

        $communityOrderInfo = PullTaskRedisModel::getMinRecordId(['channel' => 1, 'good_type' => $type, 'is_syn' => 1]);
        $recordId = !empty($communityOrderInfo) ? ($communityOrderInfo['record_id'] - 1) : 0;
        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getTripartiteOrderLists($i, $pageSize, $channelId, $recordId, $filter, 3, $type);

            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;
            foreach ($result['orderList'] as $k => $v) {
                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => $type, 'ordersn' => $v['order_sn'],'record_id'=>$v['id']]);
                if (empty($v['mid']) || empty($orderInfo)) {
                    continue;
                }
                if (empty($v['order_amount'])) {
                    continue;
                }
                $upDate = [];
                $upDate['actual_price'] = $v['order_amount'] * 100;
                $upDate['pay_time'] = empty($v['order_pay_time']) ? strtotime($v['order_create_time']) : strtotime($v['order_pay_time']);;
                $upDate['syn_price_time'] = time();
                $upDate['is_syn'] = 0;

                $upOrderInfo = PullTaskRedisModel::upOrderInfo($orderInfo['id'], $upDate);
                if (!$upOrderInfo) {
                    $desc = 0;
                }
                $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '同步价格失败' : '同步价格成功']);
            }

        }
    }


    /*
     * 同步悦淘三方订单佣金
     * @Author yangjianhua
     *
     */
    public static function synTripartiteOrderCommissionData($type = 2)
    {
        $total = 50000;
        $pageSize = 100;
        $pageNum = ceil($total / $pageSize) + 1;
        $channelId = 0;
        $filter = 1;

        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getTripartiteOrderLists($i, $pageSize, $channelId, 0, $filter, 3, $type);
            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;
            foreach ($result['orderList'] as $k => $v) {
                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => $type, 'ordersn' => $v['order_sn']]);
                if (empty($v['mid']) || empty($orderInfo) || !empty($orderInfo['commission'])) {
                    continue;
                }
                if (empty($v['promotion_amount'])) {
                    continue;
                }
                $upDate = [];
                $upDate['commission'] = $v['promotion_amount'];

                $upOrderInfo = PullTaskRedisModel::upOrderInfo($orderInfo['id'], $upDate);
            }

        }
    }


    /*
     * 获取迈图自营订单
     * @Author changxiaoyao
     *
     */
    public static function getMaituOrderData($param = [])
    {

        return PullTaskRedisModel::getMaituOrderData($param);

    }


    /*
     * 获取直订自营订单
     * @Author changxiaoyao
     *
     */
    public static function getZhidingOrderData($channel = 4)
    {
        $total = 200;
        $pageSize = 50;
        $pageNum = ceil($total / $pageSize) + 1;
        $channelId = 0;
        $filter = 1;

        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);

            $params['startTime'] = date("Y-m-d H:i:s", strtotime('-1 days'));
            $params['endTime'] = date("Y-m-d H:i:s", time());
            $params['pageNum'] = $i;
            $params['pageSize'] = $pageSize;
            $params['payStatus'] = 10;

            $result = curlPost(self::$ZDorderUrl, $params);

            if (false == $result) {
                \Log::error("接口请求异常");
                return false;
            }
            $result = json_decode($result, true);

            $insertLogId = self::_insertOrderLog($params, 1, $result['data']['list'], 1, $channel);

            if (empty($result['data']['list'])) {
                break;
            }
            $desc = 1;
            foreach ($result['data']['list'] as $k => $v) {

                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $channel, 'good_type' => 1, 'ordersn' => $v['order_sn']]);


                if ($v['room_id'] < 1 || empty($v['member_id']) || !empty($orderInfo)) {

                    continue;
                }

                if ($channel == 4) {

                    $parent_id = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                } else {

                    $parent_id = 0;
                }


                $insertData = [];
                $insertData['live_id'] = 0;
                $insertData['ext1'] = '';
                $insertData['record_id'] = $v['record_id'];
                $insertData['ordersn'] = $v['order_sn'];
                $insertData['member_id'] = $v['member_id'];
                $insertData['parent_id'] = $parent_id ?? 0;
                $insertData['buyer_phone'] = $v['buyer_phone'];
                $insertData['buyer_nickname'] = $v['buyer_nickname'];
                $insertData['goods_id'] = $v['goods_id'];
                $insertData['goods_name'] = $v['goods_name'];
                $insertData['goods_price_orig'] = $v['goods_price_orig'];
                $insertData['goods_num'] = $v['goods_num'];
                $insertData['goods_cover_image'] = $v['goods_cover_image'];
                $insertData['actual_price'] = $v['actual_price'];
                $insertData['updated_at'] = $v['update_at'];
                $insertData['pay_time'] = $v['created_at'];
                $insertData['created_at'] = $v['created_at'];
                $insertData['room_id'] = $v['room_id'];
                $insertData['pay_status'] = 1;
                $insertData['channel'] = $channel;
                $insertData['good_type'] = 0;
                $insertData['is_syn'] = 0;
                $insertData['order_from'] = 1;
                $insertData['insert_time'] = time();
                $insertData['commission'] = $v['commission'];
                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                if (!$inserOrderId) {
                    $desc = 0;
                }
                $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);
            }

        }

    }


    /*
     * 获取佣金列表
     * @Author changxiaoyao
     *
     */
    public static function getCommissionLists($params)
    {


        switch ($params['channelId']) {
            case 1:

                $result = curlPost(self::$YtOrderAmount, $params);

                if (false == $result) {
                    \Log::error("接口请求异常");
                    return false;
                }

                $amount_list = json_decode($result, true);

                break;
            case 2:

                $params['startTime'] = date("Y-m-d H:i:s", strtotime("-2 month"));

                $result = curlPost(self::$DrOrderAmount, $params);

                if (false == $result) {
                    \Log::error("接口请求异常");
                    return false;
                }

                $amount_list = json_decode($result, true);

                break;

            default:
                # code...
                break;
        }


        return $amount_list;
    }


    /*
     * 拉取悦淘三方实际佣金
     * @Author changxiaoyao
     *
     */
    public static function synTripartiteOrderTrueCommissionData($user_type = 1, $channelId = 1)
    {

        Log::info('synTripartiteOrderTrueCommissionData start');

        $total = 20000;
        $pageSize = 100;
        $pageNum = ceil($total / $pageSize) + 1;
        $good_type = 1;

        $params['startTime'] = date("Y-m-d H:i:s", strtotime("-7 days"));
        $params['endTime'] = date("Y-m-d H:i:s", time());
        $params['type'] = $user_type;
        $params['pageSize'] = $pageSize;
        $params['channelId'] = $channelId;

        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);

            $params['page'] = $i;

            $amount_list = self::getCommissionLists($params);

            if (!isset($amount_list['data']) || empty($amount_list['data'])) {
                break;
            }
            $desc = 1;

            foreach ($amount_list['data'] as $k => $v) {

                if ($channelId == 2) {

                    $array = [18 => 2, 20 => 1, 21 => 3];

                    $good_type = $array[$v['type']];
                    $v['fid'] = $v['fix'];
                } else {

                    $v['ordersn'] = number_format($v['ordersn'], 0, '.', '');
                }

                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $channelId, 'good_type' => $good_type, 'ordersn' => $v['ordersn']]);

                if (empty($orderInfo) || $v['room_id'] < 1) {
                    continue;
                }

                $commissionInfo = PullTaskRedisModel::getCommissionInfo(['order_sn' => $v['ordersn'], 'order_son_sn' => $v['ordersn_son'], 'amount' => $v['amount']]);

                if (!empty($commissionInfo)) {
                    continue;
                }

                $insertData = [];
                $insertData['room_id'] = $v['room_id'];
                $insertData['member_id'] = $v['mid'];
                $insertData['buyer_id'] = $v['fid'];
                $insertData['amount'] = $v['amount'];
                $insertData['order_son_sn'] = $v['ordersn_son'];
                $insertData['order_sn'] = $v['ordersn'];
                $insertData['member_id'] = $v['mid'];
                $insertData['channel'] = $params['channelId'];
                $insertData['good_type'] = $good_type;
                $insertData['created_at'] = time();

                $inserOrderId = PullTaskRedisModel::insertOrderData('new_commission', $insertData);
            }

        }

        Log::info('synTripartiteOrderTrueCommissionData end');
    }


    /*
     * 拉取三方实际佣金
     * @Author changxiaoyao
     *
     */
    public static function synTripartiteCpsOrderTrueCommissionData($type = 1)
    {

        Log::info('synTripartiteCpsOrderTrueCommissionData start');

        $total = 20000;
        $pageSize = 100;
        $pageNum = ceil($total / $pageSize) + 1;

        $params['updateTimeStart'] = strtotime("-1 month");
        $params['updateTimeEnd'] = time();
        $params['channelId'] = 0;
        $params['filter'] = 1;
        $params['orderStatus'] = 5;
        $params['centsStatus'] = 1;
        $params['pageSize'] = $pageSize;
        $params['orderSource'] = self::getOrderSourceByType($type);

        for ($i = 1; $i < $pageNum; $i++) {

            sleep(1);

            $params['page'] = $i;
            $result = curlPost(self::$jdDomain, $params);

            if (false == $result) {
                \Log::error("接口请求异常");
                return false;
            }
            $result = json_decode($result, true);

            if (!isset($result['data']) || empty($result['data'])) {
                break;
            }
            $desc = 1;

            foreach ($result['data'] as $k => $v) {


                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 1, 'good_type' => $type, 'ordersn' => $v['order_sn']]);


                if (empty($orderInfo)) {
                    continue;
                }

                $bonusInfo = PullTaskRedisModel::getCpsMemberCommissionInfo(['fid' => $v['mid'], 'orderNo' => $v['order_sn']]);

                if (empty($bonusInfo)) {

                    $bonusInfo = PullTaskRedisModel::getCpsMemberIntegralCommissionInfo(['fid' => $v['mid'], 'order_sn' => $v['order_sn']]);

                    if (!empty($bonusInfo) && isset($bonusInfo['integral'])) {

                        $bonusInfo['amount'] = $bonusInfo['type'] == 2 ? number_format($bonusInfo['integral'] * 399 / 800, 2) : number_format($bonusInfo['integral'] * 299 / 800, 2);
                    }
                }

                if (empty($bonusInfo)) {
                    continue;
                }

                $commissionInfo = PullTaskRedisModel::getCommissionInfo(['order_sn' => $v['order_sn'], 'member_id' => $bonusInfo['mid'], 'amount' => $bonusInfo['amount']]);

                if (!empty($commissionInfo)) {
                    continue;
                }


                $insertData = [];
                $insertData['room_id'] = $v['room_id'];
                $insertData['member_id'] = $bonusInfo['mid'];
                $insertData['buyer_id'] = $v['mid'];
                $insertData['amount'] = $bonusInfo['amount'];
                $insertData['order_son_sn'] = 0;
                $insertData['order_sn'] = $v['order_sn'];
                $insertData['channel'] = 1;
                $insertData['good_type'] = $type;
                $insertData['created_at'] = time();

                $inserOrderId = PullTaskRedisModel::insertOrderData('new_commission', $insertData);
            }

        }

        Log::info('synTripartiteCpsOrderTrueCommissionData end');
    }


    /**
     * 像redis里放入等待处理的群
     */
    public static function groupMemberData()
    {
        Log::info('groupMemberData start');
        //获取所有微信群id
        $wxGroupList = PullTaskRedisModel::getWxGroupList();
        if (empty($wxGroupList)) {
            Log::info('groupMemberData data empty');
            exit;
        }
        Log::info('originalData:' . json_encode($wxGroupList));
        $wxGroupArray = [];
        foreach ($wxGroupList as $k => $v) {
            $wxGroupArray[$v['wxid']][] = $v['room_wxid'];
        }
        Log::info('data' . json_encode($wxGroupArray));
        foreach ($wxGroupArray as $key => $value) {
            $listCacheKey = $key . 'RoomwxidListKey';
            $listCount = count(Redis::lrange($listCacheKey, 0, -1));
            if (empty($listCount)) {
                foreach ($value as $vv) {
                    Redis::rpush($listCacheKey, json_encode($vv)); //加入队列值
                }
            }
        }
        Log::info('groupMemberData end');
    }

    /**
     * 数据提交
     */
    public static function groupMangerData()
    {

        $time = date("Y-m-d", strtotime("-1 day")) . ' 00:00:00';
        $endTime = date("Y-m-d", strtotime("-1 day")) . ' 23:59:59';
        $startTime = strtotime($time);
        $endNewTime = strtotime($endTime);
        $roomList = PullTaskRedisModel::getRoomList($time);

        if (empty($roomList)) {
            return false;
        }
        foreach ($roomList as $v) {
            $activityStatisticsInfo = PullTaskRedisModel::getActivityStatistics($v['mid'], $v['bind_mid'], $v['wxid'], $v['room_wxid'], date("Y-m-d", strtotime("-1 day")));

            $data = [];
            $data['msg_num'] = PullTaskRedisModel::getMsgNum($v['room_wxid'], $time, $endTime);
            $data['source_num'] = PullTaskRedisModel::getSourceNum($v['id'], $time, $endTime);
            $data['link_num'] = PullTaskRedisModel::getLinkClickNum($v['id'], $time, $endTime);
            $data['order_num'] = PullTaskRedisModel::getOrderNum($v['id'], $startTime, $endNewTime);
            $data['member_num'] = PullTaskRedisModel::getMemberNum($v['id']);
            if (empty($activityStatisticsInfo)) {
                $data['mid'] = $v['mid'];
                $data['bind_mid'] = $v['bind_mid'];
                $data['room_wxid'] = $v['room_wxid'];
                $data['wxid'] = $v['wxid'];
                $data['date'] = date("Y-m-d", strtotime("-1 day"));
                $addId = PullTaskRedisModel::insertData('activity_statistics', $data);
            } else {
                $update = PullTaskRedisModel::updateInfo('activity_statistics', $activityStatisticsInfo['id'], $data);
            }
        }

    }

    /**
     * 处理新群用户数量, 不满足条件自动退群
     */
    public static function execLeaveRoomJudge()
    {
        $judgeNum = config('community.countNumV2') ?? 50;
        //单次取一百条数据, 循环十次
        for ($i = 0; $i < 10; $i++) {
            $newGroupList = PullTaskRedisModel::getNewGroupList();
            if (empty($newGroupList)) {
                break;
            }
            foreach ($newGroupList as $groupInfo) {
                if (empty($groupInfo)) {
                    continue;
                }
                $newGroupMemberCount = PullTaskRedisModel::getNewGroupMemberCount($groupInfo);
                $where = [
                    'id' => $groupInfo['id']
                ];
                if ($newGroupMemberCount >= $judgeNum) {
                    //群成员数量满足条件,更改检测条件,更新群成员数量
                    $update = [
                        'execute_status' => 2,
                        'member_count' => $newGroupMemberCount
                    ];
                } else {
                    //群邀请人数,不满足,下达退群任务
                    $update = [
                        'execute_status' => 3,
                        'member_count' => $newGroupMemberCount
                    ];
                    $leave_group_key = $groupInfo['wxid'] . AutoGroupCreateModel::LEAVE_GROUP_KEY;
                    $saveData = [];
                    $saveData['room_wxid'] = $groupInfo['room_wxid'];
                    Redis::rpush($leave_group_key, json_encode($saveData)); //加入队列值
                }
                PullTaskRedisModel::updateLeaveJudgeInfo($where, $update);
            }
        }

    }

    /**
     * 本地订单拉取佣金数据
     */
    public static function getOrderCommission()
    {
        //记录ID更新,第一次取最早一百条,第二次取101到200条
        $recordId = PullTaskRedisModel::getOrderInfoId(['is_commission' => 0]);
        //单次取一百条数据, 循环十次
        for ($i = 0; $i < 10; $i++) {
            $communityOrderList = PullTaskRedisModel::getOrderCommission($recordId);
            if (empty($communityOrderList)) {
                break;
            }
            foreach ($communityOrderList as $order) {
                $recordId = $order['id'];
                $order_sn = $order['ordersn_son'];
                if (empty($order_sn)) {
                    continue;
                }
                //待插入数据我们自己的
                $baseArray = [];
                $baseArray['room_id'] = $order['room_id'] ?? 0;
                $baseArray['from_id'] = $order['member_id'] ?? 0;
                $baseArray['order_sn'] = $order_sn;
                $baseArray['channel'] = $order['channel'] ?? 0;
                $baseArray['good_type'] = $order['good_type'] ?? 0;
                $baseArray['created_at'] = $order['created_at'] ?? 0;

                //获取非创客返佣
                $commissionData = PullTaskRedisModel::getCommissionByOrderSn($order_sn);
                if (!empty($commissionData)) {
                    foreach ($commissionData as $commissionInfo) {
                        $commissionArray = [];
                        $commissionArray['member_id'] = $commissionInfo['member_id'] ?? 0;
                        $commissionArray['amount'] = $commissionInfo['amount'] ?? 0;
                        $insertData = array_merge($baseArray, $commissionArray);
                        $insertOrderId = PullTaskRedisModel::insertOrderData('commission', $insertData);
                        if ($insertOrderId) {
                            PullTaskRedisModel::UpdateMallOrderIsCommission($order['id']);
                        }
                    }
                }
                //获取创客返佣
                $integralData = PullTaskRedisModel::getIntegralByOrderSn($order_sn);
                if (!empty($integralData)) {
                    foreach ($integralData as $integralInfo) {
                        $integralArray = [];
                        $integralArray['member_id'] = $integralInfo['member_id'] ?? 0;
                        $integralArray['integral'] = $integralInfo['amount'] ?? 0;
                        $insertData = array_merge($baseArray, $integralArray);
                        $insertOrderId = PullTaskRedisModel::insertOrderData('commission', $insertData);
                        if ($insertOrderId) {
                            PullTaskRedisModel::UpdateMallOrderIsCommission($order['id']);
                        }
                    }
                }
            }
        }
    }

    /**
     * 获取大人/悦淘/旅悦订单
     * @param int type 订单类型 1大人自营 2 京东
     */
    public static function insertDrOrderData($type = 1)
    {
        $total = 400;
        $pageSize = 50;
        $pageNum = ceil($total / $pageSize) + 1;
        $channel = 2;

        $communityOrderInfo = PullTaskRedisModel::getOrderInfo(['channel' => 2, 'good_type' => $type]);
        $recordId = !empty($communityOrderInfo) ? $communityOrderInfo['record_id'] : 0;

        // 遍历
        for ($i = 1; $i < $pageNum; $i++) {
            sleep(1);
            $result = self::_getDrOrderLists($i, $pageSize, $recordId, 1, $channel, $type);

            if (empty($result['orderList'])) {
                break;
            }
            $desc = 1;
            foreach ($result['orderList'] as $k => $v) {
                $orderInfo = PullTaskRedisModel::getOrderInfo(['good_type' => $type, 'ordersn' => $v['order_sn']]);
                if (!empty($orderInfo)) {
                    continue;
                }
                $customParameters = explode('/', $v['ext1']);

                $orderFrom = isset($customParameters[3]) && !empty($customParameters[3]) ? $customParameters[3] : 1;

                $insertData = [];
                $insertData['record_id'] = $v['id'];
                $insertData['ordersn'] = $v['order_sn'];
                $insertData['member_id'] = $v['mid'];
                $insertData['buyer_nickname'] = $v['nickname'];
                $insertData['buyer_phone'] = $v['mobile'];
                $insertData['goods_id'] = $v['goods_id'];
                $insertData['goods_name'] = $v['goods_name'];
                $insertData['goods_price_orig'] = $v['goods_price_orig'] * 100;
                $insertData['goods_num'] = empty($v['goods_num']) ? 1 : $v['goods_num'];
                $insertData['goods_price_member'] = empty($v['goods_price_member']) ? '' : $v['goods_price_member'] * 100;
                $insertData['goods_cover_image'] = $v['goods_cover_image'];
                $insertData['actual_price'] = empty($v['actual_price']) ? $insertData['goods_price_orig'] * $insertData['goods_num'] : $v['actual_price'];
                $insertData['updated_at'] = empty($v['updated_at']) ? 0 : strtotime($v['updated_at']);
                $insertData['pay_time'] = empty($v['pay_time']) ? strtotime($v['created_at']) : strtotime($v['pay_time']);
                $insertData['created_at'] = strtotime($v['created_at']);
                $insertData['room_id'] = $v['room_id'];
                $insertData['pay_status'] = 1;
                $insertData['channel'] = 2;
                $insertData['good_type'] = $type;
                $insertData['is_syn'] = 0;
                $insertData['order_from'] = ($orderFrom == 1 || $orderFrom == 2) ? $orderFrom : 1;
                $insertData['insert_time'] = time();
                $insertData['commission'] = empty($v['order_price']) ? '' : $v['order_price'];

                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                if (!$inserOrderId) {
                    $desc = 0;
                }
            }
            $upOrderLog = PullTaskRedisModel::upOrderLog($result['insertLogId'], ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);

        }
    }

    /**
     * 返回三方订单列表
     * @Author yangjianhua
     * @Date 2020-05-27
     * @param  int type 2 京东 3拼多多
     */
    private static function _getDrOrderLists($page = 1, $pageSize = 20, $recordId = 0, $synMethod = 1, $channel = 2, $orderType = 1)
    {

        // 业务参数
        $params = [];
        $params['page'] = $page;
        $params['pageSize'] = $pageSize;
        $params['recordId'] = $recordId;
        $params['order_type'] = $orderType;

        $result = curlPost(self::$drDomain, $params);
        if (false == $result) {
            \Log::error("接口请求异常");
            return false;
        }
        $result = json_decode($result, true);

        $insertLogId = self::_insertOrderLog($params, $orderType, $result['data'], $synMethod, $channel);

        $data = [];
        $data['orderList'] = $result['data'];
        $data['insertLogId'] = $insertLogId;

        return $data;
    }

    /**
     * 新群添加发送的素材
     */
    public static function addNewRoomDataToSend()
    {
        Log::info('tag_id  addgetDataToRoomRedis');
//        $newRoomDataKey = config('community.new_room_redis_key');
//        Redis::rpush($newRoomDataKey, json_encode(['wxid'=>'wxid_eu03czf8jklc22','room_wxid'=>'19587781102@chatroom'])); //加入 待发送的 群队列值
        $newRoomDataKey = config('community.new_room_redis_key');
        $listCount = count(Redis::lrange($newRoomDataKey, 0, -1));
        Log::info('tag_id  addgetDataToRoomRedis' . $listCount);
        if ($listCount > 0) {
            for ($i = 0; $i < $listCount; $i++) {
                $roomWxidData = json_decode(Redis::lpop($newRoomDataKey), true);
                Log::info('tag_id  addgetDataToRoomRedis' . $listCount . json_encode($roomWxidData));
                $wxid = $roomWxidData['wxid'];
                //获取即将发送的素材
                $randomStrData = PullTaskRedisModel::getWillToRoomMas($wxid);
                if (!empty($randomStrData)) {
                    $room_wxid = $roomWxidData['room_wxid'];//获取群的
                    $roomData = PullTaskRedisModel:: getSendToGroupInfo($room_wxid);
                    foreach ($randomStrData as $v) {
                        $pullData = PullTaskRedisModel::getPullToRoomData($v['random_str']);
                        $codeNumber = empty($roomData['code_number']) ? config('community.codeNumber') : $roomData['code_number'];
                        if (in_array($wxid, config('community.bind_mid_wxid'))) {
                            $redisNewKey = $wxid . $v['random_str'] . config('community.redisNewKeystr');
                            if (empty($pullData[0]->tag_ids)) {
                                self::addToWillRoomTagData($pullData, $codeNumber, $roomData, $room_wxid, $v['random_str'], $wxid, $redisNewKey);
                            } else {
                                if (in_array($roomData['tag_id'], explode(',', $pullData[0]->tag_ids))) {
                                    self::addToWillRoomTagData($pullData, $codeNumber, $roomData, $room_wxid, $v['random_str'], $wxid, $redisNewKey);
                                }
                            }
                        } else if (in_array($wxid, config('community.wxid_tag_data'))) {
                            $redisNewKey = $wxid . $v['random_str'] . config('community.redisKeystr');
                            if (empty($pullData[0]->tag_ids)) {
                                self::addToWillRoomTagData($pullData, $codeNumber, $roomData, $room_wxid, $v['random_str'], $wxid, $redisNewKey);
                            } else {
                                if (in_array($roomData['tag_id'], explode(',', $pullData[0]->tag_ids))) {
                                    self::addToWillRoomTagData($pullData, $codeNumber, $roomData, $room_wxid, $v['random_str'], $wxid, $redisNewKey);
                                }
                            }
                        } else {
                            $redisKey = $wxid . $v['random_str'];
                            $rpush = ['room_wxid' => $room_wxid, 'codeNumber' => $codeNumber, 'room_id' => $roomData['id'], 'qzmid' => $roomData['mid']];
                            Redis::rpush($redisKey, json_encode($rpush));
                        }
                    }
                }
            }
        }
    }

    /**
     * 添加缓存
     * @param $pullData
     * @param $codeNumber
     * @param $roomData
     * @param $room_wxid
     * @param $random_str
     * @param $wxid
     */
    public static function addToWillRoomTagData($pullData, $codeNumber, $roomData, $room_wxid, $random_str, $wxid, $redisNewKey)
    {
        $msg_list = PullTaskRedisModel::getSendData($pullData, $codeNumber, $roomData['id'], $roomData['mid']);
        if (!empty($msg_list)) {
            $rpush = ['room_wxid' => $room_wxid, 'room_id' => $roomData['id'], 'msg_list' => $msg_list];
            if (!empty($rpush)) {
                $is = Redis::rpush($redisNewKey, json_encode($rpush));
                Log::info('tag_id ' . $is . ' addgetDataToRoomRedis' . $random_str . $wxid . json_encode($rpush));
            }
        }
    }

    /**
     *移除缓存
     */
    public static function delRandomStrCache()
    {
        //查看总的条数
        $countData = SendInfoModel::getDelCount();
        if ($countData['count_num'] > 0) {
            $pageCount = ceil($countData['count_num'] / 20);
            for ($i = 1; $i <= $pageCount; $i++) {
                $res = ExclusiveMangerService::delRedisRandomStrCache(['offset' => ($i - 1) * 20, 'display' => 20]);
                Log::info('delRandomStrCache addgetDataToRoomRedis' . json_encode($res));
            }
        }
        $res = PullTaskRedisModel::updateRoomOrderNum();
        Log::info('PullTaskRedisModel::updateRoomOrderNum' . json_encode($res));
    }

    /**
     * 悦呗 定时跑出数据存悦呗数据库
     */
    public static function yuebeiDataSynch()
    {
        $workerData = PullTaskRedisModel::yueBeiHistoryAllData();
        if (!empty($workerData)) {
            foreach ($workerData as $workerInfo) {
                $res = PullTaskRedisModel::isSetMidYueBei($workerInfo['mid']);
                if (!empty($res)) {
                    PullTaskRedisModel::updateWorkerData(['mid' => $workerInfo['mid'], 'group_num' => $workerInfo['group_num'], 'sumPrice' => $workerInfo['sumPrice']]);
                }
            }
        }
    }


    /**
     * 拉取本地生活加油、视频充值、手机充值订单
     * param $channel_id 渠道标识 4 直订
     * param $good_type 10 话费充值 11 视频充值 12 加油
     */
    public static function insertLocalLifeOrderData($channel_id=4)
    {

        $params['startTime'] = date("Y-m-d H:i:s", strtotime('-4 days'));
        $params['endTime'] = date("Y-m-d H:i:s", time());
        $params['channelId'] = $channel_id;
        $params['filter'] = 1;
        $params['timeType'] = 0; //时间类型 1 更新时间 0 创建时间

        $result = curlPost(self::$LocalLifeUrl, $params);

        if (false == $result) {
            \Log::error("接口请求异常");
            return false;
        }

        $result = json_decode($result, true);


        if($result['code']==200&&$result['msg']=='success')
        {   

            //拉取加油单
            if(isset($result['data']['gasOrder'])&&!empty($result['data']['gasOrder'])){

                $insertLogId = self::_insertOrderLog($params, 12, $result['data']['gasOrder'], 1, $params['channelId']);

                foreach ($result['data']['gasOrder'] as $key => $v) {
                    
                    $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $params['channelId'], 'good_type' => 12, 'ordersn' => $v['order_sn'],'record_id'=>$v['id']]);
                
                     if ($v['member_id']==0 || !empty($orderInfo) || $v['room_id']==0) {
                        continue;
                    }

                    switch ($params['channelId']) {
                        case 4:

                            $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['member_id']]);

                            $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                            $memInfo['mobile'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                            $memInfo['nickname'] =isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                            break;

                        case 3:

                            $mt_memInfo = UseIndexService::getMaituMidData(['number' => $v['member_id']]);

                            $memInfo['parent_id'] = 0;
                            $memInfo['mobile'] = $mt_memInfo['mobile'];
                            $memInfo['nickname'] = $mt_memInfo['nickName'];

                            break;

                        default:

                            $memInfo = GroupManageModel::getUserParentIdByMidNew($v['member_id']);

                            break;
                    }


                    $insertData = [];
                    $insertData['record_id'] = $v['id'];
                    $insertData['ordersn'] = $v['order_sn'];//供应商支付回调唯一标识
                    $insertData['middle_order_code'] = $v['order_no'];
                    $insertData['member_id'] = $v['member_id'];
                    $insertData['parent_id'] = $memInfo['parent_id'];
                    $insertData['buyer_phone'] = $memInfo['mobile'];
                    $insertData['buyer_nickname'] = $memInfo['nickname'];
                    $insertData['bm_mobile'] = $v['mobile']; //充值对应手机号
                    $insertData['bm_order_sn'] = $v['channel_order_no'];
                    $insertData['goods_id'] = $v['gas_id'];
                    $insertData['goods_name'] = $v['gas_name'];
                    $insertData['spl_id'] = $v['other_gas_id'];
                    $insertData['goods_price_buy'] = $v['pay_amount'];
                    $insertData['actual_price'] = $v['sline_pay_amount'];
                    $insertData['goods_total'] = $v['amount_gun'];
                    $insertData['goods_price_orig'] = $v['price_unit'];
                    $insertData['pay_form_no'] = $v['pay_number'];
                    $insertData['updated_at'] =strtotime($v['update_time']);
                    $insertData['pay_time'] = $v['pay_time'];
                    $insertData['created_at'] =strtotime($v['create_time']);
                    $insertData['room_id'] = $v['room_id'];
                    $insertData['pay_status'] = 1;
                    $insertData['channel'] = $params['channelId'];
                    $insertData['good_type'] = 12;
                    $insertData['is_syn'] = 0;
                    $insertData['order_from'] = $v['source_id'];
                    $insertData['insert_time'] = time();
                    $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                    if (!$inserOrderId) {
                        $desc = 0;
                    }

                    $upOrderLog = PullTaskRedisModel::upOrderLog($insertLogId, ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);

                }

            }

            //拉取视频充值订单
            if(isset($result['data']['videoOrder'])&&!empty($result['data']['videoOrder'])){

                $insertLogId = self::_insertOrderLog($params, 11, $result['data']['videoOrder'], 1, $params['channelId']);

                foreach ($result['data']['videoOrder'] as $key => $v) {
                    
                    $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $params['channelId'], 'good_type' => 11, 'ordersn' => $v['order_sn'],'record_id'=>$v['id']]);
                
                     if ($v['member_id']==0 || !empty($orderInfo) || $v['room_id']==0) {
                        continue;
                    }

                    switch ($params['channelId']) {
                        case 4:

                            $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['member_id']]);

                            $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                            $memInfo['mobile'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                            $memInfo['nickname'] = isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                            break;

                        case 3:

                            $mt_memInfo = UseIndexService::getMaituMidData(['number' => $v['member_id']]);

                            $memInfo['parent_id'] = 0;
                            $memInfo['mobile'] = $mt_memInfo['mobile'];
                            $memInfo['nickname'] = $mt_memInfo['nickName'];

                            break;

                        default:

                            $memInfo = GroupManageModel::getUserParentIdByMidNew($v['member_id']);

                            break;
                    }


                    $insertData = [];
                    $insertData['record_id'] = $v['id'];
                    $insertData['ordersn'] = $v['order_no'];
                    $insertData['member_id'] = $v['member_id'];
                    $insertData['parent_id'] = $memInfo['parent_id'];
                    $insertData['buyer_phone'] = $memInfo['mobile'];
                    $insertData['buyer_nickname'] = $memInfo['nickname'];
                    $insertData['bm_mobile'] = $v['member_mobile']; //充值对应手机号
                    $insertData['bm_order_sn'] = $v['bm_bill_id'];
                    $insertData['goods_id'] = $v['video_id'];
                    $insertData['goods_name'] = $v['bm_item_name'];
                    $insertData['goods_price_buy'] = $v['bm_order_cost'];
                    $insertData['actual_price'] = $v['channel_pay_amount'];
                    $insertData['goods_total'] = $v['bm_item_cost'];
                    $insertData['pay_form_no'] = $v['pay_number'];
                    $insertData['updated_at'] =strtotime($v['update_time']);
                    $insertData['pay_time'] = $v['channel_pay_time'];
                    $insertData['goods_category'] = $v['type'];
                    $insertData['created_at'] =strtotime($v['create_time']);
                    $insertData['room_id'] = $v['room_id'];
                    $insertData['pay_status'] = 1;
                    $insertData['channel'] = $params['channelId'];
                    $insertData['good_type'] = 11;
                    $insertData['is_syn'] = 0;
                    $insertData['order_from'] = $v['source_id'];
                    $insertData['insert_time'] = time();
                    $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                    if (!$inserOrderId) {
                        $desc = 0;
                    }

                    $upOrderLog = PullTaskRedisModel::upOrderLog($insertLogId, ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);

                }
                
            }

            //拉取话费充值订单
            if(isset($result['data']['mobileOrder'])&&!empty($result['data']['mobileOrder'])){

                $insertLogId = self::_insertOrderLog($params, 10, $result['data']['mobileOrder'], 1, $params['channelId']);

                foreach ($result['data']['mobileOrder'] as $key => $v) {
                    
                    $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $params['channelId'], 'good_type' => 10, 'ordersn' => $v['order_sn'],'record_id'=>$v['id']]);
                
                     if ($v['member_id']==0 || !empty($orderInfo) || $v['room_id']==0) {
                        continue;
                    }

                    switch ($params['channelId']) {
                        case 4:

                            $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['member_id']]);

                            $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                            $memInfo['mobile'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                            $memInfo['nickname'] = isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                            break;

                        case 3:

                            $mt_memInfo = UseIndexService::getMaituMidData(['number' => $v['member_id']]);

                            $memInfo['parent_id'] = 0;
                            $memInfo['mobile'] = $mt_memInfo['mobile'];
                            $memInfo['nickname'] = $mt_memInfo['nickName'];

                            break;

                        default:

                            $memInfo = GroupManageModel::getUserParentIdByMidNew($v['member_id']);

                            break;
                    }


                    $insertData = [];
                    $insertData['record_id'] = $v['id'];
                    $insertData['ordersn'] = $v['order_sn'];
                    $insertData['member_id'] = $v['member_id'];
                    $insertData['parent_id'] = $memInfo['parent_id'];
                    $insertData['buyer_phone'] = $memInfo['mobile'];
                    $insertData['buyer_nickname'] = $memInfo['nickname'];
                    $insertData['bm_mobile'] = $v['mobile']; //充值对应手机号
                    $insertData['bm_order_sn'] = $v['bm_order_sn'];
                    $insertData['goods_id'] = $v['bm_item_id'];
                    $insertData['goods_name'] = $v['bm_item_name'];
                    $insertData['goods_price_buy'] = $v['order_cost'];
                    $insertData['actual_price'] = $v['order_amount'];
                    $insertData['goods_total'] = $v['total_amount'];
                    $insertData['pay_form_no'] = $v['pay_number'];
                    $insertData['updated_at'] =strtotime($v['update_time']);
                    $insertData['pay_time'] = strtotime($v['pay_time']);
                    $insertData['goods_type'] = $v['time_limit'];
                    $insertData['goods_category'] = $v['type'];
                    $insertData['created_at'] =strtotime($v['create_time']);
                    $insertData['room_id'] = $v['room_id'];
                    $insertData['pay_status'] = 1;
                    $insertData['channel'] = $v['channel_id'];
                    $insertData['good_type'] = 10;
                    $insertData['is_syn'] = 0;
                    $insertData['order_from'] = $v['source_id'];
                    $insertData['insert_time'] = time();
                    $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                    if (!$inserOrderId) {
                        $desc = 0;
                    }

                    $upOrderLog = PullTaskRedisModel::upOrderLog($insertLogId, ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);

                }
            }
        }

    }


    /**
     * 拉取本地生活加油、视频充值、手机充值订单
     * param $channel_id 渠道标识 4 直订
     * param $good_type 10 话费充值 11 视频充值 12 加油  14 电影票
     */
    public static function insertMovieOrderData()
    {

        $params['beginCreateTime'] = date("Y-m-d H:i:s", strtotime('-4 days'));
        $params['endCreateTime'] = date("Y-m-d H:i:s", time());
        $params['filter'] = 1;
        $params['timeType'] = 0; //时间类型 1 更新时间 0 创建时间

        $result = curlPost(self::$MovieUrl, $params);

        if (false == $result) {
            \Log::error("接口请求异常");
            return false;
        }



        $result = json_decode($result, true);

        if($result['code']==200&&$result['msg']=='success')
        {   

            //拉取电影票订单
            if(isset($result['data'])&&!empty($result['data'])){

                $insertLogId = self::_insertOrderLog($params, 14, $result['data'], 1, 0);

                foreach ($result['data'] as $key => $v) {
                    
                    switch ($v['channel_id']) {
                        case 1:
                            $params['channelId'] = 1;
                            break;
                        case 2:
                            $params['channelId'] = 4;
                            break;
                        
                        default:
                            # code...
                            break;
                    }

                    $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $params['channelId'], 'good_type' => 14, 'ordersn' => $v['orderNo']]);
                
                     if ($v['mid']==0 || !empty($orderInfo) || $v['roomId']==0) {
                        continue;
                    }



                    switch ($params['channelId']) {
                        case 4:

                            $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['mid']]);

                            $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['roomId']);
                            $memInfo['mobile'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                            $memInfo['nickname'] = isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                            break;

                        case 3:

                            $mt_memInfo = UseIndexService::getMaituMidData(['number' => $v['mid']]);

                            $memInfo['parent_id'] = 0;
                            $memInfo['mobile'] = $mt_memInfo['mobile'];
                            $memInfo['nickname'] = $mt_memInfo['nickName'];

                            break;

                        default:

                            $memInfo = GroupManageModel::getUserParentIdByMidNew($v['mid']);

                            break;
                    }


                    $insertData = [];
                    $insertData['record_id'] = 0;
                    $insertData['ordersn'] = $v['orderNo'];//
                    $insertData['member_id'] = $v['mid'];
                    $insertData['parent_id'] = $memInfo['parent_id'];
                    $insertData['buyer_phone'] = $memInfo['mobile'];
                    $insertData['buyer_nickname'] = $memInfo['nickname'];
                    $insertData['bm_mobile'] = $v['mobile']; //
                    $insertData['goods_id'] = $v['filmId'];
                    $insertData['goods_name'] = $v['filmName'];
                    $insertData['goods_cover_image'] = $v['pic'];
                    $insertData['actual_price'] = $v['amount']*100;
                    $insertData['goods_total'] = $v['totalPrice']*100;
                    $insertData['goods_price_orig'] = $v['marketUnitPrice']*100;
                    $insertData['updated_at'] =strtotime($v['updateTime']);
                    $insertData['pay_time'] = strtotime($v['paymentTime']);
                    $insertData['created_at'] =strtotime($v['createTime']);
                    $insertData['room_id'] = $v['roomId'];
                    $insertData['pay_status'] = 1;
                    $insertData['channel'] = $params['channelId'];
                    $insertData['good_type'] = 14;
                    $insertData['is_syn'] = 0;
                    $insertData['insert_time'] = time();
                    $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                    if (!$inserOrderId) {
                        $desc = 0;
                    }

                    $upOrderLog = PullTaskRedisModel::upOrderLog($insertLogId, ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);

                }

            }

        }
    }



    /**
     * 拉取酒店订单
     * param $channel_id 渠道标识 4 直订
     * param $good_type 10 话费充值 11 视频充值  12 加油  13酒店 
     */
    public static function insertHotelOrderData($param)
    {

        $params['startTime'] = isset($param['startTime'])?$param['startTime']:date("Y-m-d H:i:s", strtotime('-4 days'));
        $params['endTime'] = isset($param['startTime'])?$param['startTime']:date("Y-m-d H:i:s", time());
        switch ($param['channel_id']) {
            case 1:
                $params['channel_name'] = 'yuetao';
                break;
            
            case 4:
                $params['channel_name'] = 'yuecheng';
                break;

            default:
                # code...
                break;
        }
        
        $result = PullTaskRedisModel::getHotelOrderList($params);

        if(!empty($result)){

            $insertLogId = self::_insertOrderLog($params, 13, $result, 1, 0);

            foreach ($result as $key => $v) {

                $orderInfo = PullTaskRedisModel::getOrderInfo(['channel' => $param['channel_id'], 'good_type' => 13, 'ordersn' => $v['order_no']]);



                if ($v['member_id']==0 || !empty($orderInfo) || $v['room_id']==0) {

                    if(!empty($orderInfo)&&$v['order_status']==25){

                        $data['order_status'] = 2;
                        $data['pay_status'] = 0;
                        $data['end_refund_time'] = strtotime($v['cancel_time']);

                        PullTaskRedisModel:: updateOrderData($v['order_no'], $data);

                    }

                    continue;
                }

                switch ($param['channel_id']) {
                    case 4:

                        $zd_memInfo = UseIndexService::getZhidingMidData(['memberSubId' => $v['member_id']]);

                        $memInfo['parent_id'] = PullTaskRedisModel::getMidByRoomId($v['room_id']);
                        $memInfo['mobile'] = isset($zd_memInfo['mobile']) ? $zd_memInfo['mobile'] : 0;
                        $memInfo['nickname'] = isset($zd_memInfo['memberSubDto']['nickName']) ? $zd_memInfo['memberSubDto']['nickName'] : '';

                        break;

                    case 3:

                        $mt_memInfo = UseIndexService::getMaituMidData(['number' => $v['member_id']]);

                        $memInfo['parent_id'] = 0;
                        $memInfo['mobile'] = $mt_memInfo['mobile'];
                        $memInfo['nickname'] = $mt_memInfo['nickName'];

                        break;

                    default:

                        $memInfo = GroupManageModel::getUserParentIdByMidNew($v['member_id']);

                        break;
                }


                $insertData = [];
                $insertData['record_id'] = 0;
                $insertData['ordersn'] = $v['order_no'];//
                $insertData['member_id'] = $v['member_id'];
                $insertData['parent_id'] = $memInfo['parent_id'];
                $insertData['buyer_phone'] = $memInfo['mobile'];
                $insertData['buyer_nickname'] = $memInfo['nickname'];
                $insertData['goods_id'] = $v['hotel_id'];
                $insertData['goods_name'] = $v['hotel_name'];
                $insertData['goods_cover_image'] = $v['hotel_pic'];
                $insertData['actual_price'] = $v['pay_amount'];
                $insertData['goods_total'] = $v['total_amount'];
                $insertData['updated_at'] =strtotime($v['update_time']);
                $insertData['pay_time'] = strtotime($v['pay_time']);
                $insertData['created_at'] =strtotime($v['create_time']);
                $insertData['room_id'] = $v['room_id'];
                $insertData['pay_status'] = 1;
                $insertData['channel'] = $param['channel_id'];
                $insertData['good_type'] = 13;
                $insertData['is_syn'] = 0;
                $insertData['insert_time'] = time();
                $insertData['commission'] = $v['commission_amount'];
                $inserOrderId = PullTaskRedisModel::insertOrderData('mall_order_goods', $insertData);
                if (!$inserOrderId) {
                    $desc = 0;
                }

                $upOrderLog = PullTaskRedisModel::upOrderLog($insertLogId, ['desc' => empty($desc) ? '同步数据有失败' : '同步数据成功']);
            }





        }
    }

}