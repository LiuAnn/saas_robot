<?php

namespace App\Service\Community;

use App\Model\Admin\GroupModel;
use App\Model\Community\AutoGroupCreateModel;
use App\Model\Community\GroupActivationModel;
use App\Model\Community\PullTaskRedisModel;
use App\Model\Integral\GroupIntegralModel;
use App\Model\Robot\KeywordTailModel;
use App\Models\SendMiniApp;
use App\Models\WhiteListWx;
use App\Service\Sign\GroupSign;
use App\Service\Sign\GroupSignService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Model\Community\CommunityModel;
use App\Model\Community\GroupGoodsDataModel;
use App\Model\Mall\YanModel;
use App\Service\VoucherBag\CouponService;
use App\Service\Member\MemberService;
use App\Model\RobotOrder\RobotModel;
//use App\Service\Client\StallGoodsService;

class IndexService
{
    //处理请求
    public static function pathInlog($body)
    {
        $time = date("Y-m-d H:i:s", time());
        $action = $body['action'];
        if (!$action) {
            self:: return_error_result(['error_code' => 1]);
        }
        $appid = $body['appid'];
        $wxid = $body['wxid'];
        $data = $body['data'];

        if($wxid=='wxid_8z0h8r9yq2q922'){
            Log::info('this is a test for add new member'.$wxid.$action);
        }
        //日志存储
        if($action=='report_new_msg'){
            //if ($wxid == 'wxid_8z0h8r9yq2q922' || 1) {
            if (empty($data['msg']['room_wxid'])) {
//               $insertData ['appid']=$appid;
//               $insertData ['wxid']=$wxid;
//               $insertData ['created_at']=$time;
//               $insertData ['is_send']=isset($data['msg']['is_send']) && $data['msg']['is_send']==true?1:0;
               $room_wxid=$data['msg']['room_wxid'];
//               $insertData ['room_wxid']=$room_wxid;
//               $insertData ['msg_type']=$data['msg']['msg_type'];
               $wxid_from=$data['msg']['wxid_from'];
               $msg=$data['msg'];
               $message = isset($msg['raw_msg']) ? $msg['raw_msg'] : '';
//               $insertData ['wxid_to']=$data['msg']['wxid_to'];
//               $insertData ['data_info']=json_encode($data);
//            CommunityModel::insertTableData('report_msg',$insertData);
                //17466131687@chatroom
                //测试群 roomId 日志记录
                $insertData ['appid'] = $appid;
                $insertData ['wxid'] = $wxid;
                $insertData ['created_at'] = $time;
                $insertData ['is_send'] = isset($data['msg']['is_send']) && $data['msg']['is_send'] == true ? 1 : 0;
                $insertData ['room_wxid'] = $room_wxid;
                $insertData ['msg_type'] = $data['msg']['msg_type'];
                $insertData ['wxid_from'] = $wxid_from;
                $insertData ['wxid_to'] = $data['msg']['wxid_to'];
                $insertData ['data_info'] = $message;
                CommunityModel::insertTableData('report_msg', $insertData);
                if ($data['msg']['msg_type'] == 4901 && 1) {
                    $miniAppXmlMsg = xmlToArray($message);
                    $returnMsg = $miniAppXmlMsg['appmsg']['weappinfo']['pagepath'] ?? '';
                    if (!empty($returnMsg)) {
                        $msg_list = [];
                        $msg_list[] = [
                            "msg_type" => 1,
                            "msg" => $returnMsg
                        ];
                        $at_list = [];
                        $at_style = 1;//必须1
                        $wxid_to = $wxid_from;
                        if (isset($data['msg']['room_wxid']) && !empty($data['msg']['room_wxid'])) {
                            $wxid_to = $data['msg']['room_wxid'];
                        }
                        $reply = [
                            "task_type" => 1,
                            "task_dict" => [
                                "wxid_to" => $wxid_to,
                                "at_list" => $at_list, //at对象
                                "msg_list" => $msg_list,
                                "at_style" => $at_style
                            ]
                        ];
                        self::return_success_result([
                            'error_code' => 0,
                            'error_reason' => '',
                            'ack_type' => 'common_ack',
                            'data' => [
                                'reply_task_list' => empty($reply) ? [] : [$reply]
                            ]
                        ], $wxid);
                    }
                }

//                $baseXml = file_get_contents(config_path() . '/miniAppBase.xml');
//                $name = '悦地摊';
//                $userName = 'gh_bb578ce34f1f@app';
//                $title = '卖火柴的小女孩';
//                $pagePath = '';
//                $baseXml = str_replace(
//                    ['{name}', '{userName}','{title}','{pagePath}'],
//                    [$name, $userName, $title, $pagePath],
//                    $baseXml
//                );
//                $newXml = PullTaskRedisModel::getSendMiniAppXmlDataByContent($baseXml, "&room_id=121&group_mid=921696&order_froms=1");
//                $xmlData = xmlToArray($baseXml);
//
                $miniAppXml = SendMiniApp::getMiniAppList();
                $newXml = $miniAppXml[1]['app_xml'];
                $xmlData = xmlToArray($newXml);

                $xmlData['appmsg']['weappinfo']['pagepath'] =
                    'page/yuemall/pages/JDUnionDetail/JDUnionDetail.html?goods_id=64724796496&reCode=F23385334&room_id=1';


                $newXml = arr2xml($xmlData);
                $msg_list = [];
                $msg_list11[] = [
                    "msg_type" => 4901,
                    "raw_msg" => $newXml,
                    "cover_url" => null
                ];
                $insertData ['data_info'] = json_encode($msg_list);
                //CommunityModel::insertTableData('report_msg', $insertData);
                $at_list = [];
                $at_style = 1;//必须1
                $reply = [
                    "task_type" => 1,
                    "task_dict" => [
                        "wxid_to" => "22843191047@chatroom",
                        "at_list" => $at_list, //at对象
                        "msg_list" => $msg_list,
                        "at_style" => $at_style
                    ]
                ];
                if (!empty($msg_list)) {
                    self::return_success_result([
                        'error_code' => 0,
                        'error_reason' => '',
                        'ack_type' => 'common_ack',
                        'data' => [
                            'reply_task_list' => empty($reply) ? [] : [$reply]
                        ]
                    ], $wxid);
                }

            }
        }else if($action=='pull_task'){
            CommunityModel::insertTableData('pull_task',['action'=>$action,'appid'=>$appid,'wxid'=>$wxid,'data_info'=>json_encode($data),'created_at'=>$time]);
        }else{
            CommunityModel::insertTableData('action_log_one',['action'=>$action,'appid'=>$appid,'wxid'=>$wxid,'data_info'=>json_encode($data),'created_at'=>$time]);
        }
        if(in_array($wxid, config('community.bind_no_method'))&& $action!='login'&&$action!='logout'){
            self:: return_error_result(['error_reason' => '接口未实现']);
        }

        if ($action == 'login') {
            self::login($appid, $wxid, $data);
        } else if ($body['action'] == 'logout') {
            self::logout($appid, $wxid, $data);
        } else if ($body['action'] == 'pull_task') {
             //请求的操作
            //获取 当前机器人的管理者
                 $wxmidBindId = CommunityModel::getWxidBindMid($wxid);
//                if(in_array($wxid, config('community.wxid_tag_data'))){
//                    self::pull_task_test($appid, $wxid, $data);
//                }else if(in_array($wxid, config('community.bind_mid_wxid'))||$wxmidBindId->wxid_type==3){
                    self::pull_task_new($appid, $wxid, $data,$wxmidBindId);
//                }else{
//                    self::pull_task($appid, $wxid, $data);
//                }
        }else if($body['action']=='report_new_msg'){
//           if(array_key_exists('file_index',$body['data']['msg'])){
//                //处理文件删除
//               $returnData =  CommunityModel::fileIsSetServer($body);
//               if(empty($returnData->path_info)||!empty($returnData)){
//                   self::up_report_new_msg($appid, $wxid, $data);
//               }else{
//                   //文件没有处理
//                   self::report_new_msg($appid, $wxid, $data);
//               }
//           }else{
                 self::report_new_msg($appid, $wxid, $data);
//           }
        }else if($body['action']=='report_room_member_info'){
            //上报群成员详细信息
            self::report_room_member_info($appid, $wxid, $data);
        }else if($body['action']=='report_contact'){
            //上报当前好友列表和群列表
            self::report_contact($appid, $wxid, $data);
        }else if($body['action']=='report_contact_update'){
            //上报成员信息变化
            self::report_contact_update($appid, $wxid, $data);
        }else if($body['action']=='report_room_member_change'){
            //上报群成员变化
            self::report_room_member_change($appid, $wxid, $data);
        }else if($body['action']=='report_task_result'){
            //向回调接口反馈任务执行的结果
            self::report_task_result($appid, $wxid, $data);
        }else if($body['action']=='report_friend_removed'){
            //当某个好友被删除了会上报该事件
            self::report_friend_removed($appid, $wxid, $data);
        } else if($body['action']=='report_new_friend'){
            //每当有新的好友时,上报新好友的个人信息(此时对方已经成为了我的好友)
            self::report_new_friend($appid, $wxid, $data);
        } else if($body['action']=='report_user_info'){
            //上报具体某个微信的详情
            self::report_user_info($appid, $wxid, $data);
        } else if($body['action']=='report_new_room'){
            //上报新群
            self::report_new_room($appid, $wxid, $data);
        }else if( $body['action']=='report_friend_add_request'){
            //新的好友请求
            self::report_friend_add_request($appid, $wxid, $data);
        }else if( $body['action']=='report_room_removed'){
            //新的好友请求
            self::reportRoomRemoved($appid, $wxid, $data);
        }else if( $body['action']=='report_room_member_update'){
            //
            self::report_room_member_update($appid, $wxid, $data);
        }else {
            self:: return_error_result(['error_reason' => '接口未实现']);
        }
    }
    /**
     *
     * 错误接口返回
     * @param array $params
     */
    public static function return_error_result($params = ['error_reason' => '未知错误'])
    {
        $base_result = [
            'error_code' => 1,
            'error_reason' => $params['error_reason']
        ];
        echo json_encode(array_merge($base_result, $params));
        exit;
    }
    /**
     * 成功正确接口返回
     * @param array $params
     */
    public static function return_success_result($params = [], $wxid = '')
    {
        $base_result = [
            'error_code' => 0,
            'error_reason' => ''
        ];
        $time = date("Y-m-d H:i", time());
        CommunityModel::insertTableData(
            'action_log_return_one',
            ['action'=>$params['ack_type'],
                'wxid'=>$wxid,
                'data_info'=>json_encode($params),
                'created_at'=>$time,
                'type'=>1]);
        echo json_encode(array_merge($base_result, $params));
        exit;
    }
    /**
     * 登录进来
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function login($app, $wxid, $data)
    {
        //登录时判断当前微信号是否在白名单中
         $iswhiteData =  CommunityModel::isWhiteData($wxid,$data);
         //判断是否申请了机器人记录
         if(empty($iswhiteData)){
           $dataInfo = CommunityModel::isExclusiveData($wxid,$data);
           if (isset($dataInfo['manage_phone'])) {
               $bindMid = CommunityModel::getBindMid($dataInfo['manage_phone'], $dataInfo['platform_identity']);
           }
           if(!empty($dataInfo)&&!empty($bindMid)&&$bindMid['sup_id']>0){
               $order_by = CommunityModel::getOrderBydata($dataInfo['platform_identity']);
               $insertdata['wx_alias'] = $data['wx_alias'];
               $insertdata['wxid'] = $wxid;
               $insertdata['status'] = 0;
               $insertdata['created_at'] = date('Y-m-d H:i:s',time());
               $insertdata['wxid_type'] = 3;
               $insertdata['is_send_by_ass'] = 0;
               $insertdata['exclusive_mid'] = $dataInfo['mid'];
               $insertdata['platform_identity'] = $dataInfo['platform_identity'];
               $insertdata['order_by'] = $order_by['order_by']+1;
               $insertdata['wx_name'] = str_replace($order_by['order_by'], $insertdata['order_by'],$order_by['wx_name']);
               $insertdata['mid'] = $bindMid['sup_id'];
               $insertdata['bind_mid'] = $bindMid['mid'];
                CommunityModel::insertTableData('white_list_wx',$insertdata);
           }else{
               return    self::return_error_result(); exit();
           }
         }
        $params = [
            'ack_type' => 'login_ack',
            'data' => []
        ];
        CommunityModel::insertMemberLoginInfo('member_login_info',$app, $wxid, $data);//机器人登录记录

        if (isset($data['nonce'])) {
            $secretkey = config('community.appid')[$app]; //需要修改成自己的secretkey，在登录网页获取
            $params['data']['signature'] = md5($wxid . '#' . $data['nonce'] . '#' . $secretkey);
            $params['data']['option']['flag_report_contact'] =7;
            //1发送的report_contact中包含好友的信息(friend_list)
            //2	发送的report_contact中包含群的信息(group_list)
            //4	发送的:中包含关注的公众号的信息(public_list)

//            if (isset($data['qr_session_id'])) {
//                $updateData=[
//                    'qrcode_session_id'=>$data['qr_session_id'],
//                    'login_id'=>$login_id,
//                    'is_login'=>1,
//                    'updated_at'=>date('Y-m-d H:i:s',time()),
//                ];
//                CommunityModel::communityMemberQrcodeUpdate('community_member_qrcode','qrcode_session_id',$updateData);
//            }
        }
        self::return_success_result($params);
    }
    /**
     * 登出行为
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function logout($app, $wxid, $data)
    {
        $params = [
            'ack_type' => 'logout_ack',
            'data' => [
                'exit_wehub'=>1,  //出当前的wehub进程
                //'exit_wechat'=>1,//控制的微信进程
            ]
        ];

        CommunityModel::insertMemberLoginInfo('member_login_info',$app, $wxid, $data,2);//机器人登录记录
        self::return_success_result($params);
    }

    //上报当前好友列表和群列表
    //这个http请求Post的数据量会比较大(好友/群越多,post的数据就越大),请将服务端能接受的post_max_size 调整成至少10M
    public static function report_contact($appid, $wxid, $data)
    {
        if(!empty($data)){
            Log::info('getreport_contactInfo    report_contact'.$wxid);
            CommunityModel::insertReportContactRoomMemberInfo('report_room_member_info',$appid, $wxid, $data);
        }
        $data =array();
        $params = [
            'ack_type' => 'common_ack',
            'data' => $data
        ];
        self::return_success_result($params);
    }


    /**
     * 发给wehub说要上传文件
     * @param $appid
     * @param $wxid
     *
     * @param $data
     */
     public static function up_report_new_msg($appid, $wxid, $data)
    {
        $message = $data['msg'];
        $reply['task_type']=9;
        $reply['task_dict']=['file_index'=>$message['file_index']];
        self::return_success_result([
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'common_ack',
            'data' => [
                'reply_task_list' => [
                    $reply
                ]
            ]
        ]);
     }

    /**
     * 聊天内容发送 hello，则会触发回复  自动回复
     */
    public static function report_new_msg($appid, $wxid, $data)
    {

        $message = $data['msg'];
        $room_wxid = isset($message['room_wxid']) ? $message['room_wxid'] : '';
        $sender_wxid = $message['wxid_from'];
        if ($wxid == $sender_wxid) {
            $reply = [];
        } else {
            $reply =[];
        }
        //邀请入群  同意进群 去掉 2020-07-17
        if( isset($data['msg']['link_title'])&&$data['msg']['link_title']=='邀请你加入群聊'){
            $whileData = CommunityModel::getWhileBindMidGmv($wxid);
            if($whileData->exclusive_mid>0){
                if($whileData->all_order_sum>=config('community.exclusive_order_sum')&&$whileData->group_sum<50){
                    $reply=[
                        'task_type'=>19,
                        'task_dict'=>['invite_url'=>$data['msg']['link_url']],
                    ];
                }else if($whileData->group_sum<50&&$whileData->all_order_sum<config('community.exclusive_order_sum')){
                    $reply=[];
                }
            }else{
                $reply=[
                    'task_type'=>19,
                    'task_dict'=>['invite_url'=>$data['msg']['link_url']],
                ];
            }
        }

        if (isset($message['msg']) && !empty($message['msg']) && $message['msg_type'] == 1 && $message['wxid_from'] != $wxid) {
            //关键词触发记录  如果不是群内消息则不处理
            if (!empty($room_wxid)) {
                self::keywordRecord($message['msg'], $room_wxid, $wxid);
            }
        }
        //关键字回复
        if (isset($message['msg']) && !empty($message['msg'])) {
            //查询相关的回复内容
            $msg_list = [];
            if($message['msg']>0){
                $returnData =  CommunityModel::geRobotFriendReturnData($message['msg'],$message['wxid_to']);
                foreach ($returnData as $vmsg) {
                    if ($vmsg->type == 1 && !empty($vmsg->content)) {
                        $msg_list[] = [
                            'msg_type' => 1,
                            'msg' => $vmsg->content,
                        ];
                    } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                        if (empty($vmsg->content)) {
                            continue;
                        }
                        $msg_list[] = [
                            'msg_type' => 3,
                            'msg' => $vmsg->content,
                        ];
                    }else if($vmsg->type ==3&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>43,
                            'video_url'=>$vmsg->content,
                        ];
                    }else if($vmsg->type ==4&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>4903,
                            'video_url'=>$vmsg->content,
                        ];
                    } else if($vmsg->type ==8&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>4901,
                            'raw_msg'=>$vmsg->content
                        ];
                    }
                }
            }else if(strlen(self::removeEmojiChar($message['msg']))<30){
                //签到保存记录  签到关键词判断放在里边去判断
                if (!empty($room_wxid)) {
                    $roomInfo = GroupModel::getRoomIdByRoomWxId($room_wxid);
                    //群签到开关 1开启 2关闭
                    if ($roomInfo && $roomInfo['sign_switch'] == 1) {
                        $roomId = $roomInfo['id'] ?: 0;
                        if (empty($roomId)) {
                            Log::info('群信息未获取 平台0 wxid:' . $room_wxid);
                        }
                        $user_info = WhiteListWx::where('wxid', $wxid)->first();
                        if ($user_info) {
                            $platform_identity = $user_info->platform_identity;
                            $signInsertData = [];
                            $signInsertData['room_id'] = $roomId;
                            $signInsertData['member_wxid'] = $sender_wxid;
                            $signInsertData['platform_identity'] = $platform_identity;
                            $signInsertData['room_wxid'] = $room_wxid;
                            $signInsertData['report'] = trim($message['msg']);
                            $reply = GroupSignService::insertGroupSignRecord($signInsertData, $user_info);
                            Log::info('群签到日志记录--' . json_encode($reply));
                            if (!empty($reply)) {
                                self::return_success_result([
                                    'error_code' => 0,
                                    'error_reason' => '',
                                    'ack_type' => 'common_ack',
                                    'data' => [
                                        'reply_task_list' => empty($reply) ? [] : [$reply]
                                    ]
                                ]);
                            }
                        }
                    }
                }
                $resData =  CommunityModel::geRobotReplyData($message['msg'],$wxid, $message['wxid_to']);
                if (!empty($resData)) {
                    foreach ($resData as $v) {
                        if ($v->type == 1 && !empty($v->content)) {
                            $msg_list[] = [
                                'msg_type' => 1,
                                'msg' => $v->content,
                            ];
                        } else if ($v->type == 2 && !empty($v->content)) {
                            if (empty($v->content)) {
                                continue;
                            }
                            $msg_list[] = [
                                'msg_type' => 3,
                                'msg' => $v->content,
                            ];
                        }else if($v->type ==3&&!empty($v->content)){
                            if(empty($v->content)){
                                continue;
                            }
                            $msg_list[] =[
                                'msg_type'=>43,
                                'video_url'=>$v->content,
                            ];
                        }else if($v->type ==4&&!empty($v->content)){
                            if(empty($v->content)){
                                continue;
                            }
                            $msg_list[] =[
                                'msg_type'=>4903,
                                'video_url'=>$v->content,
                            ];
                        } else if($v->type ==8&&!empty($v->content)){
                            if(empty($v->content)){
                                continue;
                            }
                            $msg_list[] =[
                                'msg_type'=>4901,
                                'raw_msg'=>$v->content
                            ];
                        }
                    }
                }


        }

            if(!empty($msg_list)){
                $at_list = [];
                $at_style = 0;

                foreach ($msg_list as $key => $val){
                    // 内容中需要替换字符串为@用户时根据要求替换格式
                    if(strpos($val['msg'],'【:user_name】')){
                        $at_list = [$message['wxid_from']];
                        $msg_list[$key]['msg'] = str_replace('【:user_name】', '{$@}', $val['msg']);
                        $at_style = 1;//必须1
                    }
                }
                $reply = [
                    "task_type" => 1,
                    "task_dict" => [
                        "wxid_to" => $message['wxid_to'],
                        "at_list"=> $at_list, //at对象
                        "msg_list" => $msg_list,
                        "at_style" => $at_style
                    ]
                ];

//                CommunityModel::insertTableData('action_log_one',['action'=>'report_new_msg','appid'=>$appid,'wxid'=>$wxid,'data_info'=>json_encode($reply),'type'=>1]);
            }
        }
//        if(empty($reply)){
//            $reply =self::getReportRoomReplay($wxid);
//        }
        self::return_success_result([
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'common_ack',
            'data' => [
                'reply_task_list' => empty($reply)?[]: [$reply]
            ]
        ]);
    }


    public  static  function keywordRecord($str, $room_wxid, $wxid)
    {
        $keywordList = CommunityModel::getKeywordList($room_wxid, $wxid);
        if (!empty($keywordList)) {
            $room_id = CommunityModel::getRoomIdByRoomWxId($room_wxid);
            foreach ($keywordList as $value) {
                $is_exist = strpos($str,$value['keyword']);
                if ($is_exist !== false) {
                    if (empty($room_id)) {
                        continue;
                    }
                    //关键词存在,
                    $createTime = date("Y-m-d H") . ":00:00";
                    $insertData = [];
                    $insertData['room_id'] = $room_id;
                    $insertData['keyword_id'] = $value['id'];
                    $insertData['created_at'] = $createTime;
                    $insertData['touch_off_num'] = 1;
                    $insertData['platform_identity'] = $value['platform_identity'];
                    CommunityModel::incrRecordKeyword($value['id']);
                    $recordTail = CommunityModel::getRecordTail($insertData);
                    //记录存在次数加一,不存在新增
                    if ($recordTail) {
                        $incrResult = KeywordTailModel::incrRecordKeywordDetail($insertData);
                    } else {
                        $insertResult = KeywordTailModel::insertRecordKeywordDetail($insertData);
                    }
                }
            }
        }
    }

   public  static  function removeEmojiChar($str)
    {
        $mbLen = mb_strlen($str);

        $strArr = [];
        for ($i = 0; $i < $mbLen; $i++) {
            $mbSubstr = mb_substr($str, $i, 1, 'utf-8');
            if (strlen($mbSubstr) >= 4) {
                continue;
            }
            $strArr[] = $mbSubstr;
        }

        return implode('', $strArr);
    }

    /**
     * 欢迎新的好友的行为
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function report_new_friend($app, $wxid, $data)
    {

        $reply = [];

        $returnData =  CommunityModel::geRobotFriendReturnDataByWxid($wxid);

        if(!empty($returnData)){
            $msg_list = [];
            foreach ($returnData as $vmsg) {
                if ($vmsg->type == 1 && !empty($vmsg->content)) {
                    $msg_list[] = [
                        'msg_type' => 1,
                        'msg' => $vmsg->content,
                    ];
                } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                    if (empty($vmsg->content)) {
                        continue;
                    }
                    $msg_list[] = [
                        'msg_type' => 3,
                        'msg' => $vmsg->content,
                    ];
                }else if($vmsg->type ==3&&!empty($vmsg->content)){
                    if(empty($vmsg->content)){
                        continue;
                    }
                    $msg_list[] =[
                        'msg_type'=>43,
                        'video_url'=>$vmsg->content,
                    ];
                }else if($vmsg->type ==4&&!empty($vmsg->content)){
                    if(empty($vmsg->content)){
                        continue;
                    }
                    $msg_list[] =[
                        'msg_type'=>4903,
                        'video_url'=>$vmsg->content,
                    ];
                } else if($vmsg->type ==8&&!empty($vmsg->content)){
                    if(empty($vmsg->content)){
                        continue;
                    }
                    $msg_list[] =[
                        'msg_type'=>4901,
                        'raw_msg'=>$vmsg->content
                    ];
                }
            }

            if(!empty($msg_list)){
                $reply = [
                    "task_type" => 1,
                    "task_dict" => [
                        "wxid_to" => $data['fans_wxid'],
                        "at_list"=>[], //at对象
                        "msg_list" =>$msg_list
                    ]
                ];
            }

        }



        self::return_success_result([
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'common_ack',
            'data' =>[
                'reply_task_list'=>empty($reply)?[]: [$reply]
            ]
        ]);
    }
    /**
     * 获取未上报成员信息的群
     * @param $wxid
     */
    public static function getReportRoomReplay($wxid)
    {
        $redisKeyReport  =   $wxid.'report_room_member_info';
        $tobeCount =   count(Redis::lrange($redisKeyReport,0,-1));
        $data =[];
        if(empty($tobeCount)){
            $ReportData = CommunityModel::getNotReportRoom($wxid);  //当前微信号有没有发送
            if(!empty($ReportData)){
                foreach($ReportData as $value){
                    $reportrpush=[];
                    if(!empty($value->room_wxid)){
                        $reportrpush=['room_wxid'=>$value->room_wxid];
                    }
                    if(!empty($reportrpush)){
                        Redis::rpush($redisKeyReport,json_encode($reportrpush));
                    }
                    $data =json_decode(Redis::lpop($redisKeyReport),true);
                }

            }
        }else{
            $data =json_decode(Redis::lpop($redisKeyReport),true);
        }
        $returnData=[];
        if(!empty($data)){
            $returnData = [
                "task_type"=>4,
                "task_dict"=>["room_wxid_list"=>[$data['room_wxid']]]
            ];
        }
        return $returnData;
    }



    /**
     * 发送新的好友的信息
     * @param $room_wxid
     * @return array
     */
    public static function getSendNewFriendMsg($room_wxid)
    {
        $reply = [
            "task_type" => 1,
            "task_dict" => [
                "wxid_to" => $room_wxid,
                "at_list" => [],
                "msg_list" => [
                    [
                        'msg_type' => 1,
                        'msg' => '欢迎加入悦淘最优秀的的团队',
                    ],
                    [
                        'msg_type' => 49,
                        'link_url' => 'https://mobile.yangkeduo.com/app.html?use_reload=1&launch_url=duo_coupon_landing.html%3Fgoods_id%3D96294046619%26pid%3D9650449_125742646%26customParameters%3D665013%26cpsSign%3DCC_200328_9650449_125742646_3b51cf81f195d02cbccecfd72b74806e%26duoduo_type%3D2&campaign=ddjb&cid=launch_dl_force_',
                        'link_title' => '欢迎加入悦淘最优秀的的团队',
                        'link_desc' => '欢迎加入悦淘最优秀的的团队',
                        'link_img_url' => 'https://img.alicdn.com/bao/uploaded/i2/2887598897/O1CN01PpMGjO2FasNYC6bRK_!!2887598897.jpg',
                    ]
                ]
            ]
        ];
        return $reply;

    }
    /**
     * 轮询收到
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function pull_task($app, $wxid, $data)
    {

        $resultTemplate = [
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'pull_task_ack',
            'data' => []
        ];
        $dataList =$taskData= [];
        $time1 = strtotime(date('Y-m-d',time()))+21600;
        if(time()>$time1){
            $taskData =   self::getMessageSendInfo( $wxid, $data);
        }
//        //如果没有发消息的则走创建群
        if (empty($taskData)) {
            //机器人自动创建群
            $auto_group_list_key = $wxid . AutoGroupCreateModel::GROUP_LIST_KEY;
            $createNum = Redis::llen($auto_group_list_key);
            if ($createNum > 0) {
                $groupInfo = json_decode(Redis::lpop($auto_group_list_key),true);
                AutoGroupCreateModel::where(['id' => $groupInfo['id']])->update(['robot_send_status' => 1]);
                Log::info('ToCreateGroup ' . json_encode($groupInfo));
                $taskData = ['task_type' => 17,
                    'task_dict' => [
                        'member_list' => explode(',', $groupInfo['content'])
                    ]
                ];
            }
            //机器人自动退群
            if (empty($taskData)) {
                $leave_group_key = $wxid . AutoGroupCreateModel::LEAVE_GROUP_KEY;
                $leaveGroupNum = Redis::llen($leave_group_key);
                if ($leaveGroupNum > 0) {
                    $leaveGroupNumData = json_decode(Redis::lpop($leave_group_key), true);
                    Log::info('leaveGroup ' . json_encode($leaveGroupNumData));
                    $taskData = ['task_type' => 8,
                        'task_dict' => $leaveGroupNumData
                    ];
                }
            }

            //机器人修改新建群的昵称
            $update_group_name_key = $wxid . AutoGroupCreateModel::UPDATE_GROUP_NAME_KEY;
            $updateGroupNameNum = Redis::llen($update_group_name_key);
            if ($updateGroupNameNum > 0) {
                $updateGroupNameData = json_decode(Redis::lpop($update_group_name_key),true);
                Log::info('updateGroupName ' . json_encode($updateGroupNameData));
                $taskData = ['task_type' => 18,
                    'task_dict' => $updateGroupNameData
                ];
            }
            //机器人自动邀请好友进群
            $invite_friend_group_key = $wxid . AutoGroupCreateModel::INVITE_FRIEND_KEY;
            $inviteFriendNum = Redis::llen($invite_friend_group_key);
            if ($inviteFriendNum > 0) {
                $insertMember = json_decode(Redis::lpop($invite_friend_group_key),true);
                Log::info('inviteFriendNum ' . json_encode($insertMember));
                $taskData = ['task_type' => 3,
                    'task_dict' => $insertMember
                ];
            }
        }
        if (!empty($taskData)) {
            $task_id = self:: getPullTaskDataId();
            $dataList = [
                'task_id' => strval($task_id),
                'task_data' => $taskData
            ];
        }else{
            $listCacheKey = $wxid.'RoomwxidListKey';
            $listCount = count(Redis::lrange($listCacheKey, 0, -1));
            if(!empty($listCount)){
                $taskData =json_decode(Redis::lpop($listCacheKey),true);
                $taskDataArray = explode(' ', $taskData);

                $task_id = self:: getPullTaskDataId();
                $dataTask = [];
                $dataTask['task_type'] = 4;
                $dataTask['task_dict'] = ['room_wxid_list' => $taskDataArray];
                $dataList = [
                    'task_id' => strval($task_id),
                    'task_data' => $dataTask
                ];
            }

        }
        //这里的demo数据都是非真实数据，只是展示结构
        $resultTemplate['data'] = $dataList;
        self::return_success_result($resultTemplate);
    }


    /**
     * 最新的素材发送
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function pull_task_new($app, $wxid, $data,$wxmidBindId){

        $resultTemplate = [
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'pull_task_ack',
            'data' => []
        ];
        $dataList = [];
        $time1 = strtotime(date('Y-m-d',time()))+21600;
       // $time1 = strtotime(date('Y-m-d',time()))+0;

        if(time()>$time1){
            if( $wxmidBindId->exclusive_mid>0&&$wxmidBindId->is_send_by_ass==0){
                $taskData =   self::getExcluSendInfoNew( $wxid, $data,$wxmidBindId);
            }else{
                $taskData =   self::getTagMessageSendInfoNew( $wxid, $data,$wxmidBindId);
            }
        }
        if (!empty($taskData)) {
            $task_id = self:: getPullTaskDataId();
            $dataList = [
                'task_id' => strval($task_id),
                'task_data' => $taskData
            ];
        } else{
            $listCacheKey = $wxid.'RoomwxidListKey';
            $listCount = count(Redis::lrange($listCacheKey, 0, -1));
            if(!empty($listCount)){
                $taskData =json_decode(Redis::lpop($listCacheKey),true);
                $taskDataArray = explode(' ', $taskData);

                $task_id = self:: getPullTaskDataId();
                $dataTask = [];
                $dataTask['task_type'] = 4;
                $dataTask['task_dict'] = ['room_wxid_list' => $taskDataArray];
                $dataList = [
                    'task_id' => strval($task_id),
                    'task_data' => $dataTask
                ];
            }
        }
        //机器人自动退群
        if(empty($dataList)){
            $leave_group_key = $wxid . AutoGroupCreateModel::LEAVE_GROUP_KEY;
            $leaveGroupNum = Redis::llen($leave_group_key);
            if ($leaveGroupNum > 0) {
                $leaveGroupNumData = json_decode(Redis::lpop($leave_group_key),true);
                Log::info('leaveGroup ' . json_encode($leaveGroupNumData));
                $taskData = ['task_type' => 8,
                    'task_dict' => $leaveGroupNumData
                ];
                $task_id = self:: getPullTaskDataId();
                $dataList = [
                    'task_id' => strval($task_id),
                    'task_data' => $taskData
                ];
            }
        }
        //机器人自动创建群
        if (empty($dataList)) {
            $auto_group_list_key = $wxid . AutoGroupCreateModel::GROUP_LIST_KEY;
            $createNum = Redis::llen($auto_group_list_key);
            if ($createNum > 0) {
                $groupInfo = json_decode(Redis::lpop($auto_group_list_key),true);
                AutoGroupCreateModel::where(['id' => $groupInfo['id']])->update(['robot_send_status' => 1]);
                Log::info('ToCreateGroup ' . json_encode($groupInfo));
                $taskData = ['task_type' => 17,
                    'task_dict' => [
                        'member_list' => explode(',', $groupInfo['content'])
                    ]
                ];
                $task_id = self:: getPullTaskDataId();
                $dataList = [
                    'task_id' => strval($task_id),
                    'task_data' => $taskData
                ];
            }
        }
        //这里的demo数据都是非真实数据，只是展示结构
        $resultTemplate['data'] = $dataList;
        self::return_success_result($resultTemplate);
    }


    /**
     * 专属机器人素材发送
     * @param $wxid
     * @param $data
     * @return array
     * @throws \Exception
     */
    public static function getExcluSendInfoNew($wxid,$data,$wxmidBindId)
    {
        Log::info('getTagMessageSendInfo    bind_mid'. $wxid.json_encode($wxmidBindId));
        //查看当前专属机器人有没有素材发送
        $pushDataBind=[];

        if($wxmidBindId->exclusive_mid>0&&$wxmidBindId->is_send_by_ass==0 ){
            $exclusivePushMid = CommunityModel::getExcluPushMsgDataTag($wxmidBindId->exclusive_mid,$wxid);
            if(!empty($exclusivePushMid)){
                $pushDataBind = $exclusivePushMid;
                $random_str = $pushDataBind[0]->random_str;
                $redisNewKey = $wxid. $random_str . config('community.redisNewKeystr');
                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                if (!empty($id) && empty($tobeCount)) {
                    $pushDataBind = [];
                }
            }
        }
        //查看总管理员的素材
        if (empty($pushDataBind)&&$wxmidBindId->mid>0) {
            $TimePushDataMid = CommunityModel::getTimePushMsgDataTag($wxmidBindId->mid, $wxid);
            if(!empty($TimePushDataMid)){
                $pushDataBind = $TimePushDataMid;
                $random_str = $pushDataBind[0]->random_str;
                $redisNewKey = $wxid. $random_str . config('community.redisNewKeystr');
                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                Log::info('sendToRoomlpop redis send mid  01 '. json_encode([$tobeCount,$id,$pushDataBind]));
                if (!empty($id) && empty($tobeCount)) {
                    $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->mid, $wxid);
                }
            }
        } else {
            $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->mid, $wxid);
        }
        Log::info('getTagMessageSendInfo mid'. $wxid.json_encode($pushDataBind));
        //获取管理员的素材
        if(empty($pushDataBind)&&$wxmidBindId->bind_mid>0){
            $TimePushDataBind = CommunityModel::getTimePushMsgDataTag($wxmidBindId->bind_mid, $wxid);
            Log::info('getTagMessageSendInfo    bind_mid  01'. json_encode($TimePushDataBind));
            if (!empty($TimePushDataBind)) {
                $pushDataBind = $TimePushDataBind;
                $random_str = $pushDataBind[0]->random_str;
                $redisNewKey = $wxid.$random_str.config('community.redisNewKeystr');
                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                Log::info('getTagMessageSendInfo    bind_mid  02'. json_encode([$tobeCount,$id,$pushDataBind]));
                if (empty($tobeCount)&&!empty($id)) {
                    $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->bind_mid, $wxid);
                }
            } else {
                $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->bind_mid, $wxid);
            }
        }
        Log::info('getTagMessageSendInfo bind_mid'. $wxid.json_encode($pushDataBind));

        if(!empty($pushDataBind)){
            Log::info('getTagMessageSendInfo    bind_mid  03'.$pushDataBind[0]->random_str. json_encode($pushDataBind));
            $random_str = $pushDataBind[0]->random_str;
            $redisNewKey = $wxid.$random_str.config('community.redisNewKeystr');
            $tobeCount =   count(Redis::lrange($redisNewKey,0,-1));
            $id =  CommunityModel::getWxSendReload($random_str,$wxid);  //当前微信号有没有发送
            $data=[];
            Log::info('getTagMessageSendInfo    bind_mid  04'. json_encode([$redisNewKey,$tobeCount,$id]));
            if(empty($id)&&$tobeCount>0){
                CommunityModel::insertGetId('send_reload',['random_str'=>$random_str,'wxid'=>$wxid]);
                if( $pushDataBind[0]->exclusive_mid>0&&$wxmidBindId->is_send_by_ass==0 ){
                    //更新悦淘发送的状态
                    curlPostCommon(config('community.changeByRandomStr')[$pushDataBind[0]->platform_identity], ['flag'=>$random_str,'source'=>$pushDataBind[0]->platform_identity]);
                    //StallGoodsService::callbackAssistantPush(['flag'=>$random_str,'source'=>$pushDataBind[0]->platform_identity]);
                }
                $data =json_decode(Redis::lpop($redisNewKey),true);
                Log::info('sendToRoomlpop redis bind_mid' .$random_str. $tobeCount.json_encode($data));
            }else{
                if($tobeCount>0 ){
                    $data =json_decode(Redis::lpop($redisNewKey),true);
                    Log::info('sendToRoomlpop redis bind_mid'.$wxid .$random_str. $tobeCount.json_encode($data));
                }
            }
            Log::info('pushDataBind  empty  bind_mid 05'. json_encode($data));
            if(!empty($data)){
                $count =  CommunityModel::getSendRoomCount($random_str,$data['room_wxid']);
                if($count==0){
                    CommunityModel::insertGetId('send_room_reload',['random_str'=>$random_str,'link_type'=>$pushDataBind[0]->link_type,'wxid'=>$wxid,'room_wxid'=>$data['room_wxid'],'room_id'=>$data['room_id']]);
                    return ['task_type'=>1,'task_dict'=>['wxid_to'=>$data['room_wxid'],'msg_list'=>$data['msg_list'] ,'msg_interval'=>5]];
                }else{
                    return [];
                }
            }
        }else{
            Log::info('pushDataBind  empty  bind_mid 06');
        }
        return [];
    }

    /**
     * 管理员发送素材
     * @param $wxid
     * @param $data
     */
    public  static  function getTagMessageSendInfoNew($wxid,$data,$wxmidBindId)
    {
        //获取 当前机器人的管理者
        Log::info('getTagMessageSendInfo    bind_mid'.$wxid. json_encode($wxmidBindId));
        //查看总管理员的素材
        $TimePushDataMid = CommunityModel::getTimePushMsgDataTag($wxmidBindId->mid, $wxid);
        if (!empty($TimePushDataMid)) {
            $pushDataBind = $TimePushDataMid;
            $random_str = $pushDataBind[0]->random_str;
            $redisNewKey = $wxid. $random_str . config('community.redisNewKeystr');
            $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
            $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
            Log::info('sendToRoomlpop redis send mid  01 '. json_encode([$tobeCount,$id,$pushDataBind]));
            if (!empty($id) && empty($tobeCount)) {
                $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->mid, $wxid);
            }
        } else {
            $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->mid, $wxid);
        }
        Log::info('getTagMessageSendInfo mid'. $wxid.json_encode($pushDataBind));
        //获取绑定人的素材
        if(empty($pushDataBind)&&$wxmidBindId->bind_mid>0){
            $TimePushDataBind = CommunityModel::getTimePushMsgDataTag($wxmidBindId->bind_mid, $wxid);
            Log::info('getTagMessageSendInfo    bind_mid  01'. json_encode($TimePushDataBind));
            if (!empty($TimePushDataBind)) {
                $pushDataBind = $TimePushDataBind;
                $random_str = $pushDataBind[0]->random_str;
                $redisNewKey = $wxid.$random_str.config('community.redisNewKeystr');
                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                Log::info('getTagMessageSendInfo    bind_mid  02'. json_encode([$tobeCount,$id,$pushDataBind]));
                if (empty($tobeCount)&&!empty($id)) {
                    $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->bind_mid, $wxid);
                }
            } else {
                $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->bind_mid, $wxid);
            }
        }
        Log::info('getTagMessageSendInfo bind_mid'. $wxid.json_encode($pushDataBind));

        if(!empty($pushDataBind)){
            Log::info('getTagMessageSendInfo    bind_mid  03'.$pushDataBind[0]->random_str.$wxid. json_encode($pushDataBind));
            $random_str = $pushDataBind[0]->random_str;
            $redisNewKey = $wxid.$random_str.config('community.redisNewKeystr');
            $tobeCount =   count(Redis::lrange($redisNewKey,0,-1));
            $id =  CommunityModel::getWxSendReload($random_str,$wxid);  //当前微信号有没有发送
            $data=[];
            Log::info('getTagMessageSendInfo    bind_mid  04'. json_encode([$redisNewKey,$tobeCount,$id]));
            if(empty($id)&&$tobeCount>0){
                CommunityModel::insertGetId('send_reload',['random_str'=>$random_str,'wxid'=>$wxid]);
                $data =json_decode(Redis::lpop($redisNewKey),true);
                Log::info('sendToRoomlpop redis bind_mid' .$random_str. $tobeCount.json_encode($data));
            }else{
                if($tobeCount>0 ){
                    $data =json_decode(Redis::lpop($redisNewKey),true);
                    Log::info('sendToRoomlpop redis bind_mid'.$wxid .$random_str. $tobeCount.json_encode($data));
                }
            }
            Log::info('pushDataBind  empty  bind_mid 05'. json_encode($data));
            if(!empty($data)){
                $count =  CommunityModel::getSendRoomCount($random_str,$data['room_wxid']);
                if($count==0){
                    CommunityModel::insertGetId('send_room_reload',['random_str'=>$random_str,'link_type'=>$pushDataBind[0]->link_type,'wxid'=>$wxid,'room_wxid'=>$data['room_wxid'],'room_id'=>$data['room_id']]);
                    return ['task_type'=>1,'task_dict'=>['wxid_to'=>$data['room_wxid'],'msg_list'=>$data['msg_list'] ,'msg_interval'=>5]];
                }else{
                    return [];
                }
            }
        }else{
            Log::info('pushDataBind  empty  bind_mid 06');
        }
        return [];
    }


    /**
     * 标签消息发送
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function  pull_task_test($app, $wxid, $data)
    {
        $resultTemplate = [
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'pull_task_ack',
            'data' => []
        ];
        $dataList = [];
        $time1 = strtotime(date('Y-m-d',time()))+21600;
        //$time1 = strtotime(date('Y-m-d',time()))+0;
        if(time()>$time1){
            $taskData =   self::getTagMessageSendInfo( $wxid, $data);
        }
        if (!empty($taskData)) {
            $task_id = self:: getPullTaskDataId();
            $dataList = [
                'task_id' => strval($task_id),
                'task_data' => $taskData
            ];
        } else{
            $listCacheKey = $wxid.'RoomwxidListKey';
            $listCount = count(Redis::lrange($listCacheKey, 0, -1));
            if(!empty($listCount)){
                $taskData =json_decode(Redis::lpop($listCacheKey),true);
                $taskDataArray = explode(' ', $taskData);

                $task_id = self:: getPullTaskDataId();
                $dataTask = [];
                $dataTask['task_type'] = 4;
                $dataTask['task_dict'] = ['room_wxid_list' => $taskDataArray];
                $dataList = [
                    'task_id' => strval($task_id),
                    'task_data' => $dataTask
                ];
            }
        }
        if (empty($dataList)) {
            //机器人自动创建群
            $auto_group_list_key = $wxid . AutoGroupCreateModel::GROUP_LIST_KEY;
            $createNum = Redis::llen($auto_group_list_key);
            if ($createNum > 0) {
                $groupInfo = json_decode(Redis::lpop($auto_group_list_key),true);
                AutoGroupCreateModel::where(['id' => $groupInfo['id']])->update(['robot_send_status' => 1]);
                Log::info('ToCreateGroup ' . json_encode($groupInfo));
                $taskData = ['task_type' => 17,
                    'task_dict' => [
                        'member_list' => explode(',', $groupInfo['content'])
                    ]
                ];
                $task_id = self:: getPullTaskDataId();
                $dataList = [
                    'task_id' => strval($task_id),
                    'task_data' => $taskData
                ];
            }
            //机器人自动退群
            if (empty($dataList)) {
                $leave_group_key = $wxid . AutoGroupCreateModel::LEAVE_GROUP_KEY;
                $leaveGroupNum = Redis::llen($leave_group_key);
                if ($leaveGroupNum > 0) {
                    $leaveGroupNumData = json_decode(Redis::lpop($leave_group_key), true);
                    Log::info('leaveGroup ' . json_encode($leaveGroupNumData));
                    $taskData = ['task_type' => 8,
                        'task_dict' => $leaveGroupNumData
                    ];
                    $task_id = self:: getPullTaskDataId();
                    $dataList = [
                        'task_id' => strval($task_id),
                        'task_data' => $taskData
                    ];
                }
            }else{
                //机器人修改新建群的昵称
                $update_group_name_key = $wxid . AutoGroupCreateModel::UPDATE_GROUP_NAME_KEY;
                $updateGroupNameNum = Redis::llen($update_group_name_key);
                if ($updateGroupNameNum > 0) {
                    $updateGroupNameData = json_decode(Redis::lpop($update_group_name_key),true);
                    Log::info('updateGroupName ' . json_encode($updateGroupNameData));
                    $taskData = ['task_type' => 18,
                        'task_dict' => $updateGroupNameData
                    ];
                    $task_id = self:: getPullTaskDataId();
                    $dataList = [
                        'task_id' => strval($task_id),
                        'task_data' => $taskData
                    ];
                }
            }
            //机器人自动邀请好友进群
            if(!empty($dataList)){
                $invite_friend_group_key = $wxid . AutoGroupCreateModel::INVITE_FRIEND_KEY;
                $inviteFriendNum = Redis::llen($invite_friend_group_key);
                if ($inviteFriendNum > 0) {
                    $insertMember = json_decode(Redis::lpop($invite_friend_group_key),true);
                    Log::info('inviteFriendNum ' . json_encode($insertMember));
                    $taskData = ['task_type' => 3,
                        'task_dict' => $insertMember
                    ];
                    $task_id = self:: getPullTaskDataId();
                    $dataList = [
                        'task_id' => strval($task_id),
                        'task_data' => $taskData
                    ];
                }
            }
        }
        //这里的demo数据都是非真实数据，只是展示结构
        $resultTemplate['data'] = $dataList;
        self::return_success_result($resultTemplate);
    }
    /**
     * 第一版消息发送
     * @param $wxid
     * @param $data
     */
    public  static  function getTagMessageSendInfo($wxid,$data)
    {
        $wxmidBindId = CommunityModel::getWxidBindMid($wxid);
        Log::info('getTagMessageSendInfo    bind_mid'. json_encode($wxmidBindId));
        if(!empty($wxmidBindId)&&$wxmidBindId->bind_mid>0){
            //这里可以完成发送的消息链接  获取基础信息
            $TimePushDataBind = CommunityModel::getTimePushMsgDataTag($wxmidBindId->bind_mid, $wxid);
            Log::info('getTagMessageSendInfo    bind_mid  01'. json_encode($TimePushDataBind));
            if (!empty($TimePushDataBind)) {
                $pushDataBind = $TimePushDataBind;
                $random_str = $pushDataBind[0]->random_str;
                $redisNewKey = $wxid.$random_str.config('community.redisKeystr');
                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                Log::info('getTagMessageSendInfo    bind_mid  02'. json_encode([$tobeCount,$id,$pushDataBind]));
                if (empty($tobeCount)&&!empty($id)) {
                    $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->bind_mid, $wxid);
                }
            } else {
                $pushDataBind = CommunityModel::getPushMsgDataTag($wxmidBindId->bind_mid, $wxid);
            }
            if(!empty($pushDataBind)){
                Log::info('getTagMessageSendInfo    bind_mid  03'.$pushDataBind[0]->random_str. json_encode($pushDataBind));
                $random_str = $pushDataBind[0]->random_str;
                $redisNewKey = $wxid.$random_str.config('community.redisKeystr');
                $tobeCount =   count(Redis::lrange($redisNewKey,0,-1));
                $id =  CommunityModel::getWxSendReload($random_str,$wxid);  //当前微信号有没有发送
                $data=[];
                 Log::info('getTagMessageSendInfo    bind_mid  04'. json_encode([$redisNewKey,$tobeCount,$id]));
                if(empty($id)&&$tobeCount>0){
                    CommunityModel::insertGetId('send_reload',['random_str'=>$random_str,'wxid'=>$wxid]);
                    $data =json_decode(Redis::lpop($redisNewKey),true);
                    Log::info('sendToRoomlpop redis bind_mid' .$random_str. $tobeCount.json_encode($data));
                }else{
                    if($tobeCount>0 ){
                        $data =json_decode(Redis::lpop($redisNewKey),true);
                        Log::info('sendToRoomlpop redis bind_mid'.$wxid .$random_str. $tobeCount.json_encode($data));
                    }
                }
                Log::info('pushDataBind  empty  bind_mid 05'. json_encode($data));
                if(!empty($data)){
                    $count =  CommunityModel::getSendRoomCount($random_str,$data['room_wxid']);
                    if($count==0){
                        CommunityModel::insertGetId('send_room_reload',['random_str'=>$random_str,'link_type'=>$pushDataBind[0]->link_type,'wxid'=>$wxid,'room_wxid'=>$data['room_wxid'],'room_id'=>$data['room_id']]);
                        return ['task_type'=>1,'task_dict'=>['wxid_to'=>$data['room_wxid'],'msg_list'=>$data['msg_list'],'msg_interval'=>5]];
                    }else{
                        return [];
                    }
                }
            }else{
                Log::info('pushDataBind  empty  bind_mid 06');
            }
        }else{
            $TimePushDataMid = CommunityModel::getTimePushMsgDataTag($wxmidBindId->mid, $wxid);
            if (!empty($TimePushDataMid)) {
                $pushData = $TimePushDataMid;
                $random_str = $pushData[0]->random_str;
                $redisNewKey = $wxid. $random_str . config('community.redisKeystr');
                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                Log::info('sendToRoomlpop redis send mid  01 '. json_encode([$tobeCount,$id,$pushData]));
                if (!empty($id) && empty($tobeCount)) {
                    $pushDataMid = CommunityModel::getPushMsgDataTag($wxmidBindId->mid, $wxid);
                }
            } else {
                $pushDataMid = CommunityModel::getPushMsgDataTag($wxmidBindId->mid, $wxid);
            }
            Log::info('sendToRoomlpop redis send mid  02'. json_encode([$pushDataMid]));
            if(!empty($pushDataMid)){
                $random_str = $pushDataMid[0]->random_str;
                $redisNewKey = $wxid . $random_str . config('community.redisKeystr');;
                $tobeCount =   count(Redis::lrange($redisNewKey,0,-1));
                $id =  CommunityModel::getWxSendReload($random_str,$wxid);  //当前微信号有没有发送
                $data=[];
                Log::info('sendToRoomlpop redis send  mid 03'. json_encode([$random_str,$redisNewKey,$tobeCount,$id]));
                if(empty($id)&&$tobeCount>0){
                    CommunityModel::insertGetId('send_reload',['random_str'=>$random_str,'wxid'=>$wxid]);
                    $data =json_decode(Redis::lpop($redisNewKey),true);
                    Log::info('sendToRoomlpop redis' .$random_str. $tobeCount.json_encode($data));
                }else{
                    if($tobeCount>0 ){
                        $data =json_decode(Redis::lpop($redisNewKey),true);
                        Log::info('sendToRoomlpop redis'.$wxid .$random_str. $tobeCount.json_encode($data));
                    }
                }
                Log::info('sendToRoomlpop redis send  mid 04'. json_encode([$random_str,$data]));
                if(!empty($data)){
                    $count =  CommunityModel::getSendRoomCount($random_str,$data['room_wxid']);
                    if($count==0){
                        CommunityModel::insertGetId('send_room_reload',['random_str'=>$random_str,'link_type'=>$pushDataMid[0]->link_type,'wxid'=>$wxid,'room_wxid'=>$data['room_wxid'],'room_id'=>$data['room_id']]);
                        return ['task_type'=>1,'task_dict'=>['wxid_to'=>$data['room_wxid'],'msg_list'=>$data['msg_list'],'msg_interval'=>5]];
                    }else{
                        return [];
                    }

                }
            }else{
                Log::info('sendToRoomlpop redis send  empty');
            }
        }
        return [];
    }

    /**
     * 获取发送的消息
     * @param $wxid
     * @param $data
     */
    public  static  function  getMessageSendInfo($wxid,$data)
    {
        $id = [];
        $wxmidBindId = CommunityModel::getWxidBindMid($wxid);
            Log::info('send mid msg  ' . $wxid . '_' . $wxmidBindId->bind_mid . '_' . $wxmidBindId->bind_mid);
            $TimePushData = CommunityModel::getTimePushMsgData($wxmidBindId->mid, $wxid);
            Log::info('send mid msg 0' . json_encode($TimePushData));
            if (!empty($TimePushData)) {
                $pushData = $TimePushData;
                $random_str = $pushData[0]->random_str;
                $redisKey = $wxid . $random_str;
                $tobeCount = count(Redis::lrange($redisKey, 0, -1));
                $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                Log::info('send mid msg' .$random_str . $wxid. $tobeCount.json_encode([$id]));
                if (!empty($id) && empty($tobeCount)) {
                    $pushData = CommunityModel::getPushMsgData($wxmidBindId->mid, $wxid);
                }
            } else {
                $pushData = CommunityModel::getPushMsgData($wxmidBindId->mid, $wxid);
            }
         Log::info('send mid msg push data ' . $wxid.json_encode($pushData));
         if($wxmidBindId->bind_mid>0){
             //查询当前标签下的消息是否已发完
             if (empty($pushData)) {
                 //这里可以完成发送的消息链接  获取基础信息
                 $TimePushData = CommunityModel::getTimePushMsgData($wxmidBindId->bind_mid, $wxid);
                 Log::info('send bind_mid msg push data' . json_encode($TimePushData));
                 if (!empty($TimePushData)) {
                     $pushData = $TimePushData;
                     $random_str = $pushData[0]->random_str;
                     $redisKey = $wxid . $random_str;
                     $tobeCount = count(Redis::lrange($redisKey, 0, -1));
                     $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                     Log::info('send bind_mid msg push data' . $random_str . $tobeCount);
                     if (!empty($id) && empty($tobeCount)) {
                         $pushData = CommunityModel::getPushMsgData($wxmidBindId->bind_mid, $wxid);
                     }
                 } else {
                     $pushData = CommunityModel::getPushMsgData($wxmidBindId->bind_mid, $wxid);
                 }
                 Log::info('sendR  bind_mid send data' . $wxmidBindId->bind_mid . json_encode($pushData));
             } else {
                     $TimePushData = CommunityModel::getTimePushMsgData($wxmidBindId->bind_mid, $wxid);
                     Log::info('send bind_mid msg push data' . json_encode($TimePushData));
                     if (!empty($TimePushData)) {
                         $pushData = $TimePushData;
                         $random_str = $pushData[0]->random_str;
                         $redisKey = $wxid . $random_str;
                         $tobeCount = count(Redis::lrange($redisKey, 0, -1));
                         $id = CommunityModel::getWxSendReload($random_str, $wxid);  //当前微信号有没有发送
                         Log::info('send bind_mid msg push data' . $random_str . $tobeCount);
                         if (!empty($id) && empty($tobeCount)) {
                             $pushData = CommunityModel::getPushMsgData($wxmidBindId->bind_mid, $wxid);
                         }
                     } else {
                         if(empty($pushData)){
                             $pushData = CommunityModel::getPushMsgData($wxmidBindId->bind_mid, $wxid);
                         }
                     }
             }
         }

        if (!empty($pushData)) {
            Log::info('sendR  end  data'.$wxmidBindId->bind_mid.json_encode($pushData));
             //根据微信id  获取群主id
            $random_str = $pushData[0]->random_str;
            $redisKey  =   $wxid.$random_str;
            $tobeCount =   count(Redis::lrange($redisKey,0,-1));
            $id =  CommunityModel::getWxSendReload($random_str,$wxid);  //当前微信号有没有发送
            $data=array();
            $is=0;
            if(empty($id)){
                    $roomMidData = GroupActivationModel::getGroupOwerIdCodeNumCopy($wxid);
                    foreach($roomMidData as  $room_data){
                        $rpush =  ['room_wxid'=>$room_data->room_wxid,'codeNumber'=>empty($room_data->code_number)?config('community.codeNumber'):$room_data->code_number,'room_id'=>$room_data->id,'qzmid'=>$room_data->mid];
                        Log::info('insert send  To  Room' .$random_str.$wxid.json_encode($rpush));
                        if(!empty($rpush)){
                            $is =  Redis::rpush($redisKey,json_encode($rpush));
                            Log::info('sendToRoom' .$random_str.$wxid.$is. json_encode($rpush));
                        }
                    }
                if($is){
                    CommunityModel::insertGetId('send_reload',['random_str'=>$random_str,'wxid'=>$wxid]);
                }
                $data =json_decode(Redis::lpop($redisKey),true);
                Log::info('sendToRoomlpop' .$random_str. $tobeCount.json_encode($data));
            }else{
                if($tobeCount>0 ){
                   $data =json_decode(Redis::lpop($redisKey),true);
                    Log::info('sendToRoomlpop'.$wxid .$random_str. $tobeCount.json_encode($data));
                }
            }
            if(!empty($data)){
                //如果是文案  单个去发
                $msg_list = [];
                $more_product_text = "";
                foreach ($pushData as $vmsg) {
                    if (($vmsg->type == 1&&!empty($vmsg->content))||($vmsg->type == 6&&!empty($vmsg->content))) {
                        $shortUrl='';
                        if(!empty($vmsg->short_url)){
                            $keyParseData = parse_url($vmsg->short_url);
                            $shareCode = substr($keyParseData['path'], 1, 6);
                            $longData = YanModel::getProductLongUrl($shareCode);
                            Log::info('sendToRoom short_url '.json_encode($longData));
                            if(!empty($longData)){
                                $parseData = parse_url($longData);
                                if($parseData['path']=='/page/JD.html'){//京东联盟
                                    $longInfoUrl = explode('?', $longData);
                                    $queryData = self::convertUrlQuery($longInfoUrl[1]);
                                    $queryData['codeNumber']= $data['codeNumber'];
                                    $queryData['order_from']= 1;
                                    $queryData['room_id'] = isset($data['room_id']) ? $data['room_id']: 0;
                                    $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                                    $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                                }else if($parseData['path']=='/page/PDD.html'){
                                    $longInfoUrl = explode('?', $longData);
                                    $queryData = self::convertUrlQuery($longInfoUrl[1]);
                                    $queryData['codeNumber'] = $data['codeNumber'];
                                    $queryData['order_from']= 1;
                                    $queryData['room_id'] = isset($data['room_id']) ? $data['room_id']: 0;
                                    $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                                    $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                                }else{
                                    $longInfoUrl = explode('?', $longData);
                                    $queryData = self::convertUrlQuery($longInfoUrl[1]);
                                    $queryData['codeNumber']= $data['codeNumber'];
                                    $queryData['room_id'] = isset($data['room_id']) ? $data['room_id']: 0;
                                    $queryData['share_from']= 3;
                                    $queryData['robot_send']= 1;
                                    $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                                    $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                                }
                            }
                            if(empty($shortUrl)){
                                CommunityModel::updateSendInfo(['random_str'=>$vmsg->random_str,'is_send'=>0]);
                                break;
                            }
                        }
                        if(empty($vmsg->content)&&empty($shortUrl)){
                            continue;
                        }
                        if($pushData[0]->is_more_product==1){

                            $more_product_text = $more_product_text.'
'.str_replace('?', '', $vmsg->content).$shortUrl;
                        }else{

                            $msg_list[] =[
                                'msg_type'=>1,
                                'msg'=> str_replace('?', '', $vmsg->content).$shortUrl,
                            ];

                        }

                    }else if($vmsg->type == 2&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>3,
                            'msg'=>$vmsg->content,
                        ];
                    }else if($vmsg->type ==3&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>43,
                            'video_url'=>$vmsg->content,
                        ];
                    }else if($vmsg->type ==4&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $shortUrl='';
                        $keyParseData = parse_url($vmsg->content);
                        $shareCode = substr($keyParseData['path'], 1, 6);
                        $longData = YanModel::getProductLongUrl($shareCode);
                        Log::info('sendToRoom short_url '.json_encode($longData));
                        if(!empty($longData)){
                            $parseData = parse_url($longData);
                            if($parseData['path']=='/page/JD.html'){//京东联盟
                                $longInfoUrl = explode('?', $longData);
                                $queryData = self::convertUrlQuery($longInfoUrl[1]);
                                $queryData['codeNumber']= $data['codeNumber'];
                                $queryData['order_from']= 1;
                                $queryData['room_id'] = isset($data['room_id']) ? $data['room_id']: 0;
                                $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                                $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                            }else if($parseData['path']=='/page/PDD.html'){
                                $longInfoUrl = explode('?', $longData);
                                $queryData = self::convertUrlQuery($longInfoUrl[1]);
                                $queryData['codeNumber'] = $data['codeNumber'];
                                $queryData['order_from']= 1;
                                $queryData['room_id'] = isset($data['room_id']) ? $data['room_id']: 0;
                                $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                                $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                            }else{
                                $longInfoUrl = explode('?', $longData);
                                $queryData = self::convertUrlQuery($longInfoUrl[1]);
                                $queryData['codeNumber']= $data['codeNumber'];
                                $queryData['room_id'] = isset($data['room_id']) ? $data['room_id']: 0;
                                $queryData['share_from']= 3;
                                $queryData['robot_send']= 1;
                                $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                                $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                            }
                        }
                           if(!empty($shortUrl)){
                               $msg_list[] =[
                                   'msg_type'=>1,
                                   'msg'=>'👉 \r\n'.$shortUrl,
                               ];
                           }
                    }else if($vmsg->type ==5&&isset($data['qzmid'])>0&&$data['qzmid']>0&&$vmsg->cid>0){
                        if($vmsg->typeid==5){
                            $link_url = "https://single.yuetao.vip/coupons/detailActivity.html?codeNumber=".$data['codeNumber']."&cid=".$vmsg->cid."&parentId=".$data['qzmid']."&room_id=".$data['room_id']."&from=groupmessage3&isappinstalled=0";

                        }else{
                            $link_url = "https://single.yuetao.vip/coupons/detailGoods.html?codeNumber=".$data['codeNumber']."&cid=".$vmsg->cid."&parentId=".$data['qzmid']."&room_id=".$data['room_id']."&from=groupmessage4&isappinstalled=0";
                        }
                         $msg_list[] =[
                            'msg_type'=>49,
                            'link_url'=>$link_url,
                            'link_title'=>$vmsg->link_title,
                            'link_desc'=>$vmsg->link_desc,
                            'link_img_url'=>$vmsg->link_img_url,
                        ];
                    } else if($vmsg->type ==8&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>4901,
                            'raw_msg'=>$vmsg->content
                        ];
                    }
                }
                if($pushData[0]->is_more_product==1&&$more_product_text!=""){
                    $msg_list1=[
                        'msg_type'=>1,
                        'msg'=> $more_product_text,
                    ];

                    array_unshift($msg_list,$msg_list1);
                }
                Log::info('sendToRoom short_url '.json_encode($msg_list));
                  CommunityModel::insertGetId('send_room_reload',['random_str'=>$pushData[0]->random_str,'link_type'=>$pushData[0]->link_type,'wxid'=>$wxid,'room_wxid'=>$data['room_wxid'],'room_id'=>$data['room_id']]);
                return ['task_type'=>1,'task_dict'=>['wxid_to'=>$data['room_wxid'],'msg_list'=>$msg_list,'msg_interval'=>5]];
            }
        }
        return [];
    }

    /**
     * 根据购买者发送消息
     * @param $wxid
     * @param $data
     * @return array
     */
    public static function  getMessageSendInfoToMidRoom($wxid,$data)
    {
        $redisKey  =   'push'.$wxid.'msg';
        $tobeCount =   count(Redis::lrange($redisKey,0,-1));
        if($tobeCount==0){
            //获取定时消息
            $TimePushData = CommunityModel::getServiceTimePushMsgData($wxid);
            if (!empty($TimePushData)){
                $pushData = $TimePushData;
            }else{
                //获取即刻消息
                $pushData = CommunityModel::getServicePushMsgData($wxid);
            }
            foreach($pushData as $v){
                Redis::lpush($redisKey,json_encode($v));
            }
        }

        if($tobeCount>0){
            $data =json_decode(Redis::lpop($redisKey),true);
            $pushData = isset($data['msg_info'])?json_decode($data['msg_info'],true):[];
            $data['code_number'] =empty( $data['code_number']) ? config('community.codeNumber'): $data['code_number'];
            if(!empty($data)&&!empty($pushData)){
                //如果是文案  单个去发
                $msg_list = [];
                foreach ($pushData as $vmsg) {
                    if ($vmsg['type'] == 1&&!empty($vmsg['content'])) {
                        $shortUrl='';

                        if(!empty($vmsg['short_url']) && strpos($vmsg['short_url'],'yuetao.vip')>0){
                            $keyParseData = parse_url($vmsg['short_url']);
                            $shareCode = substr($keyParseData['path'], 1, 6);
                            $longData = YanModel::getProductLongUrl($shareCode);
                            if($longData!=0){
                                $longInfoUrl = explode('?',$longData);
                                $queryData =  self::convertUrlQuery($longInfoUrl[1]);
                                $queryData['codeNumber']= $data['code_number'];
                                $queryData['room_id']= isset($data['room_wxid'])?$data['room_wxid']:0;
                                $newUrl =   self::combineURL($longInfoUrl[0],$queryData);
                                $shortUrl = '👉'.CouponService::getShortURrl(['url'=>$newUrl]);
                            }else{
                                $shortUrl ='👉'.$vmsg['short_url'];
                            }

                        }
                        if(empty($vmsg['content'])&&empty($shortUrl)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>1,
                            'msg'=> str_replace('?', '', $vmsg['content']).$shortUrl,
                        ];
                    }else if($vmsg['type']  == 2&&!empty($vmsg['content'])){
                        if(empty($vmsg['content'])){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>3,
                            'msg'=>$vmsg['content'],
                        ];
                    }else if($vmsg['type']==3&&!empty($vmsg['content'])){
                        if(empty($vmsg['content'])){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>43,
                            'video_url'=>$vmsg['content'],
                        ];
                    }else if($vmsg['type']==4&&!empty($vmsg['content'])){
                        if(empty($vmsg['content'])){
                            continue;
                        }
                        $keyParseData = parse_url($vmsg['content']);
                        $shareCode = substr($keyParseData['path'], 1, 6);
                        $longData = YanModel::getProductLongUrl($shareCode);

                        if($longData!=0){
                            $longInfoUrl = explode('?',$longData);
                            $queryData =  self::convertUrlQuery($longInfoUrl[1]);
                            $queryData['codeNumber']= $data['code_number'];
                            $queryData['room_id']= isset($data['room_wxid'])?$data['room_wxid']:0;
                            $newUrl =   self::combineURL($longInfoUrl[0],$queryData);
                            $shortUrl = '👉'.CouponService::getShortURrl(['url'=>$newUrl]);
                        }else{
                            $shortUrl ='👉'.$vmsg['short_url'];
                        }
                        $msg_list[] =[
                            'msg_type'=>1,
                            'msg'=>'👉 \r\n'.$shortUrl,
                        ];
                    } else if($vmsg->type ==8&&!empty($vmsg->content)){
                        if(empty($vmsg->content)){
                            continue;
                        }
                        $msg_list[] =[
                            'msg_type'=>4901,
                            'raw_msg'=>$vmsg->content
                        ];
                    }
                }
                //更新消息状态  添加记录
                CommunityModel::changeServicePushMsgData($vmsg['random_str'],$wxid);
                CommunityModel::insertGetId('send_room_reload',['random_str'=>$pushData[0]->random_str,'wxid'=>$wxid,'room_wxid'=>$data['room_wxid'],'room_id'=>$data['room_id']]);
                return ['task_type'=>1,'task_dict'=>['wxid_to'=>$data['room_wxid'],'msg_list'=>$msg_list]];
            }
        }
    }






    /**
     * 返回数据
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function report_task_result($app, $wxid, $data)
    {
        $result = [
            'error_code' => 0,
            'error_reason' => '',
            'ack_type' => 'report_task_result_ack',
            'data' => [
                'task_id' => $data['task_id']
            ]
        ];
        self::return_success_result($result);
    }

    /**
     * (上报群成员详细信息)
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function report_room_member_info($appid, $wxid, $data)
    {
        if(!empty($data)&&!empty($data['room_data_list'])){
            CommunityModel::insertRoomMemberInfo('report_room_member_info',$appid, $wxid, $data);
        }
        $reply=[];
        $params = [
            'ack_type' => 'common_ack',
            'data' => ["reply_task_list"=>$reply]
        ];
        self::return_success_result($params);
    }
    /**
     * 上报成员信息变化
     * @param $appid
     * @param $wxid
     * @param $data
     * wehub     会每隔  10s 检查这些变化,然后上传这些变化的信息
     */
    public  static function report_contact_update($appid, $wxid, $data)
    {
        if (!empty($data)) {
            CommunityModel::reportContactUpdateData('report_contact_update', $appid, $wxid, $data);
        }
        $returnData=[];
        $params = [
            'ack_type' => 'common_ack',
            'data' => $returnData
        ];
        self::return_success_result($params);
    }

    /**
     * 参数拼接
     * @param $baseURL
     * @param $keysArr
     * @return string
     */
    public static  function combineURL($baseURL,$keysArr){
        $combined = $baseURL."?";
        $valueArr = array();

        foreach($keysArr as $key => $val){
            $valueArr[] = "$key=$val";
        }

        $keyStr = implode("&",$valueArr);
        $combined .= ($keyStr);

        return $combined;
    }
    /**
     * 解析path 参数
     * @param $query
     * @return array
     */
    public static function convertUrlQuery($query)
    {
        $queryParts = explode('&', $query);
        $params = array();
        foreach ($queryParts as $param) {
            $item = explode('=', $param);
            $params[$item[0]] = $item[1];
        }
        return $params;
    }

    /**
     * 群成员信息更新
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function  report_room_member_update($appid, $wxid, $data){

        if(!empty($data)){
            CommunityModel::report_room_member_update('report_room_member_update',$appid, $wxid, $data);
        }

        $params = [
            'ack_type' => 'common_ack',
            'data' => []
        ];
        self::return_success_result($params);
    }


    /**
     * 上报群成员变化
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function report_room_member_change($appid, $wxid, $data)
    {
        if(!empty($data)){
            CommunityModel::reportRoomMemberChange('report_room_member_change',$appid, $wxid, $data);
            if (isset($data['room_wxid']) && !empty($data['room_wxid']) && 1) {
                $room_wxid = $data['room_wxid'];
                $roomInfo = GroupModel::getRoomIdByRoomWxId($room_wxid);
                if ($roomInfo && $roomInfo['sign_switch'] == 1) {
                    $roomId = $roomInfo['id'] ?? 0;
                    $user_info = WhiteListWx::where('wxid', $wxid)->first();
                    if ($user_info) {
                        $platform_identity = $user_info->platform_identity;
                        $reply = [];
                        if (isset($data['flag']) && $data['flag'] == 0) {
                            $insertData = [];
                            $insertData ['data_info'] = json_encode($data);
                            CommunityModel::insertTableData('report_msg', $insertData);
                        }
                        if (isset($data['flag']) && in_array($data['flag'], [0, 1]) && !empty($roomId)) {
                            if ($data['flag'] == 1) {
                                foreach ($data['increase_member_list'] as $v) {
                                    $inviteInsertData = [];
                                    $inviteInsertData['room_id'] = $roomId;
                                    $inviteInsertData['flag'] = $data['flag'];
                                    $inviteInsertData['inviter'] = $v['inviter'] ?? '';
                                    if (empty($inviteInsertData['inviter'])) {
                                        continue;
                                    }
                                    $inviteInsertData['member_wxid'] = $v['wxid'];
                                    $inviteInsertData['platform_identity'] = $platform_identity;
                                    $inviteInsertData['room_wxid'] = $room_wxid;
                                    $reply = GroupSignService::insertGroupInviteRecord($inviteInsertData, $user_info);
                                }
                            } else {
                                foreach ($data['wxid_list'] as $v) {
                                    $inviteInsertData = [];
                                    $inviteInsertData['room_id'] = $roomId;
                                    $inviteInsertData['flag'] = $data['flag'];
                                    $inviteInsertData['member_wxid'] = $v;
                                    $inviteWhere = [];
                                    $inviteWhere['romm_id'] = $inviteInsertData['room_id'];
                                    $inviteWhere['wxid'] = $inviteInsertData['member_wxid'];
                                    $inviterWxid = GroupIntegralModel::getInviterByRoomWxIdAndWxid($inviteWhere);
                                    if (empty($inviterWxid)) {
                                        continue;
                                    }
                                    $inviteInsertData['inviter'] = $inviterWxid;

                                    $inviteInsertData['platform_identity'] = $platform_identity;
                                    $inviteInsertData['room_wxid'] = $room_wxid;
                                    $reply = GroupSignService::insertGroupInviteRecord($inviteInsertData, $user_info);
                                }
                            }
                        }
                        Log::info('群签到日志记录--' . json_encode($reply));
                        if (!empty($reply)) {
                            self::return_success_result([
                                'error_code' => 0,
                                'error_reason' => '',
                                'ack_type' => 'common_ack',
                                'data' => [
                                    'reply_task_list' => empty($reply) ? [] : [$reply]
                                ]
                            ]);
                        }
                    }
                }
            }
        }

        if($data['flag']==1){
            $platform_identity = GroupModel::getPlatformIdentity($wxid);
            $res =  CommunityModel::geNewFriendReturnData($data['room_wxid'], $platform_identity);
            $msgData ='';
            if (!empty($res)) {
                 $msgData = $res[0]->content;
             }
//             foreach($res as $return){
//                 $msgData.= $return->id.".".$return->title .' | ';
//             }
            //新好友加入时把数据更新
            if(!empty($msgData)){


                $at_list = [];
                $at_style = 0;

                // 内容中需要替换字符串为@用户时根据要求替换格式
                if(strpos($msgData,'【:user_name】')){
                    $at_list = [$wxid];
                    $msgData = str_replace('【:user_name】', '{$@}', $msgData);
                    $at_style = 1;//必须1
                }

                 $reply_task_list = [
                     'task_type' => 1,
                     'task_dict' => [
                         'wxid_to' => $data['room_wxid'],
                         'at_list' => $at_list,
                         'msg_list' => [
                             [
                                 'msg_type' => 1,
                                 'msg' =>trim($msgData,' | '),
                             ],
                         ],
                         'at_style' => $at_style
                     ]
                 ];
             }


            $reply_task_list['task_dict']['at_list']=$data['wxid_list'];
            $params = [
                'ack_type' => 'common_ack',
                'data' => [
                    'reply_task_list' => [$reply_task_list]
                ]
            ];

            self::return_success_result($params);
        }else{
            $params = [
                'ack_type' => 'common_ack',
                'data' => []
            ];
            self::return_success_result($params);
        }
    }

    /**
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function report_friend_removed($appid, $wxid, $data)
    {
        $params = [
            'ack_type' => 'common_ack',
            'data' => []
        ];
        self::return_success_result($params);
    }

    /**
     * 上报新成员的基本信息  入库
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static  function report_user_info($appid, $wxid, $data)
    {
        CommunityModel::inserReportUserInfo($appid, $wxid, $data);
        $params = [
            'ack_type' => 'common_ack',
            'data' => []
        ];
        self::return_success_result($params);
    }
    /**
     *当发现新的群时(比如被拉进了新的群或新建了新的群)
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function report_new_room($appid, $wxid, $data)
    {

        CommunityModel::userReportNewRoom($appid, $wxid, $data);

        $params = [
            'ack_type' => 'common_ack',
            'data' => []
        ];
        self::return_success_result($params);
    }
    /**
     *获取任务的参数值
     */
    public static function  getPullTaskDataId()
    {
        $pullTaskIdKey    = "pullTaskId"; //key
        $pullTaskId = Redis::get($pullTaskIdKey);
        if($pullTaskId)
        {
            $pullTaskId+=1;
        }else{
            $pullTaskId=1000;
        }
        Redis::set($pullTaskIdKey,$pullTaskId);
       return $pullTaskId;
    }

    /**
     * 好友请求自动通过
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function  report_friend_add_request($appid, $wxid, $data){
        //好友请求记录
        CommunityModel::addFriendRequest($appid, $wxid, $data);
        $params = [
            'ack_type' => 'common_ack',
            'data' => [
                'reply_task_list' => [['task_type'=>13,'task_dict'=>['v1'=>$data['v1'],'v2'=>$data['v2']]]]
            ]
        ];
        self::return_success_result($params);
    }

    public static function deleteUselessData()
    {
        $data = CommunityModel::getDeleteTableRoomId();
        foreach ($data as $v) {
            echo 1;
            $data1 = CommunityModel::getDeleteRoom($v->wxid, $v->room_wxid);
            if (!empty($data)) {
                foreach ($data1 as $v1) {
                    $res = CommunityModel::geRoomMemberApp($v1->romm_id,$v1->room_wxid);
                    if(empty($res)){
                       $res = CommunityModel::geRoomWeiXinMemberApp($v1->romm_id);
                       var_dump($res);
                    }
                }
            }
        }
        echo 2;
    }


    public static function updateCommonGroupNum($param){

        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] :100;
        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;
        $data = CommunityModel::getWeixinDataRobot($param);
        if($data){
            foreach($data as $v){
                  $res=  CommunityModel::updateRobotCommonGroupNum($v->wxid,$v->robot_wxid,$v->id);
                   var_dump([$page,$res]);
            }
        }else{
            echo 'end';
        }

    }

    /**
     * 获取数据并更新
     */
    public   static function  updateWhiteBindMidNum(){

        RobotModel::updateWhiteBindMidNum();

    }

    public static function updateRedisData($pargrams)
    {
        $keyParseData = parse_url('https://share.yuetao.vip/R3m2Iv');
        $shareCode = substr($keyParseData['path'], 1, 6);
        var_dump($shareCode);


        if($pargrams['type']==1){
            $redisNewKey = $pargrams['wxid'] . $pargrams['random_str'] . config('community.redisKeystr');
            $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                var_dump($redisNewKey);
            var_dump($tobeCount);
            if ($tobeCount) {
                $data = json_decode(Redis::lpop($redisNewKey), true);
                var_dump($tobeCount);
                var_dump($data);
            }
        }else if($pargrams['type']==2){
            $randomData = RobotModel::getSendDataOld();
            $getBindWxData = RobotModel::getBindWxData();
            if (!empty($randomData)) {
                foreach ($randomData as $v) {
                    foreach ($getBindWxData as $wx) {
                        $random_str = $v->random_str;
                        $redisNewKey = $wx->wxid . $random_str . config('community.redisKeystr');

                        $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                        var_dump($tobeCount . '-- ' . $wx->wxid.'--'.$random_str);
                        if ($tobeCount > 0) {
                            for ($i = 1; $i < $tobeCount; $i++) {
                                Redis::lpop($redisNewKey);
                            }
                            $tobeCount = 0;
                        }
                    }
                }
            } else {
                var_dump(22);
            }
        }else{
            var_dump(111);
        }

    }
    /**
     * 机器人被踢出群或主动退成某个群
     * @Author yangjiahua
     * @Date 2020-05-24
     * @param string appid
     * @param string wxid
     * @param array data
     */
    public static function reportRoomRemoved($appid, $wxid, $data)
    {
        $params = ['ack_type' => 'common_ack', 'data' => []];
        if(empty($data)){
            self::return_success_result($params);
        }
        $wxidRemoved = $data['wxid_removed'];
        $roomUserInfo = CommunityModel::getRoomMemberMId($appid, $wxidRemoved);

        if(empty($roomUserInfo) || (!empty($roomUserInfo) && !empty($roomUserInfo->is_delete))){
            self::return_success_result($params);
        }
        DB::beginTransaction();
        try{
            $upRoom = CommunityModel::updateMemberTableData(['wxid'=>$wxid,'id' => $roomUserInfo->id, 'is_delete' => 1, 'deleted_at' => date('Y-m-d H:i:s',time())]);
            if(empty($upRoom)){
                DB::rollback();
                return false;
            }

            $upRoomMemberInfo = CommunityModel::upRoomWxMemberInfo($wxidRemoved, ['is_delete' => 1, 'delete_time' => time()]);
            if(empty($upRoom)){
                DB::rollback();
                return false;
            }

            if($roomUserInfo->mid>0){
                MemberService::newSendIsRuleGroup($roomUserInfo->id);
            }




            DB::commit();
        } catch(Exception $e){
            DB::rollback();
            return false;
        }

        self::return_success_result($params);
    }

}