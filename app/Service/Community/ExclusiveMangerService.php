<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/7/14
 * Time: 19:40
 */

namespace App\Service\Community;

use Illuminate\Support\Facades\Redis;


use App\Model\Robot\ExclusiveMangerModel;
use App\Service\Source\LotteryDrawService;
use  App\Model\Source\SendInfoModel;

class ExclusiveMangerService
{
    /**
     * @param $params
     * @return mixed
     */
    public static function isEmptyData($params)
    {   
        if (empty($params['mid']) || empty($params['source'])) {
            return [];
        }else{

            if($params['source']!='2cb4a91bce63da9862f72559d4c463f5'){

                $channelData = ExclusiveMangerModel::isWhiteChannelData(['platform_identity' => $params['source'], 'exclusive_mid' => $params['mid']]);
                if(empty($channelData)){
                    return [];
                }
            }

            return 1;
        }
    }
    /**
     * 判断素材是
     * @param $params
     */
    public static function isDedicatedRobot($params)
    {
        $channelData = self::isEmptyData($params);
        if (!empty($channelData)) {
            $channelData = ExclusiveMangerModel::isSetChannelData(['platform_identity' => $params['source']]);
            if (!empty($channelData)) {
                $channelData = ExclusiveMangerModel::isWhiteChannelData(['platform_identity' => $params['source'], 'exclusive_mid' => $params['mid']]);
            }
        }
        return $channelData;
    }
    /**
     * 更新专属机器人状态
     * @param $params
     * @return array|int|mixed
     */
    public static function upSendStatusCon($params)
    {
        $channelData = self::isEmptyData($params);
        if (!empty($channelData)) {
            if (in_array($params['send_status'], array(0, 1))) {
                $channelData = ExclusiveMangerModel::isSetChannelData(['platform_identity' => $params['source']]);
                if (!empty($channelData)) {
                    $channelData = ExclusiveMangerModel::updateWhiteChannelData(['is_send_by_ass' => $params['send_status']], ['platform_identity' => $params['source'], 'exclusive_mid' => $params['mid']]);
                }
            }
        }
        return  $channelData ;
    }

    /**
     * 专属机器人素材发送
     * @param $params
     */
    public static function  addSendDataToRedis($params){

        $channelData = self::isEmptyData($params);
        //判断数量 是否已超过
        $noSendCount = SendInfoModel::isSetRandomStr(['exclusive_mid' =>$params['mid'], 'platform_identity' => $params['source'], 'deleted_at' => 0],time());
        if (!empty($channelData)&&$noSendCount<12) {
            $whereData = ExclusiveMangerModel::isWhiteChannelBindMidData(['platform_identity' => $params['source'], 'exclusive_mid' => $params['mid']]);
            if (!empty($whereData)) {
                $str = LotteryDrawService::getStr();
                $info = SendInfoModel::isSetRandomStr(['random_str' => $str, 'platform_identity' => $params['source'], 'deleted_at' => 0]);
                if (!empty($info)) {
                    $str.=$str.'A';
                }
                if (empty($info)) {
                    $created_at = date("Y-m-d H:i:s", time());
                    $commData = [];
                    $commData['mid'] = !empty($whereData[0]['bind_mid']) ? $whereData[0]['bind_mid'] : 0;
                    $commData['exclusive_mid'] = !empty($params['mid']) ? $params['mid'] : 0;
                    $commData['is_more_product'] = 0;
                    $commData['cid'] = 0;
                    $commData['link_title'] = '';
                    $commData['link_desc'] = '';
                    $commData['link_img_url'] = '';
                    $commData['coupon_num'] = 0;
                    $commData['link_type'] = 4;
                    $commData['is_send'] = 1;
                    $commData['status'] = 0;
                    $commData['random_str'] = $str;
                    $commData['created_at'] = $created_at;
                    $commData['room_ids'] = '';
                    $commData['msg_type'] = 1;
                    $commData['tag_ids'] = 0;
                    $commData['product_id'] = $params['product_id'];
                    $commData['platform_identity'] = $params['source'];
                    $commData['is_timing_send'] = !empty($params['send_time']) ? 1 : 0;
                    $commData['timing_send_time'] = !empty($params['send_time']) ?$params['send_time']: 0;
                    if (!empty($params['link_url'])) {
                        $add = $commData;
                        $add['type'] = 1;
                        $add['short_url'] = !empty($params['link_url']) ? $params['link_url'] : '';
                        $add['content'] = !empty($params['content']) ? $params['content'] : '';
                        $addData[0] = $add;
                    }
                    if (!empty($params['pic_url'])) {
                        $addPic = $commData;
                        $addPic['short_url'] = '';
                        $addPic['type'] = 2;
                        $addPic['content'] = $params['pic_url'];
                        $addData[1] =$addPic;
                    }
                    $result = SendInfoModel::addCommunitySendInfo($addData);
                    if ($result) {
                        $redisKeyRandom = config('community.insert_to_redis_random');
                        $wxAliasString = [];
                        if (!empty($whereData)) {
                            foreach ($whereData as $v) {
                                $wxAliasString[] = $v['wxid'];
                            }
                        }
                        $rediskeyData = ['mid' => $whereData[0]['bind_mid'], 'random_str' => $str, 'tag_ids' => 0, 'randomWxids' => $wxAliasString];
                        Redis::rpush($redisKeyRandom, json_encode($rediskeyData));
                    }
                    return ['flag'=>$str];
                }
            }
        }
         return  $channelData;
    }

    /**
     *
     * 素材发送 移除
     * @param $params
     * @return mixed
     */
    public static function delSendDataRedis($params)
    {
//         $res = curlPostCommon('mall.yuelvhui.test/stall/callbackAssistantPush', ['flag'=>'pP7ZvA','source'=>'7323ff1a23f0bf1cda41f690d4089353']);
        $channelData = self::isEmptyData($params);
        //判断数量 是否已超过
        $noSendCount = SendInfoModel::isSetRandomStr(['exclusive_mid' => $params['mid'], 'platform_identity' => $params['source'], 'random_str' => $params['random_str'], 'deleted_at' => 0]);
        if (!empty($channelData) && !empty($noSendCount)) {
            $upWhere = ['exclusive_mid' => $params['mid'], 'random_str' => $params['random_str']];
            if($params['send_time']>0){
                $upData = ['timing_send_time' => $params['send_time'],'updated_at'=>date("Y-m-d H:i:s", time()) ];//更新时间
            }else{
                $upData = ['deleted_at' => time()];//移除
            }
            $returnData = SendInfoModel::upCommunitySendInfo($upWhere, $upData);
            return ['flag' => $returnData];
        }
        return ['flag' => 0];
    }


    /**
     * 缓存删除
     * @param $params
     */
    public static  function  delRedisRandomStrCache($params)
    {
        $resData = SendInfoModel::delRandomStr($params);
        $returnData=[];
        if(!empty($resData)){
            foreach($resData as  $key=>$v1){
                $p = 0;
                do {
                    $d = Redis::SCAN($p, array('count'=>20,'MATCH'=>'*'.$v1['random_str'].'*')); # 遍历整个数据库，筛选出key存在f字母的出来
                    # $d = $redis->SSCAN('myset', $p, array('count'=>20,'MATCH'=>'*f*'));# 遍历指定的集合，筛选出key存在f字母的出来
                    # $d = $redis->HSCAN('hash1', 0, array('count'=>20,'MATCH'=>'k*'));# 遍历指定的哈希，筛选出key为k字母开头的数据出来
                    # $d = $redis->ZSCAN('page_rank', 0, array('count'=>20)); # 遍历整个有序集合，每次数量为20条
                    $p = $d[0];
                    //判断是否包含key
                    if(!empty($d[1])){
                        foreach($d[1] as $v2){
                            $allData  =   Redis::lrange($v2, 0, -1);
                            $returnData[$v2]=count($allData);
                            if(count($allData)>0){
                                for($i=0;$i<count($allData);$i++){
                                    Redis::lpop($v2);
                                }
                            }
                        }
                    }
                } while($p != 0);
            }

        }
        return $returnData;
    }


}