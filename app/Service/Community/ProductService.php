<?php

namespace App\Service\Community;

use App\Model\Source\SendInfoModel;
use App\Service\Robot\UserOperationLogService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Model\Community\ProductModel;
use  App\Service\VoucherBag\CouponService;

class ProductService
{
    private static $url = "https://gateway.yueshang.shop/private/v1";
    private static $ythost = "https://web.yuetao.vip/goodsDetailZY?robot_send=1&";//自营产品h5链接
    private static $h5host = "https://web.yuetao.vip/goodsDetailCPS?status=3&robot_send=1&";//京东
    private static $pddhost = "https://web.yuetao.vip/goodsDetailCPS?status=4&robot_send=1&";//拼多多
    private static $tbaokhost = "https://single.yuetao.vip/page/TB.html?robot_send=1&";
    private static $wphhost = "https://web.yuetao.vip/goodsDetailCPS?status=6&robot_send=1&";//唯品会
    private static $mthost = "https://web.yuetao.vip/cps?type=4&postType=2&robot_send=1&";//美团
    private static $ppzkhost = "https://single.yuetao.vip/page/third/party.html?type=6&robot_send=1&";
    private static $snhost = "https://single.yuetao.vip/suning-shopdetail.html?robot_send=1&";
    private static $cpsactivehost = "https://single.yuetao.vip/page/activity/NinetoNine.html?robot_send=1&";
    private static $huichang = "https://gateway.yuetao.vip/shop/v1/mall/hyk/getRobotConferenceHallList?robot_send=1&";
    private static $hykhchost = "https://web.yuetao.vip/clothesHall?robot_send=1&";//好衣库会场链接
    private static $jiudianList = "https://zhidingapi.zhiding365.com/hotel/v3/hotel/list";//酒店列表接口
    private static $zdForwardList = "https://zhidingapi.zhiding365.com/app/v1/hotel/hotelPresaleCoupon";//直订预售券列表
    private static $linJuTuanUrl = "https://neighbour.yuetao.vip";//邻居团接口地址


    /**
     * 悦淘自营商品
     * @param array $param
     * @param $memberInfo
     * @return array
     */
    public static function getProductList($param = [], $memberInfo)
    {
        //自营商品
        $limit = 10;
        $page = empty($param['page']) ? 1 : $param['page'];
        $offset = ($page - 1) * $limit;
//        $param['is_live'] = 0;//非直播商品
        $goodsData = ProductModel::getGoods($offset, $limit, $param);
        $goodsCount = ProductModel::getGoodsCount($param);

        $totalPage = ceil($goodsCount / $limit);
        $returnData = self::formateGoods($goodsData, $page, $limit);
//        $sku_num = ProductModel::getSkuNum($param);
//        $online_num = ProductModel::getOnlineNum($param);
//        $down_num = ProductModel::getDownNum($param);
        return [
            'code' => 200,
            'total' => $goodsCount,
            'page' => $page,
            'totalPage' => $totalPage,
//            'sku_num' => $sku_num,
//            'online_num' => $online_num,
//            'down_num' => $down_num,
            'data' => $returnData
        ];
    }


    /**
     * 获取自营一级分类
     * @return array
     */
    public static function getChannelOneCategoryList()
    {
        $one_list = ProductModel::getChannelOneCategoryList();

        $category = [];
        $data     = [];
        foreach ($one_list as $val) {
            $category['band_id']         = $val->id;
            $category['class_name']       = $val->name;
            $category['parent_id']  = $val->parent_id;
            $category['channel_id'] = $val->channel_id;
            $data[]                 = $category;
        }
        return [
            'code' => 200,
            'data' => $data,
            'msg'  => '查询成功'
        ];

    }



    /**
     * 格式化数据
     * @param array $param
     * @param $memberInfo
     * @return array
     */
    public static function formateGoods($goodsData = [], $page, $limit)
    {
        $returnData = [];

        foreach ($goodsData as $k => $good) {

            $sku = ProductModel::getNewSkuId($good->id);

            $spl_nickname = '';
            if ($good->spl_id) {
                $supl_info = ProductModel::getSupplierInfoById($good->spl_id);

                $spl_nickname = '';
                if (!empty($supl_info)) {
                    if (!empty($supl_info->supplier_nickname)) {
                        $spl_nickname = $supl_info->supplier_nickname;
                    } else {
                        if (!empty($supl_info->supplier_name)) {
                            $spl_nickname = $supl_info->supplier_name;
                        } else {
                            $spl_nickname = '';
                        }
                    }

                } else {
                    $spl_nickname = $spl_nickname;
                }

            }

            // $skuList = ProductModel::getNewSkus($good->id);

            // $skuLists = self::formateSku($skuList);

            $mall_distribution = self::getDistribution($good->distribution_id,$sku);

            $goods_img = ProductModel::getConverImage($good->id);

            $goodsSuggestImage = ProductModel::getNewSuggestImage($good->id);
            $returnData[] = [
                "num" => ($page - 1) * $limit + $k + 1,
                'gid' => $good->id,
                'middleProductId' => $good->middle_product_id,
                'activity_id'=>$good->act_id<1?0:$good->act_id,
                'vip_price' => empty($sku->vip_price) ? 0 : $sku->vip_price / 100,
                'buy_price' => empty($sku->buy_price) ? 0 : $sku->buy_price / 100,
                'original_price' => empty($sku->price) ? 0 : $sku->price / 100,
                'official_price' => empty($sku->official_price) ? 0 : $sku->official_price / 100,
                'product_id' => empty($sku->product_id) ? 0 : $sku->product_id,
                'sku_id' => $sku->skuId,
                'inventory' => $sku->inventory,
                'group_servant'=> $mall_distribution,
                'discount'=> $good->discount?$good->discount."%":0.00."%",
                // 'skuInfo' => $skuLists,
                'goods_name' => $good->name,
                'product_coupon' => $good->product_coupon,
                'product_discount' => $good->product_discount,
                'goods_status' => $good->state,
                'goods_type' => $good->activity_name,
                'spl_nickname' => $spl_nickname,
                'goods_img' => $goods_img,
                'goodsSuggestImage' => $goodsSuggestImage,
                'channelName' => self::getChannelNameById($good->channel_id),
                'is_check' => $good->is_check,
                'sale_num' => self::getSalePlatform($good->id),//自购省
            ];
        }


        return $returnData;
    }


    public static function getDistribution($id=0,$sku=[])
    {
        if($sku==[]||$id==0)
        {
            return 0;
        }

        $distribution = ProductModel::getDistributionInfo($id);

        $price =  empty($sku->vip_price) ? 0 : $sku->vip_price / 100;
        $buy_price =  empty($sku->buy_price) ? 0 : $sku->buy_price / 100;

        $distribution_money = ($price-$buy_price)*$distribution/100;

        return $distribution_money>0?$distribution_money:0;
    }


    /**
     * 格式化商品
     * @param array $skuData
     * @return array
     */
    public static function formateSku($skuData = [])
    {
        $returnSkuData = [];

        foreach ($skuData as $k => $sku) {

            $productSaleValueName = $sku->product_sale_value_name;
            $productSaleValueId = $sku->product_sale_value_id;
            $productPropertys = [];
            if (strpos($productSaleValueName, ';') !== false) {
                $saleValueArr = explode(';', trim($productSaleValueName, ';'));
                $length = count($saleValueArr);
                if (strpos($productSaleValueId, ',') !== false) {
                    $saleValueIds = explode(',', $productSaleValueId);
                } else {
                    $saleValueIds[] = $productSaleValueId;
                }
                for ($i = 0; $i < $length; $i++) {
                    $saleValueStr = $saleValueArr[$i];
                    $saleValue = trim(strstr($saleValueStr, ':'), ':');
                    $saleKey = strstr($saleValueStr, ':', true);
                    $saleValueId = $saleValueIds[$i];
                    $saleInfo = ProductModel::getSaleInfo($saleValueId);
                    $productPropertys[] = [
                        'pName' => $saleKey,
                        'vName' => $saleValue,
                        'skuImgUrl' => empty($saleInfo->sale_image_path) ? '' : $saleInfo->sale_image_path
                    ];
                }

            } else {
                $saleValue = trim(strstr($productSaleValueName, ':'), ':');
                $saleKey = strstr($productSaleValueName, ':', true);
                $saleValueId = $productSaleValueId;
                $saleInfo = ProductModel::getSaleInfo($saleValueId);
                $productPropertys[] = [
                    'pName' => $saleKey,
                    'vName' => $saleValue,
                    'skuImgUrl' => empty($saleInfo->sale_image_path) ? '' : $saleInfo->sale_image_path
                ];
            }


            $returnSkuData[] = [
                'vip_price' => empty($sku->vip_price) ? 0 : $sku->vip_price / 100,
                'buy_price' => empty($sku->buy_price) ? 0 : $sku->buy_price / 100,
                'original_price' => empty($sku->price) ? 0 : $sku->price / 100,
                'official_price' => empty($sku->official_price) ? 0 : $sku->official_price / 100,
                'product_id' => empty($sku->product_id) ? 0 : $sku->product_id,
                'sku_id' => $sku->skuId,
                'inventory' => $sku->inventory,
                'product_propertys' => $productPropertys
            ];
        }

        return $returnSkuData;
    }

    /**
     * 根据渠道id获取渠道名
     * @param int $id
     * @return string
     */
    public static function getChannelNameById($id = 0)
    {
        $channelname = '';
        switch ($id) {
            case 0:  // 寺库
                $channelname = '自营';
                break;
            case 1:  // 寺库
                $channelname = '寺库';
                break;
            case 2: //  网易严选
                $channelname = '网易严选';
                break;
            case 3: //  京东
                $channelname = '京东';
                break;
            case 6: //  考拉
                $channelname = '考拉';
                break;
            case 7: //  行云
                $channelname = '行云';
                break;
            case 8: //  当当
                $channelname = '当当';
                break;
            default:
                $channelname = '自营';
                break;
        }
        return $channelname;
    }

    /**
     * post数据请求
     * @param array $data
     * @param string $postUrl
     * @return bool|string
     * @throws \Exception
     */
    public static function getCurlPostData($data = array(), $postUrl = '')
    {
        $result = curlPostCommon(self::$url . $postUrl, $data);
        return $result;
    }

    /**
     * @param $gid
     * @return mixed
     */
    public static function getSalePlatform($gid)
    {
        $saleNum = ProductModel::getSaleNum($gid);
        return $saleNum;
    }


    /*********************************************************************京东返利******************************************************************************************/
    /**
     * 京东联盟页的一级类目
     * @return array
     */
    public static function getJdClassData()
    {
        $outSideClassIcon = self::getCurlPostData(array(), '/open/v2/jd/getCategory');
        $restClassData = json_decode($outSideClassIcon, true);
        $icon = array();
        if (!empty($restClassData['data']) && $restClassData['code'] == 200) {
            foreach ($restClassData['data'] as $list) {
                $icon[] = array('band_id' => $list['cid1'], 'class_name' => $list['name']);
            }
        }
        return $icon;
    }

    /**
     * 商品
     * @return array
     * @throws \Exception
     */
    public static function getJdEliteId()
    {
        $icon []= ['model_id' => 0, 'model_name' => '全部'];
        $outSideIndexIcon = self::getCurlPostData(array(), '/open/v2/jd/getActivityList');
        $restData = json_decode($outSideIndexIcon, true);
        if (!empty($restData['data']) && $restData['code'] == 200) {
            foreach ($restData['data'] as $list) {
                $icon[] = array('model_id' => $list['eliteId'], 'model_name' => $list['activity_name']);
            }
            $icon[] = ['model_id' => 129, 'model_name' => '高佣榜单'];
        }
        return $icon;
    }

    /**
     * @param $param
     * @param $member
     * @return array
     */
    public static function getJdClassAllData($param, $member)
    {
        $classData = self:: getJdClassData();
        $classEliteData = self:: getJdEliteId();
        return ['classData' => $classData, 'classEliteData' => $classEliteData];
    }

    /**
     * 根据  分类 获取数据
     * @param $classId
     * @param $pageNum
     * @param $pageSize
     */
    public static function getJdOusideGoodsByCategoryId($classId, $eliteId, $pageNum = 1, $pageSize = 10,$sorttype,$sort)
    {

        if($sorttype==1){
            $search['sortName'] = 'commission';
        } else if($sorttype==3){
            $search['sortName'] = 'price';
        }
            $search['sort']  = $sort==1? 'asc':'desc';
        //条件搜索
        if (!empty($classId)) {
            $search['cid1'] = $classId;//分类列表
            $goodsList = self::getGoodsListByClassId($search, $pageNum, $pageSize);
        } else if (!empty($eliteId)) {
            $search['eliteId'] = $eliteId;//频道列表
            $search['pageIndex'] = $pageNum;
            $search['pageSize'] = $pageSize;
            $goodsList = self::getGoodsListByEliteId($search);
        }
        return [
            'code' => 200,
            'page' => $pageNum,
            'totalPage' => $pageSize,
            'data' => $goodsList,
            'total' => 1000,
        ];

    }

    /**频道列表
     * @param $options
     * @return array
     * @throws \Exception
     */
    public static function getGoodsListByEliteId($options)
    {
        $result = self::getCurlPostData($options, '/open/v2/jd/getGoodsList');
        $restEliteIdData = json_decode($result, true);
        $goodsList = array();
        if (!empty($restEliteIdData) && $restEliteIdData['code'] == 200 && !empty($restEliteIdData['data']['data'])) {
            foreach ($restEliteIdData['data']['data'] as $k => $list) {
                $priceData = self::goodsPrice($list['priceInfo']['price'], empty($list['priceInfo']['lowestPrice']) ? '' : $list['priceInfo']['lowestPrice'], empty($list['priceInfo']['lowestCouponPrice']) ? '' : $list['priceInfo']['lowestCouponPrice']);
                $priceSave = self::getJdEarnricePrice($list['commissionInfo']);
                $goodsSuggestImage = [];
                foreach ($list['imageInfo']['imageList'] as $v) {
                    $goodsSuggestImage[] = $v['url'];
                }
                $goodsList[] = array(
                    "num" => ($options['pageIndex'] - 1) * $options['pageSize'] + $k + 1,
                    "goods_name" => $list['skuName'],
                    "gid" => $list['skuId'], //商品id
                    "sku_id" => $list['skuId'], //商品skuId
                    "middleProductId" => 0, //商品middleProductId
                    "channelName" => '京东返利', //商品channelName
//                    "sale_num" => self::getSaleNum($list['inOrderCount30Days']),//销售量
                    "original_price" => $priceData['original_price'],              //无线价
                    "vip_price" => $priceData['jd_price'],//券后价lowestCouponPrice
                    'goodsSuggestImage' => $goodsSuggestImage,//主图
                    'is_check' => 3,//主图
                    'goods_img' => $goodsSuggestImage[0],//主图
                    'group_servant' => $priceSave['share_price'],//分享赚
                    'self_price' => $priceSave['self_price'],//自购省
                    'sale_num' => self::getSalePlatform($list['skuId']),//自购省
                );
            }
        }
        return $goodsList;
    }

    /**
     * 获取品类 数据
     * @param $eliteId
     * @param $pageNum
     * @param $pageSize
     */
    public static function getGoodsListByClassId($options, $pageNum, $pageSize)
    {
        if (empty($options)) return array();
        $options['pageIndex'] = $pageNum;
        $options['pageSize'] = $pageSize;
        $result = self::getCurlPostData($options, '/open/v2/jd/getCategoryGoodsList');
        $classData = json_decode($result, true);
        $goodsList = array();
        if (!empty($classData) && $classData['code'] == 200 && !empty($classData['data'])) {
            foreach ($classData['data'] as $k => $list) {
                $priceData = self::goodsPrice($list['priceInfo']['price'], empty($list['priceInfo']['lowestPrice']) ? '' : $list['priceInfo']['lowestPrice'], empty($list['priceInfo']['lowestCouponPrice']) ? '' : $list['priceInfo']['lowestCouponPrice']);
                $priceSave = self::getJdEarnricePrice($list['commissionInfo']);
                $goodsSuggestImage = [];
                foreach ($list['imageInfo']['imageList'] as $v) {
                    $goodsSuggestImage[] = $v['url'];
                }
                $goodsList[] = array(
                    "num" => ($pageNum - 1) * $pageSize + $k + 1,
                    "goods_name" => $list['skuName'],
                    "gid" => $list['skuId'], //商品id
                    "sku_id" => $list['skuId'], //商品skuId
                    "middleProductId" => 0, //商品middleProductId
                    "channelName" => '京东返利', //商品channelName
//                    "sale_num" => self::getSaleNum($list['inOrderCount30Days']),//销售量
                    "original_price" => $priceData['original_price'],              //无线价
                    "vip_price" => $priceData['jd_price'],//券后价lowestCouponPrice
                    'goodsSuggestImage' => $goodsSuggestImage,//主图
                    'is_check' => 3,//主图
                    'goods_img' => $goodsSuggestImage[0],//主图
                    'group_servant' => $priceSave['share_price'],//分享赚
                    'self_price' => $priceSave['self_price'],//自购省
                    'sale_num' => self::getSalePlatform($list['skuId']),//自购省
                );

            }
        }
        return $goodsList;

    }

    /**
     * 优惠券数据
     * @param $couponData
     */
    public static function getGoodCouponData($origion_price, $couponData)
    {
        $last_names = array_column($couponData, 'discount');
        array_multisort($last_names, SORT_DESC, $couponData);
        $returnData = array();
        foreach ($couponData as $list) {
            if ($origion_price >= $list['quota']) {
                $returnData = array(
                    'discount' => $couponData[0]['discount'],           //拳金额
                    'link' => $couponData[0]['link'], //券图片
                    'use_start_time' => date('Y-m-d', substr($couponData[0]['useStartTime'], 0, 10)), //领取开始时间
                    'use_end_time' => date('Y-m-d', substr($couponData[0]['useEndTime'], 0, 10)), //领取结束时间
                );
            }
        }
        return $returnData;
    }

    /**
     * 赚的钱数
     * 预估赚
     * 分享赚
     * @param $commission
     * @param $couponCommission
     */
    public static function getJdEarnricePrice($commissionData, $id = 103)
    {
        $tk = array_key_exists('couponCommission', $commissionData) ? $commissionData['couponCommission'] : $commissionData['commission'];
        if ($tk > 0) {
            $distributionInfo = ProductModel::getDistributions($id);
            $info['share_price'] = !empty($distributionInfo->distribution_first) ? round(sprintf("%.2f", $tk) * ($distributionInfo->distribution_first / 100), 2) : ''; // 分享赚
            $info['self_price'] = !empty($distributionInfo->distribution_self) ? round(sprintf("%.2f", $tk) * ($distributionInfo->distribution_self / 100), 2) : '';
        } else {
            $info['share_price'] = 0; // 分享赚
            $info['self_price'] = 0;
        }
        $info['tk'] = $tk;
        return $info;
    }


    /**
     * 获取保留的位数
     * @param $num
     */
    public static function getSaleNum($num)
    {
        return $num > 10000 ? sprintf("%.2f", $num / 10000) . '万' : $num;
    }

    /**
     * 价格展示
     * @param $price              无线价格
     * @param $lowestPrice        最低价格
     * @param $lowestCouponPrice  最低价后的优惠券价
     */
    public static function goodsPrice($price, $lowestPrice = '', $lowestCouponPrice = '')
    {
        $original_price = $jd_price = $price;
        if (!empty($lowestCouponPrice)) {
            $jd_price = $lowestCouponPrice;
            if (!empty($lowestPrice)) {
                $original_price = $lowestPrice;
            }
        } else {
            if (!empty($lowestPrice)) {
                $jd_price = $lowestPrice;
            }
        }
        return array('jd_price' => $jd_price, 'original_price' => $original_price);
    }

    /*********************************************************************拼多多******************************************************************************************/
    /**
     * 拼多多选品
     * @param array $param
     * @param $memberInfo
     */
    public static function getPddClassData()
    {
        $outSideClassIcon = self::getCurlPostData(array(), '/pdd/v1/queryGoodsCategoryList');
        $restClassData = json_decode($outSideClassIcon, true);
        $icon = [];
        if (!empty($restClassData['data']) && $restClassData['code'] == 200) {
            foreach ($restClassData['data'] as $list) {
                $icon[] = array('band_id' => $list['opt_id'], 'class_name' => $list['opt_name']);
            }
        }
        return $icon;
    }

    /**
     * 拼多多分类
     * @return array
     */
    public static function getPddClassAllData()
    {
        $classData = self:: getPddClassData();
        $classEliteData = [];
        return ['classData' => $classData, 'classEliteData' => $classEliteData];
    }

    /**
     * 分类列表破数据
     * @param $classId
     * @param $pageNum
     * @param $pageSize
     * @throws \Exception
     */
    public static function getPddOusideGoodsByCategoryId($classId, $pageNum, $pageSize)
    {

        $outSideClassIcon = self::getCurlPostData(['id' => $classId, 'page' => $pageNum, 'page_size' => $pageSize], '/pdd/v1/queryGoodsList');
        $restClassData = json_decode($outSideClassIcon, true);

        $doc = [];
        foreach ($restClassData['data'] as $k => $item) {
            $docField = $item['info'];
            if (empty($docField)) {
                break;
            }
            $array = [
                "num" => ($pageNum - 1) * $pageSize + $k + 1,
                'goods_name' => $docField['goods_name'],
                'gid' => $docField['goods_id'], //商品id
                'sku_id' => $docField['goods_id'], //skuid
                'middleProductId' => 0,
                'channelName' => '拼多多',
                'goodsSuggestImage' => [$docField['goods_image_url']],
                'goods_img' => $docField['goods_thumbnail_url'],
                'original_price' => $docField['min_normal_price'],
                'vip_price' => $docField['jh_price'],
                "is_check" => 4,
                "group_servant" =>$docField['tk'],
                "sale_num" => self::getSalePlatform($docField['goods_id']),
                'couponFee' => empty($item['coupon'][0]) ? '' : $item['coupon'][0]['coupon_min_order_amount']
            ];
            $doc[] = $array;
        }
        return $doc;
    }






    /*********************************************************************京东 拼多多检索****************************************************************************************/
    /**
     * 拼多多排序
     */
    public static function pinDuoSort($params)
    {
        switch ($params['sorttype']) {
            case 1:
                return 0;
            case 2:
                return $params['sort'] == 0 ? 6 : 5;
            case 3:
                return $params['sort'] == 0 ? 4 : 3;
        }
    }

    /**
     * 接口请求
     * @param $para
     * @return array
     */
    public static function getMuchMoreDataNew($para)
    {
        $url = self::$url . "/pdd/v1/querySearch";
        $params = [
            "coupon" => $para['coupon'],
            "keyword" => $para['keyword'],
            "sort_type" => $para['sortType'],
            "page" => $para['page'],
            "page_size" => $para['page_size'],
        ];
        if (!empty($para['range_list'])) {
            $params['range_list'] = $para['range_list'];
        }
        $data = self::HttpCurl($url, $params);
        if ($data == false) {
            return [];
        }
        return $data;
    }

    public static function getPddGroupSer($money)
    {
        $distributionInfo = ProductModel::getDistributions(102);
        if (!empty($distributionInfo)) {
            $promotion = round(($money / 100) * ($distributionInfo->distribution_self / 100), 2);
        } else {
            $promotion = round($money / 100, 2);
        }
        return $promotion;
    }


    /**
     * 格式化拼多多数据
     * @param $data
     * @return array
     */
    public static function pinDuoDataHandle($data, $page, $pageSize)
    {
        if (empty($data)) {
            return [];
        }
        $doc = [];
        foreach ($data['data'] as $k => $item) {
            $docField = $item['info'];
            if (empty($docField)) {
                break;
            }
            $array = [
                "num" => ($page - 1) * $pageSize + $k + 1,
                'goods_name' => $docField['goods_name'],
                'gid' => $docField['goods_id'], //商品id
                'sku_id' => $docField['goods_id'], //skuid
                'middleProductId' => 0,
                'channelName' => '拼多多',
                'goodsSuggestImage' => [$docField['goods_image_url']],
                'goods_img' => $docField['goods_thumbnail_url'],
                'original_price' => $docField['min_normal_price'],
                'vip_price' => $docField['jh_price'],
                "is_check" => 4,
                "sale_num" => self::getSalePlatform($docField['goods_id']),
                'couponFee' => empty($item['coupon'][0]) ? '' : $item['coupon'][0]['coupon_min_order_amount'],
                'group_servant'=> $docField['tk']
            ];
            $doc[] = $array;
        }
        return $doc;
    }

    /**
     * 京东联盟商品
     * @param $options
     * @return mixed
     */
    public static function JDOutsideDataSearchData($options)
    {
        $url = self::$url . "/open/v2/jd/getSearchGoods";
        $data = self::HttpCurl($url, $options);
        return $data;
    }

    /**
     * @param $params
     * @return int|string
     * sort       0 降序  1 升序
     * sorttype   1  默认  2 销量  3 价格
     *
     */
    public static function jdOutsideSort($params)
    {
        switch ($params['sorttype']) {
            case 1:
                return  $params['sort'] == 1 ? array('sortName' => 'commission', 'sort' => 'asc'): array('sortName' => 'commission', 'sort' => 'desc');
            case 2:
                return $params['sort'] == 1? array('sortName' => 'inOrderCount30Days', 'sort' => 'asc') : array('sortName' => 'inOrderCount30Days', 'sort' => 'desc');
            case 3:
                return $params['sort'] == 1 ? array('sortName' => 'price', 'sort' => 'asc') : array('sortName' => 'price', 'sort' => 'desc');
            default:
                return array('sortName' => 'inOrderComm30Days', 'sort' => 'desc');
        }
    }


    //京东联盟搜索
    public static function JDOutsideDataHandle($classData, $page = 1, $pageSize)
    {
        $doc = [];
        if (!empty($classData) && $classData['code'] == 200 && !empty($classData['data'])) {
            foreach ($classData['data'] as $k => $list) {
                $priceData = self::goodsPrice($list['priceInfo']['price'], empty($list['priceInfo']['lowestPrice']) ? '' : $list['priceInfo']['lowestPrice'], empty($list['priceInfo']['lowestCouponPrice']) ? '' : $list['priceInfo']['lowestCouponPrice']);
                $priceSave = self::getJdEarnricePrice($list['commissionInfo']);
                $goodsSuggestImage = [];
                foreach ($list['imageInfo']['imageList'] as $v) {
                    $goodsSuggestImage[] = $v['url'];
                }
                $array = [
                    "num" => ($page - 1) * $pageSize + $k + 1,
                    "goods_name" => $list['skuName'],
                    "gid" => $list['skuId'], //商品id
                    "sku_id" => $list['skuId'], //商品skuId
                    "middleProductId" => 0, //商品middleProductId
                    "channelName" => '京东返利', //商品channelName
                    "original_price" => $priceData['original_price'],              //无线价
                    "vip_price" => $priceData['jd_price'],//券后价lowestCouponPrice
                    'goodsSuggestImage' => $goodsSuggestImage,//主图
                    'is_check' => 3,//主图
                    'goods_img' => $goodsSuggestImage[0],//主图
                    'group_servant' => $priceSave['share_price'],//分享赚
                    'self_price' => $priceSave['self_price'],//自购省
                    'sale_num' => self::getSalePlatform($list['skuId']),//自购省
                ];
                $doc[] = $array;
            }
        }
        return $doc;
    }


    public static function gettaobkOusideGoodsByCategoryId($classData, $page = 1, $pageSize)
    {
        
        $postData = array('id' => $classData,'page'=>$page,'pageSize'=>$pageSize,'mid'=>605640);

        $result = json_decode(self::getCurlPostData($postData, '/taobk/v1/queryFavoritesItem'),true);

        $doc = [];

        if ($result&&$result['code']==200&&!empty($result['data'])) {

            $doc = self::taobkOutsideDataHandle($result['data'],$page, $pageSize);
        }        

        return [
            'code' => 200,
            'page' => $page,
            'totalPage' => $pageSize,
            'data' => $doc,
            'total' => 1000,
        ];

    }



    //格式化淘宝数据
    public static function taobkOutsideDataHandle($data, $page, $pageSize)
    {

        if (empty($data)) {
            return [];
        }
        $doc = [];
        foreach ($data as $k => $item) {

            $coupon_share_url = isset($item['coupon_share_url'])?substr('https:'.$item['coupon_share_url'],0,strpos('https:'.$item['coupon_share_url'], 'relationId=')):substr($item['coupon_click_url'],0,strpos($item['coupon_click_url'], 'relationId='));

            $array = [
                "num" => ($page - 1) * $pageSize + $k + 1,
                'goods_name' => $item['title'],
                'gid' => isset($item['item_id'])?$item['item_id']:$item['num_iid'], //商品id
                'sku_id' => 0, //skuid
                'middleProductId' => 0,
                'channelName' => '淘宝',
                'goodsSuggestImage' => [$item['pict_url']],
                'goods_img' => $item['pict_url'],
                'original_price' => $item['zk_final_price'],
                'vip_price' => $item['rush_price'],
                "is_check" => 4,
                "sale_num" => self::getSalePlatform($item['seller_id']),
                'group_servant'=> $item['rebate_price'],
                'coupon_share_url'=> $coupon_share_url
            ];
            $doc[] = $array;
        }
        return $doc;
    }

    public static function getTaobkClassAllData($data,$member)
    {


        $outSideClassIcon = self::getCurlPostData(array('mid'=>605640), '/taobk/v2/queryIndex');
        $restClassData = json_decode($outSideClassIcon, true);

        $icon = [];
        if (!empty($restClassData['data']) && $restClassData['code'] == 200) {
            foreach ($restClassData['data']['menu'] as $list) {
                if($list['favorites_id']!=3787){
                    $icon[] = array('band_id' => $list['favorites_id'], 'class_name' => $list['favorites_title']);
                } 
            }
        }
        $classEliteData = [];
        return ['classData' => $icon];
    }


    /**
     * 唯品会产品分类
     * @return array
     */
    public static function getWphCategory($type)
    {
        if ($type == 1) {
            $goodsData = [
                ['band_id' => 1, 'class_name' => '精品推荐'],
            ];
        } else if ($type == 2) {
            $goodsData = [
                ['band_id' => 1, 'class_name' => '爆款推荐'],
                ['band_id' => 0, 'class_name' => '高佣推荐'],
            ];
        }
       return ['classData' => $goodsData, 'classEliteData' => $goodsData];
    }


    /**
     * 苏宁产品分类
     * @return array
     */
    public static function getSuNingCategory()
    {


        $outSideClassIcon = self::getCurlPostData(array(), '/suning/v1/commodity/inverstmentcategoryid');
        $restClassData = json_decode($outSideClassIcon, true);
        $icon = [];
        if (!empty($restClassData['data']['categoryList']) && $restClassData['code'] == 200) {
            foreach ($restClassData['data']['categoryList'] as $list) {
                $icon[] = array('band_id' => $list['id'], 'class_name' => $list['name']);
            }
        }
        $classEliteData = [];
        return ['classData' => $icon];
    }

    public static function getwphkOusideGoodsByCategoryId($param, $page = 1, $pageSize)
    {
        
        $postData = [

            'keyword' => $param['goodName']?$param['goodName']:'唯品会',
            'page' => $page,
            'pageSize' => $pageSize
        ];


        if(isset($param['sorttype'])&&$param['sorttype']>0){

            switch ($param['sorttype']) {
                case 1:
                    $postData['fieldName'] = 'COMMISSION';
                    break;
                case 3:
                    $postData['fieldName'] = 'PRICE';
                    break;
                default:
                    # code...
                    break;
            }

            $postData['order'] = $param['sort'];
        }


        if (!empty($param['minPrice']) && !empty($param['maxPrice'])) {
            $postData['priceStart'] = $param['minPrice'];
            $postData['priceEnd'] = $param['maxPrice'];
        }

        
        $result = json_decode(self::getCurlPostData($postData, '/vip/v1/getGoodsList'),true);

        $doc = [];

        if ($result&&$result['code']==200&&!empty($result['data'])) {

            $doc = self::wphOutsideDataHandle($result['data']['goodsInfoList'],$page, $pageSize);
        }        

        return [
            'code' => 200,
            'page' => $page,
            'totalPage' => $pageSize,
            'data' => $doc,
            'total' => 1000,
        ];

    }






    //格式化唯品会站外数据
    public static function wphOutsideDataHandle($data, $page, $pageSize)
    {

        if (empty($data)) {
            return [];
        }

        $doc = [];
        foreach ($data as $k => $item) {
            $array = [
                "num" => ($page - 1) * $pageSize + $k + 1,
                'goods_name' => $item['goodsName'],
                'gid' => $item['goodsId'], //商品id
                'sku_id' => 0, //skuid
                'middleProductId' => 0,
                'channelName' => '唯品会',
                'goodsSuggestImage' => [$item['goodsThumbUrl']],
                'goods_img' => $item['goodsThumbUrl'],
                'original_price' => $item['marketPrice'],
                'vip_price' => $item['vipPrice'],
                "is_check" => 4,
                "sale_num" => self::getSalePlatform($item['goodsId']),
                'couponFee' => 0,
                'group_servant'=> !empty($item['commission']) ? round(sprintf("%.2f", $item['commission']) * 100/399, 2) : 0,
            ];
            $doc[] = $array;
        }
        return $doc;
    }



    public static function getsuningOusideGoodsList($param, $page = 1, $pageSize)
    {
        
        $params = [
            'categoryId' => $param['bandId']!=0?$param['bandId']:'C1',
            'pageIndex' => $page,
            'size' => $pageSize,
            'couponMark' => 1
        ];

        $doc = [];

        $result = json_decode(self::getCurlPostData($params, '/suning/v1/commodity/inverstmentcommodity'),true);

        if ($result&&$result['code']==200&&!empty($result['data'])) {

            $doc = self::suningOutsideDataHandle($result['data'],$page,$pageSize);
        }

        return [
            'code' => 200,
            'page' => $page,
            'totalPage' => $pageSize,
            'data' => $doc,
            'total' => 1000,
        ];

    }


    //格式化苏宁站外数据
    public static function suningOutsideDataHandle($data, $page, $pageSize)
    {

        if (empty($data)) {
            return [];
        }

        $doc = [];
        foreach ($data as $k => $item) {

            $array = [
                "num" => ($page - 1) * $pageSize + $k + 1,
                'goods_name' => $item['commodityInfo']['commodityName'],
                'gid' => $item['commodityInfo']['commodityCode'], //商品id
                'supplier_code' => $item['commodityInfo']['supplierCode'], //供应商id
                'sku_id' => 0, //skuid
                'middleProductId' => 0,
                'channelName' => '苏宁易购',
                'goodsSuggestImage' =>empty($item['commodityInfo']['pictureUrl'])?[]:array_column($item['commodityInfo']['pictureUrl'],'picUrl'),//[$item['commodityInfo']['pictureUrl']['picUrl']],
                'goods_img' => empty($item['commodityInfo']['pictureUrl'])?'':$item['commodityInfo']['pictureUrl'][0]['picUrl'],
                'original_price' => $item['commodityInfo']['snPrice'],
                'vip_price' => $item['commodityInfo']['commodityPrice'],
                "is_check" => 4,
                "sale_num" => self::getSalePlatform($item['commodityInfo']['commodityCode']),
                'couponFee' => 0,
                'group_servant'=> !empty($item['commodityInfo']['rate']) ? round(sprintf("%.2f", $item['commodityInfo']['snPrice']*$item['commodityInfo']['rate']/100) * 100/399, 2) : 0,
            ];
            $doc[] = $array;
        }
        return $doc;       
    }



    /**
     * 接口请求
     * @param array $para
     * @param $memberInfo
     * @return array
     */
    public static function querySearchNewEndSearchData($para = [], $memberInfo)
    {
        $doc = [];
        $para['sorttype'] = isset($para['sorttype']) ? $para['sorttype'] : 1;
        switch ($para['type']) {
            case 6: // 拼多多搜索
                $sortType =0;
                if( $para['sorttype']==1){
                    $sortType = $para['sort']==1?13:14;
                }
                switch ($para['sorttype']) {
                    case 1:
                        $sortType = $para['sort'] ==1?13:14;
                        break;
                    case 3:
                        $sortType = $para['sort'] ==1?3:4;
                        break;

                    default:
                        # code...
                        break;
                }
                $params = [
                    'coupon' => 1,
                    'keyword' => $para['goodName'],
                    'sortType' => $sortType,
                    'page' => $para['page'],
                    'page_size' => empty($para['pageSize']) ? 10 : $para['pageSize']
                ];
                if (!empty($para['minPrice']) && !empty($para['maxPrice'])) {
                    $params['range_list'] = [
                        'range_id' => 1,
                        'range_from' => $para['minPrice'] * 100,
                        'range_to' => $para['maxPrice'] * 100
                    ];
                }
                $doc = self::pinDuoDataHandle(self::getMuchMoreDataNew($params), $para['page'], $para['pageSize']);
                break;
            case 7: // 淘宝搜索
                $sort = 3;
                switch ($para['sorttype']) {
                    case 1:
                        $sort = $para['sort'] ==1?7:8;
                        break;
                    case 3:
                        $sort = $para['sort'] ==1?3:4;
                        break;

                    default:
                        # code...
                        break;
                }
                $params = [
                    'sort' => $sort,
                    'mid' => 605640,
                    'keyword' => $para['goodName'],
                    'page' => $para['page'],
                    'page_size' => empty($para['pageSize']) ? 10 : $para['pageSize']
                ];

                if (!empty($para['minPrice']) && !empty($para['maxPrice'])) {
                    $params['start_price'] = $para['minPrice'];
                    $params['end_price'] = $para['maxPrice'];
                }
                $result = json_decode(self::getCurlPostData($params, '/taobk/v1/querySearchAll'),true);

                if ($result&&$result['code']==200&&!empty($result['data'])) {
                    $doc = self::taobkOutsideDataHandle($result['data'],$para['page'], $para['pageSize']);
                }
                break;
            case 5://京东联盟
                $params = [
                    "keyword" => $para['goodName'],
                    "pageIndex" => $para['page'],
                    "pageSize" => empty($para['pageSize']) ? 10 : $para['pageSize'],
                ];
                $sortData = self::jdOutsideSort($para);
                $params['sortName'] = $sortData['sortName'];
                $params['sort'] = $sortData['sort'];
                if (!empty($para['minPrice']) && !empty($para['maxPrice'])) {
                    $params['pricefrom'] = $para['minPrice'];
                    $params['priceto'] = $para['maxPrice'];
                }
                if( $para['bandId']>0){
                    $params['cid1'] = $para['bandId'];
                }
                $doc = self::JDOutsideDataHandle(self::JDOutsideDataSearchData($params), $para['page'], $para['pageSize']);
                break;
            case 8:
                $params = [
                    'channelType' => isset($para['channelType'])?$para['channelType']:0,
                    'page' => $para['page'],
                    'pageSize' => empty($para['pageSize']) ? 10 : $para['pageSize'],
                    'priceStart' => $para['minPrice']?$para['minPrice']:'',  //价格区间 开始价格
                    'priceEnd' => $para['maxPrice']?$para['maxPrice']:'',  //价格区间 结束价格
                ];

                if(isset($para['sorttype'])&&$para['sorttype']>0){

                    switch ($para['sorttype']) {
                        case 1:
                            $params['fieldName'] = 'COMMISSION';
                            break;
                        case 3:
                            $params['fieldName'] = 'PRICE';
                            break;
                        default:
                            # code...
                            break;
                    }

                    $params['order'] = $para['sort'];
                }


                $result = json_decode(self::getCurlPostData($params, '/vip/v1/pushGoodsList'),true);

                if ($result&&$result['code']==200&&!empty($result['data'])) {

                    $doc = self::wphOutsideDataHandle($result['data']['goodsInfoList'],$para['page'], $para['pageSize']);
                }
                break;
            case 9:
                $postData = [

                    'keyword' => $para['goodName']?$para['goodName']:'',
                    'startPrice' => $para['minPrice']?$para['minPrice']:'',  //价格区间 开始价格
                    'endPrice' => $para['maxPrice']?$para['maxPrice']:'',  //价格区间 结束价格
                    'pageIndex' => $para['page'],
                    'size' => empty($para['pageSize']) ? 10 : ($para['pageSize']==20?10:$para['pageSize']),
                ];


                if(isset($para['sorttype'])&&$para['sorttype']==1){

                    $postData['sortType'] = 6;
                }

                if(isset($para['sorttype'])&&$para['sorttype']==3){

                    $postData['sortType'] = $para['sort']==1?4:3;
                }

                $result = json_decode(self::getCurlPostData($postData, '/suning/v1/commodity/searchcommodity'),true);

                $doc = [];

                if ($result&&$result['code']==200&&!empty($result['data'])) {

                    $doc = self::suningOutsideDataHandle($result['data'],$para['page'], $para['pageSize']);
                }        

                break;
            case 11:

                if($memberInfo['platform_identity']=="7323ff1a23f0bf1cda41f690d4089353"){

                    $doc =[
                        ['num'=>1,'goods_name'=>'品牌折扣券','sku_id'=>6,'gid'=>6,'middleProductId'=>0,'channelName'=>'悦淘',
                            'goodsSuggestImage'=>[],'goods_img'=>'','original_price'=>0,'vip_price'=>0,'is_check'=>6,
                            'sale_num'=> ProductModel::getSalelifeCount(6),'couponFee'=>0,'group_servant'=>0,'desc'=>'更多本地美食，更多大牌折扣，尽在悦淘优享！'
                        ],
                        ['num'=>2,'goods_name'=>'美团','sku_id'=>5,'gid'=>5,'middleProductId'=>0,'channelName'=>'悦淘',
                            'goodsSuggestImage'=>[],'goods_img'=>'','original_price'=>0,'vip_price'=>0,'is_check'=>5,
                            'sale_num'=> ProductModel::getSalelifeCount(5),'couponFee'=>0,'group_servant'=>0,'desc'=>'更多本地美食，更多大牌折扣，尽在悦淘优享！'
                        ],
                        // ['num'=>3,'goods_name'=>'肯德基','sku_id'=>7,'gid'=>7,'middleProductId'=>0,'channelName'=>'肯德基',
                        //     'goodsSuggestImage'=>[],'goods_img'=>'','original_price'=>'','vip_price'=>'','is_check'=>7,
                        //     'sale_num'=> ProductModel::getSalelifeCount(7),'couponFee'=>'','group_servant'=>''
                        // ],
                    ];

                }else if($memberInfo['platform_identity']=="75f712af51d952af3ab4c591213dea13"){

                    $doc =[
                        ['num'=>1,'goods_name'=>'视频充值','sku_id'=>7,'gid'=>7,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhuiUp7w3CPiZg1606312422.png','original_price'=>0,'vip_price'=>0,'is_check'=>7,
                            'sale_num'=> ProductModel::getSalelifeCount(7),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>2,'goods_name'=>'话费充值','sku_id'=>8,'gid'=>8,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhuiEmdsGXCIyw1606312492.png','original_price'=>0,'vip_price'=>0,'is_check'=>8,
                            'sale_num'=> ProductModel::getSalelifeCount(8),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>3,'goods_name'=>'加油','sku_id'=>9,'gid'=>9,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhui7JA0SRLWdg1606312517.png','original_price'=>0,'vip_price'=>0,'is_check'=>9,
                            'sale_num'=> ProductModel::getSalelifeCount(9),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>4,'goods_name'=>'品牌折扣券','sku_id'=>6,'gid'=>6,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhui7isUrhxtxY1606312549.png','original_price'=>0,'vip_price'=>0,'is_check'=>6,
                            'sale_num'=> ProductModel::getSalelifeCount(6),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>5,'goods_name'=>'转盘活动','sku_id'=>10,'gid'=>10,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhui9twixC0ww01606312578.png','original_price'=>0,'vip_price'=>0,'is_check'=>10,
                            'sale_num'=> ProductModel::getSalelifeCount(10),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>6,'goods_name'=>'转盘活动','sku_id'=>11,'gid'=>11,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhuiXfP9RbYY2k1606312628.png','original_price'=>0,'vip_price'=>0,'is_check'=>11,
                            'sale_num'=> ProductModel::getSalelifeCount(11),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>7,'goods_name'=>'电影票','sku_id'=>12,'gid'=>12,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhuiJnuPFBAu8k1606312666.png','original_price'=>0,'vip_price'=>0,'is_check'=>12,
                            'sale_num'=> ProductModel::getSalelifeCount(12),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                        ['num'=>8,'goods_name'=>'白拿活动','sku_id'=>13,'gid'=>13,'middleProductId'=>0,'channelName'=>'直订',
                            'goodsSuggestImage'=>[],'goods_img'=>'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-25/21/yuelvhui0ZG5udVYAo1606312693.png','original_price'=>0,'vip_price'=>0,'is_check'=>13,
                            'sale_num'=> ProductModel::getSalelifeCount(13),'couponFee'=>0,'group_servant'=>0,'desc'=>'直订酒店享底价，京东好物更省钱，本地生活更优惠！'
                        ],
                    ];                    
                }

                break;
            case 12:
                //白拿商品curlPostCommon(self::$url . $postUrl, $data)
                $result = json_decode(curlPostCommon('https://gateway.yuetao.vip/shop/v1/mall/V3/robotActivityList',
                    ['id'=>55,'categoryId'=>1,'page'=>$para['page'],'pageSize'=> $para['pageSize'],
                        'name'=> $para['goodName'],
                        'startPrice' => $para['minPrice']?$para['minPrice']:'',  //价格区间 开始价格
                        'endPrice' => $para['maxPrice']?$para['maxPrice']:'']),true);
                $doc = self::baiNaHandle($result);
                break;

            case 10:
                //@todo 苏宁
                break;

            case 13://cps混合活动
                
                $result = ProductModel::getCpsActiveList($para);

                if(empty($result)){

                    return [];
                }

                foreach ($result as $k => $value) {

                    $array = [
                        "num" => ($para['page'] - 1) * $para['pageSize'] + $k + 1,
                        'goods_name' => $value['activity_name'],
                        'gid' => $value['id'], //商品id
                        'supplier_code' => 0, //供应商id
                        'sku_id' => 0, //skuid
                        'middleProductId' => 0,
                        'channelName' => 'cps混合活动',
                        'goodsSuggestImage' =>$value['activity_imgs'],
                        'goods_img' => $value['activity_imgs'],
                        'original_price' => 0,
                        'vip_price' => 0,
                        "is_check" => 4,
                        "sale_num" => 0,
                        'couponFee' => 0,
                        'group_servant'=> 0,
                    ];
                    $doc[] = $array;
                    # code...
                }

                break;
            case 14:
                
                $data = [
                    'page' => $para['page'],
                    'pageSize' => empty($para['pageSize']) ? 10 : $para['pageSize'],
                ];
                if(isset($para['goodName'])&&!empty($para['goodName'])){

                   $data['conferenceHallName'] = $para['goodName'];
                }

                $result = json_decode(curlPostCommon(self::$huichang, $data),true);

                if(empty($result)){

                    return [];
                }

                foreach ($result['data'] as $k => $value) {

                    $array = [
                        "num" => ($para['page'] - 1) * $para['pageSize'] + $k + 1,
                        'goods_name' => $value['brandName'],
                        'gid' => $value['id'], //商品id
                        'supplier_code' => 0, //供应商id
                        'sku_id' => 0, //skuid
                        'middleProductId' => 0,
                        'channelName' => '好衣库',
                        'goodsSuggestImage' =>$value['brandLogo'],
                        'goods_img' => $value['brandLogo'],
                        'original_price' => 0,
                        'vip_price' => 0,
                        "is_check" => 4,
                        "sale_num" => 0,
                        "introduction" => $value['introduction'],
                        'couponFee' => 0,
                        'group_servant'=> 0,
                    ];
                    $doc[] = $array;
                    # code...
                }

                 break;
            case 15: //酒店
                # code...
                $data = [
                    'page' => $para['page'],
                    'pageSize' => empty($para['pageSize']) ? 10 : $para['pageSize'],
                    'cityName'=> $para['cityName'],
                    'checkIn'=>$para['startTime'],
                    'checkOut'=>$para['endTime']
                ];
                if(isset($para['goodName'])&&!empty($para['goodName'])){

                   $data['keyword'] = $para['goodName'];
                }


                if(isset($para['sorttype'])&&$para['sorttype']==3){

                    $data['sortType'] = $para['sort']==2?3:4;
                }

                $result = json_decode(curlPostCommon(self::$jiudianList, $data),true);

                if(empty($result)||empty($result['list'])){

                    return [];
                }

                foreach ($result['list'] as $k => $value) {

                    if(isset($value['itemType'])&&$value['itemType']=='adv'){

                        foreach ($value['list'] as $key => $v) {
                            # code...
                            $array = [
                                    "num" => ($para['page'] - 1) * $para['pageSize'] + $k + 1,
                                    'goods_name' => isset($v['hotelName'])?$v['hotelName']:'',
                                    'gid' =>isset($v['hotelId'])?$v['hotelId']:0, //商品id
                                    'supplier_code' => 0, //供应商id
                                    'sku_id' => 0, //skuid
                                    'middleProductId' => 0,
                                    'channelName' => '广告',
                                    'goodsSuggestImage' => isset($v['image'])?$v['image']:'',
                                    'goods_img' =>  isset($v['image'])?$v['image']:'',
                                    'original_price' =>isset($v['originPrice'])?$v['originPrice']:0,
                                    'vip_price' =>isset($v['price'])?$v['price']:0,
                                    "is_check" => 4,
                                    "sale_num" => 0,
                                    "introduction" =>isset($v['desc'])?$v['desc']:'',
                                    'couponFee' => 0,
                                    'group_servant'=> 0,
                                    'hotelType'=> isset($v['hotelType'])?$v['hotelType']:'',
                                    'address' => isset($v['city'])?$v['city']:''
                                ];

                                $doc[] = $array;
                                # code...

                        }

                    }else{

                        $array = [
                                "num" => ($para['page'] - 1) * $para['pageSize'] + $k + 1,
                                'goods_name' => isset($value['hotelName'])?$value['hotelName']:'',
                                'gid' =>isset($value['hotelId'])?$value['hotelId']:0, //商品id
                                'supplier_code' => 0, //供应商id
                                'sku_id' => 0, //skuid
                                'middleProductId' => 0,
                                'channelName' => isset($value['channel_name'])?$value['channel_name']:'',
                                'goodsSuggestImage' => isset($value['image'])?$value['image']:'',
                                'goods_img' =>  isset($value['image'])?$value['image']:'',
                                'original_price' =>isset($value['price'])?$value['price']:0,
                                'vip_price' =>isset($value['vipPrice'])?$value['vipPrice']:0,
                                "is_check" => 4,
                                "sale_num" => 0,
                                "introduction" =>isset($value['hotel_star_name'])?$value['hotel_star_name']:'',
                                'couponFee' => 0,
                                'group_servant'=> 0,
                                'hotelType'=> isset($value['hotelType'])?$value['hotelType']:'',
                                'address' => $value['city'].$value['district'].$value['address']
                            ];
                            $doc[] = $array;
                            # code...

                    }
    
                }

                break;
            case 16: //本地生活活动页面
                # code...
                $data = [
                    'page' => $para['page'],
                    'pageSize' => empty($para['pageSize']) ? 10 : $para['pageSize'],
                    'activeName'=> $para['goodName']
                ];

                $result = ProductModel::getActiveList($data);

                if(!empty($result)){

                    foreach ($result as $k => $v) {
                        
                        $banner = json_decode($v['active_banner'],true);

                        $array = [
                            "num" => ($para['page'] - 1) * $para['pageSize'] + $k + 1,
                            'goods_name' =>  $v['active_name'],
                            'gid' => $v['id'], //商品id
                            'supplier_code' => 0, //供应商id
                            'sku_id' => 0, //skuid
                            'middleProductId' => 0,
                            'channelName' => '本地生活活动',
                            'goodsSuggestImage' =>$banner[0]['img'],
                            'goods_img' => $banner[0]['img'],
                            'original_price' => 0,
                            'vip_price' => 0,
                            "is_check" => 0,
                            "sale_num" => 0,
                            "introduction" => "",
                            'couponFee' => 0,
                            'group_servant'=> 0,
                        ];
                        $doc[] = $array;

                    }
                }else{

                    return [];
                }

                break;
            case 17: //直订 预售券
                # code...
                $data = [
                    'page' => $para['page'],
                    'pageSize' => empty($para['pageSize']) ? 10 : $para['pageSize']
                ];

                $result = json_decode(curlGet(self::$zdForwardList, $data),true);
                if(empty($result) || empty($result['data']) || !isset($result['data']['list'])){
                    return [];
                }

                $result = $result['data']['list'];
                if(!empty($result)){

                    foreach ($result as $k => $v) {
                        $array = [
                            "num" => ($para['page'] - 1) * $para['pageSize'] + $k + 1,
                            'goods_name' =>  $v['dealer_name']." ".$v['name'],
                            'gid' => $v['id'], //商品id
                            'supplier_code' => 0, //供应商id
                            'sku_id' => 0, //skuid
                            'middleProductId' => 0,
                            'channelName' => '预售券',
                            'goodsSuggestImage' =>$v['product_picture'],
                            'goods_img' => $v['product_picture'],
                            'original_price' => $v['market_price'],
                            'vip_price' => $v['sale_price'],
                            "is_check" => 0,
                            "sale_num" => 0,
                            "introduction" => $v['brief_description'],
                            'couponFee' => 0,
                            'group_servant'=> 0,
                        ];
                        $doc[] = $array;

                    }
                }else{

                    return [];
                }

                break;

        }
        $total = 1000;
        if ($para['type'] == 11) {
            $total = count($doc);
        }
        return [
            'code' => 200,
            'page' => intval($para['page']),
            'totalPage' => intval($para['pageSize']),
            'data' => $doc,
            'total' => $total,
        ];
    }


    /*********************************************************************邻居团******************************************************************************************/

    /**
     * 邻居团自营商品列表接口请求
     * @param array $para
     * @param $memberInfo
     * @return array
     */
    public static function querySearchLinJuTuanSearchData($param=[],$memberInfo)
    {

        //自营商品
        $limit = 10;
        $page = empty($param['page']) ? 1 : $param['page'];
        $offset = ($page - 1) * $limit;

        $param['type'] = 1;//固定值
        $param['page'] = $page;
        $param['pageSize'] = $param['pageSize'];
        if(isset($param['bandId'])&&!empty($param['bandId'])){
            $param['sid'] = $param['bandId'];
        }
        if(!empty($param['minPrice'])&&!empty($param['maxPrice'])){
            $param['price_between'] = $param['minPrice'].",".$param['maxPrice'];
        }
        if(!empty($param['minPrice'])&&!empty($param['maxPrice'])){
            $param['price_between'] = $param['minPrice'].",".$param['maxPrice'];
        }
        if(isset($param['minCommission'])&&!empty($param['minCommission'])&&isset($param['maxCommission'])&&!empty($param['maxCommission'])){
            $param['bonus_number_between'] = $param['minCommission'].",".$param['maxCommission'];
        }
        if(isset($param['source_id'])&&!empty($param['source_id'])){
            $param['source_id'] = $param['source_id'];
        }
        if(isset($param['goodName'])&&!empty($param['goodName'])){
            $param['keyword'] = $param['goodName'];
        }
        $result = json_decode(curlGet(self::$linJuTuanUrl."/api/tasks/lst_task" , $param),true);

        $returnData = [];

        if(isset($result['data'])&&!empty($result['data'])){

            foreach ($result['data'] as $key => $v) {

                $array = [
                    "num" => ($param['page'] - 1) * $param['pageSize'] + $key + 1,
                    'goods_name' =>  $v['store_name'],
                    'gid' => $v['id'], //商品id
                    'supplier_code' => 0, //供应商id
                    'sku_id' => 0, //skuid
                    'middleProductId' => 0,
                    'channelName' => '自营',
                    'goodsSuggestImage' =>$v['image'],
                    'goods_img' => $v['image'],
                    'original_price' => $v['price'],
                    'vip_price' => $v['price'],
                    "is_check" => 0,
                    "sale_num" => $v['sales'],
                    "introduction" => '',
                    'couponFee' => 0,
                    'group_servant'=> $v['bonus_number'],
                ];

                $returnData[] = $array;  

            }

        }

        return [
            'code' => 200,
            'total' => 1000,
            'page' => $page,
            'totalPage' => 1000/$param['pageSize'],
            'data' => $returnData
        ];

    }



    /**请求方式
     * @param $url
     * @param string $data
     * @return mixed
     */
    private static function HttpCurl($url, $data = '')
    {
        $ch = curl_init();

        $data = json_encode($data, JSON_UNESCAPED_UNICODE);

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );

        // POST数据

        curl_setopt($ch, CURLOPT_POST, 1);

        // 把post的变量加上

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($output, true);

        return $result;
    }

    /*********************************************************************格式化白拿******************************************************************************************/
    public  static function  bainaName($cid){
        if(in_array($cid,[75,76,77,78])){
            $bainaName = [75=>'注册会员白拿',76=>'金卡会员白拿',77=>'白金会员白拿',78=>'黑金会员白拿'];
            return $bainaName[$cid];
        }
         return '白拿尚未分类';
    }


    public static function baiNaHandle($data)
    {
        $doc = [];
        $i=0;

        if ($data['code'] == 200) {
            foreach($data['data'] as $goodList){

                if(isset($goodList['goodsInfo'])){
                    foreach($goodList['goodsInfo'] as $info){

                        $i++;
                        $infoData =[
                          'num'=>$i,
                          'goods_name'=>$info['goodName'],
                          'gid'=>$info['goodId'],
                          'sku_id'=>$info['productSkuId'],
                          'middleProductId'=>0,
                          'channelName'=>self::bainaName($info['category_id']),
                          'original_price'=>$info['goodPrice'],
                          'vip_price'=>$info['vipPrice'],
                          'goodsSuggestImage'=>[$info['goodCover']],
                          'is_check'=>12,
                          'goods_img'=>$info['goodCover'],
                          'group_servant'=>$info['shareMoney'],
                          'sale_num'=>ProductModel::getSalelifeCount($info['goodId']),
                        ];
                        $doc[] =$infoData;
                    }
                }
            }
        }

        return $doc;
    }

    public static function addAutoSendData($param, $member)
    {
        $insertData = [];
        $insertData['product_type']           = $param['type'] ?? 0;
        $insertData['product_id']             = $param['gid'] ?? 0;
        $insertData['sku_id']                 = $param['sku_id'] ?? 0;
        $insertData['commission_rate']        = $param['group_servant'] ?? 0;
        $insertData['product_name']           = $param['goodName'] ?? '';
        $insertData['product_price']          = $param['product_price'] ?? '';
        $insertData['product_vipPrice']       = $param['product_vipPrice'] ?? '';
        $insertData['small_goods_img']        = $param['goods_img'] ?? 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-09-28/15/yuelvhuiXElxxY2cTb1601277146.png';
        $insertData['platform_identity']      = $member['platform_identity'];
        SendInfoModel::addAutoSendData($insertData);
    }



    public static function deleteRobotSendGood($memberInfo, $param = [], $status = '')
    {
        if (!isset($param['id']) || empty($param['id']))
        {
            return ['code' => 400, 'msg' => '列表ID不能为空'];
        }
        $update  = [
            'deleted_at' => time()
        ];
        $where = [
            'id' => $param['id']
        ];
        $result = SendInfoModel::updateRobotSendGoodStatus($update, $where);

        $changeId = $param['id'];
        if ($result !== false)
        {
            $logMessage = '删除社群商品';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($update), $logMessage, $changeId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    public static function updateRobotSendGoodStatus($memberInfo, $param = [], $status = '')
    {
        if (!isset($param['id']) || empty($param['id']))
        {
            return ['code' => 400, 'msg' => '列表ID不能为空'];
        }

        if (!isset($param['goods_status']) || !in_array($param['goods_status'], [1,2]))
        {
            return ['code' => 400, 'msg' => '上下架状态不对'];
        }

        $goods_status = $param['goods_status'];
        $update  = [
            'goods_status' => $goods_status
        ];
        $where = [
            'id' => $param['id']
        ];
        $result = SendInfoModel::updateRobotSendGoodStatus($update, $where);

        $changeId = $param['id'];
        if ($result !== false)
        {
            $logMessage = '修改社群商品上下架';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($update), $logMessage, $changeId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    /*********************************************************************转连******************************************************************************************/

    public static function getProductShortLink($para = [], $memberInfo)
    {
        //4自营 5京东 6拼多多 7淘宝 8唯品会  9苏宁
        switch ($para['type']) {
            case 4: //自营商品短链
                //@todo
                $shortLink = self::Ordinarylink($para);
                break;
            case 5: // 京东联盟短链
                $shortLink = self::JdShortLink($para);
                break;
            case 6: // 拼多多商品短链
                $shortLink = self::pinShortLink($para);
                break;
            case 7:
                $shortLink = self::taobkShortLink($para);
                # code...
                break;
            case 8:
                $shortLink = self::wphShortLink($para);
                # code...
                break;
            case 9:
                $shortLink = self::snShortLink($para);
                break;
            case 12:
                $shortLink = self::bainaShortLink($para);
                # code...
                break;
            case 11:
                if (isset($para['gid'])) {
                    switch ($para['gid']) {
                        case 5://美团
                            $shortLink = self::mTShortLink();
                            break;
                        case 2://肯德基
                            break;
                        case 6://品牌折扣
                            $shortLink = self::ppzkShortLink();
                            break;
                    }
                }
                # code...
                break;
            case 13:
                $shortLink = self::getCpsActiveLink($para);
                break;
            case 14:
                $shortLink = self::hykhcShortLink($para);
                break;
        }
        return $shortLink;
    }


    /**cps活动链接
     * @param $para
     * @return array|string
     */
    public static function getCpsActiveLink($para)
    {   
        $buyUrl = CouponService::getShortURrl(array('url' => self::$cpsactivehost . 'id=' . $para['gid'] .'&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }



    /**自营商品短链
     * @param $para
     * @return array|string
     */
    public static function Ordinarylink($para)
    {   
        $buyUrl = CouponService::getShortURrl(array('url' => self::$ythost . 'product_id=' . $para['gid'] .'&product_sku_id='.$para['sku_id'].'&productType=0&addressCode=0&type=ordinary&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }

    /**京东商品短链
     * @param $para
     * @return array|string
     */
    public static function JdShortLink($para)
    {
        $buyUrl = CouponService::getShortURrl(array('url' => self::$h5host . 'goodsId=' . $para['gid'] . '&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }

    /**
     * 拼多多短链
     * @param $para
     * @return array|string
     */
    public static function pinShortLink($para)
    {
        $buyUrl = CouponService::getShortURrl(array('url' => self::$pddhost . 'goodsId=' . $para['gid'] . '&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }



    /**
     * 淘宝短链
     * @param $para
     * @return array|string
     */
    public static function taobkShortLink($para)
    { 

        if(isset($para['longUrl'])||!empty($para['longUrl'])){

            $param['gid'] = $para['gid'];
            $param['longUrl'] = $para['longUrl'];

            ProductModel::insertTbkLongUrl($param);
        }

        $para['rate'] = isset($para['rate'])?$para['rate']:0;

        $buyUrl = CouponService::getShortURrl(array('url' => self::$tbaokhost . 'id=' . $para['gid'] . '&commission='.$para['rate'].'&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }


    /**
     * 获取淘宝长链
     * @param $para
     * @return array|string
     */
    public static function getTbkLongUrl($para)
    { 

        if(isset($para['gid'])||!empty($para['gid'])){

          $url =  ProductModel::getTbkLongUrl($para['gid']);

          return ['code'=>200,'msg'=>'success','data'=>$url];

        }else{

          return ['code'=>400,'msg'=>'商品id不能为空'];
        }

    }


    /**
     * 唯品会短链
     * @param $para
     * @return array|string
     */
    public static function wphShortLink($para)
    { 
        $buyUrl = CouponService::getShortURrl(array('url' => self::$wphhost . 'goodsId=' . $para['gid'] . '&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }

    /**
     * 苏宁短链
     * @param $para
     * @return array|string
     */
    public static function snShortLink($para)
    { 
        $buyUrl = CouponService::getShortURrl(array('url' => self::$snhost . 'goodsId=' . $para['gid'] . '&supplier_code='.$para['supplier_code'].'&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }

    /**
     * 白拿链接
     * @param $para
     * @return array|string
     */
    public static function bainaShortLink($para)
    {
        $buyUrl = CouponService::getShortURrl(array('url' => self::$ythost . 'product_id=' . $para['gid'] .'&product_sku_id='.$para['sku_id'].'&productType=0&addressCode=0&type=ordinary&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }


    /**
     * 美团短链
     * @return array|string
     */
    public static function mTShortLink()
    {
        $buyUrl = CouponService::getShortURrl(array('url' => self::$mthost . 'codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }
    /**
     * 品牌折扣短链
     * @return array|string
     */
    public static function ppzkShortLink()
    {
        $buyUrl = CouponService::getShortURrl(array('url' => self::$ppzkhost . 'codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }

    /**
     * 好衣库短链
     * @return array|string
     */
    public static function hykhcShortLink($para)
    {   
        $para['goodName'] = isset($para['goodName'])?$para['goodName']:'';
        $buyUrl = CouponService::getShortURrl(array('url' => self::$hykhchost . 'epId=' . $para['gid'] .'&channelName='.$para['goodName'].'&env=0&codeNumber=' . config('community.codeNumber')));
        return $buyUrl;
    }

    /**************************************************************************大人选品库******************************************************************************************/

    /**
     * 大人商品转链
     * @param array $para
     * @param $memberInfo
     * @return array|string
     */
    public static function getDarenProductShortLink($para = [], $memberInfo){


        $returnData=[];
        if(  in_array($para['type'],[5,6])){
            $result = curlPostCommon('https://gateway.daren.tech/mall/v1/mall/robotProductLink', ['type'=>$para['type'],'admin_mid'=>$memberInfo['mid'],'product_id'=> $para['gid']]);
            $returnData = json_decode($result,true);
        }else if(in_array($para['type'],[4])){
            $result = curlPostCommon('https://gateway.daren.tech/mall/v1/mall/robotProductLink', ['type'=>$para['type'],'admin_mid'=>$memberInfo['mid'],'product_id'=> $para['gid'],'product_sku_id'=>$para['sku_id']]);
            $returnData = json_decode($result,true);
        }
        $shortLink='';
        if(!empty($returnData)&&isset($returnData['code'])){
            if($returnData['code']==200){
                return  $returnData['data']['shortUrl'];
            }
        }
        return $shortLink;
    }

    public  static  function querySearchDarenSearchData($para = [], $memberInfo)
    {
        $params['goodName'] = isset($para['goodName'])? $para['goodName']:'';
        $params['minPrice'] = isset($para['minPrice'])? $para['minPrice']:'';;
        $params['maxPrice'] = isset($para['maxPrice'])? $para['maxPrice']:'';
        $params['type']      =  isset($para['type'])?   $para['type']:4 ;
        $params['page']      =  isset($para['page'])?   $para['page']:1 ;
        $result = curlPostCommon('https://gateway.daren.tech/mall/v1/mall/getProductLists',$params);
        return $result;
    }

    /**************************************************************************大人选品库******************************************************************************************/

    /**
     * @param $param
     */
    public  static function  changeOrderGoods($param){
        $countNum = ProductModel::getNomemberPrice($param['good_type']);
//        var_dump($countNum);
//        die();
        $pageNum = ceil($countNum / $param['pageSize']);
        if ($pageNum > 0) {
            for ($i = 1; $i <= $pageNum; $i++) {
//                var_dump($i);
                $data = ProductModel::getNomemberPriceData($param['good_type'], ($i - 1) * $param['pageSize'], $param['pageSize']);
                if (!empty($data)) {
                    foreach ($data as $v) {
                        self::getShareData($v['goods_id'], $v['actual_price'], $v['good_type'], 1);
                    }
                }
            }
        }
    }

    /**
     *
     *  带有佣金的价格
     * @param $goodsId
     * @param $buyPrice
     * @param $good_type   2  京东  3  拼多多  4 唯品会  不懂不要改   spz
     * @param int $type
     * @return float|int
     * @throws \Exception
     *  不懂不要改   不懂不要改   不懂不要改
     */
    public  static function  getShareData($goodsId,$buyPrice,$good_type,$type=0){
        $goods_price_member =0;
        if($good_type==2){
            $postData['skuIds']  = $goodsId;
            $resultData =json_decode(curlPostCommon('https://gateway.yueshang.shop/private/v1/open/v2/jd/getSearchGoods',$postData),true);
            if($resultData['code']==200){
                foreach($resultData['data'] as $list){
                    $sharePrice =  self::getJdEarnricePrice($list['commissionInfo'], $id = 103);
                }
                if(!empty($sharePrice)){
                    $goods_price_member = $sharePrice['tk']*100+$buyPrice;
                }
            }
        }else if($good_type==3){
            $goodsRestData =json_decode(curlPostCommon('https://gateway.yueshang.shop/php-private/v1/pdd/v1/queryGoodsDetail',array('id' => $goodsId)),true);
            if (!empty($goodsRestData) && $goodsRestData['code'] == 200 && !empty($goodsRestData['data'])) {
                $goods_price_member = $goodsRestData['data']['info']['commission']*100+$buyPrice;
            }
        }else if($good_type==4){
            $goodsRestData =json_decode(curlPostCommon('https://gateway.yueshang.shop/private/v1/vip/v1/getGoodsInfo',array('goodsId' => $goodsId)),true);
            $commission  =isset($goodsRestData['data'][0]['commission'])?$goodsRestData['data'][0]['commission']:'';
            if ($commission>0) {
                $goods_price_member  =$commission*100+$buyPrice;
            }
        }
        if($type==1&&$goods_price_member>0){
             ProductModel::upGoodsPriceData($goodsId,['goods_price_member'=>$goods_price_member]);
        }
        return $goods_price_member;

    }


    /**************************************************************************京东佣金数据******************************************************************************************/


    /**
     * 直订自营数据
     * @param array $para
     * @param $memberInfo
     * @return bool|string
     * @throws \Exception
     */
    public  static  function querySearchZhiyingSearchData($para = [], $memberInfo)
    {
        $params['goodName'] = isset($para['goodName'])? $para['goodName']:'';
        $params['minPrice'] = isset($para['minPrice'])? $para['minPrice']:'';;
        $params['maxPrice'] = isset($para['maxPrice'])? $para['maxPrice']:'';
        $params['type']      =  isset($para['type'])?   $para['type']:4 ;
        $params['page']      =  isset($para['page'])?   $para['page']:1 ;
        $params['pageSize']  =  isset($para['pageSize'])?   $para['pageSize']:10 ;
        $result = curlPostCommon('https://zhidingapi.zhiding365.com/goods/robot/getGoodsList',$params);

        return $result;
    }


    /**
     * 直订商品转链
     * @param array $para
     * @param $memberInfo
     * @return array|string
     */
    public static function getZhidingProductShortLink($para = [], $memberInfo){
        $returnData=[];
        if( in_array($para['type'],[5])){
            $result = curlPostCommon('https://zhidingapi.zhiding365.com/apiH5/jd/jdGoodsShortCode', ['type'=>$para['type'],'mid'=>$memberInfo['mid'],'goodsId'=> $para['gid'],'skuId'=>$para['sku_id']]);
            $returnData = json_decode($result,true);
            $shortLink='';
            if(!empty($returnData)&&isset($returnData['code'])){
                if($returnData['code']==200){
                    return  $returnData['data']['url'];
                } else {
                    return  $returnData;
                }
            }
        }else if(in_array($para['type'],[4])){
            $result = curlPostCommon('http://zhidingapi.zhiding365.com/goods/robot/getShortUrl', ['type'=>$para['type'],'mid'=>$memberInfo['mid'],'goodsId'=> $para['gid'],'skuId'=>$para['sku_id']]);
            $returnData = json_decode($result,true);
            $shortLink='';
            if(!empty($returnData)&&isset($returnData['code'])){
                if($returnData['code']==200){
                    return  $returnData['data']['link_url'];
                }
            }

        }else if(in_array($para['type'],[15,11,16,17])) {
            if($para['type']==11){

                switch ($para['gid']) {
                    case 6: //品牌折扣券
                        $url = "https://h5.zhiding365.com/h5/brandDiscount?robot_send=1";
                        break;
                    case 7: //视频充值
                        $url = "https://h5.zhiding365.com/h5/TopUpIndex?robot_send=1";
                        break;
                    case 8: //话费充值
                        $url = "https://h5.zhiding365.com/h5/TopUpIndex?robot_send=1";
                        break;
                    case 9: //加油
                        $url = "https://h5.zhiding365.com/h5/comeOnGuide?robot_send=1";
                        break;
                    case 10: //转盘活动1
                        $url = "https://h5.zhiding365.com/h5/index?type=1";
                        break;
                    case 11: //转盘活动2
                        $url = "https://h5.zhiding365.com/h5/index?type=2";
                        break;
                    case 12: //电影票
                        $url = "https://h5.zhiding365.com/h5/index?type=3";
                        break;
                    case 13: //白拿活动
                        $url = "https://h5.zhiding365.com/h5/list?robot_send=1";
                        break;          
                    default:
                        $url = "https://h5.zhiding365.com/h5/hotel?hotelId=".$para['gid'];
                        break;
                }
            }else if($para['type']==16){

                $url = "https://h5.zhiding365.com/h5/fictitiousDetail?id=".$para['gid'];

            }else if($para['type']==15){

                $url = "https://h5.zhiding365.com/h5/hotel?hotelId=".$para['gid'];
            }else if($para['type']==17){

                $url = "https://h5.zhiding365.com/h5/fictitiousDetail?id=".$para['gid'];
            }
            
            $result = curlPostCommon('https://zhidingapi.zhiding365.com/hotel/v3/share/shortLink?', ['url'=>$url]);
            $returnData = json_decode($result,true);
            $shortLink='';
            if(!empty($returnData)&&isset($returnData['code'])){
                if($returnData['code']==200){
                    return  $returnData['data']['shortUrl'];
                }
            }
        }
        return $shortLink;
    }


    public static function  getZhidingCategoryList()
    {
        $result = curlPostCommon('https://zhidingapi.zhiding365.com/apiH5/jd/jdGoodsShortCode');

    }

    /**************************************************************************迈图******************************************************************************************/

      public static function  querySearchMaituSearchData($para = [], $memberInfo){

          $params['goodName'] = isset($para['goodName'])? $para['goodName']:'';
          $params['minPrice'] = isset($para['minPrice'])? $para['minPrice']:'';;
          $params['maxPrice'] = isset($para['maxPrice'])? $para['maxPrice']:'';
          $params['type']      =  isset($para['type'])?   $para['type']:4 ;
          $params['page']      =  isset($para['page'])?   $para['page']:1 ;
          $result = curlPostCommon('https://gateway.daren.tech/mall/v1/mall/getProductLists',$params);
          return $result;

      }

    /**
     * 自营分类
     * @param array $para
     * @param $memberInfo
     * @return array
     */
      public  static  function  getMaituCategoryList($para = [], $memberInfo){

          return  [];


      }

    /**
     * 短连接生成
     * @param $para
     * @param $memberInfo
     * @return string
     * @throws \Exception
     */
      public  static  function getMaituProductShortLink($para,$memberInfo){
          switch ($para['type']) {
              case 5: //京东
                  $type = 1;
                  break;
              case 6://拼多多
                  $type = 2;
                  break;
              case 8://唯品会
                  $type = 3;
                  break;
               case 4://自营
                  $type = 4;
                  break;
              default:
                  $type = 1;
          }
         $result = curlPostCommon('https://maitu-web.yuebei.vip/open/v1/getShortUrl', ['rel_type'=>$type,'admin_mid'=>$memberInfo['mid'],'goodsId'=> $para['gid']]);
         $returnData = json_decode($result,true);
          $shortLink='';
          if(!empty($returnData)&&isset($returnData['code'])){
              if($returnData['code']==200){
                  return  $returnData['data']['short_url'];
              }
          }
          return $shortLink;
      }



    /**************************************************************************直订自营数据展示******************************************************************************************/

    /**
     * @throws \Exception
     */
    public static   function  getTest($body){

        $result = curlPostCommon('https://gateway.daren.tech/open/v1/open/v1/home/getUserInfo', ['mobile'=>'17657625267']);
        $returnData = json_decode($result,true);
        if($returnData['code']==200){
            var_dump($returnData['data']);
        }

        $res1=   GroupModel::getGroupData(31);
        var_dump($res1->id);
        var_dump($res1->mid);

//      $res =   PullTaskRedisModel::getDaRenLinkUrl($body);
//      var_dump($res);
    }



    /**
     * 获取活动列表
     * @param $para
     * @param $memberInfo
     * @return string
     * @throws \Exception
     */
      public  static  function getActiveList($params,$memberInfo){

        $params['page'] = isset($params['page'])?$params['page']:1;
        $params['pageSize'] = isset($params['pageSize'])?$params['pageSize']:10;

        $list = ProductModel::getActiveList($params);

        $count = ProductModel::getActiveListCount($params);

        $returnData = [];

        if(!empty($list)){

            foreach ($list as $key => $v) {
                
                $returnData[$key]['key'] = ($params['page'] - 1) * $params['pageSize'] + $key + 1;
                $returnData[$key]['id'] = $v['id'];
                $returnData[$key]['activeName'] = $v['active_name'];
                $returnData[$key]['goodTypes'] = "";
                $returnData[$key]['clickNumber'] = $v['click_number'];
                $returnData[$key]['sendGroupNumber'] = $v['send_group_number'];
                $returnData[$key]['createdTime'] = $v['create_at'];
            }
        }

        return [
            'code' => 200,
            'total' => $count,
            'page' => $params['page'],
            'data' => $returnData
        ];

      }


    /**
     * 添加活动列表
     * @param $para
     * @param $memberInfo
     * @return string
     * @throws \Exception
     */
      public  static  function addActiveProduct($params,$memberInfo){


        if(!isset($params['active_name'])||empty($params['active_name'])){

            return ['code'=>400,'msg'=>'活动名称不能为空'];
        }

        if(!isset($params['banner'])||empty($params['banner'])){

            return ['code'=>400,'msg'=>'活动banner不能为空'];
        }

        if(!isset($params['goodsList'])||empty($params['goodsList'])){

            return ['code'=>400,'msg'=>'商品列表不能为空'];
        }

        if(!isset($memberInfo['platform_identity'])||empty($memberInfo['platform_identity'])){

            return ['code'=>400,'msg'=>'平台标识不能为空'];
        }

        DB::beginTransaction();

        try {

            $data = [];

            $data['active_name'] = $params['active_name'];
            $data['active_banner'] = json_encode($params['banner']);
            $data['platform_identity'] = $memberInfo['platform_identity'];

            $active_id = ProductModel::activeDataInsert($data);


            if(!$active_id){

                return ['code'=>400,'msg'=>'活动添加失败'];
            }

            $goodsType = [];

            foreach ($params['goodsList'] as $key => $value) {
                
                $goods_data = [];

                $goods_data['active_id'] = $active_id;
                $goods_data['good_id'] = $value['goods_id'];
                $goods_data['good_name'] = $value['goods_name'];
                $goods_data['good_type'] = $value['good_type'];
                $goods_data['good_img'] = $value['img'];
                $goods_data['sort'] = isset($value['sort'])?$value['sort']:0;
                $goods_data['price'] = $value['price'];
                $goods_data['vip_price'] = $value['vip_price'];

                $active_product = ProductModel::activeProductDataInsert($goods_data);

                if(!$active_product){

                    return ['code'=>400,'msg'=>'活动产品添加失败'];
                }

                if(!empty($goodsType)){

                    if(!in_array($value['good_type'], $goodsType)){

                        $goodsType[] = $value['good_type'];
                    }
                }else{

                    $goodsType[] = $value['good_type'];
                }
            }

            $updateActive = ProductModel::updateActive(['id'=>$active_id,'update_at'=>date("Y-m-d H:i:s",time()),'good_types'=>implode(",",$goodsType)]);

            DB::commit();

            return ['code'=>200,'msg'=>'添加成功'];

        } catch (Exception $e) {

            Db::rollback();

            return ['code'=>400,'msg'=>'活动添加失败'];
        }


      }


    /**
     * 编辑活动列表
     * @param $para
     * @param $memberInfo
     * @return string
     * @throws \Exception
     */
      public  static  function updateActiveProduct($params,$memberInfo){

        if(!isset($params['id'])||empty($params['id']))
        {
            return ['code'=>400,'msg'=>'活动ID不能为空'];
        }


        if(!isset($params['active_name'])||empty($params['active_name'])){

            return ['code'=>400,'msg'=>'活动名称不能为空'];
        }

        if(!isset($params['banner'])||empty($params['banner'])){

            return ['code'=>400,'msg'=>'活动banner不能为空'];
        }

        if(!isset($params['goodsList'])||empty($params['goodsList'])){

            return ['code'=>400,'msg'=>'商品列表不能为空'];
        }

        if(!isset($memberInfo['platform_identity'])||empty($memberInfo['platform_identity'])){

            return ['code'=>400,'msg'=>'平台标识不能为空'];
        }


        $active_Info = ProductModel::getActiveInfo($params);

        if(empty($active_Info)){

            return ['code'=>400,'msg'=>'活动不存在'];
        }

        DB::beginTransaction();

        try {

            $data = [];

            $data['id'] = $params['id'];
            $data['active_name'] = $params['active_name'];
            $data['active_banner'] = json_encode($params['banner']);
            $data['platform_identity'] = $memberInfo['platform_identity'];

            $updateActive = ProductModel::updateActive($data);


            $goodsType = [];

            $updateActiveGoods = ProductModel::updateActiveGoods(["active_id"=>$params['id'],'update_at'=>date("Y-m-d H:i:s",time()),'delete_at'=>date("Y-m-d H:i:s",time()),'is_delete'=>1]);

            foreach ($params['goodsList'] as $key => $value) {
                
                $issetActiveGoods = ProductModel::getActiveProductInfo($value,$params['id']);

                if(!empty($issetActiveGoods)){

                    $goods_data = [];

                    $goods_data['id'] = $issetActiveGoods['id'];
                    $goods_data['active_id'] = $params['id'];
                    $goods_data['good_id'] = $value['goods_id'];
                    $goods_data['good_name'] = $value['goods_name'];
                    $goods_data['good_type'] = $value['good_type'];
                    $goods_data['good_img'] = $value['img'];
                    $goods_data['sort'] = isset($value['sort'])?$value['sort']:0;
                    $goods_data['price'] = $value['price'];
                    $goods_data['vip_price'] = $value['vip_price'];
                    $goods_data['update_at'] = date("Y-m-d H:i:s",time());
                    $goods_data['delete_at'] = date("Y-m-d H:i:s",time());
                    $goods_data['is_delete'] = 0;

                    $active_product = ProductModel::activeProductDataUpdate($goods_data);


                }else{

                    $goods_data = [];

                    $goods_data['active_id'] = $params['id'];
                    $goods_data['good_id'] = $value['goods_id'];
                    $goods_data['good_name'] = $value['goods_name'];
                    $goods_data['good_type'] = $value['good_type'];
                    $goods_data['good_img'] = $value['img'];
                    $goods_data['sort'] = isset($value['sort'])?$value['sort']:0;
                    $goods_data['price'] = $value['price'];
                    $goods_data['vip_price'] = $value['vip_price'];

                    $active_product = ProductModel::activeProductDataInsert($goods_data);


                }

                if(!$active_product){

                    return ['code'=>400,'msg'=>'活动产品编辑失败'];
                }

                if(!empty($goodsType)){

                    if(!in_array($value['good_type'], $goodsType)){

                        $goodsType[] = $value['good_type'];
                    }
                }else{

                    $goodsType[] = $value['good_type'];
                }
            }

            $updateActive = ProductModel::updateActive(['id'=>$params['id'],'update_at'=>date("Y-m-d H:i:s",time()),'good_types'=>implode(",",$goodsType)]);

            DB::commit();
            
            return ['code'=>200,'msg'=>'编辑成功'];

        } catch (Exception $e) {

            DB::rollback();

            return ['code'=>400,'msg'=>"编辑失败"];
        }


      }



    /**
     * 活动详情
     * @param $para
     * @return string
     * @throws \Exception
     */
      public  static  function getActiveInfo($params){

        if(!isset($params['id'])||empty($params['id']))
        {
            return ['code'=>400,'msg'=>'活动ID不能为空'];
        }

        $active_Info = ProductModel::getActiveInfo($params);

        if(empty($active_Info)){

            return ['code'=>400,'msg'=>'活动不存在'];
        }

        $data = [];

        $data['activeName'] = $active_Info['active_name'];

        $data['banner'] = json_decode($active_Info['active_banner'],true);

        $sort = array_column($data['banner'],'sort');

        array_multisort($sort,SORT_DESC,$data['banner']);

        $active_product_list = ProductModel::getActiveProductList($active_Info['id']);

        $goodsList = [];

        foreach ($active_product_list as $key => $value) {
            
            $goodsList[$key]['goods_id'] = $value['good_id'];
            $goodsList[$key]['goods_name'] = $value['good_name'];
            $goodsList[$key]['img'] = $value['good_img'];
            $goodsList[$key]['good_type'] = $value['good_type'];
            $goodsList[$key]['price'] = $value['price'];
            $goodsList[$key]['vip_price'] = $value['vip_price'];
        }


        $data['goodsList'] = $goodsList;

        return [
            'code' => 200,
            'msg'  => "查询成功",
            'data' => $data
        ];

      }


    /**
     * 活动列表
     * @param $para
     * @return string
     * @throws \Exception
     */
      public  static  function getActiveInfoForH5($params){

        if(!isset($params['id'])||empty($params['id']))
        {
            return ['code'=>400,'msg'=>'活动ID不能为空'];
        }

        $active_Info = ProductModel::getActiveInfo($params);

        if(empty($active_Info)){

            return ['code'=>400,'msg'=>'活动不存在'];
        }

        $data = [];

        $data['activeName'] = $active_Info['active_name'];

        $bannerList = json_decode($active_Info['active_banner'],true);

        $data['banner'] = [];

        foreach ($bannerList as $k => $v) {

            $data['banner'][$k]['img'] = $v['img'];
            $data['banner'][$k]['sort'] = $v['sort'];
            $data['banner'][$k]['link'] = self::getActiveLink($active_Info['platform_identity'],$v['goods_type'],$v['goods_id']);
        }

        $sort = array_column($data['banner'],'sort');

        array_multisort($sort,SORT_DESC,$data['banner']);

        $active_product_list = ProductModel::getActiveProductList($active_Info['id']);

        $goodsList1 = [];
        $goodsList2 = [];

        foreach ($active_product_list as $key => $value) {
            
            if($value['good_type']==11){

                $goodsList1[$key]['goodsId'] = $value['good_id'];
                $goodsList1[$key]['goodsName'] = $value['good_name'];
                $goodsList1[$key]['goodsImg'] = $value['good_img'];
                $goodsList1[$key]['goodsType'] = $value['good_type'];
                $goodsList1[$key]['price'] = $value['price'];
                $goodsList1[$key]['vipPrice'] = $value['vip_price'];
                $goodsList1[$key]['link'] = self::getActiveLink($active_Info['platform_identity'],$value['good_type'],$value['good_id']);
                switch ($value['good_id']) {
                    case 6: //品牌折扣券
                        $msg = "低至85折";
                        break;
                    case 7: //视频充值
                        $msg = "低至85折";
                        break;
                    case 8: //话费充值
                        $msg = "最低1元";
                        break;
                    case 9: //加油
                        $msg = "样样都省钱";
                        break;
                    case 10: //转盘活动1
                        $msg = "免费抽奖赢大礼";
                        break;
                    case 11: //转盘活动2
                        $msg = "幸运中大奖";
                        break;
                    case 12: //电影票
                        $msg = "购票返现";
                        break;
                    case 13: //白拿活动
                        $msg = "样样都省钱";
                        break;          
                    default:
                        $msg = "";
                        break;
                }

                $goodsList1[$key]['desc'] = $msg;

            }else{

                $goodsList2[$key]['goodsId'] = $value['good_id'];
                $goodsList2[$key]['goodsName'] = $value['good_name'];
                $goodsList2[$key]['goodsImg'] = $value['good_img'];
                $goodsList2[$key]['goodsType'] = $value['good_type'];
                $goodsList2[$key]['price'] = $value['price'];
                $goodsList2[$key]['vipPrice'] = $value['vip_price'];
                $goodsList2[$key]['link'] = self::getActiveLink($active_Info['platform_identity'],$value['good_type'],$value['good_id']); 
                $goodsList2[$key]['desc'] = "";                 
            }

        }

        $updateActive = ProductModel::updateActive(['id'=>$params['id'],'click_number'=>$active_Info['click_number']+1]);

        $data['goodsList1'] = $goodsList1;
        $data['goodsList2'] = $goodsList2;

        return [
            'code' => 200,
            'msg'  => "查询成功",
            'data' => $data
        ];

      }


    /**
     * 添加直订活动链接
     * @param $para
     * @return string
     * @throws \Exception
     */
      public  static  function getActiveLink($platform_identity,$goods_type=0,$goods_id=0){

            $url = "";

            if($platform_identity=="75f712af51d952af3ab4c591213dea13"){

                switch ($goods_type) {
                    case 4:
                        $url = "https://h5.zhiding365.com/h5/CommodityDetails?id=".$goods_id;
                        break;
                    case 5:
                        $url = "https://h5.zhiding365.com/h5/shop/jdDetail?goodsId=".$goods_id;
                        break;
                    case 11:
                        switch ($goods_id) {
                            case 6: //品牌折扣券
                                $url = "https://h5.zhiding365.com/h5/brandDiscount?robot_send=1";
                                break;
                            case 7: //视频充值
                                $url = "https://h5.zhiding365.com/h5/TopUpIndex?robot_send=1";
                                break;
                            case 8: //话费充值
                                $url = "https://h5.zhiding365.com/h5/TopUpIndex?robot_send=1";
                                break;
                            case 9: //加油
                                $url = "https://h5.zhiding365.com/h5/comeOnGuide?robot_send=1";
                                break;
                            case 10: //转盘活动1
                                $url = "https://h5.zhiding365.com/h5/index?type=1";
                                break;
                            case 11: //转盘活动2
                                $url = "https://h5.zhiding365.com/h5/index?type=2";
                                break;
                            case 12: //电影票
                                $url = "https://h5.zhiding365.com/h5/index?type=3";
                                break;
                            case 13: //白拿活动
                                $url = "https://h5.zhiding365.com/h5/list?robot_send=1";
                                break;          
                            default:
                                $url = "";
                                break;
                        }
                        break;
                    case 15:
                        $url = "https://h5.zhiding365.com/h5/hotel?hotelId=".$goods_id;
                        break;
                    default:
                        $url = "";
                        break;
                }
            }

            return $url;
      }


    /**
     * 删除活动
     * @param $para
     * @return string
     * @throws \Exception
     */
      public  static  function delActiveInfo($params){

            if(!isset($params['id'])||empty($params['id'])){

                return ["code"=>400,"msg"=>"活动ID不能为空"];
            }

            $result = ProductModel::updateActive(['id'=>$params['id'],'is_delete'=>1,'update_at'=>date("Y-m-d H:i:s",time()),'delete_at'=>date("Y-m-d H:i:s",time())]);

            if($result){

                return ["code"=>200,"msg"=>"删除成功"];
            }else{

                return ["code"=>400,"msg"=>"操作失败"]; 
            }
      }

}