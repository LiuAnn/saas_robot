<?php
/**
 * 群激活
 * spz
 */

namespace App\Service\Community;

use App\Model\Admin\GroupModel;
use App\Model\Community\AutoGroupCreateModel;
use App\Model\Community\CommunityInvitationGroupModel;
use App\Model\Community\FriendsIntoGroupModel;
use App\Model\Community\GroupManageModel as CommunityGroupManageModel;
use App\Model\Member\MemberModel;
use App\Model\RobotOrder\RobotModel;
use App\Model\User\UserModel;
use App\Models\MemberInfoByAppID;
use App\Service\Robot\UserOperationLogService;
use Illuminate\Support\Facades\Redis;
use function config;
use function time;


class GroupService
{

    /**
     * 获取用户所有群列表
     * @param $params
     * @return mixed
     */
    public static function getAllGroupListByMidAndOrderSn($params)
    {
        return CommunityInvitationGroupModel::getAllGroupListByMidAndOrderSn($params);

    }

    /**
     * 获取用户信息
     * @param $groupId
     * @return mixed
     */
    public static function getGroupDetailByGroupId($groupId)
    {
        $detail = CommunityInvitationGroupModel::getGroupInfoByGroupId($groupId);

        $detail->exampleImages = config('community.exampleImages');

        return $detail;
    }

    /**
    * 更新数据
    * @param $data
    * @return mixed
    */
    public static function updateDataById($id, $data)
    {
        return CommunityInvitationGroupModel::updateDataById($id, $data);
    }


    /**
     * 建群
     * @param $data
     * @return array
     */
    public static function autoCreateGroup($memberInfo, $data)
    {

        //检测参数
        $checkParam = self::checkAutoCreateGroupParams($data,'add');

        if (!empty($checkParam))
        {
            return ['code' => 400, 'msg' => $checkParam['msg']];
        }
        //群主信息
        $groupAdminInfo = UserModel::getMemberInfoByWhere(['mobile' => $data['mobile']]);
        if (empty($groupAdminInfo)) {
            return ['code' => 400, 'msg' => '用户信息不存在'];
        }
        //机器人信息
        $robotInfo = RobotModel::getRobotInfoByWhere(['id' => $data['robot_id']]);
        if (empty($robotInfo)) {
            return ['code' => 400, 'msg' => '机器人信息不存在'];
        }
        $str  = self::getStr();
        $insert = [];
        $insert['admin_mid'] = $memberInfo['mid'];
        $insert['mid'] = $groupAdminInfo['mid'];
        $insert['admin_name'] = $groupAdminInfo['nickname'] ?: $groupAdminInfo['truename'] ?: '';
        $insert['name'] = $data['name'];
        $insert['tag_id'] = $data['tag_id'];
        $insert['tag_name'] = CommunityGroupManageModel::getTagDataName($insert['tag_id']);
        if ($robotInfo['bind_mid']) {
            $adminName = UserModel::getUserNameByMid($robotInfo['bind_mid']);
        } else {
            $adminName = UserModel::getUserNameByMid($robotInfo['mid']);
        }
        $insert['random_str'] = $str;
        $insert['robot_id'] = $data['robot_id'];
        $insert['wxid'] = $robotInfo['wxid'];
        $insert['robot_alias'] = $robotInfo['wx_alias'];
        $insert['robot_name'] = $robotInfo['wx_name'];
        $insert['robot_admin_name'] = $adminName;
        $insert['platform_identity'] = $memberInfo['platform_identity'];
        //计算成员GMV
        $insert['GMV'] = 0;
        $insert['mobile'] = $data['mobile'];
        $res = AutoGroupCreateModel::insertData($insert);
        if ($res) {
            $logMessage = '新增机器人创建群信息';
            $type = 1;
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], $type, json_encode($insert), $logMessage, $res
            );
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    public static function getStr($len=6, $chars=null)
    {
        if (is_null($chars)){
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }
        mt_srand(10000000*(double)microtime());
        for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++){
            $str .= $chars[mt_rand(0, $lc)];
        }
        return $str;
    }

    public static function checkAutoCreateGroupParams($param = [],$checkType = 'update')
    {
        if ($checkType == 'update')
        {
            if (empty($param['id']))
            {
                return ['code' => 400, 'msg' => 'ID不能为空'];
            }
        }

        if (!isset($param['name']) || empty($param['name']))
        {
            return ['code' => 400, 'msg' => '群名称不能为空'];
        }

        if (!isset($param['tag_id']) || empty($param['tag_id']))
        {
            return ['code' => 400, 'msg' => '标签不能为空'];
        }

        if (!isset($param['mobile']) || empty($param['mobile']))
        {
            return ['code' => 400, 'msg' => '手机号不能为空'];
        }

        if (!isset($param['robot_id']) || empty($param['robot_id']))
        {
            return ['code' => 400, 'msg' => '机器人不能为空'];
        }


//        if (!isset($param['name']) || empty($param['wxid']))
//        {
//            return ['code' => 400, 'msg' => '机器人不能为空'];
//        }

        if (!isset($param['robot_id']) || empty($param['robot_id']))
        {
            return ['code' => 400, 'msg' => '机器人不能为空'];
        }
    }


    public static function getLowerMemberList($param)
    {
        //要获取下级订单用户
        if (isset($param['mobile']) && !empty($param['mobile']))
        {
            $groupAdminInfo = UserModel::getMemberInfoByWhere(['mobile' => $param['mobile']]);
            //群主信息
            if (empty($groupAdminInfo)) {
                return ['code' => 400, 'msg' => '用户信息不存在'];
            }
            $param['upMid'] = $groupAdminInfo['mid'];
        }

        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        //$param['mid'] = $groupAdminInfo['mid'];
        $lowerMemberList = MemberModel::getLowerMemberList($param, $page, $pageSize);
        $lowerMemberCount = MemberModel::getLowerMemberCount($param);
        $lowerMemberCount = json_decode(json_encode($lowerMemberCount), true);
        if (!empty($lowerMemberList) && is_array($lowerMemberList)) {
            foreach ($lowerMemberList as &$value) {
                $memberInfo = MemberModel::getMemberInfo(['mid'=>$value->parent_id]);
                if ($memberInfo) {
                    $value->parent_name = $memberInfo->nickname ?: $memberInfo->truename;
                } else {
                    $value->parent_name = '上级用户';
                }
            }
        }
        $return = [];
        $return['data'] = $lowerMemberList;
        $return['total'] = $lowerMemberCount ? $lowerMemberCount[0]['cnt'] : 0;;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }

    public static function toCreateGroupList($param)
    {
        //要获取下级订单用户
        if (isset($param['mobile']) && !empty($param['mobile']))
        {
            $groupAdminInfo = UserModel::getMemberInfoByWhere(['mobile' => $param['mobile']]);
            //群主信息
            if (empty($groupAdminInfo)) {
                return ['code' => 400, 'msg' => '用户信息不存在'];
            }
            $param['groupMid'] = $groupAdminInfo['mid'];
        }

        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        //$param['mid'] = $groupAdminInfo['mid'];
        $lowerMemberList = MemberModel::toCreateGroupList($param, $page, $pageSize);
        $lowerMemberCount = MemberModel::toCreateGroupCount($param);
        $lowerMemberCount = json_decode(json_encode($lowerMemberCount), true);
        $return = [];
        $return['data'] = $lowerMemberList;
        $return['total'] = $lowerMemberCount ? $lowerMemberCount[0]['cnt'] : 0;;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }

    public static function getRobotList($memberInfo)
    {
        if ($memberInfo['user_type'] >= 2 ) {
            $robotWxAlias = CommunityGroupManageModel::getRobotWxAlias($memberInfo['mid'], $memberInfo['sup_id']);
        } else {
            $robotWxAlias = CommunityGroupManageModel::getRobotWxAlias($memberInfo['mid'],$memberInfo['sup_id'], 1);
        }
        return $robotWxAlias;
    }

    public static function friendsIntoGroup($memberInfo, $data)
    {
        if (!isset($data['room_wxid']) || empty($data['room_wxid'])) {
            return ['code' => 400, 'msg' => 'room_wxid不能为空'];
        }
        if (!isset($data['wxid']) || empty($data['wxid'])) {
            return ['code' => 400, 'msg' => 'wxid不能为空'];
        }
        if (!is_array($data['wxid'])) {
            return ['code' => 400, 'msg' => 'wxid格式不正确'];
        }
        $data['wxid'] = implode(",", $data['wxid']);
        //群主信息
        $groupInfo = GroupModel::fetchGroupInfoByWhere(['room_wxid' => $data['room_wxid']]);

        if (empty($groupInfo)) {
            return ['code' => 400, 'msg' => '群信息不存在'];
        }
        $str  = self::getStr();
        $insert = [];
        $insert['admin_mid'] = $memberInfo['mid'];
        $insert['mid'] = $groupInfo['mid'];
        $insert['name'] = $groupInfo['name'];
        $insert['wxid'] = $groupInfo['wxid'];
        $insert['random_str'] = $str;
        //$insert['robot_id'] = $data['robot_id'];
        $insert['content'] = $data['wxid'];
        $insert['room_wxid'] = $data['room_wxid'];
        $res = FriendsIntoGroupModel::insertData($insert);
        if ($res) {
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }


    public static function planFriendsIntoGroup($memberInfo, $data)
    {
        if (!isset($data['id']) || empty($data['id'])) {
            return ['code' => 400, 'msg' => '群ID不能为空'];
        }
        if (!isset($data['wxid']) || empty($data['wxid'])) {
            return ['code' => 400, 'msg' => 'wxid不能为空'];
        }
        if (!is_array($data['wxid'])) {
            return ['code' => 400, 'msg' => 'wxid格式不正确'];
        }
        $where = ['id' => $data['id']];
        //获取群信息
        $groupInfo = AutoGroupCreateModel::getGroupInfoByWhere($where);
        if (empty($groupInfo)) {
            return ['code' => 400, 'msg' => '群信息不存在'];
        }
        if ($groupInfo['robot_send_status'] == 1) {
            if (!isset($groupInfo['room_wxid']) || empty($groupInfo['room_wxid'])) {
                return ['code' => 400, 'msg' => '当前群还未绑定, 请稍后邀请'];
            }
        }
        if ($groupInfo['content']) {
            $existMid = explode(",", $groupInfo['content']);
            foreach ($data['wxid'] as $key => &$value) {
                if (in_array($value, $existMid)) {
                    unset($data['wxid'][$key]);
                }
            }
        }
        if (empty($data['wxid'])) {
            return ['code' => 200, 'msg' => 'success'];
        }
        $insertMember = $data['wxid'];
        $count = count($data['wxid']);
        $memberIds = AutoGroupCreateModel::getMemberIdsByWXID($data['wxid']);
        $memberIds = "'" . implode("','", $memberIds) . "'";
        $data['wxid'] = implode(",", $data['wxid']);
        $memberResult = AutoGroupCreateModel::getMemberGmvByIds($memberIds);
        $memberGMV = $memberResult->GMV ?: 0.00;
        $memberOrderNum = $memberResult->cnt ?: 0.00;
        if ($groupInfo['content']) {
            $data['wxid'] = $groupInfo['content'] . ',' . $data['wxid'];
        }
        $member_count = $groupInfo['member_count'] + $count;
        $GMV = $groupInfo['GMV'] + $memberGMV;
        $memberOrderNum = $groupInfo['order_count'] + $memberOrderNum;

        $update = [];
        $update['content'] = $data['wxid'];
        $update['member_count'] = $member_count;
        $update['order_count'] = $memberOrderNum;
        $update['GMV'] = $GMV;
        $res = AutoGroupCreateModel::where($where)->update($update);
        if ($res) {
            $where = ['id' => $data['id']];
            //获取群信息
            $groupInfo = AutoGroupCreateModel::getGroupInfoByWhere($where);
            if ($member_count > 1 && $groupInfo['robot_send_status'] == 0) {
                $auto_group_list_key = $groupInfo['wxid'] . AutoGroupCreateModel::GROUP_LIST_KEY;
                Redis::rpush($auto_group_list_key, json_encode($groupInfo)); //加入队列值
            } else {
                //该群已创建, 使用邀请还有进群任务
                $invite_friend_group_key = $groupInfo['wxid'] . AutoGroupCreateModel::INVITE_FRIEND_KEY;
                $saveData = [];
                $saveData['room_wxid'] = $groupInfo['room_wxid'];
                $saveData['flag'] = 0;
                if ($member_count > 40) {
                    $saveData['flag'] = 1;
                }
                foreach ($insertMember as $member) {
                    $saveData['wxid'] = $member;
                    Redis::rpush($invite_friend_group_key, json_encode($saveData)); //加入队列值
                }

            }
            $logMessage = '修改邀请好友创建群信息';
            $type = 2;
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], $type, json_encode($update), $logMessage, $data['id']
            );
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    /**
     * 建群
     * @param $data
     * @return array
     */
    public static function createGroup($data)
    {
        $createData = [];

        //查询群助手
        $assistant = CommunityInvitationGroupModel::getLeastUserGroupAssistant(1);

        //查询订单信息
        $orderInfo = CommunityInvitationGroupModel::getOrderInfo($data['ordersn']);

        if (!$orderInfo) {
            return ['code' => 400, 'msg' => '订单信息不存在'];
        }

        //组装数据
        for ($i = 0; $i < $orderInfo->service_number; $i ++) {
            $groupNo = self::getRandNumber();
            $createData [] = [
                'mid' => $orderInfo->mid,
                'ordersn' => $data['ordersn'],
                'group_number' => $groupNo,
                'group_name' => config('community.groupName').$groupNo,
                'examine_status' => 2,
                'addtime' => time(),
                'assistant_wx' => $assistant ? $assistant->assistant_wx : '',
                'tutor_wx' => config('community.tutorInfo')[0]['tutorWx'],
                'type' => 2,
            ];
        }
        $res = CommunityInvitationGroupModel::insertData($createData);
        if ($res) {
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    /**
     * 生成不重复的随机数字
     * @param  int $start  需要生成的数字开始范围
     * @param  int $end    结束范围
     * @param  int $length 需要生成的随机数个数
     * @return number      生成的随机数
     */
    public static function getRandNumber($start=0,$end=9,$length=8)
    {
        //初始化变量为0
        $connt = 0;
        //建一个新数组
        $temp = array();
        while($connt < $length){
            //在一定范围内随机生成一个数放入数组中
            $temp[] = mt_rand($start, $end);
            //$data = array_unique($temp);
            //去除数组中的重复值用了“翻翻法”，就是用array_flip()把数组的key和value交换两次。这种做法比用 array_unique() 快得多。
            $data = array_flip(array_flip($temp));
            //将数组的数量存入变量count中
            $connt = count($data);
        }
        //为数组赋予新的键名
        shuffle($data);
        //数组转字符串
        $str=implode(",", $data);
        //替换掉逗号
        $number=str_replace(',', '', $str);
        return $number;
    }


    /**
     * 群标签列表
     * @param $data
     * @return array
     */
    public static function getGroupTagList($param,$memberInfo)
    {

        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;

        $param['platform_identity'] = $memberInfo['platform_identity'];

        $list = CommunityInvitationGroupModel::getGroupTagList($param,$page,$pageSize);
        $totalCount = CommunityInvitationGroupModel::getGroupTagCount($param);

        foreach ($list as $key => $value) {

            $value->key = ($page-1)*$pageSize+1+$key;
        }

        return ["code"=>200,"data"=>['totalCount'=>$totalCount,'list'=>$list]];
    }


    /**
     * 添加群标签
     * @param $data
     * @return array
     */
    public static function addGroupTag($param,$memberInfo)
    {

        if(!isset($param['tag_name'])||empty($param['tag_name']))
        {
            return ['code'=>400,'msg'=>"群标签名称不能为空"];
        }

        $tagInfo = CommunityInvitationGroupModel::getTagInfo($param);

        if(!empty($tagInfo))
        {
            return ['code'=>400,'msg'=>"群标签名称已被使用"];
        }

        $createData = [
            'tag_name' => $param['tag_name'],
            'tag_desc' => '',
            'sort'     => 0,
            'mid'      => $memberInfo['mid'],
            'platform_identity' => $memberInfo['platform_identity']
        ];

        $res = CommunityInvitationGroupModel::addGroupTag($createData);

        if ($res) {
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    /**
     * 修改群标签
     * @param $data
     * @return array
     */
    public static function updateGroupTag($param,$memberInfo)
    {
        if(!isset($param['tag_id'])||empty($param['tag_id']))
        {
            return ['code'=>400,'msg'=>"群标签ID不能为空"];
        }

        if(!isset($param['tag_name'])||empty($param['tag_name']))
        {
            return ['code'=>400,'msg'=>"群标签名称不能为空"];
        }

        $tagInfo = CommunityInvitationGroupModel::getTagInfo($param);

        if(!empty($tagInfo)&&$tagInfo->id!=$param['tag_id'])
        {
            return ['code'=>400,'msg'=>"群标签名称已被使用"];
        }

        $createData = [
            'tag_name' => $param['tag_name'],
            'tag_desc' => '',
            'sort'     => 0,
            'mid'      => $memberInfo['mid'],
            'platform_identity' => $memberInfo['platform_identity']
        ];

        $res = CommunityInvitationGroupModel::updateGroupTag($param['tag_id'],$createData);

        if ($res) {
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    /**
     * 删除群标签
     * @param $data
     * @return array
     */
    public static function delGroupTag($param,$memberInfo)
    {
        if(!isset($param['tag_id'])||empty($param['tag_id']))
        {
            return ['code'=>400,'msg'=>"群标签ID不能为空"];
        }

        $isBindGroup = CommunityInvitationGroupModel::getGroupCountByTagID($param['tag_id']);

        if($isBindGroup>0)
        {
            return ['code'=>400,'msg'=>"正在使用无法删除"];
        }

        $createData = [
            'is_detete' => 1,
            'deteted_at' => time(),
            'mid'      => $memberInfo['mid'],
            'platform_identity' => $memberInfo['platform_identity']
        ];

        $res = CommunityInvitationGroupModel::updateGroupTag($param['tag_id'],$createData);

        if ($res) {
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    /**
     * 绑定群标签
     * @param $data
     * @return array
     */
    public static function bindGroupTag($param)
    {
        if(!isset($param['room_id'])||empty($param['room_id']))
        {
            return ['code'=>400,'msg'=>"群ID不能为空"];
        }

        if(!isset($param['tag_id'])||empty($param['tag_id']))
        {
            return ['code'=>400,'msg'=>"群标签ID不能为空"];
        }

        $tagInfo = CommunityInvitationGroupModel::getTagInfo($param);

        if(empty($tagInfo))
        {
            return ['code'=>400,'msg'=>"群标签不存在"];
        }

        $createData = [
            'tag_id' => 1
        ];

        $res = CommunityInvitationGroupModel::updateGroup($param['room_id'],$createData);

        if ($res) {
            return ['code' => 200, 'msg' => 'success'];
        }
        return ['code' => 400, 'msg' => 'error'];
    }

    /**
     * 优惠券列表
     * @param $data
     * @return array
     */
    public static function getCouponList($param,$memberInfo)
    {

        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;

        switch ($memberInfo['platform_identity']) {
            case '7323ff1a23f0bf1cda41f690d4089353':
                $param['channel_type'] = 0;
                break;

            default:
                # code...
                break;
        }

        $couponModel = CommunityInvitationGroupModel::getCouponList($param,$page,$pageSize);

        $totalCount = CommunityInvitationGroupModel::getCouponCount($param);

        $list      = [];
        $moduleMap = [
            1 => '酒店',
            2 => '商城',
            3 => '线路',
            4 => '加油充值',
            5 => '第三方通用劵',
        ];
        $channelType = [
            0 => '悦淘',
            1 => '旅悦',
            2 => '大人',
        ];
        foreach ($couponModel as $item) {
            $mod = '';
            if (in_array($item->typeid, [1, 3,4,5])) {
                $mod = empty($moduleMap[$item->modules]) ? '未知' : $moduleMap[$item->modules];
            } else if ($item->typeid == 2){
                $supper = SupplierModel::where('id', $item->modules)->first(['supplier_name']);
                $mod    = empty($supper) ? '' : $supper->supplier_name;
            }

            switch($item->typeid){
                case 1:
                    $typeName  = '通用';//"平台";
                break;
                case 2:
                    $typeName  = "第三方";
                    break;
                case 3:
                    $typeName  = "指定产品";
                    break;
                default:
                    $typeName = '通用';//"平台";
                    break;
            }



            if($item->modules==5)
            {
                switch($item->typeid){
                    case 1:
                        $typeName  = '曹操';//"";
                        break;
                    case 2:
                        $typeName  = "飞机票";
                        break;
                    case 3:
                        $typeName  = "加油";
                        break;
                    default:
                        $typeName = '';//"";
                        break;
                }
            }

            $list[] = [
                'cid'               => $item->id,
                'coupon_name'       => $item->name,
                'coupon_type'       => $item->type ? '折扣券' : '满减券',
                'coupon_type_befor' => $item->samount ? $item->samount : '无限制',
                'promotion'         => $item->type ? "{$item->amount}折" : "满{$item->samount}减{$item->amount}",
                'valid_time'        => $item->isnever == 1 ? date('Y-m-d', $item->starttime).
                    "<br>".date('Y-m-d', $item->endtime) : "领取后有效<br>有效期{$item->antedate}天",
                'modules'           => $mod,
                'stock_num'         => $item->totalnumber,
                'send_num'          => $item->send_num,
                'use_num'           => empty($item->use_num) ? 0 : $item->use_num,
                'use_percent'       => empty($item->send_num) ? 0 : round($item->use_num/$item->send_num, 4) * 100 . '%',
                'isOpen'            => $item->isopen,
                'typeName'          => $typeName,
                'state'             => $item->state,
                'typeid'             => $item->typeid,
                'channelTypeName'   => $channelType[$item->channel_type]
            ];
        }

        return ["code"=>200,"data"=>['totalCount'=>$totalCount,'list'=>$list]];

    }

    /**
     * 通过mid获取是否是站长
     *
     * @param $mid
     * @return false|int[]
     */
    public static function getStationmasterByMid($mid)
    {
        if (empty($mid)){
            return false;
        }
        $count = MemberInfoByAppID::where('mid',$mid)->where('member_count','>=',200)->where('is_delete',0)->count();
        if (empty($count) || $count < 2){
            return ['is_stationmaster' => 0];
        }
        return ['is_stationmaster' => 1];
    }

}