<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/5/7
 * Time: 11:46
 */

namespace App\Service\Community;

use Illuminate\Support\Facades\Redis;
use App\Service\VoucherBag\CouponService;
use App\Model\Community\CommunityInvitationGroupModel;
use App\Model\Community\GroupActivationModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use  App\Model\Source\SendInfoModel;
use App\Model\Admin\GroupModel;
class FriendService
{
    /**
     * 获取好友列表
     */
    public static function getRobotUserList($param,$member){

        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param['sup_id'] = $member['sup_id'];
        $param['mid'] = $member['mid'];
        $param['platform_identity'] = $member['platform_identity'];
        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;
        $result    = SendInfoModel::getRobotUserList($param);
        $count     = SendInfoModel::getRobotUserCount($param);
        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatCommunitySendInfos($result)
            ],
        ];
    }

    /**
     * 获取机器人列表
     */
    public static function getFriendRobotList($param,$member){

        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 50;
        $param['sup_id'] = $member['sup_id'];
        $param['mid'] = $member['mid'];
        $param['platform_identity'] = $member['platform_identity'];
        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;
        $result    = SendInfoModel::getFriendRobotList($param);
        $count     = SendInfoModel::getFriendRobotCount($param);
        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => $result
            ],
        ];
    }


    //性别展示
    public static function getSexData(){
        return [
            1=>'男',
            2=>'女',
            0=>'保密',
        ];
    }
    //格式化数据
    public  static function formatCommunitySendInfos($result){
        $sexData =self::getSexData();
        $listData = [];
        if ($result) {
            foreach ($result as $k => $value) {
                $ordercountData = SendInfoModel::orderNumMid($value->mid);
                $list['wxid'] = $value->wxid;
                $list['robot_wxid'] = $value->robot_wxid;
                $list['nickname'] = $value->nickname;
                $list['head_img'] = $value->head_img;
                $list['groupNum'] =  $value->common_group_num;
                $list['address'] =  $value->country . $value->province. $value->city;
                $list['sex'] = $sexData[$value->sex] ;
                $list['robotName'] = $value->wx_name;
                $list['isFriend'] = $value->is_delete>0?'否':'是' ;
                $list['orderNum'] = empty($ordercountData)? 0 :$ordercountData->order_count;
                $list['orderGmv'] = empty($ordercountData->order_money)? 0 : $ordercountData->order_money;
                $listData[]=$list;
            }
        }
        return $listData;
    }



}