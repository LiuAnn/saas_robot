<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/19
 * Time: 23:14
 */

namespace App\Service\Community;

use App\Model\Admin\UserModel as AUserModel;
use App\Model\Community\GroupActivationModel;
use App\Model\Admin\GroupManageModel as AdminGroupManageModel;
use App\Model\Community\GroupManageModel as CommunityGroupManageModel;
use App\Model\Community\ProductModel;
use App\Model\Source\SendInfoModel;
use App\Models\ChannelData;
use App\Service\Robot\UserOperationLogService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use  App\Model\Community\CommunityModel;
use  App\Model\Community\GroupGoodsDataModel;
use  App\Model\RobotOrder\RobotModel;
use  App\Model\Admin\AdminUserModel;
use  App\Service\User\LoginClientService;
use  App\Model\User\UserModel;
use  App\Model\Robot\UnitManageModel;
use  App\Model\Admin\GroupModel;

class ExternalService
{


    /**
     * 数据中台社群接口
     * @param $param
     * @param $member
     * @return array
     */
    public static function getGroupNumCount($param)
    {
        $channel_data = ChannelData::getChannelDataList();
        //昨天
        $yesterday_beginTime = date('Y-m-d 00:00:00',strtotime('-1 days'));
        $yesterday_endTime = date('Y-m-d 00:00:00');
        //7天前
        $a_week_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-7 days'));
        $a_week_ago_endTime = date('Y-m-d 00:00:00');
        //30天前
        $a_month_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-30 days'));
        $a_month_ago_endTime = date('Y-m-d 00:00:00');
        //总社群数量
        $groupTotalNum = GroupModel::getGroupTotalNum($param, 'total');
        //社群昨日新增数量
        $param['beginTime'] = $yesterday_beginTime;
        $param['endTime'] = $yesterday_endTime;
        $groupYesterdayNum = GroupModel::getGroupTotalNum($param);
        //社群周新增数量
        $param['beginTime'] = $a_week_ago_beginTime;
        $param['endTime'] = $a_week_ago_endTime;
        $groupWeekNum = GroupModel::getGroupTotalNum($param);
        //社群月新增数量
        $param['beginTime'] = $a_month_ago_beginTime;
        $param['endTime'] = $a_month_ago_endTime;
        $groupMonthNum = GroupModel::getGroupTotalNum($param);
        $totalData = [
            'channel_name' => '全部',
            'total' => $groupTotalNum,
            'yesterday' => $groupYesterdayNum,
            'a_week_ago' => $groupWeekNum,
            'a_month_ago' => $groupMonthNum,
        ];
        if(!empty($channel_data)){
            $platformData = [];
            foreach($channel_data as $key => $val) {
                $platformData[$key]['channel_name'] = $val['channel_name'];
                $param['platform_identity'] = $val['platform_identity'];
                $platformData[$key]['total'] = GroupModel::getGroupTotalNum($param, 'total');
                $param['beginTime'] = $yesterday_beginTime;
                $param['endTime'] = $yesterday_endTime;
                $platformData[$key]['yesterday'] = GroupModel::getGroupTotalNum($param);
                $param['beginTime'] = $a_week_ago_beginTime;
                $param['endTime'] = $a_week_ago_endTime;
                $platformData[$key]['a_week_ago'] = GroupModel::getGroupTotalNum($param);
                $param['beginTime'] = $a_month_ago_beginTime;
                $param['endTime'] = $a_month_ago_endTime;
                $platformData[$key]['a_month_ago'] = GroupModel::getGroupTotalNum($param);
            }
        }
        $returnData = [
            'totalData' => $totalData,
            'platformData' => $platformData,
        ];
        return [
            "code" => 200,
            "data" => $returnData
        ];
    }




    /**
     * 数据中台社群接口
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupMemberNum($param)
    {
        $channel_data = ChannelData::getChannelDataList();
        //昨天
        $yesterday_beginTime = date('Y-m-d 00:00:00',strtotime('-1 days'));
        $yesterday_endTime = date('Y-m-d 00:00:00');
        //7天前
        $a_week_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-7 days'));
        $a_week_ago_endTime = date('Y-m-d 00:00:00');
        //30天前
        $a_month_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-30 days'));
        $a_month_ago_endTime = date('Y-m-d 00:00:00');
        //总社群数量
        $groupTotalNum = GroupModel::getNewGroupTotalNum($param, 'total');
        //社群昨日新增数量
        $param['beginTime'] = $yesterday_beginTime;
        $param['endTime'] = $yesterday_endTime;
        $groupYesterdayNum = GroupModel::getNewGroupTotalNum($param);
        //社群周新增数量
        $param['beginTime'] = $a_week_ago_beginTime;
        $param['endTime'] = $a_week_ago_endTime;
        $groupWeekNum = GroupModel::getNewGroupTotalNum($param);
        //社群月新增数量
        $param['beginTime'] = $a_month_ago_beginTime;
        $param['endTime'] = $a_month_ago_endTime;
        $groupMonthNum = GroupModel::getNewGroupTotalNum($param);
        $totalData = [
            'channel_name' => '全部',
            'total' => $groupTotalNum,
            'yesterday' => $groupYesterdayNum,
            'a_week_ago' => $groupWeekNum,
            'a_month_ago' => $groupMonthNum,
        ];
        if(!empty($channel_data)){
            $platformData = [];
            foreach($channel_data as $key => $val) {
                $param = [];
                $platformData[$key]['channel_name'] = $val['channel_name'];
                $param['platform_identity'] = $val['platform_identity'];
                $total = GroupModel::getGroupTotalNum($param);
                $total_member = GroupModel::getNewGroupTotalNum($param, 'total');
                $platformData[$key]['total'] = $total;
                $platformData[$key]['total_member'] = $total_member;
                $platformData[$key]['avg_member'] =  $total ? ceil($total_member / $total) : 0;
                $param['beginTime'] = $yesterday_beginTime;
                $param['endTime'] = $yesterday_endTime;
                $platformData[$key]['yesterday'] = GroupModel::getNewGroupTotalNum($param);
                $param['beginTime'] = $a_week_ago_beginTime;
                $param['endTime'] = $a_week_ago_endTime;
                $platformData[$key]['a_week_ago'] = GroupModel::getNewGroupTotalNum($param);
                $param['beginTime'] = $a_month_ago_beginTime;
                $param['endTime'] = $a_month_ago_endTime;
                $platformData[$key]['a_month_ago'] = GroupModel::getNewGroupTotalNum($param);
            }
        }
        return [
            'totalData' => $totalData,
            'platformData' => $platformData,
        ];
    }

    /**
     * 社群交易
     * @param $param
     * @param $member
     * @return array
     */
    public static function orderGMVCount($param)
    {
        $channel_data = ChannelData::getChannelDataList();
        //昨天
        $yesterday_beginTime = date('Y-m-d 00:00:00',strtotime('-1 days'));
        $yesterday_endTime = date('Y-m-d 00:00:00');
        //7天前
        $a_week_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-7 days'));
        $a_week_ago_endTime = date('Y-m-d 00:00:00');
        //30天前
        $a_month_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-30 days'));
        $a_month_ago_endTime = date('Y-m-d 00:00:00');
        //总社群数量
        $orderTotalNum = GroupModel::getOrderTotalNum($param, 'total');
        $orderTotalGMV = GroupModel::getOrderTotalAmount($param, 'total')/100;
        //社群昨日新增数量
        $param['beginTime'] = $yesterday_beginTime;
        $param['endTime'] = $yesterday_endTime;
        $orderYesterdayNum = GroupModel::getOrderTotalNum($param);
        $orderYesterdayGMV = GroupModel::getOrderTotalAmount($param)/100;
        //社群周新增数量
        $param['beginTime'] = $a_week_ago_beginTime;
        $param['endTime'] = $a_week_ago_endTime;
        $orderWeekNum = GroupModel::getOrderTotalNum($param);
        $orderWeekGMV = GroupModel::getOrderTotalAmount($param)/100;
        //社群月新增数量
        $param['beginTime'] = $a_month_ago_beginTime;
        $param['endTime'] = $a_month_ago_endTime;
        $orderMonthNum = GroupModel::getOrderTotalNum($param);
        $orderMonthGMV = GroupModel::getOrderTotalAmount($param)/100;
        $totalData = [
            'channel_name' => '全部',
            'total' => $orderTotalNum,
            'total_gmv' => $orderTotalGMV,
            'yesterday' => $orderYesterdayNum,
            'yesterday_gmv' => $orderYesterdayGMV,
            'a_week_ago' => $orderWeekNum,
            'a_week_ago_gmv' => $orderWeekGMV,
            'a_month_ago' => $orderMonthNum,
            'a_month_ago_gmv' => $orderMonthGMV,
        ];
        if(!empty($channel_data)){
            $platformData = [];
            foreach($channel_data as $key => $val) {
                $platformData[$key]['channel_name'] = $val['channel_name'];
                $param['platform_identity'] = $val['platform_identity'];
                $platformData[$key]['total'] = GroupModel::getOrderTotalAmount($param, 'total');
                $param['beginTime'] = $yesterday_beginTime;
                $param['endTime'] = $yesterday_endTime;
                $platformData[$key]['yesterday'] = GroupModel::getOrderTotalAmount($param);
                $param['beginTime'] = $a_week_ago_beginTime;
                $param['endTime'] = $a_week_ago_endTime;
                $platformData[$key]['a_week_ago'] = GroupModel::getOrderTotalAmount($param);
                $param['beginTime'] = $a_month_ago_beginTime;
                $param['endTime'] = $a_month_ago_endTime;
                $platformData[$key]['a_month_ago'] = GroupModel::getOrderTotalAmount($param);
            }
        }
        return [
            'totalData' => $totalData,
            'platformData' => $platformData,
        ];
    }



    /**
     * 社群交易
     * @param $param
     * @param $member
     * @return array
     */
    public static function orderCountList($param)
    {
        $channel_data = ChannelData::getChannelDataList();
        //昨天
        $yesterday_beginTime = date('Y-m-d 00:00:00',strtotime('-1 days'));
        $yesterday_endTime = date('Y-m-d 00:00:00');
        //7天前
        $a_week_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-7 days'));
        $a_week_ago_endTime = date('Y-m-d 00:00:00');
        //30天前
        $a_month_ago_beginTime = date('Y-m-d 00:00:00',strtotime('-30 days'));
        $a_month_ago_endTime = date('Y-m-d 00:00:00');

        if(!empty($channel_data)){
            $platformData = [];
            foreach($channel_data as $key => $val) {
                $platformData[$key]['channel_name'] = $val['channel_name'];
                $param['platform_identity'] = $val['platform_identity'];
                $platformData[$key]['total'] = GroupModel::getOrderTotalNum($param, 'total');
                $param['beginTime'] = $yesterday_beginTime;
                $param['endTime'] = $yesterday_endTime;
                $platformData[$key]['yesterday'] = GroupModel::getOrderTotalNum($param);
                $param['beginTime'] = $a_week_ago_beginTime;
                $param['endTime'] = $a_week_ago_endTime;
                $platformData[$key]['a_week_ago'] = GroupModel::getOrderTotalNum($param);
                $param['beginTime'] = $a_month_ago_beginTime;
                $param['endTime'] = $a_month_ago_endTime;
                $platformData[$key]['a_month_ago'] = GroupModel::getOrderTotalNum($param);
            }
        }
        return $platformData;
    }

    /**
     * 社群数量增长统计图
     * @param $param
     * @param $member
     * @return array
     */
    public static function getGroupNumCountGraph($param)
    {
        $channel_data = ChannelData::getChannelDataList();

        if (isset($param['startTime']) || isset($param['endTime'])) {
            if (!isset($param['startTime'])) {
                $param['beginTime'] = $param['endTime'];
            } else {
                $param['beginTime'] = $param['startTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? date('Y-m-d', strtotime('-30 day') . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? date('Y-m-d'). '00:00:00';
        }

        //刘帅所写,更换朴恩成可注释此处至return
        $param['beginTime'] = strtotime($param['beginTime']);
        $param['endTime'] = strtotime($param['endTime']);
        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $todoReturn = [];
        if (!empty($channel_data)) {
            foreach ($channel_data as $key => $val) {
                $param['platform_identity'] = $val['platform_identity'];
                //新增总人数
                $memberTotalNum = GroupModel::getGroupNumDate($param, $format);
                $todoReturn[$val['channel_name']] = json_decode(json_encode($memberTotalNum), true);
            }
        }
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
        $start_timestamp = strtotime($param['beginTime']);
        $end_timestamp = strtotime($param['endTime']);

        // 计算日期段内有多少天
        $days = ($end_timestamp - $start_timestamp) / 86400 + 1;

        // 保存每天日期
        $date = [];

        for ($i = 0; $i < $days; $i++) {
            $date[] = date('Y-m-d', $start_timestamp + (86400 * $i));
        }

        if (!empty($channel_data)) {
            foreach ($channel_data as $key => $val) {
                foreach ($date as $date_key => $date_val) {
                    $data[$key]['channel_name'] = $val['channel_name'];
                    $param['platform_identity'] = $val['platform_identity'];
                    $data[$key]['trend'][$date_key]['date'] = $date_val;
                    $param['beginTime'] = $date_val.' 00:00:00';
                    $param['endTime'] = date('Y-m-d 00:00:00', (strtotime($date_val) + 86400));
                    $data[$key]['trend'][$date_key]['total'] = GroupModel::getVipGroupByTime($param);
                }
            }
        }

        return $data;
    }


    /**
     * 社群数量增长统计图
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupProportionGraph($param)
    {
        $channel_data = ChannelData::getChannelDataList();

        $param['limitNum'] = '360';
        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-60 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        //总社群数量
        $groupTotalNum = GroupModel::getGroupTotalNum($param, 'total');
        if(!empty($channel_data)){
            foreach($channel_data as $key => $val) {
                $param['platform_identity'] = $val['platform_identity'];
                $groupNum = GroupModel::getGroupTotalNum($param, 'total');
                $returnData[] = [
                    'sourceName' => $val['channel_name'],
                    'totalNum' => $groupNum,
                    'numProportion' => $groupTotalNum ? round($groupNum / $groupTotalNum * 100, 2) : 0 . '%'
                ];
            }
            return $returnData;
        }
    }


    /*
     * 数据统计
     */
    public static function getDataStatistics($param, $member = [])
    {
        //排序处理
        if (isset($param['orderByName']) && !empty($param['orderByName'])) {
            if (isset($param['sortType']) && !empty($param['sortType']) && in_array($param['sortType'], ['asc', 'desc'])) {
                $param[$param['orderByName']] = $param['sortType'];
            }
        }
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            switch ($param['channel_name']) {
                case 'yt':
                    $platform_identity = '7323ff1a23f0bf1cda41f690d4089353';
                    break;
                case 'dr':
                    $platform_identity = '89dafaaf53c4eb68337ad79103b36aff';
                    break;
                case 'mt':
                    $platform_identity = '183ade9bc5f1c4b52c16072362bb21d1';
                    break;
                case 'zd':
                    $platform_identity = '75f712af51d952af3ab4c591213dea13';
                    break;
                case 'ljt':
                    $platform_identity = 'c23ca372624d226e713fe1d2cd44be35';
                    break;
                default:
                    $platform_identity = '';
                    break;
            }
            $param['platform_identity'] = $platform_identity;
        }
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $num = ($page - 1) * $pageSize;
        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }
        if (isset($param['tag_id']) && !empty($param['tag_id'])) {
            $param['tag_id'] = [$param['tag_id']];
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }

        $data = GroupModel::getGroupListV2($param, $page, $pageSize);
        $count = GroupModel::getGroupListCountV2($param);
        $count = json_decode(json_encode($count), true);
        //以下默认获取一周数据
        if (isset($param['startTime']) && isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
        }
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => &$item) {
                $num++;
                //机器人微信号位置显示机器人名称
                $param['roomId'] = $roomId = $item->id;
                //管理员名称
                $item->num = $num;
                if ($item->bind_mid) {
                    $item->adminName = UserModel::getUserNameByMid($item->bind_mid);
                } else {
                    $item->adminName = UserModel::getUserNameByMid($item->rmid);
                }
                //有效订单
                $item->orderNum = $item->orderNum ?: 0;
                //销售额
                $item->rebate = $item->rebate ? round($item->rebate / 100, 2) : 0;
                $item->orderAmount = $item->orderAmount / 100;
                $item->orderAvgAmount = $item->orderAvgAmount / 100;
                //当前群成员数量
                //$item->groupMemberNum = GroupModel::getGroupUserCount($param);
                $item->groupMemberNum = $item->groupMemberNum ?: 0;

                //消费返佣
                $item->groupCommission = $item->rebate;
                //男性占比
                if (empty($item->groupMemberNum)) {
                    $item->groupManProportion = '0%';
                    $item->groupWomenProportion = '0%';
                } else {
                    $totalMemberCount = GroupModel::getGroupUserCount($param);
                    if ($totalMemberCount) {
                        $manScale = round(GroupModel::getGroupUserCount($param, 'man') / GroupModel::getGroupUserCount($param), 2);
                        //$womenScale = 1 - $manScale;
                        $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / GroupModel::getGroupUserCount($param), 2);
                        $item->groupManProportion = $manScale * 100 > 100 ? 100 . '%' : 0 . '%';
                        //女性占比
                        $item->groupWomenProportion = $womenScale * 100 > 100 ? 100 . '%' : 0 . '%';
                    } else {
                        $item->groupManProportion = 0 . '%';
                        //女性占比
                        $item->groupWomenProportion = 0 . '%';
                    }
                }
                $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';
            }
            unset($param['roomId']);
            //机器人状态
            //$item->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
            $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
            $tag_name = CommunityGroupManageModel::getTagDataName($item->tag_id);
            $item->tag_name = !empty($tag_name) ? $tag_name : '无';
//                $item->band_group = empty($param['sup_id'])?'绑定群主':'';
            if (isset($param['sup_id']) && ($param['sup_id'] == 0 || $member['sup_id'] == 0)) {
                $item->band_group = '绑定群主';
            } else {
                $item->band_group = $item->mid > 0 ? '' : '绑定群主';
            }

        }
        $data = json_decode(json_encode($data), true);
        $return['data'] = array_values($data);
        $return['total'] = $count ? $count[0]['cnt'] : 0;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }



    /*
    * 获取商品统计
     * @param Array
     */
    public static function getSellProductList($param)
    {

        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return ['code' => 400,'msg' => '缺少群ID'];
        }
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;


        $list = AdminGroupManageModel::getProductStatisticsListV2($param, $page, $pageSize);

        $totalCount = AdminGroupManageModel::getProductStatisticsCountV2($param);


        foreach ($list as $key => &$value) {
            if (config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods ') {
                switch ($value->good_type) {
                    case '1':
                        $goodsTypeName = $value->channel == 1 ? '悦淘自营' : '大人自营';
                        break;
                    case '2':
                        $goodsTypeName = '京东';
                        break;
                    case '3':
                        $goodsTypeName = '拼多多';
                        break;
                    case '4':
                        $goodsTypeName = '唯品会';
                        break;
                    case '5':
                        $goodsTypeName = '美团';
                        break;
                    case '7':
                        $goodsTypeName = '淘宝';
                        break;
                    case '8':
                        $goodsTypeName = '苏宁';
                        break;
                    case '9':
                        $goodsTypeName = '品牌折扣券';
                        break;
                    default:
                        $goodsTypeName = '其他';
                        break;
                }
                switch ($value->channel) {
                    case '1':
                        $channelName = '悦淘';
                        break;
                    case '2':
                        $channelName = '大人';
                        break;
                    default:
                        $channelName = '其他';
                        break;
                }
                // $goodsTypeName = empty($value->good_type) ? '自营商品' : '京东商品';
            } else {
                $goodsTypeName = '';
            }

            $value->key = ($page - 1) * $pageSize + $key + 1;
            $value->totalmoney = $value->totalmoney / 100;
            $value->unitPrice = $value->totalmoney / $value->goodsNumber;
            // $value->liren = self::getDistribution($value->goods_id,$value,$value->good_type,$value->goods_price_member,$value->actual_price); //群主利润
            // $value->zLiRun = $value->zLiRun>0?$value->zLiRun:0;             //总的利润
            // $value->liren = round($value->liren, 2);
            // $value->zLiRun = round($value->zLiRun, 2);
            $value->click = $value->clickNum ?: 0;
            unset($value->clickNum);
            //返佣金额没有除以4 为直订, 所以除直订平台外要除以4
            //乘以100 除以一万更精确
            if ($value->platform_identity != '75f712af51d952af3ab4c591213dea13') {
                $value->liren = $value->liren > 0 ? round(floor($value->liren / 4) / 100, 2) : 0;
                $value->zLiRun = $value->zLiRun > 0 ? round(floor($value->zLiRun / 4) / 100, 2) : 0;
            } else {
                $value->liren = $value->liren > 0 ? round(floor($value->liren) / 100, 2) : 0;
                $value->zLiRun = $value->zLiRun > 0 ? round(floor($value->zLiRun) / 100, 2) : 0;
            }

            $value->goodsTypeName = $goodsTypeName;
            $value->channelName = $channelName;
            $value->goods_price_orig = $value->goods_price_orig / 100;
            $value->actual_price = $value->actual_price / 100;
            //这里会员价显示实付款金额, 6.5 庞敬龙 改
            $value->goods_price_member = $value->goods_price_orig <= $value->actual_price
                ? round($value->actual_price, 2)
                : round($value->actual_price / $value->goods_num, 2);

        }

        return ['page' => $page, 'pageSize' => $pageSize, 'total' => $totalCount, 'data' => $list];
    }



    /**
     * 获取购买的订单列表
     */
    public static function getGroupOrderList($param = [])
    {
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return ['code' => 400,'msg' => '缺少群ID'];
        }
        $wheredata=[];
        $wheredata['page'] = $page = isset($param['page']) && !empty($param['page']) ? $param['page'] : 1;
        $wheredata['pageSize'] = $pageSize = isset($param['pageSize']) && !empty($param['pageSize']) ? $param['pageSize'] : 10;
        $wheredata['offer'] = ($wheredata['page'] -1 ) * $wheredata['pageSize'];
        $wheredata['groupName'] = isset($param['groupName']) && !empty($param['groupName']) ? $param['groupName'] : '';
        $wheredata['goodsName'] = isset($param['goodsName']) && !empty($param['goodsName']) ? $param['goodsName'] : '';
        $wheredata['buyerPhone'] = isset($param['buyerPhone']) && !empty($param['buyerPhone']) ? $param['buyerPhone'] : '';
        $wheredata['goodType'] = isset($param['goodType']) && !empty($param['goodType']) ? $param['goodType'] : '';
        $wheredata['startTime'] = isset($param['startTime']) && !empty($param['startTime']) ? $param['startTime'] : '';
        $wheredata['endTime'] = isset($param['endTime']) && !empty($param['endTime']) ? $param['endTime'] : '';
        $wheredata['groupMid'] = isset($param['groupMid']) && !empty($param['groupMid']) ? $param['groupMid'] : '';
        $wheredata['orderFrom'] = isset($param['orderFrom']) && !empty($param['orderFrom']) ? $param['orderFrom'] : '';
        $wheredata['channel'] = isset($param['channel']) && !empty($param['channel']) ? $param['channel'] : '';
        $wheredata['ordersn'] = isset($param['ordersn']) && !empty($param['ordersn']) ? $param['ordersn'] : '';
        $wheredata['roomId'] = isset($param['roomId']) && !empty($param['roomId']) ? $param['roomId'] : '';
        $wheredata['groupNameOrderBy'] = isset($param['groupNameOrderBy']) && !empty($param['groupNameOrderBy']) ? $param['groupNameOrderBy'] : '';


        $orderGoodsCount = CommunityGroupManageModel::getOrderListCountV2($wheredata);

        $orderGoodsList = CommunityGroupManageModel::getOrderListDataV2($wheredata);
        $orderGoodsListArray = [];
        foreach ($orderGoodsList as $k => $v) {

            if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
                switch ($v->good_type) {
                    case '1':
                        $goodsTypeName = $v->channel == 1 ? '悦淘自营' : '大人自营';
                        break;
                    case '2':
                        $goodsTypeName = '京东';
                        break;
                    case '3':
                        $goodsTypeName = '拼多多';
                        break;
                    case '4':
                        $goodsTypeName = '唯品会';
                        break;
                    case '5':
                        $goodsTypeName = '美团';
                        break;
                    case '7':
                        $goodsTypeName = '淘宝';
                        break;
                    case '8':
                        $goodsTypeName = '苏宁';
                        break;
                    case '9':
                        $goodsTypeName = '品牌折扣券';
                        break;
                    default:
                        $goodsTypeName = '其他';
                        break;
                }
            }else{
                $goodsTypeName = '';
            }
            $orderGoodsListArray[$k]['recordId'] = $v->record_id;
            $orderGoodsListArray[$k]['ordersn'] = $v->ordersn;
            $orderGoodsListArray[$k]['buyerNickname'] =  $v->nickname;
            $orderGoodsListArray[$k]['buyerPhone'] =  $v->mobile;
            $payType = $v->pay_type == 1 ? '微信支付' : $v->pay_type == 2 ? '支付宝' : $v->pay_type == 3 ? '小程序支付' : '微信支付';
            $orderGoodsListArray[$k]['payType'] =  $payType;
            $orderGoodsListArray[$k]['goodsName'] = $v->goods_name;
            $orderGoodsListArray[$k]['actualPrice'] = $v->actual_price / 100;
            $orderGoodsListArray[$k]['orderTime'] = date('Y-m-d H:i:s', $v->created_at);
            $orderGoodsListArray[$k]['groupName'] = $v->groupName;
            $orderGoodsListArray[$k]['roomId'] = $v->roomId;
            $orderGoodsListArray[$k]['robotName'] = $v->wx_name;
            $orderGoodsListArray[$k]['wxid'] = $v->wxid;
            $orderGoodsListArray[$k]['manageName'] = $v->name;
            $orderGoodsListArray[$k]['goodTypeName'] = $goodsTypeName;
            $orderGoodsListArray[$k]['orderFrom'] = $v->order_from == 1 ? '社群' : 'APP';
            $orderGoodsListArray[$k]['channelName'] = $v->channel == 1 ? '悦淘' : '大人';
        }

        $return = [];
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        $return['total'] = $orderGoodsCount;
        $return['data'] = $orderGoodsListArray;

        return $return;
    }


    /**
     * 获取购买的订单列表
     */
    public static function getGroupAdminGroupOrderList($param = [])
    {
        if (!isset($param['groupAdminMid']) || empty($param['groupAdminMid'])) {
            return ['code' => 400,'msg' => '缺少群主ID'];
        }
        $wheredata=[];
        $wheredata['page'] = $page = isset($param['page']) && !empty($param['page']) ? $param['page'] : 1;
        $wheredata['pageSize'] = $pageSize = isset($param['pageSize']) && !empty($param['pageSize']) ? $param['pageSize'] : 10;
        $wheredata['offer'] = ($wheredata['page'] -1 ) * $wheredata['pageSize'];
        $wheredata['groupName'] = isset($param['groupName']) && !empty($param['groupName']) ? $param['groupName'] : '';
        $wheredata['goodsName'] = isset($param['goodsName']) && !empty($param['goodsName']) ? $param['goodsName'] : '';
        $wheredata['buyerPhone'] = isset($param['buyerPhone']) && !empty($param['buyerPhone']) ? $param['buyerPhone'] : '';
        $wheredata['goodType'] = isset($param['goodType']) && !empty($param['goodType']) ? $param['goodType'] : '';
        $wheredata['startTime'] = isset($param['startTime']) && !empty($param['startTime']) ? $param['startTime'] : '';
        $wheredata['endTime'] = isset($param['endTime']) && !empty($param['endTime']) ? $param['endTime'] : '';
        $wheredata['groupMid'] = isset($param['groupAdminMid']) && !empty($param['groupAdminMid']) ? $param['groupAdminMid'] : '';
        $wheredata['orderFrom'] = isset($param['orderFrom']) && !empty($param['orderFrom']) ? $param['orderFrom'] : '';
        $wheredata['channel'] = isset($param['channel']) && !empty($param['channel']) ? $param['channel'] : '';
        $wheredata['ordersn'] = isset($param['ordersn']) && !empty($param['ordersn']) ? $param['ordersn'] : '';
        $wheredata['roomId'] = isset($param['roomId']) && !empty($param['roomId']) ? $param['roomId'] : '';
        $wheredata['groupNameOrderBy'] = isset($param['groupNameOrderBy']) && !empty($param['groupNameOrderBy']) ? $param['groupNameOrderBy'] : '';


        $orderGoodsCount = CommunityGroupManageModel::getOrderListCountV2($wheredata);

        $orderGoodsList = CommunityGroupManageModel::getOrderListDataV2($wheredata);
        $orderGoodsListArray = [];
        foreach ($orderGoodsList as $k => $v) {

            if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
                switch ($v->good_type) {
                    case '1':
                        $goodsTypeName = $v->channel == 1 ? '悦淘自营' : '大人自营';
                        break;
                    case '2':
                        $goodsTypeName = '京东';
                        break;
                    case '3':
                        $goodsTypeName = '拼多多';
                        break;
                    case '4':
                        $goodsTypeName = '唯品会';
                        break;
                    case '5':
                        $goodsTypeName = '美团';
                        break;
                    case '7':
                        $goodsTypeName = '淘宝';
                        break;
                    case '8':
                        $goodsTypeName = '苏宁';
                        break;
                    case '9':
                        $goodsTypeName = '品牌折扣券';
                        break;
                    default:
                        $goodsTypeName = '其他';
                        break;
                }
            }else{
                $goodsTypeName = '';
            }
            $orderGoodsListArray[$k]['recordId'] = $v->record_id;
            $orderGoodsListArray[$k]['ordersn'] = $v->ordersn;
            $orderGoodsListArray[$k]['buyerNickname'] =  $v->nickname;
            $orderGoodsListArray[$k]['buyerPhone'] =  $v->mobile;
            $payType = $v->pay_type == 1 ? '微信支付' : $v->pay_type == 2 ? '支付宝' : $v->pay_type == 3 ? '小程序支付' : '微信支付';
            $orderGoodsListArray[$k]['payType'] =  $payType;
            $orderGoodsListArray[$k]['goodsName'] = $v->goods_name;
            $orderGoodsListArray[$k]['actualPrice'] = $v->actual_price / 100;
            $orderGoodsListArray[$k]['orderTime'] = date('Y-m-d H:i:s', $v->created_at);
            $orderGoodsListArray[$k]['groupName'] = $v->groupName;
            $orderGoodsListArray[$k]['roomId'] = $v->roomId;
            $orderGoodsListArray[$k]['robotName'] = $v->wx_name;
            $orderGoodsListArray[$k]['wxid'] = $v->wxid;
            $orderGoodsListArray[$k]['manageName'] = $v->name;
            $orderGoodsListArray[$k]['goodTypeName'] = $goodsTypeName;
            $orderGoodsListArray[$k]['orderFrom'] = $v->order_from == 1 ? '社群' : 'APP';
            $orderGoodsListArray[$k]['channelName'] = $v->channel == 1 ? '悦淘' : '大人';
        }

        $return = [];
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        $return['total'] = $orderGoodsCount;
        $return['data'] = $orderGoodsListArray;

        return $return;
    }
    /*
     * 社群排行--群统计详情
     */
    public static function groupStatisticsDetail($param, $member = [])
    {
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            switch ($param['channel_name']) {
                case 'yt':
                    $platform_identity = '7323ff1a23f0bf1cda41f690d4089353';
                    break;
                case 'dr':
                    $platform_identity = '89dafaaf53c4eb68337ad79103b36aff';
                    break;
                case 'mt':
                    $platform_identity = '183ade9bc5f1c4b52c16072362bb21d1';
                    break;
                case 'zd':
                    $platform_identity = '75f712af51d952af3ab4c591213dea13';
                    break;
                case 'ljt':
                    $platform_identity = 'c23ca372624d226e713fe1d2cd44be35';
                    break;
                default:
                    $platform_identity = '';
                    break;
            }
            $param['platform_identity'] = $platform_identity;
        }

        if (isset($param['tag_id']) && !empty($param['tag_id'])) {
            $param['tag_id'] = [$param['tag_id']];
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                $return['data'] = [];
                return $return;
            }
        }

        $detail = GroupModel::getGroupDetailV2($param);
        //机器人微信号位置显示机器人名称
        $param['roomId'] = $roomId = $detail->id;
        //管理员名称
        if ($detail->bind_mid) {
            $detail->adminName = UserModel::getUserNameByMid($detail->bind_mid);
            $channel_name = UserModel::getPlatformIdentityByMid($detail->bind_mid);
        } else {
            $detail->adminName = UserModel::getUserNameByMid($detail->rmid);
            $channel_name = UserModel::getPlatformIdentityByMid($detail->rmid);
        }
        $detail->channel_name = $channel_name;
        $tag_name = CommunityGroupManageModel::getTagDataName($detail->tag_id);
        $detail->tag_name = $tag_name;
        //TODO 这里地址没有 通知小娟去掉
        $detail->city = '广东省深圳市';
        //销售商品数
        $detail->sellGoodsNum = '10000';
        //未支付订单
        $detail->noPayOrderNum = '10';
        //退货/退款记录
        $detail->salesReturnNum = '10';
        //退款总额
        $detail->salesReturnAmount = '1000';
        //有效订单
        $detail->orderNum = $detail->orderNum ?: 0;
        //销售额
        $detail->rebate = $detail->rebate ? round($detail->rebate / 100, 2) : 0;
        $detail->orderAmount = $detail->orderAmount / 100;
        $detail->orderAvgAmount = $detail->orderAvgAmount / 100;
        //当前群成员数量
        //$detail->groupMemberNum = GroupModel::getGroupUserCount($param);
        $detail->groupMemberNum = $detail->groupMemberNum ?: 0;

        //消费返佣
        $detail->groupCommission = $detail->rebate;
        //男性占比
        if (empty($detail->groupMemberNum)) {
            $detail->groupManProportion = '0%';
            $detail->groupWomenProportion = '0%';
        } else {
            $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $detail->groupMemberNum, 2);
            //$womenScale = 1 - $manScale;
            $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $detail->groupMemberNum, 2);
            $detail->groupManProportion = $manScale * 100 . '%';
            //女性占比
            $detail->groupWomenProportion = $womenScale * 100 . '%';
        }
        $detail->deleted_at = $detail->deleted_at > 0 ? "关闭" : '开启';

        //机器人状态
        $detail->robotStatus = $detail->robot_status == 1 ? '开启' : '关闭';
        //$tag_name = CommunityGroupManageModel::getTagDataName($detail->tag_id);
        //$detail->tag_name = !empty($tag_name) ? $tag_name : '无';
        if (isset($param['sup_id']) && ($param['sup_id'] == 0 || $member['sup_id'] == 0)) {
            $detail->band_group = '绑定群主';
        } else {
            $detail->band_group = $detail->mid > 0 ? '' : '绑定群主';
        }


        return json_decode(json_encode($detail), true);
    }


    /*
    * 数据统计
    */
    public static function groupAdminStatistics($param)
    {
        //排序处理
        if (isset($param['orderByName']) && !empty($param['orderByName'])) {
            if (isset($param['sortType']) && !empty($param['sortType']) && in_array($param['sortType'], ['asc', 'desc'])) {
                $param[$param['orderByName']] = $param['sortType'];
            }
        }
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            switch ($param['channel_name']) {
                case 'yt':
                    $platform_identity = '7323ff1a23f0bf1cda41f690d4089353';
                    break;
                case 'dr':
                    $platform_identity = '89dafaaf53c4eb68337ad79103b36aff';
                    break;
                case 'mt':
                    $platform_identity = '183ade9bc5f1c4b52c16072362bb21d1';
                    break;
                case 'zd':
                    $platform_identity = '75f712af51d952af3ab4c591213dea13';
                    break;
                case 'ljt':
                    $platform_identity = 'c23ca372624d226e713fe1d2cd44be35';
                    break;
                default:
                    $platform_identity = '';
                    break;
            }
            $param['platform_identity'] = $platform_identity;
        }
        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        $begin = ($page-1) * $pageSize;
        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                $param['roomId'] = [];
            }
        }

        if (isset($param['startTime']) && isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
        }
        $data = GroupModel::getAdminGroupListV2($param, $begin, $pageSize);
        $count = GroupModel::getAdminGroupListCountV2($param);
        $count = json_decode(json_encode($count), true);

        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => &$item) {
                $item->num = ($page - 1) * $pageSize + $key + 1;
                //管理员名称--TODO 后台显示手机号
                $memberInfo = UserModel::getMemberInfoByMid($item->groupMid);
                $item->groupAdminName = isset($memberInfo['truename']) && !empty($memberInfo['truename'])
                    ? $memberInfo['truename'] : isset($memberInfo['nickname']) && !empty($memberInfo['nickname'])
                        ? $memberInfo['nickname'] : '暂无昵称';
                //群名称
                $item->name = self::getAdminstrByType($item->wxid_type, $item->rmid, $item->bind_mid);
                //有效订单
                $item->orderNum = $item->orderNum ? $item->orderNum : 0;
                //销售额
                $item->orderAmount = $item->orderAmount ? $item->orderAmount / 100 : 0;
                //当前群成员数量
                $item->groupMemberNum = GroupModel::getGroupUserCount($param);
                //预估佣金

                if ($item->channel == 4) {

                    $item->estCommission = $item->estCommission > 0 ? sprintf("%.2f", $item->estCommission / 100 * 4) : 0;

                } else {

                    $item->estCommission = $item->estCommission > 0 ? sprintf("%.2f", $item->estCommission / 100) : 0;
                }
                $item->limitGroupCount = CommunityGroupManageModel::getlimitgroupCount($item->rmid?$item->rmid:0) ?: 0;
                //$item->growthValue = ceil($item->estCommission * 2);
                $item->orderNum = $item->orderNum ?: 0;
                //消费返佣
                if (empty($item->orderNum)) {
                    $item->groupCommission = 0;
                } else {
                    $item->groupCommission = $item->groupCommission > 0 ? sprintf("%.2f", $item->groupCommission / $item->orderNum) : 0;
                }
//                        $item->groupCommission = GroupModel::getAdminGroupTrueAmount($item->ids,$item->groupMid);
                //男性占比
                if (empty($item->groupMemberNum)) {
                    $item->groupManProportion = '0%';
                    $item->groupWomenProportion = '0%';
                } else {
                    $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                    //$womenScale = 1 - $manScale;
                    $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                    $item->groupManProportion = $manScale * 100 . '%';
                    //女性占比
                    $item->groupWomenProportion = $womenScale * 100 . '%';
                }
                $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';
                $item->plant_name = 'wehup';
            }
            unset($param['roomId']);
            //机器人状态
            //$item->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
            //TODO
            $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';

        }


        $data = json_decode(json_encode($data), true);
        $return['data'] = array_values($data);
        $return['total'] = $count ? $count[0]['cnt'] : 0;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }
    /*
    * 群主排行详情--群主详情信息
    */
    public static function groupAdminStatisticsDetail($param)
    {
        if (!isset($param['groupAdminMid']) || empty($param['groupAdminMid'])) {
            return ['code' => 400, 'msg' => '缺少群主ID'];
        }
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            switch ($param['channel_name']) {
                case 'yt':
                    $platform_identity = '7323ff1a23f0bf1cda41f690d4089353';
                    break;
                case 'dr':
                    $platform_identity = '89dafaaf53c4eb68337ad79103b36aff';
                    break;
                case 'mt':
                    $platform_identity = '183ade9bc5f1c4b52c16072362bb21d1';
                    break;
                case 'zd':
                    $platform_identity = '75f712af51d952af3ab4c591213dea13';
                    break;
                case 'ljt':
                    $platform_identity = 'c23ca372624d226e713fe1d2cd44be35';
                    break;
                default:
                    $platform_identity = '';
                    break;
            }
            $param['platform_identity'] = $platform_identity;
        }


        $data = GroupModel::getAdminGroupListV3($param);


        //管理员名称--TODO 后台显示手机号
        $memberInfo = UserModel::getMemberInfoByMid($data->groupMid);
        $data->groupAdminName = isset($memberInfo['truename']) && !empty($memberInfo['truename'])
            ? $memberInfo['truename'] : isset($memberInfo['nickname']) && !empty($memberInfo['nickname'])
                ? $memberInfo['nickname'] : '暂无昵称';
        //群名称
        $data->name = self::getAdminstrByType($data->wxid_type, $data->rmid, $data->bind_mid);
        //有效订单
        $data->orderNum = $data->orderNum ? $data->orderNum : 0;
        //销售额
        $data->orderAmount = $data->orderAmount ? $data->orderAmount / 100 : 0;
        $data->orderAvgAmount = $data->orderAvgAmount / 100;
        $data->userTypeName = '普通会员';
        //当前群成员数量
        $data->groupMemberNum = GroupModel::getGroupUserCount($param);
        //预估佣金

        if ($data->channel == 4) {

            $data->estCommission = $data->estCommission > 0 ? sprintf("%.2f", $data->estCommission / 100 * 4) : 0;

        } else {

            $data->estCommission = $data->estCommission > 0 ? sprintf("%.2f", $data->estCommission / 100) : 0;
        }
        $data->limitGroupCount = CommunityGroupManageModel::getlimitgroupCount($data->rmid ? $data->rmid : 0) ?: 0;
        //$data->growthValue = ceil($data->estCommission * 2);
        $data->orderNum = $data->orderNum ?: 0;

        //TODO
        $data->channel_name = '悦淘';
        $data->sex = '男';
        $data->city = '广东省深圳市';
        $data->register_time = '2017-07-24 17:25:38';
        $data->platform_name = 'wehub';
        //分享商品数
        $data->shareGoodsNum = '10000';
        //未支付订单
        $data->noPayOrderNum = '10';
        //退货/退款记录
        $data->salesReturnNum = '10';
        //退款总额
        $data->salesReturnAmount = '1000';

        //消费返佣
        if (empty($data->orderNum)) {
            $data->groupCommission = 0;
        } else {
            $data->groupCommission = $data->groupCommission > 0 ? sprintf("%.2f", $data->groupCommission / $data->orderNum) : 0;
        }
        $data->deleted_at = $data->deleted_at > 0 ? "关闭" : '开启';

        unset($param['roomId']);
        //机器人状态
        //$data->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
        //TODO
        $data->robotStatus = $data->robot_status == 1 ? '开启' : '关闭';


        return json_decode(json_encode($data), true);
    }



    public static function getAdminstrByType($type=1,$mid=0,$bind_mid=0)
    {

        $str = "";

        switch ($type) {
            case 1:
                $mid = $bind_mid>0?$bind_mid:$mid;
                $nickname = AdminGroupManageModel::getNickNameByMid($mid);
                $str .= $nickname;
                break;
            case 2:
                $nickname = AdminGroupManageModel::getNickNameByMid($mid);
                $str .= $nickname;
                break;
            default:
                $nickname = AdminGroupManageModel::getNickNameByMid($bind_mid);
                $str .= $nickname;
                break;
        }

        return $str;
    }


    /**
     * 社群人数占比统计
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupMemberProportionGraph($param)
    {
        $channel_data = ChannelData::getChannelDataList();

        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        }
//        else {
//            //查询开始时间
//            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-60 day')) . '00:00:00');
//            $param['endTime'] = $param['endTime'] ?? strtotime('today');
//        }
        //总社群数量
        $groupTotalNum = GroupModel::getNewGroupTotalNum($param);

        $groupHaveOrderNum = self::getGroupHaveOrderNum($param);
        if(!empty($channel_data)){
            foreach($channel_data as $key => $val) {
                $param['platform_identity'] = $val['platform_identity'];
                $groupNum = GroupModel::getNewGroupTotalNum($param);
                $haveOrderNum = self::getGroupHaveOrderNum($param);
                $returnData[] = [
                    'sourceName' => $val['channel_name'],
                    'totalNum' => $groupNum,
                    'numProportion' => $groupTotalNum ? round($groupNum / $groupTotalNum * 100, 2) : 0 . '%',
                    'haveOrderNum' => $haveOrderNum,
                    'haveOrderProportion' => $groupHaveOrderNum ? round($haveOrderNum / $groupHaveOrderNum * 100, 2) : 0 . '%',
                ];
            }
            return $returnData;
        }
    }


    /**
     * 社群数量增长统计图
     * @param $param
     * @param $member
     * @return array
     */
    public static function sexProportionGraph($param)
    {
        $sexData = [
            [
                'sex' => 1,
                'name' => '男',
            ],
            [
                'sex' => 2,
                'name' => '女',
            ],
        ];

        if (isset($param['beginTime']) || isset($param['endTime'])) {
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-60 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        //总社群数量
        $groupTotalNum = GroupModel::getNewGroupTotalNum($param, 'total');

        $groupHaveOrderNum = self::getGroupHaveOrderNum($param, 'total');
        if(!empty($sexData)){
            foreach($sexData as $key => $val) {
                $param['sex'] = $val['sex'];
                $groupNum = GroupModel::getNewGroupTotalNum($param, 'total');
                $haveOrderNum = self::getGroupHaveOrderNum($param, 'total');
                $returnData[] = [
                    'sourceName' => $val['name'],
                    'totalNum' => $groupNum,
                    'numProportion' => $groupTotalNum ? round($groupNum / $groupTotalNum * 100, 2) : 0 . '%',
                    'haveOrderNum' => $haveOrderNum,
                    'haveOrderProportion' => $groupHaveOrderNum ? round($haveOrderNum / $groupHaveOrderNum * 100, 2) : 0 . '%',
                ];
            }
            return $returnData;
        }
    }

    public static function getGroupHaveOrderNum($param, $total = '')
    {
        //存在订单的用户数量
        $param['haveOrder'] = 1;
        return GroupModel::getNewGroupTotalNum($param, $total);
    }

    /**
     * 悦呗专属机器人列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupMemberNumGraph($param)
    {
        $channel_data = ChannelData::getChannelDataList();

        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-60 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $todoReturn = [];
        if (!empty($channel_data)) {
            foreach ($channel_data as $key => $val) {
                $param['platform_identity'] = $val['platform_identity'];
                //新增总人数
                $memberTotalNum = GroupModel::getGroupMemberDate($param, $format);
                $todoReturn[$val['channel_name']] = json_decode(json_encode($memberTotalNum), true);
            }
        }
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }



    /**
     * 社群交易统计图数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function orderGMVCountGraph($param)
    {
        $channel_data = ChannelData::getChannelDataList();

        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-30 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $todoReturn = [];
        if (!empty($channel_data)) {
            foreach ($channel_data as $key => $val) {
                $param['platform_identity'] = $val['platform_identity'];
                //新增总人数
                $memberTotalNum = GroupModel::getOrderTotalDate($param, '', $format);
                $todoReturn[$val['channel_name']] = json_decode(json_encode($memberTotalNum), true);
            }
        }
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }


    /**
     * 社群交易统计图数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupOrderGMVDetailGraph($param)
    {
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return ['code' => 400,'msg' => '缺少群ID'];
        }

//        $platform_identity = GroupModel::getPlantFormNameByRoomId($param['roomId']);
//        switch ($platform_identity) {
//            case '7323ff1a23f0bf1cda41f690d4089353':
//                $channel_name = '悦淘';
//                break;
//            case '89dafaaf53c4eb68337ad79103b36aff':
//                $channel_name = '大人';
//                break;
//            case '183ade9bc5f1c4b52c16072362bb21d1':
//                $channel_name = '迈图';
//                break;
//            case '75f712af51d952af3ab4c591213dea13':
//                $channel_name = '直订';
//                break;
//            case 'c23ca372624d226e713fe1d2cd44be35':
//                $channel_name = '邻居团';
//                break;
//            default:
//                $channel_name = '悦淘';
//                break;
//        }
        $channel_name = '悦淘';
        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-30 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $todoReturn = [];
        $memberTotalNum = GroupModel::getOrderTotalDate($param, '', $format);
        $todoReturn[$channel_name] = json_decode(json_encode($memberTotalNum), true);
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }



    /**
     * 群主交易统计图数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupAdminOrderGMVGraph($param)
    {
        if (!isset($param['groupAdminMid']) || empty($param['groupAdminMid'])) {
            return ['code' => 400,'msg' => '缺少群主ID'];
        }
//        $platform_identity = GroupModel::getPlantFormNameByGroupAdminMid($param['groupAdminMid']);
//        switch ($platform_identity) {
//            case '7323ff1a23f0bf1cda41f690d4089353':
//                $channel_name = '悦淘';
//                break;
//            case '89dafaaf53c4eb68337ad79103b36aff':
//                $channel_name = '大人';
//                break;
//            case '183ade9bc5f1c4b52c16072362bb21d1':
//                $channel_name = '迈图';
//                break;
//            case '75f712af51d952af3ab4c591213dea13':
//                $channel_name = '直订';
//                break;
//            case 'c23ca372624d226e713fe1d2cd44be35':
//                $channel_name = '邻居团';
//                break;
//            default:
//                $channel_name = '悦淘';
//                break;
//        }
        $channel_name = '悦淘';

        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-30 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $todoReturn = [];
        $memberTotalNum = GroupModel::getOrderTotalDate($param, '', $format);
        $todoReturn[$channel_name] = json_decode(json_encode($memberTotalNum), true);
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }



    /**
     * 订单数量走势图
     * @param $param
     * @param $member
     * @return array
     */
    public static function orderNumCountGraph($param)
    {
        $channel_data = ChannelData::getChannelDataList();

        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-30 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $todoReturn = [];
        if (!empty($channel_data)) {
            foreach ($channel_data as $key => $val) {
                $param['platform_identity'] = $val['platform_identity'];
                //新增总人数
                $memberTotalNum = GroupModel::getOrderNumTotalDate($param, '', $format);
                $todoReturn[$val['channel_name']] = json_decode(json_encode($memberTotalNum), true);
            }
        }
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }




    /**
     * 订单数量走势图
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupOrderNumDetailGraph($param)
    {
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return ['code' => 400,'msg' => '缺少群ID'];
        }
//        $platform_identity = GroupModel::getPlantFormNameByRoomId($param['roomId']);
//        switch ($platform_identity) {
//            case '7323ff1a23f0bf1cda41f690d4089353':
//                $channel_name = '悦淘';
//                break;
//            case '89dafaaf53c4eb68337ad79103b36aff':
//                $channel_name = '大人';
//                break;
//            case '183ade9bc5f1c4b52c16072362bb21d1':
//                $channel_name = '迈图';
//                break;
//            case '75f712af51d952af3ab4c591213dea13':
//                $channel_name = '直订';
//                break;
//            case 'c23ca372624d226e713fe1d2cd44be35':
//                $channel_name = '邻居团';
//                break;
//            default:
//                $channel_name = '悦淘';
//                break;
//        }
        $channel_name = '悦淘';
        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-30 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $memberTotalNum = GroupModel::getOrderNumTotalDate($param, '', $format);
        $todoReturn[$channel_name] = json_decode(json_encode($memberTotalNum), true);

        //dd($todoReturn);
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }



    /**
     * 订单数量走势图
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupAdminOrderDetailGraph($param)
    {
        if (!isset($param['groupAdminMid']) || empty($param['groupAdminMid'])) {
            return ['code' => 400,'msg' => '缺少群ID'];
        }
//        $platform_identity = GroupModel::getPlantFormNameByGroupAdminMid($param['groupAdminMid']);
//        switch ($platform_identity) {
//            case '7323ff1a23f0bf1cda41f690d4089353':
//                $channel_name = '悦淘';
//                break;
//            case '89dafaaf53c4eb68337ad79103b36aff':
//                $channel_name = '大人';
//                break;
//            case '183ade9bc5f1c4b52c16072362bb21d1':
//                $channel_name = '迈图';
//                break;
//            case '75f712af51d952af3ab4c591213dea13':
//                $channel_name = '直订';
//                break;
//            case 'c23ca372624d226e713fe1d2cd44be35':
//                $channel_name = '邻居团';
//                break;
//            default:
//                $channel_name = '悦淘';
//                break;
//        }
        $channel_name = '悦淘';
        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-30 day')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }

        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $memberTotalNum = GroupModel::getOrderNumTotalDate($param, '', $format);
        $todoReturn[$channel_name] = json_decode(json_encode($memberTotalNum), true);

        //dd($todoReturn);
        $param['format'] = $format;
        return GroupModel::completionDateArrayV2($param, $todoReturn);
    }

    /**
     * 社群订单渠道构成统计图数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function orderSourceProportionGraph($param)
    {
        $sourceData = [
            '1' => '自营',
            '2' => '京东',
            '3' => '拼多多',
            '4' => '唯品会',
            '5' => '美团',
            //'6' => '自营',
            ///'7' => '肯德基',
            '8' => '苏宁',
            '9' => '品牌折扣券'
        ];

        $orderTotalGMV = GroupModel::getOrderTotalAmount($param, 'total')/100;
        $orderTotalNum = GroupModel::getOrderTotalNum($param, 'total');
        $returnData = [];
        foreach ($sourceData as $key => $value) {
            $param['good_type'] = $key;
            $totalGMV = GroupModel::getOrderTotalAmount($param, 'total')/100;
            $totalNum = GroupModel::getOrderTotalNum($param, 'total');
            $returnData[] = [
                'sourceName' => $value,
                'totalGMV' => $totalGMV,
                'GMVProportion' => $orderTotalGMV ? round($totalGMV / $orderTotalGMV * 100, 2) : 0 . '%',
                'totalNum' => $totalNum,
                'numProportion' => $orderTotalNum ? round($totalNum / $orderTotalNum * 100, 2) : 0 . '%',
            ];
        }
        return $returnData;
    }



    /**
     * 渠道群排行统计图数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupTopGraph($param)
    {
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            switch ($param['channel_name']) {
                case 'yt':
                    $channel_name = '悦淘';
                    $platform_identity = '7323ff1a23f0bf1cda41f690d4089353';
                    break;
                case 'dr':
                    $channel_name = '大人';
                    $platform_identity = '89dafaaf53c4eb68337ad79103b36aff';
                    break;
                case 'mt':
                    $channel_name = '迈图';
                    $platform_identity = '183ade9bc5f1c4b52c16072362bb21d1';
                    break;
                case 'zd':
                    $channel_name = '直订';
                    $platform_identity = '75f712af51d952af3ab4c591213dea13';
                    break;
                case 'ljt':
                    $channel_name = '邻居团';
                    $platform_identity = 'c23ca372624d226e713fe1d2cd44be35';
                    break;
                default:
                    $channel_name = '';
                    $platform_identity = '';
                    break;
            }
            $param['platform_identity'] = $platform_identity;
        }
        $order_type = $param['order_type'] ?? '';
        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
        }
        $param['groupBy'] = 'id';
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            $channel_data[] = [
                'platform_identity' => $platform_identity,
                'channel_name' => $channel_name,
            ];
        } else {
            $channel_data = ChannelData::getChannelDataList();
        }
        switch ($order_type) {
            //交易额
            case 'orderGMV':
                $returnData = GroupModel::getOrderGMVTopData($param);
                break;
            //订单数量
            case 'orderNum':
                $returnData = GroupModel::getOrderNumTopData($param);
                break;
            //群成员数量
            case 'groupMemberNum':
                $returnData = GroupModel::getGroupMemberNumTopData($param);
                break;
            default:
                $returnData = GroupModel::getOrderNumTopData($param);
                break;
        }
        //订单表的channel  渠道 1 悦淘  2 大人 3迈图 4直订 5邻居团
        $returnData = json_decode(json_encode($returnData), true);
        $return = [];
        if (!empty($returnData)) {
            foreach ($returnData as $key => $value) {
                foreach ($channel_data as $k => $v) {
                    $param['room_id'] = $value['id'];
                    switch ($v['channel_name']) {
                        case '悦淘':
                            $channelName = 'ytData';
                            $param['channel'] = 1;
                            break;
                        case '大人':
                            $channelName = 'drData';
                            $param['channel'] = 2;
                            break;
                        case '迈图':
                            $channelName = 'mtData';
                            $param['channel'] = 3;
                            break;
                        case '直订':
                            $channelName = 'zdData';
                            $param['channel'] = 4;
                            break;
                        case '邻居团':
                            $channelName = 'ljtData';
                            $param['channel'] = 5;
                            break;
                        default:
                            $channelName = 'ytData';
                            $param['channel'] = 1;
                            break;
                    }
                    //根据渠道获取当前群的销售额
                    $return[$channelName][$key]['num'] = GroupModel::getOrderNumChannelByGroupAdminId($param);
                    $return[$channelName][$key]['name'] = $value['name'];
                }
            }
        }
        return $return;
    }



    /**
     * 渠道群主排行统计图数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function groupAdminTopGraph($param)
    {
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            switch ($param['channel_name']) {
                case 'yt':
                    $channel_name = '悦淘';
                    $platform_identity = '7323ff1a23f0bf1cda41f690d4089353';
                    break;
                case 'dr':
                    $channel_name = '大人';
                    $platform_identity = '89dafaaf53c4eb68337ad79103b36aff';
                    break;
                case 'mt':
                    $channel_name = '迈图';
                    $platform_identity = '183ade9bc5f1c4b52c16072362bb21d1';
                    break;
                case 'zd':
                    $channel_name = '直订';
                    $platform_identity = '75f712af51d952af3ab4c591213dea13';
                    break;
                case 'ljt':
                    $channel_name = '邻居团';
                    $platform_identity = 'c23ca372624d226e713fe1d2cd44be35';
                    break;
                default:
                    $channel_name = '';
                    $platform_identity = '';
                    break;
            }
            $param['platform_identity'] = $platform_identity;
        }
        $order_type = $param['order_type'] ?? '';
        if (isset($param['startTime']) || isset($param['endTime'])) {
            $param['beginTime'] = $param['startTime'];
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
        }
        if (isset($param['channel_name']) && !empty($param['channel_name'])) {
            $channel_data[] = [
                'platform_identity' => $platform_identity,
                'channel_name' => $channel_name,
            ];
        } else {
            $channel_data = ChannelData::getChannelDataList();
        }
        switch ($order_type) {
            //交易额
            case 'orderGMV':
                $returnData = GroupModel::getOrderGMVTopData($param);
                break;
            //订单数量
            case 'orderNum':
                $returnData = GroupModel::getOrderNumTopData($param);
                break;
            //群成员数量
            case 'groupMemberNum':
                $returnData = GroupModel::getGroupMemberNumTopData($param);
                break;
            default:
                $returnData = GroupModel::getOrderNumTopData($param);
                break;
        }
        //订单表的channel  渠道 1 悦淘  2 大人 3迈图 4直订 5邻居团
        $returnData = json_decode(json_encode($returnData), true);
        $return = [];
        if (!empty($returnData)) {
            foreach ($channel_data as $k => $v) {
            foreach ($returnData as $key => $value) {
                    $param['groupAdminMid'] = $value['mid'];
                    switch ($v['channel_name']) {
                        case '悦淘':
                            $channelName = 'ytData';
                            $param['channel'] = 1;
                            break;
                        case '大人':
                            $channelName = 'drData';
                            $param['channel'] = 2;
                            break;
                        case '迈图':
                            $channelName = 'mtData';
                            $param['channel'] = 3;
                            break;
                        case '直订':
                            $channelName = 'zdData';
                            $param['channel'] = 4;
                            break;
                        case '邻居团':
                            $channelName = 'ljtData';
                            $param['channel'] = 5;
                            break;
                        default:
                            $channelName = 'ytData';
                            $param['channel'] = 1;
                            break;
                    }
                    //根据渠道获取当前群的销售额
                $return[$channelName][$key]['num'] = GroupModel::getOrderNumChannelByGroupAdminId($param);
                $return[$channelName][$key]['name'] = $value['name'];
                }
            }
        }
        return $return;
    }



    /**
     * 悦呗专属机器人列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function getRobotSendGoodList($param)
    {
        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){
            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;
        $result = RobotModel::getRobotSendGoodList($param, $page, $pageSize);
        $result = json_decode(json_encode($result), true);
        if (!empty($result)) {
            $i = ($page-1) * $pageSize + 1;
            $result = array_map(function ($value) use (&$i) {
                $value['product_index'] = $i++;
                //商品类型为4 自营 5 :京东 6: 拼多多
                //自营
                if ($value['product_type'] == 4) {
                    if (!ProductModel::getGoodExistByGoodId($value['product_id'])) {
                        $update  = [
                            'goods_status' => 2
                        ];
                        $where = [
                            'id' => $value['id']
                        ];
                        SendInfoModel::updateRobotSendGoodStatus($update, $where);
                        return '';
                    }
                }
                return $value;
            }, $result);
            $result = array_values(array_filter($result));
            $count  = RobotModel::getRobotSendGoodListCount($param);
        } else {
            $count = 0;
        }
        $return = [];
        $return['data'] = $result;
        $return['total'] = $count;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return [
            "code" => 200,
            "data" => $return
        ];
    }

    /**
     * 悦呗专属机器人列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function getRoomIdsByGroupAdmin($param)
    {
        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){
            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }
        if(!isset($param['mid']) || empty($param['mid'])){
            return ["code"=>400,"msg"=>"群主不能为空"];
        }
        $result = RobotModel::getRoomIdsByGroupAdmin($param);
        $result = $result ? array_column($result, 'id') : [];
        return [
            "code" => 200,
            "data" => $result
        ];
    }

    /**
     * 悦呗专属机器人列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function getExclusiveRobotList($param)
    {

        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){

            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }

        $param['page']                       = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize']                   = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $result = RobotModel::getExclusiveRobotList($param);
        $count  = RobotModel::getExclusiveRobotCount($param);
        $data = RobotModel::getExclusiveRobotDataCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"         => $count,
                "page"          => $param['page'],
                "pageSize"      => $param['pageSize'],
                "group_num"     => empty($data['group_num'])?0:$data['group_num'],
                "member_count"  => empty($data['member_count'])?0:$data['member_count'],
                "order_num"     => empty($data['order_num'])?0:$data['order_num'],
                "order_amount"  => empty($data['order_amount'])?0:$data['order_amount']/100,
                "list"          => self::formatRobotData($result, $param)
            ],
        ];
    }


    /**
     * 悦呗专属机器人搜索列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function getRobotListForSearch($param)
    {

        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){

            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }

        $param['page']                       = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize']                   = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $robotList = RobotModel::getExclusiveRobotListForSelect($param);

        return [
            "code" => 200,
            "data" => [
                "robotList"     => $robotList,
            ],
        ];
    }


    /**
     * 悦呗获取用户群订单数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function getGroupOrderData($param)
    {

        if(!isset($param['mid']) || empty($param['mid'])){

            return ["code"=>400,"msg"=>"用户ID不能为空"];
        }


        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){

            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }

        $groupNum = RobotModel::getGroupNumByMid($param);

        $orderAmount = RobotModel::getOrderAmountByMid($param);

        $robotNum = RobotModel::getRobotNumByMid($param);

        return [
            "code" => 200,
            "msg"  => "获取成功",
            "data" => [
                "robotNum"      => $robotNum,
                "groupNum"      => $groupNum,
                "orderAmount"   => $orderAmount/100
            ],
        ];


    }

    /**
     * 悦呗获取用户群订单数据
     * @param $param
     * @param $member
     * @return array
     */
    public static function getRobotNumData($param)
    {

        if(!isset($param['mid']) || empty($param['mid'])){

            return ["code"=>400,"msg"=>"用户ID不能为空"];
        }


        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){

            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }

        $robotNum = RobotModel::getRobotNumByMid($param);

        return [
            "code" => 200,
            "msg"  => "获取成功",
            "data" => [
                "robotNum"      => $robotNum,
            ],
        ];


    }




    /**
     * 格式化数据
     * @param $data
     */
    public static function formatRobotData($data, $param)
    {
        $returnData = [];
        if (!empty($data)) {
            foreach ($data as $k => $v) {

                $list = $v;
                $list['key'] = ($param['page']-1)+$k+1;
                $list['group_num'] = empty($v['group_num'])?0:$v['group_num'];
                $list['member_count'] = empty($v['member_count'])?0:$v['member_count'];
                $list['order_num'] = empty($v['order_num'])?0:$v['order_num'];
                $list['order_amount'] = empty($v['order_amount'])?0:$v['order_amount']/100;
                $returnData[]              = $list;
            }
        }
        return $returnData;
    }



    /**
     * 悦呗群列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function getExclusiveGroupList($param)
    {

        if(!isset($param['platform_identity']) || empty($param['platform_identity'])){

            return ["code"=>400,"msg"=>"渠道标识不能为空"];
        }

        $param['page']                       = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize']                   = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $result = RobotModel::getExclusiveGroupList($param);
        $data  = RobotModel::getExclusiveGroupCount($param);
        $count = RobotModel::getExclusiveGroupData($param);

        return [
            "code" => 200,
            "data" => [
                "total"         => empty($count['group_num'])?0:$count['group_num'],
                "page"          => $param['page'],
                "pageSize"      => $param['pageSize'],
                "member_count"  => empty($count['member_count'])?0:$count['member_count'],
                "order_num"     => empty($data['order_num'])?0:$data['order_num'],
                "order_amount"  => empty($data['order_amount'])?0:$data['order_amount']/100,
                "list"          => self::formatRobotDataV1($result, $param)
            ],
        ];
    }


    /**
     * 格式化数据
     * @param $data
     */
    public static function formatRobotDataV1($data, $param)
    {
        $returnData = [];
        if (!empty($data)) {
            foreach ($data as $k => $v) {

                $list = $v;
                $list['key'] = ($param['page']-1)+$k+1;
                $list['nick_name'] = empty($v['nick_name'])?"":$v['nick_name'];
                $list['mobile'] = empty($v['mobile'])?"":$v['mobile'];
                $list['group_num'] = empty($v['group_num'])?0:$v['group_num'];
                $list['member_count'] = empty($v['member_count'])?0:$v['member_count'];
                $list['order_num'] = empty($v['order_num'])?0:$v['order_num'];
                $list['order_amount'] = empty($v['order_amount'])?0:$v['order_amount']/100;
                $returnData[]              = $list;
            }
        }
        return $returnData;
    }


    /**
     * @param $code
     * @param $res
     * @param $data
     * @return false|string
     * @author   liuwenhao
     * @time     2020/4/14 12:56
     * @method   method   [返回json格式]
     */
    private static function responseJson($code, $res, $data = [])
    {
        $result = ['code' => $code, 'msg' => $res, 'data' => $data];
        return json_encode($result);
    }


    /**
     * 社群增长趋势图
     *
     * @param $param
     * @return array
     */
    public static function getGrowTrend($param)
    {
        $channel_data = DB::table('channel_data')
            ->select('platform_identity','channel_name')
            ->where('is_delete',0)
            ->get()
            ->toArray();

        $data = [];

        $start_timestamp = strtotime($param['beginTime']);
        $end_timestamp = strtotime($param['endTime']);

        // 计算日期段内有多少天
        $days = ($end_timestamp - $start_timestamp) / 86400 + 1;

        // 保存每天日期
        $date = [];

        for ($i = 0; $i < $days; $i++) {
            $date[] = date('Y-m-d', $start_timestamp + (86400 * $i));
        }

        if (!empty($channel_data)) {
            foreach ($channel_data as $key => $val) {
                foreach ($date as $date_key => $date_val) {
                    $data[$key]['channel_name'] = $val->channel_name;
                    $param['platform_identity'] = $val->platform_identity;
                    $data[$key]['trend'][$date_key]['date'] = $date_val;
                    $param['beginTime'] = $date_val.' 00:00:00';
                    $param['endTime'] = date('Y-m-d 00:00:00', (strtotime($date_val) + 86400));
                    $data[$key]['trend'][$date_key]['total'] = GroupModel::getVipGroupByTime($param);
                }
            }
        }

        return ['code' => 200, 'msg' => 'success', 'data' => $data];
    }

}