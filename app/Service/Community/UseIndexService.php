<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/19
 * Time: 23:14
 */

namespace App\Service\Community;

use App\Model\Community\GroupActivationModel;
use App\Models\ChannelData;
use App\Service\Robot\UserOperationLogService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use  App\Model\Community\CommunityModel;
use  App\Model\Community\GroupGoodsDataModel;
use  App\Model\RobotOrder\RobotModel;
use  App\Model\Admin\AdminUserModel;
use  App\Service\User\LoginClientService;
use  App\Model\User\UserModel;
use  App\Model\Robot\UnitManageModel;

class UseIndexService
{

    /**
     * 获取url
     * @param $platform_identity
     * @return mixed|string
     */
    public static function getPostUrl($platform_identity)
    {
        $PostUrlData = [
            'c23ca372624d226e713fe1d2cd44be35' => 'https://neighbour.yuetao.vip/api/stall/userInfo',
        ];
        if (array_key_exists($platform_identity, $PostUrlData)) {
            return $PostUrlData[$platform_identity];
        }
        return '';

    }

    /**
     * @param $request
     * @return array
     */
    public static function changeIsBuyService($pargram)
    {
        $serviceList = [];
        $is_buy      = 1;
        $payMethod   = [];
        if ($pargram['user_type'] == 1) {
            $isBuy = RobotModel::getUserOrderByMid($pargram['mid']);
            if (empty($isBuy)) {
                $is_buy      = 0;
                $serviceList = RobotModel::getRobotServiceList();
                $payMethod   = [
                    ['type' => 1, 'method' => '微信'],
                    ['type' => 2, 'method' => '支付宝'],
                ];
            }
        }
        return ["code" => 200, 'msg' => '获取成功',
                "data" => ['buyStatus' => $is_buy, 'serviceList' => $serviceList, 'payMethod' => $payMethod]
        ];
    }

    /**
     * 获取我的机器人数据
     */
    public static function getRoBotInfo($param, $member)
    {
        $page                       = isset($param['page']) ? $param['page'] : 1;
        $pageSize                   = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $param['bind_master']       = isset($param['bind_master']) ? $param['bind_master'] : 0;
        $param['bind_group_number'] = isset($param['bind_group_number']) ? $param['bind_group_number'] : 0;
        $param['orderPrice']        = isset($param['orderPrice']) ? $param['orderPrice'] : 0;
        $param['orderCount']        = isset($param['orderCount']) ? $param['orderCount'] : 0;
        if (!isset($param['mid']) || empty($param['mid'])) {
            if ($member['sup_id'] == 0) {
                $userIds       = UserModel::getDownUserIds($member['mid']);
                $userIds[]     = $member['mid'];
                $param['midS'] = $userIds;
            } else {
                $param['midS'] = [$member['mid']];
            }
        } else {
            //TODO  不是所属人员无权查看
            $param['midS'] = [$param['mid']];
        }
        $param['midS'] = json_decode(json_encode($param['midS']), true);
        $result        = RobotModel::getRobotListData($param, ($page - 1) * 10, $pageSize, $member, 1);
        $count         = RobotModel::getRobotCount($param, $member, 1);
        return [
            "code" => 200,
            "data" => [
                "total"    => $count,
                "page"     => $page,
                "pageSize" => $pageSize,
                "list"     => self::formatRobotData($result, $member, $page)
            ],
        ];
    }

    /**
     * 获取我的机器人数据
     */
    public static function getRobotList($param)
    {
        //YCloud平台标识
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $num = ($page-1)*$pageSize;
        $robotList = RobotModel::getNewRobotListData($param, $page, $pageSize);
        $count = RobotModel::getNewRobotCount($param);

        if (is_array($robotList) && count($robotList) > 0) {
            foreach ($robotList as $k => &$robotInfo) {
                $num++;
                $robotInfo->num = $num;
                $robotInfo->bind_group_num = RobotModel::bindNewGroupNumber($robotInfo->wxid);
                if (empty($robotInfo->exclusive_mid)) {
                    $robotInfo->status = '解绑';
                } else {
                    if ($robotInfo->status == 0) {
                        $robotInfo->status = '开启';
                    } else {
                        $robotInfo->status = '关闭';
                    }
                }
            }
        }
        $robotList = json_decode(json_encode($robotList), true);
        $return['data'] = array_values($robotList);
        $return['total'] = $count;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }

    /**
     * 获取我的机器人数据
     */
    public static function updateRobotStatus($param)
    {
        //YCloud平台标识
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        if (!isset($param['robotId']) || empty($param['robotId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        //updateType  1:开启 2:关闭 3:解绑
        if (!isset($param['updateType']) || !in_array($param['updateType'], [1,2,3])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数操作类型'
            ];
        }
        $robotInfo = RobotModel::fetchRobotInfoById($param['robotId']);
        if (empty($robotInfo)) {
            return [
                'code' => 200201,
                'msg' => '机器人信息不存在'
            ];
        }
        //机器人的 status 0 正常 1 删除
        if ($param['updateType'] == 1) {
            $update = [
                'status' => 0
            ];
        } elseif ($param['updateType'] == 2) {
            $update = [
                'status' => 1
            ];
        } else {
            //解绑
            $update = [
                'exclusive_mid' => 0
            ];
        }
        $updateResult = RobotModel::updateRobotInfoById($param['robotId'], $update);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        return $return = [];
    }

    public static function getChannelDataList()
    {
        return ChannelData::getChannelDataList();
    }


    public static function insertRobotInfo($params)
    {
        $insertData = [];
        $insertData['wx_alias'] = $params['wxAlias'] ?? '';
        $insertData['wxid'] = $params['wxid'] ?? '';
        $insertData['wx_name'] = $params['wxName'] ?? '';
        $insertData['platform_identity'] = $params['platformIdentity'] ?? '';
        $insertData['platform'] = $params['platform'] ?? '';
        $insertData['robot_serial_no'] = $params['robotSerialNo'] ?? '';
        $insertData['enterprise_robot_serial_no'] = $params['enterpriseRobotSerialNo'] ?? '';
        $checkRes = self::checkInsertRobotInfoData($insertData);
        if ($checkRes !== true) {
            return $checkRes;
        }
        $result = RobotModel::insertRobotInfo($insertData);
        if ($result === false) {
            return [
                'code' => 200201,
                'msg' => '添加失败'
            ];
        }
        return [];
    }

    public static function checkInsertRobotInfoData($insertData)
    {
        if (!in_array($insertData['platform'], [0,1,2])) {
            return [
                'code' => 200201,
                'msg' => '机器人平台标识参数异常'
            ];
        } else {
            if ($insertData['platform'] == 0 && empty($insertData['wxid'])) {
                return [
                    'code' => 200201,
                    'msg' => '机器人平台标识参数异常'
                ];
            } elseif ($insertData['platform'] == 1 && empty($insertData['robot_serial_no'])) {
                dd(22);
                return [
                    'code' => 200201,
                    'msg' => '机器人平台标识参数异常'
                ];
            } elseif ($insertData['platform'] == 2 && empty($insertData['enterprise_robot_serial_no'])) {
                return [
                    'code' => 200201,
                    'msg' => '机器人平台标识参数异常'
                ];
            }
        }
        return true;
    }

    /**
     * 专属机器人列表
     * @param $param
     * @param $member
     * @return array
     */
    public static function getExclusiveRoBotInfo($param, $member)
    {
        $page                       = isset($param['page']) ? $param['page'] : 1;
        $pageSize                   = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $param['bind_master']       = isset($param['bind_master']) ? $param['bind_master'] : 0;
        $param['bind_group_number'] = isset($param['bind_group_number']) ? $param['bind_group_number'] : 0;
        $param['orderPrice']        = isset($param['orderPrice']) ? $param['orderPrice'] : 0;
        $param['orderCount']        = isset($param['orderCount']) ? $param['orderCount'] : 0;
        if (!isset($param['mid']) || empty($param['mid'])) {
            if ($member['sup_id'] == 0) {
                $userIds       = UserModel::getDownUserIds($member['mid']);
                $userIds[]     = $member['mid'];
                $param['midS'] = $userIds;
            } else {
                $param['midS'] = [$member['mid']];
            }
        } else {
            $param['midS'] = [$param['mid']];
        }
        $param['midS'] = json_decode(json_encode($param['midS']), true);

        $result = RobotModel::getRobotListData($param, ($page - 1) * 10, $pageSize, $member, 3);
        $count  = RobotModel::getRobotCount($param, $member, 3);
        return [
            "code" => 200,
            "data" => [
                "total"    => $count,
                "page"     => $page,
                "pageSize" => $pageSize,
                "list"     => self::formatRobotData($result, $member, $page)
            ],
        ];
    }

    /**
     * 格式化数据
     * @param $data
     */
    public static function formatRobotData($data, $member, $page, $bindNames = [])
    {
        $returnData = [];
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if ($v->bind_mid) {
                    $adminName = RobotModel::robotManagerName($v->bind_mid);
                } else {
                    $adminName = RobotModel::robotManagerName($v->mid);
                }
                //$last_login_time         = RobotModel::lastloginTime($v->wxid);
                $last_login_time         = RobotModel::lastWorkTime($v->wxid);
                $list['last_login_time'] = !empty($last_login_time) ? $last_login_time : '登出';
                $list['id']              = ($page - 1) * 10 + $k + 1;
                $list['code_number']     = $v->order_by . '号';
                $list['wx_alias']        = $v->wx_alias;
                $list['wxid']            = $v->wxid;
                $list['adminName']       = empty($adminName) ? '归属社群' : $adminName->name;
//                $list['bind_master'] =  RobotModel::bindMasterNumber($v->wxid,$member);
                $list['bind_master'] = empty($v->bind_mid_num) ? 0 : $v->bind_mid_num;
//                $list['bind_group_number'] = RobotModel::bindGroupNumber($v->wxid,$member).'|'.RobotModel::bindGroupNumberMid($v->wxid,$member);
                $list['orderCount']        = $v->orderCount;
                $list['orderPrice']        = empty($v->orderPrice) ? 0 : $v->orderPrice / 100;
                $bind_group_num            = empty($v->bind_group_num) ? 0 : $v->bind_group_num;
                $list['bind_group_number'] = $bind_group_num;
                $list['count_num']         = RobotModel::bindGroupNumber($v->wxid, $member);
                $list['status']            = $v->deleted_at == 0 ? 0 : 1;
                $list['on_line']           = (time() - strtotime($last_login_time)) > 60 ? '已下线' : '在线中';
                $list['wx_name']           = !empty($v->wx_name) ? $v->wx_name : '';
                $returnData[]              = $list;
            }
        }
        return $returnData;
    }

    /**
     * 统计数据
     */
    public static function getRobotStatisticsByRobotId($data, $member)
    {
        //开始结束时间
        $data['beginTimeDate'] = isset($data['beginTime']) && $data['beginTime'] > 0 ? date('Y-m-d 00:00:00', $data['beginTime']) : '';
        $data['endTimeDate']   = isset($data['endTime']) && $data['endTime'] > 0 ? date('Y-m-d 00:00:00', $data['endTime'] + 86000) : '';
        $data['beginTime']     = isset($data['beginTime']) && $data['beginTime'] > 0 ? strtotime(date('Y-m-d 00:00:00', $data['beginTime'])) : '';
        $data['endTime']       = isset($data['endTime']) && $data['endTime'] > 0 ? strtotime(date('Y-m-d 00:00:00', $data['endTime'] + 86000)) : '';

        // 获取用户 总的金额总的订单数
        $orderCountMany = RobotModel::getRoomOrderData($data, $member);
        //订单数
        $orderCountNum = RobotModel::getRoomOrderCount($data, $member);
        //总群数
        $groupCountNum = RobotModel::getRoomCountNum($data, $member);
        //总的用户数
        $userCountNum = RobotModel::getUserCountNum($data, $member);
        //新增用户
        $newUserCountNum = RobotModel::getUserCountNum($data, $member, 1);
        //新增群数
        $newGroupCountNum = RobotModel::getRoomCountNum($data, $member, 1);
        // 图标数据

        //每天订单数量
        $startTime  = date("Y-m-d", strtotime("-16 day"));
        $gmvdata    = RobotModel::getEveryDayTotalGMV($data, $member, $startTime);
        $endGmvdata = [];
        foreach ($gmvdata as $k1 => $v1) {
            $endGmvdata[$v1->times] = $v1->totalmoney;
        }

        //每天新增好友数量
        $userdata    = RobotModel::getEveryDayTotalUser($data, $member, $startTime);
        $enduserdata = [];
        foreach ($userdata as $k2 => $v2) {
            $enduserdata[$v2->times] = $v2->count_1;
        }

        //每天发消息数量
        $useradddata    = RobotModel::getUserAddSendInfoData($data, $member, $startTime);
        $enduseradddata = [];
        foreach ($useradddata as $k3 => $v3) {
            $enduseradddata[$v3->times] = $v3->count_add;
        }


        if (empty($data['beginTime'])) {
            $data['beginTime'] = strtotime("-16 day");
            $data['endTime']   = time();
        }
        Log::info('my robot data Info ' . json_encode([$endGmvdata, $enduserdata, $enduseradddata]));
        $endexportGmvdata = RobotModel::completionDate($data, $endGmvdata, $enduserdata, $enduseradddata);


        return [
            "code" => 200,
            "data" => [
                "orderCountMany"   => !empty($orderCountMany) ? (int)$orderCountMany / 100 : 0,
                "orderCountNum"    => !empty($orderCountNum) ? $orderCountNum : 0,
                "groupCountNum"    => $groupCountNum,
                "userCountNum"     => $userCountNum,
                "newUserCountNum"  => $newUserCountNum,
                "newGroupCountNum" => $newGroupCountNum,
                "gmvInfo"          => array_values($endexportGmvdata['data1']),
                "userdata"         => array_values($endexportGmvdata['data2']),
                "msgdata"          => array_values($endexportGmvdata['data3']),
                "enduserdata"      => $enduserdata,

            ],
        ];


    }

    /**
     * @param $param
     * @param $member
     * @return array
     * 当前用户管理的 角色列表
     */
    public static function getUserRoles($param, $member)
    {
        $page                       = !empty($param["page"]) ? $param["page"] : 1;
        $display                    = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param["offset"]            = ($page - 1) * $display;
        $param["display"]           = $display;
        $param['platform_identity'] = $member['platform_identity'];
        $result                     = AdminUserModel::getUserRolesFrontList($param);
        $count                      = AdminUserModel::getUserRolesFrontCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"    => $count,
                "page"     => $page,
                "pageSize" => $display,
                "list"     => self::formatRoles($result)
            ],
        ];
    }

    /**
     * 格式化数据
     * @param $data
     * @return array
     */
    public static function formatRoles($data)
    {
        $list = [];

        foreach ($data as $item) {
            $list[] = [
                "roleId"    => $item->role_id,
                "roleName"  => $item->role_name . '',
                "status"    => $item->status == 1 ? '有效' : '无效',
                "createdAt" => !empty($item->created_at) ? $item->created_at . '' : '',
                "rolePwoer" => !empty($item->role_pwoer) ? $item->role_pwoer . '' : '',
                "updatedAt" => !empty($item->updated_at) ? $item->updated_at : '',
            ];
        }
        return $list;
    }

    public static function checkInputRole($param = [])
    {
        if (!isset($param['roleName']) || empty($param['roleName'])) {
            return ['code' => 400, 'msg' => '角色名称不能为空！'];
        }
    }

    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    public static function addUserRoleFront($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInputRole($param);

        if ($memberInfo['sup_id'] != 0 || $memberInfo['user_type'] < 2) {
            return ['code' => 200, 'msg' => '无权限操作！'];
        }

        if ($check_input) {
            return $check_input;
        }
        $roleInfo = AdminUserModel::getUserFrontRole(['roleName' => $param['roleName']]);

        if ($roleInfo) {
            return ['code' => 400, 'msg' => '此角色名已经有了'];
        }
        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);
        //数据拼接
        $data['mid']               = $memberInfo['mid'];
        $data['role_name']         = $param['roleName'];
        $data['created_at']        = $created_at;
        $data['role_pwoer']        = !empty($param['rolePwoer']) ? $param['rolePwoer'] : '';
        $data['platform_identity'] = $memberInfo['platform_identity'];
        //添加
        $result = AdminUserModel::addFrontRole($data);
        if ($result) {
            return ['code' => 200, 'msg' => '角色添加成功！'];
        } else {
            return ['code' => 200, 'msg' => '角色添加失败！'];
        }
    }


    public static function upUserRoleFront($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInputRole($param);
        if ($check_input) {
            return $check_input;
        }

        if (!isset($param['roleId']) || empty($param['roleId'])) {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $roleInfo = AdminUserModel::getUserRoleFront(['roleName' => $param['roleName'], 'noRoleId' => $param['roleId']]);

        if ($roleInfo) {
            return ['code' => 400, 'msg' => '此角色名已经有了'];
        }

        //当前时间戳
        $timestamp = time();

        $updated_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['role_name'] = $param['roleName'];


        $data['updated_at'] = $updated_at;

        $data['role_pwoer'] = !empty($param['rolePwoer']) ? $param['rolePwoer'] : '';


        $where = ['role_id' => $param['roleId']];

        $result = AdminUserModel::upUserFrontRole($where, $data);

        if ($result) {
            return ['code' => 200, 'msg' => '角色更新成功！'];
        } else {
            return ['code' => 400, 'msg' => '角色更新失败！'];
        }
    }


    public static function delUserRoleFront($memberInfo, $param)
    {

        if (!isset($param['roleId']) || empty($param['roleId'])) {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $roleInfo = AdminUserModel::getUserRoleFront(['roleId' => $param['roleId']]);

        if (empty($roleInfo)) {
            return ['code' => 400, 'msg' => '角色不存在'];
        }
        //当前时间戳
        $timestamp = time();
        //数据拼接

        $data['deleted_at'] = $timestamp;

        $data['status'] = 0;

        $where = ['role_id' => $param['roleId']];

        $result = AdminUserModel::upUserFrontRole($where, $data);

        if ($result) {
            $logMessage = '删除角色';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($data), $logMessage, $param['roleId']
            );
            return ['code' => 200, 'msg' => '角色删除成功！'];
        } else {
            return ['code' => 400, 'msg' => '角色删除失败！'];
        }
    }

    /**
     * 获取 详细
     * @param Array
     * @return Array
     */
    public static function getFrontRole($param)
    {
        if (!isset($param['roleId']) || empty($param['roleId'])) {
            return ['code' => 400, 'msg' => '参数错误！'];
        }
        $result = AdminUserModel::getUserFrontRole($param);

        return [
            "code" => 200,
            "data" => $result,
        ];
    }


    public static function createFrontUser($params, $memberInfo)
    {
        if (empty($params['roleId'])) {
            return ['code' => 400, 'msg' => '请选择一个叫角色！'];
        }
        if ($memberInfo['sup_id'] != 0 || $memberInfo['user_type'] < 2) {
            return ['code' => 400, 'msg' => '无权限操作！'];
        }
        if (isset($params['wxids']) && !empty($params['wxids'])) {
            //判断当前用户机器人
            $isBandCount = RobotModel::isBandWxData($params['wxids'], $memberInfo['mid']);
            if (!empty($isBandCount)) {
                return ['code' => 400, 'msg' => '包含已绑定的机器人！'];
            }
        }
        if ($params['password'] != $params['confirmPassword'] && !empty($params['password'])) {
            return ['code' => 400, 'msg' => '密码设置有误！'];
        }
        if (!isset($params['mobile']) || empty($params['mobile'])) {
            return self::responseJson(400, '手机号不能为空');
        }
        $params['group_id'] = isset($params['group_id']) ? $params['group_id'] : '';
        //验证用户是否存在
        $userData = UserModel::getLoginUser(['mobile' => $params['mobile']]);
        if (!empty($userData)) {
            return self::responseJson(400, '该手机号已存在，请联系管理员');
        }
        $addData = [
            'mobile'   => $params['mobile'],
            'password' => $params['password'],
            'name'     => $params['name'],
        ];
        DB::beginTransaction();
        if ($memberInfo['platform_identity'] == '7323ff1a23f0bf1cda41f690d4089353') {
            //查询悦淘用户是否存在
            $clientUserData = LoginClientService::getClientUser(['mobile' => $params['mobile']]);
            if (empty($clientUserData)) { //不存在，请求注册
                //不存在,同步注册
                $addData1               = $addData;
                $addData1['mobileCode'] = '';
                LoginClientService::register($addData);
            }
            //重新获取用户信息
            $clientUserData         = LoginClientService::getClientUser(['mobile' => $params['mobile']]);
            $addData['mid']         = $clientUserData->mid;
            $addData['nickname']    = $clientUserData->nickname;
            $addData['code_number'] = $clientUserData->codeNumber;
        } else if ($memberInfo['platform_identity'] == '89dafaaf53c4eb68337ad79103b36aff') {
            $userDaren = self::getDarenMidData(['mobile' => $params['mobile']]);
            if (!empty($userDaren)) {
                $addData['mid']         = $userDaren['mid'];
                $addData['nickname']    = $userDaren['nickname'];
                $addData['code_number'] = $userDaren['codeNumber'];
            } else {
                DB::rollBack();
                return self::responseJson(400, '请添加大人用户，注册失败');
            }
        } else if ($memberInfo['platform_identity'] == '75f712af51d952af3ab4c591213dea13') {
            $userZhiding = self::getZhidingMidData(['mobile' => $params['mobile']]);
            if (!empty($userZhiding)) {
                $addData['mid']         = $userZhiding['memberId'];
                $addData['nickname']    = $userZhiding['nickName'];
                $addData['code_number'] = $userZhiding['memberSubDto']['inviteCode'];
            } else {
                DB::rollBack();
                return self::responseJson(400, '请添加直订用户，注册失败');
            }
        } else if ($memberInfo['platform_identity'] == '183ade9bc5f1c4b52c16072362bb21d1') {
            $userZhiding = self::getMaituMidData(['number' => $params['mobile']]);
            if (!empty($userZhiding)) {
                $addData['mid']         = $userZhiding['memberId'];
                $addData['nickname']    = $userZhiding['nickName'];
                $addData['code_number'] = $userZhiding['inviteCode'];
            } else {
                DB::rollBack();
                return self::responseJson(400, '请添加迈途用户，注册失败');
            }
        }else if ($memberInfo['user_type']==2) {
            $userZhiding = self::geUniversalData($memberInfo['platform_identity'],['mobile' => $params['mobile']]);
            if (!empty($userZhiding)) {
                $addData['mid']         = $userZhiding['memberId'];
                $addData['nickname']    = $userZhiding['nickName'];
                $addData['code_number'] = $userZhiding['inviteCode'];
            } else {
                DB::rollBack();
                return self::responseJson(400, '请添加迈途用户，注册失败');
            }
        }
        $addData['sup_id']            = $memberInfo['mid'];
        $addData['front_role_id']     = $params['roleId'];
        $addData['platform_identity'] = $memberInfo['platform_identity'];
        $addData['user_type']         = 2;
        $addData['created_time']      = date("Y-m-d H:i:s");

        //添加用户信息
        UserModel::addUser($addData);
        if (!$addData) {
            DB::rollBack();
            return self::responseJson(400, '注册失败');
        }

        //更新 机器人绑定的的人
        if (!empty($params['wxids'])) {
            $whereData = ['mid' => $memberInfo['mid'], 'updateIds' => explode(',', $params['wxids'])];
            $upData    = ['bind_mid' => $addData['mid']];
            RobotModel::updateRobotbindMidUpdate('white_list_wx', $whereData, $upData);
        }
        if (!empty($params['group_id'])) {
            UnitManageModel::updateGroupStaff(['group_id' => $params['group_id'], 'mid' => $addData['mid'], 'platform_identity' => $memberInfo['platform_identity']]);
        }

        DB::commit();
        return self::responseJson(200, '创建成功');
    }

    public static function getDarenMidData($mobile)
    {

        $result     = curlPostCommon('https://gateway.daren.tech/open/v1/open/v1/home/getUserInfo', ['mobile' => $mobile]);
        $returnData = json_decode($result, true);
        if ($returnData['code'] == 200) {
            return $returnData['data'];
        } else {
            return [];
        }

    }

    /**
     * 直订数据获取
     * @param $mobile
     * @return array
     * @throws \Exception
     */
    public static function getZhidingMidData($mobileData)
    {

        $data = $mobileData;
        $data['mchId'] = 251;
        $res = base64_encode(json_encode($data));
        $result     = requestByPost('http://gateway.yueshang.shop/member/v1/get/member', $res);
        return json_decode(base64_decode($result['response']['data']),true);
        // Log::info('zhiding data', [$returnData]);
        // if ($returnData['code'] == 200) {
        //     return $returnData['data'];
        // } else {
        //     return [];
        // }

    }

    /**
     * 迈图用户查询
     * @param $mobileData
     * @return array
     * @throws \Exception
     */
    public static function getMaituMidData($mobileData)
    {
        $result     = curlPostCommon('https://maitu-web.yuebei.vip/app/member/getMemberInfoByNumber', $mobileData);
        $returnData = json_decode($result, true);
        if ($returnData['code'] == 200) {
            return $returnData['data'];
        } else {
            return [];
        }

    }

    /**
     * 通用的用户查询   mobile   memberId
     * @param $mobileData
     * @return array
     * @throws \Exception
     */
    public static function geUniversalData($platform_identity,$mobileData)
    {
        $postUrl =   self::getPostUrl($platform_identity);
        if(empty($postUrl)){
           return [];
        }
        $result     = curlPostCommon($postUrl, $mobileData);
        $returnData = json_decode($result, true);
        if ($returnData['status'] == 200) {
            return $returnData['data'];
        } else {
            return [];
        }

    }



    /**
     * 获取可分配的机器人
     * @param $data
     * @param $memberInfo
     */
    public static function getRobotWxAliasList($data, $memberInfo)
    {

        $NoBingdWxData = RobotModel::getNoBingdWxData($data, $memberInfo['mid']);

        return self::responseJson(200, '获取成功', $NoBingdWxData);
    }

    /**
     * 用户列表
     * @param $data
     * @param $memberInfo
     * @return false|string
     */
    public static function getUserListByMid($data, $memberInfo)
    {
        $page             = !empty($data["page"]) ? $data["page"] : 1;
        $display          = !empty($data["pageSize"]) ? $data["pageSize"] : 20;
        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;
        $param["name"]    = !empty($data["keyWord"]) ? $data["keyWord"] : '';
        $getNoUserData    = RobotModel::getNoUserData($param, $memberInfo['mid']);
        $total            = RobotModel::getUserDataCount($param, $memberInfo['mid']);
        return self::responseJson(200, '获取成功', ['list' => $getNoUserData, 'total' => $total]);

    }

    /**
     * 删除用户
     * @param $data
     * @param $memberInfo
     * @return array|false|string
     */
    public static function delFrontUser($data, $memberInfo)
    {
        $userData = UserModel::getLoginUser(['id' => $data['id'], 'sup_id' => $memberInfo['mid']]);
        if (!empty($userData)) {
            //判断有没有
            $countNum = RobotModel::getBindWxDataByMid($userData->mid);
            if ($countNum > 0) {
                return ['code' => 400, 'msg' => '已绑定的机器人,请先编辑再更改！'];
            }
        }
        $res = RobotModel::updateSee('user', 'id', ['id' => $data['id'], 'delete_at' => time()]);
        if ($res) {
            $logMessage = '删除用户';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($data), $logMessage, $data['id']
            );
            return self::responseJson(200, '删除成功');
        } else {
            return self::responseJson(400, '删除失败');
        }
    }


    public static function getFrontUserInfo($data, $memberInfo)
    {
        //验证用户是否存在
        $userData   = UserModel::getLoginUser(['id' => $data['id'], 'sup_id' => $memberInfo['mid']]);
        $returnData = [];
        if (!empty(json_decode(json_encode($userData), true))) {
            $resData = UnitManageModel::getBindGroupData($userData->mid);

            if (!empty($resData)) {
                $returnData['group_name'] = $resData[0]['group_name'];
                $returnData['group_id']   = $resData[0]['id'];
            } else {
                $returnData['group_name'] = '';
                $returnData['group_id']   = '';
            }
            $returnData['id']     = $userData->id;
            $returnData['name']   = $userData->name;
            $returnData['mobile'] = $userData->mobile;
            $returnData['roleId'] = $userData->front_role_id;
            $bindWxids            = RobotModel::isBandWxDataMid('', $memberInfo['mid'], $userData->mid);
            $returnData['wxids']  = !empty($bindWxids) ? explode(',', trim($bindWxids, ',')) : [];
        }

        return self::responseJson(200, '获取成功', $returnData);
    }

    /**
     * 获取所有分组
     * @param $data
     * @param $memberInfo
     */
    public static function getGroupAllList($data, $memberInfo)
    {

        $resData = UnitManageModel::getGroupAllList($memberInfo);
        return self::responseJson(200, '获取成功', $resData);
    }

    /**
     * 更新数据
     */
    public static function upFrontUserData($data, $memberInfo)
    {
        if (empty($data['id'])) {
            return ['code' => 400, 'msg' => '参数错误！'];
        }
        if ($memberInfo['sup_id'] != 0 || $memberInfo['user_type'] < 2) {
            return ['code' => 400, 'msg' => '无权限操作！'];
        }

        $userData = UserModel::getLoginUser(['id' => $data['id'], 'sup_id' => $memberInfo['mid']]);

        $bindWxids = RobotModel::isBandWxDataMid('', $memberInfo['mid'], $userData->mid);
        //以前绑定的
        $bindWxidData = [];
        if ($bindWxids) {
            $bindWxidData = explode(',', $bindWxids);
        }
        //提交的
        $postWxid = [];
        if (!empty($data['wxids'])) {
            $postWxid = explode(',', $data['wxids']);
        }
        $oldNotMyWxid = $newMyWxid = [];
        //绑定机器人没有变化
        if ($bindWxidData != $postWxid) {
            $interSert    = array_intersect($bindWxidData, $postWxid);
            $oldNotMyWxid = array_diff($bindWxidData, $interSert);
            $newMyWxid    = array_diff($postWxid, $interSert);
            RobotModel::updateRobotbindMidUpdate('white_list_wx', ['mid' => $memberInfo['mid'], 'updateIds' => $oldNotMyWxid], ['bind_mid' => 0]);
            RobotModel::updateRobotbindMidUpdate('white_list_wx', ['mid' => $memberInfo['mid'], 'updateIds' => $newMyWxid], ['bind_mid' => $userData->mid]);
        }
        $addData = [
            'mobile'        => $data['mobile'],
            'name'          => $data['name'],
            'front_role_id' => $data['roleId'],
        ];
        if (isset($data['password']) && !empty($data['password']) && !empty($data['password'])) {
            if ($data['password'] == $data['confirmPassword']) {
                $addData['password'] = $data['password'];
            } else {
                return ['code' => 400, 'msg' => '密码请输入一致！'];
            }
        }
        if (!empty($data['group_id'])) {
            UnitManageModel::updateGroupStaff(['group_id' => $data['group_id'], 'mid' => $userData->mid, 'platform_identity' => $memberInfo['platform_identity']]);
        }
        $res = RobotModel::updateUserDataByid('user', ['id' => $data['id'], 'mid' => $userData->mid], $addData);
        return self::responseJson(200, '更新成功', $res);
    }


    /**
     * @param $code
     * @param $res
     * @param $data
     * @return false|string
     * @author   liuwenhao
     * @time     2020/4/14 12:56
     * @method   method   [返回json格式]
     */
    private static function responseJson($code, $res, $data = [])
    {
        $result = ['code' => $code, 'msg' => $res, 'data' => $data];
        return json_encode($result);
    }


}