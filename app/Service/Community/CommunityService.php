<?php

namespace App\Service\Community;

use App\Tools\Gimage\GimageTool;
use App\Model\Member\MemberModel;
use App\Model\Admin\GroupManageModel;
use function collect;
use function dd;
use Illuminate\Support\Facades\Redis;
use App\Service\VoucherBag\CouponService;
use App\Tools\Gimage\CommunityInvitePosterImage;
use App\Model\Community\CommunityInvitationGroupModel;
use  App\Service\Admin\AdminIndexService;
use function md5;


class CommunityService
{
    /**
     * 获取攻略标签
     * @return array
     */
    public static function getRaidersLabel($source)
    {
        return [
            [
                'labelId' => 1,
                'title' => '新人上手',
            ],[
                'labelId' => 2,
                'title' => '进阶学习',
            ],
            [
                'labelId' => 3,
                'title' => '邀请海报',
            ],
        ];
    }

    /**
     * 按钮类型对应
     * @param $type
     * @return string
     */
    public static function handleButtonType($type)
    {
        $button = '';
        switch ($type) {
            case 1:
                $button = '去升级';
                break;
            case 2:
                $button = '复制微信号';
                break;
            case 3:
                $button = '去邀请';
                break;
            case 4:
                $button = '自助开通';
                break;
        }
        return $button;
    }


    /**
     * 申请群助理
     * @param $params
     * @return array
     */
    public static function applyGroupAssistant($params)
    {
        //查询群信息
        $groupInfo = self::getGroupInfoByGroupId($params['groupId']);

        if (empty($groupInfo) || $groupInfo->status != 2) {
            return ['code' => 400, 'msg' => '群在审核中或审核不通过，不可申请群助理'];
        }
        if($groupInfo && $groupInfo->assistantWx) {
            return ['code' => 400, 'msg' => '已有群助理，不可重复申请'];
        }

        $returnData = AdminIndexService::getIsAdopt($params['mid']);
        if(!$returnData){
            return ['code' => 400, 'msg' => '您已申请10个群,近一个月gmv未达1w,不予通过'];
        }
        $params['source']= !isset( $params['source'])?1: $params['source'];
        $assistant  =   [];
        $BindGmvData = ['all_order_sum'=>0,'bind_group_num'=>0];
        if($params['source']=='7323ff1a23f0bf1cda41f690d4089353'||($params['source']==1 && strlen($params['source'])==1)){
            //微信号
            $assistant =  json_decode(json_encode(CommunityInvitationGroupModel::getExclusiveUserGroupAssistant(2,'7323ff1a23f0bf1cda41f690d4089353',$params['mid'])),true);
            if(empty($assistant)){
                //上级ID
                $parentId  =  CommunityInvitationGroupModel::getParentIdByMid($params['mid']);
                if(!empty($parentId->parent_id)){
                    $assistant =  json_decode(json_encode(CommunityInvitationGroupModel::getExclusiveUserGroupAssistant(2,'7323ff1a23f0bf1cda41f690d4089353',$parentId->parent_id)),true);
                }
            }
            //查询当权管理员的群数量 与 gmv总数
            if(!empty($assistant)){
                $BindGmvData = json_decode(json_encode(CommunityInvitationGroupModel::getBindMidGmv('7323ff1a23f0bf1cda41f690d4089353',$assistant['assistant_wx'])),true) ;
            }
        }
        $flag=0;
        if (!empty($BindGmvData)) {
            if ($BindGmvData['bind_group_num']>=config('community.exclusive_small_num')&&$BindGmvData['all_order_sum']<config('community.exclusive_order_sum')) {
                $flag=1;//数量超过最小范围
            }else if($BindGmvData['bind_group_num']>config('community.exclusive_big_num')){
                $flag=2;//
            }
        }
        if ($flag==2) {
            return ['code' => 400, 'msg' => '群助手数量不足，请联系导师'];
        }

        if($params['source']==1&&empty($assistant)){
            //查询群助手
            $assistant = json_decode(json_encode(CommunityInvitationGroupModel::getLeastUserGroupAssistant(1)),true);
        }else if(empty($assistant)){
            $assistant =  json_decode(json_encode(CommunityInvitationGroupModel::getLeastUserGroupDarenAssistant($params['source'])),true);
        }

        if (empty($assistant)) {
            return ['code' => 400, 'msg' => '群助手数量不足，请联系客服'];
        }
        //修改助理微信状态
        $ass = CommunityInvitationGroupModel::updateUserGroupAssistant($assistant['id']);

        $update = 0;
        if ($ass) {
            //修改群信息中的助理微信
            $update = self::updateDataById($params['groupId'], ['assistant_wx' => $assistant['assistant_wx'],'group_flag'=>$flag]);
        }

        if ($update) {
            if($flag==1){
                return ['code' =>200, 'msg' => '成功', 'data' => ['assistantWx' => $assistant['assistant_wx']]];
            }
            return ['code' => 200, 'msg' => '成功', 'data' => ['assistantWx' => $assistant['assistant_wx']]];
        }
        return ['code' => 400, 'msg' => '申请失败'];

    }


    /**
     * YCloud申请群助理
     * @param $params
     * @return array
     */
    public static function applyYCGroupAssistant($params)
    {
        if (!isset($params['userId']) || empty($params['userId'])) {
            return [
                'code' => 400,
                'msg' => '缺少申请人ID'
            ];
        }
        if (!isset($params['images']) || empty($params['images'])) {
            return [
                'code' => 400,
                'msg' => '缺少申请人图片'
            ];
        }
        if (is_array($params['images'])) {
            $params['images'] = implode("','", $params['images']);
        }
        $params['source'] = '2cb4a91bce63da9862f72559d4c463f5';

        $existWhere = [
            'mid' => $params['userId'],
            'type' => 3,
            'examine_status' => 1,
            'platform_identity' => '2cb4a91bce63da9862f72559d4c463f5'
        ];
        $exist = CommunityInvitationGroupModel::getExistRecord($existWhere);
        if ($exist) {
            return [];
        }
        //入库信息
        $createData = [
            'mid' => $params['userId'],
            'examine_status' => 1,
            'type' => 3,
            'platform_identity' => '2cb4a91bce63da9862f72559d4c463f5',
            'addtime' => time(),
        ];
        //插入数据
        $result = CommunityService::createData($createData);
        if ($result === false) {
            return [
                'code' => 400,
                'msg' => '申请提交失败'
            ];
        }
        return [];
    }


    /**
     * YCloud申请群助理
     * @param $params
     * @return array
     */
    public static function applyYCloudStatus($params)
    {
        if (!isset($params['userId']) || empty($params['userId'])) {
            return [
                'code' => 400,
                'msg' => '缺少申请人ID'
            ];
        }
        $params['source'] = '2cb4a91bce63da9862f72559d4c463f5';

        $existWhere = [
            'mid' => $params['userId'],
            'type' => 3,
            'examine_status' => 1,
            'platform_identity' => '2cb4a91bce63da9862f72559d4c463f5'
        ];
        $exist = CommunityInvitationGroupModel::getExistRecordInfo($existWhere);
        if ($exist) {
            return [
                'status' => 2,
                'msg' => '申请正在审核中, 请等待'
            ];
        }
        $existWhere = [
            'mid' => $params['userId'],
            'type' => 3,
            'examine_status' => 2,
            'platform_identity' => '2cb4a91bce63da9862f72559d4c463f5'
        ];
        $exist = CommunityInvitationGroupModel::getExistRecordInfo($existWhere);
        if ($exist) {
            $wxid = CommunityInvitationGroupModel::getYCloudRobotInfo(['exclusive_mid' => $params['userId']]) ?: '';
            return [
                'status' => 3,
                'msg' => $wxid
            ];
        }
        return [
            'status' => 1,
            'msg' => ''
        ];
    }

    /**
     * 申请群助理V2
     * @param $params
     * @return array
     */
    public static function applyGroupAssistantV2($params)
    {
        //查询群信息
        $groupInfo = CommunityInvitationGroupModel::getGroupInfoByMid($params);
        if (empty($groupInfo) || $groupInfo->status != 2) {
            return ['code' => 400, 'msg' => '群在审核中或审核不通过，不可申请群助理'];
        } else {
            $params['groupId'] = $groupInfo->groupId;
        }
        if($groupInfo && $groupInfo->assistantWx) {
            return ['code' => 200, 'msg' => '成功', 'data' => ['assistantWx' => $groupInfo->assistantWx]];
        }
        $returnData = AdminIndexService::getIsAdopt($params['mid']);
        if(!$returnData){
            return ['code' => 400, 'msg' => '您已申请10个群,近一个月gmv未达1w,不予通过'];
        }

        $assistant  =   [];
        $BindGmvData = ['all_order_sum'=>0,'bind_group_num'=>0];
        if($params['source']=='7323ff1a23f0bf1cda41f690d4089353'){
            //微信号
            $assistant = CommunityInvitationGroupModel::getExclusiveUserGroupAssistant(2,'7323ff1a23f0bf1cda41f690d4089353',$params['mid']);
            if(empty($assistant)){
                //上级ID
                $parentId  =  CommunityInvitationGroupModel::getParentIdByMid($params['mid']);
                if(!empty($parentId->parent_id)){
                    $assistant =  CommunityInvitationGroupModel::getExclusiveUserGroupAssistant(2,'7323ff1a23f0bf1cda41f690d4089353',$parentId->parent_id);
                }
            }
            //查询当权管理员的群数量 与 gmv总数
            if(!empty($assistant)){
                $BindGmvData = json_decode(json_encode(CommunityInvitationGroupModel::getBindMidGmv('7323ff1a23f0bf1cda41f690d4089353',$assistant->assistant_wx)),true) ;
            }
        }
        $flag=0;
        if (!empty($BindGmvData)) {
            if ($BindGmvData['bind_group_num']>=config('community.exclusive_small_num')&&$BindGmvData['all_order_sum']<config('community.exclusive_order_sum')) {
                $flag=1;//数量超过最小范围
            }else if($BindGmvData['bind_group_num']>config('community.exclusive_big_num')){
                $flag=2;//
            }
        }
        if ($flag==2) {
            return ['code' => 400, 'msg' => '群助手数量不足，请联系导师'];
        }

        if(empty($assistant)){
            $assistant = CommunityInvitationGroupModel::getLeastUserGroupDarenAssistant($params['source']);
        }
        if (!$assistant) {
            return ['code' => 400, 'msg' => '群助手数量不足，请联系客服'];
        }
        //修改助理微信状态
        $ass = CommunityInvitationGroupModel::updateUserGroupAssistant($assistant->id);

        $update = 0;

        if ($ass) {
            //修改群信息中的助理微信
            $update = self::updateDataById($params['groupId'], ['assistant_wx' => $assistant->assistant_wx]);
        }

        if ($update) {
            return ['code' => 200, 'msg' => '成功', 'data' => ['assistantWx' => $assistant->assistant_wx]];
        }

        return ['code' => 400, 'msg' => '修改失败'];

    }

    /**
     * 根据群id查询查信息
     * @param $id
     * @return mixed
     */
    public static function getGroupInfoByGroupId($id)
    {
        return CommunityInvitationGroupModel::getGroupInfoByGroupId($id);
    }

    /**
     * 根据群编号查询查信息
     * @param $number
     * @return mixed
     */
    public static function getGroupInfoByGroupNumber($number)
    {
        return CommunityInvitationGroupModel::getGroupInfoByGroupNumber($number);
    }

    /**
     * 根据用户id查询未审核的群 如果有不重新生成
     * @param $mid
     * @return mixed
     */
    public static function getGroupInfoByMidAndStatus($mid, $status, $type)
    {
        return CommunityInvitationGroupModel::getGroupInfoByMidAndStatus($mid, $status, $type);
    }

    /**
     * 根据用户id查询未审核的群 如果有不重新生成
     * @param $mid
     * @return mixed
     */
    public static function getExamineStepByGroupId($params)
    {
        $mid = $params['mid'];
        $source = isset($params['source']) ? $params['source'] : '';
        $where = [
            'mid' => $mid
        ];
        if (!empty($source)) {
            $where['platform_identity'] = $source;
        }
        return CommunityInvitationGroupModel::getExamineStepByGroupId($where);
    }


    /**
     * 插入数据
     * @param $data
     * @return mixed
     */
    public static function createData($data)
    {
        return CommunityInvitationGroupModel::createData($data);
    }

    /**
     * 更新数据
     * @param $data
     * @return mixed
     */
    public static function updateDataById($id, $data)
    {
        return CommunityInvitationGroupModel::updateDataById($id, $data);
    }

    /**
     * @param $id
     * @param $mid
     * @param $data
     * @return mixed
     */
    public static function updateDataByIdMid($id,$mid, $data)
    {
        return CommunityInvitationGroupModel::updateDataByIdMid($id,$mid, $data);

    }

    /**
     * @param $id
     * @param $mid
     * @param $data
     * @return mixed
     */
    public static function updateDataByIdMidParam($params, $data)
    {

        $mid = $params['mid'];
        $source = isset($params['source']) && !empty($params['source']) ? $params['source'] : '7323ff1a23f0bf1cda41f690d4089353';
        return CommunityInvitationGroupModel::updateDataByIdMidParam($mid,$source, $data);

    }

    /**
     * 升级
     * @param $param
     * @return mixed
     */
    public static function applyReviewV2($param)
    {

        if(!isset($param['groupId'])||empty($param['groupId']))
        {
            return ['code'=>400,"msg"=>"groupId不能为空"];
        }

        if(!isset($param['mid'])||empty($param['mid']))
        {
            return ['code'=>400,"msg"=>"mid不能为空"];
        }

        $groupInfo = CommunityInvitationGroupModel::getInvitationGroupInfo($param['mid'],$param['groupId']);

        if(empty($groupInfo))
        {
            return ['code'=>400,"msg"=>"该群没有升级资格"];
        }

        if($groupInfo->upgrade_status==1)
        {

            return ['code'=>400,"msg"=>"审核中,请耐心等待"];
        }

        if($groupInfo->upgrade_status==2)
        {

            return ['code'=>400,"msg"=>"该群已升级"];
        }

        $data = [
            "upgrade_status" => 1,
            "upgrade_addtime" => time()
        ];

        $result = CommunityInvitationGroupModel::updateDataById($param['groupId'],$data);

        if($result)
        {
            return ['code'=>200,"msg"=>"提交成功"];
        }

        return ['code'=>400,"msg"=>"提交失败"];

    }

    /**
     * @param $source
     * @return mixed
     */
    public static function changeSourceIsSet($source){
        return CommunityInvitationGroupModel::changeSourceIsSet($source);
    }



    /**
     * @param $source
     * @return integer
     */
    public static function getGroupNumCount($groupNum){
        $count = CommunityInvitationGroupModel::where(['group_number' => $groupNum])->count();
        if ($count && $count > 0) {
            $groupNum = self::getRandNumber();
        }
        return $groupNum;
    }


    /**
     * 获取用户的直推群列表
     * @param $params
     * @return mixed
     */
    public static function getGroupListByMid($params)
    {
        $status = 2;

        //直推群
        if ($params['type'] == 1)
        {
            $list = CommunityInvitationGroupModel::getGroupListByMid($params);

            foreach ($list->items() as $val) {

                if ($val->status == 1) {
                    $status = 1;
                }
            }

        //间推群
        } else{
            //获取下级用户
            $memberIds = MemberModel::getSubordinateIdsByMid($params['mid'], $params['source']);

            $params['midArr'] = $memberIds;

            $list = CommunityInvitationGroupModel::getGroupListByMidArr($params);

        }

        foreach ($list->items() as $val) {
            if ($params['type']) {
                //是否有助理标识
                $val->isHasAssistant = $val->assistantWx ? 1 : 0;
            }else{
                $val->isHasAssistant = 0;
            }

            $val->isShowUpgrade = 0;

            if($val->roomId>0&&$val->status==2&&$val->statusV2!=2)
            {   
                $groupMemberCount = GroupManageModel::getUserNumber(0,$val->roomId);

                if($groupMemberCount>399)
                {
                    $val->isShowUpgrade = 1;
                }
            }
        }

        $data['status'] = $status;
        $data['total'] = $list->total();
        $data['list'] = $list->items();

        return $data;
    }
    /**
     * 获取用户的直推群列表
     * @param $params
     * @return mixed
     */
     public static function  getGroupListByMidBySource($params)
     {
         $isSet =  CommunityInvitationGroupModel::changeSourceIsSet($params['source']);
         $data=[];
         if(!empty($isSet)){
             $params['source'] = config('community.groupType')[$params['source']];//对应的type类型
             $list = CommunityInvitationGroupModel::getGroupListByMid($params);
             foreach ($list->items() as $val) {
                 //是否有助理标识
                 $val->isHasAssistant = $val->assistantWx ? 1 : 0;
             }
             $data['total'] = $list->total();
             $data['list'] = $list->items();
         }
         return $data;
     }




    /**
     * 转链
     * @param $params
     * @return bool
     */
    public static function conversionUrl($params) {

        $mid = $params['mid'];
        $str = $params['urlStr'];

        //匹配出URL地址
        preg_match_all("/(https|http):[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/",$str,$arr);

        if (!isset($arr[0][0]) || empty($arr[0][0])) {
            return false;
        }

        $url = $arr[0][0];

        $urlArr = explode('/', $url);

        //获取到短链code
        $shortUrlCode = end($urlArr);

        //查询短链code对应的长链接
        $longUrl = CommunityInvitationGroupModel::getLongUrlByShortUrlCode($shortUrlCode);

        if (!$longUrl) {
            return false;
        }

        //获取用户邀请码
        $codeNumber = MemberModel::getInvitationCodeByMember($mid);

        //处理链接 更改长链codeNumber
        $urlStr = self::handleUrl($longUrl->long_url, $codeNumber);

        //长链转短链
        $shortUrl = CouponService::getShortURrl(['url' => $urlStr]);

        //替换字符串中短链
        $data = str_replace($url, $shortUrl, $str);

        //替换邀请码
        $data = str_replace(strstr($str,"【邀请码】"), "【邀请码】".$codeNumber, $data);

        return $data;

    }

    /**
     * 生成不重复的随机数字
     * @param  int $start  需要生成的数字开始范围
     * @param  int $end    结束范围
     * @param  int $length 需要生成的随机数个数
     * @return number      生成的随机数
     */
    public static function getRandNumber($start=0,$end=9,$length=8)
    {
        //初始化变量为0
        $connt = 0;
        //建一个新数组
        $temp = array();
        while($connt < $length){
            //在一定范围内随机生成一个数放入数组中
            $temp[] = mt_rand($start, $end);
            //$data = array_unique($temp);
            //去除数组中的重复值用了“翻翻法”，就是用array_flip()把数组的key和value交换两次。这种做法比用 array_unique() 快得多。
            $data = array_flip(array_flip($temp));
            //将数组的数量存入变量count中
            $connt = count($data);
        }
        //为数组赋予新的键名
        shuffle($data);
        //数组转字符串
        $str=implode(",", $data);
        //替换掉逗号
        $number=str_replace(',', '', $str);
        $number = self::getGroupNumCount($number);
        return $number;
    }

    /**
     * 替换长链的邀请码
     * @param $longUrl
     * @param $codeNumber
     * @return mixed
     */
    public static function handleUrl($longUrl, $codeNumber) {
        $longUrlInfo = parse_url($longUrl);

        //将参数处理成数组
        $queryParts = explode('&', $longUrlInfo['query']);

        $params = array();

        //组装参数数据
        foreach ($queryParts as $param) {

            $item = explode('=', $param);

            $params[$item[0]] = $item[1];

        }

        $params['codeNumber'] = $codeNumber;

        //参数用&连接
        $urlStr = '';
        foreach($params as $key => $value) {
            $urlStr .= $key.'='.$value.'&';
        }

        //去掉最后一个&字符串
        $urlStr = trim($urlStr, '&');

        //将url参数替换掉
        $lastUrl = str_replace($longUrlInfo['query'], $urlStr, $longUrl);

        return $lastUrl;
    }

    /**
     * 通讯录列表
     * @param $params
     * @return mixed
     */
    public static function getAddressBook($params)
    {
        $mid = $params['mid'];

        //用户信息
        $data['myself'] = MemberModel::getMemberByMemberId($mid);

        //获取邀请人信息
        $data['parentUser'] = MemberModel::getMemberByMemberId($data['myself']->parent_id);

        //查找用户群数据
        $tutor = CommunityInvitationGroupModel::getToturWxByMid($mid);

        //组装数据
        foreach ($tutor as $v) {
            if ($v) {

                $info = collect(config('community.tutorInfo'))->where('tutorWx', $v)->first();

                $tmp = [
                    'mid' => 0,
                    'nickname' => $info['nickname'],
                    'truename' => '',
                    'mobile' => $info['mobile'],
                    'litpic' => $info['avatar'],
                    'card_type' => 0,
                    'jointime' => 0,
                    'wechat' => $info['tutorWx'],
                    'parent_id' => 0,
                ];

                $data['teacher'][] = $tmp;
            }
        }

        //获取直属下级用户信息
        $lists = MemberModel::getMemberChildListByMemberIdNew($mid, $params['page'], $params['pageSize']);
        $data['fans']['list'] = $lists->items();
        $data['fans']['total'] = $lists->total();

        return $data;
    }

    /**
     * 粉丝列表分页
     * @param $params
     * @return mixed
     */
    public static function fanList($params)
    {
        $lists = MemberModel::getMemberChildListByMemberIdNew($params['mid'], $params['page'], $params['pageSize']);

        $data['list'] = $lists->items();
        $data['total'] = $lists->total();

        return $data;
    }

    /**
     * 申请群助理的步骤
     * @param $params
     * @return mixed
     */
    public static function applyCopyWriting($params)
    {
        $source = $params['source'] ?? '';
        if (isset($params['groupId']) && !empty($params['groupId'])) {
            $groupInfo = CommunityInvitationGroupModel::getGroupInfoByGroupId($params['groupId']);
            $groupName = $groupInfo->groupName;
        } else {
            $groupInfo = CommunityInvitationGroupModel::getGroupInfoByMid($params);

            if (empty($groupInfo)) {
                $groupName = self::CreateCommunityInvitationInfo($params);
            } else {
                $groupName = $groupInfo->groupName;
            }
        }
        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            $data = config('community.daRenApplyCopyWriting');
        } else if ($source == '75f712af51d952af3ab4c591213dea13') {
            //直订平台标识
            $data = config('community.zhiDingApplyCopyWriting');
        } else {
            $data = config('community.applyCopyWritingV2');
        }

        $data['groupName'] = $groupName;

        return $data;
    }

    /**
     * YCloud申请群助理的步骤
     * @param $params
     * @return mixed
     */
    public static function applyYCloudCopyWriting($params)
    {
        //YCloud 平台申请助理不需要群名称
        $data = config('community.YCloudApplyCopyWriting');
        $data['groupName'] = '微小店折扣'.CommunityService::getRandNumber();;
        return $data;
    }

    /**
     * YCloud申请群助理的步骤
     * @param $params
     * @return mixed
     */
    public static function applyYCloudRules($params)
    {

        $source = '2cb4a91bce63da9862f72559d4c463f5';
        $countNum = config('community.countNum');
        //文案
        $upgradeContent = config('community.upgradeContent');

        //大人截图示例
        $exampleImages = config('community.exampleImages');


        //获取分配的群编号
        $groupNo = 834324;


        $data = [
            //群编号
            'groupNo' => $groupNo,
            //群名称
            'groupName' => config('community.groupNameData')[$source] . $groupNo,
            //导师微信
            'tutorWX' => config('community.tutorWx')[$source][0]['tutorWx'],
            //群人数要求
            'conutNum' => $countNum,
            //截图示例
            'exampleImages' => $exampleImages,
            //文案
            'upgradeContent' => $upgradeContent,
        ];
        return $data;
    }

    public static function CreateCommunityInvitationInfo($params)
    {
        $mid = $params['mid'];
        $source = isset($params['source']) && !empty($params['source']) ? $params['source'] : '7323ff1a23f0bf1cda41f690d4089353';

        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            $countNum = config('community.daRenCountNum');
            //文案
            $upgradeContent = config('community.daRenUpgradeContent');
        } elseif ($source == '75f712af51d952af3ab4c591213dea13') {
            //直订文案,邀请群限制数量
            $countNum = config('community.zhiDingCountNum');
            //文案
            $upgradeContent = config('community.zhiDingUpgradeContent');
        } else {
            $countNum = config('community.countNum');
            //文案
            $upgradeContent = config('community.upgradeContent');
        }
        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            //大人截图示例
            $exampleImages = config('community.daRenExampleImages');
        } else {
            //截图示例
            $exampleImages = config('community.exampleImages');
        }
        if($source!=1&&!empty($source)){
            $type = config('community.groupType')[$source];//对应的type类型
        }

        //获取分配的群编号
        $groupNo = CommunityService::getRandNumber();

        if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
            $groupName = config('community.daRenGroupName');
        } else {
            $groupName = config('community.groupName');
        }

        $groupName = $source==1? $groupName.$groupNo:config('community.groupNameData')[$source].$groupNo;
        $data = [
            //群编号
            'groupNo' => $groupNo,
            //群名称
            'groupName' => $groupName,
            //导师微信
            'tutorWX' =>   $source==1? config('community.tutorInfo')[0]['tutorWx']:config('community.tutorWx')[$source][0]['tutorWx'],
            //群人数要求
            'conutNum' => $countNum,
            //截图示例
            'exampleImages' => $exampleImages,
            //文案
            'upgradeContent' => $upgradeContent,
        ];
        //入库信息
        $createData = [
            'mid' => $mid,
            'group_number' => $data['groupNo'],
            'group_name' => $data['groupName'],
            'tutor_wx' => $data['tutorWX'],
            'type' => $type,
            'platform_identity' => $source==1?'7323ff1a23f0bf1cda41f690d4089353':$source,
            'addtime' => time(),
        ];
        //插入数据
        CommunityService::createData($createData);
        return $groupName;
    }

    /**
     * 分享小程序
     * @param $mid
     * @return array
     */
    public static function shareXcx($mid,$source)
    {
        $channelData =   CommunityInvitationGroupModel::getChannelDataBySource($source);
        if($source=='7323ff1a23f0bf1cda41f690d4089353'){
            //获取用户邀请码
            $codeNumber = MemberModel::getInvitationCodeByMember($mid);
            $userInfo = MemberModel::getMemberInfo(['mid'=>$mid]);
            $reCode     = trim($codeNumber);
            return [
                'showImg'   => $channelData->xcx_show_img,
                'title'    => $userInfo->nickname.'推荐您享受悦淘专属优惠',
                'desc'    => "分享还能赚佣金",
                'posterUrl' =>  $channelData->xcx_poster_url."?areCode=".$codeNumber,
                'reCode'    => $reCode,
            ];
        }else{//其他平台需要单独配置
            //@todo
            return [
                'showImg'   => $channelData->xcx_show_img,
                'title'    => '',
                'desc'    => "分享还能赚佣金",
                'posterUrl' => $channelData->xcx_poster_url,
                'reCode'    => '',
            ];
        }

    }

    /**
     * 分享海报
     * @param $mid
     * @param $posterImg
     * @return array, $this->params['posterImg'],$source
     */
    public static function raidersYCloudSharePoster($param, $posterImg)
    {
        if (!isset($param['userId']) || empty($param['userId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        $source = '2cb4a91bce63da9862f72559d4c463f5';
//        $source = '7323ff1a23f0bf1cda41f690d4089353';//TODO COOL 这里先写悦淘的
        $userId = $param['userId'];
        //获取用户邀请码
        $channelData = CommunityInvitationGroupModel::getChannelDataBySource($source);
        $params['codeNumber'] = $userId;
        //用户信息
        $params['litpic'] = 'http://images.yueshang.store/pubfile/2018/07/17/line_1531821979.png';         //用户头像
        //添加用户昵称
        $params['nickname'] = '
邀请您一起加入悦淘
分享好物，购好品，省钱又赚钱';     //昵称
        $params['posterImg'] = $posterImg;     //海报图
        $params['appId'] = $channelData->app_id;     //海报图
        $params['appSecret'] = $channelData->app_secret;     //海报图
        $params['page'] = $channelData->page;     //海报图
        $GimageTool = new GimageTool();
        $GimageTool->setGimage(new CommunityInvitePosterImage($params));
        $imgPath = $GimageTool->generatorImage();

        return $imgPath;
    }

    /**
     * 攻略分类文章
     * @return mixed
     */
    public static function raidersNewList($mid, $Source,$type=1)
    {
        $channelData =   CommunityInvitationGroupModel::getChannelDataBySource($Source);
        $groupType= $channelData->group_type;

        //查询用户是否有审核中或者审核不通过的群
        $groupInfo = CommunityInvitationGroupModel::getLastDataByMid($mid,$groupType,$Source);

        $status = 2;
        //1待审核 2审核通过 3审核不通过
        if ($groupInfo) $status = $groupInfo->examine_status;

        if ($Source == '89dafaaf53c4eb68337ad79103b36aff') {
            if ($type == 1) {
                //大人小店的新人上手
                $type = 16;
            }
        }
        //获取内容
        $data = CommunityInvitationGroupModel::getRaidersNewList($Source,$type);

        //组装数据
        foreach ($data as $value) {
            //如果是开通助理类型
            if ($value->buttonType == 4) {
                $value->status = 1;
                //查询是否有申请通过的群
                $group = CommunityInvitationGroupModel::getPassGroupByMid($mid, $Source);
                if ($group) {
                    $value->status = 2;
                }
            }
            $value->buttonText = self::handleButtonType($value->buttonType);
            $value->status = $status;
        }

        return $data;
    }



    /**
     * 攻略分类文章
     * @return mixed
     */
    public static function raidersNewListV2($mid, $source = 0)
    {
        if ($source == 1) {
            $source = '7323ff1a23f0bf1cda41f690d4089353';
        }
        if (empty($source)) {
            return ['code' => 400, 'msg' => '缺少平台标识'];
        }
        //查询用户是否有审核中或者审核不通过的群
        $groupInfo = CommunityInvitationGroupModel::getLastDataByMidV2($mid, $source);

        $status = 0;

        //1待审核 2审核通过 3审核不通过
        if ($groupInfo) $status = $groupInfo->examine_status;

        $host = 'https://image.yuetao.vip/';
        //获取内容
        $result = CommunityInvitationGroupModel::getRaidersNewListV2($source);
        $data['banner'] = [[
            'image' => $host . 'ditan/banner.jpg',
            'url'   => $host . 'ditan/gonglue.mp4'
        ]];
        $data['list'] = $result;
        //组装数据
        foreach ($data['list'] as $key => $value) {
            //如果是开通助理类型
            if ($value->buttonType == 4) {
                $value->status = 1;
                //查询是否有申请通过的群
                $group = CommunityInvitationGroupModel::getPassGroupByMidV2($mid, $source);
                if ($group) {
                    $value->status = 2;
                }
            }
            $value->buttonText = self::handleButtonTypeV2($value->buttonType);
            $value->status = $status;
            $value->button = [];
            $value->btn = [];
            //type = 1 是视频 type= 2 弹窗 type = 3 开通群助理 type = 4 分享
            //邀请是4  联系客服3
            switch ($key) {
                case 0:
                    $value->button[0]['text'] = '自助开通';
                    $value->button[0]['type'] = 3;
                    $value->button[0]['url'] = '';
                    $value->btn[] = [
                        'text' => '查看视频教学',
                        'type' => 1,
                        'url'  => $host . 'ditan/gonglue.mp4'
                    ];
                    break;
                case 1:
                    $value->button[0]['text'] = '分享教程';
                    $value->button[0]['type'] = 1;
                    $value->button[0]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/18/ylhvideoVi0mtmY6pU1605261684.mp4';
                    $value->button[1]['text'] = '转链教程';
                    $value->button[1]['type'] = 1;
                    $value->button[1]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/18/ylhvideossfnohZfdS1605261752.mp4';
                    $value->button[2]['text'] = '省钱秘籍';
                    $value->button[2]['type'] = 1;
                    $value->button[2]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/18/ylhvideoamhDth9H6y1605261810.mp4';
                    break;
                case 2:
                    $value->button[0]['text'] = '获取进群二维码';
                    $value->button[0]['type'] = 2;
                    $value->button[0]['url'] = $host . 'ditan/code.png';//图片
                    $value->button[1]['text'] = '关注公众号';
                    $value->button[1]['type'] = 2;
                    $value->button[1]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/17/yuelvhuiAO7RJgKlCA1605259607.jpg';
                    $value->button[2]['text'] = '联系客服';//这个待确认
                    $value->button[2]['type'] = 2;
                    $value->button[2]['url'] = $host . 'ditan/kefu.jpg';
                    break;
                case 3:
                    $value->button[0]['text'] = '立即邀请';
                    $value->button[0]['type'] = 4;
                    $value->button[0]['url'] = '';
                    $value->button[1]['text'] = '联系客服';
                    $value->button[1]['type'] = 2;
                    $value->button[1]['url'] = $host . 'ditan/kefu.jpg';
                    $value->btn[] = [
                        'text' => '如何高质量拉群',
                        'type' => 1,
                        'url'  => $host . 'ditan/gonglue.mp4'
                    ];
                    break;
            }
        }
        return $data;
    }



    /**
     * 攻略分类文章
     * @return mixed
     */
    public static function raidersYCloudNewList()
    {
        $source = '2cb4a91bce63da9862f72559d4c463f5';

        $status = 0;
        $host = 'https://image.yuetao.vip/';
        //获取内容
        $result = CommunityInvitationGroupModel::getRaidersNewListV2($source);
        $data['banner'] = [[
            'image' => $host . 'ditan/banner.jpg',
            'url'   => $host . 'ditan/gonglue.mp4'
        ]];
        $data['list'] = $result;
        //组装数据
        foreach ($data['list'] as $key => $value) {
            $value->buttonText = self::handleButtonTypeV2($value->buttonType);
            $value->status = $status;
            $value->button = [];
            $value->btn = [];
            //type = 1 是视频 type= 2 弹窗 type = 3 开通群助理 type = 4 分享
            //邀请是4  联系客服3
            switch ($key) {
                case 0:
                    $value->button[0]['text'] = '自助开通';
                    $value->button[0]['type'] = 3;
                    $value->button[0]['url'] = '';
                    $value->btn[] = [
                        'text' => '查看视频教学',
                        'type' => 1,
                        'url'  => $host . 'ditan/gonglue.mp4'
                    ];
                    break;
                case 1:
                    $value->button[0]['text'] = '分享教程';
                    $value->button[0]['type'] = 1;
                    $value->button[0]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/18/ylhvideoVi0mtmY6pU1605261684.mp4';
                    $value->button[1]['text'] = '转链教程';
                    $value->button[1]['type'] = 1;
                    $value->button[1]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/18/ylhvideossfnohZfdS1605261752.mp4';
                    $value->button[2]['text'] = '省钱秘籍';
                    $value->button[2]['type'] = 1;
                    $value->button[2]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/18/ylhvideoamhDth9H6y1605261810.mp4';
                    break;
                case 2:
                    $value->button[0]['text'] = '获取进群二维码';
                    $value->button[0]['type'] = 2;
                    $value->button[0]['url'] = $host . 'ditan/code.png';//图片
                    $value->button[1]['text'] = '关注公众号';
                    $value->button[1]['type'] = 2;
                    $value->button[1]['url'] = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-11-13/17/yuelvhuiAO7RJgKlCA1605259607.jpg';
                    $value->button[2]['text'] = '联系客服';//这个待确认
                    $value->button[2]['type'] = 2;
                    $value->button[2]['url'] = $host . 'ditan/kefu.jpg';
                    break;
                case 3:
                    $value->button[0]['text'] = '立即邀请';
                    $value->button[0]['type'] = 4;
                    $value->button[0]['url'] = '';
                    $value->button[1]['text'] = '联系客服';
                    $value->button[1]['type'] = 2;
                    $value->button[1]['url'] = $host . 'ditan/kefu.jpg';
                    $value->btn[] = [
                        'text' => '如何高质量拉群',
                        'type' => 1,
                        'url'  => $host . 'ditan/gonglue.mp4'
                    ];
                    break;
            }
        }
        return $data;
    }


    /**
     * 攻略分类文章
     * @return mixed
     */
    public static function raidersYCloudInvitePost()
    {
        //获取内容
        $data = CommunityInvitationGroupModel::invitePosterYCloudList();

        foreach ($data as $val) {
            $val->avatar = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-09/18/yuelvhuitnCl1IPEZJ1586427803.png";
            $val->nickname = "悦淘社交电商";
            $val->extraContent = "#点击图片获取您的专属海报#";
        }

        return $data;
    }

    /**
     * 攻略分类文章
     * @return mixed
     */
    public static function raidersYCloudStudyArticle($param)
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:20;

        $typeId = $param['typeId'] ?? 10199;

        $data = CommunityInvitationGroupModel::raidersYCloudStudyArticle([$typeId],$page,$pageSize);

        $data['list'] = array_map(function($instance){
            return [
                "aid"=>$instance->aid,
                "coverImg"=>$instance->cover_pic,
                "title"=>$instance->title,
                "videoPath"  => $instance->video_path,
                "jump"     => $instance->jump,
                "content"=>$instance->content,
                "createTime"=>$instance->created_at,
                "webUrl"  => "https://university.yuetao.vip/marketing/yuelvdaxue/index.html?id=" . $instance->aid
            ];
        },$data['list']);

        return $data;
    }




    /**
     * 按钮类型对应
     * @param $type
     * @return string
     */
    public static function handleButtonTypeV2($type)
    {
        $button = '';
        switch ($type) {
            case 1:
                $button = '去升级';
                break;
            case 2:
                $button = '复制微信号';
                break;
            case 3:
                $button = '去邀请';
                break;
            case 4:
                $button = '自助开通';
                break;
        }

        return $button;
    }

    /**
     * 邀请海报
     * @return \Illuminate\Support\Collection
     */
    public static function invitePosterList($source,$type)
    {
        //获取内容
        $data = CommunityInvitationGroupModel::invitePosterList($source,$type);
//         var_dump($data);
        foreach ($data as $val) {
            if ($source == '89dafaaf53c4eb68337ad79103b36aff') {
                $val->avatar = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-09/16/yuelvhuiiZ0Qhn4NKS1591690005.jpg";
                $val->nickname = "大人";
            } else {
                $val->avatar = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-09/18/yuelvhuitnCl1IPEZJ1586427803.png";
                $val->nickname = "悦淘社交电商";
            }
            $val->extraContent = "#点击图片获取您的专属海报#";
        }

        return $data;
    }
}


