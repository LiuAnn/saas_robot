<?php
/**
 * createdBy : PhpStorm
 * author    : ljh65582439@gmail.com
 * createdAt : 2020/4/7 4:55 下午
 */

namespace App\Service\Community;

use App\Model\Activity\ActivityInfoModel;
use App\Model\Community\GoodsBrowseModel;
use App\Model\Community\ManagementModel;
use App\Model\Community\GroupInfoModel;
use App\Model\Activity\ActivityGoodsModel;
use App\Model\Member\MemberExperienceCardModel;
use App\Service\Member\DistributionServiceImpl;

class ManagementService
{
    /**
     * 群列表
     * @param $data
     * @return array
     * @author  liujiaheng
     * @created 2020/4/8 4:13 下午
     */
    public static function queryGroupList($data)
    {
        // 已激活
        $data['type'] = 1;
        $activation   = GroupInfoModel::queryGroupList($data);

        // 未激活
        $data['type'] = 0;
        $noActivation = GroupInfoModel::queryGroupList($data);
        $noActivation = $noActivation->toArray();
        // 处理还差几个激活
        $noActivation = array_reduce($noActivation, function ($res, $item) {
            $item['clickGap'] = 10 - $item['cliecked_num'];
            $res[]            = $item;

            return $res;
        });

        return [
            'activation'   => $activation,
            'noActivation' => $noActivation,
        ];
    }

    /**
     * 群详细信息
     * @param $data
     * @return array
     * @author  liujiaheng
     * @desc    []
     * @created 2020/4/8 11:29 上午
     */
    public static function queryGroupInfo($data)
    {
        $data['date'] = date('Y-m-d');

        // 群人数
        return [
            'info'       => ManagementModel::queryGroupInfo($data),
            'all_people' => ManagementModel::queryPeopleNumber($data),
            'uv'         => GoodsBrowseModel::queryActivity($data),
            'pv'         => GoodsBrowseModel::queryActivityPv($data),
            'new_people' => ManagementModel::queryPeopleNumber($data, 1),
        ];
    }

    /**
     * 群分享详情
     * @param $data
     * @return array|mixed
     * @author  liujiaheng
     * @created 2020/4/8 3:28 下午
     */
    public static function queryShareInfo($data)
    {
        // 查询商品ID
        $queryInfo = GoodsBrowseModel::queryShareInfo($data);
        $queryInfo = $queryInfo->toArray();
        if (empty($queryInfo)) {
            return [];
        }
        $goodsId = array_column($queryInfo, 'goods_id');

        // 获取商品详情
        $goodsInfo = ActivityGoodsModel::queryGoodsInfo($goodsId);
        $goodsInfo = $goodsInfo->toArray();

        // 获取活动详情
        $activityId   = array_column($goodsInfo, 'act_id');
        $activityInfo = ActivityInfoModel::queryActivityInfo($activityId);
        $activityInfo = $activityInfo->toArray();

        // 查询全部浏览记录
        $clickInfo = GoodsBrowseModel::queryClickInfo(['groupId' => $data['groupId']]);
        $clickInfo = $clickInfo->toArray();

        // 处理详情
        return array_reduce($goodsId, function ($res, $item) use ($clickInfo, $goodsInfo, $activityInfo) {

            // 商品详情
            if (key_exists($item, $goodsInfo)) {
                $title   = $goodsInfo[$item]['goods_name'];
                $img     = $goodsInfo[$item]['goods_img'];
                $price   = $goodsInfo[$item]['price'] / 100;
                $actId   = $goodsInfo[$item]['act_id'];
                $endTime = 0;
                if (isset($activityInfo[$actId])) {
                    $timestamp = $activityInfo[$actId]['act_endtime'];
                    $endTime   = ($timestamp > time()) ? date('Y-m-d H:i:s', $timestamp) : '活动已结束';
                }

                // 计算分拥金额
                $reward = DistributionServiceImpl::sendBonusCommon(1, $price);

                foreach ($clickInfo as $value) {
                    // 处理浏览用户
                    if ($item === $value['goods_id']) {
                        $tmp['date']     = $value['created_at'];
                        $tmp['goods']    = [
                            'id'          => $item,
                            'title'       => $title,
                            'img'         => $img,
                            'group_price' => $price,
                            'end_time'    => $endTime,
                            'reward'      => isset($reward['first']) ? $reward['first'] : 0,
                        ];
                        $tmp['browse'][] = [
                            'id'    => $value['mid'],
                            'name'  => $value['member']['nickname'],
                            'img'   => $value['member']['head_img'],
                            'count' => $value['count_num']
                        ];

                        $res[] = $tmp;
                    }
                }
            }

            return $res;
        });
    }

    /**
     * 群会员列表
     * @param $data
     * @return array|mixed
     * @author  liujiaheng
     * @created 2020/4/7 6:35 下午
     */
    public static function queryMemberList($data)
    {
        $data['page'] = isset($data['page']) ? $data['page'] : 1;
        $queryInfo    = ManagementModel::queryMemberList($data);

        if (!$queryInfo->total()) {
            return [];
        }
        $queryInfo = $queryInfo->toArray();
        $dataInfo  = $queryInfo['data'];

        // 处理用户点击数量
        $handleInfo = array_reduce($dataInfo, function ($res, $item) {

            if ($item['activity']) {
                // 点击数量
                $clickNum           = array_column($item['activity'], 'count_num');
                $item['clickCount'] = array_sum($clickNum);

                // 活跃时间
                $time         = array_pop($item['activity']);
                $item['time'] = self::changeTime(strtotime($time['created_at']));
                array_push($item['activity'], $time);

            } else {
                $item['clickCount'] = 0;
                $item['time']       = '最近未活跃';
            }

            // 会员身份
            $exVip = MemberExperienceCardModel::checkExperience($item['mid'], time());
            if ($exVip) {
                $isVip = '体验会员';
            } else {
                $isVip = '普通会员';
            }
            if (isset($item['vip']['card_type'])) {
                $isVip = '精英会员';
            }
            $item['identity'] = $isVip;


            $res[] = $item;

            return $res;
        });

        if (!isset($data['sort'])) {
            return $handleInfo;
        }

        $sortArr = array_column($handleInfo, 'clickCount');

        // 排序
        if ($data['sort']) {
            array_multisort($sortArr, SORT_ASC, $handleInfo);
        } else {
            array_multisort($sortArr, SORT_DESC, $handleInfo);
        }

        return $handleInfo;
    }

    /**
     * 转化时间显示
     * @param $time
     * @return false|string
     * @author  liujiaheng
     * @created 2020/4/7 6:34 下午
     */
    public static function changeTime($time)
    {
        $time = (int)substr($time, 0, 10);
        $int  = time() - $time;
        if ($int <= 2) {
            $str = sprintf('刚刚', $int);
        } elseif ($int < 60) {
            $str = sprintf('%d秒前', $int);
        } elseif ($int < 3600) {
            $str = sprintf('%d分钟前', floor($int / 60));
        } elseif ($int < 86400) {
            $str = sprintf('%d小时前', floor($int / 3600));
        } elseif ($int < 2592000) {
            $str = sprintf('%d天前', floor($int / 86400));
        } else {
            $str = date('Y-m-d H:i:s', $time);
        }
        return $str;
    }

}
