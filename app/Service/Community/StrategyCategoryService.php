<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/8
 * Time: 17:45
 */

namespace App\Service\Community;

use App\Model\Community\StrategyArticleModel;
use App\Model\Community\StrategyCategoryModel;
use App\Service\Robot\UserOperationLogService;

class StrategyCategoryService
{
    public static function addStrategyCategory($memberInfo, $param = [])
    {
        if (empty($param['name']))
        {
            return ['code' => 400, 'msg' => '名称不能为空'];
        }

        if (empty($param['level']))
        {
            return ['code' => 400, 'msg' => '等级不能为空'];
        }

        $param['parent_id'] = !empty($param['parent_id']) ? $param['parent_id'] : 0;

        $StrategyCategory = StrategyCategoryModel::create($param);

        if ($StrategyCategory)
        {
            $logMessage = '新增社群攻略分类';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $StrategyCategory
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function updateStrategyCategory($memberInfo, $param = [])
    {
        if (empty($param['id']))
        {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }

        if (empty($param['name']))
        {
            return ['code' => 400, 'msg' => '名称不能为空'];
        }

        if (empty($param['level']))
        {
            return ['code' => 400, 'msg' => '等级不能为空'];
        }

        $param['parent_id'] = !empty($param['parent_id']) ? $param['parent_id'] : 0;

        $id = $param['id'];
        unset($param['id']);
        $StrategyCategory = StrategyCategoryModel::where('id',$id)->update($param);

        if ($StrategyCategory)
        {
            $logMessage = '修改社群攻略分类';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($param), $logMessage, $StrategyCategory
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function getStrategyCategoryList($param = [])
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result = StrategyCategoryModel::getStrategyCategoryList($param);
        $count  = StrategyCategoryModel::getStrategyCategoryListCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatStrategyCategoryList($result, $param)
            ],
        ];
    }

    public static function deleteStrategyCategory($memberInfo, $param = [])
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;
        if (empty($param['id']))
        {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        //查看该分类ID下是否存在文章
        $where = [
            'strategy_category_id' => $param['id'],
            'deleted_at' => 0
        ];
        $articleCount = StrategyArticleModel::where($where)->count();
        if ($articleCount > 0) {
            return ['code' => 400, 'msg' => '该分类下存在文章,不能删除'];
        }
        $update = [
            'deleted_at' => time()
        ];
        $delete_result = StrategyCategoryModel::where('id',$param['id'])->update($update);
        if ($delete_result)
        {
            $logMessage = '删除社群攻略分类';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function formatStrategyCategoryList($data=[], $param)
    {
        $list = [];

        if($data)
        {
            foreach ($data as $item)
            {
                $list[] = [
                    "id"          => $item->id,
                    "name"        => $item->name . '',
                    "count"       => StrategyArticleModel::getArticleCountByCategory($item->id, $param),
                    "parentId"    => $item->parent_id . '',
                    "level"       => $item->level . '',
                    "createdTime" => !empty($item->created_at) ? $item->created_at . '' : '',
                    "updatedTime" => !empty($item->updated_at) ? $item->updated_at : '',
                ];
            }
        }
        return $list;
    }



}