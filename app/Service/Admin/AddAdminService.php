<?php
namespace App\Service\Admin;

use App\Model\Admin\AdminModel;

class AddAdminService {

    const SALT_FIGURE = 'YlhAdmin';//固定盐值

    /*
     * 管理员的添加
     * @param Array
     */
    public static function addAdmin($param = [])
    {
        //验证
        $check_input = self::checkInput($param);
        if($check_input){
            return $check_input;
        }

        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);
        $updated_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $param['created_at']     = $created_at;
        $param['updated_at']     = $updated_at;
        $param['admin_password'] = self::encryptPassword($param['admin_password']);

        //管理员添加
        $adminCreate = AdminModel::create($param);

        if($adminCreate){
            return ['code' => 200 , 'msg' => '管理员添加成功！'];
        }
    }

    /*
     * 对添加数据   进行验证
     */
    public static function checkInput($param = [])
    {
        //获取   需要验证的数据
        $admin_type       = $param['admin_type'];//管理员类型
        $admin_login_type = $param['admin_login_type'];//管理员登录类型

        if (!is_numeric($admin_type)) {
            return ['code' => 400, 'msg' => '管理员类型必须为整数！'];
        }
        if (!is_numeric($admin_login_type)) {
            return ['code' => 400, 'msg' => '管理员登录类型必须为整数！'];
        }
    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /*
     * 管理员密码加密
     * 加密规则  md5(md5(密码).固定盐值)
     */
    private static function encryptPassword($password = '')
    {
        return md5(md5($password).self::SALT_FIGURE);
    }
}