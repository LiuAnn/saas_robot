<?php
namespace App\Service\Admin;

use App\Model\Admin\AdminPowerNodeModel;
use App\Model\Admin\PowerModle;

class AdminPowerNodeService {
    /*
     * 获取管理员   拥有的权限
     */
    public static function getAdminPower($adminId = '')
    {
        //验证管理员id  是否为数字
        if(!is_numeric($adminId)){
            return ['code' => 400 , 'msg' => '参数有误！'];
        }

        //获取该管理员  所具有的权限id
        $PowerId = AdminPowerNodeModel::getAdminAndPowerId($adminId);
        if(empty($PowerId)){
            return ['code' => 400 , 'msg' => '该管理员暂无权限！'];
        }

        $powerId = self::disposePowerId($PowerId);
        $powerId = rtrim($powerId,',');

        //管理员权限
        $AdminPower = PowerModle::adminPower($powerId);
        return self::infiniteClassify($AdminPower);
    }

    /*
     * 无限级分类
     */
    public static function infiniteClassify($array = [])
    {
        //第一步 构造数据
        $items = [];
        foreach ($array as $key => $value) {
            $value = get_object_vars($value);
            $items[$value['power_id']] = $value;
        }

        $tree = array();
        //遍历构造的数据
        foreach($items as $key => $value){
            //如果pid这个节点存在
            if(isset($items[$value['power_pid']])){
                //把当前的$value放到pid节点的son中 注意 这里传递的是引用 为什么呢？
                $items[$value['power_pid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    /*
     * 处理出权限id
     */
    public static function disposePowerId($PowerId)
    {
        $powerId = '';
        foreach ($PowerId as $k => $v){
            $powerId .= $v -> power_id.",";
        }
        return $powerId;
    }

    /*
     * 管理员权限添加
     */
    public static function addAdminPowerNode($param = [])
    {
        //验证  管理员id是否为数字
        if(!is_numeric($param['admin_id'])){
            return ['code' => 400 , 'msg' => '管理员参数有误！'];
        }

        //过滤掉不存在的权限id
        $PowerId = self::powerIdCount($param['power_id']);
        if($PowerId == 'no'){
            return ['code' => 400 , 'msg' => '查询不到权限！'];
        }

        //查询管理员是否存在权限
        $PowerIsId = AdminPowerNodeModel::getAdminIsPower($param['admin_id'],$PowerId);
        if(!empty($PowerIsId)){
            return ['code' => 400 , 'msg' => '该管理员存在此权限，请重新选择！'];
        }

        //验证  权限是否被删除过   是  进行回复   否  进行添加
        $Fpower = AdminPowerNodeModel::formerlyPower($param['admin_id'],$PowerId);
        if(!empty($Fpower)){
            $FadminPowerId = [];
            $time = self::formatTimeStamp(time());
            foreach ($Fpower as $k => $v){
                $FadminPowerId[] = $v->power_id;
            }

            //数据恢复
            $Rpower = AdminPowerNodeModel::recoverAdminPower($param['admin_id'],$FadminPowerId,$time);
            if($Rpower){
                $newPower = array_diff($PowerId,$FadminPowerId);
                if(empty($newPower)){
                    return ['code' => 200 , 'msg' => '管理员权限添加成功！'];
                }else{
                    return self::jointAdd($newPower,$param['admin_id']);
                }
            }
        }

        return self::jointAdd($PowerId,$param['admin_id']);
    }

    /*
     * 权限添加
     */
    public static function jointAdd($PowerId,$AdminId){
        //数据处理
        $data = self::addAdminPowerData($PowerId,$AdminId);
        $create = AdminPowerNodeModel::addPowerAdmin($data);

        if($create){
            return ['code' => 200 , 'msg' => '管理员权限添加成功！'];
        }
    }

    /*
     * 删除管理员权限
     */
    public static function deleteAdminPowerNode($param = []){
        //查询权限是否存在
        $PowerId = self::powerIdCount($param['power_id']);
        if($PowerId == 'no'){
            return ['code' => 400 , 'msg' => '查询不到权限！'];
        }

        //查询管理员是否存在权限
        $PowerIsId = AdminPowerNodeModel::getAdminIsPower($param['admin_id'],$PowerId);
        if(empty($PowerIsId)){
            return ['code' => 400 , 'msg' => '该管理员不存在此权限，请重新选择！'];
        }

        //删除权限
        $time = self::formatTimeStamp(time());
        $delete = AdminPowerNodeModel::deleteAdminPower($param['admin_id'],$PowerId,$time);

        if($delete){
            return ['code' => 200 , 'msg' => '管理员权限删除成功'];
        }
    }

    /*
     * 管理员权限添加的数据处理
     */
    public static function addAdminPowerData($PowerId = [],$adminId = ''){
        //获取当前时间戳
        $time = self::formatTimeStamp(time());
        $param = [];
        foreach ($PowerId as $k => $v){
            $param[$k]['admin_id'] = $adminId;
            $param[$k]['power_id'] = $v;
            $param[$k]['created_at'] = $time;
            $param[$k]['updated_at'] = $time;
        }
        return $param;
    }

    /*
     * 根据  权限id  获取权限数据
     */
    public static function powerIdCount($param = []){
        $PowerIdCount =  PowerModle::powerIdCount($param);

        if(empty($PowerIdCount)){
            return 'no';
        }

        $powerId = [];
        foreach ($PowerIdCount as $k => $v){
            $powerId[] = $v -> power_id;
        }
        return $powerId;
    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }
}