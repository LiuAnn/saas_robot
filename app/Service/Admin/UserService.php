<?php
namespace App\Service\Admin;

use App\Model\Admin\GroupManageModel;
use App\Model\Robot\UnitManageModel;
use App\Model\Community\CommunityInvitationGroupModel;
use App\Service\Community\GroupService;
use App\Model\Admin\UserModel;
use App\Model\Member\MemberModel;
use Illuminate\Support\Facades\Redis;

class UserService{

    /*
     * 查看用户列表
     * @param Array
     */
    public static function getUserList($param)
    {

        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;

        $result    = UserModel::getUserList($param,$page,$pageSize);

        $count     = UserModel::getUserCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => self::formatCommunityInvitationGroupList($result)
            ],
        ];

    }


    public static function formatCommunityInvitationGroupList($data)
    {

        $list = [];

        if($data)
        {
            foreach ($data as $value)
            {


                $orderInfo = self::getUserTotalSum($value->mid,$value->user_type,'','',$value->sup_id);

                $list[]=[
                    "id"                    => $value->id,  //用户表自增ID
                    "mid"                   => $value->mid, //与悦淘关联用户ID
                    "userName"              => $value->nickname, //昵称
                    "mobile"                => $value->mobile,  //手机号
                    "userType"              => $value->user_type,  //用户身份
                    "userType_desc"         => self::getExamineStatus($value->user_type), //用户身份
                    "created_time"          => $value->created_time, //注册日期
                    "userGmv"               => $orderInfo['totalMoney'], //用户总GMV
                    "orderCount"            => $orderInfo['orderCount'], //用户总订单数量
                    "groupCount"            => $orderInfo['groupCount'], //群数量
                    "groupOwnCount"         => $orderInfo['groupOwnCount'], //群主数量
                    "groupMemberCount"      => $orderInfo['groupMemberCount'] //群用户数量
                ];
            }
        }

        return $list;
    }


    public static function getExamineStatus($num='')
    {
        $str = '';//用户身份 1:普通用户 2:社群管理人员 3专属机器人
        switch ($num) {
            case 1:  //
                $str = '普通用户';
                break;
            case 2:  //
                $str = '社群管理人员';
                break;
            case 3:  //
                $str = '专属机器人';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }


    /*
     * 查看单个用户统计数据
     * @param Array
     */
    public static function getUserinfo($param)
    {

        if(!isset($param['userid'])||empty($param['userid']))
        {
            return ["code"=>400,"msg"=>"用户标识不能为空"];
        }

        $userinfo = UserModel::getUserInfo($param['userid']);

        if(empty($userinfo)||$userinfo==null)
        {
            return ["code"=>400,"msg"=>"用户信息不存在"];
        }

       $beginTime = date("Y-m-d",strtotime("-16 day"));

       $endTime = date("Y-m-d",strtotime("+1 day"));

       if(isset($param['beginTime'])&&$param['beginTime']!=null)
       {
            $beginTime = $param['beginTime'];
       }

       if(isset($param['endTime'])&&$param['endTime']!=null)
       {
            $endTime = $param['endTime'];
       }

       $orderInfo = self::getUserTotalSum($userinfo->mid,$userinfo->user_type,$beginTime,$endTime,$userinfo->sup_id);

       $dateList =self::getDateFromRange($beginTime,$endTime);

       $memberInfo = [];
       $gmvInfo = [];
       $orderCountInfo = [];
       foreach ($dateList as $k => $timeitem) {

            $memberInfo[$k]['date'] = $timeitem;
            $memberInfo[$k]['count'] = 0;
            $gmvInfo[$k]['date'] = $timeitem;
            $gmvInfo[$k]['count'] = 0;
            $orderCountInfo[$k]['date'] = $timeitem;
            $orderCountInfo[$k]['count'] = 0;
            
            foreach ($orderInfo['getEveryDayTotalmember'] as $keys => $val) {

              if($val->times==$timeitem){

                  $memberInfo[$k]['count'] = $val->count_1;
              }
            }

            foreach ($orderInfo['getEveryDayTotalGmv'] as $key => $v) {

              if($v->times==$timeitem){

                  $gmvInfo[$k]['count'] = $v->totalmoney/100;
                  $orderCountInfo[$k]['count'] = $v->count_1;
              }
            }
       } 

        return [
            "code" => 200,
            "data" => [
                "userGmv"               => $orderInfo['totalMoney'], //用户总GMV
                "orderCount"            => $orderInfo['orderCount'], //用户总订单数量
                "groupCount"            => $orderInfo['groupCount'], //群数量
                "groupOwnCount"         => $orderInfo['groupOwnCount'], //群主数量
                "groupMemberCount"      => $orderInfo['groupMemberCount'], //群用户数量
                "totalGmvByTime"        => $orderInfo['totalGmvByTime'],
                "orderCountByTime"        => $orderInfo['orderCountByTime'],
                "groupCountByTime"        => $orderInfo['groupCountByTime'],
                "groupOwnCountByTime"        => $orderInfo['groupOwnCountByTime'],
                "groupMemberCountByTime"        => $orderInfo['groupMemberCountByTime'],
                "totalMemssage"         => $orderInfo['totalMemssage'],
                "totalMemssageByTime"   => $orderInfo['totalMemssageByTime'],
                "getgroupmamangeRobot"  => $orderInfo['getgroupmamangeRobot'],
                "memberInfo"            => $memberInfo,
                "gmvInfo"               => $gmvInfo,
                "orderCountInfo"        => $orderCountInfo,

            ]
        ];

    }



    public static function getUserTotalSum($mid=0,$user_type=0,$startTime="",$beginTime="",$sup_id = 0)
    {

        $data = [
            'totalMoney'=>0,
            'orderCount'=>0,
            'groupCount'=>0,
            'groupOwnCount'=>0,
            'groupMemberCount'=>0,
        ];

        if($startTime!="")
        {
            $data['getEveryDayTotalmember'] = [];
            $data['getEveryDayTotalGmv'] = [];
            $data['totalGmvByTime'] = 0;
            $data['orderCountByTime'] = 0;
            $data['groupCountByTime'] = 0;
            $data['groupOwnCountByTime'] =  0;
            $data['groupMemberCountByTime'] = 0;
            $data['totalMemssage'] = 0;
            $data['totalMemssageByTime'] = 0;
            $data['getgroupmamangeRobot'] = 0;
             
        }

        switch ($user_type) {
            case 1:
                    
                    $groupCount = GroupManageModel::getCommunityCount("",0,$mid);

                    $result = GroupManageModel::getUserOneGmv($mid);

                    $groupOwnCount = GroupManageModel::getgroupownCount('',$mid);

                    $groupMemberCount = GroupManageModel::getUserNumber(0,'',$mid);

                    $data =  [

                            'totalMoney'=>$result->totalmoney/100,
                            'orderCount'=>$result->count_1,
                            'groupCount'=>$groupCount,
                            'groupOwnCount'=>$groupOwnCount,
                            'groupMemberCount'=>$groupMemberCount,
                        ];

                    if($startTime!="")
                    {
                        $data['getEveryDayTotalmember'] = GroupManageModel::getEveryDayTotalmember('',$startTime,$beginTime,$mid);
                        $data['getEveryDayTotalGmv'] = GroupManageModel::getUserEveryDayTotalGMV($mid,$startTime,$beginTime);

                        $orderInfo = GroupManageModel::getUserOneGmv($mid,$startTime,$beginTime);

                        $data['totalGmvByTime'] = $orderInfo->totalmoney/100;
                        $data['orderCountByTime'] = $orderInfo->count_1;
                        $data['groupCountByTime'] = GroupManageModel::getCommunityCount("",0,$mid,[],$startTime,$beginTime);
                        $data['groupOwnCountByTime'] =  GroupManageModel::getgroupownCount('',$mid,$startTime,$beginTime);
                        $data['groupMemberCountByTime'] = $groupMemberCount = GroupManageModel::getUserNumber(0,'',$mid,$startTime,$beginTime);
                        $data['totalMemssage'] = 0;
                        $data['totalMemssageByTime'] = 0;
                        $data['getgroupmamangeRobot'] = 0;
                    }

                break;
            
            case 2:
                
                   $groupCount = GroupManageModel::getgroupmamangeCount(1,$mid,'','',$sup_id);

                   $groupOwnCount = GroupManageModel::getgroupownmamangeCount(1,$mid,'','',$sup_id);

                   $groupMemberCount = GroupManageModel::getgroupownmamangeMemberCount($mid,'','',$sup_id);

                   $result = GroupManageModel::getgroupmamangeGMV($mid,'','',$sup_id);

                   $data = [

                            'totalMoney'=>$result->totalmoney/100,
                            'orderCount'=>$result->count_1,
                            'groupCount'=>$groupCount,
                            'groupOwnCount'=>$groupOwnCount,
                            'groupMemberCount'=>$groupMemberCount,
                        ];


                    if($startTime!="")
                    {

                        $data['getEveryDayTotalmember'] = GroupManageModel::getMamangeEveryDayTotalmember($mid,$startTime,$beginTime,$sup_id);
                        $data['getEveryDayTotalGmv'] = GroupManageModel::getgroupmamangeGMV($mid,$startTime,$beginTime,$sup_id);

                        $orderInfo = GroupManageModel::getgroupmamangeGMVV2($mid,$startTime,$beginTime,$sup_id);

                        $data['totalGmvByTime'] = $orderInfo->totalmoney/100;
                        $data['orderCountByTime'] = $orderInfo->count_1;
                        $data['groupCountByTime'] = GroupManageModel::getgroupmamangeCount(1,$mid,$startTime,$beginTime,$sup_id);
                        $data['groupOwnCountByTime'] =  GroupManageModel::getgroupownmamangeCount(1,$mid,$startTime,$beginTime,$sup_id);
                        $data['groupMemberCountByTime'] = $groupMemberCount = GroupManageModel::getgroupownmamangeMemberCount($mid,$startTime,$beginTime,$sup_id);
                        $data['totalMemssage'] = GroupManageModel::getMessageCount('','','',$mid,$sup_id);
                        $data['totalMemssageByTime'] = GroupManageModel::getMessageCount('',$startTime,$beginTime,$mid,$sup_id);                   
                        $data['getgroupmamangeRobot'] =$sup_id>0?1:11; //;GroupManageModel::getgroupmamangeRobot($mid);
                    } 

                break;

            case 3:
                    //获取专属用户绑定的机器人绑定的群
                    $getRoomIdsV1 = array_map("array_shift",UserModel::getRoomIdsByWxid($mid));
                    $getRoomIdsV2 = array_map("array_shift",UserModel::getRoomIdsBymid($mid));

                    $getRoomIdsV2 = [];

                    $groupCount = count(array_unique(array_merge($getRoomIdsV1,$getRoomIdsV2)));

                    if($groupCount<1){

                        break;
                    }

                    $roomIds = implode(",",array_unique(array_merge($getRoomIdsV1,$getRoomIdsV2)));



                    $groupOwnCount = GroupManageModel::getgroupownCount($roomIds);

                    $groupMemberCount = GroupManageModel::getUserNumber(0,$roomIds);

                    $result = GroupManageModel::getTotalGMV_V1($roomIds);

                    $data = [

                            'totalMoney'=>$result->totalmoney/100,
                            'orderCount'=>$result->count_1,
                            'groupCount'=>$groupCount,
                            'groupOwnCount'=>$groupOwnCount,
                            'groupMemberCount'=>$groupMemberCount,
                        ];


                    if($startTime!="")
                    {
                        $data['getEveryDayTotalmember'] = GroupManageModel::getEveryDayTotalmember($roomIds,$startTime,$beginTime);
                        $data['getEveryDayTotalGmv'] = GroupManageModel::getEveryDayTotalGMV($roomIds,$startTime,$beginTime);
                       
                        $orderInfo = GroupManageModel::getTotalGMV_V1($roomIds,$startTime,$beginTime);

                        $data['totalGmvByTime'] = $orderInfo->totalmoney/100;
                        $data['orderCountByTime'] = $orderInfo->count_1;
                        $data['groupCountByTime'] = GroupManageModel::getgroupmamangeCount(1,$mid,$startTime,$beginTime);
                        $data['groupOwnCountByTime'] =  GroupManageModel::getgroupownCount($roomIds,0,$startTime,$beginTime);
                        $data['groupMemberCountByTime'] = $groupMemberCount = GroupManageModel::getUserNumber(0,$roomIds,0,$startTime,$beginTime);
                        $data['totalMemssage'] = 0;
                        $data['totalMemssageByTime'] = 0;
                        $data['getgroupmamangeRobot'] = 0;

                    }

                break;

            default:
                # code...
                break;
        }


        return $data;
    }

    /*
     * 赠送用户群数量
     * @param Array
     */
    public static function givUserGroup($memberInfo,$param)
    {

        if(!isset($param['userid'])||empty($param['userid']))
        {
            return ["code"=>400,"msg"=>"用户标识不能为空"];
        }

        if(!isset($param['add_num'])||empty($param['add_num'])||!intval($param['add_num']))
        {
            return ["code"=>400,"msg"=>"赠送数量不能为空"];
        }

        if(!isset($param['reason'])||empty($param['reason']))
        {
            return ["code"=>400,"msg"=>"赠送原因不能为空"];
        }

        $param['admin_id'] = $memberInfo['adminUserId'];
        //查询群助手
        $assistant = CommunityInvitationGroupModel::getLeastUserGroupAssistant(1);
        //查询订单信息
        $orderInfo = UserModel::getUserInfo($param['userid']);

        if (!$orderInfo) {
            return ['code' => 400, 'msg' => '订单信息不存在'];
        }

        //组装数据
        for ($i = 0; $i < $param['add_num']; $i ++) {
            $groupNo = GroupService::getRandNumber();
            $createData [] = [
                'mid' => $orderInfo->mid,
                'ordersn' => 0,
                'group_number' => $groupNo,
                'group_name' => config('community.groupName').$groupNo,
                'examine_status' => 2,
                'addtime' => time(),
                'assistant_wx' => $assistant ? $assistant->assistant_wx : '',
                'tutor_wx' => config('community.tutorInfo')[0]['tutorWx'],
                'type' => 2,
            ];
        }

        $addLog = UserModel::insetGiveLog($param);

        $res = CommunityInvitationGroupModel::insertData($createData);        

        if ($res) {
            return ['code' => 200, 'msg' => '赠送成功'];
        }
        return ['code' => 400, 'msg' => '赠送失败'];

    }

    public static function getExclusiveRobotList($param)
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;

        $result    = UserModel::getExclusiveRobotList($param,$page,$pageSize);    
        
        $count     = UserModel::getExclusiveRobotCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => self::formatExclusiveRobotList($result)
            ],
        ];    
    }

    public static function formatExclusiveRobotList($data)
    {

        $list = [];

        if($data)
        {
            foreach ($data as $value)
            {

                $list[]=[
                    "id"                    => $value->id,  //用户表自增ID
                    "mid"                   => $value->mid, //与悦淘关联用户ID
                    "userName"              => $value->user_name, //昵称
                    "phone"                 => $value->phone,  //手机号
                    "company_name"          => $value->company_name,  //公司名称
                    "brand_name"            => $value->brand_name, //品牌名
                    "industry"              => $value->industry,  //所属行业
                    "group_num"             => self::getGroupnumForStatus($value->group_num),
                    "status"                => $value->status,  //审核状态
                    "status_remark"         => self::getExamineStatusV1($value->status), //审核状态描述
                    "created_at"            => $value->created_at,//申请时间 
                    "examine_admin"         => UserModel::getAdminUserName($value->verifier_mid) //审核管理员姓名
                ];
            }
        }

        return $list;
    }

    public static function getGroupnumForStatus($num='')
    {
        $str = '';//用户身份 1:普通用户 2:社群管理人员 3专属机器人
        switch ($num) {
            case 1:  //
                $str = '1-29个';
                break;
            case 2:  //
                $str = '30-49个';
                break;
            case 3:  //
                $str = '50-99个';
                break;
            case 4:  //
                $str = '100以上';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }

    public static function getExamineStatusV1($num='')
    {
        $str = '';//用户身份 1:普通用户 2:社群管理人员 3专属机器人
        switch ($num) {
            case 0:  //
                $str = '未审核';
                break;
            case 1:  //
                $str = '通过';
                break;
            case 2:  //
                $str = '未通过';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }


    public static function examineExclusiveRobot($memberInfo,$param)
    {

        if(!isset($param['id'])||empty($param['id']))
        {
            return ["code"=>400,"msg"=>"审核信息错误"];
        }

        $exclusiveRobotInfo = UserModel::getExclusiveRobotinfo($param['id']);

        if(empty($exclusiveRobotInfo)||$exclusiveRobotInfo==null){

            return ["code"=>400,"msg"=>"审核信息不存在"];
        }

        if(!isset($param['status'])||empty($param['status']))
        {   
            return ["code"=>400,"msg"=>"审核状态不能为空"];
        }

        if($param['status']==1)
        {
            if(!isset($param['wx_alias'])||empty($param['wx_alias']))
            {   
                return ["code"=>400,"msg"=>"用户微信号不能为空"];
            }

            $updateExclusiveRobotinfo['phone'] = $param['phone'];
            $updateExclusiveRobotinfo['user_name'] = $param['user_name'];
            $updateExclusiveRobotinfo['limit_group'] = $param['limit_group'];
            $updateExclusiveRobotinfo['updated_at'] = date("Y-m-d H:i:s",time());
            $updateExclusiveRobotinfo['verifier_mid'] = $memberInfo['adminUserId'];

            UserModel::updateExclusiveRobotinfo($param['id'],$updateExclusiveRobotinfo);

            $data['wx_alias'] = $param['wx_alias'];
            $data['wxid_type'] = 3;
            $data['mid'] = $exclusiveRobotInfo->mid;

            UserModel::insetwxwhitelist($data);

            $assistant['assistant_wx'] = $param['wx_alias'];
            $assistant['count_num'] = 0;
            $assistant['type'] = 2;
            $assistant['mid'] = $exclusiveRobotInfo->mid;

            UserModel::insetassistant($assistant);

            $userinfo = UserModel::getUserInfoBymid($exclusiveRobotInfo->mid);

            $user['user_type'] = 3;
            $user['exclusiveRobotInfo'] = md5($userinfo->mobile.time());

            UserModel::updateusertype($exclusiveRobotInfo->mid,$user);

        }else{

            $updateExclusiveRobotinfo['phone'] = $param['phone'];
            $updateExclusiveRobotinfo['user_name'] = $param['user_name'];
            $updateExclusiveRobotinfo['limit_group'] = $param['limit_group'];
            $updateExclusiveRobotinfo['updated_at'] = date("Y-m-d H:i:s",time());
            $updateExclusiveRobotinfo['verifier_mid'] = $memberInfo['adminUserId'];
            $updateExclusiveRobotinfo['status'] = 2;

            UserModel::updateExclusiveRobotinfo($param['id'],$updateExclusiveRobotinfo);
        }


        return ["code"=>200,"msg"=>"操作成功"];


    }


    public static function getUserAdminList($memberInfo,$param)
    {   

        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;
        $team_id = 0;

        if (isset($param['startTime']) && !empty($param['startTime'])) {
            $startTime = $param['startTime'];
            //$param['startTime'] = date('Y-m-d 00:00:00', $param['startTime']);
            if (!isset($param['endTime'])) {
                $param['endTime'] = strtotime(date('Y-m-d 23:59:59', $startTime));
            } else {
                $param['endTime'] = strtotime(date('Y-m-d 23:59:59', $param['endTime']));
            }
        }

        if(isset($memberInfo['platform_identity'])&&!empty($memberInfo['platform_identity']))
        {
            $param['platform_identity'] = $memberInfo['platform_identity'];
        }


        if(isset($param['team_id'])&&$param['team_id']>0){

           $team_mids = UnitManageModel::getMidByGroupId($param['team_id']);

           if(!empty($team_mids)){

              $param['team_mids'] =  implode(',',$team_mids);
           }
        }

        $result    = UserModel::getUserAdminList($param,$page,$pageSize);

        $count     = UserModel::getUserAdminCount($param);

        if(isset($param['startTime']))
        {   
            if($param['startTime']==$param['endTime'])
            {   
                $timeList[] = date("Y-m-d",$param['startTime']);
            }else{
                $timeList = self::getDateFromRange(date("Y-m-d",$param['startTime']),date("Y-m-d",$param['endTime']));
            }

            $timeList = array_reverse(array_slice(array_reverse($timeList),0,30));
        }
        //array_reveres()

        $list = [];

        foreach ($result as $key => $value) {
            
            $list[$key]['key'] = ($page-1)*$pageSize+$key+1;
            $list[$key]['mid'] = $value->mid;
            $list[$key]['name'] = $value->name;
            $list[$key]['memberCount'] = $value->memberCount;
            $list[$key]['groupCount'] = $value->groupCount;
            $list[$key]['orderCount'] = $value->orderCount;
            $list[$key]['orderAmount'] = empty($value->orderAmount)?0:$value->orderAmount/100;

            /**
             * 统计信息先去redis拿，没有在查sql，并保存一天信息
             */
            // 机器人列表
            $robotList = Redis::get('admin:user:list:robot:list:'.$value->mid);
            if (empty($robotList)){
                //保存前一天的数据
                $list[$key]['robotList'] = UserModel::getAdminRobotList($value->mid);
                Redis::set('admin:user:list:robot:list:'.$value->mid,json_encode($list[$key]['robotList']));
                Redis::expire('admin:user:list:robot:list:'.$value->mid, 86400 - (time() + 8 * 3600) % 86400);
            }else {
                $list[$key]['robotList'] = json_decode($robotList,true);
            }
            // 发送素材条数
            $relodeCount = Redis::get('admin:user:list:send:count:'.$value->mid);
            if (empty($relodeCount)){
                //保存前一天的数据
                $list[$key]['relodeCount'] = UserModel::getUserSendInfo($param,$value->mid);
                Redis::set('admin:user:list:send:count:'.$value->mid,$list[$key]['relodeCount']);
                Redis::expire('admin:user:list:send:count:'.$value->mid, 86400 - (time() + 8 * 3600) % 86400);
            }else {
                $list[$key]['relodeCount'] = $relodeCount;
            }
            // 发送群数
            $sendGroupCount = Redis::get('admin:user:list:send:group:count:'.$value->mid);
            if (empty($sendGroupCount)){
                //保存前一天的数据
                $list[$key]['sendGroupCount'] = UserModel::getUserSendGroupInfo($param,$value->mid);
                Redis::set('admin:user:list:send:group:count:'.$value->mid,$list[$key]['sendGroupCount']);
                Redis::expire('admin:user:list:send:group:count:'.$value->mid, 86400 - (time() + 8 * 3600) % 86400);
            }else {
                $list[$key]['sendGroupCount'] = $sendGroupCount;
            }
            $list[$key]['orderListByTime'] = [];
            $list[$key]['robotInfo'] = [];

            // if(count($list[$key]['robotList'])<2)
            // {
            //     $list[$key]['robotInfo'][0] = $list[$key];
            //     $robotList = [];
            //     $robotList = json_decode(json_encode($list[$key]['robotList']));

            //     $list[$key]['robotInfo'][0]['memberCount'] = $list[$key]['memberCount'];
            //     $list[$key]['robotInfo'][0]['groupCount'] = $list[$key]['groupCount'];
            //     $list[$key]['robotInfo'][0]['orderCount'] = $list[$key]['orderCount'];
            //     $list[$key]['robotInfo'][0]['orderAmount'] = $list[$key]['orderAmount'];
            //     $list[$key]['robotInfo'][0]['robotName'] = empty($robotList)?'':$robotList[0];
            //     $list[$key]['robotInfo'][0]['relodeCount'] = $list[$key]['relodeCount'];
            //     $list[$key]['robotInfo'][0]['sendGroupCount'] = $list[$key]['sendGroupCount'];
                
            // }else{

            //     $getInfoByMid = UserModel::getInfoByMid($param,$value->mid);

            //     foreach ($getInfoByMid as $item => $vo) {
                    
            //         $list[$key]['robotInfo'][$item]['memberCount'] = $vo->memberCount;
            //         $list[$key]['robotInfo'][$item]['groupCount'] = $vo->groupCount;
            //         $list[$key]['robotInfo'][$item]['orderCount'] = $vo->orderCount;
            //         $list[$key]['robotInfo'][$item]['orderAmount'] = empty($vo->orderAmount)?0:$vo->orderAmount/100;
            //         $list[$key]['robotInfo'][$item]['robotName'] = $vo->wx_name;
            //         $list[$key]['robotInfo'][$item]['relodeCount'] = UserModel::getUserSendInfoV2($param,$vo->robot_id);
            //         $list[$key]['robotInfo'][$item]['sendGroupCount'] = UserModel::getUserSendGroupInfoV2($param,$vo->robot_id);
            //     }
            // }

            if(isset($param['startTime'])){
                $getOrderListByTime = UserModel::getOrderListByTime($param,$value->mid,$timeList);
                $list[$key]['orderListByTime'] = $getOrderListByTime;
            }
        }

        if(isset($param['orderBy']))
        {
            switch ($param['orderBy']) {
                case 9:
                    array_multisort(array_column($list,'relodeCount'),SORT_DESC,$list);
                    $list = array_slice($list,($page-1)*$pageSize,($page-1)*$pageSize+$pageSize);
                    break;
                case 10:
                    array_multisort(array_column($list,'relodeCount'),SORT_ASC,$list);
                    $list = array_slice($list,($page-1)*$pageSize,($page-1)*$pageSize+$pageSize);
                    break;
                case 11:
                    array_multisort(array_column($list,'sendGroupCount'),SORT_DESC,$list);
                    $list = array_slice($list,($page-1)*$pageSize,($page-1)*$pageSize+$pageSize);
                    break;
                case 12:
                    array_multisort(array_column($list,'sendGroupCount'),SORT_ASC,$list);
                    $list = array_slice($list,($page-1)*$pageSize,($page-1)*$pageSize+$pageSize);
                    break;          
                default:
                    # code...
                    break;
            }

            if($param['orderBy']<13&&$param['orderBy']>8)
            {

                foreach ($list as $k => $v) {
                    $v['key'] = ($page-1)*$pageSize+$k+1;
                }
            }
        }

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $pageSize,
                "list"      => $list
            ],
        ];
    }

    private static function getDateFromRange($startdate, $enddate){

        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);

        // 计算日期段内有多少天
        $days = ($etimestamp-$stimestamp)/86400+1;

        // 保存每天日期
        $date = array();

        for($i=0; $i<$days; $i++){
            $date[] = date('Y-m-d', $stimestamp+(86400*$i));
        }

        return $date;
    }


    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /*
     * 管理员密码加密
     * 加密规则  md5(md5(密码).固定盐值)
     */
    private static function encryptPassword($password = '')
    {
        return md5(md5($password).self::SALT_FIGURE);
    }

    public static function getUserInfoById($mid)
    {
        return UserModel::getUserInfoBymid($mid);
    }

    public static function getUserInfoByPhone($mobile)
    {
        return UserModel::getUserInfoByPhone($mobile);
    }
}