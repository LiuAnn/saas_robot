<?php
namespace App\Service\Admin;

use App\Model\Admin\GroupManageModel;
use App\Model\Admin\AdminUserModel;
use App\Model\Admin\GroupModel;
use App\Model\Community\GroupManageModel as CommunityGroupManageModel;
use App\Model\Member\MemberModel;
use App\Model\RobotOrder\RobotModel;
use App\Model\User\UserModel;
use App\Model\Community\ProductModel;
use App\Model\Admin\UserModel as AUserModel;
use App\Service\Community\ProductService;

use App\Model\Community\GroupManageModel as groupComModel;

class GroupManageService
{

    const SALT_FIGURE = 'YlhAdmin';//固定盐值
    const LIVE_INFO_URL = 'http://gateway.daren.tech/open/v1/open/v1/liveRoom/getLiveUserByLiveMid';
    const LIVE_SUM_URL = 'http://gateway.daren.tech/open/v1/open/v1/liveRoom/getLiveNumByTime';

    /*
     * 申请群审核列表
     * @param Array
     */
    public static function getGroupVerifyList($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        // $list = GroupManageModel::getGroupVerifyList($param,$page,$pageSize);

        // $totalCount = GroupManageModel::getGroupVerifyListCount($param);

        // foreach ($list as $key => $item) {

        //     $list[$key]->addtime = self::formatTimeStamp($item->addtime);
        //     $list[$key]->examine_time = self::formatTimeStamp($item->examine_time);
        //     switch ($item->type) {
        //       case 1:
        //         $list[$key]->form = "小程序";
        //         break;
        //       case 2:
        //         $list[$key]->form = "小悦客户端";
        //         break;
        //       default:
        //         $list[$key]->form = "小程序";
        //         break;
        //     }
        // }

        // $data = [
        //     "totalCount"=>$totalCount,
        //     "list"=>empty($list)?[]:$list
        // ];

        // return ["code"=>200,"data"=>$data];

        $result = GroupManageModel::getGroupVerifyList($param, $page, $pageSize);

        $count = GroupManageModel::getGroupVerifyListCount($param);

        return [
            "code" => 200,
            "data" => [
                "total" => $count,
                "page" => $page,
                "pageSize" => $pageSize,
                "list" => self::formatCommunityInvitationGroupList($result)
            ],
        ];

    }


    public static function formatCommunityInvitationGroupList($data)
    {

        $list = [];

        if ($data) {
            foreach ($data as $value) {
                $memberInfo = MemberModel::getMemberInfo(['mid' => $value->mid]);

                $adminMemberInfo = AdminUserModel::getAdminUserInfo(['admin_user_id' => $value->examine_mid]);

                if (!empty($memberInfo)) {
                    $mobile = $memberInfo->mobile;
                    $jointime = $memberInfo->jointime;
                    if (!empty($memberInfo->nickname)) {
                        $nickname = $memberInfo->nickname;
                    } else {
                        if (!empty($memberInfo->truename)) {
                            $nickname = $memberInfo->truename;
                        } else {
                            $nickname = '';
                        }
                    }

                } else {
                    $nickname = '';
                    $mobile = "--";
                    $jointime = 0;
                }

                $countInfo = GroupManageModel::getCommunityInvitationGroupCountInfo(['mid' => $value->mid]);

                $list[] = [
                    "id" => $value->id,
                    "mid" => $value->mid,
                    "mobile" => $mobile,
                    "jointime" => $jointime > 0 ? date("Y-m-d H:i:s", $memberInfo->jointime) : "--",
                    "nickname" => $nickname,
                    "groupNumber" => $value->group_number,
                    "images" => $value->images,
                    "addtime" => date("Y-m-d H:i:s", $value->addtime),
                    "examineStatus" => $value->examine_status,
                    "examineStatusName" => self::getExamineStatus($value->examine_status),
                    "examineMember" => !empty($adminMemberInfo->staff_name) ? $adminMemberInfo->staff_name : '',
                    "examineMid" => $value->examine_mid,
                    "examineTime" => !empty($value->examine_time) ? date("Y-m-d H:i:s", $value->examine_time) : '',
                    "examineDesc" => $value->examine_desc,
                    "assistantWx" => $value->assistant_wx,
                    "tutorWx" => $value->tutor_wx,
                    "roomId" => $value->room_id,
                    "directPushNum" => !empty($countInfo) ? $countInfo->direct_push_num : 0,
                    "intervalPushNum" => !empty($countInfo) ? $countInfo->interval_push_num : 0,
                    "type" => $value->type,
                    "form" => self::getGroupForm($value->type)
                ];
            }
        }

        return $list;
    }


    public static function getExamineStatus($num = '')
    {
        $str = '';//'审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        switch ($num) {
            case 0:  //
                $str = '升级生成群号不审核';
                break;
            case 1:  //
                $str = '待审核';
                break;
            case 2:  //
                $str = '审核通过';
                break;
            case 3:  //
                $str = '审核不通过';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }


    public static function getGroupForm($num = '')
    {
        $str = '';//'审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        switch ($num) {
            case 0:  //
                $str = '微信小程序';
                break;
            case 1:  //
                $str = '微信小程序';
                break;
            case 2:  //
                $str = '小悦平台';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }

    /*
     * 申请群审核详情
     * @param Array
     */
    public static function getGroupVerifyInfo($param)
    {

        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result = GroupManageModel::getGroupVerifyInfo($param);

        if (empty($result)) {
            return ["code" => 400, "data" => '没有此数据'];
        }

        return [
            "code" => 200,
            "data" => self::formatCommunityInvitationGroupInfo($result),
        ];
    }


    public static function formatCommunityInvitationGroupInfo($data)
    {

        $list = [];

        if ($data) {
            $list = [
                "id" => $data->id,
                "mid" => $data->mid,
                "groupNumber" => $data->group_number,
                "images" => $data->images,
                "addtime" => $data->addtime,
                "examineStatus" => $data->examine_status,
                "examineMid" => $data->examine_mid,
                "examineTime" => $data->examine_time,
                "examineDesc" => $data->examine_desc,
                "assistantWx" => $data->assistant_wx,
                "tutorWx" => $data->tutor_wx,
                "roomId" => $data->room_id,
            ];
        }
        return $list;
    }


    /*
     * 群审核
     * @param Array
     */
    public static function toExamineGroup($param)
    {

        // if(!isset($param['id'])||empty($param['id']))
        // {
        //     return ["code"=>400,"msg"=>"申请群信息ID不能为空"];
        // }

        // $invitation_group_info = GroupManageModel::getGroupVerifyInfo($param['id']);

        // if(empty($invitation_group_info)||$invitation_group_info==null)
        // {
        //     return ["code"=>400,"msg"=>"申请群信息不存在"];
        // }

        // $update = [];

        // switch ($param['examineStatus']) {

        //     case 1:

        //         $update["examine_status"] = 2;
        //         $update["examine_mid"] = 1;
        //         $update["examine_time"] = time();


        //         break;
        //     case 2:

        //         $update["examine_status"] = 2;
        //         $update["examine_mid"] = 1;
        //         $update["examine_time"] = time();

        //         if(!isset($param['remark'])||empty($param['remark']))
        //         {
        //             return ["code"=>400,"msg"=>"审核不通过原因不能为空"];
        //         }

        //         $update["examine_desc"] = $param['remark'];

        //         break;
        //     default:
        //         # code...
        //         break;
        // }

        // if(empty($update))
        // {
        //     return ["code"=>400,"msg"=>"审核失败"];
        // }


        // $result = GroupManageModel::upCommunityInvitationGroup($param['id'],$update);

        // if($result){

        //     return ["code"=>200,"msg"=>"审核成功"];

        // }else{

        //     return ["code"=>400,"msg"=>"审核失败"];
        // }

        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        if (!isset($param['examineStatus']) || empty($param['examineStatus'])) {
            return ['code' => 400, 'msg' => '审核不能为空！'];
        }

        if ($param['examineStatus'] == 3) {

            if (!isset($param['examineDesc']) || empty($param['examineDesc'])) {
                return ['code' => 400, 'msg' => '审核原因不能为空！'];
            }
        }

        $result = GroupManageModel::getGroupVerifyInfoV1($param);

        if (empty($result)) {
            return ["code" => 400, "msg" => '没有此数据'];
        }

        $upData['examine_status'] = $param['examineStatus'];//审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        $upData['examine_mid'] = isset($memberInfo['adminUserId']) ? $memberInfo['adminUserId'] : 0;//审核人id
        $upData['examine_time'] = time();//审核时间
        $upData['examine_desc'] = !empty($param['examineDesc']) ? $param['examineDesc'] : '';//不通过的原因


        $upGroup = GroupManageModel::upCommunityInvitationGroup(['id' => $param['id']], $upData);


        if ($param['examineStatus'] == 2) {
            $time = time();

            $date = self::formatTimeStamp($time);

            //直推用户添加数 start
            $countInfo = GroupManageModel::getCommunityInvitationGroupCountInfo(['mid' => $result->mid]);

            if (empty($countInfo)) {
                $addCountData['mid'] = $result->mid;
                $addCountData['direct_push_num'] = 1;
                $addCountData['created_at'] = $date;

                $directPushNum = $addCountData['direct_push_num'];

                $countRes = GroupManageModel::addCommunityInvitationGroupCount($addCountData);
                $countInfoRes = GroupManageModel::addCommunityInvitationGroupCountInfo($addCountData);

            } else {

                $upCountData['direct_push_num'] = $countInfo->direct_push_num + 1;
                $upCountData['updated_at'] = $date;

                $directPushNum = $upCountData['direct_push_num'];

                $countRes = GroupManageModel::upCommunityInvitationGroupCount(['mid' => $result->mid], $upCountData);

                $addCountInfoData['mid'] = $result->mid;
                $addCountInfoData['direct_push_num'] = 1;
                $addCountInfoData['created_at'] = $date;
                $countInfoRes = GroupManageModel::addCommunityInvitationGroupCountInfo($addCountInfoData);
            }
            //直推用户添加数 end

            self::checkZt($result->mid, $directPushNum);


            //间推用户添加数 start
            $shipInfo = GroupManageModel::getMemberShipInfo(['mid' => $result->mid]);

            if (!empty($shipInfo)) {
                $paths = $shipInfo->path;
                $paths = trim($paths, "0,");
                $path = explode(",", $paths);
                if ($path) {
                    foreach ($path as $value) {
                        $jianTuicountInfo = GroupManageModel::getCommunityInvitationGroupCountInfo(['mid' => $value]);

                        if (empty($jianTuicountInfo)) {
                            $addJtCountData['mid'] = $value;
                            $addJtCountData['interval_push_num'] = 1;
                            $addJtCountData['created_at'] = $date;

                            $intervalPushNum = $addJtCountData['interval_push_num'];

                            $countRes = GroupManageModel::addCommunityInvitationGroupCount($addJtCountData);
                            $countInfoRes = GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountData);

                        } else {

                            $upJtCountData['interval_push_num'] = $jianTuicountInfo->interval_push_num + 1;
                            $upJtCountData['updated_at'] = $date;

                            $intervalPushNum = $upJtCountData['interval_push_num'];

                            $countRes = GroupManageModel::upCommunityInvitationGroupCount(['mid' => $value], $upJtCountData);

                            $addJtCountInfoData['mid'] = $value;
                            $addJtCountInfoData['interval_push_num'] = 1;
                            $addJtCountInfoData['created_at'] = $date;
                            $countInfoRes = GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountInfoData);
                        }


                        self::checkJt($value, $intervalPushNum);


                    }
                }
            }

            //间推用户添加数 end

        }


        return ["code" => 200, "msg" => '操作成功'];

    }


    //直推
    public static function checkZt($mid, $directPushNum)
    {

    }

    //间推
    public static function checkJt($mid, $directPushNum)
    {

    }

    public static function getChannelList()
    {
        $page = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"] = ($page - 1) * $display;
        $param["display"] = $display;

        $result = GroupModel::getChannelList($param);
        $result = json_decode(json_encode($result), true);
        $count = GroupModel::getChannelListCount();

        return [
            "total" => $count,
            "page" => $page,
            "pageSize" => $display,
            "data" => $result
        ];
    }


    public static function is_timestamp($timestamp)
    {
        if (!is_numeric($timestamp)) return false;
        if (strtotime(date('Y-m-d H:i:s', $timestamp)) == $timestamp) {
            return $timestamp;
        } else return false;
    }


    /*
     * 群列表
     * @param Array
     */
    public static function getAdminDataStatistics($param)
    {
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $num = ($page - 1) * $pageSize;

        $robotWxAlias = CommunityGroupManageModel::getHouTaiRobotWxAlias();

        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                return [];
            }
        }
        if (isset($param['tag_id']) && !empty($param['tag_id'])) {
            $param['tag_id'] = [$param['tag_id']];
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                return [];
            }
        }
        $data = GroupModel::getHouTaiGroupList($param, $page, $pageSize);
        $count = GroupModel::getHouTaiGroupListCount($param);
        $count = json_decode(json_encode($count), true);
        //以下默认获取一周数据
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d ', $param['beginTime']);
            $param['endTime'] = date('Y-m-d ', $param['endTime']);
        }
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => &$item) {
                $num++;
                //机器人微信号位置显示机器人名称
                $param['roomId'] = $roomId = $item->id;
                //管理员名称
                $item->num = $num;
                if ($item->bind_mid) {
                    $item->adminName = UserModel::getUserNameByMid($item->bind_mid);
                } else {
                    $item->adminName = UserModel::getUserNameByMid($item->rmid);
                }
                //有效订单
                //$item->orderNum = $item->orderNum;
                //销售额
                $item->orderAmount = $item->orderAmount / 100;
                //浏览次数商品数量
                $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                //浏览次数人数数量
                $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                //当前群成员数量
                $item->groupMemberNum = GroupModel::getGroupUserCount($param);


                //消费返佣
                $item->groupCommission = '0.00';
                //男性占比
                if (empty($item->groupMemberNum)) {
                    $item->groupManProportion = '0%';
                    $item->groupWomenProportion = '0%';
                } else {
                    $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                    //$womenScale = 1 - $manScale;
                    $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                    $item->groupManProportion = $manScale * 100 . '%';
                    //女性占比
                    $item->groupWomenProportion = $womenScale * 100 . '%';
                }
                $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';

                unset($param['roomId']);
                //机器人状态
                //$item->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
                $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
                $tag_name = CommunityGroupManageModel::getTagDataName($item->tag_id);
                $item->tag_name = !empty($tag_name) ? $tag_name : '无';
//                $item->band_group = empty($param['sup_id'])?'绑定群主':'';
                $item->band_group = $item->mid > 0 ? '' : '绑定群主';


            }
        }
        $return['total'] = $count ? $count[0]['cnt'] : 0;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        $data = json_decode(json_encode($data), true);
        $return['data'] = array_values($data);
        $return['wx_alias'] = $robotWxAlias;
        return $return;
    }


    /*
     * 后台订单折线图
     * 昨日数据
     */
    public static function getOrderGraph($param)
    {
        if (isset($param['beginTime']) || isset($param['endTime'])) {
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
            return ['code' => 200001, 'msg' => '时间格式不正确'];
        }
        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
        $endTime = date('Y-m-d 23:59:59', $param['endTime']);
        //开始结束时间容错
        //if ($param['beginTime'] == $endTime) {
        //  $endTime = date('Y-m-d 23:59:59', strtotime($endTime));
        //}
        $param['endTime'] = $endTime;
        //获取所有机器人号
        if (isset($param['mid']) && $param['mid'] > 0) {
            $robotWxAlias = CommunityGroupManageModel::getRobotWxid($param['mid'], 1);
            $wxAliasString = '';
            if (!empty($robotWxAlias)) {
                foreach ($robotWxAlias as $v) {
                    $wxAliasString .= "'" . $v->wxid . "',";
                }
                $wxAliasString = trim($wxAliasString, ',');
            }
        }
        if (isset($param['robotWx']) && !empty($param['robotWx'])) {
            $wxAliasString = "'" . $param['robotWx'] . "'";
        }
        if (!empty($wxAliasString)) {
            $param['wxAliasString'] = $wxAliasString;
        }
        //用户订单数据  orderNum  订单数量; orderAmount  订单金额
        $orderTotalData = GroupManageModel::getOrderTotalNumGroupDate($param, $format);
        $orderTotalData = json_decode(json_encode($orderTotalData), true);
        $param['beginTime'] = strtotime($param['beginTime']);
        $param['endTime'] = strtotime($param['endTime']);
        $param['format'] = $format;
        $dataKeys = [
            'orderNum',
            'orderAmount'
        ];
        $orderTotalData = GroupManageModel::completionDateArray($param, $dataKeys, $orderTotalData);
        return $orderTotalData;
    }


    /*
     * 群列表
     * @param Array
     */
    public static function getCommunityList($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $wxid = "";

        if (isset($param['wxid']) && !empty($param['wxid'])) {

            $param['wxid'] = GroupManageModel::getRobotInfo($param['wxid']);

            if ($param['wxid'] == null) {

                return ['code' => 400, "msg" => "该机器人未使用"];
            }

            $wxid = $param['wxid'];
        }


        $communityList = GroupManageModel::getCommunityList($page, $pageSize, $param);

        $communityCount = GroupManageModel::getCommunityCount($wxid, 0, 0, $param);


        $list = [];

        foreach ($communityList as $key => $item) {
            switch ($item->platform_identity) {
                case '7323ff1a23f0bf1cda41f690d4089353':
                    $channelName = '悦淘';
                    break;
                case '89dafaaf53c4eb68337ad79103b36aff':
                    $channelName = '大人';
                    break;
                default:
                    $channelName = '其他';
                    break;
            }
            $list[$key]['idNumber'] = ($page - 1) * $pageSize + $key + 1;
            $list[$key]['room_id'] = $item->id;
            $list[$key]['rid'] = $item->rid;
            $list[$key]['mid'] = $item->mobile;
            $list[$key]['groupName'] = $item->name;
            $list[$key]['robotStatus'] = 1;
            $list[$key]['member_count'] = GroupManageModel::getUserNumber(0, $item->id);

            $member_count = GroupManageModel::getErveySexCount($item->id);

            $list[$key]['manProportion'] = intval($member_count->manCount * 100) . "%";
            $list[$key]['womanProportion'] = intval($member_count->womanCount * 100) . "%";

            $list[$key]['totalPrice'] = GroupManageModel::getTotalGMV($item->id);
            $list[$key]['orderCount'] = $item->count_1;
            $list[$key]['totalBouns'] = GroupManageModel::getBonusByMid($item->mid, $item->id);

            $list[$key]['adminName'] = self::getAdminstrByType($item->wxid_type, $item->wxmid, $item->bind_mid);//
            $list[$key]['channelName'] = $channelName;
        }

        $data = [
            'totalCount' => $communityCount,
            'list' => $list
        ];

        return ["code" => 200, "data" => $data];
    }


    /*
     * 机器人列表
     * @param Array
     */
    public static function getRobotList($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $param['bind_master'] = isset($param['bind_master']) ? $param['bind_master'] : 0;
        $param['bind_group_number'] = isset($param['bind_group_number']) ? $param['bind_group_number'] : 0;
        $param['orderCount'] = isset($param['orderCount']) ? $param['orderCount'] : 0;
        $param['orderPrice'] = isset($param['orderPrice']) ? $param['orderPrice'] : 0;

        $robotList = GroupManageModel::getRobotList($page, $pageSize, $param);

        $robotCount = GroupManageModel::getRobotCount($param);

        $list = [];
        foreach ($robotList as $key => $item) {
            switch ($item->platform_identity) {
                case '7323ff1a23f0bf1cda41f690d4089353':
                    $channelName = '悦淘';
                    break;
                case '89dafaaf53c4eb68337ad79103b36aff':
                    $channelName = '大人';
                    break;
                default:
                    $channelName = '其他';
                    break;
            }
            $list[$key]['idNumber'] = ($page - 1) * $pageSize + $key + 1;
//           $list[$key]['id'] = $item->id; //机器人编码
            $list[$key]['id'] = $item->order_by; //机器人编码$list['code_number'] =$v->order_by;
            $list[$key]['orderPrice'] = empty($item->orderPrice) ? 0 : $item->orderPrice; //机器人绑定微信号
            $list[$key]['orderCount'] = empty($item->orderCount) ? 0 : $item->orderCount; //机器人绑定微信号
            $list[$key]['wxNumber'] = $item->wx_alias . '号'; //机器人绑定微信号
            $list[$key]['robotStatus'] = 1;//机器人状态
            $param['wxid'] = $item->wxid;
//           $list[$key]['groupOwnerCount'] = GroupManageModel::getGroupOwnerCount($param); //绑定群主数量
            $list[$key]['groupOwnerCount'] = empty($item->bind_mid_num) ? 0 : $item->bind_mid_num; //绑定群主数量
//           $list[$key]['groupCount'] = GroupManageModel::getCommunityCount($item->wxid);//绑定群数量
            $list[$key]['groupCount'] = empty($item->bind_group_num) ? 0 : $item->bind_group_num;//绑定群数量
            $list[$key]['count_num'] = GroupManageModel::getCommunityCount($item->wxid);//绑定群数量
            $list[$key]['adminName'] = self::getAdminstrByType($item->wxid_type, $item->mid, $item->bind_mid);//
            $list[$key]['channelName'] = $channelName;
        }

        $data = [
            "totalCount" => $robotCount,
            "list" => $list
        ];

        return ["code" => 200, "data" => $data];
    }


    public static function getAdminstrByType($type = 1, $mid = 0, $bind_mid = 0)
    {

        $str = "";

        switch ($type) {
            case 1:
                $mid = $bind_mid > 0 ? $bind_mid : $mid;
                $nickname = GroupManageModel::getNickNameByMid($mid);
                $str .= $nickname;
                break;
            case 2:
                $nickname = GroupManageModel::getNickNameByMid($mid);
                $str .= $nickname;
                break;
            default:
                $str = "";
                break;
        }

        return $str;
    }

    /*
 * 群主信息列表
 * @param Array
 */
    public static function getHouTaiGroupOwnerList($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;


        if (isset($param['wxid']) && !empty($param['wxid'])) {

            $param['wxid'] = GroupManageModel::getRobotInfo($param['wxid']);

            if ($param['wxid'] == null) {

                return ['code' => 400, "msg" => "该机器人未使用"];
            }
        }
        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
            $param['searchMid'] = $param['searchMid'][0];
        }

        $robotList = GroupManageModel::getHouTaiGroupOwnerList($page, $pageSize, $param);

        $robotCount = GroupManageModel::getHouTaiGroupOwnerCountV1($param);
        $count = json_decode(json_encode($robotCount), true);
        if (is_array($robotList) && count($robotList) > 0) {
            foreach ($robotList as $key => &$item) {
                $roomId = explode(',', $item->ids);
                $item->id = ($page - 1) * $pageSize + $key + 1;
                $param['roomId'] = $roomId;
                //管理员名称--TODO 后台显示手机号
                $item->adminName = UserModel::getUserPhoneByMid($item->mid);
                //群名称
                $item->name = self::getAdminstrByType($item->wxid_type, $item->rmid, $item->bind_mid);
                //有效订单
                //$item->orderNum = GroupModel::getOrderNum($param);
                $item->orderNum = $item->orderNum ?: 0;
                $item->limitGroupCount = GroupManageModel::getlimitgroupCount($item->rmid ? $item->rmid : 0) ?: 0;
                //销售额
                //$item->orderAmount = GroupModel::getOrderTotalAmountByRoomId($param) / 100;
                $item->orderAmount = ($item->orderAmount / 100) ?: 0;
                //浏览次数商品数量
                $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                //浏览次数人数数量
                $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                //当前群成员数量
                $item->groupMemberNum = GroupModel::getGroupUserCount($param);

                //消费返佣
                $item->groupCommission = '0.00';
                //男性占比
//                if (empty($item->groupMemberNum)) {
//                    $item->groupManProportion = '0%';
//                    $item->groupWomenProportion = '0%';
//                } else {
//                    $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
//                    //$womenScale = 1 - $manScale;
//                    $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
//                    $item->groupManProportion = $manScale * 100 . '%';
//                    //女性占比
//                    $item->groupWomenProportion = $womenScale * 100 . '%';
//                }
                $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';
                $item->mid = UserModel::getUserPhoneByMid($item->mid);
                unset($param['roomId']);
                $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
                $tag_name = CommunityGroupManageModel::getTagDataName($item->tag_id);
                $item->tag_name = !empty($tag_name) ? $tag_name : '无';
            }
        }
        //$robotList['wx_alias'] = $robotWxAlias;
        $robotList = json_decode(json_encode($robotList), true);
        $return['data'] = array_values($robotList);
        $return['total'] = $count ? $count[0]['cnt'] : 0;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }


    /*
     * 群主信息列表
     * @param Array
     */
    public static function getGroupOwnerList($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;


        if (isset($param['wxid']) && !empty($param['wxid'])) {

            $param['wxid'] = GroupManageModel::getRobotInfo($param['wxid']);

            if ($param['wxid'] == null) {

                return ['code' => 400, "msg" => "该机器人未使用"];
            }
        }

        $robotList = GroupManageModel::getGroupOwnerList($page, $pageSize, $param);

        $robotCount = GroupManageModel::getGroupOwnerCountV1($param);


        $list = [];

        foreach ($robotList as $key => $item) {
            switch ($item->platform_identity) {
                case '7323ff1a23f0bf1cda41f690d4089353':
                    $channelName = '悦淘';
                    break;
                case '89dafaaf53c4eb68337ad79103b36aff':
                    $channelName = '大人';
                    break;
                default:
                    $channelName = '其他';
                    break;
            }
            $list[$key]['id'] = ($page - 1) * $pageSize + $key + 1;
            $list[$key]['rid'] = $item->rid;
            $list[$key]['mid'] = $item->mobile;
            $list[$key]['robotStatus'] = 1;
            $list[$key]['groupCount'] = $item->count;
            $list[$key]['midv2'] = $item->mid;
            $list[$key]['channelName'] = $channelName;

            $limitgroupCount = GroupManageModel::getlimitgroupCount($item->mid ? $item->mid : 0);

            $list[$key]['limitgroupCount'] = $limitgroupCount == null ? 0 : $limitgroupCount;
            $list[$key]['adminName'] = self::getAdminstrByType($item->wxid_type, $item->wxmid, $item->bind_mid);//

        }

        $data = [

            "totalCount" => $robotCount,
            "list" => $list
        ];

        return ["code" => 200, "data" => $data];
    }

    /*
     * 获取统计信息
     * @param Array
     */
    public static function getStatisticsInfo($param)
    {
        $source = $param['source'] ?? '';

        if (isset($param['beginTime']) && !empty($param['beginTime'])) {
            $beginTime = $param['beginTime'];
        } else {
            $beginTime = date("Y-m-d", strtotime("-16 day"));
        }

        if (isset($param['endTime']) && !empty($param['endTime'])) {
            $endTime = $param['endTime'];
        } else {

            $endTime = date("Y-m-d", time());
        }

        $dateList = self::getDateFromRange($beginTime, $endTime);

        // $beginTime = date("Y-m-d",strtotime($beginTime)+3600*24);
        if ($beginTime == $endTime) {
            $endTime = date("Y-m-d", strtotime($endTime) + 3600 * 24);
        }

        $list = [];

        if (isset($param['mid']) && $param['mid'] > 0) {
            $memberInfo = UserService::getUserInfoById($param['mid']);
            $param['sup_id'] = $memberInfo->sup_id ?? 0;
            $roomWxIds = GroupModel::getRoomIdsByMid($param);
            $roomWxIds = json_decode(json_encode($roomWxIds), true);
            $wxIds = array_column($roomWxIds, 'wxid');
            $roomIds = array_column($roomWxIds, 'id');
            $roomWxId = array_column($roomWxIds, 'room_wxid');
            $param['wxIds'] = implode(',', array_unique(array_filter($wxIds)));
            $param['roomId'] = implode(',', array_filter($roomIds));
            $param['roomWxId'] = implode(',', array_unique(array_filter($roomWxId)));
            $getRobotCount = count(GroupModel::getRobotWxIds($param));
            $getWhaleRobotCount = count(GroupModel::getRobotWxIds($param, '', 1));
            $getEnterpriseRobotCount = count(GroupModel::getRobotWxIds($param, '', 2));
            $getBindRobotCount = count($wxIds);
        } else {
            //获取机器人总数量：
            $getRobotCount = GroupManageModel::getRobotCount();
            //获取探鲸机器人总数量：
            $getWhaleRobotCount = GroupManageModel::getRobotCount([], 1);
            //获取企业微信机器人总数量：
            $getEnterpriseRobotCount = GroupManageModel::getRobotCount([], 2);
            //获取绑定微信机器人总量：
            $getBindRobotCount = GroupManageModel::getBindRobotCounts($source);
        }

        $show = 0;

        if (isset($param['robot_id']) && $param['robot_id'] > 0) {


            $wxid = GroupManageModel::getRobotInfo($param['robot_id']);

            if ($wxid == null) {

                return ["code" => 400, "msg" => "机器人不存在"];
            }

            $param['wxid'] = $wxid;


            //获取机器人下面的群id
            $getRoomIds = GroupManageModel::getRoomIds($wxid);
            //获取总社群数量
            $getGroupCount = GroupManageModel::getCommunityCount($wxid);
            //获取今天新增社群数量
            $getTodayGroupCount = GroupManageModel::getCommunityCount($wxid, 1);
            //根据时间获取社群数量
            $getGroupCountByTime = GroupManageModel::getCommunityCount($wxid, 0, 0, [], $beginTime, $endTime);
            //总群主数量
            $getGroupOwnerCount = GroupManageModel::getGroupOwnerCount($param);
            //根据时间获取群主数量
            $getGroupOwnerCountByTime = GroupManageModel::getGroupOwnerCount($param, $beginTime, $endTime);

            $orderInfo = GroupManageModel::getTotalGMV_V1($getRoomIds);

            //获取总GMV
            $getTotalGMV = $orderInfo->totalmoney / 100;
            //获取总G订单数量
            $getOrderCount = $orderInfo->count_1;

            $orderInfoV1 = GroupManageModel::getTotalGMV_V1($getRoomIds, $beginTime, $endTime);

            //获取总GMV
            $getTotalGMVByTime = $orderInfoV1->totalmoney / 100;
            //获取总G订单数量
            $getOrderCountByTime = $orderInfoV1->count_1;

            //获取总用户数量
            $gettotalUser = GroupManageModel::getUserNumber(0, $getRoomIds);
            //获取新增用户数量
            $gettodaytotalUser = GroupManageModel::getUserNumber(1, $getRoomIds);
            //根据时间获取总用户数量
            $gettotalUserByTime = GroupManageModel::getUserNumber(0, $getRoomIds, 0, $beginTime, $endTime);
            //获取消息总数量
            $getMessageCount = GroupManageModel::getMessageCount($getRoomIds, '', '', 0, 0, $source);
            //按时间获取消息总数量
            $getMessageCountByTimes = GroupManageModel::getMessageCount($getRoomIds, $beginTime, $endTime, 0, 0, $source);

            $getRobotlist = GroupManageModel::getEveryDayTotalGMV($getRoomIds, $beginTime);

            $getEveryDayTotalmember = GroupManageModel::getEveryDayTotalmember($getRoomIds, $beginTime, $endTime);

            $getEveryDayTotalmessage = GroupManageModel::getMessageCountByTimes($getRoomIds, $beginTime, $endTime);

            $show = 1;

        } else {
            if (isset($param['mid']) && $param['mid'] > 0) {
                //获取总社群数量
                $getGroupCount = GroupManageModel::getCommunityCount('', 0, $param['mid']);
                //获取今天新增社群数量
                $getTodayGroupCount = GroupManageModel::getCommunityCount("", 1, $param['mid']);
                //根据时间获取社群数量
                $getGroupCountByTime = GroupManageModel::getCommunityCount("", 0, $param['mid'], [], $beginTime, $endTime);
                //总群主数量
                $getGroupOwnerCount = 1;
                //根据时间获取群主数量
                $getGroupOwnerCountByTime = 1;

            } else {
                //获取总社群数量
                $getGroupCount = GroupManageModel::getCommunityCount();
                //获取今天新增社群数量
                $getTodayGroupCount = GroupManageModel::getCommunityCount("", 1);
                //根据时间获取社群数量
                $getGroupCountByTime = GroupManageModel::getCommunityCount("", 0, 0, [], $beginTime, $endTime);
                //总群主数量
                $getGroupOwnerCount = GroupManageModel::getGroupOwnerCount();
                //根据时间获取群主数量
                $getGroupOwnerCountByTime = GroupManageModel::getGroupOwnerCount("", $beginTime, $endTime);

            }
            if (isset($param['groupId']) && $param['groupId'] > 0) {

                $orderInfo = GroupManageModel::getTotalGMV_V1($param['groupId'], $beginTime, $endTime);
                //获取总GMV
                $getTotalGMV = $orderInfo->totalmoney / 100;
                //获取总G订单数量
                $getOrderCount = $orderInfo->count_1;

                $orderInfoV1 = GroupManageModel::getTotalGMV_V1($param['groupId'], $beginTime, $endTime);

                //获取总GMV
                $getTotalGMVByTime = $orderInfoV1->totalmoney / 100;
                //获取总G订单数量
                $getOrderCountByTime = $orderInfoV1->count_1;
                //获取总用户数量
                $gettotalUser = GroupManageModel::getUserNumber(0, $param['groupId']);
                //获取新增用户数量
                $gettodaytotalUser = GroupManageModel::getUserNumber(1, $param['groupId']);
                //根据时间获取总用户数量
                $gettotalUserByTime = GroupManageModel::getUserNumber(0, $param['groupId'], 0, $beginTime, $endTime);
                //获取总消息数量
                $getMessageCount = GroupManageModel::getMessageCount($param['groupId'], '', '', 0, 0, $source);
                //按时间获取消息总数量
                $getMessageCountByTimes = GroupManageModel::getMessageCount($param['groupId'], $beginTime, $endTime, 0, 0, $source);

                $getRobotlist = GroupManageModel::getEveryDayTotalGMV($param['groupId'], $beginTime, $endTime);

                $getEveryDayTotalmember = GroupManageModel::getEveryDayTotalmember($param['groupId'], $beginTime, $endTime);

                $getEveryDayTotalmessage = GroupManageModel::getMessageCountByTimes($param['groupId'], $beginTime, $endTime);

                $show = 2;
            } else {
                if (isset($param['mid']) && $param['mid'] > 0) {
                    $roomWxIds = GroupModel::getRoomIdsByMid($param);
                    $roomWxIds = json_decode(json_encode($roomWxIds), true);
                    $wxIds = array_column($roomWxIds, 'wxid');
                    $roomIds = array_column($roomWxIds, 'id');
                    $roomWxId = array_column($roomWxIds, 'room_wxid');
                    $param['wxIds'] = implode(',', array_filter($wxIds));
                    $param['roomId'] = implode(',', array_filter($roomIds));
                    $param['roomWxId'] = implode(',', array_filter($roomWxId));
                    //获取消息总数量
                    $getMessageCount = GroupManageModel::getMessageCount('', '', '', $param['mid'], 0, $source);
                    //按时间获取消息总数量
                    $getMessageCountByTimes = GroupManageModel::getMessageCount('', $beginTime, $endTime, $param['mid'], 0, $source);
                    //根据时间获取总用户数量
                    $gettotalUserByTime = GroupManageModel::getUserNumber(0, '', $param['mid'], $beginTime, $endTime);

                    $orderInfo = GroupManageModel::getTotalGMV_V1($param['roomId']);
                    //获取总GMV
                    $getTotalGMV = $orderInfo->totalmoney / 100;
                    //获取总G订单数量
                    $getOrderCount = $orderInfo->count_1;

                    $orderInfoV1 = GroupManageModel::getTotalGMV_V1($param['roomId'], $beginTime, $endTime);

                    //获取总GMV
                    $getTotalGMVByTime = $orderInfoV1->totalmoney / 100;
                    //获取总G订单数量
                    $getOrderCountByTime = $orderInfoV1->count_1;
                    //获取总用户数量
                    $gettotalUser = GroupManageModel::getUserNumber(0, '', $param['mid']);
                    //获取新增用户数量
                    $gettodaytotalUser = GroupManageModel::getUserNumber(1, '', $param['mid']);


                    $getRobotlist = GroupManageModel::getEveryDayTotalGMV($param['roomId'], $beginTime, $endTime);

                    $getEveryDayTotalmember = GroupManageModel::getEveryDayTotalmember($param['roomId'], $beginTime, $endTime);

                    $getEveryDayTotalmessage = GroupManageModel::getMessageCountByTimes($param['roomId'], $beginTime, $endTime);

                } else {
                    //获取消息总数量
                    $getMessageCount = GroupManageModel::getMessageCount('', '', '', 0, 0, $source);
                    //按时间获取消息总数量
                    $getMessageCountByTimes = GroupManageModel::getMessageCount('', $beginTime, $endTime, 0, 0, $source);
                    //根据时间获取总用户数量
                    $gettotalUserByTime = GroupManageModel::getUserNumber(0, '', 0, $beginTime, $endTime);

                    $orderInfo = GroupManageModel::getTotalGMV_V1(0, '', '', $source);
                    //获取总GMV
                    $getTotalGMV = $orderInfo->totalmoney / 100;
                    //获取总G订单数量
                    $getOrderCount = $orderInfo->count_1;

                    $orderInfoV1 = GroupManageModel::getTotalGMV_V1(0, $beginTime, $endTime, $source);

                    //获取总GMV
                    $getTotalGMVByTime = $orderInfoV1->totalmoney / 100;
                    //获取总G订单数量
                    $getOrderCountByTime = $orderInfoV1->count_1;
                    //获取总用户数量
                    $gettotalUser = GroupManageModel::getUserNumber();
                    //获取新增用户数量
                    $gettodaytotalUser = GroupManageModel::getUserNumber(1);


                    $getRobotlist = GroupManageModel::getEveryDayTotalGMV(0, $beginTime, $endTime, $source);

                    $getEveryDayTotalmember = GroupManageModel::getEveryDayTotalmember(0, $beginTime, $endTime);

                    $getEveryDayTotalmessage = GroupManageModel::getMessageCountByTimes("", $beginTime, $endTime);

                }
            }

        }

        $gmvInfo = [];
        $huihuaInfo = [];
        $memberInfo = [];
        foreach ($dateList as $k => $timeitem) {

            $gmvInfo[$k]['date'] = $timeitem;
            $gmvInfo[$k]['count'] = 0;
            $huihuaInfo[$k]['date'] = $timeitem;
            $huihuaInfo[$k]['count'] = 0;
            $memberInfo[$k]['date'] = $timeitem;
            $memberInfo[$k]['count'] = 0;
            foreach ($getRobotlist as $key => $value) {

                if ($value->times == $timeitem) {

                    $gmvInfo[$k]['count'] = $value->totalmoney / 100;
                }
            }

            foreach ($getEveryDayTotalmember as $keys => $val) {

                if ($val->times == $timeitem) {

                    $memberInfo[$k]['count'] = $val->count_1;
                }
            }

            foreach ($getEveryDayTotalmessage as $ke => $va) {

                if ($va->times == $timeitem) {

                    $huihuaInfo[$k]['count'] = $va->count_1;
                }
            }
        }


        $data = [
            "isShowAll" => $show,
            "todayInfo" => [
                "robotCount" => $getRobotCount,
                "whaleRobotCount" => $getWhaleRobotCount,
                "enterpriseRobotCount" => $getEnterpriseRobotCount,
                "bindRobotCount" => $getBindRobotCount,
                "groupOwnerCount" => $getGroupOwnerCount,
                "getOwnerCountByTime" => $getGroupOwnerCountByTime,
                "getGroupCount" => $getGroupCount,
                "getTodayGroupCount" => $getTodayGroupCount,
                "getGroupCountByTime" => $getGroupCountByTime,
                "totalUser" => $gettotalUser,
                "newUser" => $gettodaytotalUser,
                "totoUserByTime" => $gettotalUserByTime,
                "memssageCount" => $getMessageCount,
                "memssageCountByTime" => $getMessageCountByTimes,
                "gmv" => $getTotalGMV,
                "orderCount" => $getOrderCount,
                "gmvByTime" => $getTotalGMVByTime,
                "orderCountByTime" => $getOrderCountByTime
            ],
            "huihuaInfo" => $memberInfo,
            "memberInfo" => $huihuaInfo,
            "gmvInfo" => $gmvInfo,


        ];

        return ["code" => 200, "data" => $data];
    }

    /*
     * 获取消息列表
     * @param Array
     */
    public static function getMessageList($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $message = GroupManageModel::getMessageList($param, $page, $pageSize);

        $messageCount = GroupManageModel::getMessageCount();

        $list = [];

        // foreach ($robotList as $key => $item) {

        //     $list[$key]['rid'] = $item->rid;
        //     $list[$key]['mid'] = $item->mid;
        //     $list[$key]['robotStatus'] = 1;
        //     $list[$key]['groupCount'] = $item->count;

        //     $limitgroupCount = GroupManageModel::getlimitgroupCount($item->mid);

        //     $list[$key]['limitgroupCount'] = $limitgroupCount==null?0:$limitgroupCount;

        // }

        $data = [

            "totalCount" => $messageCount,
            "list" => $message
        ];

        return ["code" => 200, "data" => $data];

    }

    /*
     * 直播订单排名统计
     * @param Array
     */
    public static function getLiveRankInfo($param)
    {

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $startTime = isset($param['startTime']) ? $param['startTime'] : '';
        $endTime = isset($param['endTime']) ? $param['endTime'] : '';

        $list = GroupManageModel::getLiveRankList($startTime, $endTime, "", $page, $pageSize, $param);

        $totalCount = GroupManageModel::getLiveRankCount($startTime, $endTime);

        $live_ids = implode(array_column($list, "live_people_id"), ",");

        $data['live_mid'] = $live_ids;

        $liveInfo = json_decode(curlPost(self::LIVE_INFO_URL, $data));

        foreach ($list as $key => $value) {

            $value->order_money = $value->order_money / 100;
            $value->sortId = ($page - 1) * $pageSize + 1 + $key;
            $value->nickname = "";
            $value->mobile = "";

            if (isset($liveInfo->data)) {

                foreach ($liveInfo->data as $v) {

                    if ($v->live_id == $value->live_people_id) {
                        $value->nickname = $v->nickname;
                        $value->mobile = $v->mobile;

                        continue;
                    }
                }


            }

        }


        return ["code" => 200, "data" => ['totalCount' => $totalCount, 'list' => $list]];

    }

    /*
     * 每天悦淘直播主播人数
     * @param Array
     */
    public static function getLiveNumByTime($param)
    {
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $data['startTime'] = isset($param['startTime']) ? $param['startTime'] : '';
        $data['endTime'] = isset($param['endTime']) ? $param['endTime'] : '';
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;

        $list = json_decode(curlPost(self::LIVE_SUM_URL, $data));

        return ["code" => 200, "data" => ['totalCount' => $list->total, 'list' => $list->data]];
    }

    /*
    * 每天悦淘直播主播人数
    * @param Array
    */
    public static function getLiveNumByTimeGraph($param)
    {
        if (isset($param['beginTime']) || isset($param['endTime'])) {
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
            return ['code' => 200001, 'msg' => '时间格式不正确'];
        }
        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
        $endTime = date('Y-m-d 23:59:59', $param['endTime']);
        //开始结束时间容错
        //if ($param['beginTime'] == $endTime) {
        //  $endTime = date('Y-m-d 23:59:59', strtotime($endTime));
        //}
        $param['endTime'] = $endTime;

        $data['startTime'] = $param['beginTime'];
        $data['endTime'] = $param['endTime'];
        $data['page'] = 1;
        $data['pageSize'] = 100;

        $list = json_decode(curlPost(self::LIVE_SUM_URL, $data));
        if ($list->code != 200) {
            return ['code' => 400, 'msg' => '大人接口返回错误'];
        }
        $liveNumData = $list->data;
        $liveNumData = array_map(function ($value) {
            return json_decode(json_encode($value), true);
        }, $liveNumData);
        $liveNumData = self::arraySortByColumn($liveNumData, 'day');
//        if ($format == 'hour') {
//            $liveNumData = self::arraySortByColumn($liveNumData, 'hour');
//        }
        $newData = [];
        foreach ($liveNumData as $key => $item) {
            if (isset($newData[$item[$format]]['date']) && $newData[$item[$format]]['date'] == $item[$format]) {
                $newData[$item[$format]]['num'] += $item['num'];
            } else {
                $newData[$item[$format]]['date'] = $item[$format];
                $newData[$item[$format]]['num'] = $item['num'];
            }
        }

        $param['beginTime'] = strtotime($param['beginTime']);
        $param['endTime'] = strtotime($param['endTime']);
        $param['format'] = $format;
        $dataKeys = [
            'num'
        ];
        $newData = GroupManageModel::completionDateArray($param, $dataKeys, $newData);
        return $newData;
    }

    public static function arraySortByColumn($source_array, $order_field, $sort_type = 'SORT_ASC')
    {
        $arrSort = array();
        foreach ($source_array as $uniqid => $row) {
            foreach ($row as $key => $value) {
                $arrSort[$key][$uniqid] = $value;
            }
        }
        array_multisort($arrSort[$order_field], constant($sort_type), $source_array);
        return $source_array;
    }

    /*
     * 小悦机器人管理总群数量
     * @param Array
     */
    public static function getRobotGroupInfoGraph($param)
    {
        if (isset($param['beginTime']) || isset($param['endTime'])) {
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
            return ['code' => 200001, 'msg' => '时间格式不正确'];
        }
        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
        $endTime = date('Y-m-d 23:59:59', $param['endTime']);
        //开始结束时间容错
        //if ($param['beginTime'] == $endTime) {
        //  $endTime = date('Y-m-d 23:59:59', strtotime($endTime));
        //}
        $param['endTime'] = $endTime;

        $robotGroupData = GroupManageModel::getRobotGroupInfoGraph($param, $format);
        $robotGroupData = json_decode(json_encode($robotGroupData), true);
        $param['beginTime'] = strtotime($param['beginTime']);
        $param['endTime'] = strtotime($param['endTime']);
        $param['format'] = $format;
        $dataKeys = [
            'groupNum'
        ];
        $robotGroupData = GroupManageModel::completionDateArray($param, $dataKeys, $robotGroupData);
        return $robotGroupData;
    }


    /*
     * 小悦机器人管理总群数量
     * @param Array
     */
    public static function getRobotGroupInfo($param)
    {
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $startTime = isset($param['startTime']) ? $param['startTime'] : '';
        $endTime = isset($param['endTime']) ? $param['endTime'] : '';

        $list = GroupManageModel::getRobotGroupInfo($startTime, $endTime, $page, $pageSize);

        $totalCount = GroupManageModel::getRobotGroupCount($startTime, $endTime);

        return ["code" => 200, "data" => ['totalCount' => $totalCount, 'list' => $list]];
    }

    /*
     * 获取商品统计
     * @param Array
     */
    public static function getProductStatistics($param, $memberInfo)
    {

        if (isset($memberInfo['platform_identity']) && !empty($memberInfo['platform_identity'])) {
            $param['channel'] = AUserModel::getChannelDataByPlatform($memberInfo['platform_identity']);
        } else {
            $param['channel'] = empty($param['channel']) ? '' : $param['channel'];
        }

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;


        $list = GroupManageModel::getProductStatisticsList($param, $page, $pageSize);

        $totalCount = GroupManageModel::getProductStatisticsCount($param);


        foreach ($list as $key => $value) {
            if (config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods ') {
                switch ($value->good_type) {
                    case '1':
                        $goodsTypeName = $value->channel == 1 ? '悦淘自营' : '大人自营';
                        break;
                    case '2':
                        $goodsTypeName = '京东';
                        break;
                    case '3':
                        $goodsTypeName = '拼多多';
                        break;
                    case '4':
                        $goodsTypeName = '唯品会';
                        break;
                    case '5':
                        $goodsTypeName = '美团';
                        break;
                    case '7':
                        $goodsTypeName = '淘宝';
                        break;
                    case '8':
                        $goodsTypeName = '苏宁';
                        break;
                    case '9':
                        $goodsTypeName = '品牌折扣券';
                        break;
                    default:
                        $goodsTypeName = '其他';
                        break;
                }
                switch ($value->channel) {
                    case '1':
                        $channelName = '悦淘';
                        break;
                    case '2':
                        $channelName = '大人';
                        break;
                    default:
                        $channelName = '其他';
                        break;
                }
                // $goodsTypeName = empty($value->good_type) ? '自营商品' : '京东商品';
            } else {
                $goodsTypeName = '';
            }

            $value->key = ($page - 1) * $pageSize + $key + 1;
            $value->totalmoney = $value->totalmoney / 100;
            $value->unitPrice = $value->totalmoney / $value->goodsNumber;
            // $value->liren = self::getDistribution($value->goods_id,$value,$value->good_type,$value->goods_price_member,$value->actual_price); //群主利润
            // $value->zLiRun = $value->zLiRun>0?$value->zLiRun:0;             //总的利润
            // $value->liren = round($value->liren, 2);
            // $value->zLiRun = round($value->zLiRun, 2);
            $value->click = $value->clickNum ?: 0;
            unset($value->clickNum);
            //返佣金额没有除以4 为直订, 所以除直订平台外要除以4
            //乘以100 除以一万更精确
            if ($memberInfo['platform_identity'] != '75f712af51d952af3ab4c591213dea13') {
                $value->liren = $value->liren > 0 ? round(floor($value->liren / 4) / 100, 2) : 0;
                $value->zLiRun = $value->zLiRun > 0 ? round(floor($value->zLiRun / 4) / 100, 2) : 0;
            } else {
                $value->liren = $value->liren > 0 ? round(floor($value->liren) / 100, 2) : 0;
                $value->zLiRun = $value->zLiRun > 0 ? round(floor($value->zLiRun) / 100, 2) : 0;
            }

            $value->goodsTypeName = $goodsTypeName;
            $value->channelName = $channelName;
            $value->goods_price_orig = $value->goods_price_orig / 100;
            $value->actual_price = $value->actual_price / 100;
            //这里会员价显示实付款金额, 6.5 庞敬龙 改
            $value->goods_price_member = $value->goods_price_orig <= $value->actual_price
                ? round($value->actual_price, 2)
                : round($value->actual_price / $value->goods_num, 2);

        }

        return ["code" => 200, "data" => ['totalCount' => $totalCount, 'list' => $list]];
    }


    public static function getDistribution($id = 0, $sku = [], $good_type, $goods_price_member, $actual_price)
    {
        if ($sku == [] || $id == 0) {
            return 0;
        }
        $distribution_money = 0;
        if ($good_type < 2) {
            $distribution = ProductModel::getDistributionInfoByGoodsId($id);
            $price = empty($sku->goods_price_member) ? 0 : $sku->goods_price_member;
            $buy_price = empty($sku->goods_price_buy) ? 0 : $sku->goods_price_buy;
            $distribution_money = ($price - $buy_price) * $distribution / 100;
        } else if ($good_type == 2) {
            $couponCommission = ($goods_price_member - $actual_price) > 0 ? $goods_price_member - $actual_price : 0;
            $distribution_money = ProductService::getJdEarnricePrice(['couponCommission' => $couponCommission / 100], 103);
        } else if ($good_type == 3) {
            $couponCommission = ($goods_price_member - $actual_price) > 0 ? $goods_price_member - $actual_price : 0;
            $distribution_money = ProductService::getJdEarnricePrice(['couponCommission' => $couponCommission / 100], 104);
        }


        return $distribution_money > 0 ? $distribution_money : 0;
    }


    public static function getGroupLeaderList($param)
    {
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;
        $begin = ($page - 1) * $pageSize;
        $source = isset($param['source']) ? $param['source'] : 1;
        $wxid = isset($param['wxid']) ? $param['wxid'] : '';
        if (empty($wxid)) {
            $wxidData = GroupManageModel::getRobotChannelWxIds(['id' => $source]);
            $wxAliasString = '';
            if (!empty($wxidData)) {
                foreach ($wxidData as $v) {
                    $wxAliasString .= "'" . $v->wxid . "',";
                }
                $wxAliasString = trim($wxAliasString, ',');
            }
        } else {
            $wxAliasString = "'" . $wxid . "'";
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
            $param['searchMid'] = $param['searchMid'][0];
        }

        $list = GroupManageModel::getLeaderListStatisticsList($param, $begin, $pageSize, $wxAliasString);
        $totalCount = GroupManageModel::getgetLeaderListStatisticsCount($param);

        if (is_array($list) && count($list) > 0) {
            foreach ($list as $key => &$item) {
                $roomId = explode(',', $item->ids);
                $item->id = ($page - 1) * $pageSize + $key + 1;
                $param['roomId'] = $roomId;
                if (empty($param['roomId'])) {
                    //管理员名称
                    $item->adminName = 'admin';
                    //群名称
                    $item->name = '暂无';
                    //群数量
                    $item->groupCount = 0;
                    //群主社群数量限制
                    $item->limitGroupCount = 0;
                    //有效订单
                    $item->orderNum = 0;
                    //销售额
                    $item->orderAmount = 0;
                    //浏览次数商品数量
                    $item->browseGoodsNum = 0;
                    //浏览次数人数数量
                    $item->browseUserNum = 0;
                    //当前群成员数量
                    $item->groupMemberNum = 0;

                    //消费返佣
                    $item->groupCommission = '0.00';
                    //男性占比
                    $item->groupManProportion = '0%';
                    $item->groupWomenProportion = '0%';
                } else {
                    //管理员名称--TODO 后台显示手机号
                    //$item->adminName = UserModel::getUserPhoneByMid($item->mid);
                    //群名称
                    $item->name = self::getAdminstrByType($item->wxid_type, $item->rmid, $item->bind_mid);
                    //有效订单
                    //$item->orderNum = GroupModel::getOrderNum($param);
                    $item->orderNum = $item->orderNum ?: 0;
                    $item->limitGroupCount = GroupManageModel::getlimitgroupCount($item->rmid ? $item->rmid : 0) ?: 0;
                    //销售额
                    //$item->orderAmount = GroupModel::getOrderTotalAmountByRoomId($param) / 100;
                    $item->orderAmount = ($item->orderAmount / 100) ?: 0;
                    //浏览次数商品数量
                    $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                    //浏览次数人数数量
                    $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                    //当前群成员数量
                    $item->groupMemberNum = GroupModel::getGroupUserCount($param);

                    //消费返佣
                    $item->groupCommission = '0.00';
                    //男性占比
                    if (empty($item->groupMemberNum)) {
                        $item->groupManProportion = '0%';
                        $item->groupWomenProportion = '0%';
                    } else {
                        $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                        //$womenScale = 1 - $manScale;
                        $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                        $item->groupManProportion = $manScale * 100 . '%';
                        //女性占比
                        $item->groupWomenProportion = $womenScale * 100 . '%';
                    }
                    $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';
                }
                unset($param['roomId']);
                //机器人状态
                $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
            }
        }


        $list = json_decode(json_encode($list), true);
//        $return['data'] = array_values($list);
//        $return['total'] = $totalCount ? $totalCount[0]->cnt : 0;
//        $return['page'] = $page;
//        $return['pageSize'] = $pageSize;

        return ["code" => 200, "data" => ['totalCount' => $totalCount ? $totalCount[0]->cnt : 0, 'list' => $list, 'page' => $page, 'pageSize' => $pageSize]];
    }


    private static function getDateFromRange($startdate, $enddate)
    {

        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);
        // 计算日期段内有多少天
        $days = ($etimestamp - $stimestamp) / 86400 + 1;

        // 保存每天日期
        $date = array();

        for ($i = 0; $i < $days; $i++) {
            $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
        }

        return $date;
    }

    /**
     * 活跃度统计数据
     * @param $param
     * @param $memberInfo
     */
    public static function getGroupActivityStatistics($param, $memberInfo)
    {

        //传入的参数条件
        $startTime = isset($param['startTime']) ? $param['startTime'] : date("Y-m-d", strtotime("-6 day"));
        $endTime = isset($param['endTime']) ? $param['endTime'] : date('Y-m-d', time());
        $bindMid = isset($param['mid']) ? $param['mid'] : 0;
        //获取 当前数据的机器人数据展示
        if ($memberInfo['user_type'] >= 2) {
            $mid = $bindMid > 0 ? $bindMid : $memberInfo['mid'];
            $supId = $memberInfo['sup_id'];
            if (!empty($bindMid) && $bindMid != $memberInfo['mid']) {
                $supId = $memberInfo['mid'];
            }
            $robotWxAlias = groupComModel::getRobotWxid($mid, $supId);
        } else {
            $robotWxAlias = groupComModel::getRobotWxid($memberInfo['mid'], $memberInfo['sup_id'], 1);
        }
        if (!isset($param['robot_wx']) || empty($param['robot_wx'])) {
            $wxAliasString = '';
            if (!empty($robotWxAlias)) {
                foreach ($robotWxAlias as $v) {
                    $wxAliasString .= "'" . $v->wxid . "',";
                }
                $wxAliasString = trim($wxAliasString, ',');
            }
        } else {
            $wxAliasString = "'" . $param['robot_wx'] . "'";
        }

        if (empty($wxAliasString)) {
            return ["code" => 200, "data" => ['list' => [], 'total' => [], 'search' => []]];
        }

        $todayTime = date('Y-m-d', time());
        $todayData = $totalData = [];
        if (strtotime($endTime) >= strtotime($todayTime)) {
            //消息数量
            $todayMsgCountNum = GroupManageModel::msgCountNum($wxAliasString, $todayTime);
            // 发群消息数量
            $todayMsgSourceNum = GroupManageModel::msgSourceNum($wxAliasString, $todayTime);
            //链接点击数
            $todayLinkNum = GroupManageModel::msgLinkNum($wxAliasString, $todayTime);
            //订单数量
            $todayOrderNum = GroupManageModel::msgOrderNum($wxAliasString, $todayTime);
            //成员数量
            $todayMemberNum = GroupManageModel::msgMemberNum($wxAliasString, $todayTime);
            //群数量
            $todayGroupNum = GroupManageModel::msgGroupNum($wxAliasString, $todayTime);
            $todayData = [
                'msg_count_num' => $todayMsgCountNum,
                'source_count_num' => $todayMsgSourceNum,
                'link_count_num' => $todayLinkNum,
                'order_count_num' => $todayOrderNum,
                'member_count_num' => $todayMemberNum,
                'group_count_num' => $todayGroupNum,
                'date' => $todayTime,
            ];
        }
        //获取总的数量
        //消息数量
        $totalData['total_msg_count'] = GroupManageModel::msgCountNum($wxAliasString, '');
        // 发群消息数量
        $totalData['total_source_count'] = GroupManageModel::msgSourceNum($wxAliasString, '');
        //链接点击数
        $totalData['total_link_count'] = GroupManageModel::msgLinkNum($wxAliasString, '');
        //订单数量
        $totalData['total_order_count'] = GroupManageModel::msgOrderNum($wxAliasString, '');
        //成员数量
        $totalData['total_member_count'] = GroupManageModel::msgMemberNum($wxAliasString, '');
        //群数量
        $totalData['total_group_count'] = GroupManageModel::msgGroupNum($wxAliasString, '');


        $ListDataNow = GroupManageModel::getGroupActivityStatistics($startTime, $endTime, $memberInfo, $wxAliasString);
        $ListData = json_decode(json_encode($ListDataNow), true);
        if (!empty($todayData)) {
            array_push($ListData, $todayData);//添加元素
        }
        $searchData ['msg_search_num'] = 0;
        $searchData ['source_search_num'] = 0;
        $searchData ['link_search_num'] = 0;
        $searchData ['order_search_num'] = 0;
        $searchData ['member_search_num'] = 0;
        $searchData ['group_search_num'] = 0;
        if (!empty($ListData)) {
            foreach ($ListData as $v) {
                $searchData ['msg_search_num'] += $v["msg_count_num"];
                $searchData ['source_search_num'] += $v["source_count_num"];
                $searchData ['link_search_num'] += $v["link_count_num"];
                $searchData ['order_search_num'] += $v["order_count_num"];
                $searchData ['member_search_num'] = $v["member_count_num"];
                $searchData ['group_search_num'] = $v["group_count_num"];
            }

        }


        return ["code" => 200, "data" => ['list' => $ListData, 'total' => $totalData, 'search' => $searchData]];

    }


    /**
     * 获取群主分佣明
     * @param $param
     * @param $memberInfo
     */
    public static function getGroupAdminCommission($param, $memberInfo)
    {

        $param['page'] = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize'] = isset($param['pageSize']) ? $param['pageSize'] : 10;

        if (isset($memberInfo['platform_identity']) && !empty($memberInfo['platform_identity'])) {
            $param['channel'] = AUserModel::getChannelDataByPlatform($memberInfo['platform_identity']);
        }


        $result = GroupModel::getGroupAdminCommission($param);
        $count = GroupModel::getGroupAdminCommissionCount($param);

        $data = [];

        foreach ($result as $key => $value) {

            $list['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list = $value;
            $list['good_type'] = getGoodTypeNameByGoodType($value['good_type']);
            $list['channel'] = getChannelNameChannel($value['channel']);
            $list['lirun'] = $value['lirun'] > 0 ? $value['lirun'] : 0;
            $list['actual_price'] = $value['actual_price'] / 100;
            $list['pay_time'] = date("Y-m-d H:i:s", $value['pay_time']);

            $data[] = $list;
        }


        return ["code" => 200, "data" => ['total' => $count, 'page' => $param['page'], 'pageSize' => $param['pageSize'], 'list' => $data]];

    }


    /**
     * 获取群主分佣明
     * @param $param
     * @param $memberInfo
     */
    public static function getGroupAdminCommissionEveryDay($param, $memberInfo)
    {

        $param['page'] = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize'] = isset($param['pageSize']) ? $param['pageSize'] : 10;

        if (isset($memberInfo['platform_identity']) && !empty($memberInfo['platform_identity'])) {
            //$param['platform_identity'] = $memberInfo['platform_identity'];
            $param['channel'] = AUserModel::getChannelDataByPlatform($memberInfo['platform_identity']);
        }

        if($param['channel']==1){

            $result = GroupModel::getGroupAdminCommissionEveryDayForYT($param);
            $count = GroupModel::getGroupAdminCommissionEveryDayForYTCount($param);
            $total_profit = GroupModel::getGroupAdminTotalCommissionEveryDayForYTCount($param);
            $total_profit = is_numeric($total_profit) ? $total_profit/100 : 0;
            $data = [];

                foreach ($result as $key => $value) {


                    $list = $value;
                    $list['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
                    $list['channel'] = getChannelNameChannel($value['channel']);
                    $list['channelId'] = $value['channel'];
                    $list['lirun'] = $value['lirun']/100;
                    $list['true_amount'] = $value['status'] > 0 ? $value['lirun']/100 : 0;
                    $list['ordersn'] = $value['order_no'];
                    $data[] = $list;
                }
        
        }else{

            $result = GroupModel::getGroupAdminCommissionEveryDay($param);
            $count = GroupModel::getGroupAdminCommissionEveryDayCount($param);
            $total_profit = GroupModel::getGroupAdminTotalCommissionEveryDayCount($param);
            $data = [];

            foreach ($result as $key => $value) {


                $list = $value;
                $list['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
                $list['channel'] = getChannelNameChannel($value['channel']);
                $list['channelId'] = $value['channel'];
                $list['lirun'] = $value['lirun'] > 0 ? $value['lirun'] : 0;
                $list['lirun'] = $value['channel'] != 4 ? $list['lirun']/4 : $list['lirun'];
                $list['true_amount'] = $value['true_amount'] > 0 ? $value['true_amount'] : 0;
                $list['mid'] = $value['mid'];
                $data[] = $list;
            }

        }
        $returnData = [
            'total' => $count,
            'page' => $param['page'],
            'pageSize' => $param['pageSize'],
            'total_profit' => $total_profit,
            'list' => $data
        ];
        return ["code" => 200, "data" => $returnData];

    }

    /**
     * 获取群自定义标签列表
     * @param $param
     * @param $array
     */
    public static function getGroupTagsList($param, $memberInfo)
    {
        $param['page'] = isset($param['page']) ? $param['page'] : 1;
        $param['pageSize'] = isset($param['pageSize']) ? $param['pageSize'] : 10;

        $getGroupTagsList = GroupModel::getGroupTagsList($param);

        $count = GroupModel::getGroupTagsCount($param);

        $list = [];

        foreach ($getGroupTagsList as $key => $value) {
           
            $list[$key]['key'] = ($param['page'] - 1) * $param['pageSize'] + $key + 1;
            $list[$key]['categoryName'] = $value['name'];
            $list[$key]['categoryId'] = $value['id'];
            $list[$key]['sort'] = $value['sort'];
        }

        return [
            'total'=>$count,
            'page' => $param['page'],
            'pageSize' => $param['pageSize'],
            'list' => $list,
        ];

    }

    /**
     * 添加自定义标签
     * @param $param
     * @param $array
     */
    public static function addGroupTags($param, $memberInfo)
    {
    
        if(!isset($param['categoryName'])||empty($param['categoryName'])){

            return ['code'=>400,'msg'=>'标签名称不能为空'];
        }

        $issetTags = GroupModel::getGroupTagInfo($param);

        if(!empty($issetTags)){

            return ['code'=>400,'msg'=>'标签名称不能重复'];
        }

        $data = [
            'name'=>$param['categoryName'],
            'sort'=>isset($param['sort'])?$param['sort']:0,
            'jd_category_id'=>isset($param['jdCategoryId'])?$param['jdCategoryId']:0,
            'pdd_category_id'=>isset($param['pddCategoryId'])?$param['pddCategoryId']:0,
            'yt_category_id'=>isset($param['ytCategoryId'])?$param['ytCategoryId']:0,
            'created_at'=>time()
        ];

        $addGroupTags = GroupModel::addGroupTags($param);

        if($addGroupTags){

            return ['code'=>200,'msg'=>'添加成功'];
        }

        return ['code'=>400,'msg'=>'添加失败'];
    }


    /**
     * 编辑自定义标签
     * @param $param
     * @param $array
     */
    public static function editGroupTags($param, $memberInfo)
    {
    
        if(!isset($param['categoryName'])||empty($param['categoryName'])){

            return ['code'=>400,'msg'=>'标签名称不能为空'];
        }

        if(!isset($param['id'])||empty($param['id'])){

            return ['code'=>400,'msg'=>'标签ID不能为空'];
        }

        $tagInfo = GroupModel::getTagInfo($param);

        if(empty($tagInfo)){

            return ['code'=>400,'msg'=>'编辑失败'];
        }

        $issetTags = GroupModel::getGroupTagInfo($param);

        foreach ($issetTags as $k => $v) {
            if($v['id']!=$param['id']){

                return ['code'=>400,'msg'=>'标签名称不能重复'];
            }
        }

        $data = [
            'name'=>$param['categoryName'],
            'sort'=>isset($param['sort'])?$param['sort']:0,
            'jd_category_id'=>isset($param['jdCategoryId'])?$param['jdCategoryId']:0,
            'pdd_category_id'=>isset($param['pddCategoryId'])?$param['pddCategoryId']:0,
            'yt_category_id'=>isset($param['ytCategoryId'])?$param['ytCategoryId']:0,
            'created_at'=>time()
        ];

        $editGroupTags = GroupModel::editGroupTags($param);

        if($editGroupTags){

            return ['code'=>200,'msg'=>'编辑成功'];
        }

        return ['code'=>400,'msg'=>'编辑失败'];
    }


    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /*
     * 管理员密码加密
     * 加密规则  md5(md5(密码).固定盐值)
     */
    private static function encryptPassword($password = '')
    {
        return md5(md5($password) . self::SALT_FIGURE);
    }
}