<?php


namespace App\Service\Admin;


use App\Model\Admin\AdminOperationLogModel;

class AdminOperationLogService
{
    /**
     * 管理员操作日志记录
     * @param $param
     * //操作类型1：新增，2：修改，3：删除 4:登录
     */
    public static function insertAdminOperation($adminId, $type, $changeData, $message, $changeId)
    {
        $insertArray = [];
        //后台操作用户id
        $insertArray['admin_user_id'] = $adminId;
        //操作路径
        //$insertArray['path'] = [];
        //请求内容
        //$insertArray['input'] = [];
        //操作类型1：新增，2：修改，3：删除
        $insertArray['type'] = $type;
        //修改内容
        $insertArray['change_data'] = $changeData;
        //日志描述
        $insertArray['message'] = $message;
        //数据类型1;一维2：二维
        $insertArray['data_type'] = 1;
        //修改或添加的主键id
        $insertArray['relevance_id'] = $changeId;
        //修改的类型，1：视频直播活动，2：商品
        //$insertArray['relevance_type'] = [];
        //创建时间,又称操作时间(时间戳)
        $insertArray['created_time'] = time();
        AdminOperationLogModel::insertAdminOperation($insertArray);
    }
}