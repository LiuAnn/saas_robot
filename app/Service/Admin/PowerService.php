<?php
namespace App\Service\Admin;

use App\Model\Admin\PowerModle;

class PowerService {
    /*
     * 获取所有的权限列表
     */
    public static function getAllPower()
    {
        //获取权限列表
       $list = PowerModle::getList();
       if(empty($list)){
           return ['code' => 400 , 'msg' => '暂无权限信息！'];
       }

       //分类
        return self::infiniteClassify($list);
    }

    /*
     * 无限级分类
     */
    public static function infiniteClassify($array = [])
    {
        //第一步 构造数据
        $items = [];
        foreach ($array as $key => $value) {
            $value = get_object_vars($value);
            $items[$value['power_id']] = $value;
        }

        $tree = array();
        //遍历构造的数据
        foreach($items as $key => $value){
            //如果pid这个节点存在
            if(isset($items[$value['power_pid']])){
                //把当前的$value放到pid节点的son中 注意 这里传递的是引用 为什么呢？
                $items[$value['power_pid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }
}