<?php


namespace App\Service\Admin;
use App\Model\Community\GroupManageModel as CommunityGroupManageModel ;
use App\Model\Admin\GroupManageModel;
use App\Model\Admin\GroupModel;
use App\Model\Admin\UserModel as adminUserModel;
use App\Model\RobotOrder\RobotModel;
use App\Model\User\UserModel;
use App\Service\Robot\UserOperationLogService;
use App\Service\Sign\GroupSignService;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
use App\Service\Community\UseIndexService;
use App\Model\Community\CommunityInvitationGroupModel;
use App\Service\Member\MemberService;
class AdminIndexService
{

    public  static    function is_timestamp($timestamp) {
        if (!is_numeric($timestamp)) return false;
        if(strtotime(date('Y-m-d H:i:s',$timestamp)) == $timestamp) {
            return $timestamp;
        } else return false;
    }
    /*
     * 后台首页数据统计
     * 昨日数据
     */
    public static function getIndexStatistics($param)
    {
        $param['channel']=  adminUserModel::getChannelDataByPlatform($param['platform_identity']);
        //查询开始时间
        if (isset($param['beginTime']) && $param['beginTime']) {
            $beginTime = $param['beginTime'];
            $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
            if (!isset($param['endTime'])) {
                $param['endTime'] = date('Y-m-d 23:59:59', $beginTime);
            } else {
                $param['endTime'] = date('Y-m-d 23:59:59', $param['endTime']);
            }
        }
        //总社群数量是否显示
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            if ($param['user_type'] >= 2) {
                $param['wxIds'] = GroupModel::getAllRobotWxIds($param);
                Log::info('roomIds user_type'.json_encode($param['wxIds']) );
                $param['roomWxId'] = GroupModel::getRoomWxIdsByWxId($param);
                $param['roomId'] = GroupModel::getRoomIdsByWxId($param);
                Log::info('roomIds user_type'.json_encode($param['roomId']) );
            } else {
                $roomWxIds = GroupModel::getRoomIdsByMid($param);
                $roomWxIds = json_decode(json_encode($roomWxIds), true);
                $wxIds = array_column($roomWxIds, 'wxid');
                $roomIds = array_column($roomWxIds, 'id');
                $roomWxId = array_column($roomWxIds, 'room_wxid');
                $param['wxIds'] = array_filter($wxIds);
                $param['roomId'] = array_filter($roomIds);
                $param['roomWxId'] = array_filter($roomWxId);
            }

            //用户订单总数
            $orderTotalNum = GroupModel::getOrderTotalNum($param, '');
            //订单总用户数
            $orderMemberTotalNum = GroupModel::getOrderMemberTotalNum($param, '');
            //一周前新增群近一周的订单总数
            $orderWeekNum = GroupModel::getOrderWeekNum($param, '');
            //用户订单金额
            $orderTotalAmount = GroupModel::getOrderTotalAmount($param, '')/100;
            //总社群数量
            $groupTotalNum = GroupModel::getGroupTotalNum($param, '');
            //总社群移除数量
            $groupTotalRemoveNum = GroupModel::getGroupTotalRemoveNum($param, '');
            //总用户数量
            $groupMemberTotalNum = GroupModel::getNewGroupTotalNum($param, '');
            //新增总人数
            $newIncNum = GroupModel::getNewGroupTotalNum($param);
            //新增群数
            $newGroupNum = GroupModel::getGroupTotalNum($param);
            //通过社区数量
            $passGroupNum = GroupModel::getPassGroupTotalNum($param, '');
        } else {

            $groupInfo = GroupModel::fetchGroupInfo(['id' => $param['roomId']]);

            $param['room_id'] = $param['roomId'];
            $param['roomId'] = [$param['roomId']];
            $param['wxIds'] = [$groupInfo['wxid']];
            $param['roomWxId'] = [$groupInfo['room_wxid']];

            //用户订单总数
            $orderTotalNum = GroupModel::getOrderTotalNum($param, '');
            //订单总用户数
            $orderMemberTotalNum = GroupModel::getOrderMemberTotalNum($param, '');
            //一周前新增群近一周的订单总数
            $orderWeekNum = GroupModel::getOrderWeekNum($param, '');
            //用户订单金额
            $orderTotalAmount = GroupModel::getOrderTotalAmount($param, '') / 100;
            //总社群数量
            $groupTotalNum = 1;
            //总社群移除数量
            $groupTotalRemoveNum = 0;
            //总用户数量
            $groupMemberTotalNum = GroupModel::getNewGroupTotalNum($param, '');
            //新增总人数
            $newIncNum = GroupModel::getNewGroupTotalNum($param);
            //通过社区数量
            $passGroupNum = 0;
            //新增群数
            $newGroupNum = 0;
        }

        //会话消息
        if(empty($param['wxIds'])) {
            $messageNum = 0;
        } else {
            $messageNum = GroupModel::getMessageNum($param, '');
        }
        $return =  [
            'orderTotalNum' => $orderTotalNum,
            'orderMemberTotalNum' => $orderMemberTotalNum,
            'orderWeekNum' => $orderWeekNum,
            'orderTotalAmount' => $orderTotalAmount,
            'groupTotalNum' => $groupTotalNum,
            'groupRemoveTotalNum' => $groupTotalRemoveNum,
            'groupMemberTotalNum' => $groupMemberTotalNum,
            'newIncNum' => $newIncNum,
            'newGroupNum' => $newGroupNum,
            'messageNum' => $messageNum
        ];
        $return['passGroupNum'] = $passGroupNum;
        if ($param['user_type'] >= 2) {
            //机器人数量
            $robotNum = count($param['wxIds']);
            //绑定机器人数量
             $bindRobotNum = GroupModel::getBindRobotNum($param);
            //群主数量
            $groupOwnNum = GroupManageModel::getGroupOwnerCount($param,$param['beginTime'],$param['endTime']);

            $registerNum = GroupModel::getRegisterNum($param);

            $return['robotNum'] = $robotNum;
            $return['bindRobotNum'] = $bindRobotNum;
            $return['groupOwnNum'] = $groupOwnNum;
            $return['registerNum'] = $groupOwnNum+$registerNum;
            $return['vipGroupNum'] = GroupModel::getVipGroupNum($param);
        }
        return $return;
    }


    /*
     * 后台首页数据统计
     * 昨日数据
     */
    public static function getIndexStatisticsTotal($param)
    {
        $param['channel']=  adminUserModel::getChannelDataByPlatform($param['platform_identity']);
        //总社群数量是否显示
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            if ($param['user_type'] >= 2) {
                $param['wxIds'] = GroupModel::getAllRobotWxIds($param);
                $roomWxAndId = GroupModel::getRoomWxAndIdsByWxId($param);
                $param['roomId'] = array_keys($roomWxAndId);
                $param['roomWxId'] = array_values($roomWxAndId);
            } else {
                $roomWxIds = GroupModel::getRoomIdsByMid($param);
                $roomWxIds = json_decode(json_encode($roomWxIds), true);
                $wxIds = array_column($roomWxIds, 'wxid');
                $roomIds = array_column($roomWxIds, 'id');
                $roomWxId = array_column($roomWxIds, 'room_wxid');
                $param['wxIds'] = array_filter($wxIds);
                $param['roomId'] = array_filter($roomIds);
                $param['roomWxId'] = array_filter($roomWxId);
            }
            //用户订单总数
            $orderTotalNum = GroupModel::getOrderTotalNum($param, 'total');
            //订单总用户数
            $orderMemberTotalNum = GroupModel::getOrderMemberTotalNum($param, 'total');
            //用户订单金额
            $orderTotalAmount = GroupModel::getOrderTotalAmount($param, 'total')/100;
            //总社群数量
            $groupTotalNum = GroupModel::getGroupTotalNum($param, 'total');
            //总社群移除数量
            $groupTotalRemoveNum = GroupModel::getGroupTotalRemoveNum($param, 'total');
            //总用户数量
            $groupMemberTotalNum = GroupModel::getNewGroupTotalNum($param, 'total');
            //通过社区数量
            $passGroupNum = GroupModel::getPassGroupTotalNum($param, 'total');
        } else {
            $groupInfo = GroupModel::fetchGroupInfo(['id' => $param['roomId']]);
            $param['roomId'] = [$param['id']];
            $param['wxIds'] = [$groupInfo['room_wxid']];
            $param['roomWxId'] = [$groupInfo['room_wxid']];
            //用户订单总数
            $orderTotalNum = GroupModel::getOrderTotalNum($param, 'total');
            //订单总用户数
            $orderMemberTotalNum = GroupModel::getOrderMemberTotalNum($param, 'total');
            //用户订单金额
            $orderTotalAmount = GroupModel::getOrderTotalAmount($param, 'total')/100;
            //总社群数量
            $groupTotalNum = 1;
            //总社群移除数量
            $groupTotalRemoveNum = 0;
            //总用户数量
            $groupMemberTotalNum = GroupModel::getNewGroupTotalNum($param, 'total');
            //通过社区数量
            $passGroupNum = GroupModel::getPassGroupTotalNum($param, 'total');
        }

        //会话消息
        if(empty($param['wxIds'])) {
            $messageNum = 0;
        } else {
            $messageNum = GroupModel::getMessageNum($param, 'total');
        }
        $return =  [
            'orderTotalNum' => $orderTotalNum,
            'orderMemberTotalNum' => $orderMemberTotalNum,
            'orderTotalAmount' => $orderTotalAmount,
            'groupTotalNum' => $groupTotalNum,
            'groupRemoveTotalNum' => $groupTotalRemoveNum,
            'groupMemberTotalNum' => $groupMemberTotalNum,
            'messageNum' => $messageNum
        ];
        if ($param['user_type'] >= 2) {
            //机器人数量
            $robotNum = count($param['wxIds']);
            //绑定机器人数量
             $bindRobotNum = GroupModel::getBindRobotNum($param);
            //群主数量
            $groupOwnNum = GroupModel::getGroupOwnNum($param, 'total');
            $registerNum = GroupModel::getRegisterNum($param,'total');
            $return['robotNum'] = $robotNum;
            $return['bindRobotNum'] = $bindRobotNum;
            $return['groupOwnNum'] = $groupOwnNum;
            $return['passGroupNum'] = $passGroupNum;
            $return['registerNum'] = $groupOwnNum+$registerNum;
            $return['vipGroupNum'] = GroupModel::getVipGroupNum($param,'total');
        }
        return $return;
    }


    /*
     * 后台首页数据统计
     * 昨日数据
     */
    public static function getIndexGraphStatistics($param)
    {
        $param['channel']=  adminUserModel::getChannelDataByPlatform($param['platform_identity']);

        if (isset($param['beginTime']) || isset($param['endTime'])) {
            if (!isset($param['beginTime'])) {
                $param['beginTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['beginTime'];
            }
        } else {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
            return ['code' => 200001, 'msg' => '时间格式不正确'];
        }
        $format = 'day';
        if (($param['endTime'] - $param['beginTime']) < 3600 * 24) {
            $format = 'hour';
        }
        $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
        $endTime = date('Y-m-d 23:59:59', $param['endTime']);
        //开始结束时间容错
        //if ($param['beginTime'] == $endTime) {
          //  $endTime = date('Y-m-d 23:59:59', strtotime($endTime));
        //}
        $param['endTime'] = $endTime;
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            if ($param['user_type'] >= 2) {
                $param['wxIds'] = GroupModel::getRobotWxIds($param);
                $param['roomWxId'] = GroupModel::getRoomWxIdsByWxId($param);
                $param['roomId'] = GroupModel::getRoomIdsByWxId($param);
            } else {
                $roomWxIds = GroupModel::getRoomIdsByMid($param);
                $roomWxIds = json_decode(json_encode($roomWxIds), true);
                $wxIds = array_column($roomWxIds, 'wxid');
                $roomIds = array_column($roomWxIds, 'id');
                $roomWxId = array_column($roomWxIds, 'room_wxid');
                $param['wxIds'] = array_filter($wxIds);
                $param['roomId'] = array_filter($roomIds);
                $param['roomWxId'] = array_filter($roomWxId);
            }
        } else {
            $groupInfo = GroupModel::fetchGroupInfoByWhere(['id' => $param['roomId']]);
            if (empty($groupInfo)) {
                return ['code' => 200001, 'msg' => '群信息不存在'];
            }
            $param['roomId'] = [$param['roomId']];
            $param['roomWxId'] = [$groupInfo['room_wxid']];
        }
        //统计图数据
        if (!empty($param['roomId'])) {
            //消息数量
            //$messageTotalNum = GroupModel::getMessageTotalNumGroupDate($param, $format);
            //$messageTotalNum = json_decode(json_encode($messageTotalNum), true);
            //素材条数
            //$fodderTotalNum = GroupModel::getFodderTotalNumGroupDate($param, $format);
            //$fodderTotalNum = json_decode(json_encode($fodderTotalNum), true);
            //链接点击量
            //$clickTotalNum = GroupModel::getClickTotalNumGroupDate($param, $format);
            //$clickTotalNum = json_decode(json_encode($clickTotalNum), true);
            //用户订单总数
            $orderTotalNum = GroupModel::getOrderTotalNumGroupDate($param, $format);
            $orderTotalNum = json_decode(json_encode($orderTotalNum), true);

            //新增总人数
            $memberTotalNum = GroupModel::getGroupMemberDate($param, $format);
            $memberTotalNum = json_decode(json_encode($memberTotalNum), true);

            //用户订单金额
            $orderTotalAmount = GroupModel::getOrderTotalAmountGroupDate($param, $format);
            $orderTotalAmount = json_decode(json_encode($orderTotalAmount), true);
        } else {
            $orderTotalNum = [];
            $orderTotalAmount = [];
            $memberTotalNum = [];
        }

        $param['beginTime'] = strtotime($param['beginTime']);
        $param['endTime'] = strtotime($param['endTime']);
        $param['format'] = $format;
        //$returnDataByDate = GroupModel::completionDate($param, $orderTotalNum, $orderTotalAmount, $memberTotalNum);
        $todoReturn = [
            'orderNum' => $orderTotalNum,
            'orderAmount' => $orderTotalAmount,
            'memberCount' => $memberTotalNum,
            //'messageNum' => $messageTotalNum,
            //'fodderNum' => $fodderTotalNum,
            //'clickNum' => $clickTotalNum
        ];
        $returnDataByDate = GroupModel::completionDateArray($param, $todoReturn);
        return $returnDataByDate;
    }


    /*
     * 订单群数量
     */
    public static function orderGroupNum($param)
    {
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
            if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d H:i:s', $param['beginTime']);
            $endTime = date('Y-m-d H:i:s', $param['endTime']);
            //开始结束时间容错
            if ($param['beginTime'] == $endTime) {
                $endTime = date('Y-m-d H:i:s', strtotime("$endTime+1day"));
            }
            $param['endTime'] = $endTime;
        }
        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        $begin = ($page-1) * $pageSize;
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            if ($param['user_type'] >= 2) {
                $param['wxIds'] = GroupModel::getRobotWxIds($param);
                $param['roomWxId'] = GroupModel::getRoomWxIdsByWxId($param);
                $param['roomId'] = GroupModel::getRoomIdsByWxId($param);
            } else {
                $platform = !isset($param['platform']) ? '' : $param['platform'];
                $roomWxIds = GroupModel::getRoomIdsByMid($param, $platform);
                $roomWxIds = json_decode(json_encode($roomWxIds), true);
                $roomIds = array_column($roomWxIds, 'id');
                $wxIds = array_column($roomWxIds, 'wxid');
                $roomWxId = array_column($roomWxIds, 'room_wxid');
                $param['wxIds'] = array_filter($wxIds);
                $param['roomId'] = array_filter($roomIds);
                $param['roomWxId'] = array_filter($roomWxId);
            }
        } else {
            $param['roomId'] = [$param['roomId']];
            $param['roomWxId'] = [$param['roomId']];
        }
        if (!empty($param['roomId'])) {
            //用户订单总数
            $orderTotalNum = GroupModel::orderGroupNum($param, $begin, $pageSize);
            $count = GroupModel::orderGroupNumCount($param);
            $orderTotalNum = json_decode(json_encode($orderTotalNum), true);
            $count = json_decode(json_encode($count), true);
        } else {
            $orderTotalNum = [];
            $count = 0;
        }

        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
        }
        $returnDataByDate['data'] = $orderTotalNum;
        $returnDataByDate['total'] = $count ? $count[0]['cnt'] : 0;
        $returnDataByDate['page'] = $page;
        $returnDataByDate['pageSize'] = $pageSize;
        return $returnDataByDate;
    }

    /*
     * 订单群列表
     */
    public static function orderGroupList($param)
    {

        if (isset($param['beginTime']) && isset($param['endTime'])) {
            //查询开始时间
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . '00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
            if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);

            $endTime = date('Y-m-d 00:00:00', $param['endTime']);
            //开始结束时间容错
            if ($param['beginTime'] == $endTime) {
                $endTime = date('Y-m-d H:i:s', strtotime("$endTime+1day"));
            }
            $param['endTime'] = $endTime;
        }
        $pageSize = $param['pageSize'] ?? 15;
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            if ($param['user_type'] >= 2) {
                $param['wxIds'] = GroupModel::getRobotWxIds($param);
                $param['roomWxId'] = GroupModel::getRoomWxIdsByWxId($param);
                $param['roomId'] = GroupModel::getRoomIdsByWxId($param);
            } else {
                $roomWxIds = GroupModel::getRoomIdsByMid($param);
                $roomWxIds = json_decode(json_encode($roomWxIds), true);
                $wxIds = array_column($roomWxIds, 'wxid');
                $roomIds = array_column($roomWxIds, 'id');
                $roomWxId = array_column($roomWxIds, 'room_wxid');
                $param['wxIds'] = array_filter($wxIds);
                $param['roomId'] = array_filter($roomIds);
                $param['roomWxId'] = array_filter($roomWxId);
            }
        } else {
            $roomId = $param['roomId'];
            $param['roomId'] = explode(',', $roomId);
            $param['roomWxId'] = explode(',', $roomId);
        }
        //用户订单总数
        $orderGroupList = GroupModel::orderGroupList($param, $pageSize);
        $orderGroupList = json_decode(json_encode($orderGroupList), true);
        
        if (is_array($orderGroupList['data']) && count($orderGroupList['data']) > 0) {
            foreach ($orderGroupList['data'] as $key => &$item) {
                $item['price'] = $item['price']/100;
                $item['groupAdminName'] = $item['nick_name'];
//                $item['groupAdminName'] = UserModel::getMemberNameByMid($item['mid']);
                $item['phone'] = $item['mobile'];
//                $item['phone'] = UserModel::getHouTaiUserPhoneByMid($item['mid']);
            }
        }

        return $orderGroupList;
    }

    /*
    * 数据统计
    */
    public static function getGroupMemberList($param)
    {
        if (!isset($param['roomId']) || empty($param['roomId']) || !is_numeric($param['roomId'])) {
            return ['code' => 200002, 'msg' => '群ID格式不正确'];
        }
        $pageSize = $param['pageSize'] ?? 10;
        $memberList = GroupModel::getGroupMemberList($param, $pageSize);
        $memberList = json_decode(json_encode($memberList), true);
        if (is_array($memberList['data']) && count($memberList['data']) > 0) {
            foreach ($memberList['data'] as $key => &$item) {
                $item['address'] = $item['country'] . $item['province'] . $item['city'];
                unset($item['country'], $item['province'], $item['city']);
            }
        }
        return $memberList;
    }


    /*
    * 数据统计
    */
    public static function getYCloudGroupMemberList($param)
    {
        if (!isset($param['roomId']) || empty($param['roomId']) || !is_numeric($param['roomId'])) {
            return ['code' => 200002, 'msg' => '群ID格式不正确'];
        }
        $pageSize = $param['pageSize'] ?? 10;
        $memberList = GroupModel::getGroupMemberList($param, $pageSize);
        $memberList = json_decode(json_encode($memberList), true);
        if (is_array($memberList['data']) && count($memberList['data']) > 0) {
            foreach ($memberList['data'] as $key => &$item) {
                $item['address'] = $item['country'] . $item['province'] . $item['city'];
                unset($item['country'], $item['province'], $item['city']);
            }
        }
        return $memberList;
    }


    /*
    * 数据统计
    */
    public static function updateGroupMember($param, $memberInfo)
    {
        if (!isset($param['signAmount']) || empty($param['signAmount']) || !is_numeric($param['signAmount'])) {
            return ['code' => 200002, 'msg' => '参数不正确'];
        }
        if (!isset($param['updateDesc']) || empty($param['updateDesc'])) {
            return ['code' => 200002, 'msg' => '参数不正确'];
        }
        $room_Id = $param['roomId'] ?? 0;
        $memberWxid = $param['memberWxid'] ?? 0;
        $signAmount = $param['signAmount'] ?? 0;
        $updateDesc = $param['updateDesc'] ?: '';
        $signInsertData = [];
        $signInsertData['room_id'] = $room_Id;
        $signInsertData['member_wxid'] = $memberWxid;
        $signInsertData['integral'] = $signAmount;
        $signInsertData['desc'] = $updateDesc;
        $signInsertData['platform_identity'] = $memberInfo['platform_identity'];
        $result = GroupSignService::updateGroupSignRecord($signInsertData, $signAmount);
        if ($result === false) {
            return ['code' => 200003, 'msg' => '修改失败'];
        }
        return [];
    }



    /*
    * 群签到开关修改
    */
    public static function updateGroupSignSwitch($param)
    {
        if (!isset($param['roomId']) || empty($param['roomId']) || !is_numeric($param['roomId'])) {
            return ['code' => 200002, 'msg' => '群ID格式不正确'];
        }
        $switch = $param['switch'] ?? 1;

        $updateResult = GroupModel::updateGroupSignSwitch($param['roomId'], $switch);
        if ($updateResult === false) {
            return ['code' => 200003, 'msg' => '修改失败'];
        }
        return ['code' => 200, 'msg' => 'success'];
    }


    /*
    * 群签到配置列表
    */
    public static function groupSignConfList($param, $memberInfo)
    {
        $where = [
            'member_id' => $memberInfo['mid'],
            'deleted_at' => 0
        ];
        $groupSignConfList = GroupModel::groupSignConfList($where);
        if ($groupSignConfList === false) {
            return ['code' => 200003, 'msg' => '修改失败'];
        }
        if (is_array($groupSignConfList) && count($groupSignConfList) > 0) {
            $num = 0;
            foreach ($groupSignConfList as $key => &$item) {
                $num++;
                $item->num = $num;
            }
        }
        return $groupSignConfList;
    }

    /*
    * 群签到配置详情
    */
    public static function groupSignConfDetail($param, $memberInfo)
    {
        if (!isset($param['roomId']) || empty($param['roomId']) || !is_numeric($param['roomId'])) {
            return ['code' => 200002, 'msg' => '群ID格式不正确'];
        }
        $where = [
            'room_id' => $param['roomId']
        ];
        return GroupModel::groupSignConfDetail($where);
    }

    /*
    * 群签到配置添加
    */
    public static function insertGroupSignConf($param, $memberInfo)
    {
        $room_Id = $param['roomId'] ?? 0;
        $signIntegral = $param['signIntegral'] ?? 0;
        $keyword = $param['keyword'] ?? '签到';
        $inviteIntegral = $param['inviteIntegral'] ?? 0;
        if (empty($signIntegral)) {
            return ['code' => 200003, 'msg' => '缺少参数'];
        }
        $platform_identity = $memberInfo['platform_identity'];
        $insertData = [
            'member_id' => $memberInfo['mid'],
            'room_id' => $room_Id,
            'keyword' => $keyword,
            'sign_integral' => $signIntegral,
            'invite_integral' => $inviteIntegral,
            'platform_identity' => $platform_identity,
            'deleted_at' => 0
        ];
        if (!empty($room_Id)) {
            $where = [
                'room_id' => $param['roomId']
            ];
        } else {
            $where = [
                'member_id' => $memberInfo['mid'],
                'room_id' => 0,
                'platform_identity' => $platform_identity
            ];
        }
        $SignConfDetail = GroupModel::groupSignConfDetail($where);
        if (!empty($SignConfDetail)) {
            $roomUpdate = [
                'member_id' => $memberInfo['mid'],
                'room_id' => $room_Id,
                'keyword' => $keyword,
                'sign_integral' => $signIntegral,
                'invite_integral' => $inviteIntegral,
                'platform_identity' => $platform_identity,
                'deleted_at' => 0
            ];
            $SignConfUpdate = GroupModel::groupSignConfUpdate($where, $roomUpdate);
            if ($SignConfUpdate === false) {
                return ['code' => 200003, 'msg' => '添加失败'];
            } else {
                return [];
            }
        }
        $groupSignConfList = GroupModel::insertGroupSignConf($insertData);
        if ($groupSignConfList === false) {
            return ['code' => 200003, 'msg' => '添加失败'];
        }
        return [];
    }


    /*
    * 群签到配置删除
    */
    public static function deleteGroupSignConf($param, $memberInfo)
    {
        if (!isset($param['Id']) || empty($param['Id'])) {
            return ['code' => 200003, 'msg' => '缺少必要参数'];
        }
        $where = [
            'id' => $param['Id']
        ];
        $groupSignConfList = GroupModel::deleteGroupSignConf($where);
        if ($groupSignConfList === false) {
            return ['code' => 200003, 'msg' => '修改失败'];
        }
        return $groupSignConfList;
    }


    /*
    * 数据统计
    */
    public static function searchGroupMember($param)
    {
        if (!isset($param['nickname']) || empty($param['nickname'])) {
            return ['code' => 200002, 'msg' => '缺少用户昵称'];
        }
        $pageSize = $param['pageSize'] ?? 10;
        $memberList = GroupModel::searchGroupMemberList($param['nickname'], $pageSize);
        $memberList = json_decode(json_encode($memberList), true);
        if (is_array($memberList['data']) && count($memberList['data']) > 0) {
            foreach ($memberList['data'] as $key => &$item) {
                $item['address'] = $item['country'] . $item['province'] . $item['city'];
                unset($item['country'], $item['province'], $item['city']);
            }
        }
        return $memberList;
    }

    public static function changeGroupRobotStatus($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }

        $where = [];
        $where['id'] = $param['id'];
        $groupInfo = GroupModel::where($where)->first();
        if (empty($groupInfo)) {
            return ['code' => 400, 'msg' => '该群不存在'];
        }
        if ($groupInfo->robot_status == 1) {
            $groupInfo->robot_status = 2;
            $desc = '关闭';
        } else {
            $groupInfo->robot_status = 1;
            $desc = '开启';
        }
        $result = $groupInfo->save();
        if ($result)
        {
            $logMessage = '修改群机器人状态' . $desc;
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    /*
     * 数据统计
     */
    public static function getDataStatistics($param,$member = [])
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;
        $num = ($page-1)*$pageSize;
        if (isset($param['user_type']) && $param['user_type'] >= 2) {
            $param['wxIds']= GroupModel::getRobotWxIds($param);

            if(count($param['wxIds'])==0)
            {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }
        $robotWxAlias = [];
        //数据中台查询不走这块
        if (!isset($param['sourceZT']) || $param['sourceZT'] != 1) {
            if (isset($param['user_type']) && $param['user_type'] >= 2) {
                $robotWxAlias = CommunityGroupManageModel::getRobotWxAlias($param['mid'], $param['sup_id']);
            } else {
                $robotWxAlias = CommunityGroupManageModel::getRobotWxAlias($param['mid'], $param['sup_id'], 1);
            }
        }
        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }
        if (isset($param['tag_id']) && !empty($param['tag_id'])) {
            $param['tag_id'] = [$param['tag_id']];
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }

        $data = GroupModel::getGroupList($param, $page, $pageSize);
        $count = GroupModel::getGroupListCount($param);
        $count = json_decode(json_encode($count), true);
        //以下默认获取一周数据
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d ', $param['beginTime']);
            $param['endTime'] = date('Y-m-d ', $param['endTime']);
        }
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => &$item) {
                $num ++;
                //机器人微信号位置显示机器人名称
                $param['roomId'] = $roomId = $item->id;
                $param['roomWxId'] = $item->room_wxid;
                //$robotInfo = GroupModel::getRobotName($roomId);
                if (empty($param['roomId'])) {
                    //管理员名称
                    $item->adminName = 'admin';
                    //有效订单
                    $item->orderNum = 0;
                    //首次下单人数
                    $item->firstOrderNum = 0;
                    $item->orderNumProportion = '0%';
                    //销售额
                    $item->orderAmount = 0;
                    //浏览次数商品数量
                    $item->browseGoodsNum = 0;
                    //浏览次数人数数量
                    $item->browseUserNum = 0;
                    //当前群成员数量
                    $item->groupMemberNum = 0;
                    //消息数量
                    $item->messageTotalNum = 0;
                    //素材条数
                    $item->fodderTotalNum = 0;
                    //链接点击量
                    $item->clickTotalNum = 0;
                    //消费返佣
                    $item->groupCommission = '0.00';
                    //男性占比
                    $item->groupManProportion = '0%';
                    $item->groupWomenProportion = '0%';
                    $item->deleted_at =  "关闭";
                    $item->robotStatus = '关闭';
                } else {
                    //管理员名称
                    $item->num = $num;
                    if ($item->bind_mid) {
                        $item->adminName = UserModel::getUserNameByMid($item->bind_mid);
                    } else {
                        $item->adminName = UserModel::getUserNameByMid($item->rmid);
                    }
                    //有效订单
                    $item->orderNum = $item->orderNum ?: 0;
                    //首次下单人数 TODO
                    $item->firstOrderNum = $item->firstOrderNum ?: 0;
                    $item->orderNumProportion =  $item->orderNumProportion ? $item->orderNumProportion . '%' : 0 . '%';
                    $item->orderMember = $item->orderMember ?: 0;
                    $item->messageTotalNum = isset($item->messageTotalNum) ? $item->messageTotalNum ?: 0 : 0;
                    //销售额
                    $item->rebate = $item->rebate ? round($item->rebate/100, 2) : 0;
                    $item->orderAmount = $item->orderAmount/100;
                    //浏览次数商品数量
                    $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                    //浏览次数人数数量
                    $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                    //当前群成员数量
                    //$item->groupMemberNum = GroupModel::getGroupUserCount($param);
                    $item->groupMemberNum = $item->groupMemberNum ?: 0;
                    //消息数量
//                    if (isset($param['beginTime']) || isset($param['endTime'])) {
//                        $item->messageTotalNum = GroupModel::getMessageTotalNum($param);
//                    }
                    $item->messageTotalNum = 0;
                    //素材条数
                    $item->fodderTotalNum = GroupModel::getFodderTotalNum($param);
                    //链接点击量
                    $item->clickTotalNum = GroupModel::getClickTotalNum($param);
                    //消费返佣
                    $item->groupCommission = $item->rebate;
                    //男性占比
                    if (empty($item->groupMemberNum)) {
                        $item->groupManProportion = '0%';
                        $item->groupWomenProportion = '0%';
                    } else {
                        $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                        //$womenScale = 1 - $manScale;
                        $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                        $item->groupManProportion = $manScale * 100 . '%';
                        //女性占比
                        $item->groupWomenProportion = $womenScale * 100 . '%';
                    }
                    $item->deleted_at =  $item->deleted_at>0?"关闭":'开启' ;
                }
                unset($param['roomId']);
                //机器人状态
                //$item->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
                $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
                $item->signStatus = $item->sign_switch == 1 ? '开启' : '关闭';
                $tag_name = CommunityGroupManageModel::getTagDataName($item->tag_id);
                $item->tag_name = !empty($tag_name)?$tag_name:'无';
//                $item->band_group = empty($param['sup_id'])?'绑定群主':'';
                if(isset($param['sup_id']) && ($param['sup_id']==0||$member['sup_id']==0)){
                    $item->band_group = '绑定群主';
                }else{
                    $item->band_group =$item->mid>0?'': '绑定群主';
                }
            }
        }
        $return['wx_alias'] = $robotWxAlias;
        $data = json_decode(json_encode($data), true);
        $return['data'] = array_values($data);
        $return['total'] = $count ? $count[0]['cnt'] : 0;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }




    /*
     * 数据统计
     */
    public static function getRoomIdsByCondition($param)
    {
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        $roomIds = GroupModel::getRoomIdsByCondition($param);
        return $roomIds ? array_column($roomIds, 'id') : [];
    }

    /*
     * 数据统计
     */
    public static function getGroupList($param,$member = [])
    {
        //userId 机器人的管理员ID
        //groupAdminId 群主ID  name 群名称
        //userId YCloud系统的用户ID
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;
        $num = ($page-1)*$pageSize;
        $groupList = GroupModel::getYCloudGroupList($param, $page, $pageSize);
        $count = GroupModel::getYCloudGroupListCount($param);
        $groupList = json_decode(json_encode($groupList), true);
        if (is_array($groupList) && count($groupList) > 0) {
            foreach ($groupList as $key => &$item) {
                $num++;
                $item['num'] = $num;
                //机器人微信号位置显示机器人名称
                //男性占比
                if (empty($item['member_count'])) {
                    $item['groupManProportion'] = '0%';
                    $item['groupWomenProportion'] = '0%';
                } else {
                    $manScale = round(GroupModel::getYCloudGroupUserCount($param, 'man') / $item['member_count'], 2);
                    //$womenScale = 1 - $manScale;
                    $womenScale = round(GroupModel::getYCloudGroupUserCount($param, 'woman') / $item['member_count'], 2);
                    $item['groupManProportion'] = $manScale * 100 . '%';
                    //女性占比
                    $item['groupWomenProportion'] = $womenScale * 100 . '%';
                }
                //机器人状态
                $item['robotStatus'] = $item['robot_status'] == 1 ? '开启' : '关闭';
                $item['tuiGuangOrderNum'] = 1;
                $item['tuiGuangGmv'] = 9.9;
                $item['jieSuanOrderNum'] = 1;
                $item['jieSuanGmv'] = 9.9;
            }
        }
        $return['data'] = array_values($groupList);
        $return['total'] = $count;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }



    /**
     * 获取我的机器人数据
     */
    public static function updateGroupStatus($param)
    {
        //YCloud平台标识
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        //updateType  1:发品 2:停止发品 群删除
        if (!isset($param['updateType']) || !in_array($param['updateType'], [1,2])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数操作类型'
            ];
        }
        $robotInfo = RobotModel::fetchGroupInfoById($param['roomId']);
        if (empty($robotInfo)) {
            return [
                'code' => 200201,
                'msg' => '群信息不存在'
            ];
        }
        //机器人的 status 0 正常 1 删除
        if ($param['updateType'] == 1) {
            $update = [
                'is_delete' => 0
            ];
        } else {
            $update = [
                'is_delete' => 1
            ];
        }
        $updateResult = RobotModel::updateGroupInfoById($param['roomId'], $update);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        return $return = [];
    }



    /*
     * 数据统计
     */
    public static function getMiniAppRobotList($param,$member = [])
    {
        //userId 机器人的管理员ID
        //groupAdminId 群主ID  name 群名称
        //userId YCloud系统的用户ID
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        if (!isset($param['userId']) || empty($param['userId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:20;
        $groupList = GroupModel::getYCloudGroupList($param, $page, $pageSize);
        $count = GroupModel::getYCloudGroupListCount($param);
        $groupList = json_decode(json_encode($groupList), true);
        if (is_array($groupList) && count($groupList) > 0) {
            foreach ($groupList as $key => &$item) {
                //机器人状态
                unset($item['mid'],$item['robot_status'],$item['member_count'],$item['robotId']);
            }
        }
        $return['data'] = array_values($groupList);
        $return['total'] = $count;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }


    /*
     * 数据统计
     */
    public static function deleteGroupStatus($param,$member = [])
    {
        //userId 机器人的管理员ID
        //groupAdminId 群主ID  name 群名称
        //userId YCloud系统的用户ID
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return [
                'code' => 400,
                'msg' => '缺少参数'
            ];
        }
        $result = GroupModel::deleteGroupStatus($param['roomId']);
        if ($result === false) {
            return [
                'code' => 400,
                'msg' => '修改失败'
            ];
        }
        return [];
    }


    /**
     * 获取我的机器人数据
     */
    public static function bindYCloudGroupMid($param)
    {
        if (!isset($param['roomId']) || empty($param['roomId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        if (!isset($param['userId']) || empty($param['userId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        if (!isset($param['phone']) || empty($param['phone'])) {
            return [
                'code' => 200201,
                'msg' => '缺少群主手机号'
            ];
        }
        if (!isset($param['codeNumber']) || empty($param['codeNumber'])) {
            return [
                'code' => 200201,
                'msg' => '缺少群主邀请码'
            ];
        }
        $update = [];
        $update['mid'] = $param['userId'];
        $update['mobile'] = $param['phone'];
        $update['code_number'] = $param['codeNumber'];
        $updateResult = GroupModel::bindYCloudGroupMid($param['roomId'], $update);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        return $return = [];
    }

    /**
     * 绑定机器人
     */
    public static function bindYCloudRobot($param)
    {
        if (!isset($param['robotId']) || empty($param['robotId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        if (!isset($param['userId']) || empty($param['userId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        $update = [];
        $update['exclusive_mid'] = $param['userId'];
        $updateResult = GroupModel::bindYCloudRobot($param['robotId'], $update);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        return $return = [];
    }
    /**
     * 绑定机器人pass
     */
    public static function applyYCRobotPass($param)
    {

        if (!isset($param['applyId']) || empty($param['applyId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少申请ID'
            ];
        }
        // 2通过 3拒绝  examineDesc不通过原因
        if (!isset($param['robotId']) || empty($param['robotId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        if (!isset($param['userId']) || empty($param['userId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少参数'
            ];
        }
        $update = [];
        $update['exclusive_mid'] = $param['userId'];
        $updateResult = GroupModel::bindYCloudRobot($param['robotId'], $update);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        $examineUpdate = [];
        $examineUpdate['examine_status'] = 2;
        $updateResult = CommunityInvitationGroupModel::updateDataById($param['applyId'], $examineUpdate);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        return $return = [];
    }

    /**
     * 绑定机器人refuse
     */
    public static function applyYCRobotRefuse($param)
    {

        if (!isset($param['applyId']) || empty($param['applyId'])) {
            return [
                'code' => 200201,
                'msg' => '缺少申请ID'
            ];
        }
        // 2通过 3拒绝  examineDesc不通过原因
        if (!isset($param['examineDesc']) || empty($param['examineDesc'])) {
            return [
                'code' => 200201,
                'msg' => '缺少拒绝原因'
            ];
        }
        $examineUpdate = [];
        $examineUpdate['examine_status'] = 3;
        if (isset($param['examineDesc']) && !empty($param['examineDesc'])) {
            $examineUpdate['examine_desc'] = $param['examineDesc'];
        }
        $updateResult = CommunityInvitationGroupModel::updateDataById($param['applyId'], $examineUpdate);
        if ($updateResult === false) {
            return [
                'code' => 200201,
                'msg' => '修改失败'
            ];
        }
        return $return = [];
    }

    /**
     * 专属机器人数据统计
     * @param $param
     * @param $member
     * @return mixed
     */
    public static function getExclusiveDataStatistics($param,$member)
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;
        $num = ($page-1)*$pageSize;
        if ($param['user_type'] >= 2) {
            $param['wxIds']= GroupModel::getRobotWxIds($param,3);
            if(count($param['wxIds'])==0)
            {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }

        if ($param['user_type'] >= 2 ) {
            $robotWxAlias = CommunityGroupManageModel::getRobotExclusiveWxAlias($param['mid'], $param['sup_id']);

        } else {
            $robotWxAlias = CommunityGroupManageModel::getRobotExclusiveWxAlias($param['mid'],$param['sup_id'], 1);
        }

        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }
        if (isset($param['tag_id']) && !empty($param['tag_id'])) {
            $param['tag_id'] = [$param['tag_id']];
        }
        if (isset($param['phone']) && !empty($param['phone'])) {
            $param['searchMid'] = GroupModel::getSearchMid($param['phone']);
            if (empty($param['searchMid'])) {
                $return['data'] = [];
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
        }
        //专属机器人查询
        $param['exclusive'] = 1;
        $data = GroupModel::getGroupList($param, $page, $pageSize);
        $count = GroupModel::getGroupListCount($param);
        $count = json_decode(json_encode($count), true);
        //以下默认获取一周数据
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d ', $param['beginTime']);
            $param['endTime'] = date('Y-m-d ', $param['endTime']);
        }
        $wxidData = [];
        foreach($robotWxAlias as $hile){
            $wxidData[$hile->wxid]=['all_order_sum'=>$hile->all_order_sum,'created_at'=>$hile->created_at];
        }
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => &$item) {
                $num ++;

                $falg =  GroupModel::getSurplusDays($item->id);
                $item->surplusDays ='正常';
                if($falg>0&&$wxidData[$item->wxid]['all_order_sum']<config('community.exclusive_order_sum')){
                    $item->surplusDays = 3- sprintf("%01.2f",(strtotime($item->created_at)-strtotime( $wxidData[$hile->wxid]['created_at']))/86400);
                    $item->surplusDays =  $item->surplusDays>=0?'剩余' .$item->surplusDays.'天':'超过了'.abs($item->surplusDays).'天';
                }
                //机器人微信号位置显示机器人名称
                $param['roomId'] = $roomId = $item->id;
                $param['roomWxId'] = $item->room_wxid;
                //$robotInfo = GroupModel::getRobotName($roomId);
                if (empty($param['roomId'])) {
                    //管理员名称
                    $item->adminName = 'admin';
                    //有效订单
                    $item->orderNum = 0;
                    //销售额
                    $item->orderAmount = 0;
                    //浏览次数商品数量
                    $item->browseGoodsNum = 0;
                    //浏览次数人数数量
                    $item->browseUserNum = 0;
                    //当前群成员数量
                    $item->groupMemberNum = 0;
                    //消息数量
                    $item->messageTotalNum = 0;
                    //素材条数
                    $item->fodderTotalNum = 0;
                    //链接点击量
                    $item->clickTotalNum = 0;
                    //消费返佣
                    $item->groupCommission = '0.00';
                    //男性占比
                    $item->groupManProportion = '0%';
                    $item->groupWomenProportion = '0%';
                    $item->deleted_at =  "关闭";
                    $item->robotStatus = '关闭';
                } else {
                    //管理员名称
                    $item->num = $num;
                    if ($item->bind_mid) {
                        $item->adminName = UserModel::getUserNameByMid($item->bind_mid);
                    } else {
                        $item->adminName = UserModel::getUserNameByMid($item->rmid);
                    }
                    //有效订单
                    $item->orderNum = $item->orderNum ?: 0;
                    //销售额
                    $item->rebate = $item->rebate ?: 0;
                    $item->orderAmount = $item->orderAmount/100;
                    //浏览次数商品数量
                    $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                    //浏览次数人数数量
                    $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                    //当前群成员数量
                    $item->groupMemberNum = GroupModel::getGroupUserCount($param);
                    //$item->groupMemberNum = $item->groupMemberNum ?: 0;
                    //消息数量
                    //$item->messageTotalNum = GroupModel::getMessageTotalNum($param);
                    $item->messageTotalNum = 0;
                    //素材条数
                    $item->fodderTotalNum = GroupModel::getFodderTotalNum($param);
                    //链接点击量
                    $item->clickTotalNum = GroupModel::getClickTotalNum($param);
                    //消费返佣
                    $item->groupCommission = $item->rebate;
                    //男性占比
                    if (empty($item->groupMemberNum)) {
                        $item->groupManProportion = '0%';
                        $item->groupWomenProportion = '0%';
                    } else {
                        $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                        //$womenScale = 1 - $manScale;
                        $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                        $item->groupManProportion = $manScale * 100 . '%';
                        //女性占比
                        $item->groupWomenProportion = $womenScale * 100 . '%';
                    }
                    $item->deleted_at =  $item->deleted_at>0?"关闭":'开启' ;
                }
                unset($param['roomId']);
                //机器人状态
                $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
                $tag_name = CommunityGroupManageModel::getTagDataName($item->tag_id);
                $item->tag_name = !empty($tag_name)?$tag_name:'无';
                if($param['sup_id']==0||$member['sup_id']==0){
                    $item->band_group = '绑定群主';
                }else{
                    $item->band_group =$item->mid>0?'': '绑定群主';
                }

            }
        }
        $return['wx_alias'] = $robotWxAlias;
        $data = json_decode(json_encode($data), true);
        $return['data'] = array_values($data);
        $return['total'] = $count ? $count[0]['cnt'] : 0;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }
    /*
    * 数据统计
    */
    public static function groupAdminStatistics($param)
    {
        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        $begin = ($page-1) * $pageSize;
        if (isset($param['user_type']) && $param['user_type'] >= 2) {
            $param['wxIds']= GroupModel::getRobotWxIds($param,1);
        }
        //数据中台查询不走这块
//        if (!isset($param['sourceZT']) || $param['sourceZT'] != 1) {
//            if ($param['user_type'] >= 2) {
//                $param['wxIds'] = GroupModel::getRobotWxIds($param, 1);
//                $param['roomWxId'] = GroupModel::getRoomWxIdsByWxId($param);
//                $param['roomId'] = GroupModel::getRoomIdsByWxId($param);
//                $param['roomId'] = json_decode(json_encode($param['roomId']), true);
//            } else {
//                $roomWxIds = GroupModel::getRoomIdsByMid($param);
//                $roomWxIds = json_decode(json_encode($roomWxIds), true);
//                $wxIds = array_column($roomWxIds, 'wxid');
//                $roomIds = array_column($roomWxIds, 'id');
//                $roomWxId = array_column($roomWxIds, 'room_wxid');
//                $param['wxIds'] = array_filter($wxIds);
//                $param['roomId'] = array_filter($roomIds);
//                $param['roomWxId'] = array_filter($roomWxId);
//            }
//        }
        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                $param['roomId'] = [];
            }
        }
        $robotWxAlias = [];
        //数据中台查询不走这块
        if (!isset($param['sourceZT']) || $param['sourceZT'] != 1) {
            //获取所有机器人号
            if (isset($param['sup_id']) && $param['sup_id'] == 0) {
                $userIds = UserModel::getDownUserIds($param['mid']);
                $userIds[] = $param['mid'];
                $param['midS'] = $userIds;
            } else {
                $param['midS'] = UserModel::getPeerUserIds($param['sup_id']);
            }
            $param['midS'] = json_decode(json_encode($param['midS']), true);
            $midS = "'" . implode("','", $param['midS']) . "'";
            if ($param['user_type'] >= 2) {
                $robotWxAlias = CommunityGroupManageModel::getRobotWxAlias($midS, $param['sup_id']);
            } else {
                $robotWxAlias = CommunityGroupManageModel::getRobotWxAlias($midS, $param['sup_id'], 1);
            }
        }
            $data = GroupModel::getAdminGroupList($param, $begin, $pageSize);
            $count = GroupModel::getAdminGroupListCount($param);
            $count = json_decode(json_encode($count), true);

            //以下默认获取一周数据
            if (isset($param['beginTime']) && isset($param['endTime'])) {
                if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                    return ['code' => 200001, 'msg' => '时间格式不正确'];
                }
                $param['beginTime'] = date('Y-m-d ', $param['beginTime']);
                $param['endTime'] = date('Y-m-d ', $param['endTime']);
            }
            if (is_array($data) && count($data) > 0) {
                foreach ($data as $key => &$item) {
                    $roomId = explode(',', $item->ids);
                    $item->id = ($page-1)*$pageSize+$key+1;
                    $param['roomId'] = $roomId;
                    if (empty($param['roomId'])) {
                        $item->groupAdminName = '群主姓名';
                        $item->growthValue = 0;
                        //管理员名称
                        $item->adminName = 'admin';
                        //群名称
                        $item->name = '暂无';
                        //群数量
                        $item->groupCount = 0;
                        //群主社群数量限制
                        $item->limitGroupCount = 0;
                        //有效订单
                        $item->orderNum = 0;
                        //销售额
                        $item->orderAmount = 0;
                        //浏览次数商品数量
                        $item->browseGoodsNum = 0;
                        //浏览次数人数数量
                        $item->browseUserNum = 0;
                        //当前群成员数量
                        $item->groupMemberNum = 0;
                        //预估佣金
                        $item->estCommission = $item->estCommission>0? sprintf("%.2f",$item->estCommission/100):0 ;
                        //消费返佣
                        $item->groupCommission = $item->groupCommission>0?sprintf("%.2f",$item->groupCommission/$item->groupCount):0 ;
//                        $item->groupCommission = GroupModel::getAdminGroupTrueAmount($item->ids,$item->groupMid);
                        //男性占比
                        $item->groupManProportion = '0%';
                        $item->groupWomenProportion = '0%';
                    } else {
                        //管理员名称--TODO 后台显示手机号
                        $memberInfo = UserModel::getMemberInfoByMid($item->groupMid);
                        $item->groupAdminName = isset($memberInfo['truename']) && !empty($memberInfo['truename'])
                            ? $memberInfo['truename'] : isset($memberInfo['nickname']) && !empty($memberInfo['nickname'])
                            ? $memberInfo['nickname'] : '暂无昵称';
//                        $item->adminName = UserModel::getUserPhoneByMid($item->mid);
                        //群名称
                        $item->name = self::getAdminstrByType($item->wxid_type,$item->rmid,$item->bind_mid);
                        //有效订单
                        //$item->orderNum = GroupModel::getOrderNum($param);
                        $item->orderNum = $item->orderNum ?$item->orderNum: 0;
                        $item->limitGroupCount = GroupManageModel::getlimitgroupCount($item->rmid?$item->rmid:0) ?: 0;
                        //销售额
                        //$item->orderAmount = GroupModel::getOrderTotalAmountByRoomId($param) / 100;
                        $item->orderAmount = $item->orderAmount?$item->orderAmount/100: 0;
                        //浏览次数商品数量
                        $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                        //浏览次数人数数量
                        $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                        //当前群成员数量
                        $item->groupMemberNum = GroupModel::getGroupUserCount($param);
                        //预估佣金

                        if($item->channel==4){

                            $item->estCommission = $item->estCommission>0? sprintf("%.2f",$item->estCommission/100*4):0 ;

                        }else{

                            $item->estCommission = $item->estCommission>0? sprintf("%.2f",$item->estCommission/100):0 ;
                        }

                        $item->growthValue = ceil($item->estCommission * 2);
                        $item->orderNum = $item->orderNum ?: 0;
                        //消费返佣
                        if (empty($item->orderNum)) {
                            $item->groupCommission = 0;
                        } else {
                            $item->groupCommission = $item->groupCommission > 0 ? sprintf("%.2f", $item->groupCommission / $item->orderNum) : 0;
                        }
//                        $item->groupCommission = GroupModel::getAdminGroupTrueAmount($item->ids,$item->groupMid);
                        //男性占比
                        if (empty($item->groupMemberNum)) {
                            $item->groupManProportion = '0%';
                            $item->groupWomenProportion = '0%';
                        } else {
                            $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                            //$womenScale = 1 - $manScale;
                            $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                            $item->groupManProportion = $manScale * 100 . '%';
                            //女性占比
                            $item->groupWomenProportion = $womenScale * 100 . '%';
                        }
                        $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';
                    }
                    unset($param['roomId']);
                    unset($item->ids);
                    //机器人状态
                    //$item->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
                    $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';
                }
            }
            $data['wx_alias'] = $robotWxAlias;


            $data = json_decode(json_encode($data), true);
            $return['data'] = array_values($data);
            $return['total'] = $count ? $count[0]['cnt'] : 0;
            $return['page'] = $page;
            $return['pageSize'] = $pageSize;
        return $return;
    }

    /*
    * 数据统计
    */
    public static function groupYCAdminStatistics($param)
    {
        //userId YCloud系统的用户ID
        //YCloud平台标识
        $param['platform_identity'] = '2cb4a91bce63da9862f72559d4c463f5';
        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        $begin = ($page - 1) * $pageSize;
        $groupAdminList = GroupModel::getYCAdminGroupList($param, $begin, $pageSize);
        $count = GroupModel::getYCAdminGroupListCount($param);

//        if (is_array($groupAdminList) && count($groupAdminList) > 0) {
//            foreach ($groupAdminList as $key => &$item) {
//            }
//        }
        $groupAdminList = json_decode(json_encode($groupAdminList), true);
        $return['data'] = array_values($groupAdminList);
        $return['total'] = $count;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }
    /**专属机器人群主统计
     * @param $param
     * @return mixed
     */
    public static function groupExclusiveAdminStatistics($param)
    {
        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 15;
        $begin = ($page-1) * $pageSize;
        if ($param['user_type'] >= 2) {
            $param['wxIds']= GroupModel::getRobotWxIds($param,3);
        }
        if ($param['user_type'] >= 2) {
            $param['wxIds'] = GroupModel::getRobotWxIds($param,3);
            $param['roomWxId'] = GroupModel::getRoomWxIdsByWxId($param);
            $param['roomId'] = GroupModel::getRoomIdsByWxId($param);
            $param['roomId'] = json_decode(json_encode($param['roomId']), true);
        } else {
            $roomWxIds = GroupModel::getRoomIdsByMid($param);
            $roomWxIds = json_decode(json_encode($roomWxIds), true);
            $wxIds = array_column($roomWxIds, 'wxid');
            $roomIds = array_column($roomWxIds, 'id');
            $roomWxId = array_column($roomWxIds, 'room_wxid');
            $param['wxIds'] = array_filter($wxIds);
            $param['roomId'] = array_filter($roomIds);
            $param['roomWxId'] = array_filter($roomWxId);
        }
        if (isset($param['tag_name']) && !empty($param['tag_name'])) {
            $param['tag_id'] = GroupModel::getTagIds($param['tag_name']);
            if (empty($param['tag_id'])) {
                $param['roomId'] = [];
            }
        }
        if (empty($param['roomId']) || count($param['roomId']) < 1) {
            $return['data'] = [];
            $return['total'] = 0;
            $return['page'] = $page;
            $return['pageSize'] = $pageSize;
        } else {
            //获取所有机器人号
            if (!isset($param['mid']) || empty($param['mid'])) {
                if ($param['sup_id'] == 0) {
                    $userIds = UserModel::getDownUserIds($param['mid']);
                    $userIds[] = $param['mid'];
                    $param['midS'] = $userIds;
                } else {
                    $param['midS'] = UserModel::getPeerUserIds($param['sup_id']);
                }
            } else {
                //TODO  不是所属人员无权查看
                $param['midS'] = [$param['mid']];
            }
            $param['midS'] = json_decode(json_encode($param['midS']), true);
            $midS = "'" . implode("','", $param['midS']) . "'";
            if ($param['user_type'] >= 2) {
                $robotWxAlias = CommunityGroupManageModel::getRobotExclusiveWxAlias($midS, $param['sup_id']);
            } else {
                $robotWxAlias = CommunityGroupManageModel::getRobotExclusiveWxAlias($midS, $param['sup_id'], 1);
            }

            if(isset($param['source'])&&$param['source']>1)
            {
                $return['wx_alias'] = $robotWxAlias;
                $return['total'] = 0;
                $return['page'] = $page;
                $return['pageSize'] = $pageSize;
                return $return;
            }
            $param['wxid_type'] = 3;
            $data = GroupModel::getAdminGroupList($param, $begin, $pageSize);
            $count = GroupModel::getAdminGroupListCount($param);
            $count = json_decode(json_encode($count), true);


            //以下默认获取一周数据
            if (isset($param['beginTime']) && isset($param['endTime'])) {
                if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                    return ['code' => 200001, 'msg' => '时间格式不正确'];
                }
                $param['beginTime'] = date('Y-m-d ', $param['beginTime']);
                $param['endTime'] = date('Y-m-d ', $param['endTime']);
            }
            if (is_array($data) && count($data) > 0) {
                foreach ($data as $key => &$item) {


                    $roomId = explode(',', $item->ids);
                    $item->id = ($page-1)*$pageSize+$key+1;
                    $param['roomId'] = $roomId;
                    if (empty($param['roomId'])) {
                        //管理员名称
                        $item->adminName = 'admin';
                        //群名称
                        $item->name = '暂无';
                        //群数量
                        $item->groupCount = 0;
                        //群主社群数量限制
                        $item->limitGroupCount = 0;
                        //有效订单
                        $item->orderNum = 0;
                        //销售额
                        $item->orderAmount = 0;
                        //浏览次数商品数量
                        $item->browseGoodsNum = 0;
                        //浏览次数人数数量
                        $item->browseUserNum = 0;
                        //当前群成员数量
                        $item->groupMemberNum = 0;

                        //消费返佣
                        $item->groupCommission = '0.00';
                        //男性占比
                        $item->groupManProportion = '0%';
                        $item->groupWomenProportion = '0%';
                    } else {
                        //管理员名称--TODO 后台显示手机号
                        //$item->adminName = UserModel::getUserNameByMid($item->mid);
//                        $item->adminName = UserModel::getUserPhoneByMid($item->mid);
                        //群名称
                        $item->name = self::getAdminstrByType($item->wxid_type,$item->rmid,$item->bind_mid);
                        //有效订单
                        //$item->orderNum = GroupModel::getOrderNum($param);
                        $item->orderNum = $item->orderNum ?: 0;
                        $item->limitGroupCount = GroupManageModel::getlimitgroupCount($item->rmid?$item->rmid:0) ?: 0;
                        //销售额
                        //$item->orderAmount = GroupModel::getOrderTotalAmountByRoomId($param) / 100;
                        $item->orderAmount = ($item->orderAmount / 100) ?: 0;
                        //浏览次数商品数量
                        $item->browseGoodsNum = GroupModel::getBrowseGoodsNum($param);
                        //浏览次数人数数量
                        $item->browseUserNum = GroupModel::getBrowseUserNum($param);
                        //当前群成员数量
                        $item->groupMemberNum = GroupModel::getGroupUserCount($param);

                        //消费返佣
                        $item->groupCommission = '0.00';
                        //男性占比
                        if (empty($item->groupMemberNum)) {
                            $item->groupManProportion = '0%';
                            $item->groupWomenProportion = '0%';
                        } else {
                            $manScale = round(GroupModel::getGroupUserCount($param, 'man') / $item->groupMemberNum, 2);
                            //$womenScale = 1 - $manScale;
                            $womenScale = round(GroupModel::getGroupUserCount($param, 'woman') / $item->groupMemberNum, 2);
                            $item->groupManProportion = $manScale * 100 . '%';
                            //女性占比
                            $item->groupWomenProportion = $womenScale * 100 . '%';
                        }
                        $item->deleted_at = $item->deleted_at > 0 ? "关闭" : '开启';
                    }
                    unset($param['roomId']);
                    //机器人状态
                    //$item->robotStatus = $robotInfo['wxid_type']==1 ? '已激活' : '未激活';
                    $item->robotStatus = $item->robot_status == 1 ? '开启' : '关闭';

                }
            }
            $data['wx_alias'] = $robotWxAlias;

            $data = json_decode(json_encode($data), true);
            $return['data'] = array_values($data);
            $return['total'] = $count ? $count[0]['cnt'] : 0;
            $return['page'] = $page;
            $return['pageSize'] = $pageSize;
        }
        return $return;
    }

    public static function getAdminstrByType($type=1,$mid=0,$bind_mid=0)
    {

        $str = "";

        switch ($type) {
            case 1:
                $mid = $bind_mid>0?$bind_mid:$mid;
                $nickname = GroupManageModel::getNickNameByMid($mid);
                $str .= $nickname;
                break;
            case 2:
                $nickname = GroupManageModel::getNickNameByMid($mid);
                $str .= $nickname;
                break;
            default:
                $nickname = GroupManageModel::getNickNameByMid($bind_mid);
                $str .= $nickname;
                break;
        }

        return $str;
    }

    /*
     * 数据统计根据机器人
     */
    public static function getDataStatisticsByRobot($param)
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;

        $param['wxIds'] = GroupModel::getRobotWxIds($param);

        //获取群列表
        $data = GroupModel::getGroupList($param, $page, $pageSize);

//        $data = GroupModel::getRobotInfo($pageSize);

        $data = json_encode($data);
        $data = json_decode($data, true);

        if (is_array($data['data']) && count($data['data']) > 0) {
            foreach ($data['data'] as $key => &$item) {
                $item['name'] = $item['wxid'];
                $idsData = GroupModel::getGroupIds($item['wxid']);
                $roomIds = array_map(function ($value) {
                    return $value->id;
                }, $idsData);
                $item['roomId'] = $roomIds;
            }
        }
        //以下默认获取一周数据
        $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d',strtotime('-1 week')).'00:00:00');
        $param['endTime'] = $param['endTime'] ?? strtotime('today');
        //$param['beginTime'] = $param['beginTime'] ?? '';
        //$param['endTime'] = $param['endTime'] ?? '';
        if ($param['endTime'] && $param['endTime']) {
            if (!self::is_timestamp($param['beginTime']) || !self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
        }
        if (is_object($data['data'])) {
            $data['data'] = json_encode($data['data']);
            $data['data'] = json_decode($data['data'], true);
        }
        if (is_array($data['data']) && count($data['data']) > 0) {
            foreach ($data['data'] as $key => &$item) {
                $param['roomId'] = $roomId = $item['roomId'];
                $param['wxId'] = $item['wxid'];
                $item['name'] = $item['wxid'];
                if (empty($param['roomId'])) {
                    //有效订单
                    $item['orderNum'] = 0;
                    //销售额
                    $item['orderAmount'] = 0.00;
                    //浏览次数商品数量
                    $item['browseGoodsNum'] = 0;
                    //浏览次数人数数量
                    $item['browseUserNum'] = 0;
                    //当前群成员数量
                    $item['groupMemberNum'] = 0;
                    //机器人编码
                    $item['robotCode'] = $item['wxid'] ?? '机器人编码';
                    //消费返佣
                    $item['groupCommission'] = '2200.00';
                    //男性占比
                    $item['groupManProportion'] = '0%';
                    $item['groupWomenProportion'] = '0%';
                } else {
                    //有效订单
                    $item['rebate'] = $item['rebate'] ?: 0;
                    $item['orderNum'] = GroupModel::getOrderNum($param);
                    //销售额
                    $item['orderAmount'] = GroupModel::getOrderTotalAmountByRoomId($param);
                    //浏览次数商品数量
                    $item['browseGoodsNum'] = GroupModel::getBrowseGoodsNum($param);
                    //浏览次数人数数量
                    $item['browseUserNum'] = GroupModel::getBrowseUserNum($param);
                    //当前群成员数量
                    $item['groupMemberNum'] = GroupModel::getGroupUserCountByRobot($param);
                    //机器人编码
                    $item['robotCode'] = $item['wxid'] ?? '机器人编码';
                    //消费返佣
                    $item['groupCommission'] = round($item['rebate']/100, 2);
                    //男性占比
                    if (empty($item['groupMemberNum'])) {
                        $item['groupManProportion'] = '0%';
                        $item['groupWomenProportion'] = '0%';
                    } else {
                        $manScale = round(GroupModel::getGroupUserCountByRobot($param, 'man') / $item['groupMemberNum'], 2);
                        $womenScale = 1 - $manScale;
                        $item['groupManProportion'] = $manScale * 100 . '%';
                        //女性占比
                        $item['groupWomenProportion'] = $womenScale * 100 . '%';
                    }
                }
                //机器人状态
                $item['robotStatus'] = $item['wxid_type']==1 ? '已激活' : '未激活';
            }
            $data['data'] = array_values($data['data']);
        }
        return $data;
    }

    /*
     * 后台首页数据统计
     */
    public static function getMemberGroupList($param)
    {
        //查询开始时间
        $beginTime = $param['beginTime'] ?? '2020-04-09 11:44:33';
        $endTime = $param['endTime'] ?? '';
        //用户群总数
        $groupTotalNum = '';
        //用户订单总数
        $orderTotalNum = '';
        //日期
        $dateInfo = '';
        //群维度
        $groupDimensionality = '';
        //链接点击数
        $linkClickNum = '';
        if ($beginTime) {
            $where[] = [
                'created_at' > $beginTime
            ];
        }
        $where = [
            'mid' => $param['memberId']
        ];
        return GroupModel::getGroupInfo($param);
    }


    /**
     * 查看当前的绑定的数据
     * @param $param
     */
    public  static function  getBingdData($param){
        $resData =  GroupModel::getBindGroupMidData($param);
        $resData->name =empty($resData->name)?'':$resData->name;
        $resData->mobile =empty($resData->mobile)?'':$resData->mobile;
        $resData->truename =empty($resData->truename)?'':$resData->truename;
        $resData->nickname =empty($resData->nickname)?'':$resData->nickname;
        $resData->code_number =empty($resData->code_number)?'':$resData->code_number;
        $resData->mid =empty($resData->mid)?0:$resData->mid;
        return $resData;
    }

    /**
     * 数据展示
     * @param $param
     */
    public static  function getDarenBingdData($param){
       $resData =  GroupModel::getBindGroupMidData($param);
       if($resData->mid>0){
           $res=self::getDarenMidData($resData->mobile);
           $resData->name =!isset($res['nickname'])?'':$res['nickname'];
           $resData->mobile =!isset($res['mobile'])?'':$res['mobile'];
           $resData->truename =!isset($res['nickname'])?'':$res['nickname'];
           $resData->nickname =!isset($res['nickname'])?'':$res['nickname'];
           $resData->code_number =!isset($res['codeNumber'])?'':$res['codeNumber'];
           $resData->mid =empty($resData->mid)?0:$resData->mid;
       }else{
           $resData->name ='';
           $resData->mobile ='';
           $resData->truename ='';
           $resData->nickname ='';
           $resData->code_number ='';
           $resData->mid ='';
       }
        return $resData;
    }

    /**
     * @param $param
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     * @throws \Exception
     */
    public static function  getZhidingBingdData($param){

        $resData =  GroupModel::getGroupData($param['id']);
        if($resData->mid>0){
            $res =UseIndexService::getZhidingMidData(['memberSubId'=>$resData->mid]);
//            var_dump($res);
            if(!empty($res)){
                $returnData['name'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
                $returnData['truename'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['nickname'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['code_number'] =!isset($res['memberSubDto']['inviteCode'])?'':$res['memberSubDto']['inviteCode'];
                $returnData['mid'] =isset($res['memberSubDto']['inviteCode'])?$res['memberSubDto']['inviteCode']:'';
            }

        }else{
            $returnData['name'] ='';
            $returnData['mobile'] ='';
            $returnData['truename'] ='';
            $returnData['nickname'] ='';
            $returnData['code_number'] ='';
            $returnData['mid'] ='';
        }

        return $returnData;
    }

    /**
     * 迈途数据查看
     * @param $param
     */
    public static function  getMaituBingdData($param){
        $resData =  GroupModel::getGroupData($param['id']);
        if($resData->mid>0){
            $res =UseIndexService::getMaituMidData(['number'=>$resData->mid]);
            if(!empty($res)){
                $returnData['name'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
                $returnData['truename'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['nickname'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['code_number'] =!isset($res['inviteCode'])?'':$res['inviteCode'];
                $returnData['mid'] =!isset($res['memberId'])?'':$res['memberId'];
            }

        }else{
            $returnData['name'] ='';
            $returnData['mobile'] ='';
            $returnData['truename'] ='';
            $returnData['nickname'] ='';
            $returnData['code_number'] ='';
            $returnData['mid'] ='';
        }

        return $returnData;
    }

    /**
     * @param $param
     * @return mixed
     * @throws \Exception
     */
    public static function getUniversalBingdData($platform_identity,$param){

        $resData =  GroupModel::getGroupData($param['id']);
        if($resData->mid>0){
            $res =UseIndexService::getMaituMidData($platform_identity,['memberId'=>$resData->mid]);
            if(!empty($res)){
                $returnData['name'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
                $returnData['truename'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['nickname'] =!isset($res['nickName'])?'':$res['nickName'];
                $returnData['code_number'] =!isset($res['inviteCode'])?'':$res['inviteCode'];
                $returnData['mid'] =!isset($res['memberId'])?'':$res['memberId'];
            }
        }else{
            $returnData['name'] ='';
            $returnData['mobile'] ='';
            $returnData['truename'] ='';
            $returnData['nickname'] ='';
            $returnData['code_number'] ='';
            $returnData['mid'] ='';
        }

        return $returnData;
    }



    /**
     * 绑定群主
     * @param $mid
     * @return mixed
     * @throws \Exception
     */
     public static function  getDarenMidData($mobile){

         $result = curlPostCommon('https://gateway.daren.tech/open/v1/open/v1/home/getUserInfo', ['mobile'=>$mobile]);
         $returnData = json_decode($result,true);
         if($returnData['code']==200){
            return $returnData['data'];
         }else{
             return [];
         }

     }


    /**
     * 根据手机号获取信息
     * @param $param
     */
    public  static function getMemInfoByMobile($param)
    {
        $resData =  GroupModel::getMemberInfo($param);
        $newData['mobile'] =empty($resData)?'':$resData->mobile;
        $newData['truename'] =empty($resData)?'':$resData->truename;
        $newData['nickname'] =empty($resData)?'':$resData->nickname;
        $newData['code_number'] =empty($resData)?'':$resData->code_number;
        $newData['mid'] =empty($resData)?0:$resData->mid;
        return $newData;
    }

    //绑定大人
    public static function getDarenMemInfoByMobile($param)
    {
        $res = self::getDarenMidData($param['mobile']);
        $resData=[];
        if(!empty($res)){
            $resData['name'] =!isset($res['nickname'])?'':$res['nickname'];
            $resData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
            $resData['truename'] =!isset($res['nickname'])?'':$res['nickname'];
            $resData['nickname'] =!isset($res['nickname'])?'':$res['nickname'];
            $resData['code_number'] =!isset($res['codeNumber'])?'':$res['codeNumber'];
            $resData['mid'] =!isset($res['mid'])?'':$res['mid'];
        }
        return $resData;
    }

    /**
     *
     *
     */
    public static function  getZhidingMemInfoByMobile($param){
        $res =UseIndexService::getZhidingMidData(['mobile'=>$param['mobile']]);
        $resData=[];
        if(!empty($res)){
            $resData['name'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
            $resData['truename'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['nickname'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['code_number'] =!isset($res['memberSubDto']['inviteCode'])?'':$res['memberSubDto']['inviteCode'];
            $resData['mid'] =isset($res['memberSubDto']['memberSubId'])?$res['memberSubDto']['memberSubId']:'';
        }
        return $resData;
    }


    /**
     * @param $param
     * @return array
     * @throws \Exception
     */
    public static function  getMaituMemInfoByMobile($param)
    {
        $res =UseIndexService::getMaituMidData(['number'=>$param['mobile']]);
        $resData=[];
        if(!empty($res)){
            $resData['name'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
            $resData['truename'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['nickname'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['code_number'] =!isset($res['inviteCode'])?'':$res['inviteCode'];
            $resData['mid'] =!isset($res['memberId'])?'':$res['memberId'];
        }
        return $resData;
    }

    /**
     *
     * 通用接口 获取数据
     *
     * @param $param
     */
    public static function getUniversalInfoByMobile($platform_identity,$param)
    {
        $res =UseIndexService::geUniversalData($platform_identity,['mobile'=>$param['mobile']]);
        $resData=[];
        if(!empty($res)){
            $resData['name'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['mobile'] =!isset($res['mobile'])?'':$res['mobile'];
            $resData['truename'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['nickname'] =!isset($res['nickName'])?'':$res['nickName'];
            $resData['code_number'] =!isset($res['inviteCode'])?'':$res['inviteCode'];
            $resData['mid'] =!isset($res['memberId'])?'':$res['memberId'];
        }
        return $resData;
    }


    /**
     * 绑定群主
     * @param $param
     */
    public   static function  bindGroupMid($param)
    {
        $resData = GroupModel::getMemberInfo($param);
        if ($resData->mid > 0&&$param['id']>0) {
             //查询当前群主的群数量
            $resReturn =   self::getIsAdopt($resData->mid);
            if(!$resReturn){
                return [];
            }
            $groupData =  GroupModel::getBindGroupMidData($param);
            $logMessage = '更新群对应的群主';

            if($groupData->mid!=$resData->mid){

                UserOperationLogService::insertAdminOperation(
                    $param['mid'], 2, json_encode(['code_member'=>$groupData->code_number,'old_mid'=>$groupData->mid,'new_mid'=>$resData->mid ,'nickname'=>$resData->nickname,'new_code_member'=>$resData->code_number,'id'=>$param['id']]), $logMessage, $param['id']
                );

                // if(isset($groupData->mid)&&$groupData->mid>0){

                //    MemberService::sendIsRuleGroup($param['id'],8); 
                // }

                $res = GroupModel::updatedGroupMid($resData->mid,$param['id'],$resData->code_number,$resData->nickname,$resData->mobile );
                //更新群主信息开始
                //MemberService::sendIsRuleGroup($param['id'],1);
                MemberService::newSendIsRuleGroup($param['id']);
                //更新群主信息结束


            }else{

                $res = [];
            }


        } else {
            $res = [];
        }
        return $res;
    }

    /**
     * 判断是否可以绑定群主 （群数量 与 过去30天总的gmv之和）
     * @param $mid
     * @return bool
     */
    public static function  getIsAdopt($mid){
        //获取群数量
        $groupNumData  =  CommunityInvitationGroupModel::getBindGroupNum($mid);
        if(!empty($groupNumData)&&$groupNumData['group_num']>=10){
            //查询当前下的所有gmv
            $allGmvData =   CommunityInvitationGroupModel::getBindGroupAllGmv($groupNumData['ids']);
            if(!empty($allGmvData)&& $allGmvData['allGroupgmv']<1000000){
                return false;
            }
        }
        return true;
    }


    /**
     * 绑定群主
     * @param $param
     */
    public   static function  bindDarenGroupMid($param)
    {
        $resData = self::getDarenMidData($param['mobile']);
        if ($resData['mid'] > 0&&$param['id']>0) {
            $groupData =  GroupModel::getGroupData($param['id']);
            $logMessage = '更新群对应的群主';
            UserOperationLogService::insertAdminOperation(
                $param['mid'], 2, json_encode(['old_mid'=>$groupData->mid,'new_mid'=>$resData['mid'] ,'new_code_member'=>$resData['codeNumber'],'nick_name'=>$resData['nickname'],'id'=>$param['id']]), $logMessage, $param['id']
            );
            $res = GroupModel::updatedGroupMid($resData['mid'] ,$param['id'],$resData['codeNumber'],$resData['nickname'],$resData['mobile']);
        } else {
            $res = [];
        }
        return $res;
    }


    public static function  getZhidingBindByMobile($param){

        $resData =UseIndexService::getZhidingMidData(['mobile'=>$param['mobile']]);
        if ($resData['memberId'] > 0&&$param['id']>0) {
            $groupData =  GroupModel::getGroupData($param['id']);
            $logMessage = '更新群对应的群主';
            UserOperationLogService::insertAdminOperation(
                $param['mid'], 2, json_encode(['old_mid'=>$groupData->mid,'new_mid'=>$resData['memberId'] ,'new_code_member'=>$resData['memberSubDto']['inviteCode'],'nick_name'=>$resData['nickName'],'id'=>$param['id']]), $logMessage, $param['id']
            );
            $res = GroupModel::updatedGroupMid($resData['memberId'] ,$param['id'],$resData['memberSubDto']['inviteCode'],$resData['nickName'],$resData['mobile']);
        } else {
            $res = [];
        }
        return $res;
    }

    /**
     * @param $param
     * @return array|int
     * @throws \Exception
     */
    public  static function getMaituBindByMobile($param){

        $resData =UseIndexService::getMaituMidData(['number'=>$param['mobile']]);
        if ($resData['memberId'] > 0&&$param['id']>0) {
            $groupData =  GroupModel::getGroupData($param['id']);
            $logMessage = '更新群对应的群主';
            UserOperationLogService::insertAdminOperation(
                $param['mid'], 2, json_encode(['old_mid'=>$groupData->mid,'new_mid'=>$resData['memberId'] ,'new_code_member'=>$resData['inviteCode'],'nick_name'=>$resData['nickName'],'id'=>$param['id']]), $logMessage, $param['id']
            );
            $res = GroupModel::updatedGroupMid($resData['memberId'] ,$param['id'],$resData['inviteCode'],$resData['nickName'],$resData['mobile']);
        } else {
            $res = [];
        }
        return $res;
    }

    /**
     * 绑定
     * @param $platform_identity
     * @param $param
     * @return array|int
     * @throws \Exception
     */
    public static  function  getUniversalBindByMobile($platform_identity,$param){

        $resData =UseIndexService::geUniversalData($platform_identity,['mobile'=>$param['mobile']]);
        if ($resData['memberId'] > 0&&$param['id']>0) {
            $groupData =  GroupModel::getGroupData($param['id']);
            $logMessage = '更新群对应的群主';
            UserOperationLogService::insertAdminOperation(
                $param['mid'], 2, json_encode(['old_mid'=>$groupData->mid,'new_mid'=>$resData['memberId'] ,'new_code_member'=>$resData['inviteCode'],'nick_name'=>$resData['nickName'],'id'=>$param['id']]), $logMessage, $param['id']
            );
            $res = GroupModel::updatedGroupMid($resData['memberId'] ,$param['id'],$resData['inviteCode'],$resData['nickName'],$resData['mobile']);
        } else {
            $res = [];
        }
        return $res;

    }

}