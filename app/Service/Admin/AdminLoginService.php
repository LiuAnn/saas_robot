<?php
namespace App\Service\Admin;

use App\Model\Admin\AdminUserModel;
use Illuminate\Support\Facades\Redis;

class AdminLoginService
{
    //固定盐值
    const SALT_FIGURE = 'dataAdmin';

    protected static $instances = [];

    protected $model;

    protected function __construct(AdminUserModel $model)
    {
        $this->adminUserId = $model->admin_user_id;

        $this->adminUserName = $model->admin_user_name;

        $this->adminUserType = $model->admin_user_type;

        $this->password = $model->password;

        $this->model = $model;

        self::$instances[$model->admin_user_id] = $this;

        return $this;
    }

    /*
     * 获取用户名和密码
     * @param  $adminUserName
     * @param  $password
     * @return $model
     */
    public static function getInstanceByNameAndPassword($param = [])
    {

        //接收管理员用户名和密码
        $adminUserName = $param['adminUserName'];
        $passWord = $param['pwd'];

        //验证  参数是否存在
        if($adminUserName == '' || $passWord == '')
        {
            return ['code' => 400 , 'msg' => '缺少必要参数'];
        }

        $adminUserInfo = AdminUserModel::getAdminUserInfo(['admin_user_name'=>$param['adminUserName']]);

        if(empty($adminUserInfo))
        {
            return ['code' => 400, 'msg' => '没有此用户'];
        }

//        dd([
//            'admin_user_name'=>$adminUserName,
//            'password'=>self::createPassword($passWord,$adminUserInfo->salt)
//        ],$adminUserInfo->salt);

        $model = AdminUserModel::where([
            'admin_user_name'=>$adminUserName,
            'password'=>self::createPassword($passWord,$adminUserInfo->salt)
        ]) -> first();


        //登录认证失败
        if(empty($model)){
            return ['code' => 400 , 'msg' => '该管理员用户名和密码错误！'];
        }
        if ($model->is_first) {
            $where = ['admin_user_id' => $model->admin_user_id];
            $update = ['is_first' => 0];
            AdminUserModel::updateUser($where, $update);
        }
        //取出验证通过的  管理员id   管理员名称
        $adminUserId   = $model->admin_user_id;
        $adminUserName = $model->admin_user_name;
        $isFirst = $model->is_first;
        $userRole      = !empty($model->user_role)?explode(",",$model->user_role):[];

        $data=[
            'adminUserId'=>$adminUserId,
            'adminUserName'=>$adminUserName,
            'userRole'=>$userRole,
            'is_first' =>$isFirst
        ];

        $token = self::makeToken($adminUserId);

        $logMessage = '登录';
        AdminOperationLogService::insertAdminOperation(
            $adminUserId, 4, json_encode($data), $logMessage, $adminUserId
        );
        return ['code' => 200 ,'msg'=>'登录成功','token'=>$token,'data' => $data];
    }


    /*
     * 获取用户名和密码
     * @param  $adminUserName
     * @param  $password
     * @return $model
     */
    public static function getInstanceByNameFormiddle($param = [])
    {

        //接收管理员用户名和密码
        $token = isset($param['token'])?$param['token']:'';
        //验证  参数是否存在
        if($token == '')
        {
            return ['code' => 400 , 'msg' => 'token不能为空'];
        }


        $postData['token'] = $token;
        $url               = "http://cidms.yuetao.vip/admin/getCidmsUserInfo";
        $userInfo          = curlPost($url,$postData);
        $arrUserInfo       = json_decode($userInfo, true);

        if ($arrUserInfo['code'] != 200) {
            return ['code' => 400, 'msg' => $arrUserInfo['msg']];
        }
        $adminUserName = $arrUserInfo['data']['managerResDTO']['mobile'];

        //验证  参数是否存在
        if($adminUserName == '')
        {
            return ['code' => 400 , 'msg' => '手机号不能为空'];
        }

        $model = AdminUserModel::where([
            'mobile'=>$adminUserName,
        ]) -> first();

        if(empty($model))
        {
            return ['code' => 400, 'msg' => '没有此用户'];
        }


        if ($model->is_first) {
            $where = ['admin_user_id' => $model->admin_user_id];
            $update = ['is_first' => 0];
            AdminUserModel::updateUser($where, $update);
        }
        //取出验证通过的  管理员id   管理员名称
        $adminUserId   = $model->admin_user_id;
        $adminUserName = $model->admin_user_name;
        $isFirst = $model->is_first;
        $userRole      = !empty($model->user_role)?explode(",",$model->user_role):[];

        $data=[
            'adminUserId'=>$adminUserId,
            'adminUserName'=>$adminUserName,
            'userRole'=>$userRole,
            'is_first' =>$isFirst
        ];

        $token = self::makeToken($adminUserId);

        $logMessage = '登录';
        AdminOperationLogService::insertAdminOperation(
            $adminUserId, 4, json_encode($data), $logMessage, $adminUserId
        );
        return ['code' => 200 ,'msg'=>'登录成功','token'=>$token,'data' => $data];
    }



    /**

    * 验证密码  md5（已经md5加密的密码+盐值）
    * @param $password
    * @return string
    */
    protected static function createPassword($password='',$salt='')
    {
        return md5($password.$salt);
    }

    public static function makeToken($memberId)
    {
        $key        = "data_robot_admin_token_mid_". $memberId;
        $token      = self::getToken($memberId);

        if(!empty($token)) {
            $expire  = 3600*3;
            Redis::expire($key, $expire);
            return $token;
        }

        $time       = time();
        $tokenStr   = "{$memberId}.{$time}";
        $token      = md5($tokenStr);
        $expire     = 3600*3;
        Redis::set($key, $token);
        Redis::expire($key, $expire);
        return $token;
    }

    public static function getToken($memberId)
    {
        $key  = "data_robot_admin_token_mid_". $memberId;
        return Redis::get($key);
    }

    public static function updatePassword($params, $member)
    {
        if($params['newPassword']==$params['confirmPassword']){
            $where = ['admin_user_id'=>$member['adminUserId']];
            $params['newPassword'] = md5($params['newPassword'] . $member['salt']);
            $update = ['password'=>$params['newPassword']];
            $addRes = AdminUserModel::updateUser($where, $update);
            if($addRes){
                return response()->fail(200 , '密码修改成功');
            }else{
                return response()->fail(400 , '更新密码');
            }
        }else{
            return response()->fail(400 , '确认密码和新密码不一致');
        }

    }

}