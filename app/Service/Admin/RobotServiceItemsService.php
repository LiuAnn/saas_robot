<?php
namespace App\Service\Admin;

use App\Model\Admin\RobotBackstageModel;

class RobotServiceItemsService{

    const SALT_FIGURE = 'YlhAdmin';//固定盐值


    /*
     * 服务项目列表
     * @param Array
     */
    public static function SerivceList($param)
    {
        $list = RobotBackstageModel::getServiceList();
        foreach ($list as $key => $item) {
            $list[$key]->create_time = date("Y-m-d H:i:s",$item->create_time);
            $list[$key]->service_price = $item->service_price/100;
        }

        return ["code"=>200,"data"=>empty($list)?[]:$list];
    }

    /*
     * 添加服务项目
     * @param Array
     */
    public static function addSerivce($param = [],$memberInfo)
    {

        //数据拼接
        $param['create_time']     = time();
        $param['service_price']   = $param['service_price']*100;//元转分
        $param['author_id'] = $memberInfo['adminUserId'];
        $param['author_name'] = $memberInfo['adminUserName'];
        //添加服务项目
        $addSerivce = RobotBackstageModel::create($param);

        if($addSerivce){
            return ['code' => 200 , 'msg' => '添加成功！'];
        }else{
            return ['code' => 400 , 'msg' => '添加失败！'];
        }
    }


    /*
     * 修改服务项目
     * @param Array
     */
    public static function updateSerivce($param = [],$memberInfo)
    {

        if (empty($param['id'])||!isset($param['id'])) {
            return ['code' => 400, 'msg' => '服务ID不能为空'];
        }
        $param['service_price']   = $param['service_price']*100;//元转分
        $param['author_id'] = $memberInfo['adminUserId'];
        $param['author_name'] = $memberInfo['adminUserName'];
        //修改服务项目
        $addSerivce = RobotBackstageModel::updateService($param);

        if($addSerivce){
            return ['code' => 200 , 'msg' => '修改成功！'];
        }else{
            return ['code' => 400 , 'msg' => '修改失败！'];
        }
    }



    /*
     * 删除服务项目
     * @param Array
     */
    public static function delService($param = [])
    {

        if (empty($param['id'])||!isset($param['id'])) {
            return ['code' => 400, 'msg' => '服务ID不能为空'];
        }
        //修改服务项目
        $addSerivce = RobotBackstageModel::delService($param);

        if($addSerivce){
            return ['code' => 200 , 'msg' => '删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '删除失败！'];
        }
    }


    /*
     * 购买接入群订单展示
     * @param Array
     */
    public static function getBuyGroupOrderList($param = [])
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;   
        //购买接入群订单展示
        $buyGroupOrderList = RobotBackstageModel::getBuyGroupOrderList($param,$page,$pageSize);

        $totalCount = RobotBackstageModel::getBuyGroupOrderCount($param);
        $list = [];

        foreach ($buyGroupOrderList as $key => $item) {
            
            $list[$key]['service_name'] = $item->service_name;
            switch ($item->service_unit) {
                case 1:
                    $unit = "年";
                    break;
                
                default:
                    $unit = "月";
                    break;
            }
            $list[$key]['username'] = $item->nickname;
            $list[$key]['usermobile'] = $item->mobile;
            $list[$key]['service_price'] = intval(($item->service_price)/100)."/".$unit;
            $list[$key]['TermOfValidity'] = date("Y-m-d",$item->created_at)."-".date("Y-m-d",$item->duetime_at);
            $list[$key]['TermOfValidity'] = date("Y-m-d",$item->created_at)."-".date("Y-m-d",$item->duetime_at);
            $list[$key]['created_at'] = date("Y-m-d",$item->created_at);
        }


        $data = [
            "totalCount"=>$totalCount,
            "list"=>$list
        ];

        return ['code' => 200 , 'data' =>$data];
    } 

    /*
     * 对添加数据   进行验证
     */
    public static function checkInput($param = [])
    {
        //获取   需要验证的数据
        $admin_type       = $param['admin_type'];//管理员类型
        $admin_login_type = $param['admin_login_type'];//管理员登录类型

        if (!is_numeric($admin_type)) {
            return ['code' => 400, 'msg' => '管理员类型必须为整数！'];
        }
        if (!is_numeric($admin_login_type)) {
            return ['code' => 400, 'msg' => '管理员登录类型必须为整数！'];
        }
    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /*
     * 管理员密码加密
     * 加密规则  md5(md5(密码).固定盐值)
     */
    private static function encryptPassword($password = '')
    {
        return md5(md5($password).self::SALT_FIGURE);
    }
}