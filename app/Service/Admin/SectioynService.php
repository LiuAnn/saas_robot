<?php
namespace App\Service\Admin;

use App\Model\Admin\SectioynModel;

class SectioynService {
    /*
     * 获取部门的所有信息
     */
    public static function getSectioyn()
    {
        //查询
        $getSectioyn = SectioynModel::getSectioyn();

        if($getSectioyn == null) {
            return ['code' => 400, 'msg' => '暂无部门数据！'];
        }

        return $getSectioyn;
    }
}