<?php
namespace App\Service\Admin;

use App\Model\Admin\AdminUserModel;
use App\Model\Mall\ProductModel;
use Illuminate\Support\Facades\Log;
class AdminUserService {

    const SALT_FIGURE = 'dataAdmin';//固定盐值

    /*
     * 管理员的添加
     * @param Array
     */
    public static function addAdminUser($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInput($param,'add');
        if($check_input){
            return $check_input;
        }

        $adminUserInfo = AdminUserModel::getAdminUserInfo2(['admin_user_name'=>$param['adminUserName']]);

        if($adminUserInfo)
        {
            return ['code' => 400, 'msg' => '此登录名已经有了'];
        }


        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['admin_user_name']    =    $param['adminUserName'];

        $data['salt']               =    self::SALT_FIGURE."_".self::getSalt();


        $data['password']           =    self::encryptPassword($param['adminPassword'],$data['salt']);

        $data['admin_user_type']    =    $param['adminUserType'];

        $data['created_at']         =    $created_at;

        $data['staff_name']         =    $param['staffName'];

        $data['mobile']             =    !empty($param['mobile'])?$param['mobile']:'';

        $data['department_id']      =    !empty($param['departmentId'])?$param['departmentId']:'';

        $data['position_id']        =    !empty($param['positionId'])?$param['positionId']:'';

        $data['user_role']          =    !empty($param['userRole'])?$param['userRole']:'';

        //管理员添加
        $adminCreate = AdminUserModel::addAdminUser($data);

        if($adminCreate)
        {
            $logMessage = '新增管理员';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 1, json_encode($data), $logMessage, $adminCreate
            );
            return ['code' => 200 , 'msg' => '管理员添加成功！'];
        }else{
            return ['code' => 200 , 'msg' => '管理员添加失败！'];
        }
    }

    /*
     * 对添加数据   进行验证
     */
    public static function checkInput($param = [],$type='')
    {
        if(!isset($param['adminUserName'])||empty($param['adminUserName']))
        {
            return ['code' => 400, 'msg' => '登录名称不能为空！'];
        }

        if($type=='add')
        {
            if(!isset($param['adminPassword'])||empty($param['adminPassword']))
            {
                return ['code' => 400, 'msg' => '管理员密码不能为空！'];
            }
        }

        if(!isset($param['adminUserType'])||empty($param['adminUserType']))
        {
            return ['code' => 400, 'msg' => '管理员类型必须为整数！'];
        }

        if(!isset($param['staffName'])||empty($param['staffName']))
        {
            return ['code' => 400, 'msg' => '员工名称不能为空！'];
        }

        if(!isset($param['userRole'])||empty($param['userRole']))
        {
            return ['code' => 400, 'msg' => '员工角色不能为空！'];
        }

//        if(!isset($param['mobile'])||empty($param['mobile']))
//        {
//            return ['code' => 400, 'msg' => '手机号不能为空！'];
//        }

    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /*
     * 管理员密码加密
     * 加密规则  md5(md5(密码).固定盐值)
     */
    private static function encryptPassword($password = '',$salt='')
    {
        return md5(md5($password).$salt);
    }

    public static function getSalt()
    {
        $str='';
        for ($i = 1; $i <= 4; $i++)
        {
            $str.=chr(rand(97, 122));
        }

        return $str;
    }

    /**
     * 获取列表
     * @param Array
     * @return Array
     */
    public static function getAdminUsers($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = AdminUserModel::getAdminUsers2($param);

        $count     = AdminUserModel::getListCount2($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatAdminUsers($result)
            ],
        ];
    }

    public static function formatAdminUsers($data)
    {
        $list = [];

        foreach ($data as $item)
        {
            $userRole ='';
            if(!empty($item->user_role))
            {
                $role_result = AdminUserModel::getRoles(['roleIds'=>$item->user_role]);
                if($role_result)
                {
                    foreach ($role_result as $value)
                    {
                        $userRole .= $value->role_name.",";
                    }
                }
            }

            $departmentInfo=AdminUserModel::getDepartment(['departmentId'=>$item->department_id]);

            $positionInfo=AdminUserModel::getPosition(['positionId'=>$item->position_id]);

            $list[] = [
                "adminUserId"     => $item->admin_user_id . '',
                "staffName"       => !empty($item->staff_name)?$item->staff_name:'',
                "departmentName"  => !empty($departmentInfo)?$departmentInfo->department_name:'',
                "positionName"    => !empty($positionInfo)?$positionInfo->position_name:'',
                "mobile"          => !empty($item->mobile)?$item->mobile:'',
                "status"          => !empty($item->status)&&$item->status==1?'有效':'无效',
                "adminUserName"   => $item->admin_user_name . '',
                "userRole"        => trim($userRole,","),
                "adminUserType"   => $item->admin_user_type. '',
                "salt"            => !empty($item->salt)?$item->salt. '':'',
                "createdAt"       => !empty($item->created_at)?$item->created_at:'',
            ];
        }
        return $list;
    }

    public static function upAdminUser($memberInfo, $param = [])
    {
        if(!isset($param['adminId'])||empty($param['adminId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        if(!isset($param['salt'])||empty($param['salt']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        //验证
        $check_input = self::checkInput($param,'up');

        if($check_input)
        {
            return $check_input;
        }

        $adminUserInfo = AdminUserModel::getAdminUserInfo2(['admin_user_name'=>$param['adminUserName'],'no_admin_user_id'=>$param['adminId']]);

        if($adminUserInfo)
        {
            return ['code' => 400, 'msg' => '此管理员名称已经有了'];
        }


        //当前时间戳
        $timestamp  = time();
        $upTime = self::formatTimeStamp($timestamp);


        //数据拼接
        $data['admin_user_name']     = $param['adminUserName'];

        if(isset($param['adminPassword'])&&!empty($param['adminPassword']))
        {
            $data['password'] = self::encryptPassword($param['adminPassword'],$param['salt']);
        }

        $data['admin_user_type']    = $param['adminUserType'];
        $data['salt']               = $param['salt'];

        $data['updated_at']         = $upTime;

        $data['staff_name']         = $param['staffName'];

        $data['mobile']             = !empty($param['mobile'])?$param['mobile']:'';;

        $data['department_id']      = !empty($param['departmentId'])?$param['departmentId']:'';

        $data['position_id']        = !empty($param['positionId'])?$param['positionId']:'';

        $data['user_role']          = !empty($param['userRole'])?$param['userRole']:'';

        $where['admin_user_id']     = $param['adminId'];


        //管理员更新
        $adminCreate = AdminUserModel::upAdminUser($where,$data);

        if($adminCreate){
            $logMessage = '修改用户信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 2, json_encode($data), $logMessage, $param['adminId']
            );
            return ['code' => 200 , 'msg' => '管理员更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '管理员更新失败！'];
        }
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getAdminUser($param)
    {
        if(!isset($param['adminId'])||empty($param['adminId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getAdminUserInfo2($param);

        if(empty($result))
        {
            return ['code' => 400 , 'msg' => '用户信息不正确！'];
        }

        return [
            "code" => 200,
            "data" => self::formatAdminUser($result),
        ];
    }

    public static function delAdminUser($param = [])
    {
        if(!isset($param['adminId'])||empty($param['adminId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $adminUserInfo = AdminUserModel::getAdminUserInfo2(['adminId'=>$param['adminId']]);

        if(empty($adminUserInfo))
        {
            return ['code' => 400, 'msg' => '此管理员不存在'];
        }

        $data['deleted_at']      = date("Y-m-d H:i:s",time());
        $data['status']          = 0;

        $where['admin_user_id']  = $param['adminId'];


        //管理员删除
        $adminCreate = AdminUserModel::upAdminUser($where,$data);

        if($adminCreate){
            return ['code' => 200 , 'msg' => '管理员删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '管理员删除失败！'];
        }

    }

    public static function formatAdminUser($data)
    {
        $list= [
            "adminUserId"     => $data->admin_user_id . '',
            "staffName"       => !empty($data->staff_name)?$data->staff_name:'',
            "departmentId"    => !empty($data->department_id)?$data->department_id:'',
            "positionId"      => !empty($data->position_id)?$data->position_id:'',
            "mobile"          => !empty($data->mobile)?$data->mobile:'',
            "status"          => !empty($data->status)&&$data->status==1?'有效':'无效',
            "adminUserName"   => $data->admin_user_name . '',
            "userRole"        => !empty($data->user_role)?$data->user_role:'',
            "adminUserType"   => $data->admin_user_type. '',
            "salt"            => !empty($data->salt)?$data->salt. '':'',
            "createdAt"       => !empty($data->created_at)?$data->created_at:'',
        ];
        return $list;
    }

    public static function getRoles($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = AdminUserModel::getRolesList($param);

        $count     = AdminUserModel::getRolesCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatRoles($result)
            ],
        ];
    }

    public static function getUserRoles($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = AdminUserModel::getUserRolesList($param);

        $count     = AdminUserModel::getUserRolesCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatRoles($result)
            ],
        ];
    }

    public static function formatRoles($data)
    {
        $list = [];

        foreach ($data as $item)
        {
            $list[] = [
                "roleId"       => $item->role_id,
                "roleName"     => $item->role_name . '',
                "status"       => $item->status==1?'有效':'无效',
                "createdAt"    => !empty($item->created_at)?$item->created_at . '':'',
                "rolePwoer"    => !empty($item->role_pwoer)?$item->role_pwoer . '':'',
                "updatedAt"    => !empty($item->updated_at)?$item->updated_at:'',
            ];
        }
        return $list;
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getRole($param)
    {
        if(!isset($param['roleId'])||empty($param['roleId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getRole($param);

        return [
            "code" => 200,
            "data" => self::formatRole($result),
        ];
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getUserRole($param)
    {
        if(!isset($param['roleId'])||empty($param['roleId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getUserRole($param);

        return [
            "code" => 200,
            "data" => self::formatRole($result),
        ];
    }

    public static function formatRole($data)
    {
        $list= [
            "roleId"       => $data->role_id . '',
            "roleName"     => $data->role_name . '',
            "rolePwoer"    => $data->role_pwoer . '',
            "createdAt"    => !empty($data->created_at)?$data->created_at . '':'',
            "updatedAt"    => !empty($data->updated_at)?$data->updated_at:'',
        ];

        return $list;
    }

    public static function addRole($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInputRole($param);

        if($check_input){
            return $check_input;
        }

        $roleInfo = AdminUserModel::getRole(['roleName'=>$param['roleName']]);

        if($roleInfo)
        {
            return ['code' => 400, 'msg' => '此角色名已经有了'];
        }

        //当前时间戳
        $timestamp              =   time();
        $created_at             =   self::formatTimeStamp($timestamp);

        //数据拼接
        $data['role_name']      =   $param['roleName'];

        $data['created_at']     =   $created_at;
        $data['role_pwoer']     =   !empty($param['rolePwoer'])?$param['rolePwoer']:'';

        //添加
        $result = AdminUserModel::addRole($data);

        if($result)
        {
            $logMessage = '新增角色';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 1, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '角色添加成功！'];
        }else{
            return ['code' => 200 , 'msg' => '角色添加失败！'];
        }
    }

    public static function addUserRole($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInputRole($param);

        if($check_input){
            return $check_input;
        }

        $roleInfo = AdminUserModel::getUserRole(['roleName'=>$param['roleName']]);

        if($roleInfo)
        {
            return ['code' => 400, 'msg' => '此角色名已经有了'];
        }

        //当前时间戳
        $timestamp              =   time();
        $created_at             =   self::formatTimeStamp($timestamp);

        //数据拼接
        $data['role_name']      =   $param['roleName'];

        $data['created_at']     =   $created_at;
        $data['role_pwoer']     =   !empty($param['rolePwoer'])?$param['rolePwoer']:'';

        //添加
        $result = AdminUserModel::addUserRole($data);

        if($result)
        {
            $logMessage = '添加管理员角色信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 1, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '角色添加成功！'];
        }else{
            return ['code' => 200 , 'msg' => '角色添加失败！'];
        }
    }

    public static function checkInputRole($param = [])
    {
        if(!isset($param['roleName'])||empty($param['roleName']))
        {
            return ['code' => 400, 'msg' => '角色名称不能为空！'];
        }
    }

    public static function upRole($param = [])
    {
        //验证
        $check_input = self::checkInputRole($param);
        if($check_input){
            return $check_input;
        }

        if(!isset($param['roleId'])||empty($param['roleId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $roleInfo = AdminUserModel::getRole(['roleName'=>$param['roleName'],'noRoleId'=>$param['roleId']]);

        if($roleInfo)
        {
            return ['code' => 400, 'msg' => '此角色名已经有了'];
        }

        //当前时间戳
        $timestamp              =   time();

        $updated_at             =   self::formatTimeStamp($timestamp);

        //数据拼接
        $data['role_name']      = $param['roleName'];


        $data['updated_at']     = $updated_at;

        $data['role_pwoer']     = !empty($param['rolePwoer'])?$param['rolePwoer']:'';


        $where = ['role_id'=>$param['roleId']];

        $result = AdminUserModel::upRole($where,$data);

        if($result)
        {
            return ['code' => 200 , 'msg' => '角色更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '角色更新失败！'];
        }
    }

    public static function upUserRole($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInputRole($param);
        if($check_input){
            return $check_input;
        }

        if(!isset($param['roleId'])||empty($param['roleId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $roleInfo = AdminUserModel::getUserRole(['roleName'=>$param['roleName'],'noRoleId'=>$param['roleId']]);

        if($roleInfo)
        {
            return ['code' => 400, 'msg' => '此角色名已经有了'];
        }

        //当前时间戳
        $timestamp              =   time();

        $updated_at             =   self::formatTimeStamp($timestamp);

        //数据拼接
        $data['role_name']      = $param['roleName'];


        $data['updated_at']     = $updated_at;

        $data['role_pwoer']     = !empty($param['rolePwoer'])?$param['rolePwoer']:'';


        $where = ['role_id'=>$param['roleId']];

        $result = AdminUserModel::upUserRole($where,$data);

        if($result)
        {
            $logMessage = '修改管理员权限信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 2, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '角色更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '角色更新失败！'];
        }
    }


    public static function delRole($memberInfo, $param = [])
    {
        if(!isset($param['roleId'])||empty($param['roleId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $roleInfo = AdminUserModel::getRole(['roleId'=>$param['roleId']]);

        if(empty($roleInfo))
        {
            return ['code' => 400, 'msg' => '角色不存在'];
        }

        //当前时间戳
        $timestamp              = time();
        //数据拼接

        $data['deleted_at']     = $timestamp;

        $data['status']         = 0;

        $where = ['role_id'=>$param['roleId']];

        $result = AdminUserModel::upRole($where,$data);

        if($result)
        {
            $logMessage = '删除管理员角色';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 3, json_encode($data), $logMessage, $param['roleId']
            );
            return ['code' => 200 , 'msg' => '角色删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '角色删除失败！'];
        }
    }

    public static function delUserRole($memberInfo, $param = [])
    {
        if(!isset($param['roleId'])||empty($param['roleId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $roleInfo = AdminUserModel::getUserRole(['roleId'=>$param['roleId']]);

        if(empty($roleInfo))
        {
            return ['code' => 400, 'msg' => '角色不存在'];
        }

        //当前时间戳
        $timestamp              = time();
        //数据拼接

        $data['deleted_at']     = $timestamp;

        $data['status']         = 0;

        $where = ['role_id'=>$param['roleId']];

        $result = AdminUserModel::upUserRole($where,$data);

        if($result)
        {
            $logMessage = '删除管理员权限信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 3, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '角色删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '角色删除失败！'];
        }
    }

    /**
     * 获取列表
     * @param Array
     * @return Array
     */
    public static function getPowers($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = AdminUserModel::getPowersList($param);

        $count     = AdminUserModel::getPowersCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatPowers($result)
            ],
        ];
    }

    /**
     * 获取用户端权限列表列表
     * @param Array
     * @return Array
     */
    public static function getUserPowerList($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        if(isset($param['source'])&&$param['source']>1)
        {
            $count = 0;
            $result = [];
        }else{

            $result    = AdminUserModel::getUserPowersList($param);

            $count     = AdminUserModel::getUserPowersCount($param);

        }

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatPowers($result)
            ],
        ];
    }

    public static function formatPowers($data)
    {
        $list = [];

        foreach ($data as $item)
        {
            $list[] = [
                "sort"       => $item->sort,
                "powerId"       => $item->power_id,
                "powerName"     => $item->power_name . '',
                "level"         => $item->level . '',
                "pid"           => $item->pid . '',
                "icon"          => !empty($item->icon)?$item->icon . '':'',
                "powerRouteName"=> !empty($item->power_route_name)?$item->power_route_name . '':'',
                "status"        => $item->status==1?'有效':'无效',
                "createdAt"     => !empty($item->created_at)?$item->created_at . '':'',
                "updatedAt"     => !empty($item->updated_at)?$item->updated_at:'',
            ];
        }
        return $list;
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getPower($param)
    {
        if(!isset($param['powerId'])||empty($param['powerId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getPower($param);

        return [
            "code" => 200,
            "data" => self::formatPower($result),
        ];
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getUserPowerInfo($param)
    {
        if(!isset($param['powerId'])||empty($param['powerId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getUserPower($param);

        return [
            "code" => 200,
            "data" => self::formatPower($result),
        ];
    }

    public static function formatPower($data)
    {

        $list = [
            "sort"       => $data->sort,
            "powerId"       => $data->power_id,
            "powerName"     => $data->power_name . '',
            "level"         => $data->level . '',
            "pid"           => $data->pid . '',
            "status"        => $data->status==1?'有效':'无效',
            "icon"          => !empty($data->icon)?$data->icon . '':'',
            "powerRouteName"=> !empty($data->power_route_name)?$data->power_route_name . '':'',
            "createdAt"     => !empty($data->created_at)?$data->created_at . '':'',
            "updatedAt"     => !empty($data->updated_at)?$data->updated_at:'',
        ];

        return $list;
    }

    public static function addPower($param = [])
    {
        //验证
        $check_input = self::checkInputPower($param);

        if($check_input){
            return $check_input;
        }

        $roleInfo = AdminUserModel::getPower(['powerName'=>$param['powerName']]);

        if($roleInfo)
        {
            return ['code' => 400, 'msg' => '此权限名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['power_name']         = $param['powerName'];
        $data['level']              = $param['level'];
        $data['sort']              =  !empty($param['sort'])?$param['sort']:0;

        $data['power_route_name']   = !empty($param['powerRouteName'])?$param['powerRouteName']:'';
        $data['pid']                = !empty($param['pid'])?$param['pid']:0;
        $data['icon']               = !empty($param['icon'])?$param['icon']:'';

        $data['status']             = 1;
        $data['created_at']         = $created_at;

        //添加
        $result = AdminUserModel::addPower($data);

        if($result)
        {
            return ['code' => 200 , 'msg' => '权限添加成功！'];
        }else{
            return ['code' => 400 , 'msg' => '权限添加失败！'];
        }
    }



    public static function addUserPower($memberInfo, $param = [])
    {
        //验证
        $check_input = self::checkInputPower($param);

        if($check_input){
            return $check_input;
        }

        $roleInfo = AdminUserModel::getUserPower(['powerName'=>$param['powerName']]);

        if($roleInfo)
        {
            return ['code' => 400, 'msg' => '此权限名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['power_name']         = $param['powerName'];
        $data['level']              = $param['level'];
        $data['sort']              =  !empty($param['sort'])?$param['sort']:0;
        $data['power_route_name']   = !empty($param['powerRouteName'])?$param['powerRouteName']:'';
        $data['pid']                = !empty($param['pid'])?$param['pid']:0;
        $data['icon']               = !empty($param['icon'])?$param['icon']:'';

        $data['status']             = 1;
        $data['created_at']         = $created_at;

        //添加
        $result = AdminUserModel::addUserPower($data);

        if($result)
        {
            $logMessage = '添加用户端权限';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 1, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '权限添加成功！'];
        }else{
            return ['code' => 400 , 'msg' => '权限添加失败！'];
        }
    }

    public static function checkInputPower($param = [])
    {
        if(!isset($param['powerName'])||empty($param['powerName']))
        {
            return ['code' => 400, 'msg' => '权限名称不能为空！'];
        }

        if(!isset($param['level'])||empty($param['level']))
        {
            return ['code' => 400, 'msg' => '菜单等级不能为空！'];
        }
    }

    public static function upPower($param = [])
    {
        if(!isset($param['powerId'])||empty($param['powerId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        //验证
        $check_input = self::checkInputPower($param);

        if($check_input){
            return $check_input;
        }

        $powerInfo = AdminUserModel::getPower(['powerName'=>$param['powerName'],'noPowerId'=>$param['powerId']]);

        if($powerInfo)
        {
            return ['code' => 400, 'msg' => '此权限名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();

        $updated_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['power_name']         = $param['powerName'];
        $data['level']              = $param['level'];
        $data['sort']              =  !empty($param['sort'])?$param['sort']:0;
        $data['power_route_name']   = !empty($param['powerRouteName'])?$param['powerRouteName']:'';

        $data['pid']                = !empty($param['pid'])?$param['pid']:0;
        $data['icon']               = !empty($param['icon'])?$param['icon']:'';

        $data['updated_at']         = $updated_at;

        $where=['power_id'=>$param['powerId']];

        //添加
        $result = AdminUserModel::upPower($where,$data);

        if($result)
        {
            return ['code' => 200 , 'msg' => '权限更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '权限更新失败！'];
        }
    }


    public static function upUserPower($memberInfo, $param = [])
    {
        if(!isset($param['powerId'])||empty($param['powerId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        //验证
        $check_input = self::checkInputPower($param);

        if($check_input){
            return $check_input;
        }

        $powerInfo = AdminUserModel::getUserPower(['powerName'=>$param['powerName'],'noPowerId'=>$param['powerId']]);

        if($powerInfo)
        {
            return ['code' => 400, 'msg' => '此权限名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();

        $updated_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['power_name']         = $param['powerName'];
        $data['level']              = $param['level'];
        $data['sort']              =  !empty($param['sort'])?$param['sort']:0;
        $data['power_route_name']   = !empty($param['powerRouteName'])?$param['powerRouteName']:'';

        $data['pid']                = !empty($param['pid'])?$param['pid']:0;
        $data['icon']               = !empty($param['icon'])?$param['icon']:'';

        $data['updated_at']         = $updated_at;

        $where=['power_id'=>$param['powerId']];

        //添加
        $result = AdminUserModel::upUserPower($where,$data);

        if($result)
        {
            $logMessage = '修改用户端权限';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 2, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '权限更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '权限更新失败！'];
        }
    }


    public static function getAdminOperationList($param = [])
    {
        //查询开始时间
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . ' 00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
            if (!is_timestamp($param['beginTime']) || !is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
            $endTime = date('Y-m-d 00:00:00', $param['endTime']);
            //开始结束时间容错
            if ($param['beginTime'] == $endTime) {
                $endTime = date('Y-m-d H:i:s', strtotime("$endTime+1day"));
            }
            $param['endTime'] = $endTime;
        }
        $pageSize = $param['pageSize'] ?? 10;
        $operationList = AdminUserModel::getAdminOperationList($param, $pageSize);
        $operationList = json_decode(json_encode($operationList), true);
        if (!empty($operationList['data']) && is_array($operationList['data'])) {
            $operationList['data'] = array_map(function ($value) {
                switch ($value['type']) {
                    case '1':
                        $value['operation'] = '新增';
                        break;
                    case '2':
                        $value['operation'] = '修改';
                        break;
                    case '3':
                        $value['operation'] = '删除';
                        break;
                    case '4':
                        $value['operation'] = '登录';
                        break;
                    default:
                        $value['operation'] = '';
                }
                $value['created_time'] = date('Y-m-d H:i:s', $value['created_time']);
                return $value;
            }, $operationList['data']);
        }
        return $operationList;
    }


    public static function getAdminListByName($memberInfo, $param)
    {
        return AdminUserModel::getAdminListByName($param);
    }

    public static function delPower($memberInfo, $param = [])
    {
        if(!isset($param['powerId'])||empty($param['powerId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $powerInfo = AdminUserModel::getPower(['powerId'=>$param['powerId']]);

        if(empty($powerInfo))
        {
            return ['code' => 400, 'msg' => '此权限不存在'];
        }

        //当前时间戳
        $timestamp  = time();

        $data['deleted_at']         = $timestamp;

        $data['status']          = 0;

        $where=['power_id'=>$param['powerId']];

        //添加
        $result = AdminUserModel::upPower($where,$data);

        if($result)
        {
            $logMessage = '删除权限';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 3, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '权限删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '权限删除失败！'];
        }
    }


    public static function delUserPower($memberInfo, $param = [])
    {
        if(!isset($param['powerId'])||empty($param['powerId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $powerInfo = AdminUserModel::getUserPower(['powerId'=>$param['powerId']]);

        if(empty($powerInfo))
        {
            return ['code' => 400, 'msg' => '此权限不存在'];
        }

        //当前时间戳
        $timestamp  = time();

        $data['deleted_at']         = $timestamp;

        $data['status']          = 0;

        $where=['power_id'=>$param['powerId']];

        //添加
        $result = AdminUserModel::upUserPower($where,$data);

        if($result)
        {
            $logMessage = '删除用户端权限';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 3, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '权限删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '权限删除失败！'];
        }
    }

    /**
     * 获取权限树状图
     * @param Array
     * @return Array
     */
    public static function getTreePowers($param)
    {
        $level1_where = ['level'=>1,'pid'=>1];

        $level1_result    = AdminUserModel::getPowers($level1_where);

        if(empty($level1_result))
        {
            return [
                "code" => 200,
                "data" => [],
            ];
        }

        $result=[];
        foreach ($level1_result as $key=>$value)
        {

            $level2_where     = ['level'=>2,'pid'=>$value->power_id];

            $level2_result    = AdminUserModel::getPowers($level2_where);

            $children=[];
            foreach ($level2_result as $k=>$v)
            {
                $children[$k]['powerId']    = $v->power_id;
                $children[$k]['powerName']  = $v->power_name;
                $children[$k]['level']      = $v->level;
                $children[$k]['sort']      = $v->sort;


                $level3_where     = ['level'=>3,'pid'=>$v->power_id];

                $level3_result    = AdminUserModel::getPowers($level3_where);

                $children3=[];
                foreach ($level3_result as $kk=>$vv)
                {
                    $children3[$kk]['powerId']    = $vv->power_id;
                    $children3[$kk]['powerName']  = $vv->power_name;
                    $children3[$kk]['level']      = $vv->level;
                    $children3[$kk]['sort']      = $vv->sort;
                }

                if($children3)
                {
                    $children[$k]['children']   = $children3;
                }

            }

            $result[$key]['powerId']    = $value->power_id;
            $result[$key]['powerName']  = $value->power_name;
            $result[$key]['level']      = $value->level;
            $result[$key]['children']   = $children;
        }

        return [
            "code" => 200,
            "data" => $result,
        ];
    }


    /**
     * 获取权限树状图
     * @param Array
     * @return Array
     */
    public static function getUserTreePowers($param)
    {
        $level1_where = ['level'=>1,'pid'=>1];

        $level1_result    = AdminUserModel::getUserPowers($level1_where);

        if(empty($level1_result))
        {
            return [
                "code" => 200,
                "data" => [],
            ];
        }

        $result=[];
        foreach ($level1_result as $key=>$value)
        {

            $level2_where     = ['level'=>2,'pid'=>$value->power_id];

            $level2_result    = AdminUserModel::getUserPowers($level2_where);

            $children=[];
            foreach ($level2_result as $k=>$v)
            {
                $children[$k]['powerId']    = $v->power_id;
                $children[$k]['powerName']  = $v->power_name;
                $children[$k]['level']      = $v->level;
                $children[$k]['sort']      = $v->sort;


                $level3_where     = ['level'=>3,'pid'=>$v->power_id];

                $level3_result    = AdminUserModel::getUserPowers($level3_where);

                $children3=[];
                foreach ($level3_result as $kk=>$vv)
                {
                    $children3[$kk]['powerId']    = $vv->power_id;
                    $children3[$kk]['powerName']  = $vv->power_name;
                    $children3[$kk]['level']      = $vv->level;
                    $children3[$kk]['sort']      = $vv->sort;
                }

                if($children3)
                {
                    $children[$k]['children']   = $children3;
                }

            }

            $result[$key]['powerId']    = $value->power_id;
            $result[$key]['powerName']  = $value->power_name;
            $result[$key]['level']      = $value->level;
            $result[$key]['children']   = $children;
        }

        return [
            "code" => 200,
            "data" => $result,
        ];
    }

    public static function getUserPowers($memberInfo,$param=[])
    {
        if(empty($memberInfo))
        {
            return ['code' => 400, 'msg' => '用户信息错误！'];
        }

        if(empty($memberInfo['userRole']))
        {
            return ['code' => 200, 'msg' => '用户无角色！'];
        }

        $roles =AdminUserModel::getRoles(['roleIds'=>$memberInfo['userRole']]);

        if(empty($roles))
        {
            return ['code' => 200, 'msg' => '无角色信息！'];
        }

        $powerIds='';
        foreach ($roles as $value)
        {
            $powerIds.=$value->role_pwoer.",";
        }

        if(empty($powerIds))
        {
            return ['code' => 200, 'msg' => '角色无权限信息！'];
        }

        $powerIds = explode(",",trim($powerIds,','));

        $powerIds = array_unique($powerIds);

        if(empty($powerIds))
        {
            return ['code' => 200, 'msg' => '无权限信息！'];
        }

        $powerIds = implode(",",$powerIds);

        $result      = AdminUserModel::getPowers(['powerIds'=>$powerIds]);

        $formatArray = self::formatPowers($result);

        $userPower= self::generateTree($formatArray);

        return [
            "code" => 200,
            "data" => $userPower,
        ];
    }


    public static function getUserPowersList($memberInfo,$param=[])
    {   
        if(empty($memberInfo))
        {
            return ['code' => 400, 'msg' => '用户信息错误！'];
        }

        if(empty($memberInfo['user_type']))
        {
            return ['code' => 200, 'msg' => '用户无角色！'];
        }
        $roles =AdminUserModel::getUserRoles(['roleIds'=>$memberInfo['front_role_id']]);

        if(empty($roles))
        {
            return ['code' => 200, 'msg' => '无角色信息！'];
        }

        $powerIds='';
        foreach ($roles as $value)
        {
            $powerIds.=$value->role_pwoer.",";
        }

        if(empty($powerIds))
        {
            return ['code' => 200, 'msg' => '角色无权限信息！'];
        }

        $powerIds = explode(",",trim($powerIds,','));

        $powerIds = array_unique($powerIds);

        if(empty($powerIds))
        {
            return ['code' => 200, 'msg' => '无权限信息！'];
        }

        $powerIds = implode(",",$powerIds);

        $result      = AdminUserModel::getUserPowers(['powerIds'=>$powerIds]);

        $formatArray = self::formatPowers($result);

        $userPower= self::generateTree($formatArray);

        return [
            "code" => 200,
            "data" => $userPower,
        ];
    }

    public static function  getUserFrontPowersList($memberInfo,$param){

        if(empty($memberInfo))
        {
            return ['code' => 400, 'msg' => '用户信息错误！'];
        }

        if(empty($memberInfo['user_type']))
        {
            return ['code' => 200, 'msg' => '用户无角色！'];
        }

        $roles =AdminUserModel::getUserRoleFront(['roleId'=>$memberInfo['front_role_id']]);

        if(empty($roles))
        {
            return ['code' => 200, 'msg' => '无角色信息！'];
        }

//        $powerIds='';
//        foreach ($roles as $value)
//        {
//            $powerIds.=$value->role_pwoer.",";
//        }
        $powerIds = $roles->role_pwoer;
        if(empty($powerIds))
        {
            return ['code' => 200, 'msg' => '角色无权限信息！'];
        }

        $powerIds = explode(",",trim($powerIds,','));

        $powerIds = array_unique($powerIds);

        if(empty($powerIds))
        {
            return ['code' => 200, 'msg' => '无权限信息！'];
        }

        $powerIds = implode(",",$powerIds);
        Log::info('powerIds  value：'.json_encode($powerIds));
        $result      = AdminUserModel::getUserPowers(['powerIds'=>$powerIds]);


        $formatArray = self::formatPowers($result);

        $userPower= self::generateTree($formatArray);
        Log::info('powerIds  value：'.json_encode($userPower));
        return [
            "code" => 200,
            "data" => $userPower,
        ];
    }



    public static function generateTree($array)
    {
        //第一步 构造数据
        $items = array();
        foreach($array as $value){
            $items[$value['powerId']] = $value;
        }
        //第二部 遍历数据 生成树状结构
        $tree = array();
        foreach($items as $key => $item){
            if(isset($items[$item['pid']])){
                $items[$item['pid']]['children'][] = &$items[$key];
            }else{
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }





    public static function getDepartments($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = AdminUserModel::getDepartmentsList($param);

        $count     = AdminUserModel::getDepartmentsCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatDepartments($result)
            ],
        ];
    }

    public static function formatDepartments($data)
    {
        $list = [];

        foreach ($data as $item)
        {
            $list[] = [
                "departmentId"    => $item->id,
                "departmentName"  => $item->department_name . '',
                "status"          => $item->status==1?'有效':'无效',
                "createdAt"       => !empty($item->created_at)?$item->created_at . '':'',
                "updatedAt"       => !empty($item->updated_at)?$item->updated_at:'',
            ];
        }
        return $list;
    }

    public static function formatDepartment($data)
    {
        $list=[];

        if($data)
        {
            $list = [
                "departmentId"   => $data->id,
                "departmentName" => $data->department_name . '',
                "status"         => $data->status==1?'有效':'无效',
                "createdAt"      => !empty($data->created_at)?$data->created_at . '':'',
                "updatedAt"      => !empty($data->updated_at)?$data->updated_at:'',
            ];
        }

        return $list;
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getDepartment($param)
    {
        if(!isset($param['departmentId'])||empty($param['departmentId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getDepartment($param);

        return [
            "code" => 200,
            "data" => self::formatDepartment($result),
        ];
    }

    public static function addDepartment($memberInfo, $param = [])
    {
        //验证
        if(!isset($param['departmentName'])||empty($param['departmentName']))
        {
            return ['code' => 400, 'msg' => '名称不能为空！'];
        }

        $departmentInfo = AdminUserModel::getDepartment(['departmentName'=>$param['departmentName']]);

        if($departmentInfo)
        {
            return ['code' => 400, 'msg' => '此部门名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['department_name']           = $param['departmentName'];
        $data['status']         = 1;
        $data['created_at']     = $created_at;

        //添加
        $result = AdminUserModel::addDepartment($data);

        if($result)
        {
            $logMessage = '添加部门信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 1, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '部门添加成功！'];
        }else{
            return ['code' => 400 , 'msg' => '部门添加失败！'];
        }
    }

    public static function upDepartment($memberInfo, $param = [])
    {
        if(!isset($param['departmentId'])||empty($param['departmentId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        //验证
        if(!isset($param['departmentName'])||empty($param['departmentName']))
        {
            return ['code' => 400, 'msg' => '名称不能为空！'];
        }


        $departmentInfo = AdminUserModel::getDepartment(['departmentName'=>$param['departmentName'],'noDepartmentId'=>$param['departmentId']]);

        if($departmentInfo)
        {
            return ['code' => 400, 'msg' => '此部门名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();

        $updated_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['department_name']         = $param['departmentName'];

        $data['updated_at']   = $updated_at;

        $where=['id'=>$param['departmentId']];

        //添加
        $result = AdminUserModel::upDepartment($where,$data);

        if($result)
        {
            $logMessage = '修改部门信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 2, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '部门更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '部门更新失败！'];
        }
    }

    public static function delDepartment($memberInfo, $param = [])
    {
        if(!isset($param['departmentId'])||empty($param['departmentId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $departmentInfo = AdminUserModel::getDepartment(['departmentId'=>$param['departmentId']]);

        if(empty($departmentInfo))
        {
            return ['code' => 400, 'msg' => '此部门不存在'];
        }

        //当前时间戳
        $timestamp  = time();

        $data['deleted_at']         = $timestamp;

        $data['status']          = 0;

        $where=['id'=>$param['departmentId']];

        //添加
        $result = AdminUserModel::upDepartment($where,$data);

        if($result)
        {
            $logMessage = '删除部门信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 3, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '部门删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '部门删除失败！'];
        }
    }



    public static function getPositions($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 10;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = AdminUserModel::getPositionsList($param);

        $count     = AdminUserModel::getPositionsCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatPositions($result)
            ],
        ];
    }

    public static function formatPositions($data)
    {
        $list = [];

        if(!empty($data))
        {
            foreach ($data as $item)
            {
                $departmentInfo=AdminUserModel::getDepartment(['departmentId'=>$item->department_id]);
                $list[] = [
                    "positionId"      => $item->id,
                    "positionName"    => $item->position_name . '',
                    "departmentName"  => !empty($departmentInfo)?$departmentInfo->department_name . '':'',
                    "status"          => $item->status==1?'有效':'无效',
                    "createdAt"       => !empty($item->created_at)?$item->created_at . '':'',
                    "updatedAt"       => !empty($item->updated_at)?$item->updated_at:'',
                ];
            }
        }

        return $list;
    }


    public static function formatPosition($data)
    {
        $list=[];

        if($data)
        {
            $list = [
                "positionId"     => $data->id,
                "positionName"   => $data->position_name . '',
                "departmentId"   => $data->department_id,
                "status"         => $data->status==1?'有效':'无效',
                "createdAt"      => !empty($data->created_at)?$data->created_at . '':'',
                "updatedAt"      => !empty($data->updated_at)?$data->updated_at:'',
            ];
        }

        return $list;
    }


    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getPosition($param)
    {
        if(!isset($param['positionId'])||empty($param['positionId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = AdminUserModel::getPosition($param);

        return [
            "code" => 200,
            "data" => self::formatPosition($result),
        ];
    }

    public static function addPosition($memberInfo, $param = [])
    {
        //验证
        if(!isset($param['positionName'])||empty($param['positionName']))
        {
            return ['code' => 400, 'msg' => '名称不能为空！'];
        }

        if(!isset($param['departmentId'])||empty($param['departmentId']))
        {
            return ['code' => 400, 'msg' => '部门不能为空！'];
        }

        $positionInfo = AdminUserModel::getPosition(['positionName'=>$param['positionName']]);

        if($positionInfo)
        {
            return ['code' => 400, 'msg' => '此职位名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();
        $created_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['position_name']           = $param['positionName'];
        $data['department_id']           = $param['departmentId'];
        $data['status']         = 1;
        $data['created_at']     = $created_at;

        //添加
        $result = AdminUserModel::addPosition($data);

        if($result)
        {
            $logMessage = '新增职位信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 1, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '职位添加成功！'];
        }else{
            return ['code' => 400 , 'msg' => '职位添加失败！'];
        }
    }

    public static function upPosition($memberInfo, $param = [])
    {
        if(!isset($param['positionId'])||empty($param['positionId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        if(!isset($param['departmentId'])||empty($param['departmentId']))
        {
            return ['code' => 400, 'msg' => '部门不能为空！'];
        }

        //验证
        if(!isset($param['positionName'])||empty($param['positionName']))
        {
            return ['code' => 400, 'msg' => '名称不能为空！'];
        }


        $PositionInfo = AdminUserModel::getPosition(['positionName'=>$param['positionName'],'noPositionId'=>$param['positionId']]);

        if($PositionInfo)
        {
            return ['code' => 400, 'msg' => '此职位名称已经有了'];
        }

        //当前时间戳
        $timestamp  = time();

        $updated_at = self::formatTimeStamp($timestamp);

        //数据拼接
        $data['position_name']         = $param['positionName'];

        $data['department_id']           = $param['departmentId'];

        $data['updated_at']   = $updated_at;

        $where=['id'=>$param['positionId']];

        //添加
        $result = AdminUserModel::upPosition($where,$data);

        if($result)
        {
            $logMessage = '修改职位信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 2, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '职位更新成功！'];
        }else{
            return ['code' => 400 , 'msg' => '职位更新失败！'];
        }
    }

    public static function delPosition($memberInfo, $param = [])
    {
        if(!isset($param['positionId'])||empty($param['positionId']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $PositionInfo = AdminUserModel::getPosition(['positionId'=>$param['positionId']]);

        if(empty($PositionInfo))
        {
            return ['code' => 400, 'msg' => '此部门不存在'];
        }

        //当前时间戳
        $timestamp  = time();

        $data['deleted_at']         = $timestamp;

        $data['status']          = 0;

        $where=['id'=>$param['positionId']];

        //添加
        $result = AdminUserModel::upPosition($where,$data);

        if($result)
        {
            $logMessage = '删除职位信息';
            AdminOperationLogService::insertAdminOperation(
                $memberInfo['adminUserId'], 3, json_encode($data), $logMessage, $result
            );
            return ['code' => 200 , 'msg' => '职位删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '职位删除失败！'];
        }
    }


    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getDepartmentsMenu($param)
    {
        $result    = AdminUserModel::getDepartments($param);

        return [
            "code" => 200,
            "data" => self::formatDepartments($result),
        ];
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getPositionsMenu($param)
    {
        $result    = AdminUserModel::getPositions($param);

        return [
            "code" => 200,
            "data" => self::formatPositions($result),
        ];
    }


    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getLevelPowers($param)
    {

        $result    = AdminUserModel::getPowers($param);

        return [
            "code" => 200,
            "data" => self::formatPowers($result),
        ];
    }

    /**
     * 获取
     * @param Array
     * @return Array
     */
    public static function getUserLevelPowers($param)
    {

        $result    = AdminUserModel::getUserPowers($param);

        return [
            "code" => 200,
            "data" => self::formatPowers($result),
        ];
    }

    /**
     * @Notes: 获取当前登陆人的角色类型
     * @Author: Yzk
     * @Date:2020/3/7 17:44
     * @param $memberInfo
     * @param array $param
     * @return array
     */
    public static function getAdminUserRole($memberInfo,$param=[])
    {
        if(empty($memberInfo))
        {
            return ['code' => 400, 'msg' => '用户信息错误！'];
        }

        if(empty($memberInfo['userRole']))
        {
            return ['code' => 400, 'msg' => '用户无角色！'];
        }

        $roles =AdminUserModel::getRoles(['roleIds'=>$memberInfo['userRole']]);

        if(empty($roles))
        {
            return ['code' => 400, 'msg' => '无角色信息！'];
        }

        $roleType = 0;
        foreach($roles as $key=>$value)
        {

            if ($value->role_name == '客服专员') {
                $roleType = 1;
            }

            if ($value->role_name == '客服主管') {
                $roleType = 2;
            }

            if ($value->role_name == '质检') {
                $roleType = 3;
            }
        }

        return ['code' => 200 , 'msg' => 'success' , 'roleType' => $roleType];

    }





































}