<?php

namespace App\Service\Stall;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
use App\Service\Community\ExclusiveMangerService;
use App\Model\Client\StallRobotMemberRelationModel;


class StallGoodsService {

    //1自营  2 京东  3 拼多多
    private static $status = [
        'self' => 1,
        'jd'   => 2,
        'pdd'  => 3
    ];
    private static $url = "https://gateway.yueshang.shop/private/v1";
    private static $JDh5host = "https://single.yuetao.group/page/JD.html?";
    private static $PDDh5host = "https://single.yuetao.group/page/PDD.html?";
    private static $ZYh5host = "https://single.yuetao.group/good-detail.html?";

    private static $robotUrl = "http://robot.yueshang.store";



    /**
     * 获取助理推送列表
     */
    public static function getAssistantList($param){
        $param['page'] = isset($param['page']) ? $param['page'] : 1; //默认第一页
        $param['pageNum'] = isset($param['pageNum']) ? $param['pageNum'] : 10;//默认每页10条
        $status = StallRobotMemberRelationModel::getPushStatus($param['mid']);
        if(empty($status)){
            $status = 0 ;
        }else{
            $status = $status[0]['push_status'];
        }
        $data = StallAssistantModel::getProductList($param);
        foreach ($data as $k =>$v){

            $goods = StallRobotMemberRelationModel::getGoodsInfoByGoodsId(['product_id'=>$params['goodsId'],'product_type'=>$params['goods_type']]);
            $data[$k]['goods_name'] = $goods['product_name'];
            $data[$k]['goods_id'] = (string)$goods['product_id'];
            $data[$k]['goods_type'] = $goods['product_type'];
            $data[$k]['sale_desc'] = $goods['market_price']/100;
            $data[$k]['buy_desc'] = $goods['buy_desc']/100;
            $data[$k]['goods_image'] = $goods['goods_img'];
            //$data[$k]['goods_coupon'] = isset($goods['coupon']) ? $goods['coupon'] : 0;
            if($v['push_status'] == 1){
                $data[$k]['end_time'] = self::hours_min(time(),$v['push_end_time']);
            }
        }

        sort($data);

        $newData['data'] = $data;
        $newData['push_status'] = $status;
        return responseJson(200 , 'success' , $newData);
    }

    /**
     * 获取单个商品推送状态
     */
    public static function getPushStatusByGoodsId($param){
        $param['acticity_id'] = empty($param['acticity_id']) ? 0 :$param['acticity_id'];
        $param['goods_sku_id'] = empty($param['goods_sku_id']) ? 0 :$param['goods_sku_id'];
        $result = StallAssistantModel::getPushStatusByGoodsId($param);
        $data['push_status']  = 0;
        $data['end_time'] = "";
        if(!empty($result) && $result[0]['push_status'] == 1){
            $data['push_status'] = 1;
            $data['end_time'] = self::hours_min(time(),$result[0]['push_end_time']);
        }
        return $data;
    }


    /**
     * 返回分享方式
     * @param $type
     * @return array|mixed
     */
    public static  function getShareType($type)
    {
        $returnData=array();
        if($type==1||$type==3){
            $data=array(
                1=>array(
                    array('id'=>1,'title'=>'保存图片'),
                    array('id'=>2,'title'=>'微信'),
                    array('id'=>3,'title'=>'朋友圈'),
                    array('id'=>4,'title'=>'微信小程序'),
                ),
                3=>array(
                    array('id'=>4,'title'=>'微信小程序'),
                    array('id'=>1,'title'=>'保存图片'),
                )
            );
            $returnData = $data[$type];
        }
        return  $returnData;
    }

    /**
     * 时间差改成小时
     */
    public static function hours_min($start_time,$end_time){
        if (strtotime($start_time) > strtotime($end_time)) list($start_time, $end_time) = array($end_time, $start_time);
        $sec = $start_time - $end_time;
        $sec = round($sec/60);
        $hours_min = floor($sec/60);
        if($hours_min <1){
            $hours_min = "最新推送";
        }elseif($hours_min>0 && $hours_min <24){
            $hours_min = $hours_min."小时前推送";
        }else{
            $hours_min ="一天前推送";
        }
        return $hours_min;
    }

    /**
     * 判断是否有专属机器人
     * @access
     * @param $params
     * @return array
     * @throws \Exception
     * @version 1.0
     * @author liuwenhao
     */
    public static function isExclusiveRobot($params){

        $requestData = [
            'source' => config("stall.robot_source"),
            'mid'    => $params['mid']
        ];

        $params['source'] = empty($params['source'])?'7323ff1a23f0bf1cda41f690d4089353':$params['source'];
        $params['mid'] = empty($params['mid'])?0:$params['mid'];


        $result =  ExclusiveMangerService::isDedicatedRobot($params);

        if(!empty($result)){

            // $data = StallRobotMemberRelationModel::getFirstInfo(['mid' => $params['mid']]);

            // if(empty($data)){
            //     $insertData = [
            //         'mid'         => $params['mid'],
            //         'push_status' => 1
            //     ];

            //     $insertData['mid']          = $params['mid'];
            //     $insertData['push_status']  = $result['is_send_by_ass'];
            //     $insertData['create_time']  = date("Y-m-d H:i:s");

            //     //新增一条记录
            //     StallRobotMemberRelationModel::insertData($insertData);
            // }

            return responseJson(200 , 'success' , ['status' => 1]);
        }else{
            return responseJson(200 , 'success' , ['status' => 0]);
        }
    }


    /**
     * 关闭机器人推送
     * @access
     * @param $params
     * @return array
     * @throws \Exception
     * @version 1.0
     * @author liuwenhao
     */
    public static function closeAssistantPush($params){

        $requestData = [
            'source'        => config('stall.robot_source'),
            'mid'           => $params['mid'],
            'send_status'   => 1 // 1 为关闭状态
        ];

        $url = "/api/community/dedrobot/upsendstatus";

        $result = self::CurlPostData($requestData , $url);

        if($result['code'] == 200){
            //修改地摊方推送状态
            $where['mid'] = $params['mid'];
            $updateRes = StallRobotMemberRelationModel::updatePushStatus($where , ['push_status' => 1]); // push_status 1 为关闭状态
            Log::info("update-push_status:" , [$updateRes]);
            return responseJson(200 , 'success');
        }else{
            return responseJson(400 , 'filed');
        }
    }


    /**
     * 开启机器人推送
     * @access
     * @param $params
     * @return array
     * @throws \Exception
     * @version 1.0
     * @author liuwenhao
     */
    public static function openAssistantPush($params){

        $requestData = [
            'source'        => config('stall.robot_source'),
            'mid'           => $params['mid'],
            'senf_status'   => 0 //  0 为开启状态
        ];

        $url = "/api/community/dedrobot/upsendstatus";

        //开启推送
        $result = self::CurlPostData($requestData , $url);

        if($result['code'] == 200){
            //修改地摊方推送状态
            $where['mid'] = $params['mid'];
            $updateRes = StallRobotMemberRelationModel::updatePushStatus($where , ['push_status' => 0]); // push_status 0 为开启状态
            Log::info("update-push_status:" , [$updateRes]);

            return responseJson(200 , 'success');
        }else{
            return responseJson(400 , 'fail');
        }
    }

    /**
     * 向机器人推送商品
     * @access
     * @param $params
     * @return array
     * @throws \Exception
     * @version 1.0
     * @author liuwenhao
     */
    public static function addAssistantPush($params){

        //判断是否有专属机器人
        $isExclusiveRobot = self::isExclusiveRobot(['mid' => $params['mid']]);

        if(empty($isExclusiveRobot) || $isExclusiveRobot['code'] != 200){
            return responseJson(400 , '没有专属机器人');
        }

        $memberRobotData = StallRobotMemberRelationModel::getFirstInfo(['mid' => $params['mid']]);

        //判断是否为关闭推送
        if(empty($memberRobotData) || $memberRobotData['push_status'] == 1){
            return responseJson(400 , '推送状态为关闭，请先打开推送按钮～');
        }

        //判断该商品是否已处于待推送状态
        $goodsData  = StallRobotMemberRelationModel::getWhereProduct(['mid' => $params['mid'] , 'goods_id' => $params['goodsId'] , 'push_status' => 0 , 'delete_time' => 0]);

        if(count($goodsData) > 0){
            return responseJson(400 , '该商品已经加入助理了，待推送中～');
        }

        $goodsCount = StallRobotMemberRelationModel::getCount(['mid' => $params['mid'] , 'push_status' => 0 , 'delete_time' => 0]);
        if($goodsCount > 12){
            return responseJson(400 , '最多能添加12个～');
        }


        $codeNumber                 = StallRobotMemberRelationModel::getGroupOwerIdCodeNum($params['mid']);
        $codeNum= empty($codeNumber) ? 'HB0717119167362' : $codeNumber;

        //查询商品信息

        $sendTime = self::getPushTime(['mid' => $params['mid']]);

        $requestData['mid']       = $params['mid'];
        $requestData['source']    = config("stall.robot_source");
        $requestData['send_time'] = $sendTime;
        $requestData['product_id']= $params['goodsId'];
        $baseRegister = Config("CacheKeyVariable.baseRegister");
        $baseUrl = Config("CacheKeyVariable.baseUrl");

        $goodsInfo = StallRobotMemberRelationModel::getGoodsInfoByGoodsId(['product_id'=>$params['goodsId'],'product_type'=>$params['goods_type']]);

        if(!empty($goodsInfo)){

            $requestData['content']  = $goods['goods_name'];
            $requestData['pic_url']  = $goods['goods_image'];
            switch ($params['goods_type']) {

                case 1: //自营商品

                    $requestData['link_url'] = ProductService::Ordinarylink(['gid'=>$goodsInfo['product_id'],'sku_id'=>$params['sku_id']]);

                    break;
                case 2: //京东

                    $requestData['link_url'] = ProductService::JdShortLink(['gid'=>$goodsInfo['product_id']]);

                    break;
                case 3: //拼多多

                    $requestData['link_url'] = ProductService::pinShortLink(['gid'=>$goodsInfo['product_id']]);

                    break;

            }



        }

        //向机器人推送
        Log::info("addSendData===robot" , [$requestData]);
        $result = ExclusiveMangerService::addSendDataToRedis($requestData);
        Log::info("addSendData===robot--result:" , ['result' => $result]);
        if(empty($result) || $result['code'] != 200){
            return responseJson(400 , 'fail');
        }
        //推送成功，储存到数据库
        $insertData = [
            'mid' => $params['mid'],
            'goods_id' => $params['goodsId'],
            'goods_sku_id' => isset($params['product_sku_id']) ? $params['product_sku_id'] : 0,
            'goods_type' => $params['goods_type'],
            'activity_id' => isset($params['activity_id']) ? $params['activity_id'] : 0,
            'random_str' => $result['data']['flag'],
            'push_status' => 0, //默认待推送
            'push_start_time' => $sendTime,
            'push_end_time' => 0,
            'create_time' => date("Y-m-d H:i:s"),
            'delete_time' => 0,
        ];

        $insertResult = StallAssistantModel::insertProduct($insertData);

        if(!$insertResult){
            return responseJson(400 , '入库失败');
        }


        $key = "goods_id=" . $params['goodsId'] . "&mid=" . $params['mid'] . "&sku_id=" . $insertData['goods_sku_id'];

        Redis::set($key , 1); //设置添加助理状态

        return responseJson(200 , 'success');
    }


    /**
     * 获取推送时间
     * @access
     * @param $params
     * @return false|float|int
     * @author liuwenhao
     * @version 1.0
     */
    public static function getPushTime($params){

        $goodsData  = StallRobotMemberRelationModel::getFirstLastProduct(['mid' => $params['mid'] , 'push_status' => 0 , 'delete_time' => 0]);

        $startTime = strtotime(date("Y-m-d 06:00:00"));

        $time = time();

        $endTime = strtotime(date("Y-m-d 22:00:00"));


        if(empty($goodsData)){  //没有未推送的订单

            if($startTime < $time && $time < $endTime){ // 时间在可发送区间
                return $time + 5 * 60; //当前时间+五分钟
            }

            if($time < $startTime){ // 时间小于早晨6点
                return $startTime + 5 * 60;
            }

            if($time > $endTime){ // 当前时间大于晚上10点
                return $endTime + 8 * 60 * 60 + 5 * 60;  //时间为第二天早晨6点+5分钟
            }
        }

        $pushTime = $goodsData[0]['push_start_time']; //最后一个未推送的商品的推送时间


        Log::info("sendGoods--time" , [$pushTime]);
//        $pushEndTime = strtotime(date("Y-m-d" , $pushTime)) + 22 * 60 * 60; // 最后一个未推送商品推送时间当天的22点
        $pushEndTime = strtotime(date("Y-m-d" , time())) + 22 * 60 * 60; // 推送时间当天的22点
        Log::info("sendGoods--time" , [$pushEndTime]);

        //大于8点小于10点
        if($pushTime + 7200 < time()){
             $pushTime = time() + 10*60;
        }
        if($pushTime + 7200 > $pushEndTime){ // 如果推送时间大于晚上十点
            return $pushEndTime + 8 * 60 * 60 + 5 * 60;
        }
        return $pushTime + 7200;  //最后一个推送时间+两个小时
    }


    /**
     * 删除推送商品
     * @access
     * @param $params
     * @return array
     * @throws \Exception
     * @version 1.0
     * @author liuwenhao
     */
    public static function delAssistantPush($params){

        $result = StallRobotMemberRelationModel::getFirstProduct(['mid' => $params['mid'] , 'goods_id' => $params['goodsId'] , 'id' => $params['id']]);

        if(empty($result)){
            return responseJson(400 , '没查到对应的商品');
        }

        $data['mid'] = $params['mid'];
        $data['source'] = $params['source'];
        $data['random_str'] = $result['random_str'];

        Log::info("delAssistantPush-request" , [$result]);
        $result= ExclusiveMangerService::delSendDataRedis($data);
        Log::info("delAssistantPush" , [$result]);

        if($result['code'] == 200){

            //删除推送商品(库里状态)
            $delStatus = StallRobotMemberRelationModel::delProduct(['random_str' => $data['random_str']]);
            if(!$delStatus){
                return responseJson(400 , '删除失败');
            }
            return responseJson(200 , 'success');
        }else{
            return responseJson(400 , 'fail');
        }

    }


    /**
     * 推送回调
     * @access
     * @param $params
     * @return array
     * @author liuwenhao
     * @version 1.0
     */
    public static function callbackAssistantPush($params){

        $where['random_str'] = $params['flag'];

        $data = [
            'push_end_time' => time(),
            'push_status'   => 1
        ];

        //修改为推送成功状态
        $updateStatus = StallAssistantModel::updateProductStatus($where , $data);

        if($updateStatus){

            //查询当前时间之前是，否有未推送订单
            $noPushList = StallAssistantModel::findNoPushList();

            //如果存在未推送成功的商品，循环将未推送成功的商品设置为超时状态
            if(count($noPushList) > 0){
                foreach ($noPushList as $val) {
                    StallAssistantModel::setStatusTimeOut(['id' => $val['id']]);
                }
            }

            return responseJson(200 , 'success');
        }else{
            return responseJson(400 , 'fail');
        }
    }


    /**
     * 判断是否加入助理
     * @access
     * @param $params
     * @return array
     * @author liuwenhao
     * @version 1.0
     */
    public static function isAddAssistant($params){

        if(!isset($params['mid']) || empty($params['mid'])){
            return responseJson(400 , 'fail' , ['isAddAssistant' => 0]);
        }

        if(!isset($params['productId']) || empty($params['productId'])){
            return responseJson(400 , 'fail' , ['isAddAssistant' => 0]);
        }

        $key = "goods_id=" . $params['productId'] . "&mid=" . $params['mid'] . "&sku_id=" . $params['productSkuId'];

        $assistant = Redis::get($key);   // 0 没有加助理 1 添加了助理
        if(!isset($assistant) || empty($assistant)){
            $assistant = 0;
        }
        return responseJson(200 , 'success' , ['isAddAssistant' => $assistant]);
    }



}