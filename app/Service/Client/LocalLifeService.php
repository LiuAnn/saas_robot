<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/21
 * Time: 下午05:43
 * Describe: 本地生活
 */

namespace App\Service\Client;

use App\Models\LocalLife;

class LocalLifeService
{

    /**
     * 本地生活列表
     *
     * @param $platform_identity
     * @return array
     */
    public static function getList($platform_identity)
    {
        $data = LocalLife::where('platform_identity', $platform_identity)->get()->toArray();
        return $data;
    }

    /**
     * 本地生活详情
     *
     * @param string $id
     * @param string $platform_identity
     * @return array
     */
    public static function getOne($id = '', $platform_identity = '')
    {
        if (empty($id) || empty($platform_identity)) {
            return [];
        }
        $data = LocalLife::where('platform_identity', $platform_identity)->find($id);
        return $data ?? [];
    }

    public static function share($mid = '')
    {
        return [];
    }
}