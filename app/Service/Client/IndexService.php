<?php

namespace App\Service\Client;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Model\Client\IndexModel;
use App\Model\Admin\GroupModel;
use App\Service\Community\ProductService;
use App\Model\Community\ProductModel;

class IndexService
{

    private static $url = "https://gateway.yueshang.shop/private/v1";

    /**
     * 小悦客户端首页
     * @param array $param
     * @param $memberInfo
     * @return array
     */
    public static function getIndexInfo($param)
    {   
        //首页banner背景图
        $localMonthStart = date('Y-m-01', strtotime(date("Y-m-d")));
        $backGroundData = [
            'backGroundImg'=>'',
            'title'=>'',
            'localMonth'=>date("m月",time()),
            'localMonthStart'=>$localMonthStart,
            'localMonthEnd'=>date('Y-m-d', strtotime("$localMonthStart +1 month -1 day"))

        ];
        //用户本月收益
        $monthlyIncome = 0;

        //用户渠道群数量
        $groupInfo = [];

        if(isset($param['mobile'])&&!empty($param['mobile'])){

            //获取用户本月收益
            $monthlyIncome = IndexModel::getMemeberProfitByMobile(['startTime'=>date('Y-m-01', strtotime(date("Y-m-d"))),'mobile'=>$param['mobile']]);
            //获取用户各个渠道的群数量
            $groupInfo = IndexModel::getAllGroupInfoByMobile($param['mobile']);
        }

        return [
            'backGroundData'=>$backGroundData,
            'logoUrl'=>"",
            'monthlyIncome'=>$monthlyIncome,
            'groupInfo'=>$groupInfo
        ];
    }


    /**
     * 小悦客户端当前渠道群信息
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function getMemberChannelInfo($param){

        if(!isset($param['mobile'])||empty($param['mobile'])){

            return ['code'=>400,'msg'=>'手机号不能为空'];
        }

        $param['channel_identity'] = !isset($param['platform_identity'])?"7323ff1a23f0bf1cda41f690d4089353":$param['platform_identity'];

        return  self::getGroupListByMobile($param);

        $myMonthlyIncome = self::getMyMonthlyIncome($param);

        $myOrderInfo = self::getMyOrderMapInfo($param);

    }


    /**
     * 获取群主订单分析统计图
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function getMyOrderMapInfo($param){

        if(!isset($param['mobile'])||empty($param['mobile'])){

            return ['code'=>400,'msg'=>'手机号不能为空'];
        }

        $param['platform_identity'] = !isset($param['platform_identity'])?"7323ff1a23f0bf1cda41f690d4089353":$param['platform_identity'];

        $data = [];

        $roomIds = IndexModel::getMemberRoomIds($param);

        if($roomIds==''){

            return [];
        }

        $totalOrder = IndexModel::getOrderCount($roomIds);

        $totalOrderCount = $totalOrder['orderCount'];

        $data['totalOrderCount'] = $totalOrderCount;  //总订单数
        $data['totalOrderAmount'] = $totalOrder['orderAmount']; //总订单金额
        $data['buyMemberCount'] = IndexModel::getBuyMemberCount($roomIds);
        $data['goodTypeList'] = [];
        $data['forLastOrder'] = [];
        if($totalOrderCount>0){

            $getOrderInfoByGoodType = IndexModel::getOrderInfoByGoodType($roomIds);

            foreach ($getOrderInfoByGoodType as $key => $value) {
                
                $data['goodTypeList'][$key]['goodTypeName'] = getGoodTypeNameByGoodType($value['good_type']);
                $data['goodTypeList'][$key]['orderCount'] = $value['count_1'];
                $data['goodTypeList'][$key]['proportion'] = $value['count_1']/$totalOrderCount*100;
            }


            $getLastOrderInfo = IndexModel::getLastOrderInfo($roomIds);

            $data['forLastOrder']['goodsName'] = $getLastOrderInfo['goods_name'];
            $data['forLastOrder']['goodsPayTime'] = $getLastOrderInfo['pay_time'];

        }

        return $data;
    }


    /**
     * 获取群主群列表
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function getGroupListByMobile($param){

        $groupList = IndexModel::getGroupListByMobile($param);

        $data = [];

        foreach ($groupList as $k => $v) {
           
            $data[$k]['name'] = $v['group_name'];//群名称
            $data[$k]['robotName'] = $v['robot_wx_name'];//机器人名称
            $data[$k]['orderNumber'] = $v['order_number'];//群订单数量
            $data[$k]['orderAmount'] = $v['order_amount']/100;//总GMV
            $data[$k]['browseGoodsNumber'] = $v['browse_goods_number'];//群内用户浏览商品总次数
            $data[$k]['groupCreatedAt'] = $v['group_created_at'];//群创建时间
            $data[$k]['group_male_proportion'] = $v['group_male_proportion'];//男性占比
            $data[$k]['group_female_proportion'] = $v['group_female_proportion'];//女性占比

        }


        return $data;

    }



    /**
     * 获取群主收益群线图
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function getMyMonthlyIncome($param){

        $data = [];


        if(!isset($param['mobile'])||empty($param['mobile'])){

            return ['code'=>400,'msg'=>'手机号不能为空'];
        }

        $para['mobile'] = $param['mobile'];
        $para['startTime'] = isset($param['startTime'])?$param['startTime']:'';
        $para['endTime'] = isset($param['endTime'])?$param['endTime']:'';

        if($param['platform_identity']=="7323ff1a23f0bf1cda41f690d4089353"){

          //用户当前时间总收益
          $data['totalIncome'] = IndexModel::getMemeberProfitByMobile($para);
          $data['incomeInfo'] = IndexModel::getMemeberProfitInfo($para);
          //用户当前时间总分佣所得金额
          $data['totalCommission'] = IndexModel::getMemeberProfitByMobile($para,100);
          $data['commissionInfo'] = IndexModel::getMemeberProfitInfo($para,100);
          //用户当前时间总奖励金额
          $data['totalReward'] = IndexModel::getMemeberProfitByMobile($para,6);
          $data['rewardInfo'] = IndexModel::getMemeberProfitInfo($para,6);
          //用户当前时间可提现金额
          $para['status'] = 1;
          $data['totalWithdrawal'] = IndexModel::getMemeberProfitByMobile($para);
          $data['withdrawalInfo'] = IndexModel::getMemeberProfitInfo($para);


        }


        return $data;

    }


    /**
     * 判断群主是否为专属机器人
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function isExclusiveRobot($param){

        $getChannelData = IndexModel::getChannelData();

        foreach ($variable as $key => $value) {
            # code...
        }

    }


    /**
     * 根据用户手机号获取各渠道用户mid
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function getMemberMidForChannelByMid($param){

        $getChannelData = IndexModel::getChannelData();

        foreach ($variable as $key => $value) {

            switch ($value['platform_identity']) {
                case '7323ff1a23f0bf1cda41f690d4089353':
                    //悦淘

                    break;
                case '89dafaaf53c4eb68337ad79103b36aff':
                    //大人
                    break;
                case '183ade9bc5f1c4b52c16072362bb21d1':
                    //迈图
                    break;
                case '75f712af51d952af3ab4c591213dea13':
                    //直订
                    break;
                case 'c23ca372624d226e713fe1d2cd44be35':
                    //邻居团
                    break;
                default:
                    # code...
                    break;
            }
        }

    }


    /**
     * 根据群主自定义标签自动匹配商品
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function getRecommendGoodsByGroupTags(){

        $pageSize = 10;
    
        for ($i=1; $i < 11; $i++) { 
            $param['page'] = $i;
            $param['pageSize'] = $pageSize;

            $getGroupTagsList = GroupModel::getGroupTagsList($param);

            if(!empty($getGroupTagsList)){

                foreach ($getGroupTagsList as $k => $v) {
                    
                    if($v['jd_category_id']>0){
                        //获取京东推荐商品
                        self::addJdRecommendGoods($v);
                    }
                    if($v['pdd_category_id']>0){
                    //获取拼多多推荐商品
                        self::addPddRecommendGoods($v);
                    }

                    if($v['yt_category_id']>0){
                    //获取悦淘自营推荐商品
                        self::addYtRecommendGoods($v);
                    }
                }

            }else{

                return ['code'=>400,'msg'=>'结束匹配'];
            }
        }
     


    }

    /**
     * 根据商品分类拉取京东推荐商品
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function addJdRecommendGoods($param){

        $data = [];

        $data['cid1'] = $param['jd_category_id'];
        $data['page'] = 1;
        $data['pageSize'] = 5;

        $result = curlPostCommon(self::$url . "/open/v2/jd/getCategoryGoodsList", $data);

        $classData = json_decode($result, true);
        $goodsList = [];
        if (!empty($classData) && $classData['code'] == 200 && !empty($classData['data'])) {
            foreach ($classData['data'] as $k => $list) {
                $priceData = ProductService::goodsPrice($list['priceInfo']['price'], empty($list['priceInfo']['lowestPrice']) ? '' : $list['priceInfo']['lowestPrice'], empty($list['priceInfo']['lowestCouponPrice']) ? '' : $list['priceInfo']['lowestCouponPrice']);
                $priceSave = ProductService::getJdEarnricePrice($list['commissionInfo']);
                $goodsSuggestImage = [];
                foreach ($list['imageInfo']['imageList'] as $v) {
                    $goodsSuggestImage[] = $v['url'];
                }
                $goodsList["product_name"]= $list['skuName'];
                $goodsList['product_id'] = $list['skuId'];
                $goodsList['sku_id'] = $list['skuId'];
                $goodsList['market_price'] = $priceData['original_price']*100;//原价
                $goodsList['product_price'] = $priceData['jd_price']*100;//券后价lowestCouponPrice
                $goodsList['goods_img'] = $goodsSuggestImage[0];//主图
                $goodsList['commission_rate'] = $priceSave['share_price']*100;//返利
                $goodsList['group_tag_id'] = $param['id'];
                $goodsList['product_type'] = 2;
                $goodsList['create_time'] = date("Y-m-d H:i:s",time());

                IndexModel::insertRecommendGoods($goodsList);
            }
        }  
    }


    /**
     * 根据商品分类拉取拼多多推荐商品
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function addPddRecommendGoods($param){

        $data = [];

        $data['id'] = $param['pdd_category_id'];
        $data['page'] = 1;
        $data['page_size'] = 10;

        $result = curlPostCommon(self::$url . "/pdd/v1/queryGoodsList", $data);

        $classData = json_decode($result, true);
        $goodsList = array();
        if (!empty($classData) && $classData['code'] == 200 && !empty($classData['data'])) {
            foreach ($classData['data'] as $k => $list) {
                $docField = $list['info'];
                if (empty($docField)) {
                    break;
                }

                $goodsList["product_name"]= $docField['goods_name'];
                $goodsList['product_id'] = $docField['goods_id'];
                $goodsList['sku_id'] = $docField['goods_id'];
                $goodsList['market_price'] = $docField['min_normal_price']*100;//原价
                $goodsList['product_price'] = $docField['jh_price']*100;//券后价lowestCouponPrice
                $goodsList['goods_img'] = $docField['goods_thumbnail_url'][0];//主图
                $goodsList['commission_rate'] = $docField['tk']*100;//返利
                $goodsList['group_tag_id'] = $param['id'];
                $goodsList['product_type'] = 3;
                $goodsList['create_time'] = date("Y-m-d H:i:s",time());

                IndexModel::insertRecommendGoods($goodsList);

                if($k==4)return true;

            }
        }  

    }


    /**
     * 根据商品分类拉取悦淘推荐商品
     * @param array $param
     * @param 。。。
     * @return array
     */
    public static function addYtRecommendGoods($param){

        $data = [];

        //$data['bandId'] = $param['yt_category_id'];

//        $param['is_live'] = 0;//非直播商品
        $goodsData = ProductModel::getGoods(1, 5, $data);
        $classData = json_decode(json_encode($goodsData),true);

        $goodsList = array();
        if (!empty($classData)) {
            foreach ($classData as $k => $good) {

                $sku = ProductModel::getNewSkuId($good['id']);

                $mall_distribution = ProductService::getDistribution($good['distribution_id'],$sku);

                $goods_img = ProductModel::getConverImage($good['id']);

                $goodsList["product_name"]= $good['name'];
                $goodsList['product_id'] = $good['id'];
                $goodsList['sku_id'] = $sku->skuId;
                $goodsList['market_price'] = empty($sku->price) ? 0 : $sku->price;//原价
                $goodsList['product_price'] = empty($sku->vip_price) ? 0 : $sku->vip_price;//券后价lowestCouponPrice
                $goodsList['goods_img'] = $goods_img;//主图
                $goodsList['commission_rate'] = $mall_distribution*100;//返利
                $goodsList['group_tag_id'] = $param['id'];
                $goodsList['product_type'] = 1;
                $goodsList['create_time'] = date("Y-m-d H:i:s",time());

                IndexModel::insertRecommendGoods($goodsList);
            }
        }  

    }



    /**
     * @param $memberId
     * @return string
     */
    public static function makeToken($mobile)
    {
        $key        = "data_robot_client_token_mobile_". $mobile;
        $token      = self::getToken($mobile);

        if(!empty($token)) {
            $expire  = 3600*3;
            Redis::expire($key, $expire);
            return $token;
        }

        $time       = time();
        $tokenStr   = "{$mobile}.{$time}";
        $token      = md5($tokenStr);
        $expire     = 3600*3;
        Redis::set($key, $token);
        Redis::expire($key, $expire);
        return $token;
    }

    /**
     * @param $mobile
     * @return
     */
    public static function getToken($mobile)
    {
        $key  = "data_robot_client_token_mobile_". $mobile;
        return Redis::get($key);
    }


}