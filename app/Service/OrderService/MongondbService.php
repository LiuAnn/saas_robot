<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/6/7
 * Time: 11:40
 */

namespace App\Service\OrderService;

use App\Model\RobotOrder\MondodbModel;

class MongondbService
{

    /**
     * @param $param
     */
    public static function mongoList($param)
    {
        return MondodbModel::mongoList($param);
    }

    /**
     * @param $param
     */
    public static function mongoAdd($param)
    {

        return MondodbModel::mongoAdd($param);
    }

    /**
     * @param $param
     */
    public static function mongoEdit($param)
    {

        return MondodbModel::mongoEdit($param);
    }

    /**
     * @param $param
     */
    public static function mongoDel($param)
    {
        return MondodbModel::mongoDel($param);
    }

}