<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/15
 * Time: 15:01
 */

namespace App\Service\OrderService;

use App\Model\RobotOrder\RobotModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
class PayUnitService
{


    /**
     * 获取支付参数
     * @return array
     */
    public function pay()
    {
        $tradeType = '';
        $payChannel = 0;
        if($this->payType == 1) {
            $tradeType = "WX_APP";
        }else if($this->payType  == 2){
            $tradeType = "ALI_APP";
        }
        $payData = [
            "orderNo"   => $this->ordersn."",
            "orderFee"  => $this->actualPrice*100,
            "productId" => $this->orderId,
            "body"      => '商城-'.$goodsTypeName.'-'.mb_substr($this->goodsBuyList[0]['goodName'],0,30,'utf-8'),
            "notifyUrl" => env("APP_URL") . "/app/mall/order/callback",
            "userId"    => $this->userId,
            "tradeType" => $tradeType, // WX_APP | ALI_APP
            "sysSource" => $tradeType == "WX_JSAPP" ? "PlayHoney" : "yuelvhuiApp",
            "payChannel" => $payChannel
        ];


        $keyJ    = md5(json_encode($payData));
        $payJson = Redis::get($keyJ);

        if(empty($payJson)) {
            $payParams = (array)PayUnitService::payParams($payData);
            if(!empty($payParams['payFormNo'])) {
                Redis::set($keyJ, json_encode($payParams));
                Redis::expire($keyJ, 1800);
            }
        } else {
            $payParams = json_decode($payJson, true);
        }
        Log::info("orderNo:{$this->ordersn} payParam:". json_encode($payParams));
        $model = MallOrderModel::getInfoByOrder($this->ordersn);
        if(count($model)){
            foreach ($model as $value){
                $parentOrdersn = $value->ordersn.'';
            }
        }else{
            $parentOrdersn = $this->ordersn.'';
        }


        return [
            "code"      => 200,
            "msg"       => "可支付订单",
            "ordersn"   => $parentOrdersn,
            "pay"       => $payParams
        ];
    }





}