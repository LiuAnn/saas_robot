<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/15
 * Time: 15:36
 */

namespace App\Service\OrderService;

use App\Model\RobotOrder\RobotModel;
use App\Service\Robot\UserOperationLogService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Service\OrderService\PayUnitService;
class OrderActionService
{
    private static $lvyueUrl  = "http://pay.yuelvhui.com";
    /**
     * 创建订单
     * @param array $param
     * @param array $member
     * @return array
     */
    public  static function  robotInitialization($param)
    {
        $service_id     = empty($param['service_id']) ? 0 : $param['service_id'];
        $mid            = empty($param['mid']) ? 0 : $param['mid'];
        $pay_amount     = empty($param['pay_amount']) ? 0 : $param['pay_amount'];
        $pay_method     = empty($param['pay_method']) ? 0 : $param['pay_method'];
        //认证支付方式
        if(!in_array($pay_method,[1,2])){
            return ['status' => 400, 'msg' => '请选择一个有效的支付方式', 'data' => []];
        }
        $serviceInfo = RobotModel::getServiceInfoById($service_id);
        //认证金额
        if(empty($serviceInfo)||$pay_amount!=$serviceInfo->service_price){
              return ['status' => 400, 'msg' => '金额有误', 'data' => []];
        }
        try {
            $ordersn = self::getOrdersn();
             $insertData=[
                'order_sn'=>$ordersn,
                'mid'=>$mid,
                'total_price'=>$serviceInfo->service_price,
                'pay_status'=>0,
                'pay_type'=>$pay_method,
                'actual_pay_price'=>$serviceInfo->service_price,
                'total_price_orig'=>$serviceInfo->service_price,
                'order_type'=>1,
                'order_source'=>1,
                'service_id'=>$service_id,
                'created_at'=>time(),
                'service_name'=>$serviceInfo->service_name,
             ];

            $returnRes = self::createOrder($insertData);
            if ($returnRes['status'] == 400) {
                return $returnRes;
            }else{


                $insertdata['mid']=$mid;
                $insertdata['ordersn']=$ordersn;
                $insertdata['service_id']=$service_id;
                $insertdata['service_name']=$serviceInfo->service_name;
                $insertdata['service_price']=$serviceInfo->service_price;
                $insertdata['service_number']=$serviceInfo->group_number;
                $insertdata['pay_status']=0;
                $insertdata['created_at']=time();
                RobotModel::insertOrderInfo($insertdata);

                $logMessage = '新增订单-订单号';
                UserOperationLogService::insertAdminOperation(
                    $mid, 1, json_encode($insertdata), $logMessage, $ordersn
                );
                return ['status' => 200, 'msg' => '订单创建成功', 'data' => ['ordersn'=>$ordersn]];
            }
        } catch (\Exception $e) {
            return ['status' => 400, 'msg' => '数据异常', 'data' => []];
        }
    }

    /**
     * 更新订单状态
     * @param $data
     */
    public static function updateOrderStatus($data)
    {
        if (empty($data["payFormNo"]) || empty($data["prepayId"])) {
            Log::info("mall pay callback data is empty");
            return;
        }

        $order   = MallOrderModel::getOrderByOrdersn($data["orderNo"]);
        $payMode = 0;
        if ($data["tradeType"] == "ALI_APP") {
            $payMode = 2;
        } else if ($data["tradeType"] == "WX_APP") {
            $payMode = 1;
        } else if ($data['tradeType'] == "WX_JSAPP") {
            if ($data['mchId'] == 158) {
                $payMode = 3;
            } else if ($data['mchId'] == 5) {
                $payMode = 7;
            } else if ($data['mchId'] == 223) {
                $payMode = 6;
            } else {
                $payMode = 5;
            }
        } else if ($data['tradeType'] == "WX_JSWEB") {
            $payMode = 10;
        } else if ($data['tradeType'] == "ALI_WAP") {
            $payMode = 9;
        }



        if (empty($order)) {
            $orderGoods = MallOrderModel::getOrderByOrderSon($data['orderNo']);
            if (empty($orderGoods)) {
                Log::info("mall pay callback ordersn:{$data['orderNo']} not found, reponseData:" . json_encode($data));
            } else {
                if ($orderGoods->pay_status != 1) {
                    $transactionNo = "";
                    if ($data["tradeType"] == "WX_APP") {
                        $transactionNo = $data["transactionNo"];
                    }
                    $orderData = [
                        "pay_status"   => 1,
                        "pay_info"     => $transactionNo,
                        "pay_form_no"  => $data["payFormNo"],
                        "actual_price" => $data["payFee"],
                        "pay_type"     => $payMode,
                        "updated_at"   => time(),
                        "pay_time"     => time()
                    ];
                    $result    = MallOrderModel::updateOrderGoods($data['orderNo'], $orderData);
                    if (!empty($orderGoods->cid)) {
                        MallOrderModel::updateOrder(['cid' => 0], $orderGoods->ordersn);
                    }

                    if (empty($result)) {
                        $jsonData = json_encode($orderData);
                        Log::info("mall pay callback data update fail,ordersn:{$data['orderNo']}---data:{$jsonData}");
                    }

                    //赠送积分
                    self::sendJifen($data["payFee"], $orderGoods->mid, $orderGoods->goods_name);

                    //增加经验
                    $levelTotalMoney = MallOrderModel::getTotalMoney($orderGoods->mid);
                    MemberLevelService::shopIncrEexperience($orderGoods->mid, $levelTotalMoney);

                    if ($orderGoods->goods_type != 5) {
                        //获取抽奖码信息
                        $codeData = [
                            'orderNo' => $data['orderNo'],
                            'channel' => 1,
                            'content' => date('Y-m-d', time()) . "购买" . $orderGoods->goods_name,
                            'mid'     => $orderGoods->mid
                        ];
                        ConsumerCodeServiceImpl::make($codeData);
                    }

                    $partner = self::getOrderThreePartner($orderGoods->order_three_partner);
                    // 发送短信通知给用户
                    $payFee = $data["payFee"] / 100;
                    $member = MemberServiceImpl::getMemberInfo($orderGoods->mid);
                    SendsmsService::messageSend($member["mobile"], "301027", [$orderGoods->goods_name, $payFee]);
                    // 抄送内部人员短信通知
                    SendsmsService::messageSend("13071100658", "301026",
                        [$member["mobile"], date("Y-m-d H:i:s", time()), "【{$partner}】" . $orderGoods->goods_name, $payFee]);
                    echo "SUCCESS";
                }
            }
        }
    }

    /**
     * 订单号生成
     */
    private static function getOrdersn() {
        return "130". time() . mt_rand(1000,9999);
    }
    /**
     * 订单插入
     * @param $redirectType
     * @return array
     */
    public static function createOrder($redirectType)
    {
        $orderId = RobotModel::insertOrder($redirectType);
        if (empty($orderId)) {
            Log::info("insert mallOrder fail data:" . json_encode($redirectType));
            return ['status' => 400, 'msg' => '创建订单失败', 'data' => []];
        }

    }
    /**
     * 获取支付相关的
     * @param array $param
     * @param array $member
     * @return array
     */
    public  static function  getPayPargrams($param)
    {
        $mid            = empty($param['mid']) ? 0 : $param['mid'];
        $order_sn       = empty($param['order_sn']) ? 0 : $param['order_sn'];
        $openId         = empty($param['openId']) ? 0 : $param['openId'];
        //认证支付方式
        $orderInfo = RobotModel::getOrderInfo($order_sn,$mid);
        if(empty($orderInfo)){
            return ['status' => 400, 'msg' => '无此订单', 'data' => []];
        }

        return  self::geyPayInfo($orderInfo,$openId);


    }




    public static function geyPayInfo($orderInfo,$openId = 0){

        Log::info(" get robot info :",[$orderInfo]);
        //微信支付
        if($orderInfo->pay_type==1){
             $tradeType = "WX_NATIVE";
             $appId     = '';
             $backUrl   = 'http://robot.yueshang.store';
            //支付宝支付
        }else if($orderInfo->pay_type==2){
            $tradeType = "ALI_QRCODE";
            $appId     = '2021001140601998';
            $backUrl   = 'http://robot.yueshang.store';
        }

        $payToken = '088d5a30bfe54b928eba8cb794846e71';

        $spbillCreateIp = \Request::getClientIp();
        $prepayBody=[
            "body"=>$orderInfo->service_name,
            "expireTime"=> (time() + 86400) * 1000,
            "feeType"=>"CNY",
            "mchId"=>"248",
            "notifyUrl"=>"http://robot-api.yueshang.store/robot/order/callback",
            "orderFee"=>$orderInfo->actual_pay_price,
            "orderNo"=> $orderInfo->order_sn,
            "productId"=>$orderInfo->service_id,
            "settlementMchId"=>"248",
            "spbillCreateIp"=>$spbillCreateIp,
            "userId"=>$orderInfo->mid,
            "version"=>"2.0",
        ];
        ksort($prepayBody);
        $signStr = "";
        foreach ($prepayBody as $key => $value) {
            $signStr .= "{$key}={$value}&";
        }
        $signStr = $signStr . "key=" . $payToken;
        $prepayBody["sign"] = md5($signStr);

        $sendData = [
            "prepayBody"    => $prepayBody,
            "tradeType"     => $tradeType,
            "sysSource"     => 'yuelvhui',
            "returnUrl"     => $backUrl,
            "openId"        => $openId,
            "appId"          => $appId,
            "payChannel"    =>"ORIGINAL"
        ];

        $dataStr = json_encode($sendData);
        $paymentUrl = self::$lvyueUrl . "/prepay";
        Log::info(" get robot pay params :",[$paymentUrl,$sendData]);

         return self::send($paymentUrl,$dataStr);

    }

    /**
     * 支付参数
     * @param $url
     * @param $data
     * @param int $type
     * @return mixed
     */
    public static function send($url, $data ,$type=1) {

        Log::info("参数是：".$data);
        // 悦淘请求头
        switch ($type){
            case 1: // 悦淘请求头规则
                $sysId  = 248;  //mchId
                $sysPwd = "088d5a30bfe54b928eba8cb794846e71";
                break;
        }
        // 获取毫秒
        $microtime  = time() * 1000;
        // 接口中的sign
        $str        =  "{$sysId}.{$microtime}.{$sysPwd}";
        $sign       = md5($str);
        $headers    = [
            "Content-Type: application/json",
            "Authorization:  Sys {$sysId}.{$microtime}.{$sign}"
        ];
      //  var_dump("Authorization:  Sys {$sysId}.{$microtime}.{$sign}");die();
        Log::info("支付请求头：".json_encode($headers));
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);    // 提交地址
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_HEADER, 0); // 设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_POST, 1); // POST

        // headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);
        $arr    = json_decode($output);
        //        Log::debug("收银台返回值：",json_decode($output,true));

        return $arr;
    }

}