<?php
namespace App\Service\OrderService;


use App\Model\Community\GroupManageModel;
use App\Model\Admin\AdminUserModel;
use App\Model\Member\MemberModel;
use App\Model\RobotOrder\RobotModel;
use App\Model\User\UserModel;
use Illuminate\Support\Facades\Log;
use App\Service\Robot\UserOperationLogService;

class GroupManageService{

    const SALT_FIGURE = 'YlhAdmin';//固定盐值


    public  static    function is_timestamp($timestamp) {
        if (!is_numeric($timestamp)) return false;
        if(strtotime(date('Y-m-d H:i:s',$timestamp)) == $timestamp) {
            return $timestamp;
        } else return false;
    }
    /*
     * 申请群审核列表
     * @param Array
     */
    public static function getGroupVerifyList($param,$member)
    {

        if (isset($param['startTime']) || isset($param['endTime'])) {
            if (!isset($param['startTime'])) {
                $param['startTime'] = $param['endTime'];
            }
            if (!isset($param['endTime'])) {
                $param['endTime'] = $param['startTime'];
            }
            $param['startTime'] = date('Y-m-d 00:00:00', $param['startTime']);
            $endTime = date('Y-m-d 23:59:59', $param['endTime']);
            //开始结束时间容错

            $param['endTime'] = $endTime;
        }
        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;
        //获取所有机器人号
        if ($member['user_type'] >= 2 ) {
            $robotWxAlias = GroupManageModel::getRobotWxid($member['mid'],$member['sup_id']);
        } else {
            $robotWxAlias = GroupManageModel::getRobotWxid($member['mid'], $member['sup_id'],1);
        }


        if (!isset($param['robot_wx']) || empty($param['robot_wx'])) {
            $wxAliasString = '';
            if (!empty($robotWxAlias)) {
                foreach ($robotWxAlias as $v) {
                    $wxAliasString .= "'" . $v->wxid . "',";
                }
                $wxAliasString = trim($wxAliasString, ',');
            }
        } else {
            $wxAliasString = "'" . $param['robot_wx'] . "'";
        }

        //待审核数量
        if (!empty($robotWxAlias)) {
            $countNumData = GroupManageModel::getNumStatus($wxAliasString, $member, $param);
            $tobesubNum = $countNumData[0]->tobesubNum;
            $pendNum = $countNumData[0]->pendNum;
            $adoptNum = $countNumData[0]->adoptNum;
            $rejectNum = $countNumData[0]->rejectNum;
            $applyTotalNum = $countNumData[0]->applyTotalNum;

            $result = GroupManageModel::getGroupVerifyList($param, $page, $pageSize, $member, $wxAliasString);
            $count = GroupManageModel::getGroupVerifyListCount($param, $member, $wxAliasString);
        } else {
            $applyTotalNum=0;
            $tobesubNum = 0;
            $pendNum = 0;
            $adoptNum = 0;
            $rejectNum = 0;
            $result = [];
            $count = 0;
        }

        return [
            "code" => 200,
            "data" => [
                "tobesubNum" => $tobesubNum,
                "pendNum" => $pendNum,
                "adoptNum" => $adoptNum,
                "rejectNum" => $rejectNum,
                "applyTotalNum" => $applyTotalNum,
                "robotAlias" => $robotWxAlias,
                "total" => $count,
                "page" => $page,
                "pageSize" => $pageSize,
                "list" => self::formatCommunityInvitationGroupList($result)
            ],
        ];
    }


    /*
     * 申请群审核列表
     * @param Array
     */
    public static function getTwiceGroupVerifyList($param,$member)
    {

        if(isset($param['startTimeV1'])&&$param['startTimeV1']!='')
        {
            $param['startTimeV1'] = strtotime(date('Y-m-d H:i:s',strtotime($param['startTimeV1'])));
        }

        if(isset($param['endTimeV1'])&&$param['endTimeV1']!='')
        {
            $param['endTimeV1'] = strtotime(date('Y-m-d H:i:s',strtotime($param['endTimeV1'])));
        }

        if (isset($param['startTimeV1']) && isset($param['endTimeV1']) &&$param['endTimeV1']==$param['startTimeV1']) {

            $param['endTimeV1'] = strtotime(date('Y-m-d  23:59:59', strtotime($param['endTimeV1'])));
        }

        $page = isset($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) ? $param['pageSize'] : 10;

        //获取所有机器人号
        $robotWxAlias=[];
        if ($member['user_type'] >= 2 ) {
            $robotWxAlias = GroupManageModel::getRobotWxAlias($member['mid'], $member['sup_id']);
        } else {
            $robotWxAlias = GroupManageModel::getRobotWxAlias($member['mid'], $member['sup_id'], 1);
        }
        $wxAliasString = '';
        if (!empty($robotWxAlias)) {
            foreach ($robotWxAlias as $v) {
                $wxAliasString .= "'" . $v->wx_alias . "',";
            }
            $wxAliasString = trim($wxAliasString, ',');
        }
        $param['robot_wx'] = $wxAliasString;

        //待审核数量
        if (!empty($wxAliasString)) {
            $countNumData = GroupManageModel::getNumStatusV2($wxAliasString, $member);
            $tobesubNum = $countNumData[0]->tobesubNum;
            $pendNum = $countNumData[0]->pendNum;
            $adoptNum = $countNumData[0]->adoptNum;
            $rejectNum = $countNumData[0]->rejectNum;

            $param['twice'] = 'twice';
            $result = GroupManageModel::getGroupVerifyListTwice($param, $page, $pageSize, $member, $wxAliasString);
            $count = GroupManageModel::getGroupVerifyListCountTwice($param, $member, $wxAliasString);
        } else {
            $tobesubNum = 0;
            $pendNum = 0;
            $adoptNum = 0;
            $rejectNum = 0;
            $result = [];
            $count = 0;
        }
        return [
            "code" => 200,
            "data" => [
                "tobesubNum" => $tobesubNum,
                "pendNum" => $pendNum,
                "adoptNum" => $adoptNum,
                "rejectNum" => $rejectNum,
                "robotAlias" => $robotWxAlias,
                "total" => $count,
                "page" => $page,
                "pageSize" => $pageSize,
                "list" => self::formatCommunityInvitationGroupListV2($result)
            ],
        ];
    }


    public static function formatCommunityInvitationGroupListV2($data)
    {

        $list = [];

        if($data)
        {
            foreach ($data as $value)
            {
                $memberInfo =   MemberModel::getMemberInfo(['mid'=>$value->mid]);

//                $adminMemberInfo =   AdminUserModel::getAdminUserInfo(['admin_user_id'=>$value->examine_mid]);

                if(!empty($memberInfo))
                {   
                    $mobile = $memberInfo->mobile;
                    $jointime = $memberInfo->jointime;
                    if(!empty($memberInfo->nickname))
                    {
                        $nickname=$memberInfo->nickname;
                    }else{
                        if(!empty($memberInfo->truename))
                        {
                            $nickname=$memberInfo->truename;
                        }else{
                            $nickname='';
                        }
                    }

                }else{
                    $nickname='';
                    $mobile = "--";
                    $jointime = 0;
                }
                $countInfo = GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$value->mid]);

                if($value->upgrade_status>1){
                    $examineMemberInfo =   MemberModel::getMemberInfo(['mid'=>$value->examine_mid]);
                }

                if($value->room_id>0){

                    $groupMemberCount = GroupManageModel::getUserNumber(0,$value->room_id);

                }else{

                    $groupMemberCount = 0;
                }

                $userIdentity =  self::getUserNowIdentity($value->mid);

                $userNextIdentity = self::getUserNextIdentity($value->mid,$userIdentity);

                $list[]=[
                    "id"                    => $value->id,
                    "mid"                   => $value->mid,
                    "mobile"                => $mobile,
                    "jointime"              => $jointime>0?date("Y-m-d H:i:s",$memberInfo->jointime):"--",
                    "nickname"              => $nickname,
                    "groupNumber"           => $value->group_number,
                    "images"                => $value->images,
                    "addtime"               => date("Y-m-d H:i:s",$value->upgrade_addtime),
                    "examineStatus"         => $value->upgrade_status,
                    "examineStatusName"     => self::getExamineStatus($value->upgrade_status),
                    "examineMember"         => !empty($examineMemberInfo->nickname)?$examineMemberInfo->nickname:'',
                    "examineMid"            => $value->upgrade_mid,
                    "examineTime"           => !empty($value->upgrade_time)?date("Y-m-d H:i:s",$value->upgrade_time):'',
                    "examineDesc"           => $value->upgrade_desc,
                    "assistantWx"           => $value->assistant_wx,
                    "tutorWx"               => $value->tutor_wx,
                    "roomId"                => $value->room_id,
                    "userIdentity"          => $userIdentity,
                    "userIdentityRemark"    => self::getExamineStatusV2($userIdentity),
                    "upgradeIdentity"          => $userNextIdentity,
                    "upgradeIdentityRemark"    => self::getExamineStatusV2($userNextIdentity),
                    "groupMemberCount"      => $groupMemberCount,
                    "directPushNum"         => !empty($countInfo)?$countInfo->shopowner_num:0,
                    "intervalPushNum"       => !empty($countInfo)?$countInfo->super_shopowner_num:0,
                    "type"                  => $value->type,
                    "number_req"                  => $value->number_req,//请求的次数
                    "form"                  => self::getGroupForm($value->type)
                ];
            }
        }

        return $list;
    }


    public static function getUserNowIdentity($mid)
    {

        $workInfo = GroupManageModel::isWork($mid);

        if($workInfo==0)
        {   

            $getUserIndenty = GroupManageModel::getUserIdentity($mid);

            $getGrowthValue = GroupManageModel::getGrowthValue($mid);

            $orderToMember = GroupManageModel::getOrderToMember($mid);

            if($getUserIndenty->member_type==0&&$getGrowthValue<200&&$getUserIndenty->card_type<1&&empty($orderToMember))
            {
                return 1;
            }

            if($getUserIndenty->member_type<2&&$getGrowthValue<800&&!empty($orderToMember)&&$orderToMember[0]->actual_price/100<499)
            {
                return 2;
            }

            if($getUserIndenty->card_type==1&&!empty($orderToMember)&&$orderToMember[0]->actual_price/100==499)
            {
                return 3;
            }


        }

        return 4;
    }

    public static function getUserNextIdentity($mid,$userIdentity)
    {   
        $userNextIdentity = 1;

        $countInfo = GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$mid]);

        if(empty($countInfo))
        {

            return 2;
        }

        $direct_push_num = $countInfo->direct_push_num;
        $direct_push_num_v2 = $countInfo->direct_push_num_v2+1;
        $shopowner_num = $countInfo->shopowner_num;
        $super_shopowner_num = $countInfo->super_shopowner_num;

        if($direct_push_num==1)
        {
            $userNextIdentity = 2;
        }

        if($direct_push_num_v2==1&&$shopowner_num>19)
        {
            $userNextIdentity = 3;
        }

        if($direct_push_num_v2==2&&$super_shopowner_num>14)
        {
            $userNextIdentity = 4;
        }

        if($userNextIdentity>$userIdentity)
        {
            return $userNextIdentity;
        }

        return $userIdentity;

    }

    public static function formatCommunityInvitationGroupList($data)
    {

        $list = [];

        if($data)
        {
            foreach ($data as $value)
            {
                //$memberInfo =   MemberModel::getMemberInfo(['mid'=>$value->mid]);

//                $adminMemberInfo =   AdminUserModel::getAdminUserInfo(['admin_user_id'=>$value->examine_mid]);

                $mobile = $value->mobile ?: '--';
                //$jointime = $value->jointime ?: 0;
                $nickname=$value->nickname ?: '';
                $countInfo = GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$value->mid]);

                if($value->examine_status>1){
                    $examineMemberInfo =   MemberModel::getMemberInfo(['mid'=>$value->examine_mid]);
                }

                $list[]=[
                    "tag_id"                    => $value->tag_id,
                    "tag_name"                    => empty(GroupManageModel::getTagDataName($value->tag_id))?"无":GroupManageModel::getTagDataName($value->tag_id),
                    "id"                    => $value->id,
                    "mid"                   => $value->mid,
                    "mobile"                => $mobile,
                    //"jointime"              => $jointime>0?date("Y-m-d H:i:s",$jointime):"--",
                    "nickname"              => $nickname,
                    "group_name"           => $value->group_name,
                    "groupNumber"           => $value->group_number,
                    "images"                => $value->images,
                    "addtime"               => date("Y-m-d H:i:s",$value->addtime),
                    "examineStatus"         => $value->examine_status,
                    "examineStatusName"     => self::getExamineStatus($value->examine_status),
                    "examineMember"         => !empty($examineMemberInfo->nickname)?$examineMemberInfo->nickname:'',
                    "examineMid"            => $value->examine_mid,
                    "examineTime"           => !empty($value->examine_time)?date("Y-m-d H:i:s",$value->examine_time):'',
                    "examineDesc"           => $value->examine_desc,
                    "assistantWx"           => $value->assistant_wx,
                    "tutorWx"               => $value->tutor_wx,
                    "roomId"                => $value->room_id,
                    "directPushNum"         => !empty($countInfo)?$countInfo->direct_push_num:0,
                    "intervalPushNum"       => !empty($countInfo)?$countInfo->interval_push_num:0,
                    "type"                  => $value->type,
                    "number_req"                  => $value->number_req,//请求的次数
//                    "form"                  => self::getGroupForm($value->type)
                ];
            }
        }

        return $list;
    }


    public static function getExamineStatus($num='')
    {
        $str = '';//'审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        switch ($num) {
            case 0:  //
                $str = '升级生成群号未提交';
                break;
            case 1:  //
                $str = '待审核';
                break;
            case 2:  //
                $str = '审核通过';
                break;
            case 3:  //
                $str = '审核拒绝';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }

    public static function getExamineStatusV2($num='')
    {
        $str = '';//'审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        switch ($num) {
            case 1:  //
                $str = '注册店主';
                break;
            case 2:  //
                $str = '店主';
                break;
            case 3:  //
                $str = '超级店主';
                break;
            case 4:  //
                $str = '创客';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }


    public static function getGroupForm($num='')
    {
        $str = '';//'审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        switch ($num) {
            case 0:  //
                $str = '微信小程序';
                break;
            case 1:  //
                $str = '微信小程序';
                break;
            case 2:  //
                $str = '小悦平台';
                break;

            default:
                $str = '';
                break;
        }
        return $str;

    }

    /*
     * 申请群审核详情
     * @param Array
     */
    public static function getGroupVerifyInfo($param)
    {

        if(!isset($param['id'])||empty($param['id']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $result    = GroupManageModel::getGroupVerifyInfo($param);

        if(empty($result))
        {
            return [ "code" => 400, "data" => '没有此数据'];
        }

        return [
            "code" => 200,
            "data" => self::formatCommunityInvitationGroupInfo($result),
        ];
    }


    public static function formatCommunityInvitationGroupInfo($data)
    {

        $list = [];
        // 统计这个人总共拒绝了多少次
        $examine =   MemberModel::geReferNumCount(['mid'=> $data->mid,'type'=>2]);
        if($data)
        {
              if($data->examine_status==2){
                   $examine_fix  = '审核通过原因';
              }else if($data->examine_status==3){
                  $examine_fix  = '审核拒绝原因';
              }else{
                  $examine_fix  = '审核结果描述';
              }
                $list=[
                    "id"                    => $data->id,
                    "mid"                   => $data->mid,
                    "groupNumber"           => $data->group_number,
                    "images"                => $data->images,
                    "addtime"               => $data->addtime,
                    "examineStatus"         => $data->examine_status,
                    "examineMid"            => $data->examine_mid,
                    "examineTime"           => $data->examine_time,
                    "examineDesc"           => $data->examine_desc,
                    "assistantWx"           => $data->assistant_wx,
                    "tutorWx"               => $data->tutor_wx,
                    "roomId"                => $data->room_id,
                    "number_req"                => $data->number_req,
                    "examine_fix"                =>$examine_fix,
                    "adopt_num"                =>1,
                    "refuse_num"                =>empty($examine)?0:$examine,
                ];
        }
        return $list;
    }


    /*
     * 群审核
     * @param Array
     */
    public static function toExamineGroup($param,$member)
    {
        if($member['user_type']<2){
            return ['code' => 400, 'msg' => '无权审核！'];
        }

        if(!isset($param['id'])||empty($param['id']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        if(!isset($param['examineStatus'])||empty($param['examineStatus']))
        {
            return ['code' => 400, 'msg' => '审核不能为空！'];
        }

        if($param['examineStatus']==3)
        {

            if(!isset($param['examineDesc'])||empty($param['examineDesc']))
            {
                return ['code' => 400, 'msg' => '审核原因不能为空！'];
            }
        }

        $result     =   GroupManageModel::getGroupVerifyInfo(['id'=> $param['id']]);

        if($result->examine_status==2)
        {
            return ['code' => 400, 'msg' => '审核失败'];
        }

        if(empty($result))
        {
            return [ "code" => 400, "msg" => '没有此数据'];
        }

        $upData['tag_id']             =   isset($param['tag_id'])?$param['tag_id']:0;//标签id
        $upData['examine_status']   =   $param['examineStatus'];//审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        $upData['examine_mid']      =    $member['mid'];//审核人id
        $upData['examine_time']     =   time();//审核时间
        $upData['examine_desc']     =   !empty($param['examineDesc'])?$param['examineDesc']:'';//不通过的原因


        $upGroup    =   GroupManageModel::upCommunityInvitationGroup(['id'=>$param['id']],$upData);

        $logData['mid']= $result->mid;
        $logData['addtime']= time();
        $logData['rel_log_id']=  $param['id'];
        $logData['examine_mid']=  $member['mid'];
        $logData['examine_time']=  time();
        $logData['type']=  $param['examineStatus']==2?1:2;
        RobotModel::addOrderInfo('examine_log',$logData);

        if($param['examineStatus']==2)
        {
            $time = time();

            $date = self::formatTimeStamp($time);

            //直推用户添加数 start
            $countInfo     =   GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$result->mid]);

            if(empty($countInfo))
            {
                $addCountData['mid']                =   $result->mid;
                $addCountData['direct_push_num']    =   1;
                $addCountData['created_at']         =   $date;

                $directPushNum =  $addCountData['direct_push_num'];

                $countRes     =   GroupManageModel::addCommunityInvitationGroupCount($addCountData);
                $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addCountData);

            }else{

                $upCountData['direct_push_num']    =   $countInfo->direct_push_num+1;
                $upCountData['updated_at']         =   $date;

                $directPushNum =  $upCountData['direct_push_num'];

                $countRes     =   GroupManageModel::upCommunityInvitationGroupCount(['mid'=>$result->mid],$upCountData);

                $addCountInfoData['mid']                =   $result->mid;
                $addCountInfoData['direct_push_num']    =   1;
                $addCountInfoData['created_at']         =   $date;
                $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addCountInfoData);
            }
            //直推用户添加数 end

            //self::checkZt($result->mid,$directPushNum);




            //间推用户添加数 start
            $shipInfo     =   GroupManageModel::getMemberShipInfo(['mid'=>$result->mid]);

            if(!empty($shipInfo))
            {
                $paths = $shipInfo->path;
                $paths = trim($paths,"0,");
                $path = explode(",",$paths);
                if($path)
                {
                    foreach ($path as $value)
                    {
                       if(!empty($value)){
                           $jianTuicountInfo     =   GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$value]);

                           if(empty($jianTuicountInfo))
                           {
                               $addJtCountData['mid']              =   $value;
                               $addJtCountData['interval_push_num']  =   1;
                               $addJtCountData['created_at']       =   $date;

                               $intervalPushNum =  $addJtCountData['interval_push_num'];

                               $countRes     =   GroupManageModel::addCommunityInvitationGroupCount($addJtCountData);
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountData);

                           }else{

                               $upJtCountData['interval_push_num']  =   $jianTuicountInfo->interval_push_num+1;
                               $upJtCountData['updated_at']         =   $date;

                               $intervalPushNum =  $upJtCountData['interval_push_num'];

                               $countRes     =   GroupManageModel::upCommunityInvitationGroupCount(['mid'=>$value],$upJtCountData);

                               $addJtCountInfoData['mid']                =   $value;
                               $addJtCountInfoData['interval_push_num']  =   1;
                               $addJtCountInfoData['created_at']         =   $date;
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountInfoData);
                           }
                           // self::checkJt($value,$intervalPushNum);
                       }
                    }
                }
            }

             $myGroupCount = GroupManageModel::getGroupCountByMid($result->mid,1);


             if($myGroupCount==1)
             {
                self::upIdentity($result->mid,1); //修改当前会员身份
                self::checkZt($result->mid);
             }
            //间推用户添加数 end

        }

        $logMessage = '修改群审核';
        UserOperationLogService::insertAdminOperation(
            $member['mid'], 4, json_encode($upData), $logMessage, $upGroup
        );
        return [ "code" => 200, "msg" => '操作成功'];

    }


    /*
     * 二次群审核
     * @param Array
     */
    public static function toTwiceExamineGroup($param,$member)
    {   
        if($member['user_type']<2){
            return ['code' => 400, 'msg' => '无权审核！'];
        }

        if(!isset($param['id'])||empty($param['id']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        if(!isset($param['examineStatus'])||empty($param['examineStatus']))
        {
            return ['code' => 400, 'msg' => '审核不能为空！'];
        }

        if($param['examineStatus']==3)
        {

            if(!isset($param['examineDesc'])||empty($param['examineDesc']))
            {
                return ['code' => 400, 'msg' => '审核原因不能为空！'];
            }
        }

        $result     =   GroupManageModel::getGroupVerifyInfo(['id'=> $param['id']]);

        if($result->upgrade_status==2)
        {
            return ['code' => 400, 'msg' => '审核失败'];
        }

        if(empty($result))
        {
            return [ "code" => 400, "msg" => '没有此数据'];
        }

        $upData['upgrade_status']   =   $param['examineStatus'];//审核状态  0升级生成群号不审核  1待审核  2审核通过  3审核不通过
        $upData['upgrade_mid']      =   $member['mid'];//审核人id
        $upData['upgrade_time']     =   time();//审核时间
        $upData['upgrade_desc']     =   !empty($param['examineDesc'])?$param['examineDesc']:'';//不通过的原因


        $upGroup    =   GroupManageModel::upCommunityInvitationGroup(['id'=>$param['id']],$upData);

        $logData['mid']= $result->mid;
        $logData['addtime']= time();
        $logData['rel_log_id']=  $param['id'];
        $logData['examine_mid']=  $member['mid'];
        $logData['examine_time']=  time();
        $logData['type']=  $param['examineStatus']==2?3:4;
        RobotModel::addOrderInfo('examine_log',$logData);

        if($param['examineStatus']==2)
        {
            $time = time();

            $date = self::formatTimeStamp($time);


            //直推用户添加数 start
            $countInfo     =   GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$result->mid]);

            if(empty($countInfo))
            {
                $addCountData['mid']                =   $result->mid;
                $addCountData['direct_push_num_v2']    =   1;
                $addCountData['created_at']         =   $date;

                $directPushNum =  $addCountData['direct_push_num_v2'];

                $countRes     =   GroupManageModel::addCommunityInvitationGroupCount($addCountData);
                $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addCountData);

            }else{

                $upCountData['direct_push_num_v2']    =   $countInfo->direct_push_num_v2+1;
                $upCountData['updated_at']         =   $date;

                $directPushNum =  $upCountData['direct_push_num_v2'];

                $countRes     =   GroupManageModel::upCommunityInvitationGroupCount(['mid'=>$result->mid],$upCountData);

                $addCountInfoData['mid']                =   $result->mid;
                $addCountInfoData['direct_push_num_v2']    =   1;
                $addCountInfoData['created_at']         =   $date;
                $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addCountInfoData);
            }
            //直推用户添加数 end

            




            //间推用户添加数 start
            $shipInfo     =   GroupManageModel::getMemberShipInfo(['mid'=>$result->mid]);

            if(!empty($shipInfo))
            {
                $paths = $shipInfo->path;
                $paths = trim($paths,"0,");
                $path = explode(",",$paths);
                if($path)
                {
                    foreach ($path as $value)
                    {
                       if(!empty($value)){
                           $jianTuicountInfo     =   GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$value]);

                           if(empty($jianTuicountInfo))
                           {
                               $addJtCountData['mid']              =   $value;
                               $addJtCountData['interval_push_num_v2']  =   1;
                               $addJtCountData['created_at']       =   $date;

                               $intervalPushNum =  $addJtCountData['interval_push_num_v2'];

                               $countRes     =   GroupManageModel::addCommunityInvitationGroupCount($addJtCountData);
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountData);

                           }else{

                               $upJtCountData['interval_push_num_v2']  =   $jianTuicountInfo->interval_push_num_v2+1;
                               $upJtCountData['updated_at']         =   $date;

                               $intervalPushNum =  $upJtCountData['interval_push_num_v2'];

                               $countRes     =   GroupManageModel::upCommunityInvitationGroupCount(['mid'=>$value],$upJtCountData);

                               $addJtCountInfoData['mid']                =   $value;
                               $addJtCountInfoData['interval_push_num_v2']  =   1;
                               $addJtCountInfoData['created_at']         =   $date;
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountInfoData);
                           }
                           
                       }
                    }
                }
            }

            //间推用户添加数 end

             $myGroupCount = GroupManageModel::getGroupCountByMid($result->mid,2);

             //达成一个400人群 20位店主成就 升级为超级会员
             if($myGroupCount==1&&$countInfo->shopowner_num>19)
             {
                self::checkJt($result->mid);
                self::upIdentity($result->mid,2); //修改当前会员身份
             }

             //达成两个400人群 15位超级店主成就 升级为创客
             if($myGroupCount==2&&$countInfo->super_shopowner_num>14)
             {
                self::upIdentity($result->mid,3); //修改当前会员身份
                
             }

        }


        return [ "code" => 200, "msg" => '操作成功'];

    }    



    //店主
    public static function checkZt($mid,$type=1)
    {
            $time = time();

            $date = self::formatTimeStamp($time);

            $shipInfo     =   GroupManageModel::getMemberShipInfo(['mid'=>$mid]);

            if(!empty($shipInfo))
            {
                $paths = $shipInfo->path;
                $paths = trim($paths,"0,");
                $path = explode(",",$paths);
                if($path)
                {
                    foreach ($path as $value)
                    {
                       if(!empty($value)){
                           $jianTuicountInfo     =   GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$value]);

                           if(empty($jianTuicountInfo))
                           {
                               $addJtCountData['mid']              =   $value;
                               $addJtCountData['shopowner_num']  =   1;
                               $addJtCountData['created_at']       =   $date;

                               $intervalPushNum =  $addJtCountData['shopowner_num'];

                               $countRes     =   GroupManageModel::addCommunityInvitationGroupCount($addJtCountData);
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountData);

                           }else{

                               $upJtCountData['shopowner_num']  =   $jianTuicountInfo->shopowner_num+1;
                               $upJtCountData['updated_at']         =   $date;

                               $intervalPushNum =  $upJtCountData['shopowner_num'];

                               $countRes     =   GroupManageModel::upCommunityInvitationGroupCount(['mid'=>$value],$upJtCountData);

                               $addJtCountInfoData['mid']                =   $value;
                               $addJtCountInfoData['shopowner_num']  =   1;
                               $addJtCountInfoData['created_at']         =   $date;
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountInfoData);
                           }
                           
                           //达成一个400人微信群 20位店长任务 成就超级店主
                           if($jianTuicountInfo->direct_push_num_v2==1&&$intervalPushNum==20)
                           {    
                                self::upIdentity($value,2); //修改当前会员身份
                                self::checkJt($value);
                           }
                       }
                    }
                }
            }


    }

    //超级店主
    public static function checkJt($mid,$type=2)
    {        
            $time = time();

            $date = self::formatTimeStamp($time);

            $shipInfo     =   GroupManageModel::getMemberShipInfo(['mid'=>$mid]);

            if(!empty($shipInfo))
            {
                $paths = $shipInfo->path;
                $paths = trim($paths,"0,");
                $path = explode(",",$paths);
                if($path)
                {
                    foreach ($path as $value)
                    {
                       if(!empty($value)){
                           $jianTuicountInfo     =   GroupManageModel::getCommunityInvitationGroupCountInfo(['mid'=>$value]);

                           if(empty($jianTuicountInfo))
                           {
                               $addJtCountData['mid']              =   $value;
                               $addJtCountData['super_shopowner_num']  =   1;
                               $addJtCountData['created_at']       =   $date;

                               $intervalPushNum =  $addJtCountData['super_shopowner_num'];

                               $countRes     =   GroupManageModel::addCommunityInvitationGroupCount($addJtCountData);
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountData);

                           }else{

                               $upJtCountData['super_shopowner_num']  =   $jianTuicountInfo->super_shopowner_num+1;
                               $upJtCountData['updated_at']         =   $date;

                               $intervalPushNum =  $upJtCountData['super_shopowner_num'];

                               $countRes     =   GroupManageModel::upCommunityInvitationGroupCount(['mid'=>$value],$upJtCountData);

                               $addJtCountInfoData['mid']                =   $value;
                               $addJtCountInfoData['super_shopowner_num']  =   1;
                               $addJtCountInfoData['created_at']         =   $date;
                               $countInfoRes =   GroupManageModel::addCommunityInvitationGroupCountInfo($addJtCountInfoData);
                           }
                           
                           //达成两个400人微信群 15位超级店主 成就创客
                           if($jianTuicountInfo->direct_push_num_v2==2&&$intervalPushNum==15)
                           {
                                self::upIdentity($value,3);
                           }
                       }
                    }
                }
            }
    }



    //超级店主  升级为创客默认为 978
    public static function upIdentity($mid,$type=1)
    {   
        //查看当前用户是否为创客 是创客时停止操作
        $workInfo = GroupManageModel::isWork($mid);

        if($workInfo==0)
        {
            $getUserIndenty = GroupManageModel::getUserIdentity($mid);

            $getGrowthValue = GroupManageModel::getGrowthValue($mid);



            switch ($type) {
                case 1:  //升级为体验会员

                    if($getUserIndenty->member_type<2&&$getGrowthValue<800&&$getUserIndenty->card_type<1)
                    {

                        $data['member_type'] = 1;
                        $data['member_from'] = 2;

                        //修改用户级别
                        $updateMemberIdentity = GroupManageModel::updateMemberIdentity($mid,$data);

                        $growth_value = 200;

                        //添加用户成长值
                        $addslineMemberGrowth = GroupManageModel::addslineMemberGrowth($mid,$growth_value,1);

                        //添加会员记录
                        $addslineMemberExperienceCard = GroupManageModel::addslineMemberExperienceCard($mid,1);

                        Log::info('add userIdentity for 1 mid:'.$mid);

                    }
                    # code...
                    break;
                case 2: //升级为499会员

                    

                    if($getUserIndenty->card_type>0)
                    {

                        $orderToMember = GroupManageModel::getOrderToMember($mid);

                        if(!empty($orderToMember)&&$orderToMember[0]->actual_price/100>399)
                        {

                            break;
                        }
                    }

                    if($getUserIndenty->member_type<3&&$getGrowthValue<1000)
                    {

                        $data['card_type'] = 1;
                        $data['member_type'] = 3;
                        $data['member_from'] = 2;

                        //修改用户级别
                        $updateMemberIdentity = GroupManageModel::updateMemberIdentity($mid,$data);

                        $growth_value = 1000;

                        //添加用户成长值
                        $addslineMemberGrowth = GroupManageModel::addslineMemberGrowth($mid,$growth_value,2);

                        Log::info('add userIdentity for 2 mid:'.$mid);

                    }                    

                    break;
                case 3: //升级为创客

                    $url = "http://gateway.yuebei.vip/branch/v1/worker/createWork";

                    $data = [
                        "mid"=>$mid,
                    ];

                    $updateMemberIdentityToWork = json_decode(curlPost($url,$data));

                    Log::info('add userIdentity for 3 mid:'.$mid);

                    break;
                default:
                    # code...
                    break;
            }
        }

    }

    /*
     * 群列表
     * @param Array
     */
    public static function getCommunityList($param)
    {

       $page = isset($param['page'])?$param['page']:1;
       $pageSize = isset($param['pageSize'])?$param['pageSize']:10;

       $communityList = GroupManageModel::getCommunityList($page,$pageSize);

       $communityCount = GroupManageModel::getCommunityCount();

       $list = [];

       foreach ($communityList as $key => $item) 
       {   
           $list[$key]['room_id'] = $item->id;
           $list[$key]['rid'] = $item->rid;
           $list[$key]['mid'] = $item->mobile;
           $list[$key]['groupName'] = $item->name;
           $list[$key]['robotStatus'] = 1;
           $list[$key]['member_count'] = $item->member_count;

           $member_count = GroupManageModel::getErveySexCount($item->id);

           $list[$key]['manProportion'] = intval($member_count->manCount*100)."%";
           $list[$key]['womanProportion'] = intval($member_count->womanCount*100)."%";

           $list[$key]['totalPrice'] = GroupManageModel::getTotalGMV($item->id);
           $list[$key]['totalBouns'] = GroupManageModel::getBonusByMid($item->mid,$item->id);
       }

       $data = [
        'totalCount'=>$communityCount,
        'list'=>$list
       ];

       return ["code"=>200,"data"=>$data];
    }


    /*
     * 机器人列表
     * @param Array
     */
    public static function getRobotList($param)
    {

       $page = isset($param['page'])?$param['page']:1;
       $pageSize = isset($param['pageSize'])?$param['pageSize']:10;        

       $robotList = GroupManageModel::getRobotList($page,$pageSize);

       $robotCount = GroupManageModel::getRobotCount();

       $list = [];

       foreach ($robotList as $key => $item) {
           
           $list[$key]['id'] = $item->id; //机器人编码
           $list[$key]['wxNumber'] = $item->wx_alias; //机器人绑定微信号
           $list[$key]['robotStatus'] = 1;//机器人状态
           $param['wxid'] = $item->wxid;
           $list[$key]['groupOwnerCount'] = GroupManageModel::getGroupOwnerCount($param); //绑定群主数量
           $list[$key]['groupCount'] = GroupManageModel::getCommunityCount($item->wxid);//绑定群数量

       }

       $data = [

            "totalCount"=>$robotCount,
            "list"=>$list
       ];

       return ["code"=>200,"data"=>$data];
    }


    /*
     * 群主信息列表
     * @param Array
     */
    public static function getGroupOwnerList($param)
    {

       $page = isset($param['page'])?$param['page']:1;
       $pageSize = isset($param['pageSize'])?$param['pageSize']:10;        

       $robotList = GroupManageModel::getGroupOwnerList($page,$pageSize,$param);

       $robotCount = GroupManageModel::getGroupOwnerCountV1($param);

       $list = [];

       foreach ($robotList as $key => $item) {
           
           $list[$key]['rid'] = $item->rid;
           $list[$key]['mid'] = $item->mobile;
           $list[$key]['robotStatus'] = 1;
           $list[$key]['groupCount'] = $item->count;

           $limitgroupCount = GroupManageModel::getlimitgroupCount($item->mid?$item->mid:0);

           $list[$key]['limitgroupCount'] = $limitgroupCount==null?0:$limitgroupCount;

       }

       $data = [

            "totalCount"=>$robotCount,
            "list"=>$list
       ];

       return ["code"=>200,"data"=>$data];
    }
    /*
     * 获取消息列表
     * @param Array
     */
    public static function getMessageList($param)
    {

       $page = isset($param['page'])?$param['page']:1;
       $pageSize = isset($param['pageSize'])?$param['pageSize']:10;        

       $message = GroupManageModel::getMessageList($param,$page,$pageSize);

       $messageCount = GroupManageModel::getMessageCount();

       $list = [];

       // foreach ($robotList as $key => $item) {
           
       //     $list[$key]['rid'] = $item->rid;
       //     $list[$key]['mid'] = $item->mid;
       //     $list[$key]['robotStatus'] = 1;
       //     $list[$key]['groupCount'] = $item->count;

       //     $limitgroupCount = GroupManageModel::getlimitgroupCount($item->mid);

       //     $list[$key]['limitgroupCount'] = $limitgroupCount==null?0:$limitgroupCount;

       // }

       $data = [

            "totalCount"=>$messageCount,
            "list"=>$message
       ];

       return ["code"=>200,"data"=>$data];

    }

    private static function getDateFromRange($startdate, $enddate){

        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);

        // 计算日期段内有多少天
        $days = ($etimestamp-$stimestamp)/86400+1;

        // 保存每天日期
        $date = array();

        for($i=0; $i<$days; $i++){
            $date[] = date('Y-m-d', $stimestamp+(86400*$i));
        }

        return $date;
    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /*
     * 管理员密码加密
     * 加密规则  md5(md5(密码).固定盐值)
     */
    private static function encryptPassword($password = '')
    {
        return md5(md5($password).self::SALT_FIGURE);
    }

    /**
     * @param $param
     * @param $member
     * @return array
     */
    public static function getTagData($param, $member)
    {
        $TagData = GroupManageModel::getTagData($member['platform_identity']);
        return ["code" => 200, "msg" => '操作成功', 'data' => $TagData];
    }

    /**
     * 更改群标签
     * @param $param
     * @param $member
     */
    public  static function changeGroupTag($param, $member)
    {
        //获取群的原来标签
        $room_id = isset($param['room_id'])?$param['room_id']:0;
        $tag_id = isset($param['tag_id'])?$param['tag_id']:0;
        if(empty($room_id)||empty($member['mid'])){
            return ["code" => 200, "msg" => '操作成功'];
        }else{
            $grouptagData =GroupManageModel::getGroupTagInfo($room_id);
            if($grouptagData->tag_id==$tag_id){
                $returnGroupData =GroupManageModel::getGroupInfo($room_id);
                if(!empty($returnGroupData)) {
                    GroupManageModel::upGroupTagGroupData('invitation_group', ['room_id' => $room_id, 'tag_id' => $tag_id]);
                }
                    return ["code" => 200, "msg" => '操作成功'];
            }else{

                $update['tag_id'] = $tag_id;
                $update['id'] = $room_id;
                $returnData =GroupManageModel::upGroupTagInfo('member_info_by_appid',$update);
                if($returnData){
                    $returnGroupData =GroupManageModel::getGroupInfo($room_id);
                    if(!empty($returnGroupData)){
                        GroupManageModel::upGroupTagGroupData('invitation_group',['room_id'=>$room_id,'tag_id'=>$tag_id]);
                    }
                    $logMessage = '更新群标签';
                    UserOperationLogService::insertAdminOperation(
                        $member['mid'], 2, json_encode([$room_id,$tag_id,$grouptagData->tag_id]), $logMessage, $room_id
                    );
                    return ["code" => 200, "msg" => '操作成功'];
                }else{
                    return ["code" => 200, "msg" => '操作失败'];
                }

            }
        }
    }
    /**
     * 发送顺序更新
     * @param $param
     * @param $member
     * @return array
     */
    public static function changeOrderBy($param, $member)
    {
        $room_id = isset($param['room_id'])?$param['room_id']:0;
        $order_by = isset($param['order_by'])?$param['order_by']:0;
        if(empty($room_id)||empty($member['mid'])){
            return ["code" => 200, "msg" => '操作成功'];
        }else{
            $grouptagData =GroupManageModel::getGroupTagInfo($room_id);
            if($grouptagData->order_num==$order_by){
                return ["code" => 200, "msg" => '操作成功'];
            }else{
                $update['order_num'] = $order_by;
                $update['id'] = $room_id;
                $returnData =GroupManageModel::upGroupTagInfo('member_info_by_appid',$update);
                if($returnData){
                    $logMessage = '更新群标签';
                    UserOperationLogService::insertAdminOperation(
                        $member['mid'], 2, json_encode([$room_id,$order_by,$grouptagData->order_num]), $logMessage, $room_id
                    );
                    return ["code" => 200, "msg" => '操作成功'];
                }else{
                    return ["code" => 200, "msg" => '操作失败'];
                }
            }
        }
    }
    /**
     *
     * 优惠券数据统计
     * @param $param
     * @param $member
     */
    public static  function  getCouponListStatisticsData($param, $member)
    {
        $page = isset($param['page'])?$param['page']:1;
        $pageSize = isset($param['pageSize'])?$param['pageSize']:10;
        $messageCount=0;
        $message=[];
         if($member['platform_identity']=='7323ff1a23f0bf1cda41f690d4089353'){
             if ((isset($param['startTime'])&&!empty($param['startTime'])) || (isset($param['endTime'])&&!empty($param['endTime']))) {
                 if (!isset($param['startTime'])) {
                     $param['startTime'] = $param['endTime'];
                 }
                 if (!isset($param['endTime'])) {
                     $param['endTime'] = $param['startTime'];
                 }
                 $param['startTime'] = strtotime($param['startTime']);
                 $endTime =$param['endTime'];
                 if($param['endTime']==$param['startTime']){
                     $endTime = date('Y-m-d 23:59:59', strtotime($param['endTime']));
                 }
                 //开始结束时间容错
                 $param['endTime'] = strtotime($endTime);
             }
             $robotWxAlias=[];
             if ($param['user_type'] >= 2 ) {
                 $robotWxAlias = GroupManageModel::getRobotWxAlias($param['mid'], $param['sup_id']);
             } else {
                 $robotWxAlias = GroupManageModel::getRobotWxAlias($param['mid'], $param['sup_id'], 1);
             }
             $wxAliasString = '';
             if (!empty($robotWxAlias)) {
                 foreach ($robotWxAlias as $v) {
                     $wxAliasString .= "'" . $v->wxid . "',";
                 }
                 $wxAliasString = trim($wxAliasString, ',');
             } else {
                 goto emptyArray;
             }

             $param['wxAliasString']=$wxAliasString;
             $message = GroupManageModel::getCouponList($param,$page,$pageSize);
             if(!empty($message)){
                 foreach(  $message  as $k=>&$v){
                     $v->num = ($page-1)*10+$k+1;
                 }
             }
             $messageCount = GroupManageModel::getCouponCount($param);

         }

        emptyArray:
        $data = [
            "totalCount"=>$messageCount,
            "list"=>$message
        ];
        return ["code"=>200,"data"=>$data];

    }

}