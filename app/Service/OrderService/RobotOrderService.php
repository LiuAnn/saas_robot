<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/14
 * Time: 21:07
 */

namespace App\Service\OrderService;

use App\Model\RobotOrder\RobotModel;
use App\Model\RobotOrder\BuyInfoModel;
use App\Model\RobotOrder\BuyOrderModel;
use App\Service\PayToken\ConfigMerchantServiceImpl;
use Illuminate\Support\Facades\Log;
use App\Model\Community\GroupManageModel;
class RobotOrderService
{

    const GRODER_KEY = "robotorder";

    /**
     * 获取服务列表  以及充值记录
     * @param $param
     */
    public static function getServiceList($param,$member)
    {

        //获取购买的服务
        $serviceList =   RobotModel::getRobotServiceList();
        foreach($serviceList as $v){
            if($v->service_unit==1){
                $v->duetime_at = date("Y-m-d",strtotime("+".$v->service_num." year"));
            }else if($v->service_unit==2){
                $v->duetime_at  = date("Y-m-d",strtotime("+".$v->service_num." month"));
            }
        }
        //获取购买的记录
        $rechargeData =   RobotModel::getRobotServiceOrderReload($member['mid']);
        //数据整合
        $returnData['serviceData'] =!empty($serviceList)?$serviceList:[];
        $returnData['rechargeData'] =!empty($rechargeData)?$rechargeData:[];
        $returnData['payMethod'] =[
            ['type'=>1,'method'=>'微信'],
            ['type'=>2,'method'=>'支付宝'],
        ];
        //返回
        return $returnData;
    }

    /**
     * 处理支付成功后的返回参数
     */
    public static function analysis($data) {

        $mchId  = $data["mchId"];
        //通知类型
        $notifyType = $data['notifyType'];

        //加密后的json数据
        $body = $data['body'];

        //解码
        $baseStr = base64_decode($body);
        if($mchId == 248) {
            $sKey = substr("088d5a30bfe54b928eba8cb794846e71",0,24);
            $deJson = openssl_decrypt($baseStr, 'DES-EDE3', $sKey, OPENSSL_RAW_DATA);
        } else {
            $configMerchantInstance = ConfigMerchantServiceImpl::getInstance($mchId);
            $sKey   = substr($configMerchantInstance->getMchKey(), 0, 24);
            $deJson  = @mcrypt_decrypt("tripledes",$sKey,$baseStr,"ecb");
        }

        Log::info('解析后端参数：'. $deJson);

        //删除多余字符
        $pos    =  strrpos($deJson,'}');
        $deJson = substr($deJson, 0, $pos + 1);
        $deJson = json_decode($deJson, true);
        $deJson["notifyType"] = $notifyType;
        return $deJson;
    }

    /**
     * 支付回调更新数据
     * @param $result
     * @author  liujiaheng
     * @desc    []
     * @created 2020/4/17 4:45 上午
     */
    public static function updateOrderStatus($result)
    {
        try {
            // 获取当前订单数据
            $orderInfo = BuyOrderModel::queryInfoByOrderSn($result['orderNo']);

            if (!$orderInfo) {
                \Log::error('robot pay callback param', [$result]);
                echo 'failed';
                exit;
            }

            $orderInfo->pay_status = 1;
            $orderInfo->pay_type = ($result['tradeType'] === 'WX_APP') ? 1 : 2;
            $orderInfo->pay_form_no = $result['payFormNo'];
            $orderInfo->pay_time = $result['endTime'];
            $orderInfo->actual_price = $result['payFee'];
            $orderInfo->save();

            // 获取购买记录
            $buyInfo = BuyInfoModel::queryInfoByOrderSn($result['orderNo']);

            if (!$buyInfo) {
                \Log::error('robot pay callback param', [$result]);
                echo 'failed';
                exit;
            }

            $buyInfo->pay_status = 1;
            $buyInfo->save();

            \Log::info('robot pay callback success', [$result]);

            echo 'success';
        } catch (\Exception $e) {

            \Log::error('robot pay callback failed', [$result]);

            echo 'failed';
        }

    }


    public static function  getPaySatus($ordersn){

      $payDataInfo  =   RobotModel::getPayOrderInfo($ordersn);

      return $payDataInfo->pay_status;

    }

    public static function getOrderGoodsTypeList()
    {
        return GroupManageModel::GOOD_TYPE_ARRAY;
    }


    /**
     * 获取购买的订单列表
     */
    public static function getOrderGoodsList($param = [], $member = [])
    {
        $wheredata=[];
        $wheredata['page'] = isset($param['page']) && !empty($param['page']) ? $param['page'] : 1;
        $wheredata['pageSize'] = isset($param['pageSize']) && !empty($param['pageSize']) ? $param['pageSize'] : 10;
        $wheredata['offer'] = ($wheredata['page'] -1 ) * $wheredata['pageSize'];
        $wheredata['groupName'] = isset($param['groupName']) && !empty($param['groupName']) ? $param['groupName'] : '';
        $wheredata['goodsName'] = isset($param['goodsName']) && !empty($param['goodsName']) ? $param['goodsName'] : '';
        $wheredata['buyerPhone'] = isset($param['buyerPhone']) && !empty($param['buyerPhone']) ? $param['buyerPhone'] : '';
        $wheredata['goodType'] = isset($param['goodType']) && !empty($param['goodType']) ? $param['goodType'] : '';
        $wheredata['startTime'] = isset($param['startTime']) && !empty($param['startTime']) ? $param['startTime'] : '';
        $wheredata['endTime'] = isset($param['endTime']) && !empty($param['endTime']) ? $param['endTime'] : '';
        $wheredata['groupMid'] = isset($param['groupMid']) && !empty($param['groupMid']) ? $param['groupMid'] : '';
        $wheredata['orderFrom'] = isset($param['orderFrom']) && !empty($param['orderFrom']) ? $param['orderFrom'] : '';
        $wheredata['channel'] = isset($param['channel']) && !empty($param['channel']) ? $param['channel'] : '';
        $wheredata['ordersn'] = isset($param['ordersn']) && !empty($param['ordersn']) ? $param['ordersn'] : '';
        $wheredata['room_id'] = isset($param['room_id']) && !empty($param['room_id']) ? $param['room_id'] : '';
        $wheredata['groupNameOrderBy'] = isset($param['groupNameOrderBy']) && !empty($param['groupNameOrderBy']) ? $param['groupNameOrderBy'] : '';

        if (isset($member['platform_identity'])){
            $wheredata['channel'] = GroupManageModel::getChannelIdByPlatform($member['platform_identity']);
        }

        if (!empty($param['platform_identity'])){
            $wheredata['channel'] = GroupManageModel::getChannelIdByPlatform($param['platform_identity']);
        }


        //获取所有机器人号
        if(isset($param['mid']) && $param['mid']>0){
            if($param['mid']==167624){
                $robotWxAlias = GroupManageModel::getRobotWxid($member['mid'], 0);
            }else if($param['mid'] == $member['mid'] && $member['sup_id'] == 0){
                $robotWxAlias = GroupManageModel::getRobotWxid($member['mid'], $member['sup_id']);
            }else{
                $robotWxAlias = GroupManageModel::getRobotWxid($param['mid'],1);
            }
        }else{
            if ($member['user_type'] >= 2) {
                $robotWxAlias = GroupManageModel::getRobotWxid($member['mid'], $member['sup_id']);
            }
        }
        $wxAliasString = '';
        if (!isset($param['robotWx']) || empty($param['robotWx'])) {
            $wxAliasString = '';
            if (!empty($robotWxAlias)) {
                foreach ($robotWxAlias as $v) {
                    $wxAliasString .= "'" . $v->wxid . "',";
                }
                $wxAliasString = trim($wxAliasString, ',');
            }
        }else if(!empty($param['robotWx'])) {
            $wxAliasString = "'" . $param['robotWx'] . "'";
        }

        $wheredata['wxAliasString']  =$wxAliasString;
        $orderGoodsCount = GroupManageModel::getOrderListCount($wheredata,$member);

        $orderGoodsList = GroupManageModel::getOrderListData($wheredata,$member);
        $totalGMV = GroupManageModel::getOrderGMVData($wheredata,$member);
        $totalGMV = is_numeric($totalGMV) ? $totalGMV / 100 : 0;
        $orderGoodsListArray = [];
        foreach ($orderGoodsList as $k => $v) {
//10 话费充值 11 视频充值  12 加油  13酒店 
            if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
                switch ($v->good_type) {
                    case '1':
                        $goodsTypeName = $v->channel == 1 ? '悦淘自营' : '大人自营';
                        break;
                    case '2':
                        $goodsTypeName = '京东';
                        break;
                    case '3':
                        $goodsTypeName = '拼多多';
                        break;
                    case '4':
                        $goodsTypeName = '唯品会';
                        break;
                    case '5':
                        $goodsTypeName = '美团';
                        break;
                    case '7':
                        $goodsTypeName = '淘宝';
                        break;
                    case '8':
                        $goodsTypeName = '苏宁';
                        break;
                    case '9':
                        $goodsTypeName = '品牌折扣券';
                        break;
                    case '10':
                        $goodsTypeName = '话费充值';
                        break;
                    case '11':
                        $goodsTypeName = '视频充值';
                        break;
                    case '12':
                        $goodsTypeName = '加油';
                        break;
                    case '13':
                        $goodsTypeName = '酒店';
                        break;
                    default:
                        $goodsTypeName = '其他';
                        break;
                }
            }else{
                $goodsTypeName = '';
            }
            $orderGoodsListArray[$k]['recordId'] = $v->record_id;
            $orderGoodsListArray[$k]['ordersn'] = $v->ordersn;
            $orderGoodsListArray[$k]['buyerNickname'] =  empty($v->nickname)?$v->mobile:$v->nickname;
            $orderGoodsListArray[$k]['buyerPhone'] =  $v->mobile;
            $orderGoodsListArray[$k]['goodsName'] = $v->goods_name;
            $orderGoodsListArray[$k]['actualPrice'] = $v->actual_price / 100;
            $orderGoodsListArray[$k]['orderTime'] = date('Y-m-d H:i:s', $v->created_at);
            $orderGoodsListArray[$k]['groupName'] = $v->groupName;
            $orderGoodsListArray[$k]['roomId'] = $v->roomId;
            $orderGoodsListArray[$k]['robotName'] = $v->wx_name;
            $orderGoodsListArray[$k]['wxid'] = $v->wxid;
            $orderGoodsListArray[$k]['manageName'] = $v->name;
            $orderGoodsListArray[$k]['goodTypeName'] = $goodsTypeName;
            $orderGoodsListArray[$k]['orderFrom'] = $v->order_from == 1 ? '社群' : 'APP';
            $orderGoodsListArray[$k]['channelName'] = $v->channel == 1 ? '悦淘' : '大人';
        }

        $return = [];
        $return['orderTotal'] = $orderGoodsCount;
        $return['orderTotalGMV'] = $totalGMV;
        $return['orderGoodsList'] = $orderGoodsListArray;

        return $return;
    }
    /**
     * @param $param
     */
    public  static function  getRoomMid($param)
    {
        $returnData=['room_id'=>0];
        if($param['dr_token']=='5882120c7346faae4b489f93d6b2913b'){
            $res = json_decode(json_encode(RobotModel::getRoomIdByMid($param['mid'])),true);
            if(empty($res)){
                $returnData['room_id']=0;
            }else{
                $returnData['room_id']=$res[0]['id'];
            }
        }
        return ['code'=>200,'msg'=>'获取成功','data'=>$returnData];
    }
    /**
     * @param $param
     */
    public  static function  getMid($param)
    {
        $returnData=['mid'=>0];
        if($param['token']=='75f712af51d952af3ab4c591213dea13'){
            $res = json_decode(json_encode(RobotModel::getMidByRoomId($param['roomId'],$param['token'])),true);
            if(empty($res)){
                $returnData['mid']=0;
            }else{
                $returnData['mid']=$res[0]['mid'];
            }
        }
        return ['code'=>200,'msg'=>'获取成功','data'=>$returnData];
    }


    /**
     * 获取群内购买商品的订单列表
     */
    public static function getGroupOrderList($param)
    {
        if(!isset($param['room_id'])||empty($param['room_id']))
        {
            return ["code" => 400, "msg" => '群ID不能为空'];
        }

        $md5Key = md5($param['room_id'].self::GRODER_KEY);

        if(!isset($param['md5Key'])||$param['md5Key']!=$md5Key)
        {
            return ["code" => 400, "msg" => '验证错误'];
        }

        $page = isset($param['page']) && !empty($param['page']) ? $param['page'] : 1;
        $pageSize = isset($param['pageSize']) && !empty($param['pageSize']) ? $param['pageSize'] : 10;

        $orderListData = GroupManageModel::getGroupOrderListData($param,$page,$pageSize);

        $orderCount = GroupManageModel::getGroupOrderListCount($param);

        $returnData = [
            'page'=>$page,
            'pageSize'=>$pageSize,
            'totalCount'=>$orderCount,
            'list'=>self::formateOrderList($orderListData)
       ];

        return ['code'=>200,'msg'=>'查询成功','data'=>$returnData];
    }


    public static function formateOrderList($result=[])
    {

        if($result==[])
        {
            return [];
        }

        $data = [];

        foreach ($result as $key => $item) {

            $data[] = [

                'member_id'=> $item->member_id,
                'parent_id'=> $item->parent_id,
                'buyer_nickname'=> $item->buyer_nickname,
                'buyer_phone'=> $item->buyer_phone,
                'ordersn_son'=> $item->ordersn_son,
                'ordersn'=> $item->ordersn,
                'ordersn_jd'=> $item->ordersn_jd,
                'ordersn_son_jd'=> $item->ordersn_son_jd,
                'goods_id'=> $item->goods_id,
                'goods_name'=> $item->goods_name,
                'goods_num'=> $item->goods_num,
                'goods_price_member'=> $item->goods_price_member,
                'goods_type'=> $item->goods_type,
                'goods_price_buy'=> $item->goods_price_buy,
                'goods_price_orig'=> $item->goods_price_orig,
                'goods_freight'=> $item->goods_freight,
                'goods_total'=> $item->goods_total,
                'goods_cover_image'=> $item->goods_cover_image,
                'pay_time'=> $item->pay_time,
                'goods_is_flash'=> $item->goods_is_flash,
                'actual_price'=> $item->actual_price,
                'goods_coupon_average'=> $item->goods_coupon_average,
                'sku_id'=> $item->sku_id,
                'channel'=> $item->channel,
                'good_type'=> $item->good_type,

            ];
        }

        return $data;
    }


}
