<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/14
 * Time: 08:43
 * Describe: 发消息相关都在这个类
 */

namespace App\Service\Platform;

use App\Model\Community\PullTaskRedisModel;
use App\Model\Mall\YanModel;
use App\Models\CensorHelper;
use App\Models\SendInfo as Send_Info;
use App\Models\SendInfoRedis;
use App\Models\User;
use App\Service\MessageCenter\CensorHelperService;
use App\Service\VoucherBag\CouponService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Models\WhiteListWx;
use App\Model\Community\CommunityModel;
use Exception;
use App\Models\MemberInfoByAppID;

class SendInfoService
{
    // 一分钟一个机器人条数发送限制
    private static $oneMinuteSendLimit = 10;
    private static $toBeSentRedis = 'robot:to:be:sent:list';

    public static function getToBeSentList()
    {
        // SET LOG
        $monolog = Log::getMonolog();
        $monolog->popHandler();
        Log::useFiles(storage_path('logs/sendInfo.log'));
        Log::info('开始获取符合条件的素材插入队列:');

        $send_info = Send_Info::where('is_send_yiyun', 0)
            ->where('is_send', 1)
            ->where('deleted_at', 0)
            ->groupBy('random_str')
            ->orderBy('id', 'ASC')
            ->limit(50)
            ->get()
            ->toArray();

        if (empty($send_info)) {
            return false;
        }

        foreach ($send_info as $key => $val) {
            // 先判断是否现在需要发送
            if ($val['is_timing_send'] == 0 || $val['timing_send_time'] < time()) {
                $tow_hours_ago_time = date('Y-m-d H:i:00', strtotime('-2 hour'));
                $thirty_minute_ago_timestamp = strtotime(date('Y-m-d H:i:00', strtotime('-30 minute')));
                // 两小时前的任务且不是定时任务的不再发送
                if (($val['created_at'] < $tow_hours_ago_time && $val['is_timing_send'] != 1) || ($val['created_at'] < $tow_hours_ago_time && $val['timing_send_time'] < $thirty_minute_ago_timestamp)) {
                    Log::info('超时的素材：' . json_encode($val['id']));
                    // 修改素材状态为已发送
                    goto setIsSend;
                }

                $where = [];

                if ($val['exclusive_mid'] > 0) {

                    $where = [
                        'exclusive_mid' => $val['exclusive_mid'],
                        'wxid_type' => 3
                    ];

                } else {

                    // 看发送的用户身份，在选择下面的机器人
                    $user_info = User::select('user_type', 'sup_id')
                        ->where('mid', $val['mid'])
                        ->where('user_type', 2)
                        ->where('delete_at', 0)
                        ->first();
                    if (empty($user_info)) {
                        continue;
                    }


                    if ($user_info['sup_id'] > 0) {
                        $where = [
                            'bind_mid' => $val['mid']
                        ];
                    } elseif ($user_info['sup_id'] == 0) {
                        $where = [
                            'mid' => $val['mid']
                        ];
                    }

                }

                //Log::info('要发的用户1：' . json_encode($user_info));

                //看那些机器人需要发送
                $push_user_list = WhiteListWx::select('wxid', 'mid', 'robot_serial_no', 'platform_identity')
                    ->where($where)
                    ->where('deleted_at', 0)
                    ->where('platform', 1)
                    ->get()
                    ->toArray();

                if (empty($push_user_list)) {
                    continue;
                }

                Log::info('要发的素材：' . json_encode($val['random_str']));
                Log::info('要发的机器人：' . json_encode($push_user_list));

                foreach ($push_user_list as $user => $user_val) {
                    $tag_ids = $val['tag_ids'];
                    $push_group_list = MemberInfoByAppID::select('id', 'wxid', 'room_wxid', 'mid', 'chat_room_serial_no')
                        ->where('wxid', $user_val['wxid'])
                        ->where('is_delete', 0)
                        ->where('is_follow', 1)
                        ->where('chat_room_serial_no', '!=', '');
                    if ($tag_ids != 0) {
                        $tag_ids = explode(',', $tag_ids);
                        $push_group_list->whereIn('tag_id', $tag_ids);
                    }
                    $push_group_list = $push_group_list->get()->toArray();

                    if (empty($push_group_list)) {
                        continue;
                    }

                    Log::info('要发的群：' . json_encode($push_group_list));

                    foreach ($push_group_list as $push_key => $push_val) {
                        if ($push_val['mid']) {
                            // GET 机器人分享码
                            $code_number_info = DB::connection('mysql_yuelvhui')->table('invitation_code')->select('code_number')->where('mid', $push_val['mid'])->first();
                        }
                        //机器人分享链接邀请码 8260930：默认邀请码
                        $code_number = empty($code_number_info) ? 8260930 : $code_number_info->code_number;

                        // GET 素材
                        $send_info_data = Send_Info::select('id', 'mid', 'type', 'tag_ids', 'cid', 'coupon_num', 'link_img_url', 'link_type', 'platform_identity', 'content', 'random_str', 'short_url', 'typeid')
                            ->where('is_send_yiyun', 0)
                            ->where('is_send', 1)
                            ->where('deleted_at', 0)
                            ->where('random_str', $val['random_str'])
                            ->get()
                            ->toArray();
                        if (empty($send_info_data)) {
                            continue;
                        }
                        foreach ($send_info_data as $send_info_data_key => $send_info_data_val) {
                            // 公共插入队列方法
                            self::setPushInfo(
                                $send_info_data_val['id'],
                                $push_val['id'],
                                $push_val['room_wxid'],
                                $user_val['wxid'],
                                $send_info_data_val['random_str'],
                                $code_number,
                                $send_info_data_val['link_type'],
                                $user_val['robot_serial_no'],
                                $push_val['chat_room_serial_no'],
                                $send_info_data_val,
                                $push_val['mid']);
                        }
                    }
                }
                // 超时的素材直接修改发送状态为已发送
                setIsSend:
                Send_Info::where('random_str', $val['random_str'])->update(['is_send_yiyun' => 1]);
            }
        }
    }

    /**
     * @param $id
     * @param string $room_id
     * @param string $room_wxid
     * @param string $wxid
     * @param string $random_str
     * @param string $code_number
     * @param string $link_type
     * @param string $robot_serial_no
     * @param string $chat_room_serial_no
     * @param array $data
     * @return false
     * @throws Exception
     */
    public static function setPushInfo($id, $room_id = '', $room_wxid = '', $wxid = '', $random_str, $code_number = '', $link_type = '', $robot_serial_no = '', $chat_room_serial_no = '', $data = [], $mid = 0)
    {
        if (empty($data)) {
            return false;
        }
        $push_final_data = [];

        // 所有参数
        // print_r(func_get_args());

        /**
         * 用于统计发送素材的参数
         * random_str ： 随机字符串
         * link_type ： 链接类型
         */
        $push_final_data['id'] = $id;
        $push_final_data['room_id'] = $room_id;
        $push_final_data['room_wxid'] = $room_wxid;
        $push_final_data['wxid'] = $wxid;
        $push_final_data['random_str'] = $random_str;
        $push_final_data['link_type'] = $link_type;

        /**
         * 消息体必要参数（IN OUT）
         * robotSerialNo : 机器人编号
         * chatRoomSerialNo ： 群编号
         */
        $push_final_data['robot_serial_no'] = $robot_serial_no;
        $push_final_data['chat_room_serial_no'] = $chat_room_serial_no;

        /**
         * 消息体必要参数（内容模版）
         *
         * nMsgNum : 消息编号
         * nMsgType ： 消息类型
         * nVoiceTime ： 语音时长/视频时长,时长单位：秒；当消息类型为以上两种类型时，必须传时长且时长要正确，否则会发送失败，当时长不正确时可能会有很大的禁封风险
         * nIsHit ： 是否艾特必填，值为空或则不为以下提供的值，调用接口可以成功，但是消息无法发送至群内。数组内仅文本消息或空消息支持@人 (0 不@人 1 @所有人（只能是管理员和群主使用，@所有人的文字内容不能为空） 2 @部分群成员)
         * nAtLocation ： @人在文本的所在位置 0 文本开始位置 1文本结束位置 2任意位置
         * vcHref ： 链接URL，当消息为视频时，此处传视频的链接地址
         * vcTitle ： 链接标题
         * vcDesc ： 链接描述
         * vcAtWxSerialNos ： 指定艾特部分人的编号(多个用','隔开,如果不用艾特则传空)
         */
        $push_final_data['data']['nMsgNum'] = 1;
        $push_final_data['data']['nMsgType'] = Send_Info::$type[$data['type']];
        $push_final_data['data']['nVoiceTime'] = 0;
        $push_final_data['data']['nIsHit'] = 0;
        $push_final_data['data']['nAtLocation'] = 0;
        $push_final_data['data']['vcHref'] = 0;
        $push_final_data['data']['vcTitle'] = 0;
        $push_final_data['data']['vcDesc'] = 0;
        $push_final_data['data']['vcAtWxSerialNos'] = [];

        // 文本
        if (($data['type'] == 1 && !empty($data['content'])) || ($data['type'] == 6 && !empty($data['content'])) || ($data['type'] == 7 && !empty($data['content']))) {
            $short_url = '';
            if (!empty($data['short_url'])) {
                if ($data['platform_identity'] == '7323ff1a23f0bf1cda41f690d4089353') {
                    // 悦淘转链
                    $short_url = '👉' . self::getShortUrl($data['short_url'], $code_number, $room_id);
                } else if ($data['platform_identity'] == 'c23ca372624d226e713fe1d2cd44be35') {
                    // 邻居团转链
                    $short_url = '👉' . PullTaskRedisModel::getLinjutuanLinkUrl(['url' => $data['short_url'], 'room_id' => $room_id, 'group_mid' => $mid, 'order_froms' => 1]);
                } else if ($data['platform_identity'] == '89dafaaf53c4eb68337ad79103b36aff') {
                    // 大人转链
                    $short_url = '👉' . PullTaskRedisModel::getDaRenLinkUrl(['url' => $data['short_url'], 'room_id' => $room_id, 'group_mid' => $mid, 'order_froms' => 1]);
                } else if ($data['platform_identity'] == '183ade9bc5f1c4b52c16072362bb21d1') {
                    // 迈图转链
                    $short_url = '👉' . PullTaskRedisModel::getMaituLinkUrl(['url' => $data['short_url'], 'room_id' => $room_id, 'group_mid' => $mid, 'order_froms' => 1]);
                } else if ($data['platform_identity'] == '75f712af51d952af3ab4c591213dea13') {
                    // 直订转链
                    $short_url = '👉' . PullTaskRedisModel::getZhidingLinkUrl(['url' => $data['short_url'], 'room_id' => $room_id, 'group_mid' => $mid, 'order_froms' => 1]);
                }
            }
            $content = rtrim($data['content']) . $short_url;
            // SET 过滤敏感词
            $push_final_data['data']['msgContent'] = self::verifyKeyWord($content);
        }
        // 图
        if ($data['type'] == 2) {
            $push_final_data['data']['msgContent'] = $data['content'];
        }
        // 视频
        if ($data['type'] == 3) {
            /**
             * 视频图和时长目前没有，先用悦淘的logo
             */
            $default_video_images = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-02-17/13/yuelvhuiQJwXComJ821581918103.jpg';
            $push_final_data['data']['msgContent'] = $data['link_img_url'] ?? $default_video_images;
            $push_final_data['data']['nVoiceTime'] = 10;
            $push_final_data['data']['vcHref'] = $data['content'];
        }
        // 链接
        if ($data['type'] == 4 && !empty($data['content'])) {
            // 转链
            $short_url = self::getShortUrl($data['content'], $code_number, $room_id);
            $push_final_data['data']['msgContent'] = $short_url;
        }
        //小程序
        if ($data['type'] == 8) {
            if ($data['platform_identity'] == '2cb4a91bce63da9862f72559d4c463f5') {
                $miniAppQueryData = [
                    'room_id' => $room_id
                ];
            } else {
                $miniAppQueryData = [
                    'room_id' => $room_id,
                    'reCode' => $code_number,
                    'code_number' => $code_number
                ];
            }
            $newXml = PullTaskRedisModel::getSendMiniAppXmlDataByContent($data['content'], $miniAppQueryData);
            $push_final_data['data']['msgContent'] = base64_encode($newXml);
            $push_final_data['data']['vcHref'] = $data['link_img_url'] ?: '';
            unset($push_final_data['data']['link_type']);
            $push_final_data['data']['vcTitle'] = $push_final_data['data']['vcDesc'] = null;
        }
        // SET Redis
        self::setSendInfoToRedis($push_final_data);
    }

    /**
     * 单条数据入队列
     *
     * @param array $data
     * @return false
     */
    private static function setSendInfoToRedis($data = [])
    {
        if (empty($data)) {
            return false;
        }
        //  TODO 查看机器人是否在线、如果不在线把消息放到另一个队列中，检测到机器人上线在发送

        // 插入Redis
        Redis::lpush(self::$toBeSentRedis, json_encode($data));
    }

    /**
     * 发送队列中的素材
     */
    public static function sendToBePushByRedis()
    {
        // SET LOG
        $monolog = Log::getMonolog();
        $monolog->popHandler();
        Log::useFiles(storage_path('logs/sendInfo.log'));

        // 按每分钟推送限制推送
        for ($i = 0; $i < self::$oneMinuteSendLimit; $i++) {
            $redis_list = Redis::rpop(self::$toBeSentRedis);
            if (empty($redis_list)) {
                continue;
            }
            $data = json_decode($redis_list, true);

            //查看当前微信号有没有推送
            $id = CommunityModel::getWxSendReload($data['random_str'], $data['wxid']);
            if (empty($id)) {
                CommunityModel::insertGetId('send_reload', ['random_str' => $data['random_str'], 'wxid' => $data['wxid']]);
            }
            $count = CommunityModel::getSendRoomCount($data['random_str'], $data['room_wxid']);
            if ($count == 0) {
                CommunityModel::insertGetId('send_room_reload', ['random_str' => $data['random_str'], 'link_type' => $data['link_type'], 'wxid' => $data['wxid'], 'room_wxid' => $data['room_wxid'], 'room_id' => $data['id']]);
            }

            $retData = YiyunService::sendInfo($data['robot_serial_no'], $data['chat_room_serial_no'], [$data['data']]);

            $insert['wxid'] = $data['wxid'];
            $insert['random_str'] = $data['random_str'];
            $insert['status'] = 0;
            $insert['group_id'] = $data['room_id'];
            $insert['send_id'] = $data['id'];
            $insert['json_data'] = $redis_list;
            $message = '已发送的素材ID：';
            // Notice 消息发送失败的任务插入补发库并通知技术
            if ($retData['nResult'] != 1) {
                $message = '发送失败的素材ID：';
                $insert['status'] = 1;
                $insert['code'] = $retData['nResult'];
                $insert['message'] = $retData['vcResult'];
                YiyunService::urgentNotice('发送群消息失败:' . json_encode($retData));
            }
            SendInfoRedis::insert($insert);
            Log::info($message . json_encode($data['id']));
        }
    }

    /**
     * 转链
     *
     * @param $short_url
     * @param $code_number
     * @param $room_id
     * @return string
     */
    public static function getShortUrl($short_url, $code_number, $room_id)
    {
        // 判断是否是URL （规避运营人员把内容添在链接字端里）
        $isUrl = filter_var($short_url, FILTER_VALIDATE_URL);
        if (empty($isUrl)) {
            return $short_url;
        }
        $keyParseData = parse_url($short_url);
        $shareCode = substr($keyParseData['path'], 1, 6);
        $longData = YanModel::getProductLongUrl($shareCode);
        if (!empty($longData)) {
            $parseData = parse_url($longData);
            $longInfoUrl = explode('?', $longData);
            $queryData = PullTaskRedisModel::convertUrlQuery($longInfoUrl[1]);
            $queryData['codeNumber'] = $code_number;
            $queryData['room_id'] = isset($room_id) ? $room_id : 0;
            if ($parseData['path'] == '/page/JD.html') {//京东联盟
                $queryData['order_from'] = 1;
            } else if ($parseData['path'] == '/page/PDD.html') {
                $queryData['order_from'] = 1;
            } else if ($parseData['path'] == '/index-zhibo-yuetao.html') {
                $queryData['share_from'] = 3;
            } else {
                $queryData['share_from'] = 3;
                $queryData['robot_send'] = 1;
            }
            $newUrl = PullTaskRedisModel::combineURL($longInfoUrl[0], $queryData);
            $short_url = CouponService::getShortURrl(['url' => $newUrl]);
        }
        return $short_url;
    }

    public static function reloadPushTask()
    {
        // SET LOG
        $monolog = Log::getMonolog();
        $monolog->popHandler();
        Log::useFiles(storage_path('logs/sendInfo.log'));
        Log::info('补发消息开始:');

        $data = SendInfoRedis::where('status', 1)->limit(10)->get()->toArray();
        if (!empty($data)) {
            foreach ($data as $key => $val) {
                SendInfoRedis::where('id', $val['id'])->update(['status' => 0]);
                if (empty($val['json_data'])) {
                    continue;
                }
                // 不满足补发条件的素材不补发
                if (in_array($val['code'], SendInfoRedis::$codeType)) {
                    continue;
                }
                // 补发总次数超过2次 不再补发
                $count = SendInfoRedis::where('wxid', $val['wxid'])->where('group_id', $val['group_id'])
                    ->where('send_id', $val['send_id'])->count();
                if ($count > 2) {
                    continue;
                }
                $json_data = json_decode($val['json_data'], true);
                # 第一段替换敏感词也没有发送成功的内容重新替换并升级敏感词级别
                if ($val['code'] == 120111) {
                    $msgContent = CensorHelperService::setCensorLevels($json_data['data']['msgContent']);
                    $json_data['data']['msgContent'] = $msgContent;
                }
                self::setSendInfoToRedis($json_data);
                Log::info('已补发的素材ID：' . json_encode($val['send_id']));
            }
        }
        echo 'success';
    }

    /**
     * 过滤敏感词
     * Describe: 本方法只能满足基础的敏感词替换，如果要优化，需要做敏感词等级，替换、审核、禁止发送， 日志也要独立
     *
     * @param string $str
     * @return mixed|string|string[]
     */
    public static function verifyKeyWord($str = '')
    {
        if (empty($str)) {
            return $str;
        }
        # SET LOG
        $monolog = Log::getMonolog();
        $monolog->popHandler();
        Log::useFiles(storage_path('logs/sendInfo.log'));

        $keyword = CensorHelper::where('deleted_at', 0)->get()->toArray();
        if (empty($keyword)) {
            return $str;
        }
        foreach ($keyword as $key => $val) {
            if (strstr($str, $val['prohibited_word']) == true) {
                $replacement = $val['replacement'];
                # 敏感词等级2级时用第二种替换方式
                if ($val['levels'] == 2) {
                    $replacement = $val['replacement_second'];
                }
                // 替换字符串敏感内容
                $str = str_replace($val['prohibited_word'], $replacement, $str);
                Log::info('本次替换的敏感词', [$val['prohibited_word']]);
            }
        }
        return $str;
    }
}