<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/08/31
 * Time: 上午11:43
 */

namespace App\Service\Platform;

use App\Models\YiyunCallback;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Models\WhiteListWx;
use App\Models\MemberLoginInfo;
use App\Models\RobotFriend;
use App\Model\Community\CommunityModel;
use Exception;
use App\Models\MemberInfoByAppID;
use App\Models\MemberWeixinInfo;

class EnterpriseService
{

    //access_token
    protected static $access_token = 'enterprise:access:token';
    //授权地址
    protected static $AUTHORIZED_URL = 'http://merchant.opsdns.cc:8081';
    //服务地址
    protected static $SERVICE_URL = 'http://merchant.opsdns.cc:8081';
    //商家编号
    protected static $MERCHANT = '202011120001487';
    //商家秘钥
    protected static $SECRET = 'ff3ddc4c86024eb2b0fd91ad985617aa';
    //渠道标识-悦淘
    protected static $PLATFORM_IDENTITY = '7323ff1a23f0bf1cda41f690d4089353';


    /**
     * 接口授权 获取Token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getToken()
    {
        $token = Redis::get(self::$access_token);
        // 没取到缓存里的Token时，在去远程获取
        if (empty($token)) {
            $url = self::$AUTHORIZED_URL . '/uaa/oauth/token';
            $data = [
                'grant_type' => 'client_credentials',
                'scope' => 'api',
            ];
            $Authorization = 'Basic ' . base64_encode(self::$MERCHANT . ':' . self::$SECRET);

            $headers = [
                "Authorization: " . $Authorization . "",
            ];

            //curl post
            $retData = self::curlRequest($url, $data, $headers);
            if (empty($retData)) {
                return '';
            }
            $result = json_decode($retData, true);

            //失败
            if ($result['code'] != 0) {
                error_log('请求日期：' . date('Y-m-d H:i:s') . ' 返回内容：' . json_encode($result) . "\n", 3, storage_path('logs/getTokenError.log'));
                return '';
            }
            //把取到的token存入redis，并设置过期时间为2小时
            Redis::set(self::$access_token, $result['data']);
            Redis::expire(self::$access_token, 7200);
            $token = $result['data'];
        }
        return $token;
    }


    /**
     * 登陆
     *
     * @param $wx_alias
     * @return mixed
     * @throws Exception
     */
    public static function login($platform_identity)
    {
        //机器人编号（非第一次登录时请求参数中的机器人编号）
        $enterprise_robot_serial_no = '';
        $url = self::$SERVICE_URL . '/wxscrm/Robot/RobotLogin';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $enterprise_robot_serial_no, //机器人编号，首次登录机器人编号可不传
            'rela_serial_no' => '', //商家业务编号，非必传参数。传入值后，该接口的回调信息中也会返回该参数
        ];
        $retData = self::QinRequest($url, $data);
        // 失败
        if ($retData['code'] != 0) {
            throw new Exception($retData['message'], $retData['code']);
        }
        YiyunCallback::insert([
            'serial_no' => $retData['serial_no'],
            'platform_identity' => $platform_identity
        ]);
        return $retData;
    }

    /**
     * callBack 登陆成功的操作
     *
     * @param $data
     * @return bool
     */
    public static function loginSuccess($data)
    {
        //企业微信机器人编号
        $robot_serial_no = empty($data['robot_serial_no']) ? '' : $data['robot_serial_no'];

        if (empty($robot_serial_no)) {
            return false;
        }

        $callBackInfo = YiyunCallback::select('platform_identity')
            ->where('serial_no', $data['serial_no'])
            ->where('merchant_no', '')
            ->first();

        if(empty($callBackInfo)){
            return false;
        }

        //查询用户信息
        $user_info = WhiteListWx::where('enterprise_robot_serial_no', $robot_serial_no)->whereStatus(0)->first();

        // 没有用户信息，新用户逻辑
        if (empty($user_info)) {
            $bind_mid = 921696;
            $order_by = WhiteListWx::select('order_by')->orderBy('id', 'DESC')->first();
            //TODO mid 和 bind_mid 目前写死
            WhiteListWx::insert(
                [
                    'wx_alias' => '',
                    'wxid' => '',
                    'robot_serial_no' => '',
                    'enterprise_robot_serial_no' => $robot_serial_no,
                    'status' => 0,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'wxid_type' => 3,
                    'is_send_by_ass' => 0,
                    'exclusive_mid' => 0,
                    'platform_identity' => $callBackInfo->platform_identity,
                    'order_by' => $order_by->order_by + 1,
                    'wx_name' => '探鲸企业微信机器人' . ($order_by->order_by + 1) . '号',
                    'mid' => 167624,
                    'bind_mid' => $bind_mid,
                    'platform' => 2,
                ]
            );
        }

        //登陆成功获取企业微信机器人群信息
        $group_list = self::getGroupList($robot_serial_no);
        if (!empty($group_list)) {
            foreach ($group_list['data'] as $key => $val) {
                // 除了外部群外的企业微信群，拉来暂时没用
                if ($val['group_type'] != 2) {
                    continue;
                }
                $group_info = MemberInfoByAppID::where('enterprise_group_serial_no', $val['group_serial_no'])->first();
                if (!empty($group_info)) {
                    continue;
                }
                $insertInfo = [];
                $insertInfo['action'] = 'login_new_room';
                $insertInfo['appid'] = self::$MERCHANT;
                $insertInfo['wxid'] = '';
                $insertInfo['room_wxid'] = '';
                $insertInfo['name'] = $val['name'];
                $insertInfo['owner_wxid'] = '';
                $insertInfo['head_img'] = '';
                $insertInfo['member_count'] = 0;
                $insertInfo['memberInfo_list'] = '';
                $insertInfo['is_follow'] = 0;
                $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
                $insertInfo['chat_room_serial_no'] = '';
                $insertInfo['enterprise_robot_serial_no'] = $robot_serial_no;
                $insertInfo['enterprise_group_serial_no'] = $val['group_serial_no'];
                $insertInfo['group_type'] = $val['group_type'];
                MemberInfoByAppID::create($insertInfo);
            }
        }

        //登陆成功通知企业微信机器人客户信息
        try {
            //通知
            self::getMyCustomers($robot_serial_no);
        } catch (Exception $e) {
            //通知错误时的处理
        }

        //记录登陆log
        self::addLoginLog($data, 1);
        return true;
    }

    public static function logOut($wx_alias)
    {
        if (empty($wx_alias)) {
            throw new Exception('params is error', 400);
        }
        //获取用户信息，然后再插入
        $user_info = WhiteListWx::where('wx_alias', $wx_alias)->first();
        $url = self::$SERVICE_URL . '/wxscrm/Robot/RobotLoginOut';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $user_info['enterprise_robot_serial_no'], //机器人编号，首次登录机器人编号可不传
            'rela_serial_no' => '', //商家业务编号，非必传参数。传入值后，该接口的回调信息中也会返回该参数
        ];

        $result = self::QinRequest($url, $data);
        // 失败
        if (empty($result) || $result['code'] != 0) {
            throw new Exception($result['message'], $result['code']);
        }
        $log_data['data']['name'] = $user_info->wx_name;
        $log_data['data']['wx_alias'] = $wx_alias;
        $log_data['data']['wxid'] = $user_info->wxid;
        $log_data['robot_serial_no'] = $user_info->enterprise_robot_serial_no;
        self::addLoginLog($log_data, 2);
        return $result;
    }

    public static function curlRequest($url, $data = null, $header = null)
    {
        //初始化浏览器
        $ch = curl_init();
        //设置浏览器，把参数url传到浏览器的设置当中
        curl_setopt($ch, CURLOPT_URL, $url);
        //以字符串形式返回到浏览器当中
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //禁止https协议验证域名，0就是禁止验证域名且兼容php5.6
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //禁止https协议验证ssl安全认证证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //判断data是否有数据，如果有data数据传入那么就把curl的请求方式设置为POST请求方式
        if (!empty($data)) {
            //设置POST请求方式
            @curl_setopt($ch, CURLOPT_POST, true);
            //设置POST的数据包
            @curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        //设置header头
        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        //让curl发起请求
        $str = curl_exec($ch);
        //关闭curl浏览器
        curl_close($ch);
        //把请求回来的数据返回
        return $str;
    }

    /**
     * 记录登陆日志
     *
     * @param $data
     * @param $type 1：登陆、2：登出
     * @return bool
     */
    public static function addLoginLog($data, $type)
    {
        //记录登陆log
        MemberLoginInfo::insert(
            [
                'appid' => self::$MERCHANT,
                'wxid' => empty($data['data']['wxid']) ? '' : $data['data']['wxid'],
                'machine_id' => '',
                'client_version' => '',
                'province' => '',
                'city' => '',
                'head_img' => empty($data['data']['profile_photo']) ? '' : $data['data']['profile_photo'],
                'nickname' => empty($data['data']['name']) ? '' : $data['data']['name'],
                'nonce' => '',
                'wx_alias' => empty($data['data']['wx_alias']) ? '' : $data['data']['wx_alias'],
                'sex' => empty($data['data']['gender']) ? '' : $data['data']['gender'],
                'type' => $type,
                'enterprise_robot_serial_no' => $data['robot_serial_no']
            ]
        );
        return true;
    }

    /**
     * 群消息发送
     *
     * @param string $robotSerialNo 机器人编号
     * @param string $chatRoomSerialNo 群编号
     * @param array $data
     * @return bool
     */
    public static function sendInfo($robotSerialNo = '', $chatRoomSerialNo = '', $data = [])
    {
        if (empty($robotSerialNo) || empty($chatRoomSerialNo) || empty($data)) {
            return ['code' => 400, 'message' => '参数错误'];
        }

        //获取群信息
        $url = self::$SERVICE_URL . '/wxscrm/Message/SendMessagesToGroup';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'rela_serial_no' => '',
            'group_serial_no' => $chatRoomSerialNo,
            'data' => [
                $data
            ]
        ];
        $retData = self::QinRequest($url, $data);
        return $retData;
    }

    /**
     * 统一请求
     *
     * @param $url
     * @param $data
     * @return bool
     */
    protected static function QinRequest($url, $data)
    {

        $data = json_encode($data);

        $token = self::getToken();
        if (empty($token)) {
            return [
                'code' => 500,
                'message' => 'Get Token Error, Pleas check the log',
            ];
        }

        $Authorization = 'Bearer ' . $token;
        $headers = [
            "Authorization: " . $Authorization . "",
            "Content-Type:application/json"
        ];

        //curl post
        $retData = self::curlRequest($url, $data, $headers);
        $result = json_decode($retData, true);

        //请求失败做记录
        if ($result['code'] != 0) {
            error_log('请求日期：' . date('Y-m-d H:i:s') . ' 返回内容：' . json_encode($result) . "\n", 3, storage_path('logs/enterprise-qin-error.log'));
            return [
                'code' => $result['code'],
                'message' => $result['message'],
            ];
        }
        return $result;
    }


    /**
     * 一键创建外部群
     *
     * @param string $robotSerialNo 机器人编号
     * @param string $chatRoomSerialNo 群编号
     * @param array $data
     * @return bool
     */
    public static function CreateCustomerGroup($robotSerialNo = '', $CreateGroupSendMessage = '')
    {
        //一键创建外部群
        $url = self::$SERVICE_URL . '/wxscrm/Group/CreateCustomerGroup';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'rela_serial_no' => '',
            'message' => $CreateGroupSendMessage,
        ];
        $result = self::QinRequest($url, $data);
        // 失败
        if ($result['code'] != 0) {
            throw new Exception($result['message'], $result['code']);
        }
        return $result;
    }


    /**
     * 获取我的客户列表
     *
     * @param string $robotSerialNo 机器人编号
     * @param array $data
     * @return bool
     */
    public static function getMyCustomers($robotSerialNo = '')
    {
        //获取我的客户列表
        $url = self::$SERVICE_URL . '/wxscrm/Contact/GetMyCustomers';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'rela_serial_no' => '',
        ];
        $result = self::QinRequest($url, $data);
        // 失败
        if ($result['code'] != 0) {
            throw new Exception($result['message'], $result['code']);
        }
        return $result;
    }


    /**
     * 获取保存在通讯录中群列表
     *
     * @param string $robotSerialNo 机器人编号
     * @param array $data
     * @return bool
     */
    public static function getContactGroups($robotSerialNo = '')
    {
        //获取我的客户列表
        $url = self::$SERVICE_URL . '/wxscrm/Group/GetContactGroups';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'rela_serial_no' => '',
        ];
        $result = self::QinRequest($url, $data);
        // 失败
        if ($result['code'] != 0) {
            throw new Exception($result['message'], $result['code']);
        }
        return $result;
    }

    /**
     * 获取机器人开关状态接口
     *
     * @param string $robotSerialNo 机器人编号
     * @param array $data
     * @return bool
     */
    public static function getRobotConfigs($robotSerialNo = '')
    {
        //获取我的客户列表
        $url = self::$SERVICE_URL . '/wxscrm/robot/getRobotConfigs';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'rela_serial_no' => '',
        ];
        $result = self::QinRequest($url, $data);
        // 失败
        if ($result['code'] != 0) {
            throw new Exception($result['message'], $result['code']);
        }
        return $result;
    }

    /**
     * 获取群用户信息
     *
     * @param $robot_serial_no
     * @param $group_serial_no
     * @return bool
     * @throws Exception
     */
    public static function getGroupUserInfo($robot_serial_no, $group_serial_no)
    {
        $url = self::$SERVICE_URL . '/wxscrm/Group/GetGroupMembers';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robot_serial_no,
            'group_serial_no' => $group_serial_no,
            'rela_serial_no' => ''
        ];
        $result = self::QinRequest($url, $data);
        print_r($result);
        exit;
        // 失败
        if ($result['code'] != 0) {
            throw new Exception($result['message'], $result['code']);
        }
        return $result;
    }

    /**
     * 新人进群
     *
     * @param $data
     * @return bool
     */
    public static function intoGroup($data)
    {
        if (empty($data)) {
            return false;
        }
        // 获取群信息 没获取到创建群
        $group_info = MemberInfoByAppID::where('enterprise_group_serial_no', $data['data']['group_serial_no'])->first();
        if (empty($group_info)) {
            $insertInfo = [];
            $insertInfo['action'] = 'report_new_room';
            $insertInfo['appid'] = self::$MERCHANT;
            $insertInfo['wxid'] = '';
            $insertInfo['room_wxid'] = '';
            $insertInfo['name'] = $data['data']['group_name'];
            $insertInfo['owner_wxid'] = '';
            $insertInfo['head_img'] = '';
            $insertInfo['member_count'] = 0;
            $insertInfo['memberInfo_list'] = '';
            $insertInfo['is_follow'] = 0;
            $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
            $insertInfo['chat_room_serial_no'] = '';
            $insertInfo['enterprise_robot_serial_no'] = $data['data']['robot_serial_no'];
            $insertInfo['enterprise_group_serial_no'] = $data['data']['group_serial_no'];
            $insertInfo['group_type'] = empty($data['data']['group_type']) ? 2 : $data['data']['group_type'];
            $group_info = MemberInfoByAppID::create($insertInfo);
            if ($group_info->id > 0) {
                CommunityModel::bindingRoomId('企业微信群（请改成自己想改的群名）', $group_info->id);
            }
        }

        if (!empty($data['data']['members'])) {
            foreach ($data['data']['members'] as $val) {
                //看这个用户是否在这个群
                $is_into = MemberWeixinInfo::where('romm_id', $group_info->id)
                    ->where('enterprise_robot_serial_no', $val['member_serial_no'])
                    ->first();

                if (empty($is_into)) {
                    $insert['romm_id'] = $group_info->id;
                    $insert['wxid'] = '';
                    $insert['room_wxid'] = '';
                    $insert['wx_alias'] = '';
                    $insert['room_nickname'] = $val['member_name'];
                    $insert['nickname'] = $val['member_name'];
                    $insert['head_img'] = $val['member_profile_photo'];
                    $insert['sex'] = '';
                    $insert['country'] = '';
                    $insert['province'] = '';
                    $insert['city'] = '';
                    $insert['inviter'] = '';
                    $insert['is_delete'] = 0;
                    $insert['enterprise_robot_serial_no'] = $val['member_serial_no'];
                    $insert['enterprise_group_serial_no'] = $data['data']['group_serial_no'];
                    $insert['inviter_robot_serial_no'] = $val['inviter_serial_no'];
                    MemberWeixinInfo::create($insert);
                } else {
                    $update['id'] = $is_into->id;
                    $update['room_nickname'] = $val['member_name'];
                    $update['nickname'] = $val['member_name'];
                    $update['head_img'] = $val['member_profile_photo'];
                    MemberWeixinInfo::where('id', $is_into->id)->update($update);
                }
            }
        }
        // 修改群内总人数
        MemberInfoByAppID::where('id', $group_info->id)->update([
            'member_count' => empty($data['data']['member_count']) ? $group_info->member_count : $data['data']['member_count'],
        ]);
        return true;
    }

    /**
     * 用户退群
     *
     * @param $data
     * @return bool
     */
    public static function exitGroup($data)
    {
        if (empty($data)) {
            return false;
        }
        $room_id = 0;
        if (!empty($data['data'])) {
            foreach ($data['data']['members'] as $val) {
                $group_info = MemberInfoByAppID::where('enterprise_group_serial_no', $data['data']['group_serial_no'])->first();
                if (empty($group_info)) {
                    continue;
                }
                $room_id = $group_info->id;

                //看这个用户是否在这个群
                $is_into = MemberWeixinInfo::where('romm_id', $group_info->id)
                    ->where('enterprise_robot_serial_no', $val['member_serial_no'])
                    ->first();

                if (!empty($is_into)) {
                    $update['id'] = $is_into->id;
                    $update['romm_id'] = $group_info->id;
                    $update['is_delete'] = 1;
                    $update['delete_time'] = time();
                    MemberWeixinInfo::where('id', $is_into->id)->update($update);
                }

                //退群机器人
                if ($data['robot_serial_no'] == $val['member_serial_no']) {
                    MemberInfoByAppID::where('id', $group_info->id)->update(
                        [
                            'is_delete' => 1,
                            'deleted_at' => date('Y-m-d H:i:s')
                        ]
                    );
                }
            }
        }
        if ($room_id) {
            $mem_count = MemberInfoByAppID::where('enterprise_group_serial_no', $data['data']['group_serial_no'])->where('is_delete', 0)->count();
            print_r($mem_count);
            MemberInfoByAppID::where('id', $room_id)->update([
                'member_count' => $mem_count
            ]);
        }
        return true;
    }

    /**
     * 同步获取机器人微信群列表
     *
     * @param string $robotSerialNo
     * @return array|bool
     */
    public static function getGroupList($robotSerialNo = '')
    {
        if (empty($robotSerialNo)) {
            return [];
        }
        //获取我的客户列表
        $url = self::$SERVICE_URL . '/wxscrm/Group/List';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'rela_serial_no' => '',
        ];
        $result = self::QinRequest($url, $data);
        // 失败
        if ($result['code'] != 0) {
            return [];
        }
        return $result;
    }

    /**
     * 同步获取机器人客户信息
     *
     * @param string $robotSerialNo
     * @return array|bool
     */
    public static function getCustomerList($robotSerialNo = '', $customer_serial_nos = [])
    {
        if (empty($robotSerialNo) || empty($customer_serial_nos)) {
            return [];
        }
        //获取我的客户列表
        $url = self::$SERVICE_URL . '/wxscrm/contact/GetCustomerInfo';
        $data = [
            'merchant_no' => self::$MERCHANT, //商家编号
            'robot_serial_no' => $robotSerialNo,
            'customer_serial_nos' => $customer_serial_nos,
        ];
        $result = self::QinRequest($url, $data);
        // 失败
        if ($result['code'] != 0) {
            return [];
        }
        return $result;
    }

    /**
     * 记录我的客户列表
     *
     * @param $data
     * @return array
     */
    public static function setMyCustomers($data)
    {
        if (empty($data)) {
            return [];
        }
        exit;
        foreach ($data['data']['members'] as $key => $val) {
            //判断当前机器人的好友列表是否包含 当前好友
            $isFriendData = RobotFriend::where('enterprise_robot_serial_no', $data['robot_serial_no'])
                ->where('enterprise_serial_no', $val['customer_serial_no'])
                ->first();
            if (empty($isFriendData)) {
                $insert = [
                    'robot_wxid' => '',
                    'wxid' => '',
                    'platform_identity' => self::$PLATFORM_IDENTITY,
                    'wx_alias' => '',
                    'nickname' => $val['name'],
                    'head_img' => $val['profile_photo'],
                    'sex' => $val['gender'],
                    'enterprise_robot_serial_no' => $data['robot_serial_no'],
                    'enterprise_serial_no' => $val['customer_serial_no']
                ];
                RobotFriend::insert($insert);
            } else {
                $update = [
                    'nickname' => $val['name'],
                    'head_img' => $val['profile_photo'],
                    'sex' => $val['gender'],
                ];
                $isFriendData->update($update);
            }
        }
    }
}