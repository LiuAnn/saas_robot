<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 2021/1/4
 * Time: 下午6:21
 * Describe: 小程序相关
 */

namespace App\Service\Platform;

use Exception;
use App\Models\Developers;
use Illuminate\Support\Facades\Log;

class MiniProgramService
{
    private $appId;
    private $appSecret;
    # 小程序通过前端请求的code 完成登录流程 并拿到会话秘钥
    private $code2sessionUrl = 'https://api.weixin.qq.com/sns/jscode2session?';

    /**
     * 构造函数
     *
     * WechatService constructor.
     * @param $wechatId
     */
    public function __construct($wechatId)
    {
        $developers = Developers::select('app_id', 'app_secret')
            ->where('id', $wechatId)
            ->where('app_status', 0)
            ->where('deleted_at', 0)
            ->first();
        // 小程序的appId
        $this->appId = $developers->app_id;
        $this->appSecret = $developers->app_secret;
    }

    /**
     * 完成登录流程
     *
     * @param $code
     * @return mixed
     * @throws Exception
     */
    public function getSessionKeyByCode($code)
    {
        $url = $this->code2sessionUrl . "appid=" . $this->appId . "&secret=" . $this->appSecret . "&js_code=" . $code . "&grant_type=authorization_code";
        $res = $this->httpGet($url);

        return json_decode($res, true);
    }

    private function httpGet($url)
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 500);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_URL, $url);
            $res = curl_exec($curl);
            curl_close($curl);
            return $res;
        } catch (Exception $ex) {
            Log::info($ex->getMessage());
            throw new Exception('access_token');
        }
    }

    /**
     * 解密完整用户信息的加密数据
     *
     * @param $encryptedData
     * @param $iv
     * @param $sessionKey
     * @return array
     */
    public function decryptData($encryptedData, $iv, $sessionKey)
    {
        if (strlen($sessionKey) != 24) {
            return ['code' => -41003, 'data' => []];
        }
        $aesKey = base64_decode($sessionKey);

        if (strlen($iv) != 24) {
            return ['code' => -41002, 'data' => []];
        }
        $aesIV = base64_decode($iv);

        $aesCipher = base64_decode($encryptedData);

        $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

        $dataObj = json_decode($result);
        if ($dataObj == NULL) {
            return ['code' => -41003, 'data' => []];
        }
        if ($dataObj->watermark->appid != $this->appId) {
            return ['code' => -41005, 'data' => []];
        }
        return ['code' => 200, 'data' => $dataObj];
    }
}