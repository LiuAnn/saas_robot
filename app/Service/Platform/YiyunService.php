<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/08/31
 * Time: 上午11:43
 */

namespace App\Service\Platform;

use App\Model\Admin\GroupModel;
use App\Model\Integral\GroupIntegralModel;
use App\Models\IntoGroupReply;
use App\Models\MemberInfoByAppID;
use App\Service\Sign\GroupSignService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Models\WhiteListWx;
use App\Models\MemberLoginInfo;
use App\Models\RobotFriend;
use App\Model\Community\CommunityModel;
use Exception;

class YiyunService
{

    //access_token
    protected static $access_token = 'yiyun:access:token';
    //授权地址
    protected static $AUTHORIZED_URL = 'https://merchant.auth.tusepaas.com:8091';
    //服务地址
    protected static $SERVICE_URL = 'https://merchant.wapi.tusepaas.com:8091';
    //商家编号
    protected static $MERCHANT = '202010280000026';
    //商家秘钥
    protected static $SECRET = 'b0a91e0471034c358aad3bed6f32ef5b';

    protected static $PLATFORM_IDENTITY = '7323ff1a23f0bf1cda41f690d4089353';


    /**
     * 接口授权 获取Token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getToken()
    {
        $token = Redis::get(self::$access_token);
        // 没取到缓存里的Token时，在去远程获取
        if (empty($token)) {
            $url = self::$AUTHORIZED_URL . '/api/oauth/get_token';
            $data = [
                'merchant' => self::$MERCHANT,
                'secret' => self::$SECRET,
            ];
            //curl post
            $retData = curlPostCommon($url, $data, []);
            if (empty($retData)) {
                error_log('请求日期：' . date('Y-m-d H:i:s') . ' 返回内容：' . $retData . "\n", 3, storage_path('logs/redis.log'));
                return '';
            }
            $result = json_decode($retData, true);
            //失败
            if ($result['code'] != 1) {
                error_log('请求日期：' . date('Y-m-d H:i:s') . ' 返回内容：' . json_encode($result) . "\n", 3, storage_path('logs/redis.log'));
                return '';
            }
            //把取到的token存入redis，并设置过期时间为2小时
            Redis::set(self::$access_token, $result['token']);
            Redis::expire(self::$access_token, 7200);
            $token = $result['token'];
        }
        return $token;
    }


    /**
     * 登陆
     *
     * @param $wx_alias
     * @param $nRegionCode
     * @return mixed
     * @throws Exception
     */
    public static function login($wx_alias, $nRegionCode)
    {
        //机器人编号（非第一次登录时请求参数中的机器人编号）
        $vcRobotSerialNo = '';
        $user_info = WhiteListWx::select('robot_serial_no')->where('wx_alias', $wx_alias)->first();
        //登录方式 10 扫码登录 11 缓存登录
        $nLoginType = 10;
        if (!empty($user_info)) {
            $vcRobotSerialNo = $user_info->robot_serial_no;
            $nLoginType = 11;
        }

        $url = self::$SERVICE_URL . '/api/Robot/UserLogin';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'nAuthorize' => 1, //平台授权方式 1 平台授权 （使用最新流程的商家，此处请一定输入值：1，否则则将以老的登录方式来走登录流程。）
            'vcRelationSerialNo' => $vcRobotSerialNo,
            'nRegionCode' => $nRegionCode,
            'nLoginType' => $nLoginType,
            //文档没有明确是否必传，和区别'nLoginType' => $nLoginType,
        ];
        $data = json_encode($data);

        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($data) . "",
            "Accept: application/json",
            "token:" . YiyunService::getToken(),
            "merchantNo:" . self::$MERCHANT
        ];
        //curl post
        $retData = self::curlRequest($url, $data, $headers);
        $login_info = json_decode($retData, true);
        // 失败
        if ($login_info['nResult'] != 1) {
            throw new Exception($login_info['vcResult'], $login_info['nResult']);
        }
        return $login_info;
    }

    /**
     * callBack 登陆成功的操作
     *
     * @param $data
     * @return bool
     */
    public static function loginSuccess($data)
    {
        //微信号
        $wx_alias = empty($data['Data']['vcWxAlias']) ? '' : $data['Data']['vcWxAlias'];
        //微信ID
        $wx_id = empty($data['vcRobotWxId']) ? '' : $data['vcRobotWxId'];
        //机器人ID
        $robot_serial_no = empty($data['Data']['vcRobotSerialNo']) ? '' : $data['Data']['vcRobotSerialNo'];

        if (empty($wx_id)) {
            return false;
        }

        //查询用户信息
        $user_info = WhiteListWx::where('wxid', $wx_id)->whereStatus(0)->first();

        // 没有用户信息，新用户逻辑
        if (empty($user_info)) {
            $bind_mid = 921696;
            $order_by = WhiteListWx::select('order_by')->orderBy('id', 'DESC')->first();
            //TODO mid 和 bind_mid 目前写死
            WhiteListWx::insert(
                [
                    'wx_alias' => $wx_alias,
                    'wxid' => $wx_id,
                    'robot_serial_no' => $robot_serial_no,
                    'status' => 0,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'wxid_type' => 3,
                    'is_send_by_ass' => 0,
                    'exclusive_mid' => 0,
                    'platform_identity' => self::$PLATFORM_IDENTITY,
                    'order_by' => $order_by->order_by + 1,
                    'wx_name' => '易云测试机器人' . ($order_by->order_by + 1) . '号',
                    'mid' => 167624,
                    'bind_mid' => $bind_mid,
                    'platform' => 1,
                ]
            );
        } else {
            WhiteListWx::where('wxid', $wx_id)->update([
                'robot_serial_no' => $robot_serial_no
            ]);
        }

        //记录登陆log
        self::addLoginLog($data, 1);
        return true;
    }

    public static function curlRequest($url, $data = null, $header = null)
    {
        //初始化浏览器
        $ch = curl_init();
        //设置浏览器，把参数url传到浏览器的设置当中
        curl_setopt($ch, CURLOPT_URL, $url);
        //以字符串形式返回到浏览器当中
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //禁止https协议验证域名，0就是禁止验证域名且兼容php5.6
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //禁止https协议验证ssl安全认证证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //判断data是否有数据，如果有data数据传入那么就把curl的请求方式设置为POST请求方式
        if (!empty($data)) {
            //设置POST请求方式
            @curl_setopt($ch, CURLOPT_POST, true);
            //设置POST的数据包
            @curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        //设置header头
        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        //让curl发起请求
        $str = curl_exec($ch);
        //关闭curl浏览器
        curl_close($ch);
        //把请求回来的数据返回
        return $str;
    }

    /**
     * 记录登陆日志
     *
     * @param $data
     * @param $type 1：登陆、2：登出
     * @return bool
     */
    public static function addLoginLog($data, $type)
    {
        //记录登陆log
        MemberLoginInfo::insert(
            [
                'appid' => self::$MERCHANT,
                'wxid' => $data['Data']['vcRobotWxId'],
                'machine_id' => '',
                'client_version' => '',
                'province' => empty($data['Data']['vcProvince']) ? '' : $data['Data']['vcProvince'],
                'city' => empty($data['Data']['vcCity']) ? '' : $data['Data']['vcCity'],
                'head_img' => empty($data['Data']['vcHeadImgUrl']) ? '' : $data['Data']['vcHeadImgUrl'],
                'nickname' => empty($data['Data']['vcNickName']) ? '' : $data['Data']['vcNickName'],
                'nonce' => '',
                'wx_alias' => empty($data['Data']['vcWxAlias']) ? '' : $data['Data']['vcWxAlias'],
                'sex' => empty($data['Data']['sex']) ? '' : $data['Data']['sex'],
                'type' => $type,
            ]
        );
        return true;
    }


    /**
     * 获取机器人群列表
     *
     * @param $robotSerialNo 机器人编号
     * @param string $chatRoomSerialNo 群编号 (不传查全部)
     * @param int $is_openMessage 是否开通 (10 是 11 否, 0 查询全部)
     * @return array
     */
    public static function getGroup($robotSerialNo, $chatRoomSerialNo = '', $is_openMessage = 0)
    {
        $url = self::$SERVICE_URL . '/api/ChatRoom/GetChatRoomList';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $robotSerialNo,
            'vcChatRoomSerialNo' => $chatRoomSerialNo,
            'isOpenMessage' => $is_openMessage,
            //文档没有明确是否必传，和区别'nLoginType' => $nLoginType,
        ];
        return self::QinRequest($url, $data);
    }

    /**
     * 获取机器人好友列表
     *
     * @param $merchantNo
     * @param $robotSerialNo
     * @param string $chatRoomSerialNo
     * @param int $is_openMessage
     * @return bool
     */
    public static function getMemberList($merchantNo, $robotSerialNo, $chatRoomSerialNo = '', $is_openMessage = 0)
    {
        $url = self::$SERVICE_URL . '/api/Friend/GetFriendList';
        $data = [
            'vcMerchantNo' => $merchantNo, //商家编号
            'vcRobotSerialNo' => $robotSerialNo,
        ];
        return self::QinRequest($url, $data);
    }


    /**
     * 上报机器人好友列表的同时上报群信息
     *
     * @param $data
     * @param $wxid
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public static function setFriendAndGroupList($data, $wxid)
    {
        $user_info = WhiteListWx::select('platform_identity')->where('wxid', $data['vcRobotWxId'])->first();
        if (empty($user_info)) {
            return false;
        }
        $platform_identity = $user_info->platform_identity;

        if (empty($data['Data']['FriendList'])) {
            return false;
        }

        //用户好友入库
        foreach ($data['Data']['FriendList'] as $key => $userInfo) {
            //判断当前机器人的好友列表是否包含 当前好友
            $isFriendData = RobotFriend::where('wxid', $userInfo['vcFriendWxId'])
                ->where('robot_wxid', $wxid)
                ->first();
            if (empty($isFriendData)) {
                $insert = [
                    'robot_wxid' => $wxid,
                    'wxid' => $userInfo['vcFriendWxId'],
                    'platform_identity' => $platform_identity,
                    'wx_alias' => $userInfo['vcWxAlias'],
                    'nickname' => $userInfo['vcNickName'],
                    'head_img' => $userInfo['vcHeadImgUrl'],
                    'sex' => $userInfo['nSex'],
                    'country' => $userInfo['vcCountry'],
                    'province' => $userInfo['vcProvince'],
                    'city' => $userInfo['vcCity'],
                ];
                RobotFriend::insert($insert);
            } else {
                $update = [
                    'robot_wxid' => $wxid,
                    'wxid' => $userInfo['vcFriendWxId'],
                    'platform_identity' => $platform_identity,
                    'wx_alias' => $userInfo['vcWxAlias'],
                    'nickname' => $userInfo['vcNickName'],
                    'head_img' => $userInfo['vcHeadImgUrl'],
                    'sex' => $userInfo['nSex'],
                    'country' => $userInfo['vcCountry'],
                    'province' => $userInfo['vcProvince'],
                    'city' => $userInfo['vcCity'],
                ];
                $isFriendData->update($update);
            }
        }

        $isOpenMessage = 10;
        $vcChatRoomSerialNo = '';

        //获取群信息
        $url = self::$SERVICE_URL . '/api/ChatRoom/GetChatRoomList';
        $param = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $data['vcRobotSerialNo'],//机器人编号
            'vcChatRoomSerialNo' => $vcChatRoomSerialNo, //群编号 不传查全部
            'isOpenMessage' => $isOpenMessage, //是否关注群0 全部，10 是，11 否
        ];
        $param = json_encode($param);

        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($param) . "",
            "Accept: application/json",
            "token:" . YiyunService::getToken(),
            "merchantNo:" . self::$MERCHANT
        ];
        //curl post
        $retData = self::curlRequest($url, $param, $headers);
        $group_list = json_decode($retData, true);
        // 失败
        if ($group_list['nResult'] != 1) {
            error_log('群列表获取失败：' . $retData . "\n", 3, storage_path('logs/callback-error.log'));
            return false;
        }

        if (is_array($group_list['Data'])) {
            foreach ($group_list['Data'] as $key => $rooInfo) {
                $isSetId = CommunityModel::getRoomMemberMId($data['vcMerchantNo'], $rooInfo['vcChatRoomId']);
                if (!empty($isSetId) && !empty($rooInfo['vcName'])) {
                    if ($rooInfo['nRobotInStatus'] != 0) {
                        continue;
                    }
                    $updatedData = [];
                    $updatedData['id'] = $isSetId->id;
                    $updatedData['action'] = 'report_room_member_info';
                    $updatedData['appid'] = self::$MERCHANT;
                    $updatedData['wxid'] = $wxid;
                    $updatedData['room_wxid'] = $rooInfo['vcChatRoomId'];
                    $updatedData['name'] = $rooInfo['vcName'];
                    $updatedData['owner_wxid'] = $rooInfo['vcAdminWxId'];
                    $updatedData['head_img'] = $rooInfo['vcHeadImg'];
                    $updatedData['member_count'] = $rooInfo['nUserCount'];
                    $updatedData['memberInfo_list'] = '';
                    $updatedData['is_delete'] = 0;
                    $updatedData['chat_room_serial_no'] = $rooInfo['vcChatRoomSerialNo'];
                    $updatedData['updated_at'] = date('Y-m-d H:i:s', time());
                    $res = CommunityModel::updateWxidMemberTableData($updatedData);
                    if ($res && !empty($rooInfo['vcName']) && empty($isSetId->mid)) {
                        CommunityModel::bindingRoomId($rooInfo['vcName'], $isSetId->id);
                    }
                } else {
                    if (!empty($rooInfo['vcName'])) {
                        $insertInfo = [];
                        $insertInfo['action'] = 'report_room_member_info';
                        $insertInfo['appid'] = self::$MERCHANT;
                        $insertInfo['wxid'] = $wxid;
                        $insertInfo['room_wxid'] = $rooInfo['vcChatRoomId'];
                        $insertInfo['name'] = $rooInfo['vcName'];
                        $insertInfo['owner_wxid'] = $rooInfo['vcAdminWxId'];
                        $insertInfo['head_img'] = $rooInfo['vcHeadImg'];
                        $insertInfo['member_count'] = $rooInfo['nUserCount'];
                        $insertInfo['memberInfo_list'] = '';
                        $insertInfo['tag_id'] = 11;//默认移除标签
                        $insertInfo['chat_room_serial_no'] = $rooInfo['vcChatRoomSerialNo'];
                        $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
                        $id = CommunityModel::insertGetId('member_info_by_appid', $insertInfo);
                        if ($id > 0) {
                            CommunityModel::bindingRoomId($rooInfo['vcName'], $id);
                        }
                    }
                }
            }
        }
        return true;
    }


    /**
     * 创建群(暂时没用 后期如果有平台创建群可以用这个方法)
     *
     * @param $robot_id
     * @param $room_name
     * @param $friend_list
     * @param string $ChatAdminType
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createGroup($robot_id, $room_name, $friend_list, $ChatAdminType = '')
    {
        //('E186EB2B0BF55D218AB25C994B7BEBB5','测试创建群001','E186EB2B0BF55D218AB25C994B7BEBB5,31A9D55BEEBAD9B09C13333C52C3CA5D,A4361FD74B73CAC0812F68C723AD97F7,3D6AEA78BFCD49A768CC11BC7DFF4B7D');

        //获取群信息
        $url = self::$SERVICE_URL . '/api/ChatRoom/RobotCreateChatRoom';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $robot_id,
            'vcChatRoomName' => $room_name,
            'vcFriendSerialNos' => $friend_list
        ];
        self::QinRequest($url, $data);
    }

    /**
     * 机器人退出登陆
     *
     * @param $data
     * @return array
     */
    public static function logout($data)
    {
        if (empty($data['vcRobotWxId'])) {
            return [];
        }
        //获取用户信息，然后再插入
        $user_info = WhiteListWx::where('wxid', $data['vcRobotWxId'])->first();
        $log_data['Data']['vcRobotWxId'] = $data['vcRobotWxId'];
        $log_data['Data']['province'] = '';
        $log_data['Data']['vcCity'] = '';
        $log_data['Data']['nickname'] = $user_info->wx_name;
        $log_data['Data']['head_img'] = '';
        $log_data['Data']['wx_alias'] = $user_info->wx_alias;
        $log_data['Data']['sex'] = '';

        self::addLoginLog($log_data, 2);
        return true;
    }

    /**
     * @param string $robotSerialNo 机器人编号
     * @param string $chatRoomSerialNo 群编号
     * @param array $push_data
     * @return bool
     */
    public static function sendInfo($robotSerialNo = '', $chatRoomSerialNo = '', $push_data = [])
    {
        if (empty($robotSerialNo) || empty($chatRoomSerialNo) || empty($push_data)) {
            return false;
        }

        //获取群信息
        $url = self::$SERVICE_URL . '/api/ChatMessages/SendGroupChatMessages';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $robotSerialNo,
            'vcRelaSerialNo' => '',
            'vcChatRoomSerialNo' => $chatRoomSerialNo,
            'data' => $push_data
        ];
        return self::QinRequest($url, $data);
    }


    /**
     * 新用户进群
     */
    public static function getIntoGroup($data)
    {
        if (empty($data)) {
            return false;
        }
        // GET 群信息
        $group_info = MemberInfoByAppID::select('id', 'tag_id')
            ->where('is_delete', 0)
            ->where('room_wxid', $data['Data']['vcChatRoomId'])
            ->first();
        if (empty($group_info)) {
            return false;
        }
        // GET 机器人信息
        $user_info = WhiteListWx::where('status', 0)->where('wxid', $data['vcRobotWxId'])->first();
        if (empty($user_info)) {
            return false;
        }

        // GET 是否有符合条件的进群回复语
        $reply_message_data = IntoGroupReply::select('title', 'style', 'type', 'tag_id', 'image', 'content', 'reply_address', 'platform_identity')
            ->where('tag_id', $group_info->tag_id)
            ->where('platform_identity', $user_info->platform_identity)
            ->first();
        // SET 进群回复开始
        if ($reply_message_data) {
            // 推送基础字端
            $push_data = [
                'nMsgNum' => 1,
                'nMsgType' => 2001,
                'msgContent' => '',
                'nVoiceTime' => 0,
                'nIsHit' => 0,
                'nAtLocation' => 1,
                'vcHref' => '',
                'vcTitle' => '',
                'vcDesc' => '',
            ];

            // 源消息内容
            $content = $reply_message_data->content;
            // GET 欢迎语内容是否有需要替换的标签
            if (strpos($content, '【:user_name】')) {
                if (!empty($data['Data']['Members'])) {
                    // 需要推送的用户编号
                    $atSerialNos = [];
                    // SET 需要推送的内容替换
                    foreach ($data['Data']['Members'] as $key => $val) {
                        // TODO 后期可以改为多个参数，@多个人等
                        $content = str_replace('【:user_name】', '@$' . $val['vcMemberUserSerialNo'] . '$', $content);
                        // SET 多人同时进群时，组合用户编号
                        array_push($atSerialNos, $val['vcMemberUserSerialNo']);
                    }
                    $push_data['nIsHit'] = 2;
                    $push_data['nAtLocation'] = 2;
                    $push_data['vcAtWxSerialNos'] = $atSerialNos;
                }
            }
            $push_data['msgContent'] = $content;
            // 进群欢迎语推送
            self::sendInfo($data['vcRobotSerialNo'], $data['Data']['vcChatRoomSerialNo'], [$push_data]);
        }

        if (!empty($data['Data']['Members'])) {
            foreach ($data['Data']['Members'] as $val) {
                //看这个用户是否在这个群
                $is_into = CommunityModel::isSetWeiXinInfoRoom([
                        'wxid' => $val['vcMemberUserWxId'],
                        'room_wxid' => $data['Data']['vcChatRoomId'],
                        'room_id' => $group_info->id,
                    ]
                );
                if (empty($is_into)) {
                    $insert['romm_id'] = $group_info->id;
                    $insert['wxid'] = $val['vcMemberUserWxId'];
                    $insert['room_wxid'] = $data['Data']['vcChatRoomId'];
                    $insert['wx_alias'] = '';
                    $insert['room_nickname'] = $val['vcGroupNickName'];
                    $insert['nickname'] = $val['vcNickName'];
                    $insert['head_img'] = $val['vcHeadImgUrl'];
                    $insert['sex'] = '';
                    $insert['country'] = '';
                    $insert['province'] = '';
                    $insert['city'] = '';
                    $insert['inviter'] = $val['vcFatherWxId'];
                    $insert['is_delete'] = 0;
                    $insert['user_serial_no'] = $val['vcMemberUserSerialNo'];
                    CommunityModel::insertGetId('member_weixin_info', $insert);
                } else {
                    $update['id'] = $is_into->id;
                    $update['romm_id'] = $group_info->id;
                    $update['wxid'] = $val['vcMemberUserWxId'];
                    $update['room_wxid'] = $data['Data']['vcChatRoomId'];
                    $update['room_nickname'] = $val['vcGroupNickName'];
                    $update['nickname'] = $val['vcNickName'];
                    $update['head_img'] = $val['vcHeadImgUrl'];
                    $update['is_delete'] = 0;
                    $update['user_serial_no'] = $val['vcMemberUserSerialNo'];
                    $update['delete_time'] = '';
                    $update['inviter'] = $val['vcFatherWxId'];
                    CommunityModel::changeWeixinInfoByWxid($update);
                }

            }
        }
        CommunityModel::upByAppIdMid(['id' => $group_info->id, 'member_count' => $data['Data']['nMemberCount'], 'wxid' => $data['vcRobotWxId'], 'is_delete' => 0]);
        $pushData = [];
        foreach ($data['Data']['Members'] as $v) {
            $inviteInsertData = [];
            $inviteInsertData['room_id'] = $group_info->id;
            $inviteInsertData['flag'] = 1;
            $inviteInsertData['inviter'] = $v['vcFatherWxId'] ?? '';
            if (empty($inviteInsertData['inviter'])) {
                continue;
            }
            $inviteInsertData['member_wxid'] = $v['vcMemberUserWxId'];
            $inviteInsertData['platform_identity'] = $user_info->platform_identity;
            $inviteInsertData['room_wxid'] = $data['Data']['vcChatRoomId'];
            $pushData = GroupSignService::insertGroupInviteRecord($inviteInsertData, $user_info);
            if (!empty($pushData)) {
                if (empty($inviteInsertData['inviter'])) {
                    $pushData['vcAtWxSerialNos'] = '';
                    $pushData['nAtLocation'] = 0;
                    $pushData['nIsHit'] = 0;
                } else {
                    $pushData['vcAtWxSerialNos'] = [$v['vcFatherWxUserSerialNo']];
                    $pushData['nAtLocation'] = 0;
                    $pushData['nIsHit'] = 2;
                }
                Log::info('群邀请好友回复探鲸--' . json_encode($pushData));
                self::sendInfo($data['vcRobotSerialNo'], $data['Data']['vcChatRoomSerialNo'], [$pushData]);
            }
        }
        return true;
    }

    /**
     * 用户退群
     *
     * @param $data
     * @return bool
     */
    public static function exitGroup($data)
    {
        if (empty($data)) {
            return false;
        }
        $room_id = 0;
        if (!empty($data['Data'])) {
            foreach ($data['Data'] as $val) {
                $room_wxid = $val['vcChatRoomId'];
                $roomInfo = GroupModel::getRoomIdByRoomWxId($room_wxid);
                $room_id = $roomInfo['id'] ?? 0;
                //$room_id = CommunityModel::getIdByRoomId($val['vcChatRoomId']);
                if (empty($room_id)) {
                    continue;
                }
                //看这个用户是否在这个群
                $is_into = CommunityModel::isSetWeiXinInfoRoom([
                        'wxid' => $val['vcMemberUserWxId'],
                        'room_wxid' => $val['vcChatRoomId'],
                        'room_id' => $room_id,
                    ]
                );
                if (!empty($is_into)) {
                    $update['id'] = $is_into->id;
                    $update['romm_id'] = $room_id;
                    $update['wxid'] = $val['vcMemberUserWxId'];
                    $update['room_wxid'] = $val['vcChatRoomId'];
                    $update['is_delete'] = 1;
                    $update['delete_time'] = time();
                    CommunityModel::changeWeixinInfoByWxid($update);
                }

                //退群机器人
                if ($data['vcRobotWxId'] == $val['vcMemberUserWxId']) {
                    CommunityModel::upByAppIdMid(['id' => $room_id, 'wxid' => $data['vcRobotWxId'], 'is_delete' => 1, 'deleted_at' => date('Y-m-d H:i:s')]);
                } else {

                    $user_info = WhiteListWx::where('wxid', $data['vcRobotWxId'])->first();
                    $inviteInsertData = [];
                    $inviteInsertData['room_id'] = $room_id;
                    $inviteInsertData['flag'] = 0;
                    $inviteInsertData['member_wxid'] = $val['vcMemberUserWxId'];

                    $inviteWhere = [];
                    $inviteWhere['romm_id'] = $room_id;
                    $inviteWhere['wxid'] = $inviteInsertData['member_wxid'];
                    $inviterWxid = GroupIntegralModel::getInviterByRoomWxIdAndWxid($inviteWhere);
                    if (empty($inviterWxid)) {
                        continue;
                    }
                    $inviteWhere['wxid'] = $inviterWxid;
                    $inviterSerialNo = GroupIntegralModel::getInviterSerialNoByRoomWxIdAndWxid($inviteWhere);
                    if (empty($inviterSerialNo)) {
                        continue;
                    }
                    $inviteInsertData['inviter'] = $inviterWxid;

                    $inviteInsertData['platform_identity'] = $user_info->platform_identity;
                    $inviteInsertData['room_wxid'] = $room_wxid;
                    $pushData = GroupSignService::insertGroupInviteRecord($inviteInsertData, $user_info);
                    if (!empty($pushData)) {
                        if (empty($inviterWxid)) {
                            $pushData['vcAtWxSerialNos'] = '';
                            $pushData['nAtLocation'] = 0;
                            $pushData['nIsHit'] = 0;
                        } else {
                            $pushData['vcAtWxSerialNos'] = [$inviterSerialNo];
                            $pushData['nAtLocation'] = 0;
                            $pushData['nIsHit'] = 2;
                        }
                        Log::info('群邀请好友回复探鲸--' . json_encode($pushData));
                        self::sendInfo($data['vcRobotSerialNo'], $val['vcChatRoomSerialNo'], [$pushData]);
                    }
                }
                //TODO 取消关注
            }
        }
        if ($room_id) {
            $mem_count = CommunityModel::getNewMemCount($room_id);
            CommunityModel::upByAppIdMid(['id' => $room_id, 'member_count' => $mem_count, 'wxid' => $data['vcRobotWxId']]);
        }
        return true;
    }

    /**
     * 机器人关注群
     *
     * @param string $robotSerialNo 机器人编号
     * @param string $chatRoomSerialNo 群编号
     * @return bool
     */
    public static function follow($robotSerialNo = '', $chatRoomSerialNo = '')
    {
        if (empty($robotSerialNo) || empty($chatRoomSerialNo)) {
            return false;
        }

        $url = self::$SERVICE_URL . '/api/ChatRoom/RobotChatRoomOpen';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $robotSerialNo,
            'vcChatRoomSerialNo' => $chatRoomSerialNo,
        ];
        self::QinRequest($url, $data);
    }

    /**
     * 机器人被好友拉进群
     *
     * @param $data
     * @return bool
     */
    public static function robotIntoGroup($data)
    {
        if (empty($data)) {
            return false;
        }

        // 先看群存不存在，不存在先创建群
        $room_id = CommunityModel::getIdByRoomId($data['Data']['vcChatRoomId']);

        if (empty($room_id)) {
            $insertInfo = [];
            $insertInfo['action'] = 'report_room_member_info';
            $insertInfo['appid'] = self::$MERCHANT;
            $insertInfo['wxid'] = $data['vcRobotWxId'];
            $insertInfo['room_wxid'] = $data['Data']['vcChatRoomId'];
            $insertInfo['name'] = $data['Data']['vcChatRoomName'];
            $insertInfo['owner_wxid'] = $data['Data']['vcAdminWxId'];
            $insertInfo['head_img'] = $data['Data']['vcHeadImg'];
            $insertInfo['member_count'] = $data['Data']['nUserCount'];
            $insertInfo['memberInfo_list'] = '';
            $insertInfo['is_follow'] = 1;
            $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
            $insertInfo['chat_room_serial_no'] = $data['Data']['vcChatRoomSerialNo'];
            $room_id = CommunityModel::insertGetId('member_info_by_appid', $insertInfo);
            if ($room_id > 0) {
                CommunityModel::bindingRoomId($data['Data']['vcChatRoomName'], $room_id);
            }
        } else {
            MemberInfoByAppID::where('id', $room_id)->update([
                'is_follow' => 1,
                'wxid' => $data['vcRobotWxId'],
                'member_count' => $data['Data']['nUserCount'],
                'head_img' => $data['Data']['vcHeadImg'],
            ]);
        }

        if (!empty($data['Data'])) {
            //看这个用户是否在这个群
            $is_into = CommunityModel::isSetWeiXinInfoRoom([
                    'wxid' => $data['vcRobotWxId'],
                    'room_wxid' => $data['Data']['vcChatRoomId'],
                    'room_id' => $room_id
                ]
            );

            if (empty($is_into)) {
                $insert['romm_id'] = $room_id;
                $insert['wxid'] = $data['vcRobotWxId'];
                $insert['room_wxid'] = $data['Data']['vcChatRoomId'];
                $insert['wx_alias'] = '';
                $insert['room_nickname'] = '';
                $insert['nickname'] = $data['Data']['vcNickName'];
                $insert['head_img'] = $data['Data']['vcHeadImgUrl'];
                $insert['sex'] = '';
                $insert['country'] = '';
                $insert['province'] = '';
                $insert['city'] = '';
                $insert['inviter'] = $data['Data']['vcFriendWxId'];
                $insert['is_delete'] = 0;
                CommunityModel::insertGetId('member_weixin_info', $insert);
            } else {
                $update['nickname'] = $data['Data']['vcNickName'];
                $update['head_img'] = $data['Data']['vcHeadImgUrl'];
                CommunityModel::updateWxUserInfo($is_into->id, $update);
            }

            //看白名单有没有机器人信息(平台机器人的逻辑)
            $white_list = WhiteListWx::where('wxid', $data['vcRobotWxId'])->first();
            if (empty($white_list)) {
                $bind_mid = 921696;
                $order_by = WhiteListWx::select('order_by')->orderBy('id', 'DESC')->first();
                //TODO mid 和 bind_mid 目前写死
                WhiteListWx::insert(
                    [
                        'wx_alias' => '',
                        'wxid' => $data['vcRobotWxId'],
                        'robot_serial_no' => $data['vcRobotSerialNo'],
                        'status' => 0,
                        'created_at' => date('Y-m-d H:i:s', time()),
                        'wxid_type' => 3,
                        'is_send_by_ass' => 0,
                        'exclusive_mid' => 0,
                        'platform_identity' => self::$PLATFORM_IDENTITY,
                        'order_by' => $order_by->order_by + 1,
                        'wx_name' => '易云测试机器人' . ($order_by->order_by + 1) . '号',
                        'mid' => 167624,
                        'bind_mid' => $bind_mid,
                        'platform' => 1,
                    ]
                );
            } else {
                WhiteListWx::where('wxid', $data['vcRobotWxId'])->update([
                    'robot_serial_no' => $data['vcRobotSerialNo']
                ]);
            }
            //机器人关注群
            self::follow($data['vcRobotSerialNo'], $data['Data']['vcChatRoomSerialNo']);
        }
        // 机器人进群拉取群用户信息
        self::getChatRoomUserInfo($data);
        CommunityModel::upByAppIdMid(['id' => $room_id, 'member_count' => $data['Data']['nUserCount'], 'wxid' => $data['vcRobotWxId'], 'is_delete' => 0]);
        return true;
    }

    /**
     * 添加平台机器人好友后，通过接口同意
     */
    public static function adoptFriend($data)
    {
        $url = self::$SERVICE_URL . '/api/Friend/AcceptNewFriendRequest';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $data['vcRobotSerialNo'],//要添加的机器人编号
            'vcSerialNo' => $data['vcSerialNo'],//回调的添加好友操作编号
        ];
        self::QinRequest($url, $data);
        return true;
    }


    /**
     * 机器人被踢出群
     *
     * @param $data
     * @return bool
     */
    public static function kickOutGroup($data)
    {
        if (empty($data)) {
            return false;
        }

        $room_id = 0;
        if (!empty($data['Data'])) {
            $room_id = CommunityModel::getIdByRoomId($data['Data']['vcChatRoomId']);
            if (empty($room_id)) {
                return false;
            }
            //看这个用户是否在这个群
            $is_into = CommunityModel::isSetWeiXinInfoRoom([
                    'wxid' => $data['vcRobotWxId'],
                    'room_wxid' => $data['Data']['vcChatRoomId'],
                    'room_id' => $room_id,
                ]
            );

            if (!empty($is_into)) {
                $update['id'] = $is_into->id;
                $update['romm_id'] = $room_id;
                $update['wxid'] = $data['vcRobotWxId'];
                $update['room_wxid'] = $data['Data']['vcChatRoomId'];
                $update['is_delete'] = 1;
                $update['delete_time'] = time();
                CommunityModel::changeWeixinInfoByWxid($update);
            }
        }

        if ($room_id) {
            $mem_count = CommunityModel::getNewMemCount($room_id);
            CommunityModel::upByAppIdMid(['id' => $room_id, 'member_count' => $mem_count, 'wxid' => $data['vcRobotWxId'], 'is_delete' => 1, 'deleted_at' => date('Y-m-d H:i:s')]);
        }
        return true;
    }

    /**
     * 拉机器人进群需要手动通过场景
     */
    public static function robotPullGroupAdopt($data)
    {
        $url = self::$SERVICE_URL . '/api/ChatRoom/RobotPullGroupAdopt';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $data['vcRobotSerialNo'],//机器人编号
            'vcSerialNo' => $data['vcSerialNo'],//回调的操作编号
        ];
        self::QinRequest($url, $data);
        return true;
    }


    /**
     * 拉取群用户信息通知
     *
     * @param $data
     * @return bool
     */
    public static function getChatRoomUserInfo($data)
    {
        $url = self::$SERVICE_URL . '/api/ChatRoom/GetChatRoomUserInfo';

        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $data['vcRobotSerialNo'],//机器人编号
            'vcChatRoomSerialNo' => $data['Data']['vcChatRoomSerialNo'],//群编号
        ];
        self::QinRequest($url, $data);
        return true;
    }


    /**
     * 群内实时消息回调-关键词回复
     *
     * @param $data
     * @return bool
     */
    public static function reportMessageMsg($data)
    {
//        if ($data['Data']['nMsgType'] == 2013 && $data['Data']['vcFromWxUserWxId'] == 'E186EB2B0BF55D218AB25C994B7BEBB5') {
//            //测试群 roomId 日志记录
//            $insertData ['appid'] = '91b2f476922e89a6';
//            $insertData ['wxid'] = $data['vcRobotWxId'];
//            $insertData ['is_send'] = 1;
//            $insertData ['room_wxid'] = $data['Data']['vcChatRoomId'];
//            $insertData ['msg_type'] = 2013;
//            $insertData ['wxid_from'] = $data['Data']['vcChatRoomId'];
//            $insertData ['wxid_to'] = $data['Data']['vcChatRoomId'];
//            $insertData ['data_info'] = $data['Data']['vcContent'];
//            CommunityModel::insertTableData('report_msg', $insertData);
//        }
        if ($data['Data']['nMsgType'] == 2001 && strlen(base64_decode($data['Data']['vcContent'])) < 30) {
            $message = base64_decode($data['Data']['vcContent']);
            Log::info('探鲸机器人关键词回复1 ' . $message);

            $user_info = WhiteListWx::where('wxid', $data['vcRobotWxId'])->first();
            if (empty($user_info)) {
                return false;
            }
            $platform_identity = $user_info->platform_identity;
            //签到保存记录  签到关键词判断放在里边去判断
            if (1) {
                $roomInfo = GroupModel::getRoomIdByRoomWxId($data['Data']['vcChatRoomSerialNo'], 1);
                //群签到开关 1开启 2关闭
                if ($roomInfo && $roomInfo['sign_switch'] == 1) {
                    $roomId = $roomInfo['id'] ?: 0;
                    if (empty($roomId)) {
                        Log::info('群信息未获取 平台1 wxid:' . $data['Data']['vcChatRoomSerialNo']);
                    }
                    $signInsertData = [];
                    $signInsertData['room_id'] = $roomId;
                    $signInsertData['member_wxid'] = $data['Data']['vcFromWxUserWxId'];
                    $signInsertData['platform_identity'] = $platform_identity;
                    $signInsertData['report'] = trim($message);
                    $pushData = GroupSignService::insertGroupSignRecord($signInsertData, $user_info);
                    Log::info('群签到日志记录探鲸--' . json_encode($message));
                    if (!empty($pushData)) {
                        if (empty($data['Data']['vcFromWxUserSerialNo'])) {
                            $pushData['vcAtWxSerialNos'] = '';
                            $pushData['nAtLocation'] = 0;
                            $pushData['nIsHit'] = 0;
                        } else {
                            $pushData['vcAtWxSerialNos'] = [$data['Data']['vcFromWxUserSerialNo']];
                            $pushData['nAtLocation'] = 0;
                            $pushData['nIsHit'] = 2;
                        }
                        Log::info('群签到日志记录探鲸--' . json_encode($pushData));
                        self::sendInfo($data['vcRobotSerialNo'], $data['Data']['vcChatRoomSerialNo'], [$pushData]);
                    }
                }
            }

            $resData = CommunityModel::geNewFriendReturnDataV1($data['Data']['vcChatRoomId'], $platform_identity, $message);
            Log::info('探鲸机器人关键词回复2 ' . json_encode($resData));
            if (!empty($resData)) {


                foreach ($resData as $v) {
                    if ($v->type == 1 && !empty($v->content)) {
                        $msg_list[] = [
                            'nMsgType' => 2001,
                            'msgContent' => $v->content,
                        ];
                    } else if ($v->type == 2 && !empty($v->content)) {
                        if (empty($v->content)) {
                            continue;
                        }
                        $msg_list[] = [
                            'nMsgType' => 2002,
                            'msgContent' => $v->content,
                        ];
                    } else if ($v->type == 3 && !empty($v->content)) {
                        if (empty($v->content)) {
                            continue;
                        }
                        $msg_list[] = [
                            'nMsgType' => 2003,
                            'msgContent' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-02-17/13/yuelvhuiQJwXComJ821581918103.jpg',
                            'nVoiceTime' => 10,
                            'vcHref' => $v->content,
                        ];
                    } else if ($v->type == 4 && !empty($v->content)) {
                        if (empty($v->content)) {
                            continue;
                        }
                        $msg_list[] = [
                            'nMsgType' => 2004,
                            'msgContent' => $v->content,
                        ];
                    } else if ($v->type == 8 && !empty($v->content)) {
                        if (empty($v->content)) {
                            continue;
                        }
                        $msg_list[] = [
                            'nMsgType' => 2013,
                            'msgContent' => $v->content,
                        ];
                    }
                }
            }


            if (!empty($msg_list)) {

                foreach ($msg_list as $key => $val) {
                    // 内容中需要替换字符串为@用户时根据要求替换格式

                    $push_data = [
                        'nMsgNum' => 1,
                        'nMsgType' => 2001,
                        'msgContent' => '',
                        'nVoiceTime' => 0,
                        'nIsHit' => 0,
                        'nAtLocation' => 1,
                        'vcHref' => '',
                        'vcTitle' => '',
                        'vcDesc' => '',
                    ];

                    $push_data['nMsgType'] = $msg_list[$key]['nMsgType'];

                    if (strpos($val['msgContent'], '【:user_name】')) {
                        $push_data['msgContent'] = str_replace('【:user_name】', "@$" . $data['Data']['vcFromWxUserSerialNo'] . "$", $val['msgContent']);
                        $push_data['vcAtWxSerialNos'] = [$data['Data']['vcFromWxUserSerialNo']];
                        $push_data['nAtLocation'] = 2;
                        $push_data['nIsHit'] = 2;
                    } else {


                        $push_data['msgContent'] = $msg_list[$key]['msgContent'];
                        $push_data['nVoiceTime'] = isset($msg_list[$key]['nVoiceTime']) ? $msg_list[$key]['nVoiceTime'] : 0;
                        //小程序没有封面图,则不传这个封面图字段
                        if ($push_data['nMsgType'] == 2013) {
                            if (isset($msg_list[$key]['vcHref']) && empty($msg_list[$key]['vcHref'])) {
                                $push_data['vcHref'] = $msg_list[$key]['vcHref'];
                            }
                        } else {
                            $push_data['vcHref'] = isset($msg_list[$key]['vcHref']) ? $msg_list[$key]['vcHref'] : '';
                        }

                    }


                    Log::info('探鲸机器人关键词回复3 ' . json_encode($push_data));

                    self::sendInfo($data['vcRobotSerialNo'], $data['Data']['vcChatRoomSerialNo'], [$push_data]);
                }
            }
        }

    }

    /**
     * 群用户信息回调
     *
     * @param $data
     * @return bool
     */
    public static function getGroupUserInfo($data)
    {
        if (empty($data)) {
            return false;
        }
        $room_id = CommunityModel::getIdByRoomId($data['Data']['vcChatRoomId']);
        if (empty($room_id)) {
            return false;
        }
        if (!empty($data['Data']['Members'])) {
            foreach ($data['Data']['Members'] as $val) {
                //看这个用户是否在这个群
                $is_into = CommunityModel::isSetWeiXinInfoRoom([
                        'wxid' => $val['vcMemberUserWxId'],
                        'room_wxid' => $data['Data']['vcChatRoomId'],
                        'room_id' => $room_id,
                    ]
                );
                if (empty($is_into)) {
                    $insert['romm_id'] = $room_id;
                    $insert['wxid'] = $val['vcMemberUserWxId'];
                    $insert['room_wxid'] = $data['Data']['vcChatRoomId'];
                    $insert['wx_alias'] = '';
                    $insert['room_nickname'] = $val['vcGroupNickName'];
                    $insert['nickname'] = $val['vcNickName'];
                    $insert['head_img'] = $val['vcHeadImgUrl'];
                    $insert['sex'] = '';
                    $insert['country'] = '';
                    $insert['province'] = '';
                    $insert['city'] = '';
                    $insert['inviter'] = $val['vcFatherWxId'];
                    $insert['is_delete'] = 0;
                    $insert['user_serial_no'] = $val['vcMemberUserSerialNo'];
                    CommunityModel::insertGetId('member_weixin_info', $insert);
                } else {
                    $update['id'] = $is_into->id;
                    $update['romm_id'] = $room_id;
                    $update['wxid'] = $val['vcMemberUserWxId'];
                    $update['room_wxid'] = $data['Data']['vcChatRoomId'];
                    $update['room_nickname'] = $val['vcGroupNickName'];
                    $update['nickname'] = $val['vcNickName'];
                    $update['head_img'] = $val['vcHeadImgUrl'];
                    $update['is_delete'] = 0;
                    $update['delete_time'] = '';
                    $update['user_serial_no'] = $val['vcMemberUserSerialNo'];
                    $update['inviter'] = $val['vcFatherWxId'];
                    CommunityModel::changeWeixinInfoByWxid($update);
                }

            }
        }
        CommunityModel::upByAppIdMid(['id' => $room_id, 'member_count' => $data['Data']['nMemberCount'], 'wxid' => $data['vcRobotWxId'], 'is_delete' => 0]);
        return true;
    }


    /**
     * 统一请求
     *
     * @param $url
     * @param $data
     * @return bool
     */
    protected static function QinRequest($url, $data)
    {
        //获取token
        $token = YiyunService::getToken();
        if (empty($token)) {
            return false;
        }

        $data = json_encode($data);

        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($data) . "",
            "Accept: application/json",
            "token:" . $token,
            "merchantNo:" . self::$MERCHANT
        ];
        //curl post
        $retData = self::curlRequest($url, $data, $headers);
//        dd($retData);
        $result = json_decode($retData, true);
        //请求失败做记录
        if ($result['nResult'] != 1) {
            error_log('请求日期：' . date('Y-m-d H:i:s') . ' 返回内容：' . json_encode($result) . "\n", 3, storage_path('logs/qin-error.log'));
            return $result;
        }
        return $result;
    }

    /**
     * 获取敏感词列表
     * @return bool
     */
    public static function getKeyWordList()
    {
        $url = self::$SERVICE_URL . '/api/SensitiveContent/KeywordList';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
        ];
        return self::QinRequest($url, $data);
    }

    /**
     * 撤回消息
     *
     * @param $vcRobotSerialNo
     * @param $nChatType
     * @param $vcFriendsGroupNo
     * @param $vcMsgId
     * @return bool
     */
    public static function setWithdraw($vcRobotSerialNo, $nChatType, $vcFriendsGroupNo, $vcMsgId)
    {
        $url = self::$SERVICE_URL . '/api/ChatMessages/WithdrawMessage';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $vcRobotSerialNo, //机器人编号
            'nChatType' => $nChatType, //聊天类型：0 群聊、1 私聊
            'vcFriendsGroupNo' => $vcFriendsGroupNo, //群\好友编号（聊天类型为0，传群编号；聊天类型为1，传好友编号）
            'vcMsgId' => $vcMsgId, //消息ID
        ];
        return self::QinRequest($url, $data);
    }

    /**
     * 个人私聊推送
     *
     * @param string $vcRobotSerialNo
     * @param string $vcToWxSerialNo
     * @param string $content
     * @return bool
     */
    public static function urgentNotice($content = '', $vcRobotSerialNo = '00A3FB0BD4194D323C37A408EFC10213', $vcToWxSerialNo = '82CEEB89BB8F1CA0A7126956AAFD10EF')
    {
        if (empty($content)) {
            return false;
        }
        $push_data = [
            'nMsgNum' => 1,
            'nMsgType' => 2001,
            'msgContent' => $content,
            'nVoiceTime' => 0,
            'vcHref' => '',
            'vcTitle' => '',
            'vcDesc' => '',
        ];

        $url = self::$SERVICE_URL . '/api/ChatMessages/SendPrivateChatMessages';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $vcRobotSerialNo, //机器人编号
            'vcToWxSerialNo' => $vcToWxSerialNo, //用户编号
            'Data' => [$push_data], //消息
        ];
        self::QinRequest($url, $data);
    }


    /**
     * 获取机器人详情
     *
     * @param $vcRobotSerialNos
     * @param int $nPageIndex
     * @return bool
     */
    public static function getRobotInfo($vcRobotSerialNos = [], $nPageIndex = 1)
    {
        $url = self::$SERVICE_URL . '/api/Robot/MerchantRobotList';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNos' => $vcRobotSerialNos, //机器人编号（支持传1-100个机器人）
            'nPageIndex' => $nPageIndex, // 机器人数量分页页码数（默认传1，查询机器人总数的第一页，传对应的数字则返回对应分页的机器人信息，机器人总数分页小于传的页码，则返回空数据）此参数必传
        ];
        return self::QinRequest($url, $data);
    }

    /**
     * 修改机器人名称
     *
     * @param array $vcRobotSerialNos
     * @param string $vcNickName
     * @return bool
     */
    public static function setRobotNickname($vcRobotSerialNos = [], $vcNickName = '')
    {
        if (empty($vcRobotSerialNos) || empty($vcNickName)) {
            return [];
        }
        $url = self::$SERVICE_URL . '/api/Robot/ModifyProfileName';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $vcRobotSerialNos, //机器人编号
            'vcNickName' => $vcNickName, //新昵称
        ];
        return self::QinRequest($url, $data);
    }

    /**
     * 修改机器人头像
     *
     * @param array $vcRobotSerialNos
     * @param string $imageUrl
     * @return bool
     */
    public static function setAvatar($vcRobotSerialNos = [], $imageUrl = '')
    {
        if (empty($vcRobotSerialNos) || empty($imageUrl)) {
            return [];
        }
        $url = self::$SERVICE_URL . '/api/Robot/ModifyProfileHeadImg';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $vcRobotSerialNos, //机器人编号
            'vcHeadImgUrl' => $imageUrl, //新昵称
        ];
        return self::QinRequest($url, $data);
    }

    /**
     * 主动添加好友
     *
     * @param array $vcRobotSerialNos
     * @param string $account
     * @param string $message
     * @return array|bool
     */
    public static function addFriends($vcRobotSerialNos = [], $account = '', $message = '我是您的专属机器人，通过申请，开启我们的旅程吧～')
    {
        if (empty($vcRobotSerialNos) || empty($account) || empty($message)) {
            return [];
        }
        $url = self::$SERVICE_URL . '/api/Friend/NewAddByAlias';
        $data = [
            'vcMerchantNo' => self::$MERCHANT, //商家编号
            'vcRobotSerialNo' => $vcRobotSerialNos, //机器人编号
            'vcAccount' => $account, //微信号/手机号/二维码地址（传二维码地址可以通过扫码加好友，二维码必须是解析出来的地址，非二维码上传至网站的地址。提示：可以通过草料二维码网站解析二维码地址）
            'vcHelloWord' => $message, //打招呼（必填-例如：我是XXX）
        ];
        return self::QinRequest($url, $data);
    }

}