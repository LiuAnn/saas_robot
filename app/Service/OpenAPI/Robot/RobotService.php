<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 2021/1/13
 * Time: 下午8:38
 * Describe: 机器人开放接口-群相关
 */

namespace App\Service\OpenAPI\Robot;

use Exception;
use App\Service\Platform\YiyunService;

class RobotService
{
    /**
     * 获取机器人信息
     *
     * @param $robotSerialNo
     * @return array
     * @throws Exception
     */
    public static function getInfo($robotSerialNo = '')
    {
        if (empty($robotSerialNo)) {
            throw new Exception('params is fail', 400);
        }
        $retData = YiyunService::getRobotInfo([$robotSerialNo]);
        if ($retData['nResult'] != 1 || empty($retData['Data']['RobotList'])) {
            throw new Exception($retData['vcResult'], 400);
        }
        return $retData['Data']['RobotList'][0] ?? [];
    }

    /**
     * 修改机器人昵称
     *
     * @param $robotSerialNo
     * @param $nickname
     * @return array
     * @throws Exception
     */
    public static function setNickname($robotSerialNo = '', $nickname)
    {
        if (empty($robotSerialNo)) {
            throw new Exception('params is fail', 400);
        }
        $retData = YiyunService::setRobotNickname($robotSerialNo, $nickname);
        print_r($retData);
        exit;
        if ($retData['nResult'] != 1) {
            throw new Exception($retData['vcResult'], 400);
        }
        return [];
    }

    /**
     * 修改机器人头像
     *
     * @param string $robotSerialNo
     * @param $nickname
     * @return array|mixed
     * @throws Exception
     */
    public static function setAvatar($robotSerialNo = '', $nickname)
    {
        if (empty($robotSerialNo) || empty($nickname)) {
            throw new Exception('params is fail', 400);
        }
        $retData = YiyunService::setAvatar($robotSerialNo, $nickname);
        if ($retData['nResult'] != 1) {
            throw new Exception($retData['vcResult'], 400);
        }
        return [];
    }

    public static function addFriends($robotSerialNo = '', $account = '', $message = '')
    {
        if (empty($robotSerialNo) || empty($account) || empty($message)) {
            throw new Exception('params is fail', 400);
        }
        $retData = YiyunService::addFriends($robotSerialNo, $account, $message);
        if ($retData['nResult'] != 1) {
            throw new Exception($retData['vcResult'], 400);
        }
        return [];
    }

}