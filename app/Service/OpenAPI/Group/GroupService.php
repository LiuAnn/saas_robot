<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 2021/1/13
 * Time: 下午8:38
 * Describe: 机器人开放接口-群相关
 */

namespace App\Service\OpenAPI\Group;


use App\Model\Client\IndexModel;
use App\Models\SyncGroup;

class GroupService
{
    public static function getGroupList($param)
    {
        if (empty($param['mobile']) || empty($param['channel_identity'])) {
            return [];
        }
        $groupList = SyncGroup::select('group_name', 'robot_wx_name', 'order_number', 'order_amount', 'browse_goods_number',
            'group_created_at', 'group_member_count', 'group_male_proportion', 'group_female_proportion')
            ->where('deleted_at', 0)
            ->where('group_mobile', $param['mobile'])
            ->where('channel_identity', $param['channel_identity'])
            ->get()
            ->toArray();

        if (empty($groupList)) {
            return [];
        }
        $retData = [];
        foreach ($groupList as $key => $val) {
            $retData[$key]['name'] = $val['group_name'];//群名称
            $retData[$key]['robotName'] = $val['robot_wx_name'];//机器人名称
            $retData[$key]['orderNumber'] = $val['order_number'];//群订单数量
            $retData[$key]['orderAmount'] = $val['order_amount']; //GMV
            $retData[$key]['browseGoodsNumber'] = $val['browse_goods_number'];//群内用户浏览商品总次数
            $retData[$key]['groupCreatedAt'] = $val['group_created_at'];//群创建时间
            $retData[$key]['groupMemberCount'] = $val['group_member_count'];//群总数
            $retData[$key]['groupMaleProportion'] = $val['group_male_proportion'];//男性占比
            $retData[$key]['groupFemaleProportion'] = $val['group_female_proportion'];//女性占比
        }
        return $retData;
    }

}