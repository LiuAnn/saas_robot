<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/12
 * Time: 16:37
 */

namespace App\Service\VoucherBag;

use App\Model\Mall\YanModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class CouponService
{
    /**
     * 生成短链接
     * @param $params
     * @return array|string
     */
    public static function getShortURrl($params)
    {
        if (empty($params['url'])) {
            return ['code' => 400, 'msg' => '缺少参数', 'data' => []];
        }
        $str = $params['url'];
        $sign = md5($str);
        $shortCode = YanModel::getProductShortCode($sign);
        if (empty($shortCode)) {
            $shortCode = self::getShortCode($str, $sign);
        }
        return 'https://share.yuetao.vip/' . $shortCode;
    }


    /**
     * CPS活动保存素材长链接转换短链接
     * @param $params
     * @return array|string
     */
    public static function getShortUrlByLongUrl($url)
    {
        $sign = md5($url);
        $shortCode = YanModel::getProductShortCode($sign);
        if (empty($shortCode)) {
            $shortCode = self::getShortCode($url, $sign);
        }
        return 'https://share.yuetao.vip/' . $shortCode;
    }

    public static function querySendMiniAppXml($baseXml, $value)
    {
        $xmlData = xmlToArray($baseXml);
        //name:小程序名称  userName:小程序原始ID  title:分享标题 pagePath:小程序跳转路径
        if (isset($value['name']) && !empty($value['name'])) {
            $xmlData['appmsg']['sourcedisplayname'] = $value['name'];
        }
        if (isset($value['userName']) && !empty($value['userName'])) {
            $xmlData['appmsg']['weappinfo']['username'] = $value['userName'] . '@app';
        }
        if (isset($value['title']) && !empty($value['title'])) {
            $xmlData['appmsg']['title'] = $value['title'];
        }
        if (isset($value['pagePath']) && !empty($value['pagePath'])) {
            $xmlData['appmsg']['weappinfo']['pagepath'] = $value['pagePath'];
        }
        return arr2xml($xmlData);
    }

    /**
     * 生成短连接码
     * @param $longUrl
     * @return array
     */
    public static function shortUrl($longUrl)
    {
        $key = 'swz0823'; //自定义key值
        $base32 = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // 利用md5算法方式生成hash值
        $hex = hash('md5', $longUrl . $key);
        $hexLen = strlen($hex);
        $subHexLen = $hexLen / 8;

        $output = array();
        for ($i = 0; $i < $subHexLen; $i++) {
            // 将这32位分成四份，每一份8个字符，将其视作16进制串与0x3fffffff(30位1)与操作
            $subHex = substr($hex, $i * 8, 8);
            // $idx = 0x3FFFFFFF & (1 * ('0x' . $subHex));
            $idx = 0x3FFFFFFF & hexdec($subHex);
            // 这30位分成6段, 每5个一组，算出其整数值，然后映射到我们准备的62个字符
            $out = '';
            for ($j = 0; $j < 6; $j++) {
                $val = 0x0000003D & $idx;
                $out .= $base32[$val];
                $idx = $idx >> 5;
            }
            $output[$i] = $out;
        }
        $shortCode = $output[1];
        $checkIsSet  = YanModel::checkIsSetShortCode($shortCode);
        if($checkIsSet){
            $shortCode = $output[2];
        }
        return $shortCode;

    }

    /**
     * 获取链接
     * @param $longUrl
     * @param $sign
     * @return array|int
     */
    public static function getShortCode($longUrl, $sign)
    {
        $shortCode = self::shortUrl($longUrl);
        $data = [
            'long_url' => $longUrl,
            'short_url_code' => $shortCode,
            'sign' => $sign,
            'create_time' => time()
        ];
        $addProductShortCode = YanModel::addProductShortCode($data);
        if (!$addProductShortCode) {
            $shortCode = 0;
        }
        return $shortCode;
    }






}


















