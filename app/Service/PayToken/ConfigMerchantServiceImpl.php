<?php
namespace App\Service\PayToken;

use App\Model\PayToken\ConfigMerchantModel;

/**
 * Class LeaderServiceImpl
 * @package App\Service\Line
 */
class ConfigMerchantServiceImpl
{
    protected static $instances = [];

    protected $sysFlag = '';

    protected $tradeType = '';

    protected $createTime = null;

    protected $isDisabled = 0;

    protected $remark = '';

    protected $groupId = 0;

    protected $parentMchId = 0;

    protected $model = null;

    //初始化
    protected function __construct(ConfigMerchantModel $model)
    {
        $this->mchId = $model->mch_id;

        $this->mchName = $model->mch_name;

        $this->mchKey = $model->mch_key;

        $this->sysFlag = $model->sys_flag;

        $this->tradeType = $model->trade_type;

        $this->createTime = $model->create_time;

        $this->isDisabled = $model->is_disabled;

        $this->remark = $model->remark;

        $this->groupId = $model->group_id;

        $this->parentMchId = $model->parent_mch_id;

        $this->model = $model;

        self::$instances[$this->mchId] = $this;

        return $this;
    }

    /**
     * @return int|mixed
     */
    public function getMchId()
    {
        return $this->mchId;
    }

    /**
     * @param int|mixed $mchId
     */
    public function setMchId($mchId)
    {
        $this->mchId = $mchId;
    }

    /**
     * @return mixed|string
     */
    public function getMchName()
    {
        return $this->mchName;
    }

    /**
     * @param mixed|string $mchName
     */
    public function setMchName($mchName)
    {
        $this->mchName = $mchName;
    }

    /**
     * @return mixed|string
     */
    public function getMchKey()
    {
        return $this->mchKey;
    }

    /**
     * @param mixed|string $mchKey
     */
    public function setMchKey($mchKey)
    {
        $this->mchKey = $mchKey;
    }

    /**
     * @return mixed|string
     */
    public function getSysFlag()
    {
        return $this->sysFlag;
    }

    /**
     * @param mixed|string $sysFlag
     */
    public function setSysFlag($sysFlag)
    {
        $this->sysFlag = $sysFlag;
    }

    /**
     * @return mixed|string
     */
    public function getTradeType()
    {
        return $this->tradeType;
    }

    /**
     * @param mixed|string $tradeType
     */
    public function setTradeType($tradeType)
    {
        $this->tradeType = $tradeType;
    }

    /**
     * @return mixed|null
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @param mixed|null $createTime
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
    }

    /**
     * @return int|mixed
     */
    public function getisDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @param int|mixed $isDisabled
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;
    }

    /**
     * @return mixed|string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed|string $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return int|mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param int|mixed $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return int|mixed
     */
    public function getParentMchId()
    {
        return $this->parentMchId;
    }

    /**
     * @param int|mixed $parentMchId
     */
    public function setParentMchId($parentMchId)
    {
        $this->parentMchId = $parentMchId;
    }

    /**
     * 获取实例
     * @param $mchId
     * @return ConfigMerchantServiceImpl|mixed
     */
    public static function getInstance($mchId)
    {
        foreach (self::$instances as $instance){
            if($instance->getMchId() == $mchId){
                return $instance;
            }
        }

        $model = ConfigMerchantModel::find($mchId);

        return new self($model);
    }

    /**
     * 获取模型
     * @return array
     */
    public function getModel()
    {
        return $this->model->toArray();
    }
}
