<?php
namespace App\Service;

use App\Exceptions\ServicesException;
use App\Model\Member\MemberModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Model\Weixin\WeixinsmallModel;

class WeixinService {

    private $OK = 0;
    private $IllegalAesKey = -41001;
    private $IllegalIv = -41002;
    private $IllegalBuffer = -41003;
    private $DecodeBase64Error = -41004;

    private $appId;
    private $appSecret;
    public $url;
    private $zhidingKey;
    private $sessionKey;

    private $session_key_url = "https://api.weixin.qq.com/sns/jscode2session?";  //code 换取 session_key

    public function _config($weixin_id)
    {
        $sql = " select * from sline_com_weixin where weixin_id=:weixin_id limit 1";
        $parms['weixin_id'] = $weixin_id;
        $results = DB::select($sql, $parms);

        $result = $results[0];
        $this->appId = $result->appid;
        $this->appSecret = $result->appsecret;
        $this->zhidingKey = $result->key;
    }


    public function getAccessToken($weixin_id)
    {
        return   $this->getNewAccessToken($this->appId,$this->appSecret);
    }

    //最新的生成   access_token 方法
    public  static function    getNewAccessToken($appid,$appSecret)
    {
        $accessTokenKey = $appid . '@AccessTokenKey';
        $accessToken    = Redis::get($accessTokenKey);
        if (empty($accessToken)) {
            $getAccessUrl   = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appid . '&secret=' . $appSecret;
            $jsonToken      = file_get_contents($getAccessUrl, false);
            $accessTokenArr = json_decode($jsonToken, true);
            if (empty($accessTokenArr['access_token'])) {
                return '';
            }
            $accessToken = $accessTokenArr['access_token'];
            Log::info('jsonTokeGeneratorQrCodes '.$accessToken);
            Redis::set($accessTokenKey, $accessToken);
            Redis::expire($accessTokenKey, 7100);
        }
        return $accessToken;
    }

    //最新的生成   access_token 方法
    public static function getCopyNewAccessToken($appid, $appSecret)
    {
        $accessTokenKey = $appid . '@AccessTokenKey';
        $getAccessUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appid . '&secret=' . $appSecret;
        $jsonToken = file_get_contents($getAccessUrl, false);
        $accessTokenArr = json_decode($jsonToken, true);
        if (empty($accessTokenArr['access_token'])) {
            return '';
        }

        $accessToken = $accessTokenArr['access_token'];
        Log::info('CopyjsonTokeGeneratorQrCodes '.$accessToken);
        Redis::set($accessTokenKey, $accessToken);
        Redis::expire($accessTokenKey, 7100);

        return $accessToken;
    }



    //远程获取accss_token等信息
    private function httpGet($url)
    {
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 500);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_URL, $url);
            $res = curl_exec($curl);
            curl_close($curl);
            return $res;
        }catch (\Exception $ex){
            Log::info($ex->getMessage());
            throw new ServicesException('access_token');
        }
    }

    //获取redis缓存
    private function get_redis_str($filename)
    {
        try{
            return json_decode(Redis::get($filename));
        }catch (\Exception $exception){
            Log::info($exception->getMessage());
            throw new ServicesException('redis');
        }
    }

    //设置redis缓存
    private function set_redis_str($filename,$content)
    {
        try{
            Redis::set($filename,$content);
        }catch (\Exception $exception){
            Log::info($exception->getMessage());
            throw new ServicesException('redis');
        }
    }


    public function get_key($weixin_id,$code)
    {
        $this->_config($weixin_id);
        $url = $this->session_key_url."appid=".$this->appId."&secret=".$this->appSecret."&js_code=".$code."&grant_type=authorization_code";
        $res = $this->httpGet($url);

        return json_decode($res,true);
    }



    /**
     * 构造函数
     * @param $sessionKey string 用户在小程序登录后获取的会话密钥
     * @param $appid string 小程序的appid
     */
    public function WXBizDataCrypt( $weixin_id, $sessionKey)
    {
        $this->_config($weixin_id);
        $this->sessionKey = $sessionKey;
    }


    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData( $encryptedData, $iv, &$data )
    {
        if (strlen($this->sessionKey) != 24) {
            return $this->IllegalAesKey;
        }
        $aesKey=base64_decode($this->sessionKey);


        if (strlen($iv) != 24) {
            return $this->IllegalIv;
        }
        $aesIV=base64_decode($iv);

        $aesCipher=base64_decode($encryptedData);

        $result=openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

        $dataObj=json_decode( $result );
        if( $dataObj  == NULL )
        {
            return $this->IllegalBuffer;
        }
        if( $dataObj->watermark->appid != $this->appId )
        {
            return $this->IllegalBuffer;
        }
        $data = $result;
        return $this->OK;
    }

    //生成 access_token
    public function access_token($mid)
    {
        $model = MemberModel::find($mid);
        $time = time();
        $sign = md5($mid.'.'.$time.'.'.$model->pwd);
        return $mid.'.'.$time.'.'.$sign;
    }

    //生成refresh_token
    public function refresh_token($mid)
    {
        $model = MemberModel::find($mid);
        $uuid = md5($mid);
        $time = time();
        $refresh_sign = md5($mid.'.'.$time.'.'.$uuid.'.'.$model->pwd);
        return $mid.'.'.$time.'.'.$uuid.'.'.$refresh_sign;
    }

    /**
     * 写入微信用户
     */
    public function addWxUser(array $data)
    {
        return WeixinsmallModel::create($data);
    }

    /**
     * 根据openid获取用户
     */
    public function getWxUser($openId)
    {
        return WeixinsmallModel::where('openid',$openId)->first();
    }

}
