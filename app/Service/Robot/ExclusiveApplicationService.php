<?php

namespace App\Service\Robot;
use App\Model\Robot\ExclusiveApplicationModel;

class ExclusiveApplicationService
{
    public static function getExclusiveApplicationList($params)
    {

        $applicationList = ExclusiveApplicationModel::getExclusiveApplicationList($params);
        $applicationList = json_decode(json_encode($applicationList), true);
//        if (isset($applicationList['data']) && count($applicationList['data']) > 0) {
            $applicationList = array_map(function ($value) {
                switch ($value['group_num'])
                {
                    case '1':
                        $desc = '1-29个群';
                        break;
                    case '2':
                        $desc = '30-49个群';
                        break;
                    case '3':
                        $desc = '50-99个群';
                        break;
                    case '4':
                        $desc = '100个群以上';
                    break;
                    default :
                        $desc = '1-29个群';
                        break;
                }
                switch ($value['status'])
                {
                    case '0':
                        $status = '未审核';
                        break;
                    case '1':
                        $status = '已通过';
                        break;
                    case '2':
                        $status = '不通过';
                        break;
                    default :
                        $status = '未审核';
                        break;
                }
                $value['status'] = $status;
                $value['group_num'] = $desc;
                return $value;
            }, $applicationList);
//        }
        return $applicationList;
    }

    public static function addExclusiveApplication($params)
    {
        //检测参数
        $checkParam = self::checkExclusiveApplicationParams($params,'add');

        if (!empty($checkParam))
        {
            return ['code' => 400, 'msg' => $checkParam['msg']];
        }

        $params['created_at'] = date('Y-m-d H:i:s',time());

        $ExclusiveApplication = (new ExclusiveApplicationModel)->insert($params);

        if ($ExclusiveApplication)
        {
            $logMessage = '新增专属机器人申请';
            UserOperationLogService::insertAdminOperation(
                $params['mid'], 1, json_encode($params), $logMessage, $ExclusiveApplication
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
        //return ExclusiveApplicationModel::addExclusiveApplication();
    }

    public static function updateExclusiveApplication($params)
    {
        $params['deleted_at']=time();
        $res = ExclusiveApplicationModel::updateExclusiveApplication($params);
        if ($res)
        {
            $logMessage = '删除专属机器人申请';
            UserOperationLogService::insertAdminOperation(
                $params['mid'], 3, json_encode($params), $logMessage, $params['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function checkExclusiveApplicationParams($param = [],$checkType = 'update')
    {
        if ($checkType == 'update') {
            if (empty($param['id'])) {
                return ['code' => 400, 'msg' => 'ID不能为空'];
            }
        }

        if (empty($param['mid']))
        {
            return ['code' => 400, 'msg' => '用户ID不能为空'];
        }

        if (empty($param['user_name']))
        {
            return ['code' => 400, 'msg' => '用户姓名不能为空'];
        }

        if (empty($param['user_wx']))
        {
            return ['code' => 400, 'msg' => '用户微信号不能为空'];
        }

        if (empty($param['phone']))
        {
            return ['code' => 400, 'msg' => '用户手机号不能为空'];
        }

        if (empty($param['industry']))
        {
            return ['code' => 400, 'msg' => '所属行业不能为空'];
        }

        if (empty($param['brand_name']))
        {
            return ['code' => 400, 'msg' => '品牌不能为空'];
        }

        if (empty($param['group_num']))
        {
            return ['code' => 400, 'msg' => '拥有群数量不能为空'];
        }
    }

    public  static function getBrandDataApplication($params)
    {
       return ['code' => 200, 'msg' => '获取成功','data'=>config('community.barandData')];
    }


    public  static function addExclusiveRobot($params){

        //检测参数
        $checkParam = self::checkExclusiveRobot($params,'add');
        if (!empty($checkParam))
        {
            return ['code' => 400, 'msg' => $checkParam['msg'] ,'data'=>[]];
        }

        $params['created_at'] = date('Y-m-d H:i:s',time());

        unset($params['source']);

        if ($params['platform_identity']=='c23ca372624d226e713fe1d2cd44be35')
        {
            $params['brand_name']='邻居团';
        }


        $ExclusiveApplication = (new ExclusiveApplicationModel)->insert($params);

        if ($ExclusiveApplication)
        {
            $logMessage = '新增专属机器人申请';
            UserOperationLogService::insertAdminOperation(
                $params['mid'], 1, json_encode($params), $logMessage, $ExclusiveApplication
            );
            return ['code' => 200, 'msg' => '操作成功!','data'=>[]];
        }else{
            return ['code' => 400, 'msg' => '操作失败!','data'=>[]];
        }

    }

    public static function checkExclusiveRobot($param = [],$checkType = 'update')
    {
        if ($checkType == 'update') {
            if (empty($param['id'])) {
                return ['code' => 400, 'msg' => 'ID不能为空'];
            }
        }
        //只有邻居团才可以
        if (empty($param['platform_identity'])||$param['platform_identity']!='c23ca372624d226e713fe1d2cd44be35')
        {
            return ['code' => 400, 'msg' => '渠道错误'];
        }

        if (empty($param['mid']))
        {
            return ['code' => 400, 'msg' => '用户ID不能为空'];
        }

        if (empty($param['user_wx']))
        {
            return ['code' => 400, 'msg' => '用户微信号不能为空'];
        }
      if (empty($param['manage_phone']))
        {
            return ['code' => 400, 'msg' => '用户微信号不能为空'];
        }

        if (empty($param['phone']))
        {
            return ['code' => 400, 'msg' => '用户手机号不能为空'];
        }



    }

}