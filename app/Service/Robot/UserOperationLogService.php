<?php


namespace App\Service\Robot;


use App\Model\Robot\UserOperationLogModel;
use App\Model\User\UserModel;
use App\Model\Community\GroupManageModel;
class UserOperationLogService
{

    /**
     * 管理员操作日志记录
     * @param $param
     * //操作类型1：新增，2：修改，3：删除 4:登录
     */
    public static function insertAdminOperation($adminId, $type, $changeData, $message, $changeId)
    {
        $insertArray = [];
        //后台操作用户id
        $insertArray['mid'] = $adminId;
        //操作路径
        //$insertArray['path'] = [];
        //请求内容
        //$insertArray['input'] = [];
        //操作类型1：新增，2：修改，3：删除
        $insertArray['type'] = $type;
        //修改内容
        $insertArray['change_data'] = $changeData;
        //日志描述
        $insertArray['message'] = $message;
        //数据类型1;一维2：二维
        $insertArray['data_type'] = 1;
        //修改或添加的主键id
        $insertArray['relevance_id'] = $changeId;
        //修改的类型，1：视频直播活动，2：商品
        //$insertArray['relevance_type'] = [];
        //创建时间,又称操作时间(时间戳)
        $insertArray['created_time'] = time();
        UserOperationLogModel::insertAdminOperation($insertArray);
    }

    public  static    function is_timestamp($timestamp) {
        if (!is_numeric($timestamp)) return false;
        if(strtotime(date('Y-m-d H:i:s',$timestamp)) == $timestamp) {
            return $timestamp;
        } else return false;
    }

    public static function getUserOperationList($memberInfo, $param = [])
    {
        //查询开始时间
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = $param['beginTime'] ?? strtotime(date('Y-m-d', strtotime('-1 week')) . ' 00:00:00');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
            if (!self::is_timestamp($param['beginTime']) ||!self::is_timestamp($param['endTime'])) {
                return ['code' => 200001, 'msg' => '时间格式不正确'];
            }
            $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
            $endTime = date('Y-m-d 00:00:00', $param['endTime']);
            //开始结束时间容错
            if ($param['beginTime'] == $endTime) {
                $endTime = date('Y-m-d H:i:s', strtotime("$endTime+1day"));
            }
            $param['endTime'] = $endTime;
        }
        if (!isset($param['mid']) || empty($param['mid'])) {
            if ($memberInfo['sup_id'] == 0) {
                $userIds = UserModel::getDownUserIds($memberInfo['mid']);
                $userIds[] = $memberInfo['mid'];
                $param['midS'] = $userIds;
            } else {
                $param['midS'] = UserModel::getPeerUserIds($memberInfo['sup_id']);
            }
        } else {
            //TODO  不是所属人员无权查看
            $param['midS'] = [$param['mid']];
        }
        $pageSize = $param['pageSize'] ?? 10;
        $operationList = UserOperationLogModel::getUserOperationList($param, $pageSize);
        $operationList = json_decode(json_encode($operationList), true);
        if (!empty($operationList['data']) && is_array($operationList['data'])) {
            $operationList['data'] = array_map(function ($value) {
                switch ($value['type']) {
                    case '1':
                        $value['operation'] = '新增';
                        break;
                    case '2':
                        $value['operation'] = '修改';
                        break;
                    case '3':
                        $value['operation'] = '删除';
                        break;
                    default:
                        $value['operation'] = '登录';
                        break;
                }
                $value['created_time'] = date('Y-m-d H:i:s', $value['created_time']);
                return $value;
            }, $operationList['data']);
        }
        return $operationList;
    }

    public static function getUserListByName($memberInfo, $param)
    {
        if ($memberInfo['sup_id'] == 0) {
            $userIds = UserModel::getDownUserIds($memberInfo['mid']);
            $userIds[] = $memberInfo['mid'];
            $param['midS'] = $userIds;
        } else {
            //$param['midS'] = UserModel::getPeerUserIds($memberInfo['sup_id']);
            //只查看他自己
            $param['midS'][] = $memberInfo['mid'];
        }
        return UserModel::getUserListByName($param);
    }

    public static function getHouTaiUserListByName($param)
    {
        return UserModel::getUserListByName($param);
    }

    /**获取数据列表
     * @param $param
     * @return array
     */
    public static function getSendGroupNumByLinkType( $param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;
        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;
        $list =  GroupManageModel::getSendGroupNumByLinkType($param);
        $listCount =  GroupManageModel::getSendGroupNumByLinkTypeCount($param);
        return  ['list'=>$list,'total_count'=>$listCount];
    }


}