<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/7/8
 * Time: 15:16
 */

namespace App\Service\Robot;

use App\Model\Robot\UnitManageModel;

class UnitManageService
{   

    static $drDomain = 'https://gateway.daren.tech/open/v1/open/user/getBroadcaster?live_id=';

    /**
     * 获取分队列表
     * @return array
     */
    public static function getListData($params, $memberInfo)
    {
        $params['page'] = isset($params['page']) ? $params['page'] : 1;
        $params['pageSize'] = isset($params['pageSize']) ? $params['pageSize'] : 10;
        $data = UnitManageModel::getlist($params, $memberInfo);
        $total = UnitManageModel::getlistCount($params, $memberInfo);
        return [
            'code' => 200,
            'data' => $data,
            'total' => $total
        ];
    }

    /**
     * 获取待发送的数据
     * @param $params
     * @param $memberInfo
     * @return array
     */
    public static function getNotBindMemberData($params, $memberInfo)
    {
        $data = UnitManageModel::getNoMemberData($memberInfo);
        return [
            'code' => 200,
            'data' => $data,
        ];
    }

    /**
     * 数据添加
     */
    public static function addData($params, $memberInfo)
    {

        $params['group_name'] = isset($params['group_name']) ? $params['group_name'] : '';
        $returnId = $isSet = $leader = 0;
        if (!empty($params['data'])) {
            foreach ($params['data'] as $v) {
                //查看当前用户是否 已经分配到其他队伍
                $bindMidCount = UnitManageModel::getfindStaffWhere(['bind_mid' => $v['bind_mid'], 'deleted_at' => 0]);
                if ($bindMidCount > 0) {
                    $isSet += 1;
                }
                if ($v['group_mid'] > 0) {
                    $leader++;
                }
            }
        } else {
            return ['code' => 400, 'data' => [], 'msg' => '请选择管理员'];
        }
        if ($isSet == 1) {
            return ['code' => 400, 'data' => [], 'msg' => '请不要重复选择管理员'];
        } else if ($leader != 1) {
            return ['code' => 400, 'data' => [], 'msg' => '请选择一个组长'];
        }
        if (!empty($params['group_name']) && $isSet == 0) {
            $data = UnitManageModel::getfindDataWhere(['group_name' => $params['group_name'], 'deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']], "*");
            if (empty($data)) {
                $indertData['group_name'] = $params['group_name'];
                $indertData['platform_identity'] = $memberInfo['platform_identity'];
                $indertData['created_at'] = date('Y-m-d H:i:s', time());
                $returnId = UnitManageModel::groupManageAdd($indertData);
                if ($returnId > 0 && !empty($params['data'])) {
                    foreach ($params['data'] as $v) {
                        $insertStaff=[];
                        $insertStaff['bind_mid'] = $v['bind_mid'];
                        $insertStaff['group_id'] = $returnId;
                        $insertStaff['platform_identity'] = $memberInfo['platform_identity'];
                        if ($v['group_mid'] == 1) {
                            $insertStaff['group_mid'] = $v['bind_mid'];
                        }
                        UnitManageModel::groupManageStaffAdd($insertStaff);
                    }
                }
            }
        }
        if ($returnId > 0) {
            return ['code' => 200, 'data' => [], 'msg' => '添加成功'];
        } else {
            return ['code' => 400, 'data' => [], 'msg' => '分队名称已存在'];
        }
    }

    /**
     * 获取详细
     */
    public static function getGroupListData($params, $memberInfo)
    {

        $params['platform_identity']  = $memberInfo['platform_identity'];

        $params['page'] = isset($params['page']) ? $params['page'] : 1;
        $params['pageSize'] = isset($params['pageSize']) ? $params['pageSize'] : 10;

        $mangerData = self::getformatList(UnitManageModel::getfindDataStaffWhere($params));


        return ['code' => 200, 'data' => $mangerData];
    }

    /**
     * @param $params
     * @param $memberInfo
     * @return array
     */
    public static function getGroupInfoById($params, $memberInfo)
    {

        $params['id'] = isset($params['id']) ? $params['id'] : 0;
        $params['platform_identity'] = $memberInfo['platform_identity'];


        $data = UnitManageModel::getfindDataWhere(['id' => $params['id'], 'deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']], "*");
        //获取绑定的管理员
        $mangerData = UnitManageModel::getfindDataStaffWhereNew(['scg.group_id' => $params['id'], 'user_type'=>2,'scg.deleted_at' => 0, 'scg.platform_identity' => $memberInfo['platform_identity']]);
        if (!empty($data)) {
            if (!empty($mangerData)) {
                foreach ($mangerData as &$v) {
                    $v['group_mid'] = $v['group_mid'] > 0 ? 1 : 0;
                }
            }
            return ['code' => 200, 'data' => ['id' => $data['id'], 'group_name' => $data['group_name'], 'data' => $mangerData]];
        }
    }

    public static function getformatList($list)
    {

        if (empty($list)) return [];

        $data = [];

        foreach ($list as $key => $value) {

            $data[$key]['key'] = $key+1;
            $data[$key]['group_id'] = $value['id'];
            $data[$key]['groupCount'] = empty($value['groupCount']) ? 0 : $value['groupCount'];
            $data[$key]['orderCount'] = empty($value['orderCount']) ? 0 : $value['orderCount'];
            $data[$key]['orderAmount'] = empty($value['orderAmount']) ? 0 : $value['orderAmount']/100;
            $data[$key]['leader_name'] = UnitManageModel::getUserNameByMid($value['group_mid']);
            $data[$key]['group_name'] = $value['group_name'];

        }

        return $data;

    }


    /**
     * @param $params
     * @param $memberInfo
     */
    public static function upGroupInfo($params, $memberInfo)
    {
        $params['id'] = isset($params['id']) ? $params['id'] : '';
        $params['group_name'] = isset($params['group_name']) ? $params['group_name'] : '';
        $returnId = 0;
        if (empty($params['id'])) {
            return ['code' => 400, 'data' => [], 'msg' => '操作异常'];
        }
        //名称
        if (!empty($params['group_name'])) {
            $data = UnitManageModel::getfindDataWhere([['id', '<>', $params['id']], 'group_name' => $params['group_name'], 'deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']], "*");
            if (empty($data)) {
                $upData['group_name'] = $params['group_name'];
                $returnId = UnitManageModel::upDataWhere(['id' => $params['id']], $upData);
            }
        }
        $mangerData = UnitManageModel::getfindDataStaffWhereNew1(['group_id' => $params['id'], 'deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']]);

        if (!empty($mangerData)) {
            foreach ($mangerData as $v) {
                $delwhere['group_id'] = $params['id'];
                $delwhere['bind_mid'] = $v['bind_mid'];
                UnitManageModel::deleteDataStaffWhere($delwhere);
            }
        }
        $res=0;
        //添加
        if (!empty($params['data'])) {
            foreach ($params['data'] as $v) {
                $insertStaff=[];
                $insertStaff['bind_mid'] = $v['bind_mid'];
                $insertStaff['group_id'] = $params['id'];
                $insertStaff['platform_identity'] = $memberInfo['platform_identity'];
                if ($v['group_mid'] == 1) {
                    $insertStaff['group_mid'] = $v['bind_mid'];
                }
               $res = UnitManageModel::groupManageStaffAdd($insertStaff);
            }
        }
        if ($returnId > 0||$res>0) {
            return ['code' => 200, 'data' => [], 'msg' => '更新成功'];
        } else {
            return ['code' => 400, 'data' => [], 'msg' => '该名称已存在'];
        }
    }

    /**
     * 删除分组
     * @param $params
     * @param $memberInfo
     * @return array
     */
    public static function delGroupInfo($params, $memberInfo)
    {
        $params['id'] = isset($params['id']) ? $params['id'] : '';
        $params['staff_id'] = isset($params['staff_id']) ? $params['staff_id'] : '';
        $returnId = 0;
        if (!empty($params['staff_id']) && !empty($params['id'])) {
            $data = UnitManageModel::getfindStaffWhere(['group_id' => $params['id'], 'deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']], '*');
            if (empty($data)) {
                $upData['deleted_at'] = time();
                $returnId = UnitManageModel::upDataWhere(['id' => $params['id']], $upData);
            } else {
                $delwhere['id'] = $params['staff_id'];
                $delwhere['group_id'] = $params['id'];
                $returnId = UnitManageModel::deleteDataStaffWhere($delwhere);
            }
        }
        if ($returnId > 0) {
            return ['code' => 200, 'data' => [], 'msg' => '删除成功'];
        } else {
            return ['code' => 400, 'data' => [], 'msg' => '分组下有成员不能删除'];
        }
    }

    /**
     * 解散小组
     * @param $params
     * @return array
     */
    public static function disbandGroup($params, $memberInfo)
    {
        $params['group_id'] = isset($params['group_id']) ? $params['group_id'] : '';
        $returnId = 0;
        if (!empty($params['group_id'])) {
            $data = UnitManageModel::getfindDataStaffWhereNew1(['group_id' => $params['group_id'], 'deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']], '*');
            if (!empty($data)) {
                foreach ($data as $v) {
                    $delwhere=[];
                    $delwhere['group_id'] = $params['group_id'];
                    $delwhere['bind_mid'] = $v['bind_mid'];
                    $returnId = UnitManageModel::deleteDataStaffWhere($delwhere);
                }
            }
        }
        if ($returnId > 0) {
            $upData['deleted_at'] = time();
            UnitManageModel::upDataWhere(['id' => $params['group_id']], $upData);
            return ['code' => 200, 'data' => [], 'msg' => '删除成功'];
        } else {
            return ['code' => 400, 'data' => [], 'msg' => '分组下有成员不能删除'];
        }

    }
    /**
     * 直播间数据统计
     */
    public static function  liveRoomList($params, $memberInfo){





    }


    /**
     * 直播社群统计
     * @param $params
     * @return array
     */
    public static function getLiveList($params,$memberInfo)
    {
        $params['page'] = isset($params['page']) ? $params['page'] : 1;
        $params['pageSize'] = isset($params['pageSize']) ? $params['pageSize'] : 10;
        $data = UnitManageModel::getLiveList($params, $memberInfo);
        $total = UnitManageModel::getLiveCount($params, $memberInfo);
        return [
            'code' => 200,
            'data' => self::getlivformatList($data,$params['page'],$params['pageSize']),
            'total' => $total
        ];       
    }


    public static function getlivformatList($data,$page = 1,$pageSize = 10)
    {

        if(empty($data)) return [];

        $list = [];

        $live_str = implode(",",array_column($data,'live_id'));

        $live_info = json_decode(curlPost(self::$drDomain.$live_str,[]));

        foreach ($data as $key => $value) {

            $list[$key]['key'] = ($page-1)+$key+1;
            $list[$key]['live_id'] = $value['live_id'];
            $list[$key]['admin_name'] = $value['name'];
            $list[$key]['group_name'] = UnitManageModel::getGroupNameByMid($value['mid']);
            $list[$key]['mid'] = $value['mid'];
            $list[$key]['order_count'] = $value['orderCount'];
            $list[$key]['order_amount'] = $value['orderAmount']/100;
            $list[$key]['live_mobile'] = "";
            $list[$key]['live_nickName'] = '';
            $list[$key]['live_number'] = 0;
            $list[$key]['share_num'] = 0;

            foreach ($live_info->data as $k => $v) {

                if($v->id==$value['live_id']){

                    $list[$key]['live_mobile'] = $v->mobile;
                    $list[$key]['live_nickName'] = $v->nickname;
                    $list[$key]['live_number'] = $v->num;
                }
            }
        }


        return $list;
    }

}