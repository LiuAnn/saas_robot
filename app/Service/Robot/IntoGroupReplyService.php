<?php


namespace App\Service\Robot;


use App\Model\Robot\IntoGroupReplyModel;

class IntoGroupReplyService
{

    /**
     * 关键词回复列表
     * @param $param
     * @return mixed
     */
    public static function updateIntoGroupReplyTag($memberInfo, $param = [])
    {
        if (!isset($param['tag_id']) || empty($param['tag_id'])) {
            return ['code' => 400, 'msg' => '标签ID不能为空'];
        }
        if (!is_array($param['tag_id'])) {
            return ['code' => 400, 'msg' => '标签id格式不正确'];
        }
        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];
        $tagId = implode(',', $param['tag_id']);
        $update = [
            'tag_id' => $tagId
        ];
        $StrategyCategory = IntoGroupReplyModel::updateIntoGroupReplyTag($tagId, $param);

        if ($StrategyCategory)
        {
            $logMessage = '修改所有新人进群回复的标签';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($update), $logMessage, $StrategyCategory
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }
    /**
     * 关键词回复列表
     * @param $param
     * @return mixed
     */
    public static function intoGroupReplyList($param,$memberInfo)
    {
        $pageSize = isset($param["pageSize"]) ? $param["pageSize"] : 20;
        $page = isset($param["page"]) ? $param["page"] : 1;
        $begin = isset($param["page"]) ? ($param["page"] - 1) * $pageSize : 0;

        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];

        $result = IntoGroupReplyModel::keyWordsReplyList($param, $begin, $pageSize);
        foreach ($result as &$value) {
            if (isset($value->reply_address) && $value->reply_address == 1) {
                $value->robot_id = $value->tag_id;
                $value->tag_id = [];
            } else {
                $value->robot_id = '';
            }
        }
        $count = IntoGroupReplyModel::keyWordsReplyCount($param);
        return [
            'data' => $result,
            'total'=>$count,
            'page' => $page,
            'pageSize'=>$pageSize
        ];
    }


    public static function addIntoGroupReply($memberInfo, $param = [])
    {
        $checkResult = self::checkIntoGroupReplyParam($param);
        if (isset($checkResult['code'])) {
            return $checkResult;
        }
        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];

        //$param['reply_address']; 0群内回复1加好友回复
        if (isset($param['reply_address']) && $param['reply_address'] == 1) {
            if (isset($param['robot_id'])) {
                $param['tag_id'] = $param['robot_id'];
            }
        }
        if (isset($param['robot_id'])) {
            unset($param['robot_id']);
        }
        $insertId = IntoGroupReplyModel::insertGetId($param);

        if ($insertId)
        {
            $logMessage = '新增关键词消息回复';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $insertId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    public static function addIntoGroupGreetingInfo($memberInfo, $param = [])
    {
        if (empty($param['content'])) {
            return ['code' => 400, 'msg' => '内容不能为空'];
        }
        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];

        $where = [];
        $where['mid'] = $memberInfo['mid'];
        $where['platform_identity'] = $memberInfo['platform_identity'];
        $GreetingInfo = IntoGroupReplyModel::getIntoGroupGreetingInfo($where);
        if($GreetingInfo) {
            $update = ['content' => $param['content']];
            $insertId = IntoGroupReplyModel::updateIntoGroupGreetingInfo($where, $update);
        } else {
            $insertId = IntoGroupReplyModel::addIntoGroupGreetingInfo($param);
        }

        if ($insertId)
        {
            $logMessage = '新增新人进群问候语';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $insertId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function getIntoGroupGreetingInfo($memberInfo, $param = [])
    {
        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];

        $GreetingInfo = IntoGroupReplyModel::getIntoGroupGreetingInfo($param) ?: [];
        return ['greetingInfo' => $GreetingInfo];
    }


    public static function updateIntoGroupReply($memberInfo, $param = [])
    {
        $checkResult = self::checkIntoGroupReplyParam($param, 'update');
        if (isset($checkResult['code'])) {
            return $checkResult;
        }
        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];
        if (isset($param['reply_address']) && $param['reply_address'] == 1) {
            if (isset($param['robot_id'])) {
                $param['tag_id'] = $param['robot_id'];
            }
        }
        if (isset($param['robot_id'])) {
            unset($param['robot_id']);
        }
        $id = $param['id'];
        unset($param['id']);

        $StrategyCategory = IntoGroupReplyModel::where('id',$id)->update($param);

        if ($StrategyCategory)
        {
            $logMessage = '修改关键词消息回复';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($param), $logMessage, $StrategyCategory
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    public static function deleteIntoGroupReply($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        $update = [
            'deleted_at' => time()
        ];
        $delete_result = IntoGroupReplyModel::where('id',$param['id'])->update($update);
        if ($delete_result)
        {
            $logMessage = '删除关键词消息回复';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    private static function checkIntoGroupReplyParam($param, $type = '')
    {
        if ($type == 'update') {
            if (!isset($param['id']) || empty($param['id'])) {
                return ['code' => 400, 'msg' => 'ID不能为空'];
            }
        }
        if (empty($param['title'])) {
            return ['code' => 400, 'msg' => '标题不能为空'];
        }
//        if (empty($param['style'])) {
//            return ['code' => 400, 'msg' => '消息回复样式不能为空'];
//        }
        if (empty($param['type'])) {
            return ['code' => 400, 'msg' => '消息类型不能为空'];
        }
        if (empty($param['content'])) {
            return ['code' => 400, 'msg' => '内容不能为空'];
        }
        return [];
    }
}