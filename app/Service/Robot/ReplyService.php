<?php


namespace App\Service\Robot;


use App\Model\Community\CommunityInvitationGroupModel;
use App\Model\Robot\MessageKeywordModel;
use App\Model\Robot\MessageKeywordReplyModel;

class ReplyService
{
    public static function getKeyWordsList($param)
    {
        $pageSize = isset($param["pageSize"]) ? $param["pageSize"] : 20;
        $keyWordsListData = MessageKeywordModel::getKeyWordsList($param, $pageSize);
        if ($keyWordsListData && isset($keyWordsListData['data'])) {
            foreach ($keyWordsListData['data'] as $key => &$val) {
                if ($val->tag_id) {
                    $tagIds = explode(',', $val->tag_id);
                    $tag_name = CommunityInvitationGroupModel::getTagNameByTagIds($tagIds);
                    $tag_name = implode(',', $tag_name);
                } else {
                    $tag_name = '';
                }
                $val->tag_name = $tag_name;
            }
        }
        return $keyWordsListData;
    }

    public static function getKeyWordsDetail($param)
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => '关键词不能为空'];
        }
        $keyWordsInfo = MessageKeywordModel::getKeyWordsDetail($param);
        $tag = [];
        if ($keyWordsInfo->tag_id) {
            $tagIds = explode(',', $keyWordsInfo->tag_id);
            foreach ($tagIds as $key => $val) {
                $tag[$key]['tag_id'] = $val;
                $tag[$key]['tag_name'] = CommunityInvitationGroupModel::getTagNameByTagId($val);
            }
            $tag_name = CommunityInvitationGroupModel::getTagNameByTagIds($tagIds);
        } else {
            $tag_name = '';
        }
        $keyWordsInfo->tag_name = $tag_name;
        $keyWordsInfo->tag = $tag;
        return ['code' => 200, 'data' =>$keyWordsInfo];
    }

    /**
     * 关键词回复列表
     * @param $param
     * @return mixed
     */
    public static function keyWordsReplyList($param)
    {
        $pageSize = isset($param["pageSize"]) ? $param["pageSize"] : 20;
        $page = isset($param["page"]) ? $param["page"] : 1;
        $begin = isset($param["page"]) ? ($param["page"] - 1) * $pageSize : 0;
        $result = MessageKeywordReplyModel::keyWordsReplyList($param, $begin, $pageSize);
        $count = MessageKeywordReplyModel::keyWordsReplyCount($param);
        return [
            'data' => $result,
            'total'=>$count,
            'page' => $page,
            'pageSize'=>$pageSize
        ];
    }

    public static function addKeyWords($memberInfo, $param = [])
    {
        if (empty($param['keyword']))
        {
            return ['code' => 400, 'msg' => '关键词不能为空'];
        }
        if (isset($param['tag_id']))
        {
            if (!is_array($param['tag_id'])) {
                return ['code' => 400, 'msg' => '标签ID类型不正确'];
            }
            $param['tag_id'] = implode(',', $param['tag_id']);
        }

        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];


        $insertId = MessageKeywordModel::addKeyWords($param);

        if ($insertId)
        {
            $logMessage = '新增关键词';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $insertId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    public static function addKeyWordsReply($memberInfo, $param = [])
    {
        if (empty($param['content'])) {
            return ['code' => 400, 'msg' => '内容不能为空'];
        }
        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];


        $insertId = MessageKeywordReplyModel::insertGetId($param);

        if ($insertId)
        {
            $logMessage = '新增关键词消息回复';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $insertId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function updateKeyWords($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }

        if (!isset($param['keyword']) || empty($param['keyword'])) {
            return ['code' => 400, 'msg' => '关键字内容不能为空'];
        }

        if (isset($param['tag_id']))
        {
            if (!is_array($param['tag_id'])) {
                return ['code' => 400, 'msg' => '标签ID类型不正确'];
            }
            $param['tag_id'] = implode(',', $param['tag_id']);
        }

        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];

        $id = $param['id'];
        unset($param['id']);
        $StrategyCategory = MessageKeywordModel::where('id',$id)->update($param);

        if ($StrategyCategory)
        {
            $logMessage = '修改关键词';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($param), $logMessage, $StrategyCategory
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function updateKeyWordsReply($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        if (empty($param['title'])) {
            return ['code' => 400, 'msg' => '标题不能为空'];
        }
        if (empty($param['content'])) {
            return ['code' => 400, 'msg' => '内容不能为空'];
        }

        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];

        $id = $param['id'];
        unset($param['id']);
        $StrategyCategory = MessageKeywordReplyModel::where('id',$id)->update($param);

        if ($StrategyCategory)
        {
            $logMessage = '修改关键词消息回复';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 2, json_encode($param), $logMessage, $StrategyCategory
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }

    public static function deleteKeyWords($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        //查看该分类ID下是否存在文章
//        $where = [
//            'strategy_category_id' => $param['id'],
//            'deleted_at' => 0
//        ];
//        $articleCount = MessageKeywordModel::where($where)->count();
//        if ($articleCount > 0) {
//            return ['code' => 400, 'msg' => '该分类下存在文章,不能删除'];
//        }
        $update = [
            'deleted_at' => time()
        ];
        $delete_result = MessageKeywordModel::where('id',$param['id'])->update($update);
        if ($delete_result)
        {
            $logMessage = '删除关键词';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
        //$data = MessageKeywordModel::deleteKeyWords($param, $pageSize);
    }

    public static function deleteKeyWordsReply($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        $update = [
            'deleted_at' => time()
        ];
        $delete_result = MessageKeywordReplyModel::where('id',$param['id'])->update($update);
        if ($delete_result)
        {
            $logMessage = '删除关键词消息回复';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }
}