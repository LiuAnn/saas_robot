<?php


namespace App\Service\Robot;


use App\Model\Admin\GroupModel;
use App\Model\Community\CommunityInvitationGroupModel;
use App\Model\Robot\KeywordModel;

class KeyWordService
{
    public static function getKeyWordsList($param)
    {
        $pageSize = isset($param["pageSize"]) ? $param["pageSize"] : 20;
        $keyWordsListData = KeywordModel::getKeyWordsList($param, $pageSize);
        if ($keyWordsListData && isset($keyWordsListData['data'])) {
            foreach ($keyWordsListData['data'] as $key => &$val) {
                if ($val->tag_id) {
                    $tagIds = explode(',', $val->tag_id);
                    $tag_name = CommunityInvitationGroupModel::getTagNameByTagIds($tagIds);
                    $tag_name = implode(',', $tag_name);
                } else {
                    $tag_name = '';
                }
                $val->tag_name = $tag_name;
            }
        }
        return $keyWordsListData;
    }


    public static function keyWordsDetail($param)
    {
        if (empty($param['id']))
        {
            return ['code' => 400, 'msg' => 'id不能为空'];
        }
        $keyWordsInfo = KeywordModel::fetchKeyWordsInfo($param['id']);
        if (empty($keyWordsInfo)) {
            return ['code' => 400, 'msg' => '关键词不存在'];
        }
        if (!empty($keyWordsInfo['deleted_at'])) {
            return ['code' => 400, 'msg' => '关键词已删除'];
        }
        //以下默认获取一周数据
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = date('Y-m-d H:i:s', $param['beginTime']);
            $param['endTime'] = date('Y-m-d H:i:s', $param['endTime']);
        }
        $page = $param['page'] ?? 1;
        $pageSize = $param['pageSize'] ?? 20;
        $begin = ($page-1) * $pageSize;
        $keyWordsListData = KeywordModel::keyWordsDetail($param, $begin, $pageSize);
        $keyWordsListCount = KeywordModel::keyWordsDetailCount($param);
        $return = [];
        $return['data'] = $keyWordsListData;
        $return['total'] = $keyWordsListCount;
        $return['page'] = $page;
        $return['pageSize'] = $pageSize;
        return $return;
    }


    public static function addKeyWords($memberInfo, $param = [])
    {
        if (empty($param['keyword']))
        {
            return ['code' => 400, 'msg' => '关键词不能为空'];
        }
        if (isset($param['tag_id']))
        {
            if (!is_array($param['tag_id'])) {
                return ['code' => 400, 'msg' => '标签ID类型不正确'];
            }
            $param['tag_id'] = implode(',', $param['tag_id']);
        }

        $param['mid'] = $memberInfo['mid'];
        $param['platform_identity'] = $memberInfo['platform_identity'];


        $insertId = KeywordModel::addKeyWords($param);

        if ($insertId)
        {
            $logMessage = '新增记录关键词';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 1, json_encode($param), $logMessage, $insertId
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
    }


    public static function deleteKeyWords($memberInfo, $param = [])
    {
        if (!isset($param['id']) || empty($param['id'])) {
            return ['code' => 400, 'msg' => 'ID不能为空'];
        }
        $update = [
            'deleted_at' => time()
        ];
        $where = [
            'mid' => $memberInfo['mid'],
            'platform_identity' => $memberInfo['platform_identity'],
            'id' => $param['id']
        ];
        $delete_result = KeywordModel::where($where)->update($update);
        if ($delete_result)
        {
            $logMessage = '删除记录关键词';
            UserOperationLogService::insertAdminOperation(
                $memberInfo['mid'], 3, json_encode($param), $logMessage, $param['id']
            );
            return ['code' => 200, 'msg' => '操作成功!'];
        }else{
            return ['code' => 400, 'msg' => '操作失败!'];
        }
        //$data = KeywordModel::deleteKeyWords($param, $pageSize);
    }

}