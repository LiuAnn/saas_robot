<?php

namespace App\Service\Common;


use App\Service\Mall\OperateSerivice;
use Mail;

class MailService {
    public static function sendStatsData()
    {
        $yesterDay = time() - 86400;
        $start     = strtotime(date('Y-m-d', $yesterDay));
        $end       = strtotime(date('Y-m-d'));
        $statsData = OperateSerivice::statsData($start, $end);
        $view = 'emails.stats';
        $to   = 'dixon@yuelvhui.com';
        $subject = "悦淘运营日报。";

        Mail::send($view, $statsData, function ($message) use ( $to, $subject) {
            $message->to($to)->cc(['jiangshelei@yuelvhui.com', 'houyanli@yuelvhui.com', 'chenjunlei@yuelvhui.com','dengliangpeng@yuelvhui.com', 'liuyi@yuelvhui.com'])->subject($subject);
        });
    }
}