<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/15
 * Time: 20:59
 */

namespace App\Service\Source;

use App\Model\Admin\AdminUserModel;
use App\Model\Mall\YanModel;
use App\Model\Member\MemberModel;
use  App\Model\Source\SendInfoModel;
use  App\Model\Community\CommunityModel;
use  App\Model\Community\GroupActivationModel;
use App\Models\SendMiniApp;
use App\Service\Robot\UserOperationLogService;
use App\Model\Community\CommunityInvitationGroupModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Model\Admin\GroupModel;
use App\Model\RobotOrder\RobotModel;
use App\Model\Community\GroupManageModel;
use  App\Service\VoucherBag\CouponService;
class LotteryDrawService
{
    /**
     * 获取我的可以发消息的群
     * @return array
     */
    public static function getMyRoomListByMid($param,$member)
    {
        $mid = !empty($param["mid"]) ? $param["mid"] :0;
        if(empty($mid)){
            return [
                "code" => 200,
                "msg" => '缺少参数',
                "data" => [],
            ];
        }
        if($member['user_type']==2){
            $result=[];
        }else{
            $result    = SendInfoModel::getMyRoomListByMid($mid);
        }
        return [
            "code" => 200,
            "data" => $result,
        ];
    }


    //链接类型链接类型  1 直播间 2 商品列表  3 单品  4 普通商品
   public static function getLinkType(){
         return [
             1=>'直播间',
             2=>'商品列表',
             3=>'单品',
             4=>'普通商品',
             5=>'京东',
             6=>'拼多多',
         ];
   }


    //链接类型链接类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9 品牌折扣券 10 话费充值 11 视频充值 12 加油 13 酒店
   public static function getSelectType(){

            return [
                ['id'=>1,'name'=>'自营'],
                ['id'=>2,'name'=>'京东'],
                ['id'=>3,'name'=>'拼多多'],
                ['id'=>4,'name'=>'唯品会'],
                ['id'=>5,'name'=>'美团'],
                ['id'=>7,'name'=>'淘宝'],
                ['id'=>8,'name'=>'苏宁'],
                ['id'=>9,'name'=>'品牌折扣券'],
                ['id'=>10,'name'=>'话费充值'],
                ['id'=>11,'name'=>'视频充值'],
                ['id'=>12,'name'=>'加油'],
                ['id'=>13,'name'=>'酒店'],
                ['id'=>14,'name'=>'电影票']
            ];
   }   

    /**
     * 消息列表
     * @param $param
     * @return array
     */
    public static function fodderTypeList($member)
    {
        return SendInfoModel::getFodderTypeList($member['platform_identity']);
    }

    /**
     * 消息列表
     * @param $param
     * @return array
     */
    public static function linkTypeList($member, $param)
    {
        if (!isset($param['type_id']) || empty($param['type_id'])) {
            //4 链接类型ID
            $param['type_id'] = 4;
        }

        if(isset($param['sourse'])&&$param['sourse']==2){
            return [
                ['id'=>4,'name'=>'自营商品'],
                ['id'=>5,'name'=>'京东商品'],
                ['id'=>6,'name'=>'拼多多'],
                ['id'=>7,'name'=>'淘宝'],
                ['id'=>8,'name'=>'唯品会'],
                ['id'=>9,'name'=>'苏宁'],
                ['id'=>11,'name'=>'折扣券'],
            ];

        }else{

            return SendInfoModel::getLinkTypeList($member['platform_identity'], $param['type_id']);

        }

    }

    /**
     * 消息列表
     * @param $param
     * @return array
     */
    public static function getSourceMaterialList($param,$member)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] :10;
        $mid = !empty($param["mid"]) ? $param["mid"] :0;
        $param['sup_id'] = $member['sup_id'];
        $param['mid'] = $member['mid'];
        $param['platform_identity'] = $member['platform_identity'];

        if(empty($mid)){
            $param['mid']=$member['mid'];
        }
        if(empty($mid)){
            return [
                "code" => 200,
                "msg" => '缺少参数',
                "data" => [],
            ];
        }
        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $searchList = self::searchList($param['mid'],$param['sup_id']);



        $result    = SendInfoModel::getCommunitySendInfos('random_str',$param);
        $count     = SendInfoModel::getCommunitySendInfoCount($param);
        //
        $wxIds=[];
        if ($member['user_type'] >= 2) {
             $wxIds = GroupModel::getAllRobotWxIds($param);
             $countNum = SendInfoModel::getGroupCountNum($wxIds);
        }else{
             $countNum = SendInfoModel::getGroupCountNumMid($member['mid']);
        }

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "searchList"=> $searchList,
                "pageSize"  => $display,
                "list"      => self::formatCommunitySendInfos($result,$param['mid'],$member['user_type'],$countNum, $param['sup_id'], $param['platform_identity'],$wxIds,$page,$display)
            ],
        ];
    }



    /**
     * 消息列表
     * @param $param
     * @return array
     */
    public static function getSourceMaterialListV2($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] :10;


        if((!isset($param['platform_identity'])||empty($param['platform_identity']))||(!isset($param['exclusive_mid'])||empty($param['exclusive_mid']))){
            return [
                "code" => 200,
                "msg" => '参数不正确！',
                "data" => [],
            ];
        }

        $param['exclusive_mid'] = $param['exclusive_mid'];
        $param['platform_identity'] = $param['platform_identity'];


        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = SendInfoModel::getCommunitySendInfosV2('random_str',$param);
        $count     = SendInfoModel::getCommunitySendInfoCountV2($param);

        $countNum = SendInfoModel::getGroupCountByExMid($param['exclusive_mid']);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatCommunitySendInfosV2($result,$param['exclusive_mid'],1,$countNum, $param['platform_identity'],$page,$display)
            ],
        ];
    }


    /**
     * 列表数据整合
     * @param array $data
     * @return array
     */
    public static function formatCommunitySendInfosV2($data=[],$mid,$user_type,$countNum, $platform_identity,$page=1,$pageSize=10)
    {
        $linkType  =  self::getLinkType();
        $list = [];

        if($data)
        {
            foreach ($data as $key=>$item)
            {
                $data=SendInfoModel::getCommunitySendInfo2s(['randomStr'=>$item->random_str,'platform_identity'=>$platform_identity]);
                $sendall = SendInfoModel::getSendgGroupNum($item->random_str,[]);
                $sendEnd =  $sendall > $countNum ?$countNum:$sendall;
                $sendNum = "$sendEnd/$countNum";
                foreach ($data as $k=>$value)
                {   
                    $list[$key][$k]['key'] = ($page-1)*$pageSize+$key+1;
                    $list[$key][$k]['mid']       = $value->mid;
                    $list[$key][$k]['exclusive_mid']       = $value->exclusive_mid;
                    $list[$key][$k]['id']       = $value->id;
                    $list[$key][$k]['type']     = self::getTypeName2($value->type);
                    $list[$key][$k]['isSend']   = self::getIsSend($value->is_send);
                    $list[$key][$k]['robotSendStatus']= self::getRobotSendData($value->random_str,$mid,1);
                    $list[$key][$k]['status']   = self::getStatus($value->status);
                    $list[$key][$k]['content']  = $value->type==5?$value->link_title:$value->content;
                    if ($value->type == 8) {
                        $list[$key][$k]['content']  = '小程序';
                    }
                    $list[$key][$k]['shortUrl']  = $value->short_url;
                    $list[$key][$k]['randomStr']= $value->random_str;
                    $list[$key][$k]['createdAt']= $value->created_at;
                    $list[$key][$k]['msg_type']= self::getMsgType($value->status);
                    $list[$key][$k]['timing_time']= self::getFormatTime($value->timing_send_time);
                    $list[$key][$k]['sendNum']= '已发送群'.$sendNum;
                    $list[$key][$k]['link_type']= $value->link_type>0?(in_array($value->link_type,$linkType)?$linkType[$value->link_type]:''):'';
                }
            }
        }
        return $list;
    }


    public static function getMiniAppTemplateIdList()
    {
        $MiniXmlList = SendMiniApp::getMiniAppTemplateIdList();
        return [
            "code" => 200,
            "data" => $MiniXmlList
        ];
    }

    public static function searchList($mid,$sup_id)
    {
        $data = [];

        $data[0]['userName'] = "admin";
        $data[0]['userId'] = $mid;
        if($sup_id>0)
        {
            $data[0]['userId'] = $sup_id;
            $data[1]['userName'] = "我发送的";
            $data[1]['userId'] = $mid;

        }else{

            $userList = SendInfoModel::getMyUserList($mid);

            foreach ($userList as $key => $v) {
                
                $data[$key+1]['userName'] = empty($v->name)?$v->nickname:$v->name;
                $data[$key+1]['userId'] = $v->mid;
            }
        }
        return $data;

    }

    /**
     * 列表数据整合
     * @param array $data
     * @return array
     */
    public static function formatCommunitySendInfos($data=[],$mid,$user_type,$countNum,$sup_id, $platform_identity,$wxIds,$page=1,$pageSize=10)
    {
        $linkType  =  self::getLinkType();
        $list = [];
        $userData =  SendInfoModel::allUserList();
        $userName =[];
        foreach($userData as $name){
            $userName[$name->mid] =$name->name;
        }

        if($data)
        {
            foreach ($data as $key=>$item)
            {
                $data=SendInfoModel::getCommunitySendInfo2s(['randomStr'=>$item->random_str,'mid'=>$mid,'sup_id'=>$sup_id,'platform_identity'=>$platform_identity]);
                $sendall = SendInfoModel::getSendgGroupNum($item->random_str,$wxIds);
                $sendEnd =  $sendall > $countNum ?$countNum:$sendall;
                $sendNum = "$sendEnd/$countNum";
                foreach ($data as $k=>$value)
                {   
                    $list[$key][$k]['key'] = ($page-1)*$pageSize+$key+1;
                    if($sup_id==0){
                        $sentby =  $value->mid==$mid?'admin': (empty($userName[$value->mid])?'':$userName[$value->mid]);
                    }else{
                        $sentby =  $value->mid==$sup_id?'admin': (empty($userName[$value->mid])?'':$userName[$value->mid]);
                    }
                    $list[$key][$k]['sentby']       =  $sentby;
                    $list[$key][$k]['sup_id']       = $sup_id;
                    $list[$key][$k]['mid']       = $value->mid;
                    $list[$key][$k]['exclusive_mid']       = $value->exclusive_mid;
                    $list[$key][$k]['id']       = $value->id;
                    $list[$key][$k]['type']     = self::getTypeName2($value->type);
                    $list[$key][$k]['isSend']   = self::getIsSend($value->is_send);
                    $list[$key][$k]['robotSendStatus']= self::getRobotSendData($value->random_str,$mid,$user_type,$sup_id);
                    $list[$key][$k]['status']   = self::getStatus($value->status);
                    $list[$key][$k]['content']  = $value->type==5?$value->link_title:$value->content;
                    if ($value->type == 8) {
                        $list[$key][$k]['content']  = '小程序';
                    }
                    $list[$key][$k]['shortUrl']  = $value->short_url;
                    $list[$key][$k]['randomStr']= $value->random_str;
                    $list[$key][$k]['createdAt']= $value->created_at;
                    $list[$key][$k]['msg_type']= self::getMsgType($value->status);
                    $list[$key][$k]['timing_time']= self::getFormatTime($value->timing_send_time);
                    $list[$key][$k]['sendNum']= '已发送群'.$sendNum;
                    $list[$key][$k]['link_type']=  in_array($value->link_type,$linkType)?$linkType[$value->link_type]:'';
                }
            }
        }
        return $list;
    }

    /**
     * 机器人发送状态
     * @param $random_str
     * @param $mid
     * @param int $user_type
     * @return string
     */
    public static function getRobotSendData($random_str,$mid,$user_type,$sup_id=0)
    {

       if($user_type==1){
           $res = SendInfoModel:: getIsSendReload(['randomStr'=>$random_str]);
           if($res==0){
               return '未发送';
           }else{
               return '已发送';
           }
       }else if($user_type==2){

            if($sup_id==0){
                $res = SendInfoModel:: getIsSendReload(['randomStr'=>$random_str]);
                $serviceWxid =   RobotModel::getBindWxDataByMidData($mid);
            }else{
                $res = SendInfoModel:: getIsSendReloadTwo(['randomStr'=>$random_str,'mid'=>$mid]);
                $serviceWxid =   RobotModel::getBindWxDataByMid($mid);
            }
           if($res==$serviceWxid&&$res>0){
               return '所有机器人已发送'. $res.'/'.$serviceWxid;
           }else{
               return  ' 已发送机器人/所有机器人  '. $res.'/'.$serviceWxid;
           }
       }

    }


    /**
     * 格式化消息类型
     * @param int $type
     * @return string
     */
    public static function getMsgType($type=0)
    {
        $str = '';//1 定时  2 非定时
        switch ($type) {
            case 1:
                $str = '定时消息';
                break;
            default:
                $str = '';
                break;
        }
        return $str;
    }
    /**
     * 格式化定时时间
     * @param int $timing_time
     * @return false|string
     */
    public static function getFormatTime($timing_time=0)
    {
        $str='';
         if($timing_time>0){
             $str = date("Y-m-d H:i:s",$timing_time);
         }
        return $str;
    }

    /**
     * 格式化消息类型
     * @param int $type
     * @return string
     */
    public static function getTypeName2($type=0)
    {
        $str = '';//1 文本 2 图片 3 视频 4 链接 8小程序显示文本
        switch ($type) {
            case 1:
                $str = '文本';
                break;
            case 2:
                $str = '图片';
                break;
            case 3:
                $str = '视频';
                break;
            case 4:
                $str = '链接';
                break;
            case 5:
                $str = '优惠券';
                break;
            case 8:
                $str = '文本';
                break;
            default:
                $str = '';
                break;
        }
        return $str;
    }

    /**
     * 发送状态
     * @param string $isSend
     * @return string
     */
    public static function getIsSend($isSend='')
    {
        $str = '';//0 未发送 1 已发送',
        switch ($isSend) {
            case 0:
                $str = '不发送';
                break;
            case 1:
                $str = '要发送';
                break;
            default:
                $str = '';
                break;
        }
        return $str;

    }

    /**
     * 发送类型
     * @param string $status
     * @return string
     */
    public static function getRobotSendStatus($status='')
    {
        $str = '';//0 未发送 1 已发送',
        switch ($status) {
            case 0:
                $str = '所有机器人未发送';
                break;
            case 1:
                $str = '所有机器人已发送';
                break;
            default:
                $str = '';
                break;
        }
        return $str;

    }

    /**
     * @param string $status
     * @return string
     */
    public static function getStatus($status='')
    {
        $str = '';//0 正常 1 删除
        switch ($status) {
            case 0:
                $str = '正常';
                break;
            case 1:
                $str = '删除';
                break;
            default:
                $str = '';
                break;
        }
        return $str;

    }
    /**
     * 获取消息详细
     * @param $param
     * @return array
     */
    public static function getSourceMaterialInfo($param,$member)
    {
        if(!isset($param['randomStr'])||empty($param['randomStr']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }
        $param['sup_id'] = $member['sup_id'];
        $param['mid']    =  $member['mid'];
        $param['platform_identity']    =  $member['platform_identity'];
        $result    = SendInfoModel::getCommunitySendInfo2s($param);
        if(empty($result))
        {
            return [ "code" => 200, "data" =>[]];
        }
        return [
            "code" => 200,
            "data" => self::formatCommunitySendInfo($result,$member),
            "roomids" =>$result[0]->room_ids,
        ];
    }


    /**
     * 获取消息详细
     * @param $param
     * @return array
     */
    public static function getSourceMaterialInfoV2($param)
    {
        if(!isset($param['randomStr'])||empty($param['randomStr']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }

        $param['platform_identity']    =  $param['platform_identity'];
        $result    = SendInfoModel::getCommunitySendInfo2s($param);
        if(empty($result))
        {
            return [ "code" => 200, "data" =>[]];
        }
        return [
            "code" => 200,
            "data" => self::formatCommunitySendInfoV2($result,$param),
            "roomids" =>$result[0]->room_ids,
        ];
    }


    /**
     * 获取 消息详细
     * @param $data
     * @return array
     */
    public static function formatCommunitySendInfoV2($data,$member)
    {

        $list = [];
        if($data)
        {
            foreach ($data as $value)
            {

                $is_edit=$value->exclusive_mid==$member['exclusive_mid']?1:0;
                $list[]=[
                    "is_edit"                    => $is_edit,
                    "mid"                   => $value->mid,
                    "link_type"            => $value->link_type,
                    "isMoreProduct"            => $value->is_more_product,
                    "isMoreTags"           => empty($value->isMoreTags)?0:$value->isMoreTags,
                    "tag_ids"              => empty($value->tag_ids)?0:$value->tag_ids,
                    "id"                    => $value->id,
                    "type"                  => $value->type,
                    "isSend"                => $value->is_send,
                    "link_title"            => $value->link_title,
                    "link_img_url"          => $value->link_img_url,
                    "link_desc"             => $value->link_desc,
                    "cid"                   => empty($value->cid)?0:$value->cid,
                    "coupon_num"            => empty($value->coupon_num)?0:$value->coupon_num,
                    "status"                => $value->status,
                    "content"               => $value->content. '',
                    "shortUrl"              => !empty($value->short_url)?$value->short_url . '':'',
                    "createdAt"             => !empty($value->created_at)?$value->created_at . '':'',
                     "msg_type"             =>self::getMsgType($value->is_timing_send),
                     "timingSendTime"          =>date("Y-m-d H:i:s ",$value->timing_send_time),
                     "roomids"               =>empty($value->room_ids)?'':$value->room_ids,
                     "isTimingSend"               =>empty($value->is_timing_send)?0:$value->is_timing_send,
                ];
            }
        }
        return $list;
    }


    /**
     * 获取 消息详细
     * @param $data
     * @return array
     */
    public static function formatCommunitySendInfo($data,$member)
    {

        $list = [];
        if($data)
        {
            foreach ($data as $value)
            {
                if($member['sup_id']==0){
                    $is_edit=1;
                }else{
                    $is_edit=$value->mid==$member['mid']?1:0;
                }
                $list[]=[
                    "is_edit"                    => $is_edit,
                    "mid"                   => $value->mid,
                    "link_type"            => $value->link_type,
                    "isMoreProduct"            => $value->is_more_product,
                    "isMoreTags"           => empty($value->isMoreTags)?0:$value->isMoreTags,
                    "tag_ids"              => empty($value->tag_ids)?0:$value->tag_ids,
                    "id"                    => $value->id,
                    "type"                  => $value->type,
                    "isSend"                => $value->is_send,
                    "link_title"            => $value->link_title,
                    "link_img_url"          => $value->link_img_url,
                    "link_desc"             => $value->link_desc,
                    "cid"                   => empty($value->cid)?0:$value->cid,
                    "coupon_num"            => empty($value->coupon_num)?0:$value->coupon_num,
                    "status"                => $value->status,
                    "content"               => $value->content. '',
                    "shortUrl"              => !empty($value->short_url)?$value->short_url . '':'',
                    "createdAt"             => !empty($value->created_at)?$value->created_at . '':'',
                     "msg_type"             =>self::getMsgType($value->is_timing_send),
                     "timingSendTime"          =>date("Y-m-d H:i:s ",$value->timing_send_time),
                     "roomids"               =>empty($value->room_ids)?'':$value->room_ids,
                     "isTimingSend"               =>empty($value->is_timing_send)?0:$value->is_timing_send,
                ];
            }
        }
        return $list;
    }

    /**
     * 处理时间戳
     * @param timestamp
     * @return string
     */
    private static function formatTimeStamp($timestamp)
    {
        return !empty($timestamp) ? date("Y-m-d H:i:s", $timestamp) : "暂无";
    }

    /**
     * 添加数据
     * @param $param
     * @return array
     */
    public static function addgetSourceMaterial($param,$member)
    {
        //验证
        $check_input = self::checkInputCommunitySendInfo($param);

        if($check_input){
            return $check_input;
        }
        $mid = !empty($param["mid"]) ? $param["mid"] :0;
        $room_ids = !empty($param["roomids"]) ? $param["roomids"] :'';
        if(empty($mid)){
            return [
                "code" => 400,
                "msg" => '参数错误！',
                "data" => [],
            ];
        }

        //当前时间戳
        $timestamp              =   time();
        $created_at             =   self::formatTimeStamp($timestamp);
        $str  = self::getStr();
        $info = SendInfoModel::getCommunitySendInfo2s(['randomStr'=>$str,'sup_id'=>$member['sup_id'],'mid'=>$mid,'platform_identity'=>$member['platform_identity']]);
        if(!empty($info))
        {
            return ['code' => 400 , 'msg' => '重新点击一下！'];
        }
        $msg_type = $member['user_type'];

        $add=[];
        $saveInsertData = [];
        $tag_ids = "";
        //根据mid  获取wxid
        foreach ($param['data'] as $key=>$value) {
            //这里是CPS活动传输的长链保存为短链
            if ($value['type'] == 7) {
                $value['type'] = 1;
                //use  App\Service\VoucherBag\CouponService;
                $url = !empty($value['shortUrl']) ? $value['shortUrl'] : '';
                //CPS活动也地址 TODO
                $host = 'https://web.yuetao.vip/blank';
                $url = $host . "?robot_send=1&url=" . urlencode($url);

                $value['shortUrl'] = CouponService::getShortUrlByLongUrl($url);
            }
            //type 8 为发送小程序
            if ($value['type'] == 8) {
                //name:小程序名称  userName:小程序原始ID  title:分享标题 pagePath:小程序跳转路径
                $name = $value['name'] ?? '';
                $userName = isset($value['userName']) ? $value['userName'] . '@app' : '';
                $title = $value['title'];
                $pagePath = $value['pagePath'] ?? '';
                //模板下拉选择, 有模板ID数据库查询
                if (isset($value['templateId']) && !empty($value['templateId'])) {
                    $baseXml = SendMiniApp::getMiniAppXMLInfoById($value['templateId']);
                    if (empty($baseXml)) {
                        return [
                            "code" => 400,
                            "msg" => '小程序模板文件不存在',
                            "data" => [],
                        ];
                    }
                    $baseXml = CouponService::querySendMiniAppXml($baseXml, $value);
                } else {
                    $baseXml = file_get_contents(config_path() . '/miniAppBase.xml');
                    $baseXml = str_replace(
                        ['{name}', '{userName}','{title}','{pagePath}'],
                        [$name, $userName, $title, $pagePath],
                        $baseXml
                    );
                }
                $value['content'] = $baseXml;
                //test
            }
            $add[$key]['mid'] = !empty($mid) ? $mid : 0;
            $add[$key]['type'] = !empty($value['type']) ? $value['type'] : 0;
            $add[$key]['is_more_product'] = !empty($value['isMoreProduct']) ? $value['isMoreProduct'] : 0;
            $add[$key]['cid'] = !empty($value['cid']) ? $value['cid'] : 0;
            $add[$key]['link_title'] = !empty($value['link_title']) ? $value['link_title'] : '';
            $add[$key]['link_desc'] = !empty($value['link_desc']) ? $value['link_desc'] : '';
            $add[$key]['link_img_url'] = !empty($value['link_img_url']) ? $value['link_img_url'] : '';
            $add[$key]['coupon_num'] = !empty($value['coupon_num']) ? $value['coupon_num'] : '';
            $add[$key]['link_type'] = !empty($value['link_type']) ? $value['link_type'] : 4;
            $add[$key]['is_send'] = !empty($value['isSend']) ? $value['isSend'] : 0;
            $add[$key]['status'] = !empty($value['status']) ? $value['status'] : 0;
            $add[$key]['is_timing_send'] = !empty($value['isTimingSend']) ? $value['isTimingSend'] : 0;
            $add[$key]['timing_send_time'] = !empty($value['timingSendTime']) ? strtotime($value['timingSendTime']) : 0;
            $add[$key]['short_url'] = !empty($value['shortUrl']) ? $value['shortUrl'] : '';
            $add[$key]['content'] = !empty($value['content']) ? $value['content'] : '';
            $add[$key]['random_str'] = $str;
            $add[$key]['created_at'] = $created_at;
            $add[$key]['room_ids'] = $room_ids;
            $add[$key]['msg_type'] = $msg_type;
            $add[$key]['tag_ids'] = !empty($value['tag_ids']) ? $value['tag_ids'] : 0;
            $add[$key]['typeid'] = !empty($value['typeid']) ? $value['typeid'] : 0;
            $add[$key]['platform_identity'] = isset($member['platform_identity']) ? $member['platform_identity'] : '';
            $add[$key]['isMoreTags'] = isset($value['isMoreTags'])?$value['isMoreTags']:0;

            if(isset($value['isMoreTags'])&&$value['isMoreTags']==1){

                $tags1 = explode(",",$value['tag_ids']);

                if($tag_ids==""&&$value['tag_ids']!=""){

                    $tag_ids = $value['tag_ids'];

                }else if($tag_ids!=""&&$value['tag_ids']!="") {

                    $tag_array1 = explode(",",$tag_ids);
                    $tag_array2 = explode(",",$value['tag_ids']);
                    
                    $array3 = array_keys(array_flip($tag_array1)+array_flip($tag_array2));

                    $tag_ids = implode(",",$array3);

                }


            }else{

                $tag_ids = $value['tag_ids'];
            }



            if ($value['type'] == 5) {
                $add[$key]['link_img_url'] = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-05-24/22/yuelvhuihbUe9hAV401590330312.jpg";
                self::getSendCouponList($value, $member);
            }
            if ($member['platform_identity'] == '7323ff1a23f0bf1cda41f690d4089353' && $member['user_type'] >= 2) {
                if (isset($value['shortUrl'])) {
                    $shortUrl = !empty($value['shortUrl']) ? $value['shortUrl'] : '';
                    if (!empty($shortUrl)) {
                        $shortCode = substr(trim($shortUrl), -6);
                        $longUrl = YanModel::getProductLongUrl($shortCode);
                        //判断长连接 是否自营 京东 拼多多
                        //自营链接 goodsDetailZY 京东,拼多多 goodsDetailCPS
                        //商品保存字段 product_type 商品类型：1:自营，2:京东，3:拼多多，4:唯品会，5:淘宝
                        //小程序接口字段 //4自营 5京东 6拼多多 7淘宝 8唯品会  9苏宁
                        if (preg_match("/goodsDetailZY/", $longUrl)) {
                            $arr = explode('&', parse_url($longUrl)['query']);
                            foreach ($arr as $k => $v) {
                                $arr = explode('=', $v);
                                $queryData[$arr[0]] = $arr[1];
                            }
                            $postUrl = 'https://gateway.yuetao.vip/shop/v/mall/getProductBuyDatailV4';
                            $product_type = 4;
                            $params = [];
                            $params['product_id'] = $queryData['product_id'];
                            $params['product_sku_id'] = $queryData['product_sku_id'];
                            $params['uid'] = '0';
                            $postResult = curlPost($postUrl, $params);
                            if (false == $postResult) {
                                \Log::error("保存素材接口请求异常" . json_encode($params));
                                continue;
                            }
                            $postResult = json_decode($postResult, true);
                            if (isset($postResult['code']) && $postResult['code'] == 200) {
                                $goodInfo = $postResult['data'];
                                $where = [
                                    'product_id' => $goodInfo['product_id'],
                                    'platform_identity' => $member['platform_identity']
                                ];
                                $sendDataExist = SendInfoModel::getAutoSendData($where);
                                if ($sendDataExist) {
                                    continue;
                                }
                                $saveInsertData[$key]['product_type'] = $product_type;
                                $saveInsertData[$key]['product_id'] = $goodInfo['product_id'];
                                $saveInsertData[$key]['sku_id'] = $queryData['product_sku_id'];
                                $saveInsertData[$key]['product_name'] = $goodInfo['goodName'];
                                $saveInsertData[$key]['product_price'] = $goodInfo['goodPrice'];
                                $saveInsertData[$key]['product_vipPrice'] = $goodInfo['goodVipPrice'];
                                $saveInsertData[$key]['commission_rate'] = $goodInfo['newShareScore']['share'] ?? 0;
                                $saveInsertData[$key]['small_goods_img'] = $goodInfo['banner'][0];
                                $saveInsertData[$key]['platform_identity'] = $member['platform_identity'];
                            }
                            //自营商品保存
                            continue;
                        }
                        if (preg_match("/goodsDetailCPS/", $longUrl)) {
                            //有京东,拼多多,唯品会 判断京东拼多多
                            $arr = explode('&', parse_url($longUrl)['query']);
                            foreach ($arr as $k => $v) {
                                $arr = explode('=', $v);
                                $queryData[$arr[0]] = $arr[1];
                            }
                            //status 京东 3; 拼多多 4; 唯品会 6;
                            if (!isset($queryData['status']) || !in_array($queryData['status'], [3, 4])) {
                                continue;
                            }
                            $postUrl = 'https://gateway.yuetao.vip/shop/v/outside/CpsGoodsDetail';
                            //京东
                            if ($queryData['status'] == 3) {
                                $product_type = 5;
                                $params = [];
                                $params['goods_id'] = $queryData['goodsId'];
                                $params['brand'] = 'jd';
                            }
                            //拼多多
                            if ($queryData['status'] == 4) {
                                $product_type = 6;
                                $params = [];
                                $params['goods_id'] = $queryData['goodsId'];
                                $params['brand'] = 'pdd';
                            }
                            $postResult = curlPost($postUrl, $params);
                            if (false == $postResult) {
                                \Log::error("保存素材接口请求异常" . json_encode($params));
                                continue;
                            }
                            $postResult = json_decode($postResult, true);
                            if (isset($postResult['code']) && $postResult['code'] == 200) {
                                $goodInfo = $postResult['data'];
                                $where = [
                                    'product_id' => $goodInfo['goods_id'],
                                    'platform_identity' => $member['platform_identity']
                                ];
                                $sendDataExist = SendInfoModel::getAutoSendData($where);
                                if ($sendDataExist) {
                                    continue;
                                }
                                $saveInsertData[$key]['product_type'] = $product_type;
                                $saveInsertData[$key]['product_id'] = $saveInsertData[$key]['sku_id'] = $goodInfo['goods_id'];
                                $saveInsertData[$key]['product_name'] = $goodInfo['goods_name'];
                                $saveInsertData[$key]['product_price'] = $goodInfo['original_price'];
                                $saveInsertData[$key]['product_vipPrice'] = $goodInfo['current_price'];
                                $saveInsertData[$key]['commission_rate'] = $goodInfo['share_save_price'];
                                $saveInsertData[$key]['small_goods_img'] = $goodInfo['goods_image'][0];
                                $saveInsertData[$key]['platform_identity'] = $member['platform_identity'];
                            }
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
         $result= SendInfoModel::addCommunitySendInfo($add);
        if ($saveInsertData) {
            SendInfoModel::addAutoSendData($saveInsertData);
        }
        if ($result) {
//            $newBindMid = config('community.bind_mid_new');
//            if(in_array($mid,$newBindMid)||$member['platform_identity']!='7323ff1a23f0bf1cda41f690d4089353' ){
                $redisKeyRandom =  config('community.insert_to_redis_random');
                $robotWxAlias =   GroupManageModel::getAllRobotWxAlias($mid,$member['sup_id'],0);
                $wxAliasString=[];
                if (!empty($robotWxAlias)) {
                    foreach ($robotWxAlias as $v) {
                        $wxAliasString[]=  $v->wxid;
                    }
                }
                $rediskeyData = ['mid' => $mid, 'random_str' => $str, 'tag_ids' => !empty($tag_ids) ? $tag_ids : 0,'randomWxids'=>$wxAliasString];
                 $res1 = Redis::rpush($redisKeyRandom, json_encode($rediskeyData));
                $count = Redis::llen($redisKeyRandom);
                $count1 =count(Redis::lrange($redisKeyRandom, 0, -1));
                Log::info('insert_to_redis_random   res '.$redisKeyRandom .$count.$count1.$res1.$result.json_encode($rediskeyData));
//            }
            $logMessage = '新增素材商品';
            UserOperationLogService::insertAdminOperation(
                $mid, 1, json_encode($add), $logMessage, $result
            );
            return ['code' => 200, 'msg' => '添加成功！'];
        } else {
            return ['code' => 400, 'msg' => '添加失败！'];
        }
    }


    /**
     * 添加数据(对外机器人发送素材)
     * @param $param
     * @return array
     */
    public static function addgetSourceMaterialV2($param)
    {

        if(!isset($param['platform_identity'])||empty($param['platform_identity']))
        {
            return ['code' => 400, 'msg' => '平台标识不能为空！'];
        }

        //验证
        $check_input = self::checkInputCommunitySendInfo($param);

        if($check_input){
            return $check_input;
        }
        $mid = !empty($param["mid"]) ? $param["mid"] :0;
        $room_ids = !empty($param["roomids"]) ? $param["roomids"] :'';
        if(empty($mid)){
            return [
                "code" => 400,
                "msg" => '参数错误！',
                "data" => [],
            ];
        }

        //当前时间戳
        $timestamp              =   time();
        $created_at             =   self::formatTimeStamp($timestamp);
        $str  = self::getStr();
        $info = SendInfoModel::getCommunitySendInfo2s(['randomStr'=>$str,'platform_identity'=>$param['platform_identity']]);
        if(!empty($info))
        {
            return ['code' => 400 , 'msg' => '重新点击一下！'];
        }
        $msg_type = $param['user_type'];//1:普通管理员 2:社群管理人员 3专属机器人

        $add=[];
        $tag_ids = "";
        //根据mid  获取wxid
        foreach ($param['data'] as $key=>$value) {
            //这里是CPS活动传输的长链保存为短链
            if ($value['type'] == 7) {
                $value['type'] = 1;
                //use  App\Service\VoucherBag\CouponService;
                $url = !empty($value['shortUrl']) ? $value['shortUrl'] : '';
                //CPS活动也地址 TODO
                $host = 'https://web.yuetao.vip/blank';
                $url = $host . "?robot_send=1&url=" . urlencode($url);

                $value['shortUrl'] = CouponService::getShortUrlByLongUrl($url);
            }
            //type 8 为发送小程序
            if ($value['type'] == 8) {
                //name:小程序名称  userName:小程序原始ID  title:分享标题 pagePath:小程序跳转路径
                $name = $value['name'] ?? '';
                $userName = isset($value['userName']) ? $value['userName'] . '@app' : '';
                $title = $value['title'];
                $pagePath = $value['pagePath'] ?? '';
                //模板下拉选择, 有模板ID数据库查询
                if (isset($value['templateId']) && !empty($value['templateId'])) {
                    $baseXml = SendMiniApp::getMiniAppXMLInfoById($value['templateId']);
                    if (empty($baseXml)) {
                        return [
                            "code" => 400,
                            "msg" => '小程序模板文件不存在',
                            "data" => [],
                        ];
                    }
                    $baseXml = CouponService::querySendMiniAppXml($baseXml, $value);
                } else {
                    $baseXml = file_get_contents(config_path() . '/miniAppBase.xml');
                    $baseXml = str_replace(
                        ['{name}', '{userName}','{title}','{pagePath}'],
                        [$name, $userName, $title, $pagePath],
                        $baseXml
                    );
                }
                $value['content'] = $baseXml;
                //test
            }
            $add[$key]['mid'] = empty($mid) ? $mid : 0;
            $add[$key]['type'] = !empty($value['type']) ? $value['type'] : 0;
            $add[$key]['is_more_product'] = isset($value['isMoreProduct']) ? $value['isMoreProduct'] : 0;
            $add[$key]['cid'] = isset($value['cid']) ? $value['cid'] : 0;
            $add[$key]['link_title'] = isset($value['link_title']) ? $value['link_title'] : '';
            $add[$key]['link_desc'] = isset($value['link_desc']) ? $value['link_desc'] : '';
            $add[$key]['link_img_url'] = isset($value['link_img_url']) ? $value['link_img_url'] : '';
            $add[$key]['coupon_num'] = isset($value['coupon_num']) ? $value['coupon_num'] : '';
            $add[$key]['link_type'] = isset($value['link_type']) ? $value['link_type'] : 4;
            $add[$key]['is_send'] = isset($value['isSend']) ? $value['isSend'] : 0;
            $add[$key]['status'] = isset($value['status']) ? $value['status'] : 0;
            $add[$key]['is_timing_send'] = isset($value['isTimingSend']) ? $value['isTimingSend'] : 0;
            $add[$key]['timing_send_time'] = isset($value['timingSendTime']) ? strtotime($value['timingSendTime']) : 0;
            $add[$key]['short_url'] = isset($value['shortUrl']) ? $value['shortUrl'] : '';
            $add[$key]['content'] = isset($value['content']) ? $value['content'] : '';
            $add[$key]['random_str'] = $str;
            $add[$key]['created_at'] = $created_at;
            $add[$key]['room_ids'] = $room_ids;
            $add[$key]['msg_type'] = $msg_type;
            $add[$key]['exclusive_mid'] = $msg_type==3?$mid:0;
            $add[$key]['tag_ids'] = isset($value['tag_ids']) ? $value['tag_ids'] : 0;
            $add[$key]['typeid'] = isset($value['typeid']) ? $value['typeid'] : 0;
            $add[$key]['platform_identity'] = isset($param['platform_identity']) ? $param['platform_identity'] : '';
            $add[$key]['isMoreTags'] = isset($value['isMoreTags'])?$value['isMoreTags']:0;

            $tag_ids = $add[$key]['tag_ids'];



            if ($value['type'] == 5) {
                $add[$key]['link_img_url'] = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-05-24/22/yuelvhuihbUe9hAV401590330312.jpg";
                self::getSendCouponList($value, $member);
            }
        }
        
        $result= SendInfoModel::addCommunitySendInfo($add);

        if ($result) {
//            $newBindMid = config('community.bind_mid_new');
//            if(in_array($mid,$newBindMid)||$member['platform_identity']!='7323ff1a23f0bf1cda41f690d4089353' ){
                $redisKeyRandom =  config('community.insert_to_redis_random');
                $robotWxAlias =   GroupManageModel::getAllRobotWxAliasV2($param['platform_identity'],$mid,$msg_type);
                $wxAliasString=[];
                if (!empty($robotWxAlias)) {
                    foreach ($robotWxAlias as $v) {
                        $wxAliasString[]=  $v->wxid;
                    }
                }
                $rediskeyData = ['mid' => $mid, 'random_str' => $str, 'tag_ids' => !empty($tag_ids) ? $tag_ids : 0,'randomWxids'=>$wxAliasString];
                $res1 = Redis::rpush($redisKeyRandom, json_encode($rediskeyData));
                $count = Redis::llen($redisKeyRandom);
                $count1 =count(Redis::lrange($redisKeyRandom, 0, -1));
                Log::info('insert_to_redis_random   res '.$redisKeyRandom .$count.$count1.$res1.$result.json_encode($rediskeyData));
//            }
            $logMessage = '新增素材商品';
            UserOperationLogService::insertAdminOperation(
                $mid, 1, json_encode($add), $logMessage, $result
            );
            return ['code' => 200, 'msg' => '添加成功！','random_str'=>$str];
        } else {
            return ['code' => 400, 'msg' => '添加失败！'];
        }
    }


    /**
     * 添加数据(对外机器人发送素材)
     * @param $param
     * @return array
     */
    public static function upCommunitySendInfoV2($param)
    {

        if(!isset($param['platform_identity'])||empty($param['platform_identity']))
        {
            return ['code' => 400, 'msg' => '平台标识不能为空！'];
        }

        //验证
        $check_input = self::checkInputCommunitySendInfo($param);

        if($check_input){
            return $check_input;
        }
        $mid = !empty($param["mid"]) ? $param["mid"] :0;
        $room_ids = !empty($param["roomids"]) ? $param["roomids"] :'';
        if(empty($mid)){
            return [
                "code" => 400,
                "msg" => '参数错误！',
                "data" => [],
            ];
        }

        //当前时间戳
        $timestamp              =   time();
        $updated_at             =   self::formatTimeStamp($timestamp);

        foreach ($param['data'] as $k=>&$item)
        {
            if(!empty($item['id']))
            {
                $info = SendInfoModel::getCommunitySendInfo(['id'=>$item['id'],'platform_identity'=>$param['platform_identity']]);
            }
        }


        $str  = self::getStr();
        $info = SendInfoModel::getCommunitySendInfo2s(['randomStr'=>$str,'platform_identity'=>$param['platform_identity']]);
        if(empty($info)){
            $str  = self::getStr();
            $info1 = SendInfoModel::getCommunitySendInfoStr($str);
            if(!empty($info1))
            {
                return ['code' => 400 , 'msg' => '重新点击一下！'];
            }
        }
        $msg_type = $param['user_type'];//1:普通管理员 2:社群管理人员 3专属机器人
        $addRes=$upRes=0;
        $add=[];
        $tag_ids = "";

        DB::beginTransaction();
        $error=0;
        //根据mid  获取wxid
        foreach ($param['data'] as $key=>$value) {
            //这里是CPS活动传输的长链保存为短链
            if ($value['type'] == 7) {
                $value['type'] = 1;
                //use  App\Service\VoucherBag\CouponService;
                $url = !empty($value['shortUrl']) ? $value['shortUrl'] : '';
                //CPS活动也地址 TODO
                $host = 'https://web.yuetao.vip/blank';
                $url = $host . "?robot_send=1&url=" . urlencode($url);

                $value['shortUrl'] = CouponService::getShortUrlByLongUrl($url);
            }
            //type 8 为发送小程序
            if ($value['type'] == 8) {
                //name:小程序名称  userName:小程序原始ID  title:分享标题 pagePath:小程序跳转路径
                $name = $value['name'] ?? '';
                $userName = isset($value['userName']) ? $value['userName'] . '@app' : '';
                $title = $value['title'];
                $pagePath = $value['pagePath'] ?? '';
                //模板下拉选择, 有模板ID数据库查询
                if (isset($value['templateId']) && !empty($value['templateId'])) {
                    $baseXml = SendMiniApp::getMiniAppXMLInfoById($value['templateId']);
                    if (empty($baseXml)) {
                        return [
                            "code" => 400,
                            "msg" => '小程序模板文件不存在',
                            "data" => [],
                        ];
                    }
                    $baseXml = CouponService::querySendMiniAppXml($baseXml, $value);
                } else {
                    $baseXml = file_get_contents(config_path() . '/miniAppBase.xml');
                    $baseXml = str_replace(
                        ['{name}', '{userName}','{title}','{pagePath}'],
                        [$name, $userName, $title, $pagePath],
                        $baseXml
                    );
                }
                $value['content'] = $baseXml;
                //test
            }

            if(!empty($value['id']))
            {
                $up = [];
                $up['type']      = $value['type'];
                $up['is_send']   = $value['isSend'];
                $up['content']   = $value['content'];
                $up['is_more_product']      = isset($value['isMoreProduct'])?$value['isMoreProduct']:0;
                $up['link_type']   = empty($value['link_type'])?4:$value['link_type'];
                if (isset($value['link_img_url']) && !empty($value['link_img_url'])){
                    $up['link_img_url']   = $value['link_img_url'];
                }
                $up['tag_ids']   = isset($value['tag_ids']) ? $value['tag_ids'] : 0;
                $up['typeid']   = isset($value['typeid']) ? $value['typeid'] : 0;
                $up['short_url'] = isset($value['shortUrl']) ? $value['shortUrl'] : '';
                $up['is_timing_send']    = isset($value['isTimingSend']) ? $value['isTimingSend'] : 0;
                $up['timing_send_time']  = isset($value['timingSendTime']) ? strtotime($value['timingSendTime']) : 0;
                $up['updated_at']= $updated_at;
                $up['msg_type']= $msg_type;
                $up['room_ids']= !empty($room_ids)?$room_ids:0;
                $where = [];
                $where['id']     = $value['id'];
                $upRes  = SendInfoModel::upCommunitySendInfo($where,$up);
                if($upRes === false)
                {
                    $error=1;
                }
            }else{

                $add[$key]['mid'] = empty($mid) ? $mid : 0;
                $add[$key]['type'] = !empty($value['type']) ? $value['type'] : 0;
                $add[$key]['is_more_product'] = isset($value['isMoreProduct']) ? $value['isMoreProduct'] : 0;
                $add[$key]['cid'] = isset($value['cid']) ? $value['cid'] : 0;
                $add[$key]['link_title'] = isset($value['link_title']) ? $value['link_title'] : '';
                $add[$key]['link_desc'] = isset($value['link_desc']) ? $value['link_desc'] : '';
                $add[$key]['link_img_url'] = isset($value['link_img_url']) ? $value['link_img_url'] : '';
                $add[$key]['coupon_num'] = isset($value['coupon_num']) ? $value['coupon_num'] : '';
                $add[$key]['link_type'] = isset($value['link_type']) ? $value['link_type'] : 4;
                $add[$key]['is_send'] = isset($value['isSend']) ? $value['isSend'] : 0;
                $add[$key]['status'] = isset($value['status']) ? $value['status'] : 0;
                $add[$key]['is_timing_send'] = isset($value['isTimingSend']) ? $value['isTimingSend'] : 0;
                $add[$key]['timing_send_time'] = isset($value['timingSendTime']) ? strtotime($value['timingSendTime']) : 0;
                $add[$key]['short_url'] = isset($value['shortUrl']) ? $value['shortUrl'] : '';
                $add[$key]['content'] = isset($value['content']) ? $value['content'] : '';
                $add[$key]['random_str'] = $str;
                $add[$key]['created_at'] = $updated_at;
                $add[$key]['room_ids'] =empty($info->random_str)?$str:$info->random_str ;
                $add[$key]['msg_type'] = $msg_type;
                $add[$key]['exclusive_mid'] = $msg_type==3?$mid:0;
                $add[$key]['tag_ids'] = isset($value['tag_ids']) ? $value['tag_ids'] : 0;
                $add[$key]['typeid'] = isset($value['typeid']) ? $value['typeid'] : 0;
                $add[$key]['platform_identity'] = isset($param['platform_identity']) ? $param['platform_identity'] : '';
                $add[$key]['isMoreTags'] = isset($value['isMoreTags'])?$value['isMoreTags']:0;

                $tag_ids = $add[$key]['tag_ids'];



                if ($value['type'] == 5) {
                    $add[$key]['link_img_url'] = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-05-24/22/yuelvhuihbUe9hAV401590330312.jpg";
                    self::getSendCouponList($value, $member);
                }
            }
        }
        
        if($addRes>0||$upRes>0){
           SendInfoModel::upCommunitySendInfo(['random_str'=>empty($info->random_str)?$str:$info->random_str],['up_flag'=>1,'is_redis'=>0]);
//            $newBindMid = config('community.bind_mid_new');
//            if(in_array($mid,$newBindMid)||$member['platform_identity']!='7323ff1a23f0bf1cda41f690d4089353'){
                $redisKeyRandom =  config('community.insert_to_redis_random');
                $robotWxAlias =   GroupManageModel::getAllRobotWxAliasV2($param['platform_identity'],$mid,$msg_type);
                $wxAliasString=[];
                if (!empty($robotWxAlias)) {
                    foreach ($robotWxAlias as $v) {
                        $wxAliasString[]=  $v->wxid;
                    }
                }
                $rediskeyData = ['mid' => $mid, 'random_str' => $str, 'tag_ids' => !empty($tag_ids) ? $tag_ids : 0,'randomWxids'=>$wxAliasString];
                $res1 = Redis::rpush($redisKeyRandom, json_encode($rediskeyData));
                $count = Redis::llen($redisKeyRandom);
                $count1 =count(Redis::lrange($redisKeyRandom, 0, -1));
                Log::info('insert_to_redis_random   res '.$redisKeyRandom .$count.$count1.$res1.json_encode($rediskeyData));
//            }

        }

        if($error==1)
        {
            DB::rollBack();
            return ['code'=>400,'msg'=>'操作失败'];
        }
        DB::commit();
        $logMessage = '新增素材商品';
        $type = 1;
        if(!empty($value['id']) && $up) {
            $add = $up;
            $logMessage = '修改素材商品';
            $type = 2;
        }
        UserOperationLogService::insertAdminOperation(
            $mid, $type, json_encode($add), $logMessage, $addRes
        );
        return ['code' => 200 , 'msg' => '操作成功！'];
    }


    public static function upCommunitySendInfo($param = [],$member)
    {
        //验证
        $check_input = self::checkInputCommunitySendInfo($param);
        if($check_input){
            return $check_input;
        }
        //当前时间戳
        $timestamp              =   time();
        $updated_at             =   self::formatTimeStamp($timestamp);
        $mid = !empty($param["mid"]) ? $param["mid"] :0;
        $room_ids = !empty($param["roomids"]) ? $param["roomids"] :'';
        if(empty($mid)){
            return ["code" => 400,"msg" => '参数错误！',"data" => [],];
        }

        $msg_type = $member['user_type'];

        $add=[];

        $info='';

        foreach ($param['data'] as $k=>&$item)
        {
            //小程序不做修改
            if ($item['type'] == 8) {
                continue;
            }
            //这里是CPS活动传输的长链保存为短链
            if ($item['type'] == 7) {
                $item['type'] = 1;
                //匹配出URL地址
                preg_match_all("/(https|http):[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/", $item['shortUrl'], $arr);

                if (isset($arr[0][0]) && !empty($arr[0][0])) {

                    $url = $arr[0][0];

                    $urlArr = explode('/', $url);

                    //获取到短链code
                    $shortUrlCode = end($urlArr);

                    //查询短链code对应的长链接
                    $longUrl = CommunityInvitationGroupModel::getLongUrlByShortUrlCodeV2($shortUrlCode);

                    if (!$longUrl) {
                        //use  App\Service\VoucherBag\CouponService;
                        $url = !empty($item['shortUrl']) ? $item['shortUrl'] : '';
                        //CPS活动也地址 TODO
                        $host = 'https://web.yuetao.vip/blank';
                        $url = $host . "?robot_send=1&url=" . urlencode($url);

                        $item['shortUrl'] = CouponService::getShortUrlByLongUrl($url);
                    }
                }
            }
            if(!empty($item['id']))
            {
                $info = SendInfoModel::getCommunitySendInfo(['mid'=>$member['mid'], 'id'=>$item['id'],'sup_id'=>$member['sup_id'],'platform_identity'=>$member['platform_identity']]);
            }
        }
        $addRes=$upRes=0;
        $str='';
        if(empty($info)){
            $str  = self::getStr();
            $info1 = SendInfoModel::getCommunitySendInfoStr($str);
            if(!empty($info1))
            {
                return ['code' => 400 , 'msg' => '重新点击一下！'];
            }
        }


        DB::beginTransaction();
        $error=0;
        if(!empty($info)||(empty($info)&&empty($info1)))
        {
            foreach ($param['data'] as $key=>$value)
            {
                if(!empty($value['id']))
                {
                    $up = [];
                    $up['type']      = $value['type'];
                    $up['is_send']   = $value['isSend'];
                    $up['content']   = $value['content'];
                    $up['is_more_product']      = !empty($value['isMoreProduct'])?$value['isMoreProduct']:0;
                    $up['link_type']   = empty($value['link_type'])?4:$value['link_type'];
                    if (isset($value['link_img_url']) && !empty($value['link_img_url'])){
                        $up['link_img_url']   = $value['link_img_url'];
                    }
                    $up['tag_ids']   = empty($value['tag_ids'])?0:$value['tag_ids'];
                    $up['typeid']   = empty($value['typeid'])?0:$value['typeid'];
                    $up['short_url'] = !empty($value['shortUrl'])?$value['shortUrl']:'';
                    $up['is_timing_send']    = !empty($value['isTimingSend'])?$value['isTimingSend']:0;
                    $up['timing_send_time']  = !empty($value['timingSendTime'])?strtotime($value['timingSendTime']):0;
                    $up['updated_at']= $updated_at;
                    $up['msg_type']= $msg_type;
                    $up['room_ids']= !empty($room_ids)?$room_ids:0;
                    $where = [];
                    $where['id']     = $value['id'];
                    $upRes  = SendInfoModel::upCommunitySendInfo($where,$up);
                    if($upRes === false)
                    {
                        $error=1;
                    }
                }else{
                    $add['mid']      = !empty($mid)?$mid:0;
                    $add['is_send']   = $value['isSend'];
                    $add['is_more_product']      = !empty($value['isMoreProduct'])?$value['isMoreProduct']:0;
                    $add['link_type']   = empty($value['link_type'])?4:$value['link_type'];
                    $add['type']      = $value['type'];
                    $add['status']    = !empty($value['status'])?$value['status']:0;
                    $add['cid']      = !empty($value['cid'])?$value['cid']:0;
                    $add['link_title']      = !empty($value['link_title'])?$value['link_title']:'';
                    $add['link_desc']      = !empty($value['link_desc'])?$value['link_desc']:'';
                    $add['link_img_url']      = !empty($value['link_img_url'])?$value['link_img_url']:'';
                    $add['coupon_num']      = !empty($value['coupon_num'])?$value['coupon_num']:'';                 
                    $add['is_timing_send']    = !empty($value['isTimingSend'])?$value['isTimingSend']:0;
                    $add['tag_ids']    = !empty($value['tag_ids'])?$value['tag_ids']:0;
                    $add['timing_send_time']  = !empty($value['timingSendTime'])?strtotime($value['timingSendTime']):0;
                    $add['short_url'] = !empty($value['shortUrl'])?$value['shortUrl']:'';
                    $add['content']   = $value['content'];
                    $add['random_str']= empty($info->random_str)?$str:$info->random_str ;
                    $add['created_at']= $updated_at;
                    $add['msg_type']= $msg_type;
                    $add['room_ids']= !empty($room_ids)?$room_ids:0;
                    $add['typeid']=!empty($value['typeid'])?$value['typeid']:0;
                    $add['platform_identity']= isset($member['platform_identity'])?$member['platform_identity']:'' ;
                    if($value['type']==5)
                    {
                        $add['link_img_url'] = "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-05-24/22/yuelvhuihbUe9hAV401590330312.jpg";
                        self::getSendCouponList($value,$member);
                    }
                    if($value['type']==8)
                    {
                        $add['msg_type']= 4901;
                    }
                    $addRes = SendInfoModel::addCommunitySendInfo($add);

                    if(!$addRes)
                    {
                        $error=1;
                    }

                }
            }
        }

        if($addRes>0||$upRes>0){
           SendInfoModel::upCommunitySendInfo(['random_str'=>empty($info->random_str)?$str:$info->random_str],['up_flag'=>1,'is_redis'=>0]);
//            $newBindMid = config('community.bind_mid_new');
//            if(in_array($mid,$newBindMid)||$member['platform_identity']!='7323ff1a23f0bf1cda41f690d4089353'){
                $redisKeyRandom =  config('community.insert_to_redis_random');
                $robotWxAlias =   GroupManageModel::getAllRobotWxAlias($mid,$member['sup_id'],0);
                $wxAliasString = [];
                if (!empty($robotWxAlias)) {
                    foreach ($robotWxAlias as $v) {
                        $wxAliasString[]=  $v->wxid;
                    }
                }
                $rediskeyData = ['mid' => $mid, 'random_str' => empty($info->random_str)?$str:$info->random_str, 'tag_ids' =>!empty($value['tag_ids'])?$value['tag_ids'] : 0,'randomWxids'=>$wxAliasString];
                $res1 = Redis::rpush($redisKeyRandom, json_encode($rediskeyData));
                $count = Redis::llen($redisKeyRandom);
                $count1 =count(Redis::lrange($redisKeyRandom, 0, -1));
                Log::info('insert_to_redis_random   res '.$redisKeyRandom .$count.$count1.$res1.json_encode($rediskeyData));
//            }

        }

        if($error==1)
        {
            DB::rollBack();
            return ['code'=>400,'msg'=>'操作失败'];
        }
        DB::commit();
        $logMessage = '新增素材商品';
        $type = 1;
        if(!empty($value['id']) && $up) {
            $add = $up;
            $logMessage = '修改素材商品';
            $type = 2;
        }
        UserOperationLogService::insertAdminOperation(
            $mid, $type, json_encode($add), $logMessage, $addRes
        );
        return ['code' => 200 , 'msg' => '操作成功！'];
    }



    public static function checkInputCommunitySendInfo($param = [])
    {


        if(!isset($param['data'])||empty($param['data']))
        {
            return ['code' => 400, 'msg' => '数据不能为空！'];
        }


        foreach ($param['data'] as $value)
        {
            if(!isset($value['type'])||empty($value['type']))
            {
                return ['code' => 400, 'msg' => '类型不能为空！'];
            }

            if ($value['type'] != 8) {
                if ($value['type'] != 5 && (!isset($value['content']) || empty($value['content']))) {
                    return ['code' => 400, 'msg' => '内容不能为空！'];
                }
            }
        }


    }



    public static function getStr($len=6, $chars=null)
    {
        if (is_null($chars)){
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }
        mt_srand(10000000*(double)microtime());
        for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++){
            $str .= $chars[mt_rand(0, $lc)];
        }
        $info = SendInfoModel::getCommunitySendInfoRandom($str);
        if(!empty($info))
        {
            for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++){
                $str .= $chars[mt_rand(0, $lc)];
            }
            return $str;
        }else{
             return $str;
        }

    }


    public static function delCommunitySendInfo($param = [],$memberInfo)
    {
        if(!isset($param['id'])||empty($param['id']))
        {
            return ['code' => 400, 'msg' => '参数错误！'];
        }
        $mid = $memberInfo['mid'];

        $info   =   SendInfoModel::getCommunitySendInfo(['mid'=>$memberInfo['mid'],'id'=>$param['id'],'sup_id'=>$memberInfo['sup_id'],'platform_identity'=>$memberInfo['platform_identity']]);

        if(empty($info))
        {
            return ['code' => 400, 'msg' => '此数据不存在！'];
        }
        $upData['deleted_at'] = time();
        $upData['status']     = 1;
        $upData['is_redis']     = 0;


//        if(in_array($mid,$manageMid)){
//            $whereData=['id'=>$param['id']];
//        }else{
            $whereData=['id'=>$param['id'],'mid'=>$mid];
//        }
        $res    =   SendInfoModel::upCommunitySendInfo($whereData,$upData);
        if($res)
        {
            $logMessage = '删除素材商品';
            UserOperationLogService::insertAdminOperation(
                $mid, 3, json_encode($upData), $logMessage, $res
            );
            return ['code' => 200 , 'msg' => '删除成功！'];
        }else{
            return ['code' => 400 , 'msg' => '删除失败！'];
        }
    }


    public static function getCommunityInvitationGroupList($param)
    {
        $page    = !empty($param["page"]) ? $param["page"] : 1;
        $display = !empty($param["pageSize"]) ? $param["pageSize"] : 20;

        $param["offset"]  = ($page - 1) * $display;
        $param["display"] = $display;

        $result    = LotteryDrawModel::getCommunityInvitationGroupList($param);

        $count     = LotteryDrawModel::getCommunityInvitationGroupListCount($param);

        return [
            "code" => 200,
            "data" => [
                "total"     => $count,
                "page"      => $page,
                "pageSize"  => $display,
                "list"      => self::formatCommunityInvitationGroupList($result)
            ],
        ];
    }


    public static function formatCommunityInvitationGroupList($data)
    {

        $list = [];

        if($data)
        {
            foreach ($data as $value)
            {
                $memberInfo =   MemberModel::getMemberInfo(['mid'=>$value->mid]);

                $adminMemberInfo =   AdminUserModel::getAdminUserInfo(['admin_user_id'=>$value->examine_mid]);

                if(!empty($memberInfo))
                {
                    if(!empty($memberInfo->nickname))
                    {
                        $nickname=$memberInfo->nickname;
                    }else{
                        if(!empty($memberInfo->truename))
                        {
                            $nickname=$memberInfo->truename;
                        }else{
                            $nickname='';
                        }
                    }

                }else{
                    $nickname='';
                }

                $countInfo = LotteryDrawModel::getCommunityInvitationGroupCountInfo(['mid'=>$value->mid]);

                $list[]=[
                    "id"                    => $value->id,
                    "mid"                   => $value->mid,
                    "mobile"                => $memberInfo->mobile,
                    "jointime"              => date("Y-m-d H:i:s",$memberInfo->jointime),
                    "nickname"              => $nickname,
                    "groupNumber"           => $value->group_number,
                    "images"                => $value->images,
                    "addtime"               => date("Y-m-d H:i:s",$value->addtime),
                    "examineStatus"         => $value->examine_status,
                    "examineStatusName"     => self::getExamineStatus($value->examine_status),
                    "examineMember"         => !empty($adminMemberInfo->staff_name)?$adminMemberInfo->staff_name:'',
                    "examineMid"            => $value->examine_mid,
                    "examineTime"           => !empty($value->examine_time)?date("Y-m-d H:i:s",$value->examine_time):'',
                    "examineDesc"           => $value->examine_desc,
                    "assistantWx"           => $value->assistant_wx,
                    "tutorWx"               => $value->tutor_wx,
                    "roomId"                => $value->room_id,
                    "directPushNum"         => !empty($countInfo)?$countInfo->direct_push_num:0,
                    "intervalPushNum"       => !empty($countInfo)?$countInfo->interval_push_num:0,
                ];
            }
        }

        return $list;
    }

    public static function getCanUseTag($param,$memberInfo)
    {
        $array = [['tag_id'=>0,'tag_name'=>'全部']];
        if ($memberInfo['user_type'] >= 2) {
            $returnData =  SendInfoModel::getMyGroupTagDataByWxid($memberInfo);
        }else{
             $returnData =   SendInfoModel::getMyGroupTagData($memberInfo['mid']);
        }
        return [
            "code" => 200,
            "data" => array_merge($returnData,$array)
        ];
    }


    /**
     * 获取渠道
     * @param $memberInfo
     */
    public static function getChannelData($memberInfo){
        $returnData = '';

        $channel =  SendInfoModel::getChannelDataByPlatform($memberInfo['platform_identity']);
        return [
            "code" => 200,
            "data" => json_decode(json_encode($channel),true)
        ];
    }


    /**
     * 给群主发送优惠券存入redis消息队列
     * @param $data
     * @return array
     */
    public static function getSendCouponList($param,$memberInfo)
    {
        $mid = $memberInfo['mid'];
        $tag_ids = $param['tag_ids'];
        $sendNum = $param['coupon_num'];
        $cid = $param['cid'];
        //根据管理员mid获取群mid列表
        $getMidList = SendInfoModel::getMyGroupOwnMidByWxid($mid,$tag_ids,$memberInfo['sup_id']);

        $midCount = count($getMidList);

        if($midCount<1)
        {
            return [
                "code" => 400,
                "msg" => "没有绑定群主，无法发送微信号"
            ];                                                                                                                                                               
        }

        $couponInfo = CommunityInvitationGroupModel::getCouponInfo(array("id"=>$cid));

        if(empty($couponInfo))
        {
            return [
                "code" => 400,
                "msg" => "优惠券不存在"
            ];              
        }

        //获取优惠券数量
        $sendCouponNum =$midCount*$sendNum;

        if($couponInfo->totalnumber<$sendCouponNum)
        {
            return [
                "code" => 400,
                "msg" => "优惠券数量不足"
            ];            
        }


        $redisKey = "send_coupon_to_group_own";

        foreach ($getMidList as $key => $value) {

            $rediskey=['mid'=>$value->mid,'sendNum'=>$sendNum,'cid'=>$cid,'adminMid'=>$mid];

            Redis::rpush($redisKey,json_encode($rediskey));

            Log::info('insert redis of groupOwn coupon' .json_encode($rediskey));
        }


        CommunityInvitationGroupModel::updateCouponTotalNum($cid, $sendCouponNum);
    }

    /**
     * 从消息队列中取出信息给群主发送优惠券
     * @param $data
     * @return array
     */
    public static function sendCouponToGroupOwn()
    {
        set_time_limit(-1);
        @ini_set('memory_limit','512M');

        $redisKey = "send_coupon_to_group_own";

        $redisCount = Redis::llen($redisKey);

        if($redisCount>0)
        {
            $data =json_decode(Redis::lpop($redisKey),true);

            $cid    = $data['cid'];

            $couponInfo = CommunityInvitationGroupModel::getCouponInfo(array("id"=>$cid));

            $sendNum = $data['sendNum'];

            Log::info('send coupon to groupOwn from redis' .json_encode($data));

            if ($couponInfo->isnever == 1) {
                $startTime = $couponInfo->starttime;
                $endTime   = $couponInfo->endtime;
            } else if($couponInfo->isnever == 2) {
                $startTime = strtotime(Date('Y-m-d'));
                $endTime   = $startTime + $couponInfo->antedate*24*60*60;
            }


            $coupon = [];
            $time   = time();
 
            for ($i = 0; $i < $sendNum; $i++) {
                $coupon[] = [
                    "mid"        => $data['mid'],
                    "cid"        => $cid,
                    "totalnum"   => 1,
                    "usenum"     => 0,
                    "addtime"    => $time,
                    "is_give"    => 1,
                    "givetime"   => $time,
                    "start_date" => $startTime,
                    "end_dates"  => $endTime,
                    "sourse"     => 1
                ];
            }

            try{

                $coupon_reload = [

                    'mid'       =>$data['mid'],
                    'send_num'  =>$sendNum,
                    'cid'       =>$cid,
                    'admin_mid' =>$data['adminMid']
                ];

                DB::transaction(function () use($coupon,$coupon_reload){
                    CommunityInvitationGroupModel::insertCoupon($coupon);
                    CommunityInvitationGroupModel::insertCouponReload($coupon_reload);
                });

            }catch (Exception $exception) {

                Log::info('send coupon to groupOwn from redis fail' .json_encode($data));
            }


            self::sendCouponToGroupOwn();
        }
    }

    /**
     * 文本域  文本与url区分
     * @param $memberInfo
     * @param $param
     */
    public static function  getTextUrlSeparate($memberInfo,$param)
    {

        if(empty($param['content'])){
            return [
                "code" => 400,
                "msg" => "内容不能为空"
            ];
        }else{
             $resUrlData = self::get_all_url($param['content']);
            $data=[];
             if(is_array($resUrlData)){
                 foreach ($resUrlData as  $key=>$v){
                     $arr=$arr1=[];
                     $arr= explode($v,$param['content']);
                     if(count($resUrlData)>1){
                         if(empty($arr[1])){
                             $arr1= explode($resUrlData[$key-1],$arr[0]);
                             $content =$arr1[1];
                         }else{
                             $resUrlData1 = self::get_all_url($arr[0]);
                             if(!is_array($resUrlData1)){
                                 $content =$arr[0];
                             }else{
                                 $arr1= explode($resUrlData1[$key-1],$arr[0]);
                                 $content =$arr1[1];
                             }
                         }

                     }else{
                         $content = implode('',$arr);
                     }
                     $url =$v;
                     $data[] = ['content'=>$content,'url'=>$url];
                 }
             }else{
               $data[] = ['content'=>$resUrlData,'url'=>''];
             }
            return [
                "code" => 200,
                "msg" => "成功",
                "data" => $data,
            ];

        }


    }

   public static function get_all_url($str){
       //https正则匹配
       preg_match_all("/https:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/",$str,$match);
       if(!empty($match)){
           $urlData = $match[0];
       }
       //http匹配
       preg_match_all("/http:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/",$str,$match1);
       if(!empty($match1)){
           $urlData1 = $match1[0];
       }
       if(empty($urlData)&&empty($urlData1)){
           return  $str;
       }if(empty($urlData)&&!empty($urlData1)){
           return  $urlData1;
       }if(!empty($urlData)&&empty($urlData1)){
           return  $urlData;
       }else{
          return  array_merge($urlData,$urlData1);
       }
   }

}