<?php
namespace App\Service\Solr;
/**
 * 数据同步更新索引库增删改
 *
 * Version  :  3.0
 * Create by:  王玉彬<wangyubin@yuelvhui.com>
 * Copyright:  Copright (c) 悦旅汇, www.yuelvehui.com
 * Created on: 2019/3/26 15:00
 */

use Illuminate\Support\Facades\Log;

class SolrOperationService {

    protected $index = "localhost";

    // solr对象
    protected $client = NULL;

    // 初始索引操作类
    public function __construct()
    {

        $config = config('database.mall.endpoint');

        $this->client = new \SolrClient($config[$this->index]);
    }

    // 根据ID删除多条
    public function deleteIdsIndex($ids=[]) {
        $this->client->deleteByQuery($ids);
        $this->client->commit();
    }

    // 根据字段删除
    public function deleteByQuery($field="*", $value="*") {

        $this->client->deleteByQuery("{$field}:{$value}");
        $this->client->commit();
    }

    // 新增一条或更新一条索引数据
    public function createIndex($data) {
        $config_ext = config('database.mall.endpoint');
        $config = $config_ext[$this->index];
        try{
            $uri = "http://{$config['hostname']}:{$config['port']}{$config['path']}/get?ids={$data['id']}&wt={$config['wt']}";
            $serializedResult = json_decode(file_get_contents($uri), true);

            // 数据存在执行更新操作
            if($serializedResult['response']['docs']) {

                $doc = new \SolrInputDocument();
                foreach ($serializedResult['response']['docs'][0] as $index => $item) {

                    if(array_key_exists($index, $data)) {
                        $doc->addField($index, $data[$index]);
                    } else {
                        $doc->addField($index, $item);
                    }
                }
                $response = $this->client->addDocument($doc);
                $this->client->commit();
            }
            // 执行新增操作
            else {

                $doc = new \SolrInputDocument();
                foreach ($data as $index => $item) {
                    $doc->addField($index, $item);
                }
                $response = $this->client->addDocument($doc);
                $this->client->commit();
            }

        }catch (\Exception $exception) {
            Log::error('更新索引异常', [$exception]);
        }
    }
}
 
