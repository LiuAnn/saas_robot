<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/11/29
 * Time: 上午11:43
 * Describe : 同步群服务
 */

namespace App\Service\Statistics;

use App\Models\BuyInfo;
use App\Models\SyncGroup;
use Illuminate\Support\Facades\DB;

class GroupService
{
    protected static $page = 1;
    protected static $perPage = 10;

    /**
     * 获取群统计
     *
     * @param $param
     * @return array
     */
    public static function getGroup($param)
    {
        $offset = ($param['page'] - 1) * $param['pageSize'];
        // Field
        $field = ['group_id', 'group_name', 'group_tag_id', 'robot_wx_name', 'order_number', 'order_amount',
            'group_member_count', 'group_commission', 'group_male_proportion', 'group_female_proportion',
            'user_name', 'group_is_delete'];

        $data = SyncGroup::select($field)
            ->where('group_is_delete', 0)
            ->when(
                $param['platform_identity'],
                function ($query) use ($param) {
                    return $query->where('channel_identity', '=', $param['platform_identity']);
                }
            )
            ->when(
                $param['platform'],
                function ($query) use ($param) {
                    return $query->where('robot_platform', '=', $param['platform']);
                }
            )
            ->when(
                $param['order_number'],
                function ($query) use ($param) {
                    return $query->orderBy('order_number', 'DESC');
                }
            )
            ->when(
                $param['order_amount'],
                function ($query) use ($param) {
                    return $query->orderBy('order_amount', 'DESC');
                }
            )
            ->when(
                $param['beginTime'],
                function ($query) use ($param) {
                    return $query->whereBetween('group_created_at', [date('Y-m-d 00:00:00', $param['beginTime']), date('Y-m-d 00:00:00', $param['endTime'])]);
                }
            )
            ->withCount(['getTagInfo as group_tag_name' => function ($query) {
                $query->select('tag_name');
            }])
            ->paginate($param['pageSize'])
            ->toArray();

        if (empty($data)) {
            return [
                'page' => 1,
                'pageSize' => 10,
                'total' => 0,
                'list' => [],
            ];
        }
        foreach ($data['data'] as $key => $val) {
            $offset++;
            $data['data'][$key]['id'] = $offset;
            $data['data'][$key]['group_tag_name'] = $val['group_tag_name'] ?? '无';
        }
        return [
            'page' => (int)$param['page'],
            'pageSize' => (int)$param['pageSize'],
            'total' => $data['total'],
            'list' => $data['data'],

        ];
    }

    /**
     * 获取群主统计
     *
     * @param $param
     * @return array
     */
    public static function getLeader($param)
    {
        $offset = ($param['page'] - 1) * $param['pageSize'];
        // Field
        $field = ['group_mid', 'robot_id', 'robot_mid', 'user_name', 'group_mobile', 'group_robot_status', 'robot_platform', 'robot.order_by'];

        $data = SyncGroup::select($field)
            ->leftJoin('white_list_wx as robot', function ($query) {
                $query->on('robot.id', '=', 'robot_id');
            })
            ->selectRaw(DB::raw('count(*) as group_count'))
            ->selectRaw(DB::raw('group_concat(distinct(order_by)) as robot_id'))
            ->selectRaw(DB::raw('sum(order_amount) as order_amount'))
            ->selectRaw(DB::raw('sum(order_number) as order_number'))
            ->when(
                $param['platform_identity'],
                function ($query) use ($param) {
                    return $query->where('channel_identity', '=', $param['platform_identity']);
                }
            )
            ->when(
                $param['platform'],
                function ($query) use ($param) {
                    return $query->where('robot_platform', '=', $param['platform']);
                }
            )
            ->when(
                $param['phone'],
                function ($query) use ($param) {
                    return $query->where('group_mobile', '=', $param['phone']);
                }
            )
            ->when(
                $param['group_count'],
                function ($query) use ($param) {
                    return $query->orderBy('group_count', 'DESC');
                }
            )
            ->when(
                $param['order_amount'],
                function ($query) use ($param) {
                    return $query->orderBy('order_amount', 'DESC');
                }
            )
            ->when(
                $param['order_number'],
                function ($query) use ($param) {
                    return $query->orderBy('order_number', 'DESC');
                }
            )
            ->where('group_is_delete', 0)
            ->where('group_mid', '<>', 0)
            ->groupBy('group_mid')
            ->paginate($param['pageSize'])
            ->toArray();

        if (empty($data)) {
            return [
                'page' => 1,
                'pageSize' => 10,
                'total' => 0,
                'list' => [],
            ];
        }
        foreach ($data['data'] as $key => $val) {
            $offset++;
            $data['data'][$key]['id'] = $offset;
            // GET 群主社群群数量
            $limit_group_count = BuyInfo::where('mid', $val['robot_mid'])
                ->where('deleted_at', 0)
                ->where('duetime_at', '<', time())
                ->first([DB::raw('sum(service_number) as limit_group_count')]);
            $data['data'][$key]['limit_group_count'] = $limit_group_count->limit_group_count ?? 0;
            $data['data'][$key]['platform'] = SyncGroup::$robot_platform[$val['robot_platform']];
        }
        return [
            'page' => (int)$param['page'],
            'pageSize' => (int)$param['pageSize'],
            'total' => $data['total'],
            'list' => $data['data'],

        ];
    }
}