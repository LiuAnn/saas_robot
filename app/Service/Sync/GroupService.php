<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/11/29
 * Time: 上午11:43
 * Describe : 同步群服务
 */

namespace App\Service\Sync;

use App\Models\GoodsBrowse;
use App\Models\RoomActivation;
use App\Models\User;
use App\Models\MemberInfoByAppID;
use App\Models\MemberWeixinInfo;
use App\Models\SendReload;
use App\Models\SendRoomReload;
use App\Models\SyncGroup;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class GroupService
{
    //Redis
    protected static $robot_sync_group_redis = 'robot:sync:group:list:{}';
    # 最新删除的机器人群日期
    protected static $robot_sync_group_status_deleted_at = 'robot:sync:group:status:deleted:at';
    protected static $offset = 0;
    protected static $size = 200;
    protected static $fields = [
        'group.id as group_id', 'group.name as group_name', 'group.room_wxid as group_wxid', 'group.mid as group_mid', 'group.robot_status as group_robot_status', 'group.tag_id as group_tag_id', 'group.robot_status as group_robot_status', 'group.member_count as group_member_count', 'group.nick_name as group_master_name', 'group.mobile as group_mobile', 'group.created_at as group_created_at', 'group.is_delete as group_is_delete',
        'robot.id as robot_id', 'robot.wxid as robot_wxid', 'robot.bind_mid as robot_bind_mid', 'robot.mid as robot_mid', 'robot.wxid_type as robot_wxid_type', 'robot.wx_name as robot_wx_name', 'robot.platform as robot_platform', 'robot.platform_identity as robot_platform_identity', 'robot.robot_serial_no', 'robot.enterprise_robot_serial_no',
    ];


    public static function setSyncByGroup()
    {
        // SET LOG
        $monolog = Log::getMonolog();
        $monolog->popHandler();
        Log::useFiles(storage_path('logs/sync/group.log'));
        Log::info('同步开始');
        $start_time = microtime(true);
        // GET 群总数
        $total = MemberInfoByAppID::where('is_delete', 0)->count();
        // 余次
        $remainder = (int)ceil($total / self::$size);
        $offset = self::$offset;
        // 分批同步
        for ($i = 1; $i <= $remainder; $i++) {
            // GET LIST
            $group_list = MemberInfoByAppID::from('member_info_by_appid as group')
                ->select(self::$fields)
                ->leftJoin('white_list_wx as robot', function ($robot_join) {
                    $robot_join->on('robot.wxid', '=', 'group.wxid')
                        ->on('robot.enterprise_robot_serial_no', '=', 'group.enterprise_robot_serial_no');
                })
                ->leftJoin('mall_order_goods as order', function ($order_join) {
                    $order_join->on('order.room_id', '=', 'group.id')
                        ->where('order.pay_status', 1)
                        ->where('order.room_id', '>', 0);
                })
                ->withCount('getCountOrderGoods as order_number')
                ->withCount(['getSumActualPrice as order_amount' => function ($order_info) {
                    $order_info->select(DB::raw('sum(actual_price) as order_amount'));
                }])
                ->where('group.is_delete', 0)
                ->groupBy('group.id')
                ->limit(self::$size)
                ->offset($offset)
                ->get()
                ->toArray();
            if (empty($group_list)) {
                continue;
            }

            $handle_data = [];
            foreach ($group_list as $key => $val) {
                // GET 发送素材总数
                $group_send_reload = SendReload::where('wxid', $val['robot_wxid'])->count();
                // GET 发送群总数
                $group_message_number = SendRoomReload::where('room_id', $val['group_id'])->count();
                // SET 男女比例
                if (empty($val['group_member_count'])) {
                    $group_male_proportion = 0;
                    $group_female_proportion = 0;
                } else {
                    // GET 群人数男女比例
                    $group_member_info = MemberWeixinInfo::where('romm_id', $val['group_id'])
                        ->first([
                            DB::raw('count(*) as member_count'),
                            DB::raw('round(sum(case when sex =1 then 1 else 0 end ) / count(*) * 100,0) as male'),
                            DB::raw('round(sum(case when sex =2 then 1 else 0 end ) / count(*) * 100,0) as female')
                        ])->toArray();
                    $group_member_info = $group_member_info ?? [];
                    if ($val['group_member_count'] < $group_member_info['member_count']) {
                        $val['group_member_count'] = $group_member_info['member_count'];
                    }
                    $group_male_proportion = $group_member_info['male'] ?? 0;
                    $group_female_proportion = $group_member_info['female'] ?? 0;
                }

                // 有绑定ID按绑定ID取，没有则按MID取
                if ($val['robot_bind_mid'] > 0) {
                    $mid = $val['robot_bind_mid'];
                } else {
                    $mid = $val['robot_mid'];
                }
                // GET 管理员信息 && 平台信息
                $user_info = User::from('user as user')
                    ->select('user.id', 'user.name', 'user.mobile', 'user.platform_identity', 'channel.id as channel_id', 'channel.channel_name')
                    ->leftJoin('channel_data as channel', 'channel.platform_identity', '=', 'user.platform_identity')
                    ->where('mid', $mid)
                    ->first();
                // GET 群内用户商品点击率
                $browse_goods_number = RoomActivation::where('romm_id', $val['group_id'])->count();
                // GET 群内用户浏览商品总人数
                $browse_user_number = GoodsBrowse::where('romm_id', $val['group_id'])->count();
                // GET 复购率
                $sql = "select count(*) as count from (select count(*) from sline_community_mall_order_goods where member_id in (select member_id from (select member_id from sline_community_mall_order_goods where room_id = ? group by member_id having count(member_id) > 1) b )  group by member_id) as a;";
                $repurchase_rate = DB::selectOne($sql, [$val['group_id']]);

                $handle_data[$key]['group_id'] = $val['group_id'];
                $handle_data[$key]['group_name'] = $val['group_name'] ?? '';
                $handle_data[$key]['group_wxid'] = $val['group_wxid'] ?? '';
                $handle_data[$key]['group_tag_id'] = $val['group_tag_id'] ?? 0;
                $handle_data[$key]['group_tag_name'] = '';
                $handle_data[$key]['group_robot_status'] = $val['group_robot_status'] ?? 0;
                $handle_data[$key]['group_mid'] = $val['group_mid'] ?? 0;
                $handle_data[$key]['group_mobile'] = $val['group_mobile'] ?? 0;
                $handle_data[$key]['group_master_name'] = $val['group_master_name'] ?? '';
                $handle_data[$key]['group_is_delete'] = $val['group_is_delete'] ?? '';
                $handle_data[$key]['group_created_at'] = $val['group_created_at'] ?? '';
                $handle_data[$key]['group_member_count'] = $val['group_member_count'];
                $handle_data[$key]['robot_id'] = $val['robot_id'] ?? 0;
                $handle_data[$key]['robot_mid'] = $val['robot_mid'] ?? 0;
                $handle_data[$key]['robot_bind_mid'] = $val['robot_bind_mid'] ?? '';
                $handle_data[$key]['robot_wxid'] = $val['robot_wxid'] ?? '';
                $handle_data[$key]['robot_wx_name'] = $val['robot_wx_name'] ?? '';
                $handle_data[$key]['robot_wxid_type'] = $val['robot_wxid_type'] ?? '';
                $handle_data[$key]['robot_platform'] = $val['robot_platform'] ?? '';
                $handle_data[$key]['order_number'] = $val['order_number'] ?? '';
                $handle_data[$key]['order_amount'] = round($val['order_amount'] / 100, 2) ?? 0.00;
                $handle_data[$key]['robot_serial_no'] = $val['robot_serial_no'] ?? '';
                $handle_data[$key]['enterprise_robot_serial_no'] = $val['enterprise_robot_serial_no'] ?? '';
                $handle_data[$key]['group_commission'] = 0.00;
                $handle_data[$key]['group_send_reload'] = $group_send_reload ?? '';
                $handle_data[$key]['group_message_number'] = $group_message_number ?? '';
                $handle_data[$key]['group_male_proportion'] = $group_male_proportion;
                $handle_data[$key]['group_female_proportion'] = $group_female_proportion;
                $handle_data[$key]['user_id'] = $user_info->id ?? 0;
                $handle_data[$key]['user_name'] = $user_info->name ?? '';
                $handle_data[$key]['user_mobile'] = $user_info->mobile ?? '';
                $handle_data[$key]['channel_id'] = $user_info->channel_id ?? 0;
                $handle_data[$key]['channel_name'] = $user_info->channel_name ?? '';
                $handle_data[$key]['channel_identity'] = !isset($user_info->platform_identity) ? ($val['robot_platform_identity'] ?? '') : $user_info->platform_identity;
                $handle_data[$key]['browse_goods_number'] = $browse_goods_number ?? 0;
                $handle_data[$key]['browse_user_number'] = $browse_user_number ?? 0;
                $handle_data[$key]['first_order_number'] = 0;
                $handle_data[$key]['repurchase_rate'] = $handle_data[$key]['order_number'] > 0 ? round($repurchase_rate->count / $handle_data[$key]['order_number'] * 100, 0) : 0;

                // SET
                $sync_group = SyncGroup::where('group_id', $val['group_id'])->pluck('group_id')->first();
                if (!empty($sync_group)) {
                    SyncGroup::where('group_id', $val['group_id'])->update($handle_data[$key]);
                    unset($handle_data[$key]);
                }
            }

            // ADD
            $insert_result = SyncGroup::insert($handle_data);
            if (!$insert_result) {
                Log::error('同步失败');
            }
            $offset = $i * self::$size;
            echo '第' . $offset . '条数据完成同步' . "\n";
        }

        $end_time = microtime(true);
        $time = $end_time - $start_time;
        echo "本次同步用时" . $time;
        Log::info('本次同步' . $total . '条记录,用时：' . $time);
        Log::info('同步结束');
    }


    /**
     * 同步单条数据
     *
     * @param $id 群ID
     * @return string[]
     */
    public static function set($id)
    {
        // 同步一条数据：用于紧急情况、或当条数据需要更新、或同步数据因为外部原因数据不准确时
        return ['code' => 200, 'message' => 'success'];
    }


    /**
     * 获取群信息
     *
     * @param array $where
     * @param int $offset
     * @param int $size
     * @return mixed
     */
    public static function getGroup($where = [], $offset = 0, $size = 10)
    {
        $group_list = MemberInfoByAppID::from('member_info_by_appid as group')
            ->select(self::$fields)
            ->leftJoin('white_list_wx as robot', function ($robot_join) {
                $robot_join->on('robot.wxid', '=', 'group.wxid')
                    ->on('robot.enterprise_robot_serial_no', '=', 'group.enterprise_robot_serial_no');
            })
            ->leftJoin('mall_order_goods as order', function ($order_join) {
                $order_join->on('order.room_id', '=', 'group.id')
                    ->where('order.pay_status', 1)
                    ->where('order.room_id', '>', 0);
            })
            ->withCount('getCountOrderGoods as order_number')
            ->withCount(['getSumActualPrice as order_amount' => function ($order_info) {
                $order_info->select(DB::raw('sum(actual_price) as order_amount'));
            }])
            ->where('group.is_delete', 0)
            ->where($where)
            ->groupBy('group.id')
            ->limit($size)
            ->offset($offset)
            ->get()
            ->toArray();

        return $group_list;
    }

    public static function syncGroupStatus()
    {
        # SET LOG
        $monolog = Log::getMonolog();
        $monolog->popHandler();
        Log::useFiles(storage_path('logs/sync/group.log'));
        Log::info('同步删除的群开始');
        $start_time = microtime(true);
        $date = '';
        # GET 最后一次删除的群时间
        $last_one_sync_deleted_at = Redis::get(self::$robot_sync_group_status_deleted_at);
        if (empty($last_one_sync_deleted_at)) {
            Log::info('我来到了全量数据同步');
            # GET 群总数
            $total = MemberInfoByAppID::where('is_delete', 1)->count();
            # 余次
            $remainder = (int)ceil($total / self::$size);
            $offset = self::$offset;
            // 分批同步
            for ($i = 1; $i <= $remainder; $i++) {
                $group_list = MemberInfoByAppID::select('id', 'name', 'deleted_at')
                    ->where('is_delete', 1)
                    ->limit(self::$size)
                    ->offset($offset)
                    ->orderBy('deleted_at', 'ASC')
                    ->get()
                    ->toArray();
                if (empty($group_list)) {
                    continue;
                }
                foreach ($group_list as $key => $val) {
                    $sync_info = SyncGroup::select('group_id', 'group_name')->where('group_id', $val['id'])->first();
                    if (empty($sync_info)) {
                        continue;
                    }
                    $date = $val['deleted_at'] == '0000-00-00 00:00:00' ? date('Y-m-d') : strtotime($val['deleted_at']);
                    SyncGroup::where('group_id', $val['id'])->update([
                        'group_is_delete' => 1,
                        'deleted_at' => $date
                    ]);
                }
                $offset = $i * self::$size;
                echo '第' . $offset . '条群状态数据同步完成' . "\n";
            }
        } else {
            Log::info('我来到来增量数据同步');
            # GET 群总数
            $total = MemberInfoByAppID::where('is_delete', 1)
                ->where('deleted_at', '>', date('Y-m-d H:i:s', $last_one_sync_deleted_at))
                ->count();
            $group_list = MemberInfoByAppID::select('id', 'nick_name', 'deleted_at')
                ->where('is_delete', 1)
                ->where('deleted_at', '>', date('Y-m-d H:i:s', $last_one_sync_deleted_at))
                ->orderBy('deleted_at', 'ASC')
                ->get()
                ->toArray();
            if (empty($group_list)) {
                $date = $last_one_sync_deleted_at;
                Log::info('本次没有需要同步的群状态');
            } else {
                foreach ($group_list as $key => $val) {
                    $sync_info = SyncGroup::select('group_id', 'group_name')->where('group_id', $val['id'])->first();
                    if (empty($sync_info)) {
                        continue;
                    }
                    $date = strtotime($val['deleted_at']);
                    SyncGroup::where('group_id', $val['id'])->update([
                        'group_is_delete' => 1,
                        'deleted_at' => $date
                    ]);
                }
            }
        }
        # 同步完成设置最后一个删除时间到Redis
        Redis::set(self::$robot_sync_group_status_deleted_at, $date);
        $end_time = microtime(true);
        $time = $end_time - $start_time;
        echo "本次同步状态用时" . $time;
        Log::info('本次同步状态' . $total . '条记录,用时：' . $time);
        Log::info('同步结束奥利给');
    }
}