<?php
namespace App\Service\Sign;

use App\Model\Integral\GroupIntegralConfModel;
use App\Model\Integral\GroupIntegralModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class GroupSignService{
    /**
     * @param $signInsertData  //待插入数据
     * @param array $robotInfo  //白名单机器人信息
     * @return array
     */
    public static function insertGroupSignRecord($signInsertData, $robotInfo = []) {
        $insertData['continuous_num'] = 1;
        $insertData['integral'] = 0;
        $insertData['room_id'] = $signInsertData['room_id'];
        $wxid_from = $insertData['member_wxid'] = $signInsertData['member_wxid'];
        $insertData['platform_identity'] = $signInsertData['platform_identity'];
        $room_wxid = $signInsertData['room_wxid'] ?? '';
        $msgData = [];
        $msgData['room_wxid'] = $room_wxid;
        $msgData['wxid_from'] = $wxid_from;
        //获取签到配置积分
        $roomWhere = [
            'room_id' => $signInsertData['room_id']
        ];
        $signConfigInfo = GroupIntegralConfModel::getSignConfigInfo($roomWhere);
        if (empty($signConfigInfo)) {
            //如果为空 取管理员配置
            $memberWhere = [
                'member_id' => $robotInfo['mid'],
                'platform_identity' => $robotInfo['platform_identity'],
            ];
            $signConfigInfo = GroupIntegralConfModel::getSignConfigInfo($memberWhere);
            if (empty($signConfigInfo)) {
                return [];
            }
        }
        $sign_amount = $signConfigInfo['sign_integral'];
        $keyword = $signConfigInfo['keyword'];
        //签到关键字不与配置相同则不做记录
        if ($signInsertData['report'] != $keyword) {
            return [];
            $msgData['msg'] = '该群目前没有开启签到功能!';
            return self::getPlatFormMsg($msgData, $robotInfo['platform']);
        }
        $getIntegralWhere = [];
        $getIntegralWhere['romm_id'] = $insertData['room_id'];
        $getIntegralWhere['wxid'] = $insertData['member_wxid'];
        $integral = GroupIntegralModel::getGroupMemberIntegral($getIntegralWhere);
        $insertData['integral'] = $sign_amount;
        //判断昨天是否签到,获取昨天的连续签到次数
        $yesterdayWhere = [];
        $yesterdayWhere['room_id'] = $signInsertData['room_id'];
        $yesterdayWhere['member_wxid'] = $signInsertData['member_wxid'];
        $yesterdayData = GroupIntegralModel::getYesterdayGroupSignRecord($yesterdayWhere);
        if ($yesterdayData) {
            $insertData['continuous_num'] = $yesterdayData['continuous_num'] + 1;
        }
        //判断今天是否已经签到
        $existWhere = [];
        $existWhere['room_id'] = $signInsertData['room_id'];
        $existWhere['member_wxid'] = $signInsertData['member_wxid'];
        $exist = GroupIntegralModel::ifExistGroupSignRecord($existWhere);
        if ($exist) {
            $msgData['msg'] = "今天您已签到,\n目前剩余积分:" . $integral;
            return self::getPlatFormMsg($msgData, $robotInfo['platform']);
        }
        $updateStatus = self::updateGroupSignRecord($insertData, $sign_amount);
        if ($updateStatus) {
            $totalIntegral = $integral + $sign_amount;
            $msgData['msg'] = "您连续签到{$insertData['continuous_num']}次";
            $msgData['msg'] .= "\n本次赠送积分:{$sign_amount}";
            $msgData['msg'] .= "\n目前剩余积分:{$totalIntegral}";
            return self::getPlatFormMsg($msgData, $robotInfo['platform']);
        }
        return [];
    }


    /**
     * @param $inviteInsertData  //待插入数据
     * @param array $robotInfo  //白名单机器人信息
     * @return array
     */
    public static function insertGroupInviteRecord($inviteInsertData, $robotInfo = []) {
        $insertData['continuous_num'] = 1;
        //积分变更类型1:签到2:邀请成员3:管理员操作
        $insertData['type'] = 2;
        $insertData['integral'] = 0;
        $insertData['room_id'] = $inviteInsertData['room_id'];
        $wxid_from = $insertData['member_wxid'] = $inviteInsertData['inviter'] ?? '';
        $insertData['platform_identity'] = $inviteInsertData['platform_identity'];
        $room_wxid = $inviteInsertData['room_wxid'] ?? '';
        $msgData = [];
        $msgData['room_wxid'] = $room_wxid;
        $msgData['wxid_from'] = $wxid_from;
        //获取签到配置积分
        $roomWhere = [
            'room_id' => $inviteInsertData['room_id']
        ];
        $signConfigInfo = GroupIntegralConfModel::getSignConfigInfo($roomWhere);
        if (empty($signConfigInfo)) {
            //如果为空 取管理员配置
            $memberWhere = [
                'member_id' => $robotInfo['mid'],
                'platform_identity' => $robotInfo['platform_identity'],
            ];
            $signConfigInfo = GroupIntegralConfModel::getSignConfigInfo($memberWhere);
            if (empty($signConfigInfo)) {
                return [];
            }
        }
        if (empty($signConfigInfo['invite_integral'])) {
            return [];
        }
        $invite_integral = $signConfigInfo['invite_integral'];
        //flag == 1 是进群, 0为退群
        if ($inviteInsertData['flag'] == 0) {
            $returnMsg = "您邀请的好友在一天内退出群聊";
            $returnMsg .= "\n扣除积分:{$invite_integral}";
            $invite_integral = 0 - $invite_integral;
        } else {
            $returnMsg = "您邀请的好友进群";
            $returnMsg .= "\n本次赠送积分:{$invite_integral}";
        }
        $getIntegralWhere = [];
        $getIntegralWhere['romm_id'] = $insertData['room_id'];
        $getIntegralWhere['wxid'] = $insertData['member_wxid'];
        $integral = GroupIntegralModel::getGroupMemberIntegral($getIntegralWhere);
        $insertData['integral'] = $invite_integral;
        //获取本群邀请总人数
        $updateStatus = self::updateGroupSignRecord($insertData, $invite_integral);
        if ($updateStatus) {
            $totalIntegral = $integral + $invite_integral;
            $msgData['msg'] = $returnMsg;
            $msgData['msg'] .= "\n目前剩余积分:{$totalIntegral}";
            return self::getPlatFormMsg($msgData, $robotInfo['platform']);
        }
        return [];
    }



    public static function updateGroupSignRecord($insertData, $sign_amount) {
        DB::beginTransaction();
        try {
            //签到积分保存
            $recordInsert = GroupIntegralModel::insertGroupSignRecord($insertData);
            if (!$recordInsert) {
                DB::rollBack();
            }
            //用户账户变更
            $updateWhere = [];
            $updateWhere['romm_id'] = $insertData['room_id'];
            $updateWhere['wxid'] = $insertData['member_wxid'];
            $updateResult = GroupIntegralModel::updateGroupMemberIntegral($updateWhere, $sign_amount);
            if ($updateResult === 0) {
                Log::info('群成员不存在--' . json_encode($updateWhere));
            }
            if ($updateResult === false) {
                DB::rollBack();
            }
            DB::commit();
            return true;
        } catch (Exception $exception) {
            DB::rollback();
            return false;
        }
    }

    public static function getPlatFormMsg($msgData, $platForm) {
        if ($platForm == 1) {
            return $msg_list = [
                'nMsgNum' => 1,
                'nVoiceTime' => 0,
                'nIsHit' => 1,
                'vcHref' => '',
                'vcTitle' => '',
                'vcDesc' => '',
                'nMsgType' => 2001,
                'msgContent' => $msgData['msg'],
                'nAtLocation' => 0,//@人在文本的所在位置 0 文本开始位置 1文本结束位置 2任意位置
                'vcAtWxSerialNos' => []
            ];
        }
        if ($platForm == 0) {
            return [
                "task_type" => 1,
                "task_dict" => [
                    "wxid_to" => $msgData['room_wxid'],
                    "at_list" => [$msgData['wxid_from']], //at对象
                    "msg_list" => [[
                        "msg_type" => 1,
                        "msg" => '{$@}' . $msgData['msg']
                    ]],
                    "at_style" => 1
                ]
            ];
        }
        return [];
    }
}