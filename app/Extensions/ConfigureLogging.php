<?php
/**
 * 自定义日志，用来方便配置日志文件
 *
 */

namespace App\Extensions;

use Illuminate\Log\Writer;
use Illuminate\Contracts\Foundation\Application;

class ConfigureLogging
{
    /**
     * 设置应用的Monolog处理程序
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Log\Writer  $log
     * @return void
     */
    public function configureHandlers(Application $app, Writer $log)
    {
        $method = 'configure'.ucfirst($app['config']['app.log']).'Handler';

        $this->{$method}($app, $log);
    }

    /**
     * 设置应用single模式下的Monolog处理程序
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Log\Writer  $log
     * @return void
     */
    protected function configureSingleHandler(Application $app, Writer $log)
    {
        $config = $app->make('config');
        $filename = $config->get('app.log_path') . '/' . $config->get('app.log_name') . '.log';
        $log->useFiles($filename);
    }

    /**
     * 设置应用daily模式下的Monolog处理程序
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Log\Writer  $log
     * @return void
     */
    protected function configureDailyHandler(Application $app, Writer $log)
    {
        $config = $app->make('config');
        $filename = $config->get('app.log_path') . '/' . $config->get('app.log_name') . '.log';
        $log->useDailyFiles(
            $filename,
            $app->make('config')->get('app.log_max_files', 5)
        );
    }

    /**
     * 设置应用syslog模式下的Monolog处理程序
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Log\Writer  $log
     * @return void
     */
    protected function configureSyslogHandler(Application $app, Writer $log)
    {
        $log->useSyslog($app->make('config')->get('app.log_name'));
    }

    /**
     * 设置应用errorlog模式下的Monolog处理程序
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Log\Writer  $log
     * @return void
     */
    protected function configureErrorlogHandler(Application $app, Writer $log)
    {
        $log->useErrorLog();
    }
}
