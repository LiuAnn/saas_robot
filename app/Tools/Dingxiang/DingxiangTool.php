<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/21
 * Time: 21:47
 */

namespace App\Tool\Dingxiang;

use Illuminate\Support\Facades\Log;

require_once("CaptchaClient.php");
class DingxiangTool
{

    private $insertContent;
    private $Dingxaing;

    public function __construct( $insertContent = []){
        $this->insertContent = $insertContent;
    }

    public function changeToken(){
        Log::info(" get photo:",[$this->Dingxaing]);
        return $this->Dingxaing->change();
    }
}
