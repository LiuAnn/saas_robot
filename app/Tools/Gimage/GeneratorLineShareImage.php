<?php
/**
 * 创建线路分享的图片
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/7
 * Time: 下午3:26
 */

namespace App\Tools\Gimage;

use Intervention\Image\ImageManagerStatic as Image;
use App\Service\WeixinService;

use App\Tools\Gimage\GimageAbstract;

class GeneratorLineShareImage extends GimageAbstract implements GimageInterface
{
    protected $supplementHeight = 0;//补充的高度
    private $codeNumber;//用户的分享码

    public function __construct(array $insertContent = [],$codeNumber = '')
    {
        $this->codeNumber = $codeNumber;
        $this->saveDirectory = 'line';

        parent::__construct($insertContent);
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage(){

        if(empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->generatorHeader();
        $this->generatorAddress();
        $this->generatorContent();
        $this->generatorQrCode();
        $this->generatorDefault();



        $imgName = 'line_1_'.$this->insertContent['line_id'].".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }

    /**
     * 编辑图片的头部部分
     * @return mixed
     */
    public function generatorHeader(){
        //固定头部图片的样式
        $headerImg = Image::make($this->insertContent['pic'])->resize(750, 335);
        $this->imgObj->insert($headerImg, 'top', 0, 0);

        $log = Image::make(base_path()."/public/images/share_logo.png")->resize(180, 60);
        $this->imgObj->insert($log, 'top-right', 0, 0);


    }

    /**
     * 编辑目的地
     * @return mixed
     */
    public function generatorAddress(){

        $leavecity = $this->insertContent['leavecity'];
        $aimcity = $this->insertContent['aimcity'];
        $address = $leavecity." · ".$aimcity;
        $addressLen = mb_strlen($address);

        if( $addressLen >= 11)
        {
            $address = mb_substr($address,0 ,7,'utf-8')."...";
            $addressLen = 10;
        }

        $addressWidth = 170 + (($addressLen-7)*28);
        $this->imgObj->rectangle(0,270,$addressWidth,335, function ($draw) {
            $draw->background('rgba(255,119,73, 1)');
        });

        $this->imgObj->text($address, 15, 285, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(28);
            $font->color('#FFFFFF');
            $font->align('left');
            $font->valign('top');
        });

        //编辑天数

        $day = $this->insertContent['day'];
        $dayLength = mb_strlen($day);
        $dayWidth = $addressWidth + 70 + ($dayLength - 2) * 20;

        $this->imgObj->rectangle($addressWidth,270,$dayWidth,335, function ($draw2) {
            $draw2->background('rgba(6,142,41,1)');
        });

        $this->imgObj->text($day,$addressWidth + 10,  285, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(28);
            $font->color('#FFFFFF');
            $font->align('left');
            $font->valign('top');
        });

    }

    /**
     * 编辑图片的中间内容部分
     * @return mixed
     */
    public function generatorContent(){

        //编辑标题
        $title = $this->insertContent['title'];
        $titleLen = mb_strlen($title);
        $titleHeight = 360;
        if($titleLen > 19){

            $titleR = $titleLen >=43 ? mb_substr($title, 25,23,'utf-8')."..." : mb_substr($title, 25);
            $title = mb_substr($title,0,25,'utf-8');

            $this->imgObj->text($titleR,25,  $titleHeight + 45, function($font) {
                $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
                $font->size(30);
                $font->color('#000000');
                $font->align('left');
                $font->valign('top');
            });

        }

        $this->imgObj->text($title,25,  $titleHeight, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });

        //编辑副标题
        $subtitle =  $this->filter_mark($this->insertContent['subTitle']);
        $subtitle = preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/","",$subtitle);

        $subLen = mb_strlen($subtitle);
        $subtitleHeight = $titleLen > 19 ? $titleHeight + (50*2) :$titleHeight + (60*1);
        $subtitleR = '';
        $buchong = 0;

        if($subLen >26){

            $subtitleR = mb_substr($subtitle,24,24);
            if(mb_strlen($subtitleR) >26)
                $subtitleR = mb_substr($subtitle,24,24)."...";

            $subtitle  = $subLen >= 26 ? mb_substr($subtitle,0,24) : $subtitle;

            $buchong = 40;
        }

        $this->imgObj->text($subtitle,25,  $subtitleHeight, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(28);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });
        $this->imgObj->text($subtitleR,25,  $subtitleHeight+40, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(28);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });

        //编辑现价
        $prices = $this->insertContent['priced'] < $this->insertContent['price'] ? $this->insertContent['priced'] : $this->insertContent['price'];
        $pricesLen = mb_strlen($prices);
        $pricesHeight = $subtitleHeight + 80 + $buchong;

        $this->imgObj->text('￥',30,$pricesHeight+15, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(45);
            $font->color('#f95309');
            $font->align('left');
            $font->valign('top');
        });

        $this->imgObj->text($prices,70,$pricesHeight, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(70);
            $font->color('#f95309');
            $font->align('left');
            $font->valign('top');
        });

        //编辑原价
        if(isset($this->insertContent['price']) && $this->insertContent['priced'] != $this->insertContent['price']){

            $priced = $this->insertContent['priced'] < $this->insertContent['price'] ? $this->insertContent['price'] : $this->insertContent['priced'];
            $pricedLen = mb_strlen($priced);

            $pricedWidth = 160 + ($pricesLen-1) * 40;
            $pricedHeight = $pricesHeight+20;

            $this->imgObj->text('￥',$pricedWidth-25,$pricedHeight+8, function($font) {
                $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
                $font->size(23);
                $font->color('#777777');
                $font->align('left');
                $font->valign('top');
            });

            $this->imgObj->text($priced,$pricedWidth,$pricedHeight, function($font) {
                $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
                $font->size(35);
                $font->color('#777777');
                $font->align('left');
                $font->valign('top');
            });

            $lineWidth = $pricedWidth + 30 + ($pricedLen-1) * 20;

            $this->imgObj->line($pricedWidth-30, $pricedHeight +15, $lineWidth, $pricedHeight+15, function ($draw) {
                $draw->color('#000000');
            });

            //编辑优惠信息

            if(isset($this->insertContent['pricec'])) {
                $this->imgObj->text($this->insertContent['pricec'], 30, $pricesHeight + 80, function ($font) {
                    $font->file(base_path() . '/app/Tools/Gimage/wrf.ttf');
                    $font->size(27);
                    $font->color('#eea236');
                    $font->align('left');
                    $font->valign('top');
                });
            }
        }



    }


    /**
     * 编辑分享二维码
     */
    public function generatorQrCode(){

        $lineId = $this->insertContent['line_id'];
        $codeNumber = $this->codeNumber;

        $data = [
            'page'   => 'page/line/pages/detail/detail',
            'scene'  =>  "I=$lineId&C=$codeNumber"
        ];


        $headers    = [
            "Content-Type: application/json",
        ];

        //获取accessToken
        $accessToken =  WeixinService::getNewAccessToken('wxa404e150131464ed','be193d0c70732904d5f4b30f89f6f775');
        if(empty($accessToken)) {
            return '';
        }

        $url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessToken;

        $data = json_encode($data);

        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $QrCode = curl_exec($ch);
        if (is_array(json_decode($QrCode,true))){
            //获取accessToken
            $accessToken =  WeixinService::getCopyNewAccessToken('wxa404e150131464ed','be193d0c70732904d5f4b30f89f6f775');
            if(empty($accessToken)) {
                return '';
            }

            $url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessToken;

            $data = json_encode($data);

            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            if (!$QrCode = curl_exec($ch)){
                return "";
            }
        }


        $QrCodeImg = Image::make($QrCode)->resize(180,180);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 30, 100);
    }

    /**
     * 编辑其他信息
     */

    public function generatorDefault()
    {
        $this->imgObj->text('长按识码订购',30, 900 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(28);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });

        //编辑客服信息
        $this->imgObj->text('挖矿悦豆',270, 660 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(38);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });


        $this->imgObj->text('能抵扣，能兑现',270, 720 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });

        //自己消费返20%悦旅币，直推消费返10%悦旅币
        $this->imgObj->text('自己消费返',270, 780 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });
        $this->imgObj->text('20%',430, 780 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(38);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
        $this->imgObj->text('悦豆',520, 780 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });

        $this->imgObj->text('推广消费返',270, 840 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });
        $this->imgObj->text('10%',430, 840 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(38);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
        $this->imgObj->text('悦豆',520, 840 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });

        $this->imgObj->text('客服热线: 400-110-9600',270, 900 , function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(30);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });
    }

    public function filter_mark($text){
        if(trim($text)=='')return '';

        $text = str_replace('，',',',$text);
        $text = str_replace('；',';',$text);
        $text = str_replace('、',',',$text);
        $text = str_replace(' ','',$text);
        $text = str_replace('  ','',$text);
        $text = str_replace('   ','',$text);
        $text = str_replace('    ','',$text);

        //$text=preg_replace("/[[:punct:]\s]/",' ',$text);
        //$text=urlencode($text);
        //$text=preg_replace("/(%7E|%60|%21|%40|%23|%24|%25|%5E|%26|%27|%2A|%28|%29|%2B|%7C|%5C|%3D|\-|_|%5B|%5D|%7D|%7B|%3B|%22|%3A|%3F|%3E|%3C|\.|%2F|%A3%BF|%A1%B7|%A1%B6|%A1%A2|%A1%A3|%A3%AC|%7D|%A1%B0|%A3%BA|%A3%BB|%A1%AE|%A1%AF|%A1%B1|%A3%FC|%A3%BD|%A1%AA|%A3%A9|%A3%A8|%A1%AD|%A3%A4|%A1%A4|%A3%A1|%E3%80%82|%EF%BC%81|%EF%BC%8C|%EF%BC%9B|%EF%BC%9F|%EF%BC%9A|%E3%80%81|%E2%80%A6%E2%80%A6|%E2%80%9D|%E2%80%9C|%E2%80%98|%E2%80%99|%EF%BD%9E|%EF%BC%8E|%EF%BC%88)+/",' ',$text);
        //$text=urldecode($text);
        return trim($text);
    }
}