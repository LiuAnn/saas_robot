<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/12
 * Time: 下午8:57
 */

namespace App\Tools\Gimage;

use Intervention\Image\ImageManagerStatic as Image;
//use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CommunityInvitePosterImage extends GimageAbstract implements GimageInterface
{
    protected $imgType  = 'jpeg';
    protected $supplementHeight = 0;//补充的高度

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->genratorBackGround();

        //生成二维码
        $this->generatorQrCode($this->insertContent['appId'],$this->insertContent['appSecret'],$this->insertContent['page']);

        $this->makeUserImg();

        $imgName = 'community_invite'.time().$this->insertContent['codeNumber'].'.'.$this->imgType;

        return $this->save($imgName);


    }

    /**
     * 编辑背景
     */

    public function genratorBackGround()
    {
        $backGroundImg = $this->insertContent['posterImg'];

        $this->imgObj = Image::make($backGroundImg)->resize(750,1334);
    }

    /**
     * 编辑分享二维码
     */
    public function generatorQrCode($appId,$appSecret,$page){
//        $appId = 'wxa404e150131464ed';
//        $appSecret = 'be193d0c70732904d5f4b30f89f6f775';
//
//        $page  = 'page/strategy/index/index';
        $params = [
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'info' => [
                'page'   => $page,
                'scene'  =>  "R=".$this->insertContent['codeNumber']
            ],
        ];
        $QrCode = parent::generatorQrCodes($params);

        if(!empty($QrCode)&&empty(json_decode($QrCode)->errcode)) {

            $QrCodeImg = Image::make($QrCode)->resize(120, 120);

            $new= Image::canvas(150,150);

            $r=$QrCodeImg->width() /2;
            for($x=0;$x<$QrCodeImg->width();$x++) {

                for($y=0;$y<$QrCodeImg->height();$y++) {

                    $c=$QrCodeImg->pickColor($x,$y,'array');

                    if(((($x-$r) * ($x-$r) + ($y-$r) * ($y-$r)) < ($r*$r))) {

                        $new->pixel($c,$x,$y);
                    }
                }}

            $this->imgObj->insert($new, 'top-left', 550, 1200);
        }
    }

    /**
     * 处理用户图片
     */
    public function makeUserImg()
    {
        //头像处理
        $headerUrl = $this->insertContent['litpic'];
        $headerimg = Image::make($headerUrl);
        $bakPath   = public_path("share/share_".time()."_bak.jpeg");
        $_path = 'share';
        if (!file_exists($_path)) {
            if (!mkdir($_path, 0777, true)) {
            }
        }
        $headerimg->save($bakPath);
        $headerimg = Image::make($this->generatorRoundImg($bakPath))->resize(96, 96);
        $this->delteImg($bakPath);
        $this->imgObj->insert($headerimg, 'top-left', 63, 1220);
        //用户昵称
        $text = $this->insertContent['nickname'];
        $this->imgObj->text($text,165,1230, function($font) {
            $font->file(self::getFontPath());
            $font->size(20);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

}
