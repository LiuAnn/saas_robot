<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/22
 * Time: 上午5:11
 */
namespace App\Tools\Gimage;

use App\Tools\Ucloud\Image;
use Illuminate\Http\UploadedFile;

class ImgCompressV2
{
    /**
     * 原始文件
     * @var UploadedFile|string
     */
    protected $srcFile = '';

    /**
     * 初始化
     * ImgCompressV2 constructor.
     * @param UploadedFile $srcFile
     */
    public function __construct(UploadedFile $srcFile)
    {
        $this->srcFile = $srcFile;
    }

    /**
     * @return mixed
     */
    public function compress()
    {
        try{
            /**
             * 获取文件宽高
             */
            list($width,$height) = getimagesize($this->srcFile->getRealPath());

            dump(filesize($this->srcFile->getRealPath()));

            /**
             * 根据图片类型，选择处理函数
             */
            $map = [
                'image/png'=>'imagecreatefrompng',
                'image/jpeg'=>'imagecreatefromjpeg',
                'image/gif'=>'imagecreatefromgif'
            ];

            $mapOne = [
                'image/png'=>'imagejpeg',
                'image/jpeg'=>'imagepng',
                'image/gif'=>'imagegif'
            ];

            //读取方式
            $actionReadFun = array_get($map,$this->srcFile->getClientMimeType());
            //写入方式
            $actionWriteFun = array_get($mapOne,$this->srcFile->getClientMimeType());

            if(!($actionReadFun && $actionWriteFun)){
                return ['status'=>false,'msg'=>'类型错误,允许类型jpg,png,gif'];
            }

            //获取图片文件流
            $srcImage = @$actionReadFun($this->srcFile->getRealPath());

            //压缩图片
            $newImage = imagecreatetruecolor($width,$height);
            imagecopyresampled($newImage,$srcImage,0,0,0,0, $width, $height,$width,$height);
            imagedestroy($srcImage);

            //创建图片
            $fileName = str_random(10).time().'.'.$this->srcFile->getClientOriginalExtension();
            @$actionWriteFun($newImage,$fileName);

            dump(filesize($fileName));

            //上传图片
            $info = (new Image())->uploadUcloud($fileName,$fileName);

            //销毁图片
            @imagedestroy($newImage);

            if($info['error_code']!==100) {
                throw new \Exception('图片上传remote失败！');
            }

            //删除[public]临时文件
            @unlink($fileName);

            return ['status'=>true,'msg'=>$info['msg']];
        }catch (\Exception $exception){
            return ['status'=>false,'msg'=>$exception->getMessage()];
        }
    }


}
