<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/12
 * Time: 下午8:57
 */

namespace App\Tools\Gimage;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GetUniversityImage extends GimageAbstract implements GimageInterface
{
    protected $imgType  = 'jpeg';
    protected $supplementHeight = 0;//补充的高度

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->genratorBackGround();

        //生成二维码
        $this->generatorQrCode();

        $this->makeUserImg();

        $imgName = 'university'.time().$this->insertContent['codeNumber'].'.'.$this->imgType;

        return $this->save($imgName);
        //return $this->preview();

    }

    /**
     * 编辑背景
     */

    public function genratorBackGround()
    {
//        $backGroundImg = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-03-18/14/yuelvhuiuCBtASrl8c1584513176.jpeg';
        $backGroundImg = 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-03-18/15/yuelvhui5uCUjKalAy1584516372.jpeg';

        $this->imgObj = Image::make($backGroundImg)->resize(750,1334);
    }

    /**
     * 编辑分享二维码
     */
    public function generatorQrCode(){
        $url = 'https://single.yuetao.vip/page/goods/good-detail.html?product_id=378859&product_sku_id=402544&productType=0&type=ordinary&codeNumber='.$this->insertContent['codeNumber'];
        QrCode::format('png')->size(170)->generate($url,
            public_path("share/code_.png"));

        $QrCode = public_path("share/code_.png");
        $QrCodeImg = Image::make($QrCode)->resize(170, 170);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 560, 89);


    }

    /**
     * 处理用户图片
     */
    public function makeUserImg()
    {
        //头像处理
        $headerUrl = $this->insertContent['litpic'];
        $headerimg = Image::make($headerUrl);
        $bakPath   = public_path("share/share_".time()."_bak.jpeg");
        $headerimg->save($bakPath);
        $headerimg = Image::make($this->generatorRoundImg($bakPath))->resize(96, 96);
        $this->delteImg($bakPath);
        $this->imgObj->insert($headerimg, 'top-left', 18, 28);
        //用户昵称
        $this->imgObj->text($this->insertContent['nickname'],150,56, function($font) {
            $font->file(self::getFontPath());
            $font->size(41);
            $font->color('#FFEAB7');
            $font->align('left');
            $font->valign('top');
        });
    }

}
