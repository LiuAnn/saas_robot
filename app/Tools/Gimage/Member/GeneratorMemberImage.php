<?php
namespace App\Tools\Gimage\Member;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;
use App\Tools\Ucloud\Image as HttpImg;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GeneratorMemberImage extends GimageAbstract implements GimageInterface
{
    //appId
    protected $appId = '';

    //appSecret
    protected $appSecret = '';

    //跳转页面
    protected $page = '';

    //背景图片地址
    protected $bgImgUrl = '';

    //二维码携带的参数
    protected $extendData = [];

    public $moreImg = [];
    /**
     * 初始化
     * GeneratorMemberImage constructor.
     * @param array $insertContent
     */
    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->appId = array_get($insertContent,'appId','');

        $this->appSecret = array_get($insertContent,'appSecret','');

        $this->page = array_get($insertContent,'page','');

        $this->bgImgUrl = array_get($insertContent,'bgImgUrl','');

        $this->extendData = array_get($insertContent,'extendData','');

        $this->saveDirectory = 'member_share';
    }

    /**
     * 创建画布
     * @param string $width
     * @param string $height
     * @param string $backGround
     * @return \Intervention\Image\Image
     */
    public static function canvas($width = '750', $height = '980' ,$backGround = '')
    {
        return $backGround ? Image::canvas($width, $height,$backGround) :
            Image::canvas($width, $height);
    }

    /**
     * 二维码分享1
     * @return mixed
     */
    public function generatorImage()
    {
        //创建画板
        $this->imgObj = self::canvas(660,897);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize(660,897);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I'=>$this->extendData['I'],
            'C'=>$this->extendData['C'],
            'appId'=>$this->appId,
            'appSecret'=>$this->appSecret,
            'page'=>$this->page
        ]);

        //插入小程序二维码
        $QrCodeImg = Image::make($QrCode)->resize(176,176);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 242, 80);

        //保存图片
        $imgName = time().str_random(5).'shareImg.'.$this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 二维码分享二
     * @return mixed
     */
    public function generatorImageBonus()
    {
        //创建画板
        //$this->imgObj = self::canvas(660,897);
        $this->imgObj = self::canvas(300,300);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I' => $this->extendData['I'],
            'C' => $this->extendData['C'],
            'appId' => $this->appId,
            'appSecret' => $this->appSecret,
            'page' => $this->page
        ]);

        //插入小程序二维码
        $QrCodeImg = Image::make($QrCode)->resize(300, 300);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 0, 0);

        //保存图片
        $imgName = time() . str_random(5) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 二维码分享二
     * @return mixed
     */
    public function generatorImageBonus2()
    {
        //创建画板
        $this->imgObj = self::canvas(642,840);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize(642,840);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I' => $this->extendData['I'],
            'C' => $this->extendData['C'],
            'appId' => $this->appId,
            'appSecret' => $this->appSecret,
            'page' => $this->page
        ]);

        //插入小程序二维码
        $QrCodeImg = Image::make($QrCode)->resize(180,180);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 242, 110);

        //插入文本
        $this->imgObj->text($this->extendData['trueName'],330,450, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(43);
            $font->color('#904E2F');
            $font->align('center');
            $font->valign('top');
        });

        $this->imgObj->text($this->extendData['moneyMsg'],330,515, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            //$font->size(42);
            $font->size(24);
            $font->color('#BB2E29');
            $font->align('center');
            $font->valign('top');
        });


        //保存图片
        $imgName = time() . str_random(5) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 我的二维码分享海报
     * @param $url
     * @return mixed
     */
    public function generatorImageCenter($url)
    {
        //创建画板
        $this->imgObj = self::canvas(642,840);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize(642,840);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        /****
         * 生成二维码
         */
        QrCode::format('png')->size(180)->generate($url,
            public_path("share/code_.png"));

        $QrCode = public_path("share/code_.png");
        $QrCodeImg = Image::make($QrCode)->resize(180, 180);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 230, 118);

        //保存图片
        $imgName = time() . str_random(5) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 我的二维码分享海报第二版
     * @return mixed
     */
    public function generatorImageCenterV2()
    {
        //创建画板
        $this->imgObj = self::canvas(642,840);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize(642,840);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize(642,840);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I' => $this->extendData['I'],
            'C' => $this->extendData['C'],
            'appId' => $this->appId,
            'appSecret' => $this->appSecret,
            'page' => $this->page
        ]);

        $QrCodeImg = Image::make($QrCode)->resize(180, 180);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 230, 118);

        //保存图片
        $imgName = time() . str_random(5) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    public function generatorImageCenterV3($nickName)
    {
        $width = 1035;
        $height= 1575;

        //创建画板
        $this->imgObj = self::canvas($width,$height);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize($width,$height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize($width,$height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I'         => $this->extendData['I'],
            'C'         => $this->extendData['C'],
            'appId'     => $this->appId,
            'appSecret' => $this->appSecret,
            'page'      => $this->page
        ]);

        $QrCodeImg = Image::make($QrCode)->resize(300, 300);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 700, 200);

        //插入文本
        $this->imgObj->text("长按识别二维码",850,1400, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(32);
            $font->color('#AAAAAA');
            $font->align('center');
            $font->valign('top');
        });

        //插入文本
        $this->imgObj->text("领取新人礼包",855,1460, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(32);
            $font->color('#AAAAAA');
            $font->align('center');
            $font->valign('top');
        });

        //插入文本

        $this->imgObj->text($nickName . "邀您加入悦淘",350,1400, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(58);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });

        //保存图片
        $imgName = time() . str_random(5) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 根据参数，远程获取二维码
     * @param array $params
     * @return mixed|string
     */
    public function     generatorQrCode(array $params)
    {
        //二维码参数
        $scene = "I=".$params['I']."&C=".$params['C'];

        //参数拼接
        $paramData = [
            'app_id'            => $params['appId'],
            'app_secret'        => $params['appSecret'],
            'info' => ['page'   =>$params['page'], 'scene'=>$scene]
        ];

        //打印参数
        Log::debug('活动中心分享参互',$paramData);

        //获取远程图片
        $qrCode = parent::generatorQrCodes($paramData);
        return json_decode($qrCode,true) === null ? $qrCode : '';
    }

    /**
     * 生成我的二维码
     */
    public function makeQrCode($mid, $url) {
        $dir        = "images/share/myqr";
        $imgName    = "{$mid}.png";
        $filePath   = "{$dir}/{$imgName}";

        QrCode::format("png")->margin(.1)->size(200)->generate($url, public_path($filePath));

        return public_path($filePath);
    }

    /**
     * 生成用户海报二维码地址
     * @param $mid
     * @param $nickname
     * @param $url
     * @return mixed
     */
    public function makeMemberQrCode ($mid, $nickname, $url) {
        $bgImage = "https://image.yuelvhui.com/pubfile/2019/06/01/line_1559345715.jpg";

        $width  = 690;
        $height = 1050;

        $qrCodePath = $this->makeQrCode($mid, $url);

        $this->imgObj = self::canvas($width, $height);

        //插入背景图片
        $QrCodeImg  = Image::make($bgImage)->resize($width, $height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        // 二维码
        $makeCode = Image::make($qrCodePath)->resize(150, 150);
        $this->imgObj->insert($makeCode, "bottom-left", 480, 170);
        $this->imgObj->text($nickname . "\n邀您加入悦淘",165,900, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(32);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });

        $imgName   = time() . str_random(5) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }


    /**
     * 生成多张用户海报二维码地址
     * @param $mid
     * @param $nickname
     * @param $url
     * @param $type
     * @return mixed
     */
    public function makeMultiMemberQrCode($mid, $nickname, $url, $type = 1) {
        $bgImageMap =
            [
                'https://image.yuelvhui.com/pubfile/2019/07/23/line_1563871725_43368.png',
                'https://image.yuelvhui.com/pubfile/2019/07/23/line_1563873939_94040.png',
                'https://image.yuelvhui.com/pubfile/2019/07/23/line_1563874056_93401.png',
            ];
        $bgImage = $bgImageMap[$type];
        $width   = 632;
        $height  = 876;
        $qrCodePath = $this->makeQrCode($mid, $url);

        $this->imgObj = self::canvas($width, $height);

        //插入背景图片
        $QrCodeImg  = Image::make($bgImage)->resize($width, $height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        // 二维码
        $makeCode = Image::make($qrCodePath)->resize(150, 150);
        $this->imgObj->insert($makeCode, "bottom-left", 450, 120);
        $this->imgObj->text($nickname . "\n邀您加入悦淘",155,750, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(32);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });

        $imgName   = time() . str_random(100) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }


    /**
     * 生成多张用户海报二维码地址
     * @param $mid
     * @param $nickname
     * @param $url
     * @param $type
     * @param $data
     * @return mixed
     */
    public function makeNewMultiMemberQrCode($mid, $nickname, $url, $type = 1, $data = []) {
        $bgImageMap =
            [
                1 => 'https://image.yuelvhui.com/pubfile/2019/07/30/line_1564479000_41746.png',
                2 => 'https://image.yuelvhui.com/pubfile/2019/07/30/line_1564479147_31703.png',
                3 => 'https://image.yuelvhui.com/pubfile/2019/07/30/line_1564479213_35502.png',
            ];
        $bgImage = $bgImageMap[$type];
        $width   = 632;
        $height  = 876;
        $qrCodePath = $this->makeQrCode($mid, $url);

        $this->imgObj = self::canvas($width, $height);

        //插入背景图片
        $QrCodeImg  = Image::make($bgImage)->resize($width, $height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        if (!empty($data)) {
            $firstTitle  = $data['firstTitle'];
            $secondTitle = $data['secondTitle'];
            $this->imgObj->text($firstTitle,235,250, function($font) {
                $font->file(GeneratorMemberImage::getNewFontPath());
                $font->size(55);
                $font->color('#FFFFFF');
                $font->align('center');
                $font->valign('top');
            });

            $this->imgObj->text($secondTitle,305,350, function($font) {
                $font->file(GeneratorMemberImage::getNewFontPath());
                $font->size(55);
                $font->color('#FFFFFF');
                $font->align('center');
                $font->valign('top');
            });
        }
        // 二维码
        $makeCode = Image::make($qrCodePath)->resize(150, 150);
        $this->imgObj->insert($makeCode, "bottom-left", 450, 120);
        $this->imgObj->text($nickname . "\n邀您加入悦淘",155,750, function($font) {
            $font->file(GeneratorMemberImage::getQxFontPath());
            $font->size(32);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });

        $imgName   = time() . str_random(100) . 'shareImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 生成用户海报二维码地址
     * @return mixed
     */
    public function makeMemberQrCode2()
    {
        $width  = 690;

        $height = 1050;

        //创建画板
        $this->imgObj = self::canvas($width,$height);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize($width,$height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize($width,$height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I'         => $this->extendData['I'],
            'C'         => $this->extendData['C'],
            'appId'     => $this->appId,
            'appSecret' => $this->appSecret,
            'page'      => $this->page
        ]);

        $QrCodeImg = Image::make($QrCode)->resize(140, 140);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 470, 130);

        $this->imgObj->text($this->extendData['nickName'] . "\n邀您一起去旅行",165,940, function($font) {
            $font->file(GeneratorMemberImage::getFontPath());
            $font->size(32);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });


        //保存图片
        $imgName = time() . str_random(5) . 'mall_index_Img.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }


    /**
     * 生成用户海报二维码地址
     * @return mixed
     */
    public function makeFacialMaskQrCode($mid, $reCode)
    {

        $width  = 750;
        $height = 1334;

        //创建画板
        $this->imgObj = self::canvas($width,$height);
        $bgImage    = "https://image.yuelvhui.com/pubfile/2019/03/03/line_1551600386.jpg";

        //插入背景图片
        $QrCodeImg = Image::make($bgImage)->resize($width,$height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I'         => $mid,
            'C'         => $reCode,
            'appId'     => "wx331f738189e50cca",
            'appSecret' => "f6d7eaf295006d6836b9a8314cf5fd9b",
            'page'      => "page/Yuemall/pages/FacialMask/FacialMask",
        ]);

        $QrCodeImg = Image::make($QrCode)->resize(140, 140);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 310, 90);
        //保存图片
        $imgName = time() . str_random(5) . 'mall_fm_Img.' . $this->imgType;
        //返回图片地址
        return $this->save($imgName);
    }

}