<?php
namespace App\Tools\Gimage\Lottery;

use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;

class LotteryImage extends GimageAbstract implements GimageInterface
{
    //appId
    protected $appId = '';

    //appSecret
    protected $appSecret = '';

    //跳转页面
    protected $page = '';

    //背景图片地址
    protected $bgImgUrl = '';

    //二维码携带的参数
    protected $extendData = [];

    /**
     * 初始化
     * GeneratorMemberImage constructor.
     * @param array $insertContent
     */
    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->appId = array_get($insertContent, 'appId', '');

        $this->appSecret = array_get($insertContent, 'appSecret', '');

        $this->page = array_get($insertContent, 'page', '');

        $this->bgImgUrl = array_get($insertContent, 'bgImgUrl', '');

        $this->extendData = array_get($insertContent, 'extendData', '');

        $this->saveDirectory = 'member_lottery';
    }

    /**
     * 创建画布
     * @param string $width
     * @param string $height
     * @param string $backGround
     * @return \Intervention\Image\Image
     */
    public static function canvas($width = '750', $height = '980', $backGround = '')
    {
        return $backGround ? Image::canvas($width, $height, $backGround) :
            Image::canvas($width, $height);
    }

    /**
     * 小程序二维码分享
     * @return mixed
     */
    public function generatorImage()
    {
        $width = 690;

        $height= 1050;

        //创建画板
        $this->imgObj = self::canvas($width, $height);
        //$this->imgObj = Image::make($this->bgImgUrl)->resize($width, $height);
        //插入背景图片
        $QrCodeImg = Image::make($this->bgImgUrl)->resize($width, $height);
        $this->imgObj->insert($QrCodeImg, 'top-left', 0, 0);

        //远程获取小程序二维码
        $QrCode = $this->generatorQrCode([
            'I' => $this->extendData['I'],
            'C' => $this->extendData['C'],
            'appId' => $this->appId,
            'appSecret' => $this->appSecret,
            'page' => $this->page
        ]);

        //插入小程序二维码
        $QrCodeImg = Image::make($QrCode)->resize(150, 150);
        $this->imgObj->insert($QrCodeImg, 'bottom-left', 470, 170);

        //插入文本
        $text = "\"{$this->extendData['nickName']}\"".'邀您一起抽豪礼';
        $this->imgObj->text($text, 180, 900, function ($font) {
            $font->file(self::getFontPath());
            $font->size(24);
            $font->color('#AAAAAA');
            $font->align('center');
            $font->valign('top');
        });

        //保存图片
        $imgName = time() . str_random(5) . 'lotteryImg.' . $this->imgType;

        //返回图片地址
        return $this->save($imgName);
    }

    /**
     * 根据参数，远程获取二维码
     * @param array $params
     * @return mixed|string
     */
    protected function generatorQrCode(array $params)
    {
        //二维码参数
        $scene = "I=".$params['I']."&C=".$params['C'];

        //参数拼接
        $paramData = [
            'app_id'            => $params['appId'],
            'app_secret'        => $params['appSecret'],
            'info' => ['page'   =>$params['page'], 'scene'=>$scene]
        ];

        //获取远程图片
        $qrCode = parent::generatorQrCodes($paramData);

        return json_decode($qrCode,true) === null ? $qrCode : '';
    }
}