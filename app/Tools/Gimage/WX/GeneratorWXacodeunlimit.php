<?php
/**
 * Version  :  1.0
 * Create by:  李虎<lihu@yuelvhui.com>
 * Copyright:  Copright (c) 悦旅汇, www.yuelvehui.com
 * Created on:  2018/10/11 下午3:43
 */
namespace App\Tools\Gimage\WX;

use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;
use App\Service\WeixinService;

class GeneratorWXacodeunlimit extends GimageAbstract implements GimageInterface
{
    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'wx';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {
        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->getwxacodeunlimit();

        $imgName = $this->insertContent['appid'].time()."_code.".$this->imgType;
        $imgpath = $this->save($imgName);
        \Log::debug('-----------------------------code_params返回'.$imgpath.'------------------------------');

        return $imgpath;
    }

    /**
     * 生成小程序的二维码
     * @return string
     */
    private function getwxacodeunlimit()
    {
        $page = isset($this->insertContent['page']) && !empty($this->insertContent['page']) ? $this->insertContent['page'] : 'pages/home/home';
        $data = [
            'page'   => $page,
            'scene'  => 'I='.$this->insertContent['mid'].'&C='.$this->insertContent['mCode']
        ];

        $headers    = [
            "Content-Type: application/json",
        ];


        $accessToken =  WeixinService::getNewAccessToken($this->insertContent['appid'],$this->insertContent['appSecret']);

        $url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessToken;

        $data = json_encode($data);
\Log::debug('-----------------------------code_params二维码参数'.$data.'------------------------------');
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $QrCode = curl_exec($ch);

        if (is_array(json_decode($QrCode,true))){
            $accessToken =  WeixinService::getCopyNewAccessToken($this->insertContent['appid'],$this->insertContent['appSecret']);

            $url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessToken;

            $data = json_encode($data);
            \Log::debug('-----------------------------code_params二维码参数'.$data.'------------------------------');
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            if (!$QrCode = curl_exec($ch)){
                return "";
            }
        }

        $this->imgObj = Image::make($QrCode)->resize(240,240);
    }








}