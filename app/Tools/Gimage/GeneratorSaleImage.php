<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/12
 * Time: 下午8:57
 */

namespace App\Tools\Gimage;

use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageAbstract;
use App\Tools\Gimage\GimageTools;

class GeneratorSaleImage extends GimageAbstract implements GimageInterface
{
    protected $supplementHeight = 0;//补充的高度

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'seal';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->generatorCover();
        $this->genratorBackGround();
        $this->generatorPrices();
        $this->generatorPriced();

        $imgName = 'seal_share_'.time().$this->insertContent['type']."_".$this->insertContent['id'].".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }

    /**
     * 编辑背景
     */

    public function genratorBackGround()
    {
        $backGroundImg = 'https://images.yueshang.store/open/2019/01/25/5c4b11d27e3651548423634.png';
        $bgImg = Image::make($backGroundImg)->resize(750,230);
        $this->imgObj->insert($bgImg, 'bottom-left', 0, 0);
    }

    /**
     * 编辑产品封面图
     * @return mixed
     */
    public function generatorCover()
    {
        if (empty($this->insertContent['cover'])) {
            return [];
        }
        $this->imgObj = Image::make($this->insertContent['cover'])->resize(750, 600);

        $rount = 'https://images.yueshang.store/open/2019/01/25/5c4b1193927211548423571.png';
        $rountImg = Image::make($rount)->resize(400,220);
        $this->imgObj->insert($rountImg, 'top-left', 50, 100);

    }

    /**
     * 编辑现价
     * @return mixed
     */
    public function generatorPriced()
    {
        $prices = $this->insertContent['prices'] < $this->insertContent['priced'] ? $this->insertContent['prices'] : $this->insertContent['priced'];


        /*if($this->insertContent['type'] == 'hotel')
            $prices = $this->insertContent['priced'];*/

        $this->imgObj->text('会员价:￥'.$prices,70,155, function($font) {
            $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
            $font->size(50);
            $font->color('#FFE401');
            $font->align('left');
            $font->valign('top');
        });

    }

    /**
     * 编辑原价
     */
    public function generatorPrices()
    {

        if($this->insertContent['priced']!=$this->insertContent['prices']){

            $priced = $this->insertContent['prices'] > $this->insertContent['priced'] ? $this->insertContent['prices'] : $this->insertContent['priced'];


            $this->imgObj->text('原价:￥'.$priced,120,245, function($font) {
                $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
                $font->size(40);
                $font->color('#FFFFFF');
                $font->align('left');
                $font->valign('top');
            });


            $lineWidth = strlen($priced) * 20;
            if(strlen($priced) == 5)
                $lineWidth = strlen($priced) * 46;

            $this->imgObj->line(120, 265, 260+$lineWidth, 265, function ($draw) {
                $draw->color('#FFFFFF');
            });
        }
    }


}