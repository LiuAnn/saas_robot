<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/30
 * Time: 下午6:46
 */

namespace App\Tools\Gimage\Goods;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;


class GeneratorNewMiniGoodsImage extends GimageAbstract implements GimageInterface
{

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'goods';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->imgObj   = self::canvas(750,1066);


        $this->generatorBG();
        $this->generatorGoodImg();
        $this->generatorTitle();
        if($this->insertContent['mini_type'] == 'yldr'){
            $this->generatorVipPriceOfYldr();
            $this->generatorGoodPriceOfYldr();
        }else{
            $this->generatorVipPrice();
            $this->generatorGoodPrice();
        }

        $this->generatorShareWord();
        $this->generatorQrCode();
        $imgName = 'goods_share_'.$this->insertContent['goodId'].'_'.time().".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }


    public function generatorBG()
    {
        $bgImg = 'https://images.yueshang.store/open/2019/01/25/5c4aed4ba1ac41548414283.png';
        if ($this->insertContent['mini_type'] == 'yldr'){
            $bgImg = 'https://images.yueshang.store/open/2019/01/28/5c4ef301eb8781548677889.jpg';
        }

        $this->imgObj = Image::make($bgImg)->resize(750,1334);
    }


    /**
     * 编辑商品图片
     */
    public function generatorGoodImg()
    {
        $bgImg = Image::make($this->insertContent['goodsCoverImg'])->resize(660,656);
        $this->imgObj->insert($bgImg, 'top-left', 45, 128);

        $zzimgPath = 'https://images.yueshang.store/open/2019/01/25/5c4aedd970c0e1548414425.png';
        $zzimg = Image::make($zzimgPath)->resize(660,96);
        $this->imgObj->insert($zzimg, 'top-left', 45, 690);

    }



    /**
     *编辑商品的标题
     */
    public function generatorTitle()
    {
        $titleLen = mb_strlen($this->insertContent['goodName']);
        if($titleLen<=18){
            $this->imgObj->text($this->insertContent['goodName'],90,722, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(30);
                $font->color('#FFFFFF');
                $font->align('left');
                $font->valign('top');
            });
        }else{
            $title = mb_substr($this->insertContent['goodName'],0,26);

            $subtitle = mb_substr($this->insertContent['goodName'],26,24);

            $this->imgObj->text($title,90,712, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(24);
                $font->color('#FFFFFF');
                $font->align('left');
                $font->valign('top');
            });

            $this->imgObj->text($subtitle,90,742, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(24);
                $font->color('#FFFFFF');
                $font->align('left');
                $font->valign('top');
            });

        }




    }

    public function generatorVipPriceOfYldr()
    {
        if(isset($this->insertContent['vipPrice']) && !empty($this->insertContent['vipPrice']) && $this->insertContent['vipPrice'] > 0)
        {

            $priceWord = $this->insertContent['goodsFlashState'] == 1 ? '抢购价' : '会员价';
            $this->imgObj->text($priceWord.'：',74,867, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(30);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });

            $this->imgObj->text('￥'.$this->insertContent['vipPrice'],197,867, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(40);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });
        }
    }
    public function generatorGoodPriceOfYldr(){
        if(isset($this->insertContent['goodPrice']) && !empty($this->insertContent['goodPrice']) && $this->insertContent['goodPrice'] > 0)
        {
            if($this->insertContent['goodPrice'] != $this->insertContent['vipPrice']) {

                $this->imgObj->text('原价：', 74, 920, function ($font) {
                    $font->file(GeneratorGoodsImage::getFontPath());
                    $font->size(24);
                    $font->color('#999999');
                    $font->align('left');
                    $font->valign('top');
                });

                $this->imgObj->text('￥' . $this->insertContent['goodPrice'], 144, 922, function ($font) {
                    $font->file(GeneratorGoodsImage::getFontPath());
                    $font->size(24);
                    $font->color('#999999');
                    $font->align('left');
                    $font->valign('top');
                });


                $vipPriceLen = mb_strlen($this->insertContent['goodPrice']);

                $vipPriceLength = (int)167 + $vipPriceLen * 15;

                $this->imgObj->line(62, 935, $vipPriceLength, 935, function ($draw) {
                    $draw->color('#999999');
                    //$draw->width('2px');
                });
            }
        }

    }
    /**
     * 编辑抢购价vipPrice
     */
    public function generatorVipPrice()
    {
        if(isset($this->insertContent['goodPrice']) && !empty($this->insertContent['goodPrice']) && $this->insertContent['goodPrice'] > 0)
        {
            if($this->insertContent['priceName'] == '网易严选价格')
                $this->insertContent['priceName'] = '严选价格';

            $priceWord = $this->insertContent['goodsFlashState'] == 1 ? '抢购价' : $this->insertContent['priceName'];
            $this->imgObj->text($priceWord.'：',74,867, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(30);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });

            $this->imgObj->text('￥'.$this->insertContent['goodPrice'],197,867, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(40);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });
        }
    }
    /**
     * 编辑原价 goodPrice
     */
    public function generatorGoodPrice()
    {
        $remind = $this->insertContent['remind'];
        if(!empty($remind)){
            $this->imgObj->text($remind, 74, 920, function ($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(28);
                $font->color('#F4B445');
                $font->align('left');
                $font->valign('top');
            });
        }
    }

    /**
     * 编辑宣传内容
     */
    public function generatorShareWord()
    {
        $shareWord = '自用省钱，分享赚钱';
        if ($this->insertContent['mini_type'] == 'yldr'){
            $shareWord = '全球悦旅大人，可浪、可嗨、可交友';
        }

        $this->imgObj->text($shareWord,73,990, function($font) {
            $font->file(GeneratorGoodsImage::getFontPath());
            $font->size(24);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 编辑分享二维码
     */
    public function generatorQrCode(){

        $appId = '';
        $appSecret = '';

        if($this->insertContent['mini_type'] == 'wanmi'){
            $appId = 'wxa404e150131464ed';
            $appSecret = 'be193d0c70732904d5f4b30f89f6f775';
            $page  = 'page/Yuemall/pages/details/details';

        }elseif ($this->insertContent['mini_type'] == 'newMini'){
            $appId = 'wx331f738189e50cca';
            $appSecret = 'f6d7eaf295006d6836b9a8314cf5fd9b';
            $page  = 'page/Yuemall/pages/details/details';
        }elseif ($this->insertContent['mini_type'] == 'yldr'){
            $appId = 'wx3b5c2af451998243';
            $appSecret = 'ffbe0f98af4a8def1e533ad4aeaecbff';
            $page  = 'page/find/pages/buy/details/details';
        }

        $params = [
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'info' => [
                'page'   => $page,
                'scene'  =>  "I=".$this->insertContent['goodId']."&C=".$this->insertContent['codeNumber']
            ],
        ];
        Log::debug('分享商品的参数:'.json_encode($params));
        $QrCode = parent::generatorQrCodes($params);
        if(empty(json_decode($QrCode)->errcode)){

            $QrCodeImg = Image::make($QrCode)->resize(193,193);
            $this->imgObj->insert($QrCodeImg, 'bottom-right', 47, 324);

            $this->imgObj->text('长按识别二维码',533,1015, function($font) {
                $font->file(GeneratorGoodsImage::getFontPath());
                $font->size(24);
                $font->color('#AAAAAA');
                $font->align('left');
                $font->valign('top');
            });
        }

    }


}