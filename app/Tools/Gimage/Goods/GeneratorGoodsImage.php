<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/30
 * Time: 下午6:46
 */

namespace App\Tools\Gimage\Goods;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;


class GeneratorGoodsImage extends GimageAbstract implements GimageInterface
{
    protected $supplementHeight = 0;//补充的高度

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'seal';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->generatorCover();
        $this->genratorBackGround();
        $this->generatorPrices();
        $this->generatorPriced();

        $imgName = 'goods_share_'.time().$this->insertContent['goodId'].".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }

    /**
     * 编辑背景
     */

    public function genratorBackGround()
    {
        $backGroundImg = 'https://images.yueshang.store/open/2019/01/25/5c4b1dfecfac11548426750.png';
        $bgImg = Image::make($backGroundImg)->resize(750,230);
        $this->imgObj->insert($bgImg, 'bottom-left', 0, 0);
    }

    /**
     * 编辑产品封面图
     * @return mixed
     */
    public function generatorCover()
    {
        $this->imgObj = Image::make($this->insertContent['goodsCoverImg'])->resize(750, 600);

        $rount = 'https://images.yueshang.store/open/2019/01/25/5c4b1193927211548423571.png';
        $rountImg = Image::make($rount)->resize(400,220);
        $this->imgObj->insert($rountImg, 'top-left', 50, 100);
    }

    /**
     * 编辑会员价格
     * @return mixed
     */
    public function generatorPriced()
    {
        if(isset($this->insertContent['goodPrice']) && !empty($this->insertContent['goodPrice']) && $this->insertContent['goodPrice'] > 0)
        {
            if($this->insertContent['priceName'] == '网易严选价格')
                $this->insertContent['priceName'] = '严选价格';

            $priceWord = $this->insertContent['goodsFlashState'] == 1 ? '抢购价' : $this->insertContent['priceName'];
            $this->imgObj->text($priceWord.'￥'.$this->insertContent['goodPrice'],70,155, function($font) {
                $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
                $font->size(50);
                $font->color('#FFE401');
                $font->align('left');
                $font->valign('top');
            });
        }

    }

    /**
     * 编辑原价
     */
    public function generatorPrices()
    {
        $remind = $this->insertContent['remind'];
        if(!empty($remind)){
            $this->imgObj->text($remind,120,245, function($font) {
                $font->file(base_path().'/app/Tools/Gimage/wrf.ttf');
                $font->size(40);
                $font->color('#FFFFFF');
                $font->align('left');
                $font->valign('top');
            });
        }

    }

}