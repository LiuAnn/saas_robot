<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/7
 * Time: 下午3:13
 */

namespace App\Tools\Gimage;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\ImgCompress;

use App\Service\WeixinService;

abstract class GimageAbstract
{
    protected $imgObj;
    protected $saveDirectory = '';
    protected $imgType = 'jpeg';
    protected $baseSavePath = '';
    protected $insertContent = [];
    protected $url = "http://gateway.yuetao.vip/center/v1/app/third/upload";

    public function __construct($insertContent = [])
    {
        $this->imgObj       = self::canvas();
        $this->baseSavePath = base_path() . '/public/images/share/';
        if (!empty($insertContent))
            $this->insertContent = $insertContent;
    }

    /**
     * 创建默认的Image
     */
    public static function canvas($width = '750', $height = '980', $backgroud = '#FFFFFF')
    {
//        var_dump([$width, $height, $backgroud]);die();
        return Image::canvas($width, $height, $backgroud);
}

    /**
     * 预览Image
     */
    public function preview()
    {
        return $this->imgObj->response($this->imgType);
    }

    /**
     * 保存图片到本地并返回图片的实例
     * @param string $savePath
     * @return mixed
     */
    public function save($imgName, $savePath = '')
    {

        if (empty($savePath))
            $savePath = $this->baseSavePath . '/' . $this->saveDirectory;


        if (!is_dir($savePath))
            mkdir($savePath, 0777, true);

        $files   = $savePath . "/" . $imgName;
        $dst_img = $savePath . "/dst_" . $imgName;

        if (file_exists($dst_img))
            unlink($dst_img);

        $this->imgObj->save($files);


        $img = new \CURLFile(realpath($files));
        Log::info(" get photos 4:", [$img]);
        $pathInfo = self::postCurl($this->url, ['file' => $img]);

        Log::info('get imgage :', [$pathInfo]);
        if ($pathInfo['code'] == 200 && $pathInfo['data']['code'] == 200) {
            $imgUrl = $pathInfo['data']['url'];
        }

        if (file_exists($files))
            unlink($files);

        if (file_exists($dst_img))
            unlink($dst_img);

        return $imgUrl;


        /* (new ImgCompress($files,1))->compressImg($dst_img);

         if(file_exists($files))
             unlink($files);

         //上传值图片服务器
         $info = (new HttpImg())->newUploadUcloud($dst_img,$imgName);

         if(file_exists($dst_img))
             unlink($dst_img);

         if($info['error_code']!==100) {
             return '';
         }

         return $info['msg'];*/
    }

    public static function postCurl($url, $postData)
    {
        $headers = [
            "content-type: multipart/form-data"
        ];
        set_time_limit(0);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response    = curl_exec($curl);
        $res         = [];
        $res['code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $res['data'] = json_decode($response, TRUE);

        return $res;
    }

    /**
     * 删除图片
     * @param $imgPath
     * @return bool
     */
    public function delteImg($imgPath)
    {
        if (file_exists($imgPath))
            unlink($imgPath);

        return true;
    }

    /**
     * 获取字体路径
     */
    public static function getFontPath()
    {
        return base_path() . '/app/Tools/Gimage/wrf.ttf';
    }


    /**
     * 获取字体路径(方正正粗黑简体)
     */
    public static function getNewFontPath()
    {
        return base_path() . '/app/Tools/Gimage/fzzchjt.TTF';
    }


    /**
     * 获取字体路径(方正兰亭纤黑简体)
     */
    public static function getQxFontPath()
    {
        return base_path() . '/app/Tools/Gimage/FZLTXHJW.TTF';
    }


    /**裁剪图片为圆形图片
     * @param $imgpath
     * @return resource
     */
    public function generatorRoundImg($imgpath)
    {
        /*
        $src_img = '';
        try{
            $src_img = @imagecreatefrompng($imgpath);
        }catch ( Exception $exception){
            $src_img = @imagecreatefromjpeg($imgpath);
        }
        */

        $src_img = @imagecreatefromjpeg($imgpath);

        $wh  = getimagesize($imgpath);
        $w   = $wh[0];
        $h   = $wh[1];
        $w   = min($w, $h);
        $h   = $w;
        $img = imagecreatetruecolor($w, $h);
        //这一句一定要有
        imagesavealpha($img, true);
        //拾取一个完全透明的颜色,最后一个参数127为全透明
        $bg = imagecolorallocatealpha($img, 255, 255, 255, 127);
        imagefill($img, 0, 0, $bg);
        $r   = $w / 2; //圆半径
        $y_x = $r; //圆心X坐标
        $y_y = $r; //圆心Y坐标
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $rgbColor = imagecolorat($src_img, $x, $y);
                if (((($x - $r) * ($x - $r) + ($y - $r) * ($y - $r)) < ($r * $r))) {
                    imagesetpixel($img, $x, $y, $rgbColor);
                }
            }
        }

        return $img;
    }

    /**
     * 编辑分享二维码
     * @param $params
     * @return mixed|string
     */
    public function generatorQrCodes($params)
    {
        $appid     = $params['app_id'];
        $appSecret = $params['app_secret'];

        $headers = [
            "Content-Type: application/json",
        ];

        $accessToken= WeixinService::getNewAccessToken($appid,$appSecret);

        $url  = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessToken;
        $data = json_encode($params['info']);


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $QrCode = curl_exec($ch);
        if (is_array(json_decode($QrCode,true))) {
            $accessToken= WeixinService::getCopyNewAccessToken($appid,$appSecret);

            $url  = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessToken;
            $data = json_encode($params['info']);


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            if (!$QrCode = curl_exec($ch)) {
                return "";
            }
        }

        return $QrCode;
    }


}