<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/9/3
 * Time: 下午12:28
 */

namespace App\Tools\Gimage\Line;

use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;

class GeneratorLineLimitImage extends GimageAbstract implements GimageInterface
{
    const BUY_LIMIT_YES = 1;
    const BUY_LIMIT_NO  = 0;

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'buylimit';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->imgObj   = self::canvas(420,336);
        $this->generatorCover();
        $this->genratorWK();

        if($this->insertContent['isBuylimit'] == self::BUY_LIMIT_YES){
            $this->genratorLimitPrices();
        }else{
            $this->genratorPrices();
        }

        $imgName = 'line_share_limit'.time().$this->insertContent['line_id'].".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }

    /**
     * 编辑产品封面图
     * @return mixed
     */
    public function generatorCover()
    {
        $coversImg = $this->insertContent['pic'];
        $cover = Image::make($coversImg)->resize(421,330);
        $this->imgObj->insert($cover, 'top-left', 0, 0);
    }

    /**
     * 编辑抢购
     * @return mixed
     */
    public function genratorLimitPrices()
    {

        //抢购标识
        $flagImg = base_path().'/app/Tools/Gimage/template/line/6.png';
        $flag = Image::make($flagImg);
        $this->imgObj->insert($flag, 'top-left', 0, 0);

        //右上角标识
        $rightBg = base_path().'/app/Tools/Gimage/template/line/2.png';
        $rightBg = Image::make($rightBg);
        $this->imgObj->insert($rightBg, 'top-right', 0, 0);

        //编辑原价格
        $oldpricelen = strlen($this->insertContent['price']);
        $oldWordWidth = [
            1 => '320',
            2 => '320',
            3 => '320',
            4 => '310',
            5 => '310',
        ];
        $this->imgObj->text('原价',$oldWordWidth[$oldpricelen],5, function($font) {
            $font->file(GeneratorLineLimitImage::getFontPath());
            $font->size(17);
            $font->color('#171715');
            $font->align('left');
            $font->valign('top');
        });

        $oldPriceWidth = [
            1 =>'358',
            2 =>'358',
            3 =>'358',
            4 =>'348',
            5 =>'345',
        ];
        $this->imgObj->text($this->insertContent['price'],$oldPriceWidth[$oldpricelen],6, function($font) {
            $font->file(GeneratorLineLimitImage::getFontPath());
            $font->size(21);
            $font->color('#171715');
            $font->align('left');
            $font->valign('top');
        });

        $oldPricelineLeftWidth = [
            1 =>'355',
            2 =>'355',
            3 =>'355',
            4 =>'345',
            5 =>'345',
        ];
        $oldPricelineRightWidth = [
            1 =>'388',
            2 =>'388',
            3 =>'398',
            4 =>'398',
            5 =>'410',
        ];

        $this->imgObj->line($oldPricelineLeftWidth[$oldpricelen], 15, $oldPricelineRightWidth[$oldpricelen], 15, function ($draw) {
            $draw->color('#171715');
        });

        //编辑抢购价
        $buylimitPricelen = strlen($this->insertContent['buylimitPrice']);
        $buylimitPriceWidth = [
            1 =>'290',
            2 =>'290',
            3 =>'270',
            4 =>'255',
            5 =>'250',
        ];


        if($buylimitPricelen > 4){
            $this->imgObj->text($this->insertContent['buylimitPrice'],$buylimitPriceWidth[$buylimitPricelen],35, function($font) {
                $font->file(GeneratorLineLimitImage::getFontPath());
                $font->size(45);
                $font->color('#ffeb3b');
                $font->align('left');
                $font->valign('top');
            });
        }else{
            $this->imgObj->text($this->insertContent['buylimitPrice'],$buylimitPriceWidth[$buylimitPricelen],33, function($font) {
                $font->file(GeneratorLineLimitImage::getFontPath());
                $font->size(50);
                $font->color('#ffeb3b');
                $font->align('left');
                $font->valign('top');
            });
        }

        ///编辑抢购日期
        $this->imgObj->text($this->insertContent['buyLimitTime'],258,85, function($font) {
            $font->file(GeneratorLineLimitImage::getFontPath());
            $font->size(20);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

    }

    /**
     * 编辑非抢购
     */

    public function genratorPrices()
    {
        //可抵扣
        if(isset($this->insertContent['pricec']) && !empty($this->insertContent['pricec'])){

            $rightBg = base_path().'/app/Tools/Gimage/template/line/4.png';
            $rightBg = Image::make($rightBg);
            $this->imgObj->insert($rightBg, 'top-right', 0, 0);
            //编辑原价
            //编辑原价格
            $oldpricelen = strlen($this->insertContent['price']);
            $oldWordWidth = [
                1 => '320',
                2 => '320',
                3 => '320',
                4 => '310',
                5 => '310',
            ];
            $this->imgObj->text('原价',$oldWordWidth[$oldpricelen],5, function($font) {
                $font->file(GeneratorLineLimitImage::getFontPath());
                $font->size(17);
                $font->color('#171715');
                $font->align('left');
                $font->valign('top');
            });

            $oldPriceWidth = [
                1 =>'358',
                2 =>'358',
                3 =>'358',
                4 =>'348',
                5 =>'345',
            ];
            $this->imgObj->text($this->insertContent['price'],$oldPriceWidth[$oldpricelen],6, function($font) {
                $font->file(GeneratorLineLimitImage::getFontPath());
                $font->size(21);
                $font->color('#171715');
                $font->align('left');
                $font->valign('top');
            });

            $oldPricelineLeftWidth = [
                1 =>'355',
                2 =>'355',
                3 =>'355',
                4 =>'345',
                5 =>'345',
            ];
            $oldPricelineRightWidth = [
                1 =>'388',
                2 =>'388',
                3 =>'398',
                4 =>'398',
                5 =>'410',
            ];

            $this->imgObj->line($oldPricelineLeftWidth[$oldpricelen], 15, $oldPricelineRightWidth[$oldpricelen], 15, function ($draw) {
                $draw->color('#171715');
            });
            //编辑价格
            $pricedlen = strlen($this->insertContent['priced']);
            $pricedWidth = [
                1 =>'290',
                2 =>'290',
                3 =>'270',
                4 =>'255',
                5 =>'250',
            ];

            if($pricedlen > 4){
                $this->imgObj->text($this->insertContent['priced'],$pricedWidth[$pricedlen],35, function($font) {
                    $font->file(GeneratorLineLimitImage::getFontPath());
                    $font->size(45);
                    $font->color('#ffeb3b');
                    $font->align('left');
                    $font->valign('top');
                });
            }else{
                $this->imgObj->text($this->insertContent['priced'],$pricedWidth[$pricedlen],33, function($font) {
                    $font->file(GeneratorLineLimitImage::getFontPath());
                    $font->size(50);
                    $font->color('#ffeb3b');
                    $font->align('left');
                    $font->valign('top');
                });
            }

            //编辑抵扣信息
            if($this->insertContent['dedue'] > 999){
                $this->imgObj->text($this->insertContent['pricec'],258,85, function($font) {
                    $font->file(GeneratorLineLimitImage::getFontPath());
                    $font->size(18);
                    $font->color('#ffffff');
                    $font->align('left');
                    $font->valign('top');
                });
            }else{
                $this->imgObj->text($this->insertContent['pricec'],258,85, function($font) {
                    $font->file(GeneratorLineLimitImage::getFontPath());
                    $font->size(20);
                    $font->color('#ffffff');
                    $font->align('left');
                    $font->valign('top');
                });
            }


        }else{//非抵扣

            $rightBg = base_path().'/app/Tools/Gimage/template/line/5.png';
            $rightBg = Image::make($rightBg);
            $this->imgObj->insert($rightBg, 'top-right', 0, 0);

            //编辑价格
            $priceslen = strlen($this->insertContent['price']);
            $pricesWidth = [
                1 =>'290',
                2 =>'290',
                3 =>'270',
                4 =>'255',
                5 =>'250',
            ];

            if($priceslen > 4){
                $this->imgObj->text($this->insertContent['price'],$pricesWidth[$priceslen],10, function($font) {
                    $font->file(GeneratorLineLimitImage::getFontPath());
                    $font->size(45);
                    $font->color('#ffeb3b');
                    $font->align('left');
                    $font->valign('top');
                });
            }else{
                $this->imgObj->text($this->insertContent['price'],$pricesWidth[$priceslen],10, function($font) {
                    $font->file(GeneratorLineLimitImage::getFontPath());
                    $font->size(50);
                    $font->color('#ffeb3b');
                    $font->align('left');
                    $font->valign('top');
                });
            }

        }


    }

    /**
     * 编辑底部挖矿图片
     * @return mixed
     */
    public function genratorWK()
    {
        $bgImg = base_path().'/app/Tools/Gimage/template/line/1.png';
        $bgImg = Image::make($bgImg);

        $this->imgObj->insert($bgImg, 'bottom-left', 0, 0);
    }
}