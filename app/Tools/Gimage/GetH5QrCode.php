<?php
/**
 * Created by PhpStorm.
 * User: nnnn
 * Date: 2020/3/11
 * Time: 下午7:27
 */

namespace App\Tools\Gimage;

class GetH5QrCode{

    //请求中台路径
    private static $url = "https://gateway.yueshang.shop/private/v1";

    /**
     * 获取h5二维码
     * @param $url
     * @return mixed
     */
    public static function h5QrCode($url) {

        $data  = self::getCurlPostData(array('url'=>$url),'/open/jd/createQRcode');

        return  $data;
    }
    /**
     * post数据请求
     * @param array $data
     * @param string $postUrl
     * @return bool|string
     * @throws \Exception
     */
    public static function getCurlPostData($data = array(), $postUrl='')
    {
        $result = self::postCurls(self::$url.$postUrl, $data);
        return $result;
    }

    /**
     * POST请求https接口返回内容
     * @param string $url [请求的URL地址]
     * @param string $post [请求的参数]
     * @return  string
     */
    public static function postCurls($url, $post_data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);    // 提交地址
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_HEADER, 0); // 设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_POST, 1); // POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $output = curl_exec($ch);
        $arr    = json_decode($output, true);
        return $arr;
    }
}