<?php
/**
 * Description : class Description
 * Version  :  1.0
 * Create by:  李虎<lihu@yuelvhui.com>
 * Copyright:  Copright (c) 悦旅汇, www.yuelvehui.com
 * Created on:  2018/11/30 2:03 PM
 */

namespace App\Tools\Gimage\HD;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;

class GeneratorHDImage  extends GimageAbstract implements GimageInterface
{
    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'hd';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->imgObj   = self::canvas(750,1334);


        $this->genratorBackGround();
        //$this->generatorMember();
        $this->generatorQrCode();
        $imgName = 'hd_share'.$this->insertContent['mid'].time().".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }

    /**
     * 编辑背景
     */

    public function genratorBackGround()
    {
        // http://image.yuelvhui.com/open/2018/12/03/1543850838.jpg
        $backGroundImg = 'https://image.yuelvhui.com/pubfile/2019/04/05/line_1554404826.jpg';
        $bgImg = Image::make($backGroundImg)->resize(750,1334);
        $this->imgObj->insert($bgImg, 'top-left', 0, 0);
    }

    /**
     * 编辑邀请信息
     */

    public function generatorMember()
    {
        $nickname = $this->insertContent['nickname'];

        if (preg_match("/^[\x7f-\xff]+$/", $nickname)){
            $nickname = mb_strlen($nickname) > 5 ? mb_substr($nickname,0,5).'..':$nickname;
        }else{
            $nickname = mb_strlen($nickname) > 8 ? mb_substr($nickname,0,8).'..':$nickname;
        }

        $this->imgObj->text($nickname.'的汇店',370,59, function($font) {
            $font->file(GeneratorHDImage::getFontPath());
            $font->size(36);
            $font->color('#FF6A23');
            $font->align('center');
            $font->valign('top');
        });
    }

    /**
     * 编辑分享二维码
     */
    public function generatorQrCode(){
        $params = [
            'app_id' => 'wx40a98b4882ac8a28',
            'app_secret' => 'a986865ffc40cbc821b7320da780dfd5',
            'info' => [
                'page'   => 'page/ArticleHome/ArticleHome',
                'scene'  =>  "I=".$this->insertContent['mid']."&C=".$this->insertContent['codeNumber']
            ],
        ];
        $QrCode = parent::generatorQrCodes($params);
        if(empty(json_decode($QrCode)->errcode)){

            $QrCodeImg = Image::make($QrCode)->resize(226,226);
            $this->imgObj->insert($QrCodeImg, 'bottom-left', 262, 184);

//            $this->imgObj->text('扫码注册领50元现金红包',200,1184, function($font) {
//                $font->file(GeneratorHDImage::getFontPath());
//                $font->size(30);
//                $font->color('#000000');
//                $font->align('left');
//                $font->valign('top');
//            });
//
//            $this->imgObj->text('推广一人可得百元',232,1251, function($font) {
//                $font->file(GeneratorHDImage::getFontPath());
//                $font->size(36);
//                $font->color('#000000');
//                $font->align('left');
//                $font->valign('top');
//            });
        }

    }


}