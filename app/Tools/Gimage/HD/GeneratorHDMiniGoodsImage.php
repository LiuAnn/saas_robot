<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/30
 * Time: 下午6:46
 */

namespace App\Tools\Gimage\HD;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;


class GeneratorHDMiniGoodsImage extends GimageAbstract implements GimageInterface
{

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'goods';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->imgObj   = self::canvas(750,1066);


        $this->genratorLogo();
        $this->generatorGoodImg();
        $this->generatorTime();
        $this->generatorTitle();
        $this->generatorVipPrice();
        $this->generatorGoodPrice();
        $this->generatorShareWord();
        $this->generatorQrCode();
        $imgName = 'goods_share_'.time().".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();

    }


    /**
     * 编辑logo
     */
    public function genratorLogo()
    {
        $backGroundImg = base_path().'/app/Tools/Gimage/template/logo.png';
        $bgImg = Image::make($backGroundImg)->resize(267,77);
        $this->imgObj->insert($bgImg, 'top-left', 60, 25);

    }

    /**
     * 编辑商品图片
     */
    public function generatorGoodImg()
    {
        $bgImg = Image::make($this->insertContent['goodsCoverImg'])->resize(650,500);
        $this->imgObj->insert($bgImg, 'top-left', 50, 150);
    }


    /**
     * 编辑抢购时间
     */
    public function generatorTime()
    {
        if(!empty($this->insertContent['goodsFlashState'])
            && isset($this->insertContent['goodsFlashState'])
            && $this->insertContent['goodsFlashState'] == 1
        ){

            $backGroundImg = base_path().'/app/Tools/Gimage/template/Goods/qgjzsj.png';
            $bgImg = Image::make($backGroundImg)->resize(471,50);
            $this->imgObj->insert($bgImg, 'top-left', 59, 694);

            $this->imgObj->text($this->insertContent['goodsFlashEnd'],270,708, function($font) {
                $font->file(GeneratorHDMiniGoodsImage::getFontPath());
                $font->size(24);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });
        }
    }

    /**
     *编辑商品的标题
     */
    public function generatorTitle()
    {
        $titleLen = mb_strlen($this->insertContent['goodName']);
        $title = $titleLen > 10 ? mb_substr($this->insertContent['goodName'],0,10).'...' : $this->insertContent['goodName'];

        $this->imgObj->text($title,62,790, function($font) {
            $font->file(GeneratorHDMiniGoodsImage::getFontPath());
            $font->size(30);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 编辑抢购价vipPrice
     */
    public function generatorVipPrice()
    {
        if(isset($this->insertContent['goodPrice']) && !empty($this->insertContent['goodPrice']) && $this->insertContent['goodPrice'] > 0)
        {
            if($this->insertContent['priceName'] == '网易严选价格')
                $this->insertContent['priceName'] = '严选价格';

            $priceWord = $this->insertContent['goodsFlashState'] == 1 ? '抢购价' : $this->insertContent['priceName'];
            $this->imgObj->text($priceWord.'：',62,847, function($font) {
                $font->file(GeneratorHDMiniGoodsImage::getFontPath());
                $font->size(30);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });

            $this->imgObj->text('￥'.$this->insertContent['goodPrice'],185,852, function($font) {
                $font->file(GeneratorHDMiniGoodsImage::getFontPath());
                $font->size(30);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });
        }
    }
    /**
     * 编辑原价 goodPrice
     */
    public function generatorGoodPrice()
    {
        $remind = $this->insertContent['remind'];
        if (!empty($remind)) {
            $this->imgObj->text($remind, 62, 898, function ($font) {
                $font->file(GeneratorHDMiniGoodsImage::getFontPath());
                $font->size(24);
                $font->color('#000000');
                $font->align('left');
                $font->valign('top');
            });
        }

    }

    /**
     * 编辑宣传内容
     */
    public function generatorShareWord()
    {
        $this->imgObj->text('自用省钱，分享赚钱',62,948, function($font) {
            $font->file(GeneratorHDMiniGoodsImage::getFontPath());
            $font->size(30);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 编辑分享二维码
     */
    public function generatorQrCode(){
        $params = [
            'app_id' => 'wx40a98b4882ac8a28',
            'app_secret' => '1ecc0ac84936d9b3da7ad5bb7658f66f',
            'info' => [
                'page'   => 'page/Yuemall/pages/details/details',
                'scene'  =>  "I=".$this->insertContent['goodId']."&C=".$this->insertContent['codeNumber']
            ],
        ];
        Log::debug('分享商品的参数:'.json_encode($params));
        $QrCode = parent::generatorQrCodes($params);
        if(empty(json_decode($QrCode)->errcode)){

            $QrCodeImg = Image::make($QrCode)->resize(193,193);
            $this->imgObj->insert($QrCodeImg, 'bottom-left', 519, 127);

            $this->imgObj->text('长按识别二维码',533,955, function($font) {
                $font->file(GeneratorHDMiniGoodsImage::getFontPath());
                $font->size(24);
                $font->color('#AAAAAA');
                $font->align('left');
                $font->valign('top');
            });
        }

    }


}