<?php
/**
 * Description : class Description
 * Version  :  1.0
 * Create by:  李虎<lihu@yuelvhui.com>
 * Copyright:  Copright (c) 悦旅汇, www.yuelvehui.com
 * Created on:  2018/11/26 11:18 PM
 */

namespace App\Tools\Gimage\HD;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageInterface;
use App\Tools\Gimage\GimageAbstract;

class GeneratorHDHotelImage extends GimageAbstract implements GimageInterface
{
    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'hotel';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {

        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->generatorBG();
        $this->generatorHotelPic();
        $this->generatorTitle();
        $this->generatorQrCode();
        $this->generatorShareWord();
        $this->generatorHotelPrice();
        $this->generatorHotelStatisfy();
        $this->generatorHotelScore();

        $imgName = 'hotel_share_'.time().".".$this->imgType;
        return $this->save($imgName);
        //return $this->preview();
    }


    public function generatorBG()
    {
        $bgImg = 'https://image.yuelvhui.com/open/2019/01/25/5c4aed4ba1ac41548414283.png';
        $this->imgObj = Image::make($bgImg)->resize(750,1334);
    }

    /**
     * 编辑酒店图片
     */

    public function generatorHotelPic()
    {
        $bgImg = Image::make($this->insertContent['hotelPic'])->resize(660,656);
        $this->imgObj->insert($bgImg, 'top-left', 45, 128);

        $zzimgPath = 'https://image.yuelvhui.com/open/2019/01/25/5c4aedd970c0e1548414425.png';
        $zzimg = Image::make($zzimgPath)->resize(660,96);
        $this->imgObj->insert($zzimg, 'top-left', 45, 690);
    }

    /**
     *编辑酒店的标题
     */
    public function generatorTitle()
    {
        $titleLen = mb_strlen($this->insertContent['hotelName']);
        $title = $titleLen > 22 ? mb_substr($this->insertContent['hotelName'],0,22).'...' : $this->insertContent['hotelName'];

        $this->imgObj->text($title,90,722, function($font) {
            $font->file(GeneratorHDHotelImage::getFontPath());
            $font->size(30);
            $font->color('#FFFFFF');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 编辑评分
     */
    public function generatorHotelScore()
    {
        $this->imgObj->text($this->insertContent['hotelScore'],74,837, function($font) {
            $font->file(GeneratorHDHotelImage::getFontPath());
            $font->size(30);
            $font->color('#FF6232');
            $font->align('left');
            $font->valign('top');
        });

        $this->imgObj->text('分',118,837, function($font) {
            $font->file(GeneratorHDHotelImage::getFontPath());
            $font->size(24);
            $font->color('#CCCCCC');
            $font->align('left');
            $font->valign('top');
        });

    }

    /**
     * 编辑满意度
     */
    public function generatorHotelStatisfy()
    {
        $this->imgObj->text($this->insertContent['hotelStatisfy'],178,837, function($font) {
            $font->file(GeneratorHDHotelImage::getFontPath());
            $font->size(30);
            $font->color('#FF6232');
            $font->align('left');
            $font->valign('top');
        });

        $this->imgObj->text('满意度',247,837, function($font) {
            $font->file(GeneratorHDHotelImage::getFontPath());
            $font->size(24);
            $font->color('#CCCCCC');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 编辑价格
     */

    public function generatorHotelPrice()
    {
        if(empty($this->insertContent['hotelPrice']) || $this->insertContent['hotelPrice'] == 0) {
            $this->imgObj->text('暂无报价',72,910, function($font) {
                $font->file(GeneratorHDHotelImage::getFontPath());
                $font->size(40);
                $font->color('#FF6032');
                $font->align('left');
                $font->valign('top');
            });

        }else{
            $this->imgObj->text('￥'.$this->insertContent['hotelPrice'],70,910, function($font) {
                $font->file(GeneratorHDHotelImage::getFontPath());
                $font->size(40);
                $font->color('#FF6232');
                $font->align('left');
                $font->valign('top');
            });

            $hotelPriceLen = mb_strlen($this->insertContent['hotelPrice']);

            $hotelPriceLength  = (int)140 + $hotelPriceLen*16;

            $this->imgObj->text('起',$hotelPriceLength,915, function($font) {
                $font->file(GeneratorHDHotelImage::getFontPath());
                $font->size(24);
                $font->color('#CCCCCC');
                $font->align('left');
                $font->valign('top');
            });
        }

    }

    /**
     * 编辑宣传介绍
     */
    public function generatorShareWord()
    {
        $this->imgObj->text('悦旅会酒店底价销售',73,990, function($font) {
            $font->file(GeneratorHDHotelImage::getFontPath());
            $font->size(24);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }



    /**
     * 编辑分享二维码
     */
    public function generatorQrCode(){
        $params = [
            'app_id' => 'wx40a98b4882ac8a28',
            'app_secret' => '1ecc0ac84936d9b3da7ad5bb7658f66f',
            'info' => [
                'page'   => 'page/hotel/pages/home/hotel/hotelDetail/index',
                'scene'  =>  "I=".$this->insertContent['hotelId']."&C=".$this->insertContent['codeNumber']
            ],
        ];

        $QrCode = parent::generatorQrCodes($params);
        Log::debug('分享酒店的参数:'.json_encode($params));

        if(empty(json_decode($QrCode)->errcode)){

            $QrCodeImg = Image::make($QrCode)->resize(193,193);
            $this->imgObj->insert($QrCodeImg, 'bottom-right', 47, 324);

            $this->imgObj->text('长按识别二维码',533,1015, function($font) {
                $font->file(GeneratorHDHotelImage::getFontPath());
                $font->size(24);
                $font->color('#AAAAAA');
                $font->align('left');
                $font->valign('top');
            });
        }

    }

}