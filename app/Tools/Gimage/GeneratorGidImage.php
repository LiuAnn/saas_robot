<?php
/**
 * Created by PhpStorm.
 * User: lihu
 * Date: 2018/8/22
 * Time: 上午1:48
 */

namespace App\Tools\Gimage;

use Intervention\Image\ImageManagerStatic as Image;
use App\Tools\Gimage\GimageAbstract;
use App\Tools\Gimage\GimageTools;

class GeneratorGidImage extends GimageAbstract implements GimageInterface
{
    protected $supplementHeight = 0;//补充的高度

    public function __construct(array $insertContent = [])
    {
        parent::__construct($insertContent);

        $this->saveDirectory = 'dig';
    }

    /**
     * 创建图片
     * @return mixed
     */
    public function generatorImage()
    {
        if (empty($this->insertContent) || !isset($this->insertContent))
            return '';

        $this->imgObj = Image::make(base_path()."/app/Tools/Gimage/template/wk_share.jpg")->resize(750, 1334);

        $imgName = 'dig_share_'.$this->insertContent['mid'].time().".".$this->imgType;

        if(isset($this->insertContent['test']) && $this->insertContent['test'] == 1){
            $this->generatorQrCode();
            return $this->preview();
        }else{
            $this->generatorHeader();
            $this->generatorUserInfo();
            $this->generatorUserOrder();
            $this->generatorAlwaysDig();
            $this->generatorNowDig();
            $this->generatorDigOrder();
            $this->generatorQrCode();

        }


        return $this->save($imgName);
    }

    /**
     * 创建头像
     * @return mixed
     */
    public function generatorHeader()
    {
        $headerUrl = $this->insertContent['headImg'];

        $headerimg = Image::make($headerUrl);
        $bakPath   = base_path().'/public/share/'.'dig_share_'.$this->insertContent['mid'].'_bak.jpeg';
        $headerimg->save($bakPath);

        $headerimg = Image::make($this->generatorRoundImg($bakPath))->resize(75, 75);
        $this->delteImg($bakPath);
        $this->imgObj->insert($headerimg, 'top-left', 70, 660);
    }

    /**
     * 创建用户简介
     * @return mixed
     */
    public function generatorUserInfo()
    {
        $userInfo = $this->insertContent['userName'];
        $this->imgObj->text($userInfo,155, 660 , function($font) {//180
            $font->file(GeneratorGidImage::getFontPath());
            $font->size(30);
            $font->color('#434343');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 创建用户排名信息
     * @return mixed
     */
    public function generatorUserOrder()
    {
        $userOrder = $this->insertContent['rank'];
        $userOrderWord = "当天排名{$userOrder}名请继续加油哦";
        if($userOrder == 0)
            $userOrderWord = "当天暂无排名请继续加油哦";

        //处理排名信息
        $this->imgObj->text($userOrderWord,155, 710 , function($font) {//180
            $font->file(GeneratorGidImage::getFontPath());
            $font->size(28);
            $font->color('#777777');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 创建连续挖矿天数
     * @return mixed
     */
    public function generatorAlwaysDig()
    {
        $alwaysDig = isset($this->insertContent['days']) && !empty($this->insertContent['days']) ? $this->insertContent['days'] : 0;
        $alwaysDigX =[
            1 => '120',
            2 => '110',
            3 => '95',
            4 => '65',
        ];
        $alwaysDigLen = strlen($alwaysDig);

        $this->imgObj->text($alwaysDig,$alwaysDigX[$alwaysDigLen], 885 , function($font) {
            $font->file(GeneratorGidImage::getFontPath());
            $font->size(50);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 创建当天挖矿天数
     * @return mixed
     */
    public function generatorNowDig()
    {
        $nowDig = isset($this->insertContent['score']) && !empty($this->insertContent['score']) ? $this->insertContent['score'] : 0;
        $nowDigX =[
            1 => '365',
            2 => '350',
            3 => '330',
            4 => '315',
        ];
        $nowDigLen = strlen($nowDig);

        $this->imgObj->text($nowDig,$nowDigX[$nowDigLen], 885 , function($font) {
            $font->file(GeneratorGidImage::getFontPath());
            $font->size(50);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 创建挖矿排名
     * @return mixed
     */
    public function generatorDigOrder()
    {
        $noworder = isset($this->insertContent['rank']) && !empty($this->insertContent['rank']) ? $this->insertContent['rank'] : 0;
        $noworderX =[
            1 => '600',
            2 => '580',
            3 => '560',
            4 => '535',
            5 => '510',
            6 => '485',
        ];
        $noworderLen = strlen($noworder);
        $this->imgObj->text($noworder,$noworderX[$noworderLen], 885 , function($font) {
            $font->file(GeneratorGidImage::getFontPath());
            $font->size(50);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
        });
    }

    /**
     * 创建二维码
     * @return mixed
     */
    public function generatorQrCode()
    {
        $QrCodeUrl = $this->insertContent['QrCodeUrl'];
        $QrCode = Image::make($QrCodeUrl);
        $this->imgObj->insert($QrCode, 'bottom-right', 70, 90);
    }

}