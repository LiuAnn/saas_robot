<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2018/9/16
 * Time: 上午12:27
 */

namespace App\Model\PayToken;

use Illuminate\Database\Eloquent\Model;

class ConfigMerchantModel extends Model
{
    protected $connection = 'mysql_kernel';

    protected $table = "config_merchant";

    protected $primaryKey = 'mch_id';

    //关闭 Laravel 自动管理列
    public $timestamps = false;

    //设置和名单，为空则表明任何字段都可插入数据库
    protected  $guarded = [];
}