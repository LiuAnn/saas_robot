<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2018/12/12
 * Time: 下午10:38
 */

namespace App\Model\Mall;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OperationLog extends Model
{
    //定义表
    protected $table = "sline_operation_log";

    //定义主键
    protected $primaryKey = "id";


}