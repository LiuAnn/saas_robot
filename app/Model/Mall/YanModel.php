<?php

namespace App\Model\Mall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class YanModel extends Model
{

    public static function getProductShortCode($sign){
        $sql = " select short_url_code from mall_long_short_url where sign = ? ";
        $res =DB::connection('mysql_malls')->select($sql,[$sign]);
        return empty($res) ? 0 : $res[0]->short_url_code;
    }

    public static function getProductLongUrl($shortCode)
    {
        $sql = " select long_url from mall_long_short_url where short_url_code = ?  order by id  desc limit 1";
        $res =DB::connection('mysql_malls')->select($sql,[$shortCode]);
        return empty($res) ? 0 : $res[0]->long_url;
    }


    public  static  function  addProductShortCode($data)
    {
        return    DB::connection('mysql_malls')->table("long_short_url")->insertGetId($data);
    }


    public  static  function  checkIsSetShortCode($code)
    {
        $sql = " select count(*) total  from mall_long_short_url where  short_url_code = ? ";
        $res =   DB::connection('mysql_malls')->select($sql,[$code]);
        return  empty($res) ? 0 : $res[0]->total;
    }


}
