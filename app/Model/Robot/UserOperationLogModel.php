<?php

namespace App\Model\Robot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserOperationLogModel extends Model
{
    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'user_operation_log';

    protected $primaryKey = 'id';

    public static function insertAdminOperation($insert)
    {
        return self::insert($insert);
    }

    public static function getUserOperationList($param = [], $pageSize = 10)
    {
        $query = DB::connection('robot')->table('user_operation_log as a')
            ->leftjoin('user as b','a.mid','=','b.mid')
            ->select(['a.mid', 'b.nickname', 'a.type', 'a.change_data', 'a.message',
                'a.relevance_id', 'a.created_time']);
        //查询操作人id
        if (isset($param['midS']) && !empty($param['midS'])) {
            $query = $query->whereIn('a.mid', $param['midS']);
        }
        //查询类型 1 新增 2修改 3删除 4登录
        if (isset($param['pageSize']) && !empty($param['pageSize'])) {
            $pageSize = $param['pageSize'];
        }
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
            $query = $query->whereBetween('a.created_time', [$param['beginTime'], $param['endTime']]);
        }
        return $query->orderBy('a.id', 'desc')->paginate($pageSize);
    }
}
