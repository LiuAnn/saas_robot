<?php
/**
 * Created by PhpStorm.
 * User: 管理组员
 * Date: 2020/7/8
 * Time: 15:20
 */

namespace App\Model\Robot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ExclusiveMangerModel  extends Model
{
    /**
     * 判断平台是否存在
     * @param $whereData
     * @return array|mixed
     */
    public  static function isSetChannelData($whereData)
    {
        $returnData = DB::table('channel_data')
            ->select('id')
            ->where($whereData)
            ->get();
        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }
    /**
     * 判断是否为专属机器人
     * @param $WhileData
     */
    public   static function isWhiteChannelData($WhileData){
        $returnData = DB::table('white_list_wx')
            ->select('id','is_send_by_ass')
            ->where($WhileData)
            ->first();

        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }
    /**
     * 更新专属机器人的 发送状态
     */
    public static  function updateWhiteChannelData($update,$where){
        return DB::table("white_list_wx")->where($where)->update($update);
    }


    /**
     * 判断是否为专属机器人
     * @param $WhileData
     */
    public   static function isWhiteChannelBindMidData($WhileData){

        $returnData = DB::table('white_list_wx')
            ->select('id','is_send_by_ass','bind_mid','mid','wxid')
            ->where($WhileData)
            ->get();
        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }


}