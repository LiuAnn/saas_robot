<?php

namespace App\Model\Robot;

use App\Service\Community\StrategyArticleService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExclusiveApplicationModel extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * 关联到模型的数据表
     * @var string
     */
    protected $table = 'exclusive_application';

    protected $primaryKey = 'id';

    public static function getExclusiveApplicationList($params)
    {
        return DB::table('exclusive_application')->where(['mid' => $params['mid'],'deleted_at'=>0])->get();

    }


    public static function addExclusiveApplication()
    {
        return self::paginate();
    }


    public static function updateExclusiveApplication($data)
    {
        return DB::table("exclusive_application")->where(['id'=>$data['id'],'mid'=>$data['mid']])->update($data);
    }
}
