<?php

namespace App\Model\Robot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IntoGroupReplyModel extends Model
{
    //新人进群回复消息样式列表
    const STYLE_LIST = [
        ['id' => 1, 'name' => '回复样式一'],
        ['id' => 2, 'name' => '回复样式二'],
        ['id' => 3, 'name' => '回复样式三']
    ];

    //新人进群回复消息类型
    const TYPE_LIST = [
        ['id' => 2, 'name' => '图文'],
        ['id' => 1, 'name' => '文本'],
        ['id' => 3, 'name' => '视频'],
        ['id' => 4, 'name' => '文件']
    ];

    protected $table = 'into_group_reply';

    const PAGE_SIZE = 15;


    /*
    * 关键词回复列表
    */
    public static function updateIntoGroupReplyTag($tagId, $param) {
        $where = self::updateIntoGroupReplyTagWhere($param);
        $sql = "UPDATE `sline_community_into_group_reply` set `tag_id` = ? WHERE {$where}";
        return DB::connection('robot')->update($sql, [$tagId]);
    }
    /*
    * 关键词回复列表
    */
    public static function keyWordsReplyList($param, $begin, $pageSize) {
        $where = self::keyWordsReplyListWhere($param);
        $sql = "SELECT *";
        $sql .= " FROM `sline_community_into_group_reply` where {$where} limit $begin, $pageSize";
        return DB::connection('robot')->select($sql);
    }

    /*
    * 关键词回复列表
    */
    public static function keyWordsReplyCount($param) {
        $where = self::keyWordsReplyListWhere($param);
        $sql = "SELECT count(*) as cnt";
        $sql .= " FROM `sline_community_into_group_reply` where {$where}";
        $count =  DB::connection('robot')->select($sql);
        return $count[0]->cnt;
    }

    public static function keyWordsReplyListWhere($param)
    {
        $where = 'deleted_at = 0';
        if(isset($param['platform_identity']) && !empty($param['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "platform_identity = '".$param['platform_identity']."'";
        }
        if(isset($param['style']) && !empty($param['style'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "style = {$param['style']}";
        }
        return $where;
    }

    public static function updateIntoGroupReplyTagWhere($param)
    {
        $where = 'deleted_at = 0';
        if(isset($param['mid']) && !empty($param['mid'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "mid = {$param['mid']}";
        }
        if(isset($param['platform_identity']) && !empty($param['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "platform_identity = '{$param['platform_identity']}'";
        }
        return $where;
    }

    public static function addIntoGroupGreetingInfo($param)
    {
        return DB::connection('robot')->table('into_group_greeting')->insertGetId($param);
    }

    public static function updateIntoGroupGreetingInfo($where, $update)
    {
        return DB::connection('robot')->table('into_group_greeting')
            ->where($where)->update($update);
    }

    public static function getIntoGroupGreetingInfo($where)
    {
        return DB::connection('robot')->table('into_group_greeting')
            ->where($where)->first();
    }
}
