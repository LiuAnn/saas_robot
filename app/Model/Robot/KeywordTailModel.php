<?php

namespace App\Model\Robot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KeywordTailModel extends Model
{
    protected $table = 'record_keyword_detail';


    /**
     * 记录关键词详情触发次数加一
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function incrRecordKeywordDetail($condition){
        $where = [
            'room_id' => $condition['room_id'],
            'keyword_id' => $condition['keyword_id'],
            'platform_identity' => $condition['platform_identity'],
        ];
        return DB::table('record_keyword_detail')->where($where)->increment('touch_off_num');
    }


    /**
     * 记录关键词详情触发次数加一
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function insertRecordKeywordDetail($insertData){
        return DB::table('record_keyword_detail')->insertGetId($insertData);
    }

}
