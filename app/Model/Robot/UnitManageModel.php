<?php
/**
 * Created by PhpStorm.
 * User: 管理组
 * Date: 2020/7/8
 * Time: 15:20
 */

namespace App\Model\Robot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UnitManageModel  extends Model
{
    protected $table = 'group_manage';
    public $timestamps = false;

    /**
     * 添加小队
     * @param $data
     * @return mixed
     */
    public static  function  groupManageAdd($data)
   {
        return  DB::connection('robot')->table('group_manage')->insertGetId($data);
    }
    /**
     * 关联数据
     * @param $data
     * @return int
     */
    public static  function  groupManageStaffAdd($data)
    {
        return  DB::connection('robot')->table('group_staff_manage')->insertGetId($data);
    }
    /**
     * 分队列表
     * @param $params
     * @return mixed
     */
    public  static function getlist($params,$memberInfo)
    {
        return UnitManageModel::select('group_name','id as group_id')
            ->where(['deleted_at'=>0,'platform_identity'=>$memberInfo['platform_identity']])
            ->offset(($params['page']-1)*$params['pageSize'])
            ->limit($params['pageSize'])
            ->orderBy('id', 'desc')
            ->get();
    }

    public  static function getGroupAllList($memberInfo)
    {
        return UnitManageModel::select('group_name','id as group_id')
            ->where(['deleted_at'=>0,'platform_identity'=>$memberInfo['platform_identity']])
            ->orderBy('id', 'desc')
            ->get();
    }
    /**
     * 总的条数
     * @param $params
     * @param $memberInfo
     * @return int
     */
    public static function getlistCount($params, $memberInfo)
    {
        return DB::connection('robot')->table('group_staff_manage')->select('*')->where(['deleted_at' => 0, 'platform_identity' => $memberInfo['platform_identity']])->count();
    }

    /**
     * 获取没有绑定分组的人
     * @param $memberInfo
     */
    public static function  getNoMemberData($memberInfo){
        $getMemberDataSql = "select scw.bind_mid,scu.name  from  sline_community_white_list_wx as scw  
                             left join sline_community_user as scu on scw.bind_mid=scu.mid
                            where scw.bind_mid  not in(
                            SELECT bind_mid  FROM sline_community_group_staff_manage as scg  
                            where   scg.deleted_at=0  and   scg.platform_identity='".$memberInfo['platform_identity']."') 
                            and   scw.platform_identity='".$memberInfo['platform_identity']."'   
                            and   scu.platform_identity='".$memberInfo['platform_identity']."' and scu.user_type=2  group by scw.bind_mid ";
        $returnData =DB::select($getMemberDataSql);
        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }

    /**
     * 获取绑定的管理员
     * @param $data
     * @param string $filed
     * @return array|mixed
     */

    public  static function getfindDataStaffWhereNew($data)
    {
        $returnData = DB::table('group_staff_manage  as scg')
            ->leftJoin('user  as scu', 'scg.bind_mid', '=', 'scu.mid')
            ->select('scg.bind_mid', 'scg.group_mid', 'scg.id as staff_id', 'scu.name')
            ->where($data)
            ->get();
        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }

    public  static function getfindDataStaffWhereNew1($data)
    {
        $returnData = DB::table('group_staff_manage  as scg')
            ->select('scg.bind_mid', 'scg.group_mid', 'scg.id')
            ->where($data)
            ->get();
        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }

    /**
     * @param $data
     * @param int $page
     * @param int $pageSize
     * @return array|mixed
     */
    public  static function getfindDataStaffWhere($data,$page=1,$pageSize = 10)
    {   

        $where = self::getCommunityUserAdminWhere($data);

        if(isset($data['platform_identity'])&&$data['platform_identity']!="")
        {
            $where1 = " and m.platform_identity='".$data['platform_identity']."'";
        }

        $page = ($page-1)*$pageSize;

        $limit = " limit $page,$pageSize";

        $orderBy = " order by orderAmount desc";

        if(isset($data['orderBy']))
        {

            switch ($data['orderBy']) {
                case 1:
                    $orderBy = " order by orderAmount desc";
                    break;
                case 2:
                    $orderBy = " order by orderAmount asc";
                    break;
                case 3:
                    $orderBy = " order by orderCount desc";
                    break;
                case 4:
                    $orderBy = " order by orderCount asc";
                    break;
            }

        }
        $sql  = "select m.id,m.group_name,u.group_mid,count(DISTINCT g.id) as groupCount,count(o.room_id) as orderCount,sum(o.actual_price) orderAmount from sline_community_group_manage m";
        $sql .= " left join sline_community_group_staff_manage u on u.group_id = m.id and u.deleted_at=0";
        $sql .= " left join sline_community_white_list_wx w on u.bind_mid = w.bind_mid and w.status=0";
        $sql .= " left join sline_community_member_info_by_appid g on w.wxid = g.wxid and g.mid>0";
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id = g.id and o.pay_status=1 and o.room_id>0 $where";
        $sql .= " where m.deleted_at=0 $where1  GROUP BY m.id $orderBy $limit ";
        $returnData= DB::select($sql);

        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }


    /**
     * 获取绑定的管理员数量
     * @param $data
     * @param string $filed
     * @return array|mixed
     */
    public  static function getfindDataStaffCount($data)
    {   
        $where = "";

        if(isset($data['platform_identity'])&&$data['platform_identity']!="")
        {
            $where = " and platform_identity='".$data['platform_identity']."'";
        }

        if(isset($data['id'])&&$data['id']!="")
        {
            $where = " and group_id='".$data['id']."'";
        }

        $sql = "select count(*) as count_1 from sline_community_group_staff_manage where deleted_at=0 $where";

        $result = DB::select($sql);

        $arrayData  = json_decode(json_encode($result),true);

        return empty($arrayData)?0:$arrayData[0]['count_1'];
    }

    public static function getCommunityUserAdminWhere($param)
    {
        $where = "";

        if(isset($param['startTime'])&&$param['startTime']!="")
        {
            $where .=" and o.pay_time>'".$param['startTime']."'"; 
        }

        if(isset($param['endTime'])&&$param['endTime']!="")
        {
            $where1 =" and o.pay_time<'".$param['endTime']."'";

            if(isset($param['startTime'])&&isset($param['endTime'])&&$param['startTime']==$param['endTime'])
            {   
                $param['endTime'] = $param['endTime']+86400;

                $where1 =" and o.pay_time<'".$param['endTime']."'";
            }

            $where = $where.$where1;       
        }


        return $where;
    }

    /**
     * 删除历史
     * @param $delwhere
     */
    public static function  deleteDataStaffWhere($delwhere){
        $returnData = DB::connection('robot')->table('group_staff_manage')->where($delwhere)->delete();
        $arrayData = json_decode(json_encode($returnData), true);
        return empty($arrayData) ? [] : $arrayData;
    }
    /**
     * 单条数据查询
     * @param $data
     * @return mixed
     */
    public  static function getfindDataWhere($data,$filed='*')
    {
        $returnData= DB::connection('robot')->table('group_manage')->select($filed)->where($data)->first();
        $arrayData  = json_decode(json_encode($returnData),true);
        return  empty($arrayData)?[]:$arrayData;
    }

    /**数据更新
     * @param $id
     * @param $data
     * @param $updata
     * @return array|mixed
     */
    public static  function upDataWhere($wheredata, $updata)
    {
        $returnData = DB::connection('robot')->table('group_manage')->where($wheredata)->update($updata);
        $arrayData = json_decode(json_encode($returnData), true);
        return empty($arrayData) ? [] : $arrayData;
    }
    /**
     * 查看关联表事都有数据
     * @param $data
     * @param string $filed
     * @return array|mixed
     */
    public static function  getfindStaffWhere($data,$filed='*'){
        $returnCount= DB::connection('robot')->table('group_staff_manage')->select($filed)->where($data)->count();
        return  empty($returnCount)?0:$returnCount;
    }

    /**
     * 更新群组
     * @param $bindMid
     */
   public static function  getBindGroupData($bindMid){
       $data = DB::table('group_staff_manage as  scgs')
           ->leftJoin('group_manage  as scgm', 'scgs.group_id', '=', 'scgm.id')
           ->where(['scgs.deleted_at' => 0, 'scgs.bind_mid' => $bindMid,'scgm.deleted_at' => 0,])
           ->select('scgm.id', 'scgm.group_name')
           ->get();
       $data = json_decode(json_encode($data), true);
       return $data ? $data : [];

   }
   //加条件，时间
   public   static function updateGroupStaff($bindMid){
       $data = self::getfindStaffWhere(['bind_mid'=>$bindMid['mid']]);
       if(!empty($data)){
           DB::connection('robot')->table('group_manage')
               ->where(['bind_mid'=>$bindMid['mid'],'platform_identity'=>$bindMid['platform_identity']])
               ->update(['group_id'=>$bindMid['group_id']]);
       }else{
           $insertData['bind_mid']=$bindMid['mid'];
           $insertData['group_id']=$bindMid['group_id'];
           $insertData['platform_identity']=$bindMid['platform_identity'];
           self::groupManageStaffAdd($insertData);
       }
   }


//查询用户的认购的城数

    public function buy_num($uid){

        $startDate = date('Y-m-01', strtotime(date("Y-m-d")));

        $endDate = date('Y-m-d', strtotime("$startDate +1 month -1 day"));

        // 将日期转换为Unix时间戳

        $endDate=$endDate." 22:59:59";

        $startDateStr = strtotime($startDate);

        $endtDateStr = strtotime($endDate);

        return $this->where('uid',$uid)->where('buy_type',1)->whereBetween('create_time', array($startDateStr,$endtDateStr))->sum('buy_num');

    }

    /**
     * 根据id查找城池信息 只返回某个字段的值
     * @param $id
     * @return array
     */
    public function getCityName($id)
    {
        if($this->where('city_id',$id)->first()){
            return $this->where('city_id',$id)->lists('city_name')[0];
        }else{
            return [];
        }
    }


    public static function getUserNameByMid($id)
    {
        $result = DB::table('user')->where("mid",$id)->pluck('name');

        $arrayData  = json_decode(json_encode($result),true);

        return empty($arrayData)?'':$arrayData[0];
    }



    public static function getMidByGroupId($id)
    {
        $result = DB::table('group_staff_manage')->where("group_id",$id)->pluck('bind_mid');

        $arrayData  = json_decode(json_encode($result),true);

        return empty($arrayData)?[]:$arrayData;
    }



    public static function getLiveList($param)
    {   

        $where = self::getCommunityUserAdminWhere($param);

        $page = ($param['page']-1)*$param['pageSize'];

        $pageSize = $param['pageSize'];

        $limit = " limit $page,$pageSize";

        $sql  = " select u.name,u.mid,o.live_id,count(o.ordersn) as orderCount,sum(o.actual_price) orderAmount from  ".config('community.oldMallOrderGoods')." o";
        $sql .= " left join sline_community_member_info_by_appid g on g.id=o.room_id";
        $sql .= " left join sline_community_white_list_wx w on g.wxid=w.wxid";
        $sql .= " left join sline_community_user u on w.bind_mid = u.mid";
        $sql .= " where o.live_id>0 $where group by o.live_id $limit";

        $result = DB::select($sql);

        $arrayData  = json_decode(json_encode($result),true);

        return empty($arrayData)?[]:$arrayData;
    }


    public static function getLiveCount($param)
    {

        $sql  = " select count(*) from  ".config('community.oldMallOrderGoods')." o";
        $sql .= " where o.live_id>0 group by o.live_id";

        $result = DB::select($sql);

        return empty($result)?0:count($result);
    }

    public static function getGroupNameByMid($uid)
    {
        if(empty($uid))return '';

        $sql  = " select m.group_name from sline_community_group_staff_manage s";
        $sql .=" left join sline_community_group_manage m on m.id=s.group_id";
        $sql .=" where s.deleted_at = 0 and m.deleted_at=0 and s.bind_mid = $uid limit 1";

        $result = DB::select($sql);

        $arrayData  = json_decode(json_encode($result),true);

        return empty($arrayData)?'':$arrayData['0']['group_name'];
    }




}