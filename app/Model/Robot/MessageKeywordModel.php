<?php

namespace App\Model\Robot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MessageKeywordModel extends Model
{

    protected $table = 'message_keyword';

    const PAGE_SIZE = 15;

    public $timestamps = false;

    /*
    * 关键词列表
    */
    public static function getKeyWordsList($param, $pageSize) {
        $where = [
            'deleted_at' => 0
        ];
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where['platform_identity'] = $param['platform_identity'];
        }
        $query = DB::connection('robot')
            ->table('message_keyword')
            ->where($where);
        if (isset($param['keyword']) && !empty($param['keyword'])) {
            $query = $query->where('keyword','like','%'.$param['keyword'].'%');
        }
        return $query->paginate($pageSize)->toArray();
    }


    /*
    * 关键词列表
    */
    public static function getKeyWordsDetail($param) {
        //$where = ['deleted_at' => 0];
        $where = [];
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where['platform_identity'] = $param['platform_identity'];
        }
        if (isset($param['id']) && !empty($param['id'])) {
            $where['id'] = $param['id'];
        }
        $query = DB::connection('robot')
            ->table('message_keyword')
            ->where($where);
        return $query->first();
    }


    public static function addKeyWords($param) {
        return DB::table('message_keyword')->insertGetId($param);
    }

    public static function updateKeyWords($param, $pageSize) {
        $where = ['romm_id' => $param['roomId']];
        $fields = ['nickname', 'head_img', 'sex', 'country', 'province', 'city', 'wx_alias', 'room_nickname', 'created_at'];
        $query = DB::connection('robot')->table('member_weixin_info')->select($fields)->where($where);
        if (isset($param['nickname']) && !empty($param['nickname'])) {
            $query = $query->where('nickname','like','%'.$param['nickname'].'%');
        }
        return $query->paginate($pageSize)->toArray();
    }

    public static function deleteKeyWords($param, $pageSize) {
        $where = ['romm_id' => $param['roomId']];
        $fields = ['nickname', 'head_img', 'sex', 'country', 'province', 'city', 'wx_alias', 'room_nickname', 'created_at'];
        $query = DB::connection('robot')->table('member_weixin_info')->select($fields)->where($where);
        if (isset($param['nickname']) && !empty($param['nickname'])) {
            $query = $query->where('nickname','like','%'.$param['nickname'].'%');
        }
        return $query->paginate($pageSize)->toArray();
    }
}
