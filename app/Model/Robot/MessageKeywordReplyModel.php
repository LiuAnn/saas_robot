<?php

namespace App\Model\Robot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class MessageKeywordReplyModel extends Model
{

    protected $table = 'message_reply';

    const PAGE_SIZE = 15;


    /*
    * 关键词回复列表
    */
    public static function keyWordsReplyList($param, $begin, $pageSize) {
        $where = self::keyWordsReplyListWhere($param);
        $sql = "SELECT Mr.*, Mk.keyword";
        $sql .= " FROM `sline_community_message_reply` as Mr left join `sline_community_message_keyword` as Mk on Mr.`keyword_id` = Mk.`id` where {$where} limit $begin, $pageSize";
        return DB::connection('robot')->select($sql);
    }

    /*
    * 关键词回复列表
    */
    public static function keyWordsReplyCount($param) {
        $where = self::keyWordsReplyListWhere($param);
        $sql = "SELECT count(*) as cnt";
        $sql .= " FROM `sline_community_message_reply` as Mr where {$where}";
        $count =  DB::connection('robot')->select($sql);
        return $count[0]->cnt;
    }

    public static function keyWordsReplyListWhere($param)
    {
        $where = 'Mr.deleted_at = 0';
        if(isset($param['platform_identity']) && !empty($param['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "Mr.platform_identity = '{$param['platform_identity']}'";
        }
        if(isset($param['keyword']) && !empty($param['keyword'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "Mr.keyword like %{$param['keyword']}%";
        }
        return $where;
    }
}
