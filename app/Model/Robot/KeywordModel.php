<?php

namespace App\Model\Robot;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KeywordModel extends Model
{

    protected $table = 'record_keyword';

    const PAGE_SIZE = 15;

    public $timestamps = false;

    /*
    * 关键词列表
    */
    public static function getKeyWordsList($param, $pageSize) {
        $where = [
            'deleted_at' => 0
        ];
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where['platform_identity'] = $param['platform_identity'];
        }
        $query = DB::connection('robot')
            ->table('record_keyword')
            ->where($where);
        if (isset($param['keyword']) && !empty($param['keyword'])) {
            $query = $query->where('keyword','like','%'.$param['keyword'].'%');
        }
        if (isset($param['touch_off_num'])&&!empty($param['touch_off_num'])&&in_array($param['touch_off_num'], ['asc', 'desc'])) {
            $query = $query->orderBy('touch_off_num',$param['touch_off_num']);
        }
        return $query->paginate($pageSize)->toArray();
    }

    /*
    * 关键词列表
    */
    public static function keyWordsDetail($param, $begin = 0, $pageSize = 10) {
        $where = self::getKeyWordsDetailWhere($param);
        $orderBy = self::getKeyWordsDetailOrderBy($param);
        $sql = "SELECT d.room_id,d.keyword_id,k.keyword,d.touch_off_num,d.created_at,g.name ";
        $sql .= " FROM sline_community_record_keyword_detail d";
        $sql .= " LEFT JOIN sline_community_record_keyword k ON d.keyword_id = k.id";
        $sql .= " LEFT JOIN sline_community_member_info_by_appid g ON d.room_id = g.id";
        $sql .= " WHERE  {$where} {$orderBy} limit $begin, $pageSize";
        //$sql .= " WHERE  {$where} limit $begin, $pageSize";
        return DB::connection('robot')->select($sql);
    }

    /*
    * 关键词列表
    */
    public static function keyWordsDetailCount($param) {
        $where = self::getKeyWordsDetailWhere($param);
        $sql = "SELECT  count(*) as cnt";
        $sql .= " FROM sline_community_record_keyword_detail d";
        $sql .= " LEFT JOIN sline_community_member_info_by_appid g ON d.room_id = g.id";
        $sql .= " WHERE  {$where}";
        $count = DB::connection('robot')->selectOne($sql);
        return $count ? $count->cnt : 0;
    }

    public static function getKeyWordsDetailWhere($condition)
    {

        $where = "";

        if(isset($condition['id']) && !empty($condition['id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "d.keyword_id  = {$condition['id']}";
        }
        if(isset($condition['room_name']) && !empty($condition['room_name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like '%{$condition['room_name']}%'";
        }
        if(isset($condition['beginTime'])&&isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "d.created_at between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "d.created_at >= '{$condition['beginTime']}'";
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "d.created_at <= '{$condition['endTime']}'";
        }
        return $where;
    }

    public static function getKeyWordsDetailOrderBy($condition)
    {
        $orderBy = '';
        if(isset($condition['touch_off_num'])&&!empty($condition['touch_off_num'])&&in_array($condition['touch_off_num'], ['asc', 'desc'])){
            $orderBy .= "order by d.touch_off_num {$condition['touch_off_num']}";
        }
        return $orderBy;
    }

    /*
    * 关键词列表
    */
    public static function getKeyWordsDetail($param) {
        //$where = ['deleted_at' => 0];
        $where = [];
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where['platform_identity'] = $param['platform_identity'];
        }
        if (isset($param['id']) && !empty($param['id'])) {
            $where['id'] = $param['id'];
        }
        $query = DB::connection('robot')
            ->table('record_keyword')
            ->where($where);
        return $query->first();
    }


    public static function addKeyWords($param) {
        return DB::table('record_keyword')->insertGetId($param);
    }



    public static function fetchKeyWordsInfo($id) {

        $keyWordsInfo = DB::table('record_keyword')->where(['id' => $id])->first();
        return $keyWordsInfo ? get_object_vars($keyWordsInfo) : [];
    }

    public static function updateKeyWords($param, $pageSize) {
        $where = ['romm_id' => $param['roomId']];
        $fields = ['nickname', 'head_img', 'sex', 'country', 'province', 'city', 'wx_alias', 'room_nickname', 'created_at'];
        $query = DB::connection('robot')->table('member_weixin_info')->select($fields)->where($where);
        if (isset($param['nickname']) && !empty($param['nickname'])) {
            $query = $query->where('nickname','like','%'.$param['nickname'].'%');
        }
        return $query->paginate($pageSize)->toArray();
    }

    public static function deleteKeyWords($param, $pageSize) {
        $where = ['romm_id' => $param['roomId']];
        $fields = ['nickname', 'head_img', 'sex', 'country', 'province', 'city', 'wx_alias', 'room_nickname', 'created_at'];
        $query = DB::connection('robot')->table('member_weixin_info')->select($fields)->where($where);
        if (isset($param['nickname']) && !empty($param['nickname'])) {
            $query = $query->where('nickname','like','%'.$param['nickname'].'%');
        }
        return $query->paginate($pageSize)->toArray();
    }
}
