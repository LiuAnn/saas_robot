<?php

namespace App\Model\Integral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupIntegralConfModel extends Model
{
    //指定表名
    protected $table = 'group_integral_conf';
    //指定主键字段
    protected $primaryKey = 'id';

    public static function getSignConfigInfo($where)
    {
        $where['deleted_at'] = 0;
        $signConfigInfo = DB::connection('robot')->table('group_integral_conf')
            ->where($where)->first();
        return $signConfigInfo ? json_decode(json_encode($signConfigInfo), true) : [];
    }



    public static function getSignIntegralByRoomId($roomId)
    {
        $where = [
            'room_id' => $roomId,
            'deleted_at' => 0
        ];
        return DB::connection('robot')->table('group_integral_conf')
            ->where($where)->value('sign_integral');
    }


    public static function getSignIntegralByMemberId($where)
    {
        $where['deleted_at'] = 0;
        return DB::connection('robot')->table('group_integral_conf')
            ->where($where)->value('sign_integral');
    }
}
