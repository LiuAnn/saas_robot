<?php

namespace App\Model\Integral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupIntegralModel extends Model
{
    //指定表名
    protected $table = 'group_integral';
    //指定主键字段
    protected $primaryKey = 'id';

    public static function ifExistGroupSignRecord($where)
    {
        $beginTime = date('Y-m-d');
        return DB::connection('robot')->table('group_integral')
            ->where($where)->where('created_at', '>', $beginTime)->count();
    }

    public static function getYesterdayGroupSignRecord($where)
    {
        $beginTime = date('Y-m-d', strtotime("-1 day"));
        $endTime = date('Y-m-d');
        $yesterdayData = DB::connection('robot')->table('group_integral')
            ->where($where)->whereBetween('created_at', array($beginTime,$endTime))->first();
        return $yesterdayData ? json_decode(json_encode($yesterdayData), true) : [];
    }


    public static function insertGroupSignRecord($insertData)
    {
        return DB::connection('robot')->table('group_integral')->insertGetId($insertData);
    }


    public static function updateGroupMemberIntegral($where, $sign_amount)
    {
        return DB::connection('robot')->table('member_weixin_info')
            ->where($where)->increment('integral_account', $sign_amount);
    }

    public static function getGroupMemberIntegral($where)
    {
        return DB::connection('robot')->table('member_weixin_info')
            ->where($where)->value('integral_account');
    }


    public static function getInviterByRoomWxIdAndWxid($where)
    {
        return DB::connection('robot')->table('member_weixin_info')
            ->where($where)->value('inviter');
    }


    public static function getInviterSerialNoByRoomWxIdAndWxid($where)
    {
        return DB::connection('robot')->table('member_weixin_info')
            ->where($where)->value('user_serial_no');
    }

    public static function getGroupSignKeyword($where)
    {
        return DB::connection('robot')->table('member_weixin_info')
            ->where($where)->value('keyword');
    }
}
