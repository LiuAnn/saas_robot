<?php
namespace App\Model\University;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class UniversityModel extends Model
{


    public static function getOrCodeImages($data=[])
    {
        $where = self::getWhere($data);

        $sql    = "select * from sline_or_code_image WHERE deleted_at=0 {$where} order by id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    public static function getWhere($param = [])
    {
        $where='';

        if(isset($param['id'])||!empty($param['id']))
        {
            $where.=" and id = {$param['id']}";
        }

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and name like '%{$param["keyWord"]}%'";
        }

        return $where;
    }

    /**
     * 统计数据数量
     */
    public static function getListCount($param)
    {
        $where  = self::getWhere($param);
        $sql    = "select count(*) count from sline_or_code_image where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    public static function getOrCodeImage($data=[])
    {
        $where = self::getWhere($data);

        $result='';
        if ($where)
        {
            $sql    = "select * from sline_or_code_image where deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
        }

        return $result;
    }

    public static function upOrCodeImage($where,$data)
    {
        return DB::table('sline_or_code_image')->where($where)->update($data);
    }








}