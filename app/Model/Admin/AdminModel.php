<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminModel extends Model {

    /*
     * 管理员的添加
     */
    public static function create($param = [])
    {
        return self::newDb()
            -> table('ylh_admin')
            -> insert($param);
    }

    /*
     * 查询管理员信息
     */
    public static function getAdmin()
    {

    }

    public static function newDb()
    {
        return DB::connection('statistics');
    }
}