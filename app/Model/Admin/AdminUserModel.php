<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminUserModel extends Model
{
    use SoftDeletes;

    protected $table = 'admin';

    protected $primaryKey = 'admin_user_id';

    protected $guarded = [];

    public static $publicKeys = [
        "admin_user_id",
        'admin_user_name',
        'admin_user_type'
    ];

    /*
     * 查询是否存在该管理员
     */
    public static function adminUserId($adminUserId = 0)
    {
        return AdminUserModel::where(['admin_user_id' => $adminUserId])
            -> first();
    }

    /*
     * 查询所有的管理员
     */
    public static function allAdmin()
    {
        return AdminUserModel::where(['deleted_at' => null])
            -> get()
            -> toArray();
    }
    /**
     * @param $params
     * @return bool
     * @author   liuwenhao
     * @time     2020/4/14 15:20
     * @method   method   [更改密码]
     */
    public static function updateUser($where, $update){
        return self::where($where)->update($update);
    }

    /*
     * 插入
     */
    public static function addAdminUser($data=[]){
        return AdminUserModel::insertGetId($data);
    }

    public static function getAdminUserInfo($data=[])
    {
        $where = self::getWhere($data);

        return AdminUserModel::where($where)
            -> first();
    }

    public static function getWhere($param=[])
    {
        $where=[];

        if(isset($param['admin_user_name'])||!empty($param['admin_user_name']))
        {
            $where['admin_user_name'] = $param['admin_user_name'];
        }
        if(isset($param['admin_user_id'])||!empty($param['admin_user_id']))
        {
            $where['admin_user_id'] = $param['admin_user_id'];
        }

        return $where;
    }


    /*
     * 查询所有的管理员
     */
    public static function getAdminUsers($param=[])
    {
        $where = self::getWhere($param);

        return AdminUserModel::where($where)->orderBy("admin_user_id",'desc')
            -> get()->toArray();
    }

    public static function getAdminListByName($param = [])
    {
        $fields = [
            'admin_user_id', 'staff_name'
        ];
        $query = AdminUserModel::select($fields);
        if (isset($param['userName']) && !empty($param['userName'])) {
            $query = $query->where('staff_name','like','%'.$param['userName'].'%');
        }
        if (!isset($param['pageSize']) || empty($param['pageSize'])) {
            $param['pageSize'] = 10;
        }
        return $query->where(['deleted_at' => 0])->paginate($param['pageSize']);
    }


    /*
    * 查询所有的管理员
    */
    public static function getListCount($param=[])
    {
        $where = self::getWhere($param);

        return AdminUserModel::where($where)
            -> count();
    }

    public static function getAdminUserInfo2($data=[])
    {
        $where = self::getWhere_str($data);

        //
        $sql    = "select * from sline_community_admin WHERE (deleted_at is NUll or deleted_at = '0000-00-00 00:00:00') and admin_user_id>0 {$where} ";
        $result = DB::selectOne($sql);
        return $result;
    }

    public static function getWhere_str($param=[])
    {
        $where='';

        if(isset($param['admin_user_name'])||!empty($param['admin_user_name']))
        {
            $where.=" and admin_user_name = '{$param['admin_user_name']}'";
        }

        if(isset($param['no_admin_user_id'])||!empty($param['no_admin_user_id']))
        {
            $where.=" and admin_user_id != {$param['no_admin_user_id']}";
        }

//        if(isset($param['admin_user_id'])||!empty($param['admin_user_id']))
//        {
//            $where.=" and admin_user_id = {$param['admin_user_id']}";
//        }

        if(isset($param['adminId'])||!empty($param['adminId']))
        {
            $where.=" and admin_user_id = {$param['adminId']}";
        }

        if(isset($param['staffName'])||!empty($param['staffName']))
        {
            $where .= " and staff_name like '%{$param["staffName"]}%'";
        }

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and (staff_name like '%{$param["keyWord"]}%' or admin_user_name like '%{$param["keyWord"]}%')";
        }
        return $where;
    }

    public static function upAdminUser($where,$data)
    {
        return DB::table('admin')->where($where)->update($data);
    }





    public static function getAdminUsers2($data=[])
    {
        $where = self::getWhere_str($data);

        $sql    = "select * from sline_community_admin WHERE (deleted_at is NUll or deleted_at = '0000-00-00 00:00:00') and admin_user_id!=48 {$where} order by admin_user_id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    /**
     * 统计数据数量
     */
    public static function getListCount2($param)
    {
        $where  = self::getWhere_str($param);
        $sql    = "select count(*) count from sline_community_admin where (deleted_at is NUll or deleted_at = '0000-00-00 00:00:00') and admin_user_id!=48 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }



    public static function getRolesList($data=[])
    {
        $where = self::getRoleWhere($data);

        $sql    = "select * from sline_community_new_role WHERE deleted_at=0 {$where} order by role_id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }


    public static function getUserRolesList($data=[])
    {
        $where = self::getRoleWhere($data);

        $sql    = "select * from sline_community_user_role WHERE deleted_at=0 {$where} order by role_id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    /**
     * spz
     * @param array $data
     * @return array
     */
    public static function getUserRolesFrontList($data=[])
    {
        $where = self::getRoleFrontWhere($data);

        $sql    = "select * from  sline_community_user_role_front  WHERE deleted_at=0 {$where} order by role_id desc limit ?, ?";

        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    /**条件
     * @param $param
     * @return int
     */
    public  static function  getUserRolesFrontCount($param){
        $where  = self::getRoleFrontWhere($param);
        $sql    = "select count(*) count from  sline_community_user_role_front  where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }


    public static function getUserFrontRole($data=[])
    {
        $where = self::getRoleFrontWhere($data);
        $result='';
        if ($where)
        {
            $sql    = "select * from  sline_community_user_role_front  where deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
        }
        return $result;
    }


    public static function getUserRoleFront($data=[])
    {
        $where = self::getRoleFrontWhere($data);

        $result='';
        if ($where)
        {
            $sql    = "select * from sline_community_user_role_front where deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
        }

        return $result;
    }
    /**条件
     * @param array $param
     * @return string
     */
    public static function getRoleFrontWhere($param = [])
    {
        $where='';

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and role_name like '%{$param["keyWord"]}%'";
        }

        if(isset($param['roleName'])||!empty($param['roleName']))
        {
            $where.=" and role_name = '{$param['roleName']}'";
        }

        if(isset($param['roleId'])||!empty($param['roleId']))
        {
            $where.=" and role_id = {$param['roleId']}";
        }
        if(isset($param['roleIds'])||!empty($param['roleIds']))
        {
            $where.=" and role_id in ({$param['roleIds']})";
        }

        if(isset($param['noRoleId'])||!empty($param['noRoleId']))
        {
            $where.=" and role_id != {$param['noRoleId']}";
        }


        if(isset($param['platform_identity']))
        {
            $where.=" and platform_identity = '{$param['platform_identity']}'";
        }

        if(isset($param['mid'])||!empty($param['mid']))
        {
            $where.=" and mid = {$param['mid']}";
        }
        return $where;
    }



    public static function getRoles($data=[])
    {
        $where = self::getRoleWhere($data);

        $sql    = "select * from sline_community_new_role WHERE deleted_at=0 {$where} order by role_id desc";
        $result = DB::select($sql);
        return $result;
    }

    public static function getUserRoles($data=[])
    {
        $where = self::getRoleWhere($data);

        $sql    = "select * from sline_community_user_role WHERE deleted_at=0 {$where} order by role_id desc";
        $result = DB::select($sql);
        return $result;
    }


    public static function getRoleWhere($param = [])
    {
        $where='';

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and role_name like '%{$param["keyWord"]}%'";
        }

        if(isset($param['roleName'])||!empty($param['roleName']))
        {
            $where.=" and role_name = '{$param['roleName']}'";
        }

        if(isset($param['roleId'])||!empty($param['roleId']))
        {
            $where.=" and role_id = {$param['roleId']}";
        }
        if(isset($param['roleIds'])||!empty($param['roleIds']))
        {
            $where.=" and role_id in ({$param['roleIds']})";
        }

        if(isset($param['noRoleId'])||!empty($param['noRoleId']))
        {
            $where.=" and role_id != {$param['noRoleId']}";
        }

        return $where;
    }

    /**
     * 统计数据数量
     */
    public static function getRolesCount($param)
    {
        $where  = self::getRoleWhere($param);
        $sql    = "select count(*) count from sline_community_new_role where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    /**
     * 统计数据数量
     */
    public static function getUserRolesCount($param)
    {
        $where  = self::getRoleWhere($param);
        $sql    = "select count(*) count from sline_community_user_role where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }



    public static function getRole($data=[])
    {
        $where = self::getRoleWhere($data);

        $result='';
        if ($where)
        {
            $sql    = "select * from sline_community_new_role where deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
        }

        return $result;
    }

    public static function getUserRole($data=[])
    {
        $where = self::getRoleWhere($data);

        $result='';
        if ($where)
        {
            $sql    = "select * from sline_community_user_role where deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
        }

        return $result;
    }





    /*
   * 插入
   */
    public static function addRole($data=[])
    {
        return DB::table('new_role')->insertGetId($data);
    }

    public static function addFrontRole($data=[])
    {
        return DB::table('user_role_front')->insertGetId($data);
    }

    /*
   * 插入
   */
    public static function addUserRole($data=[])
    {
        return DB::table('user_role')->insertGetId($data);
    }

    public static function upRole($where,$data)
    {
        return DB::table('new_role')->where($where)->update($data);
    }

    public static function upUserRole($where,$data)
    {
        return DB::table('user_role')->where($where)->update($data);
    }

    public static function upUserFrontRole($where,$data)
    {
        return DB::table('user_role_front')->where($where)->update($data);
    }


    public static function getPowerWhere($param = [])
    {
        $where='';

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and power_name like '%{$param["keyWord"]}%'";
        }

        if(isset($param['powerId'])||!empty($param['powerId']))
        {
            $where.=" and power_id = {$param['powerId']}";
        }

        if(isset($param['powerIds'])||!empty($param['powerIds']))
        {
            $where.=" and power_id in ({$param['powerIds']})";
        }

        if(isset($param['powerName'])||!empty($param['powerName']))
        {
            $where.=" and power_name = '{$param['powerName']}'";
        }

        if(isset($param['noPowerId'])||!empty($param['noPowerId']))
        {
            $where.=" and power_id != {$param['noPowerId']}";
        }

        if(isset($param['level'])||!empty($param['level']))
        {
            $where.=" and level = {$param['level']}";
        }

        if(isset($param['pid'])||!empty($param['pid']))
        {
            $where.=" and pid = {$param['pid']}";
        }

        return $where;
    }



    public static function getPowersList($data=[])
    {
        $where = self::getPowerWhere($data);

        $sql    = "select * from sline_community_new_power WHERE deleted_at=0 {$where} order by sort desc,power_id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    public static function getUserPowersList($data=[])
    {
        $where = self::getPowerWhere($data);

        $sql    = "select * from sline_community_user_power WHERE deleted_at=0 {$where} order by sort desc,power_id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    public static function getPowers($data=[])
    {
        $where = self::getPowerWhere($data);

        $sql    = "select * from sline_community_new_power WHERE deleted_at=0 {$where} order by power_id desc";
        $result = DB::select($sql);
        return $result;
    }

    public static function getUserPowers($data=[])
    {
        $where = self::getPowerWhere($data);

        $sql    = "select * from sline_community_user_power WHERE deleted_at=0 {$where} order by sort desc, power_id asc";
        $result = DB::select($sql);
        return $result;
    }

    /**
     * 统计数据数量
     */
    public static function getPowersCount($param)
    {
        $where  = self::getPowerWhere($param);
        $sql    = "select count(*) count from sline_community_new_power where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    /**
     * 统计数据数量
     */
    public static function getUserPowersCount($param)
    {
        $where  = self::getPowerWhere($param);
        $sql    = "select count(*) count from sline_community_user_power where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    public static function getPower($data=[])
    {
        $where = self::getPowerWhere($data);

        $sql    = "select * from sline_community_new_power WHERE deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return $result;
    }

    public static function getAdminOperationList($param = [], $pageSize = 10)
    {
        $query = DB::connection('robot')->table('admin_log as a')
            ->leftjoin('admin as b','b.admin_user_id','=','a.admin_user_id')
            ->select(['a.admin_user_id', 'b.staff_name', 'a.type', 'a.change_data', 'a.message',
                'a.relevance_id', 'a.created_time']);
        $where = [];
        //查询操作人id
        if (isset($param['admin_user_id']) && !empty($param['admin_user_id'])) {
            $where['a.admin_user_id'] = $param['admin_user_id'];
        }
        //查询类型 1 新增 2修改 3 删除 4登录
        if (isset($param['type']) && !empty($param['type'])) {
            $where['a.type'] = $param['type'];
        }
        if (isset($param['relevance_id']) && !empty($param['relevance_id'])) {
            $where['a.relevance_id'] = $param['relevance_id'];
        }
        if (isset($param['pageSize']) && !empty($param['pageSize'])) {
            $pageSize = $param['pageSize'];
        }
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (isset($param['staff_name']) && !empty($param['staff_name'])) {
            $query = $query->where('staff_name','like','%'.$param['staff_name'].'%');
        }
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
            $query = $query->whereBetween('a.created_time', [$param['beginTime'], $param['endTime']]);
        }
        return $query->paginate($pageSize);
    }

    public static function getUserPower($data=[])
    {
        $where = self::getPowerWhere($data);

        $sql    = "select * from sline_community_user_power WHERE deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return $result;
    }


    /*
   * 插入
   */
    public static function addPower($data=[])
    {
        return DB::table('new_power')->insertGetId($data);
    }

    /*
   * 插入
   */
    public static function addUserPower($data=[])
    {
        return DB::table('user_power')->insertGetId($data);
    }

    public static function upPower($where,$data)
    {
        return DB::table('new_power')->where($where)->update($data);
    }

    public static function upUserPower($where,$data)
    {
        return DB::table('user_power')->where($where)->update($data);
    }

    public static function getDepartmentsList($data=[])
    {
        $where = self::getDepartmentWhere($data);

        $sql    = "select * from sline_community_new_department WHERE deleted_at=0 {$where} order by id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    public static function getDepartmentWhere($param = [])
    {
        $where='';

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and department_name like '%{$param["keyWord"]}%'";
        }

        if(isset($param['departmentName'])||!empty($param['departmentName']))
        {
            $where.=" and department_name = '{$param['departmentName']}'";
        }

        if(isset($param['noDepartmentId'])||!empty($param['noDepartmentId']))
        {
            $where.=" and id != {$param['noDepartmentId']}";
        }

        if(isset($param['departmentId'])||!empty($param['departmentId']))
        {
            $where.=" and id = {$param['departmentId']}";
        }


        return $where;
    }

    /**
     * 统计数据数量
     */
    public static function getDepartmentsCount($param)
    {
        $where  = self::getDepartmentWhere($param);
        $sql    = "select count(*) count from sline_community_new_department where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    public static function getDepartment($data=[])
    {
        $where = self::getDepartmentWhere($data);

        if($where)
        {
            $sql    = "select * from sline_community_new_department WHERE deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
            return $result;
        }
    }

    /*
   * 插入
   */
    public static function addDepartment($data=[])
    {
        return DB::table('new_department')->insertGetId($data);
    }

    public static function upDepartment($where,$data)
    {
        return DB::table('new_department')->where($where)->update($data);
    }



    public static function getPositionsList($data=[])
    {
        $where = self::getPositionWhere($data);

        $sql    = "select * from sline_community_new_position WHERE deleted_at=0 {$where} order by id desc limit ?, ?";
        $result = DB::select($sql, [$data["offset"], $data["display"]]);
        return $result;
    }

    public static function getPositionWhere($param = [])
    {
        $where='';

        if(isset($param['keyWord'])||!empty($param['keyWord']))
        {
            $where .= " and position_name like '%{$param["keyWord"]}%'";
        }

        if(isset($param['positionName'])||!empty($param['positionName']))
        {
            $where.=" and position_name = '{$param['positionName']}'";
        }

        if(isset($param['noPositionId'])||!empty($param['noPositionId']))
        {
            $where.=" and id != {$param['noPositionId']}";
        }

        if(isset($param['positionId'])||!empty($param['positionId']))
        {
            $where.=" and id = {$param['positionId']}";
        }

        if(isset($param['departmentId'])||!empty($param['departmentId']))
        {
            $where.=" and department_id = {$param['departmentId']}";
        }


        return $where;
    }

    /**
     * 统计数据数量
     */
    public static function getPositionsCount($param)
    {
        $where  = self::getPositionWhere($param);
        $sql    = "select count(*) count from sline_community_new_position where deleted_at=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    public static function getPosition($data=[])
    {
        $where = self::getPositionWhere($data);

        if($where)
        {
            $sql    = "select * from sline_community_new_position WHERE deleted_at=0 {$where}";
            $result = DB::selectOne($sql);
            return $result;
        }
    }

    /*
   * 插入
   */
    public static function addPosition($data=[])
    {
        return DB::table('new_position')->insertGetId($data);
    }

    public static function upPosition($where,$data)
    {
        return DB::table('new_position')->where($where)->update($data);
    }

    public static function getDepartments($data=[])
    {
        $where = self::getDepartmentWhere($data);

        $sql    = "select * from sline_community_new_department WHERE deleted_at=0 {$where}";
        $result = DB::select($sql);
        return $result;
    }



    public static function getPositions($data=[])
    {
        $where = self::getPositionWhere($data);

        $sql    = "select * from sline_community_new_position WHERE deleted_at=0 {$where}";
        $result = DB::select($sql);
        return $result;
    }







































}