<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PowerModle extends Model {
    /*
     * 查询权限列表
     */
    public static function getList()
    {
        $sql = "SELECT power_id,power_name,power_route_name,power_icon,power_pid,power_level "
            ."FROM ylh_power WHERE deleted_at is null";
        $result = self::newDb() -> select($sql);

        return !empty($result) ? $result : [];
    }

    /*
     * 获取管理员的权限
     */
    public static function adminPower($powerId = '')
    {
        $sql = "SELECT power_id,power_name,power_route_name,power_icon,power_pid,power_level "
            ."FROM ylh_power WHERE deleted_at is null and power_id in ".'('.$powerId.')';
        $result = self::newDb() -> select($sql);

        return !empty($result) ? $result : [];
    }

    /*
     * 查询权限
     *@param Array
     */
    public static function powerIdCount($param = []){
        $result = self::newDb()
            -> table('ylh_power')
            -> where('deleted_at',null)
            -> whereIn('power_id',$param)
            -> get()
            -> toArray();

        return !empty($result) ? $result : [];
    }


    public static function newDb()
    {
        return DB::connection('statistics');
    }
}