<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminPowerNodeModel extends Model{
    /*
     * 服务项目列表
     */
    public static function getServiceList()
    {
        return self::newDb()
            -> table('sline_community_service')
            -> where('is_delete',0)
            -> orderBy('sort')
            -> get()
            -> toArray();
    }





    /*
     * 服务项目删除
     */
    public static function delService($param)
    {

        $data['is_delete'] = 1;

        return self::newDb()
            -> table('sline_community_service')
            -> where('id',$param['id'])
            -> update($data);        
    }



    /*
     * 获取管理员下权限id
     */
    public static function getAdminAndPowerId($adminId)
    {
        return self::newDb()
            -> table('ylh_admin_power_node')
            -> where('deleted_at',null)
            -> where(['admin_id' => $adminId])
            -> get()
            -> toArray();
    }

    /*
     * 给管理员添加权限
     */
    public static function addPowerAdmin($param = []){
        return self::newDb()
            -> table('ylh_admin_power_node')
            -> insert($param);
    }

    /*
     * 查询管理员是否已存在该权限
     */
    public static function getAdminIsPower($adminId = '',$param = []){
        return self::newDb()
            -> table('ylh_admin_power_node')
            -> where('deleted_at',null)
            -> where('admin_id',$adminId)
            -> whereIn('power_id',$param)
            -> get()
            -> toArray();
    }

    /*
     * 批量删除  管理员的权限（修改）
     */
    public static function deleteAdminPower($adminId = '',$param = [],$time = ''){
        return self::newDb()
            -> table('ylh_admin_power_node')
            -> where('admin_id',$adminId)
            -> whereIn('power_id',$param)
            -> update(['deleted_at'=>$time,'updated_at'=>$time]);
    }

    /*
     * 查询存在过的权限
     */
    public static function formerlyPower($adminId = '',$param = []){
        return self::newDb() -> table('ylh_admin_power_node')
            -> where('deleted_at','!=',null)
            -> where('admin_id',$adminId)
            -> whereIn('power_id',$param)
            -> get()
            -> toArray();
    }

    /*
     * 将存在过数据进行恢复
     */
    public static function recoverAdminPower($adminId = '',$param = [],$time = ''){
        return self::newDb()
            -> table('ylh_admin_power_node')
            -> where('admin_id',$adminId)
            -> whereIn('power_id',$param)
            -> update(['deleted_at'=>null,'updated_at'=>$time]);
    }

    public static function newDb()
    {
        return DB::connection('robot');
    }
}