<?php


namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupModel extends Model
{
    protected $table = 'member_info_by_appid';

    const PAGE_SIZE = 15;


    public static function getGroupInfo($param) {
        $where = [
            'mid' => $param['memberId']
        ];
        $fields = ['id', ];
        $query = self::select(['id', 'mid', 'action'])->where($where);
        //查询开始时间
        $beginTime = $param['beginTime'] ?? '2020-04-07 11:44:33';
        $endTime = $param['endTime'] ?? '';
        if ($beginTime) {
            $query->where('created_at', '>=', $beginTime);
        }
        if ($endTime) {
            $query->where('created_at', '>=', $endTime);
        }
        return $query->paginate(self::PAGE_SIZE)->toArray();
    }


    public static function fetchGroupInfo($where) {
        $query = self::select(['id', 'wxid', 'room_wxid'])->where($where);

        return $query->first()->toArray();
    }


    public static function fetchGroupInfoByWhere($where) {
        $query = self::where($where);

        $res = $query->first();
        return $res ? $res->toArray() : [];
    }

    /*
     * 获取当前用户的机器人总量
     */
    public static function getRobotTotalNum($memberId = '') {
        $where = [];
        if ($memberId) {
            $where['mid'] = $memberId;
        }
        return DB::connection('robot')->table('buy_info')
            ->where($where)
            ->where('duetime_at', '>', time())->sum('service_number');
    }

    /*
     * 获取绑定微信机器人总量
     */
    public static function getRobotBindNum($memberId = '') {
        $where = [
            //是否激活群
            'is_activation' => 1
        ];
        if ($memberId) {
            $where['mid'] = $memberId;
        }
        return DB::connection('robot')->table('member_info_by_appid')
            ->where($where)
            ->count();
    }

    /*
     * 获取消息数量
     */
    public static function getMessageNum($condition = [], $total = '')
    {
        //$query = DB::connection('robot')->table('send_info')->leftJoin('send_reload', 'send_info.random_str',
          //  '=', 'send_reload.random_str');
        $query = DB::connection('robot')->table('send_room_reload');
        $query = $query->whereIn('room_id', $condition['roomId']);
        if (!$total) {
            $param['beginTime'] = $param['beginTime'] ?? strtotime('yesterday');
            $param['endTime'] = $param['endTime'] ?? strtotime('today');
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        return $query->count();
    }

    /*
    * 获取群的用户总数
    */
    public static function getGroupMemberTotalNum($condition = []) {
        $query = DB::connection('robot')->table('member_weixin_info');
        if ($condition['user_type'] == 1) {
            $query = $query->where(['mid' => $condition['mid']]);
        } else {
            $query = $query->whereIn('wxid', $condition['wxIds']);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $beginTime = date('Y-m-d 00:00:00', $condition['beginTime']);
            $endTime = date('Y-m-d 00:00:00', $condition['endTime']);
            $query = $query->whereBetween('created_at', [$beginTime, $endTime]);
        }
        return $query->count();
    }

    /*
    * 获取订单交易额
    */
    public static function getOrderTotalDate($condition = [], $total = '', $format = 'day') {
        $condition['beginTime'] = date('Y-m-d H:i:s', $condition['beginTime']);
        $condition['endTime'] = date('Y-m-d H:i:s', $condition['endTime']);
        $condition['total'] = $total;
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
        $where = self::getOrderTotalWhere($condition);
        $sql = "select FROM_UNIXTIME(o.created_at, '{$format}') as date,TRUNCATE(sum(actual_price)/100, 2) as value";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id";
        $sql .= "  where {$where} group by date";
        return DB::select($sql);
    }

    /*
    * 获取订单订单数量
    */
    public static function getPlantFormNameByRoomId($roomId) {

        $sql = "select r.platform_identity";
        $sql .= " from ";
        $sql .= " ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " left join ylh_robot.sline_community_white_list_wx as r";
        $sql .= " on g.wxid = r.wxid";
        $sql .= "  where g.id = '{$roomId}'";

        $result = DB::selectOne($sql);
        return $result ? $result->platform_identity : '';
    }


    /*
    * 获取订单订单数量
    */
    public static function getPlantFormNameByGroupAdminMid($mid) {

        $sql = "select r.platform_identity";
        $sql .= " from ";
        $sql .= " ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " left join ylh_robot.sline_community_white_list_wx as r";
        $sql .= " on g.wxid = r.wxid";
        $sql .= "  where g.mid = '{$mid}'";

        $result = DB::selectOne($sql);
        return $result ? $result->platform_identity : '';
    }


    /*
    * 获取订单订单数量
    */
    public static function getOrderNumTotalDate($condition = [], $total = '', $format = 'day') {
        $condition['beginTime'] = date('Y-m-d H:i:s', $condition['beginTime']);
        $condition['endTime'] = date('Y-m-d H:i:s', $condition['endTime']);
        $condition['total'] = $total;
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
        $where = self::getOrderTotalWhere($condition);
        $sql = "select FROM_UNIXTIME(o.created_at, '{$format}') as date,count(1) as value";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id";
        $sql .= "  where {$where} group by date";

        return DB::select($sql);
    }




    /*
     * 获取用户订单总数
     */
    public static function getOrderTotalNum($condition = [], $total = '') {
        $condition['total'] = $total;
        $where = self::getOrderTotalWhere($condition);
        $sql = "select count(1) as cnt";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }


    /*
     * 获取用户订单总数
     */
    public static function getOrderMemberTotalNum($condition = [], $total = '') {
        $condition['total'] = $total;
        $where = self::getOrderTotalWhere($condition);
        $sql = "select count(distinct(member_id)) as cnt";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id";
        $sql .= "  where {$where}";
        // echo $sql;
        // die;
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }

    public static function getOrderTotalWhere($condition)
    {
        //$where='o.pay_status = 1 and g.is_delete = 0';
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                // 订单生成在哪个平台就属于哪个平台，不需要在找群的机器人是哪个平台
                //$where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
                $where .= '1=1';
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.channel = {$condition['channel']}";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime']) && $condition['total'] != 'total'){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "o.pay_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['roomId'])  && !empty($condition['roomId']) && is_numeric($condition['roomId'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['roomId']}";
        }

        if(isset($condition['groupAdminMid'])  && !empty($condition['groupAdminMid'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['groupAdminMid']}";
        }
        if(isset($condition['good_type'])  && !empty($condition['good_type'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.good_type = {$condition['good_type']}";
        }
        return $where;
    }


    /*
     * 获取用户订单总数
     */
    public static function getOrderNumChannelByGroupAdminId($condition = []) {
        $where = self::getOrderNumChannelByGroupAdminIdWhere($condition);
        $sql = "select TRUNCATE(sum(actual_price)/100, 2) as cnt";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id and g.mid > 0";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return $count ? $count->cnt ?: 0 : 0;
    }

    public static function getOrderNumChannelByGroupAdminIdWhere($condition)
    {
        //$where='o.pay_status = 1 and g.is_delete = 0';
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.channel = {$condition['channel']}";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "o.pay_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['room_id']}";
        }
        if(isset($condition['groupAdminMid'])  && !empty($condition['groupAdminMid'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['groupAdminMid']}";
        }
        if(isset($condition['good_type'])  && !empty($condition['good_type'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.good_type = {$condition['good_type']}";
        }
        return $where;
    }

    /*
     * 获取用户订单总数
     */
    public static function getOrderNumChannelByRoomId($condition = []) {
        $where = self::getOrderNumTopDataWhere($condition);
        $sql = "select TRUNCATE(sum(actual_price)/100, 2) as cnt";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id and g.mid > 0";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return $count ? $count->cnt ?: 0 : 0;
    }

    public static function getOrderNumChannelByRoomIdWhere($condition)
    {
        //$where='o.pay_status = 1 and g.is_delete = 0';
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.channel = {$condition['channel']}";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "o.pay_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['room_id']}";
        }
        if(isset($condition['good_type'])  && !empty($condition['good_type'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.good_type = {$condition['good_type']}";
        }
        return $where;
    }


    /*
     * 获取用户订单总数
     */
    public static function getOrderNumTopData($condition = []) {
        $where = self::getOrderNumTopDataWhere($condition);
        $groupBy = self::getGroupByByCondition($condition);
        //$sql = "select count(1) as num,g.mid,group_concat(g.id) as roomIds,";
        if (isset($condition['groupBy']) && !empty($condition['groupBy'])) {
            $sql = "select TRUNCATE(sum(actual_price)/100, 2) as num,g.id,";
        } else {
            $sql = "select TRUNCATE(sum(actual_price)/100, 2) as num,g.mid,";
        }
        $sql .= "CASE when g.nick_name is not null THEN g.nick_name when u.nickname is not null THEN u.nickname else u.truename END as name";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id and g.mid > 0";
        $sql .= " left join yuelvhui.sline_member as u";
        $sql .= " on g.mid = u.mid";
        $sql .= "  where {$where} {$groupBy} order by num desc limit 30";
        $count = DB::select($sql);
        return $count;
    }



    public static function getOrderNumTopDataWhere($condition)
    {
        //$where='o.pay_status = 1 and g.is_delete = 0';
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.channel = {$condition['channel']}";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "o.pay_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['room_id']}";
        }
        if(isset($condition['good_type'])  && !empty($condition['good_type'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.good_type = {$condition['good_type']}";
        }
        return $where;
    }



    /*
     * 获取用户订单总数
     */
    public static function getGroupMemberNumTopData($condition = []) {
        $where = self::getGroupMemberNumTopDataWhere($condition);
        $groupBy = self::getGroupByByCondition($condition);
        //$sql = "select sum(DISTINCT(g.member_count)) as num,g.mid,group_concat(g.id) as roomIds,";
        if (isset($condition['groupBy']) && !empty($condition['groupBy'])) {
            $sql = "select sum(member_count) as num,g.id,";
        } else {
            $sql = "select sum(member_count) as num,g.mid,";
        }
        $sql .= "CASE when g.nick_name is not null THEN g.nick_name when u.nickname is not null THEN u.nickname else u.truename END as name";
        $sql .= " from ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " left join yuelvhui.sline_member as u";
        $sql .= " on g.mid = u.mid";
        $sql .= "  where {$where} {$groupBy} order by num desc limit 30";
        $count = DB::select($sql);
        return $count;
    }



    public static function getGroupMemberNumTopDataWhere($condition)
    {
        $where='g.is_delete = 0 and g.mid > 0';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= " g.wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= " g.wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= " g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= " g.wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx  WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $where .= "g.created_at between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.id = {$condition['room_id']}";
        }

        return $where;
    }




    /*
     * 获取用户订单总数
     */
    public static function getOrderWeekNum($condition = [], $total = '') {
        $condition['total'] = $total;
        $where = self::getOrderWeekNumWhere($condition);
        $sql = "select count(1) as cnt";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " right join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }


    public static function getOrderWeekNumWhere($condition)
    {
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }


        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['room_id']}";
        }

        $where = $where ? $where . ' and ' : $where;
        $beginTime = date("Y-m-d",strtotime("-1 week")) . ' 00:00:00';
        $endTime = date("Y-m-d",strtotime("-6 day")) . ' 00:00:00';
        $where .= "g.bind_mid_time between '{$beginTime}' and '{$endTime}'";
        $where = $where ? $where . ' and ' : $where;
        $where .= "g.is_delete = 0";
        return $where;
    }

    /*
     * 获取用户订单总数
     */
    public static function getOrderTotalAmount($condition = [], $total = '') {
        $condition['total'] = $total;
        $where = self::getOrderTotalAmountWhere($condition);
        $sql = "select sum(actual_price) as actual_price";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id";
        $sql .= "  where {$where}";

        $count = DB::selectOne($sql);
        return !empty($count) ? $count->actual_price : 0;
    }

    public static function getOrderTotalAmountWhere($condition)
    {
        //$where='o.pay_status = 1 and g.is_delete = 0';
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.channel = {$condition['channel']}";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime']) && $condition['total'] != 'total'){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "o.pay_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['room_id']}";
        }
        if(isset($condition['good_type'])  && !empty($condition['good_type'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.good_type = {$condition['good_type']}";
        }
        return $where;
    }



    /*
     * 获取用户订单总数
     */
    public static function getOrderGMVTopData($condition = []) {
        $where = self::getOrderGMVTopDataWhere($condition);
        $groupBy = self::getGroupByByCondition($condition);
        //$sql = "select TRUNCATE(sum(actual_price)/100, 2) as orderGMV,g.mid,u.nickname,u.name";
        //$sql = "select sum(actual_price) as num,g.mid,group_concat(g.id) as roomIds,";
        if (isset($condition['groupBy']) && !empty($condition['groupBy'])) {
            $sql = "select TRUNCATE(sum(actual_price)/100, 2) as num,g.id,";
        } else {
            $sql = "select TRUNCATE(sum(actual_price)/100, 2) as num,g.mid,";
        }
        $sql .= "CASE when g.nick_name is not null THEN g.nick_name when u.nickname is not null THEN u.nickname else u.truename END as name";
        $sql .= " from ".config('community.oldMallOrderGoods')." as o";
        $sql .= " left join ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " on o.room_id = g.id and g.mid > 0";
        $sql .= " left join yuelvhui.sline_member as u";
        $sql .= " on g.mid = u.mid";
        $sql .= "  where {$where} {$groupBy} order by num desc limit 30";
        return DB::select($sql);
    }


    public static function getGroupByByCondition($condition)
    {
        if (isset($condition['groupBy']) && !empty($condition['groupBy'])) {
            return " group by g." . $condition['groupBy'] . " ";
        } else {
            return " group by g.mid ";
        }
    }

    public static function getOrderGMVTopDataWhere($condition)
    {
        //$where='o.pay_status = 1 and g.is_delete = 0';
        $where='o.pay_status = 1';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.channel = {$condition['channel']}";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "o.pay_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.room_id = {$condition['room_id']}";
        }
        if(isset($condition['good_type'])  && !empty($condition['good_type'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.good_type = {$condition['good_type']}";
        }
        return $where;
    }


    /*
     * 获取总社群数量
     */
    public static function getGroupTotalNum($condition = [], $total = '') {
        $condition['total'] = $total;
        $where = self::getGroupTotalNumWhere($condition);
        $sql = "select count(1) as cnt";
        $sql .= " from ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }



    public static function getGroupTotalNumWhere($condition)
    {
        $where='';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where = $where ? $where . ' and ' : $where;
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['beginTime']) && isset($condition['endTime']) && $condition['total'] != 'total'){
            $where = $where ? $where . ' and ' : $where;
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $where .= "g.bind_mid_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }
        if(isset($condition['limitNum']) && $condition['limitNum']>0){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.member_count >= '{$condition['limitNum']}'";
        }
        $where = $where ? $where . ' and ' : $where;
        $where .= "g.is_delete = 0";
        return $where;
    }



    /*
    * 获取总社群数量
    */
    public static function getGroupTotalRemoveNum($condition = [], $total = '') {

        $condition['total'] = $total;
        $where = self::getGroupTotalRemoveNumWhere($condition);
        $sql = "select count(1) as cnt";
        $sql .= " from ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }


    public static function getGroupTotalRemoveNumWhere($condition)
    {
        $where='';
        $where = $where ? $where . ' and ' : $where;
        if($condition['sup_id']>0){
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } else {
                $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx)";
            }
        }
        if(isset($condition['beginTime']) && isset($condition['endTime']) && $condition['total'] != 'total'){
            $where = $where ? $where . ' and ' : $where;
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $where .= "g.bind_mid_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }
        $where = $where ? $where . ' and ' : $where;
        $where .= "g.is_delete = 1";
        return $where;
    }

    /*
    * 获取群的新增用户数量
    */
    public static function getNewGroupTotalNum($condition = [], $total='') {
        $condition['total'] = $total;
        $where = self::getNewGroupTotalNumWhere($condition);
        $sql = "select count(distinct `wxid`) as cnt";
        $sql .= " from ylh_robot.sline_community_member_weixin_info as info";
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.member_id = info.mid and o.pay_status=1 and o.room_id>0";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }



    public static function getNewGroupTotalNumWhere($condition)
    {
        $where='info.is_delete = 0';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
            $where .= " wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']}))";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
                $where .= " wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']}))";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
                $where .= " wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}'))";
            } else {
                $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
                $where .= " wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx  WHERE `platform_identity` <> ''))";
            }
        }
        if(isset($condition['beginTime']) && isset($condition['endTime']) && $condition['total'] != 'total'){
            $where = $where ? $where . ' and ' : $where;
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $where .= "info.created_at between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }

        if(isset($condition['room_id'])  && !empty($condition['room_id'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "info.romm_id = {$condition['room_id']}";
        }
        if(isset($condition['sex'])  && !empty($condition['sex'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "info.sex = {$condition['sex']}";
        }
        if(isset($condition['haveOrder'])  && !empty($condition['haveOrder'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "o.member_id > 0";
        }

        return $where;
    }



    /*
    * 获取群的新增用户数量
    */
    public static function getPassGroupTotalNum($condition = [], $total='') {
        $condition['total'] = $total;
        $where = self::getPassGroupTotalNumWhere($condition);
        $sql = "select count(1) as cnt";
        $sql .= " from ylh_robot.sline_community_invitation_group as inv";
        $sql .= "  where {$where}";
        $count = DB::selectOne($sql);
        return !empty($count) ? $count->cnt : 0;
    }


    public static function getPassGroupTotalNumWhere($condition)
    {
        $where="inv.examine_status = 2 AND inv.platform_identity = '{$condition['platform_identity']}'";
        if($condition['user_type']>=2 && $condition['sup_id']>0){
            $where = $where ? $where . ' and ' : $where;
            $where .= "inv.assistant_wx in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }
        if(isset($condition['beginTime']) && isset($condition['endTime']) && $condition['total'] != 'total'){
            $where = $where ? $where . ' and ' : $where;
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where .= "inv.examine_time between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }
        return $where;
    }

    /*
     * 获取用户订单总数
     */
    public static function getWxIds() {
        $query = DB::connection('robot')->table('white_list_wx')->select('wxid');
        return $query->get();
    }


    /*
     * 获取机器人的平台标识
     */
    public static function getPlatformIdentity($wxid) {
        $where = ['wxid' => $wxid];
        $query = DB::connection('robot')->table('white_list_wx');
        return $query->where($where)->value('platform_identity');
    }

    /*
     * 获取用户订单总数
     */
    public static function getRoomIds($roomIds) {
        $query = DB::connection('robot')->table('member_info_by_appid')->select('id')->whereIn('wxid', $roomIds);
        return $query->get();
    }

    /*
     * 获取用户订单总数
     */
    public static function getRobotInfo($pageSize) {
        $fields = ['id', 'wxid', 'wxid_type'];
        $query = DB::connection('robot')->table('white_list_wx')->select($fields);
        return $query->paginate($pageSize)->toArray();
    }

    /*
     * 获取用户订单总数
     */
    public static function getRoomIdsByMid($param, $platform = '') {
        $where = [
            'member_info_by_appid.mid' => $param['mid']
        ];
        $fields = ['member_info_by_appid.id', 'member_info_by_appid.wxid', 'member_info_by_appid.room_wxid'];
        $query = DB::connection('robot')->table('member_info_by_appid');
        if (!empty($platform)){
            $where['white_list_wx.platform'] = $platform;
            $query->leftJoin('white_list_wx','member_info_by_appid.wxid','=','white_list_wx.wxid');
        }
        $query->select($fields)->where($where);
        $data = $query->get();
        return $data;
    }

    /*
    * 获取用户订单总数
    */
    public static function getRoomWxAndIdsByWxId($param) {
        $query = DB::connection('robot')->table('member_info_by_appid')->where('mid','>',0)
        ->whereIn('wxid', $param['wxIds']);
        return $query->pluck('room_wxid', 'id')->toArray();
    }

    /*
     * 获取用户订单总数
     */
    public static function getRoomWxIdsByWxId($param) {
        $query = DB::connection('robot')->table('member_info_by_appid')->where('mid','>',0)
            ->whereIn('wxid', $param['wxIds']);
        return $query->pluck('room_wxid');
    }

    /*
 * 获取用户订单总数
 */
    public static function getRoomIdsByWxId($param) {
        $query = DB::connection('robot')->table('member_info_by_appid')->where('mid','>',0);
        if (isset($param['platform']) && $param['platform'] == 2) {
            $query = $query->whereIn('enterprise_robot_serial_no', $param['wxIds']);
        } else {
            $query = $query->whereIn('wxid', $param['wxIds']);
        }
        return $query->pluck('id');
    }

    /*
     * 群标识
     */
    public static function getRobotWxIds($param = [],$type=1 , $platform = '') {
        $where = [];
        if($param['sup_id']>0){
            $where['bind_mid'] = $param['mid'];
        }else{
            if (isset($param['mid']) && !empty($param['mid'])) {
                $where['mid'] = $param['mid'];
            }
        }
        if (isset($param['platform']) && $param['platform'] !== '' && $param['platform'] !== null) {
            $where['platform'] =  $platform;
        }
        $where['deleted_at'] = 0;
        // $query = DB::connection('robot')->table('white_list_wx')->where('wxid_type',$type)
        //     ->where($where);
        $query = DB::connection('robot')->table('white_list_wx')->where($where);

        if (isset($where['platform']) && $where['platform'] == 2) {
            //微信机器人编号
            return $query->pluck('enterprise_robot_serial_no');
        } else {
            return $query->pluck('wxid');
        }
    }



    public static function getAllRobotWxIds($param = []) {
        $where = [];
        if($param['sup_id']>0){
            $where['bind_mid'] = $param['mid'];
        }else{
            if (isset($param['mid']) && !empty($param['mid'])) {
                $where['mid'] = $param['mid'];
            }
        }
        $where['deleted_at'] = 0;
        $query = DB::connection('robot')->table('white_list_wx')
            ->where($where);
        return $query->pluck('wxid');
    }

    /*
     * 获取用户订单总数
     */
    public static function getGroupOwnNum($param, $total = '') {

        $wxids = '';
        if(!empty($param['wxIds'])){
            $param['wxIds'] = json_decode(json_encode($param['wxIds']), true);
            $param['wxIds']=array_filter($param['wxIds']);
            $wxids = "'".implode("','", $param['wxIds'])."'";
        }
        if(!empty($wxids)){
            $where = '';
            if (isset($param['beginTime']) && isset($param['endTime']) && $total != 'total') {
                if (is_numeric($param['beginTime']) && is_numeric($param['endTime'])) {
                    $param['beginTime'] = date('Y-m-d 00:00:00', $param['beginTime']);
                    $param['endTime'] = date('Y-m-d 23:59:59', $param['endTime']);
                }
                $where = " and created_at between '{$param['beginTime']}' and '{$param['endTime']}' ";
            }else{
                if($total!='total'){
                    $beginTime = date('Y-m-d');
                    $where = " and created_at >= {$beginTime} ";
                }

            }
            if(isset($param['room_id'])  && !empty($param['room_id'])){

                $where = " and id = {$param['room_id']}";
            }
            $sql  = "select  count(1) as countNum,mid  from  sline_community_member_info_by_appid  where wxid in ({$wxids}) {$where} group by mid ";
            $returnData  = DB::connection('robot')->select($sql);
            return !empty($returnData) ? count($returnData) : 0;
        }else{
            return 0;
        }

    }


    /**
    * 获取（360人）群数量
    *
    */
    public static function getVipGroupNum($param,$total=''){

        $where = "";

        if($total!='total'){

            $where = "is_delete = 0";

            if (isset($param['beginTime']) && isset($param['endTime'])) {
                $param['beginTime'] = strtotime($param['beginTime']);
                $param['endTime'] = strtotime($param['endTime']);
                $where .= " and created_time between '{$param['beginTime']}' and '{$param['endTime']}' ";
            }

            if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

                $where .= " and platform_identity='{$param['platform_identity']}'";
            }

            $sql = "select count(*) as count_1 from sline_community_vip_group_log where {$where}";

        }else{

            $where = " g.is_delete=0 and member_count>359";


            if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

                $where .= " and w.platform_identity='{$param['platform_identity']}'";
            }

            $sql = "select count(*) as count_1 from sline_community_member_info_by_appid g ";
            $sql.= " left join sline_community_white_list_wx w on w.wxid=g.wxid";
            $sql.= " where {$where}";

        }

        $result = DB::connection('robot')->selectOne($sql);

        return !empty($result)?$result->count_1:0;

    }

    /*
     * 获取群主下级总数
     */
    public static function getRegisterNum($param, $total = '') {

        if(!isset($param['platform_identity'])||$param['platform_identity']!='7323ff1a23f0bf1cda41f690d4089353'){

            return 0;
        }

        $where1 = "";

        $wxids = '';
        if(!empty($param['wxIds'])){
            $param['wxIds'] = json_decode(json_encode($param['wxIds']), true);
            $param['wxIds']=array_filter($param['wxIds']);
            $wxids = "'".implode("','", $param['wxIds'])."'";

            $where1 = " and w.wxid in ({$wxids})";
        }


        $where = "";

        if($total!='total'){
            if (isset($param['beginTime']) && isset($param['endTime'])) {
                $param['beginTime'] = strtotime($param['beginTime']);
                $param['endTime'] = strtotime($param['endTime']);
                $where = " and jointime between '{$param['beginTime']}' and '{$param['endTime']}' ";
            }

        }

        $sql = "select count(*) as num from yuelvhui.sline_member m ";
        $sql.= " left join (";
        $sql.= " select g.mid from sline_community_member_info_by_appid g";
        $sql.= " left join  sline_community_white_list_wx w on w.wxid = g.wxid";
        $sql.= " where w.platform_identity = '7323ff1a23f0bf1cda41f690d4089353' and g.mid>0 {$where1}  GROUP BY mid) u on m.parent_id = u.mid";
        $sql.= " where u.mid>0 and u.mid!=67 {$where} ";

        $returnData  = DB::connection('robot')->selectOne($sql);
        return !empty($returnData) ? $returnData->num : 0;

    }



    /*
     * 获取已绑定的机器人数量
     */
    public static function getBindRobotNum($param) {
//        $query = DB::connection('robot')->table('member_info_by_appid')->select('wxid')
//            ->whereIn('wxid', $param['wxIds']);
//        return $query->distinct('wxid')->count() ?: 0;

         $wxids = '';
         if(!empty($param['wxIds'])){
             foreach($param['wxIds'] as $v){
                 $wxids.= "'".$v."',";
             }
         }
         if(!empty($wxids)){
             $sql  = 'select  count(1) as countNum,wxid  from  sline_community_member_info_by_appid  where wxid in ('.trim($wxids,',').')  group by wxid ';
             $returnData  = DB::connection('robot')->select($sql);
             return !empty($returnData) ? count($returnData) : 0;
         }else{
             return 0;
         }

    }

    /*
     * 获取用户订单总数
     */
    public static function getOrderTotalNumGroupDate($condition = [], $format = 'day') {
        $where = [
            'pay_status' => 1
        ];
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods')->where($where)
        $query = DB::connection('robot')->table('mall_order_goods')->where($where)
            ->whereIn('room_id', $condition['roomId']);
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $query = $query->where(['channel' => $condition['channel']]);
        }
        return $query->selectRaw("DATE_FORMAT(FROM_UNIXTIME(created_at), '{$format}') as date,COUNT(*) as value")
            ->groupBy('date')->get()->toArray();
    }

    /*
     * 消息数量
     */
    public static function getMessageTotalNumGroupDate($condition = [], $format = 'day') {
        $where = [
            'is_send' => 1
        ];
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods')->where($where)
//        DB::connection('robot')->enableQueryLog();
        $query = DB::connection('robot')->table('report_msg')->where($where)
            ->whereIn('room_wxid', $condition['roomWxId']);
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        return  $query->selectRaw("DATE_FORMAT(created_at, '{$format}') as date,COUNT(*) as value")
            ->groupBy('date')->get()->toArray();

//        $queries = DB::connection('robot')->getQueryLog();
//        var_dump($queries);die();
    }


    /*
     * 素材条数
     */
    public static function getFodderTotalNumGroupDate($condition = [], $format = 'day') {
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods')->where($where)
        $query = DB::connection('robot')->table('send_room_reload')
            ->whereIn('room_wxid', $condition['roomWxId']);
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        return $query->selectRaw("DATE_FORMAT(created_at, '{$format}') as date,COUNT(*) as value")
            ->groupBy('date')->get()->toArray();
    }


    /*
     * 获取用户订单总数
     */
    public static function getClickTotalNumGroupDate($condition = [], $format = 'day') {
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods')->where($where)
        $query = DB::connection('robot')->table('room_activation')
            ->whereIn('romm_id', $condition['roomId']);
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        return $query->selectRaw("DATE_FORMAT(created_at, '{$format}') as date,COUNT(*) as value")
            ->groupBy('date')->get()->toArray();
    }


    /*
    * 获取用户订单总数
    */
    public static function orderGroupNum($condition = [], $begin = 0, $pageSize = 15) {
        $where = self::getOrderGroupWhere($condition);
        $sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(created_at), '%Y-%m-%d') as date, DATE_FORMAT(FROM_UNIXTIME(created_at), '%H') as hour, COUNT(distinct(room_id)) as value, group_concat(room_id) as roomIds";
//        $sql .= " FROM `sline_mall_order_goods` where {$where} group by date,hour order by date desc, hour desc limit $begin, $pageSize";
//        $sql .= " FROM `sline_community_mall_order_goods` where {$where} group by date,hour order by date desc, hour desc limit $begin, $pageSize";
        $sql .= " FROM " .config('community.oldMallOrderGoods')." where {$where} group by date,hour order by date desc, hour desc limit $begin, $pageSize";
//        return DB::connection('mysql_yuelvhui')->select($sql);
        return DB::connection('robot')->select($sql);
    }


    /*
    * 获取用户订单总数
    */
    public static function orderGroupNumCount($condition = []) {
        $where = self::getOrderGroupWhere($condition);
        $sql = "SELECT count(*) as cnt from (SELECT DATE_FORMAT(FROM_UNIXTIME(created_at), '%Y-%m-%d') as date, DATE_FORMAT(FROM_UNIXTIME(created_at), '%H') as hour, COUNT(distinct(room_id)) as value";
//        $sql .= " FROM `sline_mall_order_goods` where {$where} group by date,hour) as a";
//        $sql .= " FROM `sline_community_mall_order_goods` where {$where} group by date,hour) as a";
        $sql .= " FROM ".config('community.oldMallOrderGoods')." where {$where} group by date,hour) as a";
//        return DB::connection('mysql_yuelvhui')->select($sql);
        return DB::connection('robot')->select($sql);
    }

    public static function getOrderGroupWhere($condition)
    {
        $where='pay_status = 1 and room_id <> 0';

        if (isset($condition['platform']) && $condition['platform'] !== '' && $condition['platform'] !== null){
            $where.=" and room_id in ('" . implode("','" ,$condition['roomId']) . "')";
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $where.=" and created_at between {$condition['beginTime']} and {$condition['endTime']}";
        }
        return $where;
    }

    public static function getGroupListOrderWhere($condition)
    {
        $where='1=1 ';
        if (isset($condition['timeType'])&&$condition['timeType'] == 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d 00:00:00', $condition['beginTime']));
                $end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                $where .= "pay_time between {$begin} and {$end}";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d 00:00:00', $condition['beginTime']));
                $where .= "pay_time >= {$begin}";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                $where .= "pay_time <= {$end}";
            }
        }
        return $where;
    }

    public static function getGroupListOrderWhereV2($condition)
    {
        $where='1=1 ';
        if (isset($condition['timeType'])&&$condition['timeType'] == 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d 00:00:00', $condition['beginTime']));
                $end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                $where .= "pay_time between {$begin} and {$end}";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d 00:00:00', $condition['beginTime']));
                $where .= "pay_time >= {$begin}";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                $where .= "pay_time <= {$end}";
            }
        }
        return $where;
    }

    public static function getGroupListMsgWhere($condition)
    {
        $where = '';
//        if (isset($condition['timeType'])&&$condition['timeType'] == 'group') {
//            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
//                $where = $where ? $where . ' and ' : $where;
//                $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
//                $end = date('Y-m-d 23:59:59', $condition['endTime']);
//                $where .= "created_at between '{$begin}' and '{$end}'";
//            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
//                $where = $where ? $where . ' and ' : $where;
//                $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
//                $where .= "created_at >= '{$begin}'";
//            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
//                $where = $where ? $where . ' and ' : $where;
//                $end = date('Y-m-d 23:59:59', $condition['endTime']);
//                $where .= "created_at <= '{$end}'";
//            }
//        }
        return $where ? " WHERE {$where}" : $where;
    }

    public static function getGroupListWhere($condition)
    {
        $where='g.is_delete = 0 ';
        //$where='1=1 ';
        if(!empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            //$where .= "g.name like '%{$condition['name']}%'";
            $where .= '(g.name like "%'.$condition['name'].'%"  or  g.name like "'.$condition['name'].'%")';
        }
        if(isset($condition['tag_id'])&&count($condition['tag_id'])){
            $where = $where ? $where . ' and ' : $where;
            if (is_array($condition['tag_id'])) {
                $tagId = implode(',', $condition['tag_id']);
            } else {
                $tagId = $condition['tag_id'];
            }
            $where .= "g.tag_id in ({$tagId})";
        }

        if(isset($condition['roomId'])&&!empty($condition['roomId'])&&count($condition['roomId'])){
            $where = $where ? $where . ' and ' : $where;
            $roomId = implode(',', $condition['roomId']);
            $where .= "g.id in ({$roomId})";
        }
        if (isset($condition['timeType'])&&$condition['timeType'] == 'group') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $end = date('Y-m-d 23:59:59', $condition['endTime']);
                $where .= "g.created_at between '{$begin}' and '{$end}'";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $where .= "g.created_at >= '{$begin}'";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $end = date('Y-m-d 23:59:59', $condition['endTime']);
                $where .= "g.created_at <= '{$end}'";
            }
        }
        if (isset($condition['user_type']) && $condition['user_type'] >= 2) {
            if(!empty($condition['wx_alias'])){
                $where = $where ? $where . ' and ' : $where;
                $where .= "r.wx_alias = '{$condition['wx_alias']}'";
            }else{
                $where = $where ? $where . ' and ' : $where;
                if(isset($condition['sup_id']) && $condition['sup_id']>0){
                    $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']}";
                }else{
                    if (isset($condition['mid']) && !empty($condition['mid'])) {
                        $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']}";
                    } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                        $where .= " g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
                    } else {
                        $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> ''";
                    }
                }
                if (isset($condition['exclusive']) && $condition['exclusive'] == 1) {
                    $where .= " AND wxid_type = 3";
                }
                $where .= " )";
            }
        } else {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
            if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
                $where = $where ? $where . ' and ' : $where;
                $where .= "g.mid = {$condition['searchMid']}";
            } else {
                if (isset($condition['mid']) && !empty($condition['mid'])) {
                    $where = $where ? $where . ' and ' : $where;
                    $where .= "g.mid = {$condition['mid']}";
                }
            }
        }
        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;

            $searchMid = is_array($condition['searchMid']) ? implode("','", $condition['searchMid']) : $condition['searchMid'];
            $where .= "g.mid in ('{$searchMid}')";
        }
        if(!empty($condition['isBingGroupLeader']) && isset($condition['isBingGroupLeader'])){
            $where = $where ? $where . ' and ' : $where;
            if($condition['isBingGroupLeader'] == 1){
                $where .= 'g.mid > 0';
            }else{
                $where .= 'g.mid = 0';
            }
        }
        if(!empty($condition['groupMid']) && isset($condition['groupMid'])){
            $where = $where ? $where . ' and ' : $where;
            //$where .= "g.name like '%{$condition['name']}%'";
            $where .= "g.mid = ".$condition['groupMid']."";
        }
        return $where;
    }



    public static function getYCloudGroupListWhere($condition)
    {
        $where='1=1 ';
        if(isset($condition['name']) && !empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " g.name like '%{$condition['name']}%'";
            //$where .= '(g.name like "%'.$condition['name'].'%"  or  g.name like "'.$condition['name'].'%")';
        }

        if(isset($condition['roomId']) && !empty($condition['roomId'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " g.id = {$condition['roomId']}";
            //$where .= '(g.name like "%'.$condition['name'].'%"  or  g.name like "'.$condition['name'].'%")';
        }

        if(isset($condition['groupLittleNum']) && !empty($condition['groupLittleNum'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " g.member_count > {$condition['groupLittleNum']}";
        }
        if(isset($condition['groupBigNum']) && !empty($condition['groupBigNum'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " g.member_count < {$condition['groupBigNum']}";
        }

        $where = $where ? $where . ' and ' : $where;
        $where .= " g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE";
        $where .= " `platform_identity` = '{$condition['platform_identity']}'";
        if(isset($condition['userId']) && $condition['userId']>0){
            $where .= " AND exclusive_mid = {$condition['userId']}";
        }
        $where .= " )";

        if(isset($condition['userId']) && $condition['userId']>0){
            $where = $where ? $where . ' AND ' : $where;
            $where .= " r.exclusive_mid = {$condition['userId']}";
        }
        if(isset($condition['groupAdminId']) && !empty($condition['groupAdminId'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = ".$condition['groupAdminId']."";
        }
        return $where;
    }


    public static function getRoomIdsByConditionWhere($condition)
    {
        $where='g.is_delete = 0 ';
        if(isset($condition['name']) && !empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " g.name like '%{$condition['name']}%'";
            //$where .= '(g.name like "%'.$condition['name'].'%"  or  g.name like "'.$condition['name'].'%")';
        }

        $where = $where ? $where . ' and ' : $where;
        $where .= " g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE";
        $where .= " `platform_identity` = '{$condition['platform_identity']}'";
        if(isset($condition['userId']) && $condition['userId']>0){
            $where .= " AND exclusive_mid = {$condition['userId']}";
        }
        if(isset($condition['robotId']) && $condition['robotId']>0){
            $where .= " AND id = {$condition['robotId']}";
        }
        $where .= " )";

        if(isset($condition['groupAdminId']) && !empty($condition['groupAdminId'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = ".$condition['groupAdminId']."";
        }
        return $where;
    }



    public static function getGroupListWhereV2($condition)
    {
        $where='g.is_delete = 0 ';
        //$where='1=1 ';
        if(!empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            //$where .= "g.name like '%{$condition['name']}%'";
            $where .= '(g.name like "%'.$condition['name'].'%"  or  g.name like "'.$condition['name'].'%")';
        }
        if(isset($condition['tag_id'])&&count($condition['tag_id'])){
            $where = $where ? $where . ' and ' : $where;
            if (is_array($condition['tag_id'])) {
                $tagId = implode(',', $condition['tag_id']);
            } else {
                $tagId = $condition['tag_id'];
            }
            $where .= "g.tag_id in ({$tagId})";
        }

        if(isset($condition['roomId'])&&!empty($condition['roomId'])){
            $where = $where ? $where . ' and ' : $where;
            //$roomId = implode(',', $condition['roomId']);
            $where .= "g.id = '{$condition['roomId']}'";
        }
        if (isset($condition['timeType'])&&$condition['timeType'] == 'group') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $end = date('Y-m-d 23:59:59', $condition['endTime']);
                $where .= "g.created_at between '{$begin}' and '{$end}'";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $where .= "g.created_at >= '{$begin}'";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $end = date('Y-m-d 23:59:59', $condition['endTime']);
                $where .= "g.created_at <= '{$end}'";
            }
        }

        if (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= " g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
        } else {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` <> '')";
        }


        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;

            $searchMid = is_array($condition['searchMid']) ? implode("','", $condition['searchMid']) : $condition['searchMid'];
            $where .= "g.mid in ('{$searchMid}')";
        }
        if(!empty($condition['isBingGroupLeader']) && isset($condition['isBingGroupLeader'])){
            $where = $where ? $where . ' and ' : $where;
            if($condition['isBingGroupLeader'] == 1){
                $where .= 'g.mid > 0';
            }else{
                $where .= 'g.mid = 0';
            }
        }
        if(!empty($condition['groupMid']) && isset($condition['groupMid'])){
            $where = $where ? $where . ' and ' : $where;
            //$where .= "g.name like '%{$condition['name']}%'";
            $where .= "g.mid = ".$condition['groupMid']."";
        }

        if(!empty($condition['groupAdminMid']) && isset($condition['groupAdminMid'])){
            $where = $where ? $where . ' and ' : $where;
            //$where .= "g.name like '%{$condition['name']}%'";
            $where .= "g.mid = ".$condition['groupAdminMid']."";
        }
        return $where;
    }

    public static function getHouTaiGroupListWhere($condition)
    {
        $where='g.is_delete = 0';
        //$where='1=1';
        if(!empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like '%{$condition['name']}%'";
        }
        if(isset($condition['tag_id'])&&!empty($condition['tag_id'])&&count($condition['tag_id'])){
            $where = $where ? $where . ' and ' : $where;
            $tagId = implode(',', $condition['tag_id']);
            $where .= "g.tag_id in ({$tagId})";
        }

        if(isset($condition['roomId'])&&!empty($condition['roomId'])&&count($condition['roomId'])){
            $where = $where ? $where . ' and ' : $where;
            $roomId = implode(',', $condition['roomId']);
            $where .= "g.id in ({$roomId})";
        }
        if(isset($condition['beginTime'])&&isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at between '{$begin}' and '{$end}'";
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $where .= "g.created_at >= '{$begin}'";
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $where = $where ? $where . ' and ' : $where;
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at <= '{$end}'";
        }
        if (isset($condition['user_type']) && $condition['user_type'] >= 2) {
            if(!empty($condition['wx_alias'])){
                $where = $where ? $where . ' and ' : $where;
                $where .= "r.wx_alias = '{$condition['wx_alias']}'";
            }else{
                $arr=[];
                foreach($condition['wxIds'] as $v){
                    $arr[] = $v;
                }
                if(!empty($arr)){
                    $where = $where ? $where . ' and ' : $where;
                    $wxIds = implode("','", $arr);
                    $where .= "g.wxid in ('{$wxIds}')";
                }
            }
        } else {
            if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
                $where = $where ? $where . ' and ' : $where;
                $where .= "g.mid = {$condition['searchMid']}";
            } else {
                if (isset($condition['mid']) && !empty($condition['mid'])) {
                    $where = $where ? $where . ' and ' : $where;
                    $where .= "g.mid = {$condition['mid']}";
                }
            }
        }
        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['searchMid']}";
        }




        if(isset($condition)&&!empty($condition['platform_identity'])){
            $where .= " and r.platform_identity = '{$condition['platform_identity']}'";
        }

        if(isset($condition['platform']) && $condition['platform'] !== ''){
            $where .= " and r.platform= '{$condition['platform']}'";
        }

        return $where;
    }
    public static function getAdminGroupListWhere($condition)
    {

        $where = "g.mid > 0 AND g.is_delete = 0 ";

        if(!empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like %{$condition['name']}%";
        }
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.bind_mid = {$condition['mid']}";
        }elseif (isset($condition['mid']) && !empty($condition['mid'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.mid = {$condition['mid']}";
        }
        if(isset($condition['tag_id'])&&!empty($condition['tag_id'])&&count($condition['tag_id'])){
            $where = $where ? $where . ' and ' : $where;
            $tagId = implode(',', $condition['tag_id']);
            $where .= "g.tag_id in ({$tagId})";
        }
        if (!isset($condition['timeType']) || $condition['timeType'] != 'order') {
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $condition['endTime'] = $condition['beginTime'] == $condition['endTime'] ? $condition['endTime'] + 3600 : $condition['endTime'];
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at between '{$begin}' and '{$end}'";
        } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $where .= "g.created_at >= '{$begin}'";
        } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at <= '{$end}'";
        }
    }

        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['searchMid']}";
        }

        if(isset($condition['platform_identity'])&&!empty($condition['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.platform_identity = '".$condition['platform_identity']."'";
        }

        if (isset($condition['phone']) && !empty($condition['phone'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mobile = '{$condition['phone']}'";
        }

        if (isset($condition['wxid_type']) && !empty($condition['wxid_type'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.wxid_type = {$condition['wxid_type']}";
        }

        return $where;
    }


    public static function getYCAdminGroupListWhere($condition)
    {

        $where = "g.mid > 0 AND g.is_delete = 0 ";


        if (isset($condition['userId']) && !empty($condition['userId'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "r.exclusive_mid = {$condition['userId']}";
        }

        if (isset($condition['groupAdminId']) && !empty($condition['groupAdminId'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['groupAdminId']}";
        }

        if(isset($condition['platform_identity'])&&!empty($condition['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "r.platform_identity = '{$condition['platform_identity']}'";
        }

        return $where;
    }


    public static function getAdminGroupListWhereV2($condition)
    {

        $where = "g.mid > 0 AND g.is_delete = 0 ";

        if(!empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like %{$condition['name']}%";
        }
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.bind_mid = {$condition['mid']}";
        }elseif (isset($condition['mid']) && !empty($condition['mid'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.mid = {$condition['mid']}";
        }
        if(isset($condition['tag_id'])&&!empty($condition['tag_id'])&&count($condition['tag_id'])){
            $where = $where ? $where . ' and ' : $where;
            $tagId = implode(',', $condition['tag_id']);
            $where .= "g.tag_id in ({$tagId})";
        }
        if (!isset($condition['timeType']) || $condition['timeType'] != 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $condition['endTime'] = $condition['beginTime'] == $condition['endTime'] ? $condition['endTime'] + 3600 : $condition['endTime'];
                //$begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $begin = $condition['beginTime'];
                //$end = date('Y-m-d 00:00:00', $condition['endTime']);
                $end = $condition['endTime'];
                $where .= "g.created_at between '{$begin}' and '{$end}'";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $begin = $condition['beginTime'];
                $where .= "g.created_at >= '{$begin}'";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$end = date('Y-m-d 00:00:00', $condition['endTime']);
                $end = $condition['endTime'];
                $where .= "g.created_at <= '{$end}'";
            }
        }

        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['searchMid']}";
        }

        if (isset($condition['groupAdminMid']) && !empty($condition['groupAdminMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['groupAdminMid']}";
        }

        if(isset($condition['platform_identity'])&&!empty($condition['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.platform_identity = '".$condition['platform_identity']."'";
        }

        if (isset($condition['phone']) && !empty($condition['phone'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mobile = {$condition['phone']}";
        }

        if (isset($condition['wxid_type']) && !empty($condition['wxid_type'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.wxid_type = {$condition['wxid_type']}";
        }

        return $where;
    }


    public static function getAdminGroupListWhereV3($condition)
    {

        $where = "g.mid > 0 AND g.is_delete = 0 ";

        if(!empty($condition['name'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like %{$condition['name']}%";
        }
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.bind_mid = {$condition['mid']}";
        }elseif (isset($condition['mid']) && !empty($condition['mid'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.mid = {$condition['mid']}";
        }
        if(isset($condition['tag_id'])&&!empty($condition['tag_id'])&&count($condition['tag_id'])){
            $where = $where ? $where . ' and ' : $where;
            $tagId = implode(',', $condition['tag_id']);
            $where .= "g.tag_id in ({$tagId})";
        }
        if (!isset($condition['timeType']) || $condition['timeType'] != 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $condition['endTime'] = $condition['beginTime'] == $condition['endTime'] ? $condition['endTime'] + 3600 : $condition['endTime'];
                //$begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $begin = $condition['beginTime'];
                //$end = date('Y-m-d 00:00:00', $condition['endTime']);
                $end = $condition['endTime'];
                $where .= "g.created_at between '{$begin}' and '{$end}'";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$begin = date('Y-m-d 00:00:00', $condition['beginTime']);
                $begin = $condition['beginTime'];
                $where .= "g.created_at >= '{$begin}'";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$end = date('Y-m-d 00:00:00', $condition['endTime']);
                $end = $condition['endTime'];
                $where .= "g.created_at <= '{$end}'";
            }
        }

        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['searchMid']}";
        }

        if (isset($condition['groupAdminMid']) && !empty($condition['groupAdminMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['groupAdminMid']}";
        }

        if(isset($condition['platform_identity'])&&!empty($condition['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.platform_identity = '".$condition['platform_identity']."'";
        }

        if (isset($condition['phone']) && !empty($condition['phone'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mobile = {$condition['phone']}";
        }

        if (isset($condition['wxid_type']) && !empty($condition['wxid_type'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "w.wxid_type = {$condition['wxid_type']}";
        }

        return $where;
    }
    /*
    * 获取用户订单列表
    */
    public static function orderGroupList($condition = [], $pageSize = 15) {
        $where = [
            'pay_status' => 1
        ];
        $fields = ['a.id', 'a.name', 'a.mid', 'o.record_id','a.nick_name','a.mobile'];
        $query = DB::connection('robot')->table(trim(config('community.oldMallOrderGoods'),' ').' as o');
        $query->getConnection()->setTablePrefix('');
        $query = $query->rightjoin ('ylh_robot.sline_community_member_info_by_appid as a', 'o.room_id', '=', 'a.id')
            ->select($fields)
            ->selectRaw('count(o.record_id) as orderNum')
            ->selectRaw('sum(o.actual_price) as price')
            ->where($where);
        if (!empty($condition['roomId'])) {
            $query = $query->whereIn('o.room_id', $condition['roomId']);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $query = $query->whereBetween('o.created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        return $query->groupBy('room_id')->paginate($pageSize)->toArray();
    }


    /*
     * 获取群总数
     */
    public static function getGroupNumDate($condition = [], $format = 'day') {
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
        $where = self::getGroupNumDateWhere($condition);
        $sql = "select DATE_FORMAT(created_at, '{$format}') as date,COUNT(*) as value";
        $sql .= " from ylh_robot.sline_community_member_info_by_appid as g";
        $sql .= " where {$where} group by date";
        return DB::select($sql);
    }



    public static function getGroupNumDateWhere($condition)
    {
        $where='';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= " g.wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']})";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= " g.wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']})";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= " g.wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}')";
            } else {
                $where .= " g.wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx  WHERE `platform_identity` <> '')";
            }
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $where .= "g.created_at between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }
        if(isset($condition['limitNum']) && $condition['limitNum']>0){
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.member_count >= '{$condition['limitNum']}'";
        }
        return $where;
    }


    /*
     * 获取用户总数
     */
    public static function getGroupMemberDate($condition = [], $format = 'day') {
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
        $where = self::getGroupMemberDateWhere($condition);
        $sql = "select DATE_FORMAT(created_at, '{$format}') as date,COUNT(*) as value";
        $sql .= " from ylh_robot.sline_community_member_weixin_info as info";
        $sql .= " where {$where} group by date";
        return DB::select($sql);
    }


    public static function getGroupMemberDateWhere($condition)
    {
        $where='';
        $where = $where ? $where . ' and ' : $where;
        if(isset($condition['sup_id']) && $condition['sup_id']>0){
            $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
            $where .= " wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE bind_mid = {$condition['mid']}))";
        }else{
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
                $where .= " wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE mid = {$condition['mid']}))";
            } elseif (isset($condition['platform_identity']) && !empty($condition['platform_identity'])) {
                $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
                $where .= " wxid in ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx WHERE `platform_identity` = '{$condition['platform_identity']}'))";
            } else {
                $where .= "info.room_wxid in ( SELECT room_wxid FROM ylh_robot.sline_community_member_info_by_appid WHERE ";
                $where .= " wxid IN ( SELECT wxid FROM ylh_robot.sline_community_white_list_wx  WHERE `platform_identity` <> ''))";
            }
        }
        if(isset($condition['beginTime']) && isset($condition['endTime'])){
            $where = $where ? $where . ' and ' : $where;
            if (is_numeric($condition['beginTime']) && is_numeric($condition['endTime'])) {
                $condition['beginTime'] = date('Y-m-d 00:00:00', $condition['beginTime']);
                $condition['endTime'] = date('Y-m-d 00:00:00', $condition['endTime']);
            }
            $where .= "info.created_at between '{$condition['beginTime']}' and '{$condition['endTime']}'";
        }
        return $where;
    }


    /*
     * 补全统计日期中没有数据的日期
     */
    public static function completionDate($param, $orderTotalNum, $orderTotalAmount, $memberTotalNum)
    {
        #计算日期内天数
        $stimestamp = $param['beginTime'];
        $etimestamp = $param['endTime'];
        if ($param['format'] == 'day') {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 86400;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
            }
        } else {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 3600;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                //$date[] = date('Y-m-d H', $stimestamp + (3600 * $i));
                $date[] = date('H', $stimestamp + (3600 * $i));
            }
        }
        #循环补全日期
        foreach ($date as $key => $val) {
            if ($param['format'] != 'day') {
                $val = intval($val);
            }
            $data[$val] = [
                'orderNum' => 0, //订单数量
                'orderAmount' => sprintf("%.2f",0), //订单金额
                'memberCount' => 0 //新增用户
            ];
            foreach ($orderTotalNum as $item => $value) {
                if ($val == $value['date']) {
                    $data[$val]['orderNum'] = $value['value'];
                }
            }
            foreach ($orderTotalAmount as $item => $value) {
                if ($val == $value['date']) {
                    $data[$val]['orderAmount'] = sprintf("%.2f",$value['value']/100);
                }
            }
            foreach ($memberTotalNum as $item => $value) {
                if ($val == $value['date']) {
                    $data[$val]['memberCount'] = $value['value'];
                }
            }
        }
        return $data;
    }


    /*
     * 补全统计日期中没有数据的日期
     */
    public static function completionDateArray($param, $todoReturn)
    {
        #计算日期内天数
        $stimestamp = $param['beginTime'];
        $etimestamp = $param['endTime'];
        if ($param['format'] == 'day') {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 86400;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
            }
        } else {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 3600;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                //$date[] = date('Y-m-d H', $stimestamp + (3600 * $i));
                $date[] = date('H', $stimestamp + (3600 * $i));
            }
        }
        #循环补全日期
        foreach ($date as $k => $val) {
            if ($param['format'] != 'day') {
                $val = intval($val);
            }
            $dataKeys = array_keys($todoReturn);
            foreach ($dataKeys as $v) {
                $data[$val][$v] = 0;
            }
//            $data[$val] = [
//                'orderNum' => 0, //订单数量
//                'orderAmount' => sprintf("%.2f",0), //订单金额
//                'memberCount' => 0 //新增用户
//            ];
            foreach ($todoReturn as $key => $value) {
                foreach ($value as $item) {
                    if ($val == $item['date']) {
                        $data[$val][$key] = $item['value'];
                        if ($key == 'orderAmount') {
                            $data[$val][$key] = sprintf("%.2f",$item['value']/100
                            );
                        }
                    }
                }
            }
        }
        return $data;
    }




    /*
     * 补全统计日期中没有数据的日期
     */
    public static function completionDateArrayV2($param, $todoReturn)
    {
        #计算日期内天数
        $stimestamp = $param['beginTime'];
        $etimestamp = $param['endTime'];
        if ($param['format'] == 'day') {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 86400;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
            }
        } else {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 3600;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                //$date[] = date('Y-m-d H', $stimestamp + (3600 * $i));
                $date[] = date('H', $stimestamp + (3600 * $i));
            }
        }
        foreach ($todoReturn as $index => $item) {
            switch ($index) {
                case '悦淘':
                    $channelName = 'ytData';
                    break;
                case '大人':
                    $channelName = 'drData';
                    break;
                case '迈图':
                    $channelName = 'mtData';
                    break;
                case '直订':
                    $channelName = 'zdData';
                    break;
                 case '邻居团':
                    $channelName = 'ljtData';
                    break;
                default:
                    $channelName = 'other';
                    break;
            }
            #循环补全日期
            foreach ($date as $k => $val) {
                if ($param['format'] != 'day') {
                    $data[$channelName][$k]['num'] = 0;
                }
                $data[$channelName][$k]['day'] = $val;
                $data[$channelName][$k]['num'] = 0;
                foreach ($item as $key => $value) {
                    if (isset($value['date']) && $value['date'] == $val) {
                        $data[$channelName][$k]['num'] = $value['value'];
                    }
                }
            }
        }
        return $data;
    }

    /*
     * 补全统计日期中没有数据的日期
     */
    public static function completionDateHour($param, $orderTotalNum)
    {
        #计算日期内天数
        $stimestamp = $param['beginTime'];
        $etimestamp = $param['endTime'];
        #计算日期段内有多少天
        $days = ($etimestamp - $stimestamp) / 86400;
        $hours = date('H', $etimestamp);
        #保存每天日期
        $date = array();
        for ($i = 0; $i <= $days; $i++) {
            $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
        }
        $end = 24;
        #循环补全日期
        foreach ($date as $key => $val) {
            if ($hours != 00) {
                if (end(array_keys($date)) == $key) {
                    $end = $hours;
                }
            }
            #循环补全小时
            for ($i = 00; $i < $end; $i++) {
                $data[] = [
                    'date' => $val, //日期
                    'hour' => $i, //时间
                    'orderGroupNum' => 0 //订单数量
                ];
                foreach ($orderTotalNum as $item => $value) {
                    if ($val == $value['date'] && $value['hour'] == $i) {
                        $data[]['date'] = $val;
                        $data[]['hour'] = $i;
                        $data[]['orderGroupNum'] = $value['value'];
                    }
                }
            }
        }
        return $data;
    }


    /*
     * 获取用户订单总数
     */
    public static function getOrderTotalAmountGroupDate($condition = [], $format = 'day') {
        $where = [
            'pay_status' => 1
        ];
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods')->where($where)
           $query = DB::connection('robot')->table('mall_order_goods')->where($where)
            ->whereIn('room_id', $condition['roomId']);
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $condition['beginTime'] = strtotime($condition['beginTime']);
            $condition['endTime'] = strtotime($condition['endTime']);
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }
        if(isset($condition['channel']) && !empty($condition['channel'])){
            $query = $query->where(['channel' => $condition['channel']]);
        }
        return $query->selectRaw("DATE_FORMAT(FROM_UNIXTIME(created_at), '{$format}') as date,SUM(actual_price) as value")
            ->groupBy('date')->get()->toArray();
    }

    /*
    * 获取社群订单总数
    */
    public static function getOrderTotalAmountByRoomId($param) {
        $where = [
            //订单是否支付
            'pay_status' => 1
        ];
        $where['room_id'] = $param['roomId'];
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods');
        $query = DB::connection('robot')->table('mall_order_goods');
        if (is_array($param['roomId'])) {
            $query = $query->whereIn('room_id', $param['roomId']);
        } else {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('pay_time', [$param['beginTime'], $param['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('pay_time','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('pay_time','<=',$condition['endTime']);
        }
        return $query->sum('actual_price');
    }

    /*
     * 获取用户的群列表
     */
    public static function getGroupList($condition, $page = 1, $pageSize = 10)
    {
        $begin = ($page-1)*$pageSize;
        $where = self::getGroupListWhere($condition);
        $orderWhere = self::getGroupListOrderWhere($condition);
//        $msgWhere = self::getGroupListMsgWhere($condition);
        $orderBy = self::getGroupListOrderBy($condition);
        $having = self::getGroupListHaving($condition);
        $sql = "select  g.created_at,g.wxid,g.id,g.name,g.room_wxid,g.mid,g.robot_status,g.deleted_at,g.tag_id,g.sign_switch,r.bind_mid,r.mid as rmid,";
        //$sql .= "TRUNCATE((sum(o.goods_price_member)-sum(o.goods_price_buy))*0.85*0.1,2) rebate,";
        $sql .= "c.rebate,";
        $sql .= "g.member_count groupMemberNum,";
//        $sql .= "msg.messageTotalNum,";
        $sql .= "r.id as rid,r.wxid_type as wxid_type,r.mid as wxmid,r.wx_name as wx_alias,";
        $sql .= "o.orderNumRoom orderNum,o.orderAmountRoom orderAmount,o.orderMemberRoom as orderMember,";
        $sql .= "g.mobile,oNew.firstOrderNumRoom firstOrderNum,TRUNCATE(oTwo.orderNumProportionNum / g.member_count * 100, 2) orderNumProportion";
        $sql .= " from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select room_id,sum(actual_price) orderAmountRoom,channel,count(id) orderNumRoom,good_type,";
        $sql .= "COUNT(DISTINCT(`member_id`)) as orderMemberRoom,";
        $sql .= "sum(TRUNCATE(CASE WHEN good_type = 1 THEN ( goods_price_member - goods_price_buy - goods_coupon_average ) / 4 else commission / 4  END ,2)) estCommissionRoom  ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN (select * from (select room_id,channel,count(firstOrderNumRoomNew) firstOrderNumRoom from (select room_id,channel,count(member_id) firstOrderNumRoomNew";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id,member_id Having count(member_id) = 1) as fOne group by room_id) fTre) oNew";
        $sql .= " ON g.id = oNew.room_id ";
        $sql .= " LEFT JOIN (select * from (select room_id,channel,count(orderNumProportionNumNew) orderNumProportionNum from (select room_id,channel,count(member_id) orderNumProportionNumNew";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id,member_id Having count(member_id) > 1) as oOne group by room_id) oTre) oTwo";
        $sql .= " ON g.id = oTwo.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " LEFT JOIN (select room_id,sum(amount) rebate";
        $sql .= " from ";
        $sql .= " sline_community_new_commission group by member_id) c";
        $sql .= " ON c.room_id =g.id ";
//        //群消息数量排序
//        $sql .= " LEFT JOIN (select COUNT(1) as messageTotalNum, room_wxid";
//        $sql .= " from ";
//        $sql .= " sline_community_report_msg {$msgWhere} group by room_wxid) msg";
//        $sql .= " ON msg.room_wxid =g.room_wxid ";
        $sql .= " where {$where} group by g.id {$having} {$orderBy} limit $begin,$pageSize";
        //dd($sql);
        return DB::select($sql);
    }



    /*
     * 获取用户的群列表
     */
    public static function getRoomIdsByCondition($condition)
    {
        $where = self::getRoomIdsByConditionWhere($condition);
        $sql = "select  distinct(g.id)";
        $sql .= " from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " where {$where}";
        $result = DB::select($sql);
        return $result ? json_decode(json_encode($result), true) : [];
    }


    /*
     * 获取用户的群列表
     */
    public static function deleteGroupStatus($roomId)
    {
        return DB::table('member_info_by_appid')->where(['id' => $roomId])->update(['is_delete' => 1]);
    }


    /*
     * 获取用户的群列表
     */
    public static function getYCloudGroupList($condition, $page = 1, $pageSize = 10)
    {
        $begin = ($page-1)*$pageSize;
        $where = self::getYCloudGroupListWhere($condition);
        $sql = "select  g.id,r.id as robotId,r.exclusive_mid,g.name,g.mid,g.robot_status,g.member_count,g.is_delete";
        $sql .= " from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " where {$where} limit $begin,$pageSize";
        return DB::select($sql);
    }

    public static function getChannelList($data=[])
    {
        $where = self::getChannelListWhere();

        if (!empty($data['limit']) && $data['limit']=='all')
        {
            $sql    = "select id,platform_identity,channel_name from sline_community_channel_data WHERE is_delete=0 {$where} ";
            $result = DB::select($sql);
        }else{
            $sql    = "select id,platform_identity,channel_name from sline_community_channel_data WHERE is_delete=0 {$where} order by id desc limit ?, ?";
            $result = DB::select($sql, [$data["offset"], $data["display"]]);
        }

        return $result;
    }


    public static function getChannelListWhere($param = [])
    {
        $where='';
        if (isset($param['name']) && !empty($param['name'])) {
            $where = "AND `name` like '%" . $param['name'] . "%' ";
        }
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= "AND `platform_identity` = '" . $param['platform_identity'] . "'";
        }
        return $where;
    }

    public static function getChannelListCount()
    {
        $where  = self::getChannelListWhere();
        $sql    = "select count(*) count from sline_community_channel_data WHERE is_delete=0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }


    /*
     * 获取用户的群列表
     */
    public static function getHouTaiGroupList($condition, $page = 1, $pageSize = 10)
    {
        $begin = ($page-1)*$pageSize;
        $where = self::getHouTaiGroupListWhere($condition);
        $orderBy = self::getHouTaiGroupListOrderBy($condition);
        $having = self::getHouTaiGroupListHaving($condition);

        $sql = "select g.id,g.name,g.room_wxid,g.mid,g.robot_status,g.deleted_at,g.tag_id,c.channel_name,r.bind_mid,r.mid as rmid,";
        $sql .= "r.id as rid,r.wxid_type as wxid_type,r.mid as wxmid,count(o.room_id) orderNum,r.wx_name as wx_alias,r.platform,";
        $sql .= "sum(actual_price) orderAmount,u.mobile from sline_community_member_info_by_appid g";
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id = g.id and o.pay_status=1 and o.room_id>0";
        if (isset($condition['platform']) && $condition['platform'] == 2) {
            $sql .= " left join sline_community_white_list_wx r on r.enterprise_robot_serial_no = g.enterprise_robot_serial_no";
        } else {
            $sql .= " left join sline_community_white_list_wx r on r.wxid = g.wxid";
        }
        $sql .= " left join sline_community_user u on u.mid=g.mid ";
        $sql .= " left join sline_community_channel_data c on c.platform_identity=u.platform_identity ";
        $sql .= " where {$where} group by g.id {$having} {$orderBy} limit $begin,$pageSize";
        return DB::select($sql);
    }

    /*
    * 获取用户的群列表
    */
    public static function getGroupListCount($condition)
    {
        $where = self::getGroupListWhere($condition);
        $orderWhere = self::getGroupListOrderWhere($condition);
        $having = self::getGroupListHaving($condition);
        if (strlen($orderWhere) > 4) {
            $joinType = 'right';
        } else {
            if (isset($condition['haveOrder']) && $condition['haveOrder'] == 1) {
                $joinType = 'right';
            } else {
                $joinType = 'left';
            }
        }
        $sql = "select count(*) as cnt";
        $sql .= " from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " {$joinType} JOIN (select room_id";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        //群消息数量排序
        $sql .= " where {$where} {$having} ";

        return DB::select($sql);
    }


    /*
    * 获取用户的群列表
    */
    public static function getYCloudGroupListCount($condition)
    {
        $where = self::getYCloudGroupListWhere($condition);
        $sql = "select count(*) as cnt";
        $sql .= " from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " where {$where}";
        $result = DB::selectOne($sql);
        return $result ? $result->cnt : 0;
    }


    /*
    * 获取用户的群列表
    */
    public static function getGroupListCountV2($condition)
    {
        $where = self::getGroupListWhereV2($condition);
        $orderWhere = self::getGroupListOrderWhereV2($condition);
        $having = self::getGroupListHavingV2($condition);
        if (strlen($orderWhere) > 4) {
            $joinType = 'right';
        } else {
            if (isset($condition['haveOrder']) && $condition['haveOrder'] == 1) {
                $joinType = 'right';
            } else {
                $joinType = 'left';
            }
        }
        $sql = "select count(*) as cnt";
        $sql .= " from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " {$joinType} JOIN (select room_id";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        //群消息数量排序
        $sql .= " where {$where} {$having} ";

        return DB::select($sql);
    }

    /*
    * 获取用户的群列表
    */
    public static function getHouTaiGroupListCount($condition)
    {
        $where = self::getHouTaiGroupListWhere($condition);
        $having = self::getHouTaiGroupListHaving($condition);
        $where1 = "";
        if(isset($condition)&&!empty($condition['platform_identity'])){
            $where1= " and u.platform_identity = '{$condition['platform_identity']}'";
        }
        $sql = "SELECT count(*) as cnt FROM (select g.*,r.id as rid,r.wxid_type as wxid_type,r.mid as wxmid,count(o.room_id) orderNum,r.wx_name as wx_alias,";
        $sql .= "sum(actual_price) orderAmount from sline_community_member_info_by_appid g";
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id = g.id and o.pay_status=1 and o.room_id>0";
        $sql .= " left join sline_community_white_list_wx r on r.wxid = g.wxid";
        $sql .= " left join sline_community_user u on u.mid=g.mid ";
        $sql .= " left join sline_community_channel_data c on c.platform_identity=u.platform_identity $where1";
        $sql .= " where {$where} group by g.id {$having}) as a";
        return DB::select($sql);
    }
    /*
    * 获取用户的群列表
    */
    public static function getGroupListHaving($condition)
    {
        $having = 'having ';
        $tail = '';
        if (isset($condition['haveOrder']) && !empty($condition['haveOrder'])) {
            if ($condition['haveOrder'] == 1) {
                $tail .= " count(o.room_id) > 0";
            } else {
                $tail .= " count(o.room_id) = 0";
            }
        }
        if ($tail) {
            return $having.$tail;
        }
        return '';
    }

    /*
* 获取用户的群列表
*/
    public static function getGroupListHavingV2($condition)
    {
        $having = 'having ';
        $tail = '';
        if (isset($condition['haveOrder']) && !empty($condition['haveOrder'])) {
            if ($condition['haveOrder'] == 1) {
                $tail .= " count(o.room_id) > 0";
            } else {
                $tail .= " count(o.room_id) = 0";
            }
        }
        if ($tail) {
            return $having.$tail;
        }
        return '';
    }
    /*
     * 获取用户的群列表
     */
    public static function getGroupDetailV2($condition)
    {
        $where = self::getGroupListWhereV2($condition);
        $orderWhere = self::getGroupListOrderWhereV2($condition);
        $sql = "select  g.created_at,g.name,g.id,g.mid,g.robot_status,g.deleted_at,r.bind_mid,g.tag_id,";
        //$sql .= "TRUNCATE((sum(o.goods_price_member)-sum(o.goods_price_buy))*0.85*0.1,2) rebate,";
        $sql .= "r.mid as rmid,c.rebate,";
        $sql .= "g.member_count groupMemberNum,";
        $sql .= "r.mid as wxmid,r.wx_name as wx_alias,g.mobile,";
        $sql .= "o.orderNumRoom orderNum,o.orderAmountRoom orderAmount,";
        $sql .= "TRUNCATE(orderAmountRoom / orderNumRoom , 0) orderAvgAmount from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select room_id,sum(actual_price) orderAmountRoom,channel,count(id) orderNumRoom,good_type,";
        $sql .= "COUNT(DISTINCT(`member_id`)) as orderMemberRoom,";
        $sql .= "sum(TRUNCATE(CASE WHEN good_type = 1 THEN ( goods_price_member - goods_price_buy - goods_coupon_average ) / 4 else commission / 4  END ,2)) estCommissionRoom  ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " LEFT JOIN (select room_id,sum(amount) rebate";
        $sql .= " from ";
        $sql .= " sline_community_new_commission group by member_id) c";
        $sql .= " ON c.room_id =g.id ";
        $sql .= " where {$where}";
        return DB::selectOne($sql);
    }


    /*
     * 获取用户的群列表
     */
    public static function getGroupListV2($condition, $page = 1, $pageSize = 10)
    {
        $begin = ($page-1)*$pageSize;
        $where = self::getGroupListWhereV2($condition);
        $orderWhere = self::getGroupListOrderWhereV2($condition);
//        $msgWhere = self::getGroupListMsgWhere($condition);
        $orderBy = self::getGroupListOrderByV2($condition);
        $having = self::getGroupListHavingV2($condition);
        $sql = "select  g.name,g.id,g.mid,g.robot_status,g.deleted_at,g.tag_id,r.bind_mid,";
        //$sql .= "TRUNCATE((sum(o.goods_price_member)-sum(o.goods_price_buy))*0.85*0.1,2) rebate,";
        $sql .= "r.mid as rmid,c.rebate,";
        $sql .= "g.member_count groupMemberNum,";
        $sql .= "r.mid as wxmid,r.wx_name as wx_alias,";
        $sql .= "o.orderNumRoom orderNum,o.orderAmountRoom orderAmount,";
        $sql .= "TRUNCATE(orderAmountRoom / orderNumRoom , 0) orderAvgAmount from ";
        $sql .= " sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select room_id,sum(actual_price) orderAmountRoom,channel,count(id) orderNumRoom,good_type,";
        $sql .= "COUNT(DISTINCT(`member_id`)) as orderMemberRoom,";
        $sql .= "sum(TRUNCATE(CASE WHEN good_type = 1 THEN ( goods_price_member - goods_price_buy - goods_coupon_average ) / 4 else commission / 4  END ,2)) estCommissionRoom  ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where pay_status=1 and room_id>0 and {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " LEFT JOIN (select room_id,sum(amount) rebate";
        $sql .= " from ";
        $sql .= " sline_community_new_commission group by member_id) c";
        $sql .= " ON c.room_id =g.id ";
        $sql .= " where {$where} group by g.id {$having} {$orderBy} limit $begin,$pageSize";
        return DB::select($sql);
    }

    /*
    * 获取用户的群列表
    */
    public static function getHouTaiGroupListHaving($condition)
    {
        $having = 'having ';
        $tail = '';
        if (isset($condition['haveOrder']) && !empty($condition['haveOrder'])) {
            if ($condition['haveOrder'] == 1) {
                $tail .= " count(o.room_id) > 0";
            } else {
                $tail .= " count(o.room_id) = 0";
            }
        }
        if ($tail) {
            return $having.$tail;
        }
        return '';
    }

    public static function getGroupListOrderBy($condition)
    {
        $orderBy = '';
        if(isset($condition['orderNum'])&&!empty($condition['orderNum'])&&in_array($condition['orderNum'], ['asc', 'desc'])){
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if(isset($condition['orderAmount'])&&!empty($condition['orderAmount'])&&in_array($condition['orderAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }
        if(isset($condition['orderMember'])&&!empty($condition['orderMember'])&&in_array($condition['orderMember'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderMember {$condition['orderMember']}" : "order by orderMember {$condition['orderMember']}";
        }
        if(isset($condition['groupMemberNum'])&&!empty($condition['groupMemberNum'])&&in_array($condition['groupMemberNum'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", groupMemberNum {$condition['orderAmount']}" : "order by groupMemberNum {$condition['groupMemberNum']}";
        }
        if(isset($condition['rebate'])&&!empty($condition['rebate'])&&in_array($condition['rebate'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", c.rebate {$condition['rebate']}" : "order by c.rebate {$condition['rebate']}";
        }
        if(isset($condition['rebateNew'])&&!empty($condition['rebateNew'])&&in_array($condition['rebateNew'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", rebateNew {$condition['rebateNew']}" : "order by rebateNew {$condition['rebateNew']}";
        }
        if(isset($condition['firstOrderNum'])&&!empty($condition['firstOrderNum'])&&in_array($condition['firstOrderNum'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", firstOrderNum {$condition['firstOrderNum']}" : "order by firstOrderNum {$condition['firstOrderNum']}";
        }
        if(isset($condition['orderNumProportion'])&&!empty($condition['orderNumProportion'])&&in_array($condition['orderNumProportion'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderNumProportion {$condition['orderNumProportion']}" : "order by orderNumProportion {$condition['orderNumProportion']}";
        }
//        if(isset($condition['messageTotalNum'])&&!empty($condition['messageTotalNum'])&&in_array($condition['messageTotalNum'], ['asc', 'desc'])){
//            $orderBy = $orderBy ? $orderBy . ", msg.messageTotalNum {$condition['messageTotalNum']}" : "order by msg.messageTotalNum {$condition['messageTotalNum']}";
//        }
        if($orderBy=='')
        {
            $orderBy .= "order by g.id desc";
        }
        return $orderBy;
    }

    public static function getGroupListOrderByV2($condition)
    {
        $orderBy = '';
        if(isset($condition['orderNum'])&&!empty($condition['orderNum'])&&in_array($condition['orderNum'], ['asc', 'desc'])){
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if(isset($condition['orderAmount'])&&!empty($condition['orderAmount'])&&in_array($condition['orderAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }
        if(isset($condition['orderAvgAmount'])&&!empty($condition['orderAvgAmount'])&&in_array($condition['orderAvgAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAvgAmount {$condition['orderAvgAmount']}" : "order by orderAvgAmount {$condition['orderAvgAmount']}";
        }
        if(isset($condition['orderMember'])&&!empty($condition['orderMember'])&&in_array($condition['orderMember'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderMember {$condition['orderMember']}" : "order by orderMember {$condition['orderMember']}";
        }
        if(isset($condition['groupMemberNum'])&&!empty($condition['groupMemberNum'])&&in_array($condition['groupMemberNum'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", groupMemberNum {$condition['orderAmount']}" : "order by groupMemberNum {$condition['groupMemberNum']}";
        }
        if(isset($condition['rebate'])&&!empty($condition['rebate'])&&in_array($condition['rebate'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", c.rebate {$condition['rebate']}" : "order by c.rebate {$condition['rebate']}";
        }
        if(isset($condition['rebateNew'])&&!empty($condition['rebateNew'])&&in_array($condition['rebateNew'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", rebateNew {$condition['rebateNew']}" : "order by rebateNew {$condition['rebateNew']}";
        }
//        if(isset($condition['messageTotalNum'])&&!empty($condition['messageTotalNum'])&&in_array($condition['messageTotalNum'], ['asc', 'desc'])){
//            $orderBy = $orderBy ? $orderBy . ", msg.messageTotalNum {$condition['messageTotalNum']}" : "order by msg.messageTotalNum {$condition['messageTotalNum']}";
//        }
        if($orderBy=='')
        {
            $orderBy .= "order by g.id desc";
        }
        return $orderBy;
    }




    public static function getAdminGroupListOrderBy($condition)
    {
        $orderBy = '';
        if(isset($condition['orderNum'])&&!empty($condition['orderNum'])&&in_array($condition['orderNum'], ['asc', 'desc'])){
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if(isset($condition['orderAmount'])&&!empty($condition['orderAmount'])&&in_array($condition['orderAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }


        if(isset($condition['orderCom'])&&!empty($condition['orderCom'])&&in_array($condition['orderCom'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", groupCommission {$condition['orderCom']}" : " order by groupCommission {$condition['orderCom']}";
        }

        if(isset($condition['estCom'])&&!empty($condition['estCom'])&&in_array($condition['estCom'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", estCommission {$condition['estCom']}" : " order by estCommission {$condition['estCom']}";
        }

        if(isset($condition['orderBy'])&&$condition['orderBy']==2)
        {
            $orderBy = $orderBy ? $orderBy . ", groupCount asc" : " order by groupCount asc";
        }else{

            $orderBy = $orderBy ? $orderBy . ", groupCount desc" : " order by groupCount desc";
        }



        return $orderBy;
    }


    public static function getAdminGroupListOrderByV2($condition)
    {
        $orderBy = '';

        if(isset($condition['orderNum'])&&!empty($condition['orderNum'])&&in_array($condition['orderNum'], ['asc', 'desc'])){
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if(isset($condition['groupCount'])&&!empty($condition['groupCount'])&&in_array($condition['groupCount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", groupCount {$condition['groupCount']}" : "order by groupCount {$condition['groupCount']}";
        }
        if(isset($condition['orderAmount'])&&!empty($condition['orderAmount'])&&in_array($condition['orderAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }


        if(isset($condition['orderCom'])&&!empty($condition['orderCom'])&&in_array($condition['orderCom'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", groupCommission {$condition['orderCom']}" : " order by groupCommission {$condition['orderCom']}";
        }

        if(isset($condition['estCom'])&&!empty($condition['estCom'])&&in_array($condition['estCom'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", estCommission {$condition['estCom']}" : " order by estCommission {$condition['estCom']}";
        }

        if(isset($condition['orderBy'])&&$condition['orderBy']==2)
        {
            $orderBy = $orderBy ? $orderBy . ", groupCount asc" : " order by groupCount asc";
        }else{

            $orderBy = $orderBy ? $orderBy . ", groupCount desc" : " order by groupCount desc";
        }



        return $orderBy;
    }

    public static function getHouTaiGroupListOrderBy($condition)
    {
        $orderBy = '';
        if(isset($condition['orderNum'])&&!empty($condition['orderNum'])&&in_array($condition['orderNum'], ['asc', 'desc'])){
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if(isset($condition['orderAmount'])&&!empty($condition['orderAmount'])&&in_array($condition['orderAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }
        return $orderBy;
    }


    /*
    * 获取用户的群主列表
     */
    public static function getTagIds($tag_name) {
        return DB::connection('robot')->table('tag')
            ->where('tag_name','like','%'.$tag_name.'%')->pluck('id')->toArray();
    }

    /*
    * 群签到开关修改
     */
    public static function updateGroupSignSwitch($id, $switch) {
        return DB::connection('robot')->table('member_info_by_appid')
            ->where(['id'=>$id])->update(['sign_switch' => $switch]);
    }

    /*
    * 群签到配置列表
     */
    public static function groupSignConfList($where) {
        $groupSignConfList = DB::connection('robot')->table('group_integral_conf')
            ->where($where)->get();
        return $groupSignConfList ? $groupSignConfList->toArray() : [];
    }

    /*
    * 群签到配置列表
    */
    public static function groupSignConfDetail($where) {
        $groupSignConfList = DB::connection('robot')->table('group_integral_conf')
            ->where($where)->first();
        return $groupSignConfList ? json_decode(json_encode($groupSignConfList), true) : [];
    }


    /*
    * 群签到配置列表
    */
    public static function groupSignConfUpdate($where, $roomUpdate) {
        return DB::connection('robot')->table('group_integral_conf')
            ->where($where)->update($roomUpdate);
    }

    /*
    * 群签到配置添加
     */
    public static function insertGroupSignConf($insertData) {
        return DB::connection('robot')->table('group_integral_conf')
            ->insertGetId($insertData);
    }


    /*
    * 群签到配置删除
     */
    public static function deleteGroupSignConf($where) {
        $update = [
            'deleted_at' => time()
        ];
        return DB::connection('robot')->table('group_integral_conf')
            ->where($where)->update($update);
    }

    /*
    * 获取用户的群主列表
     */
    public static function getRoomIdByRoomWxId($RoomWxId, $platform = 0) {
        $query = DB::connection('robot')->table('member_info_by_appid')->select(['id', 'sign_switch']);
        if ($platform == 1) {
            $query->where(['chat_room_serial_no' => $RoomWxId]);
        } elseif ($platform == 2) {
            $query->where(['enterprise_group_serial_no' => $RoomWxId]);
        } else {
            $query->where(['room_wxid' => $RoomWxId]);
        }
        $roomInfo = $query->first();
        return $roomInfo ? json_decode(json_encode($roomInfo), true) : [];
    }

    /*
    * 获取用户的群主列表
     */
    public static function getSearchMid($phone) {
//        return DB::connection('mysql_yuelvhui')->table('member')
//            ->where(['mobile' => $phone])->pluck('mid')->toArray();
        return DB::connection('robot')->table('member_info_by_appid')
            ->where(['mobile' => $phone])->pluck('mid')->toArray();
    }

    /*
    * 获取用户的群主列表
     */
    public static function getAdminGroupList($condition, $begin = 0, $pageSize = 10) {
        $where = self::getAdminGroupListWhere($condition);
        $orderBy = self::getAdminGroupListOrderBy($condition);
        $orderWhere = self::getAdminGroupListOrderWhere($condition);


        // $sql = "SELECT  g.created_at,g.wxid,g.id,g.room_wxid,r.wx_alias,g.mid as groupMid,g.robot_status,";
        // $sql .= "r.order_by as rid,r.wxid_type,r.bind_mid,r.mid as rmid,r.deleted_at,g.ids,";
        // $sql .= "g.groupCount,o.orderAmount,";
        // $sql .= "o.orderNum ,g.mobile as adminName ";//
        // $sql .= " FROM (select count(1) as groupCount,id,created_at,wxid,room_wxid,mid,robot_status,mobile,";
        // $sql .= "group_concat(distinct id) as ids";
        // $sql .= " from `sline_community_member_info_by_appid` where {$where} group by mid) as g";
        // $sql .= " left join sline_community_white_list_wx as r on g.wxid = r.wxid";
        // $sql .= " left join (select room_id,sum(actual_price) orderAmount,count(room_id) orderNum from ";
        // $sql .= config('community.oldMallOrderGoods')." group by room_id) o";
        // $sql .= " on o.room_id = g.id";
        // $sql .= " group by g.mid {$orderBy} limit $begin, $pageSize";

        $sql = "SELECT g.mid as groupMid,g.robot_status,group_concat(distinct(w.order_by)) as rid,w.wxid_type,w.bind_mid,w.mid as rmid,w.deleted_at,group_concat(distinct(g.id)) as ids,";
        $sql .= " count(DISTINCT(g.id)) as groupCount,sum(o.orderAmountRoom) orderAmount ,o.channel,";
        $sql .= " sum(o.orderNumRoom) orderNum ,g.mobile as adminName,scoc.groupCommission,";
        $sql .= "sum(o.estCommissionRoom) estCommission ";//
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select room_id,sum(actual_price) orderAmountRoom,channel,count(id) orderNumRoom,good_type,";
        $sql .= "sum(TRUNCATE(CASE WHEN good_type = 1 THEN ( goods_price_member - goods_price_buy - goods_coupon_average ) / 4 else commission / 4  END ,2)) estCommissionRoom  ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx w ON w.wxid = g.wxid";
        $sql .= " LEFT JOIN (select member_id,sum(amount) as groupCommission";
        $sql .= " from ";
        $sql .= " sline_community_new_commission where is_delete=1 group by member_id) scoc";
        $sql .= " ON scoc.member_id =g.mid ";
        $sql .= " WHERE  {$where}";
        $sql .= " group by g.mid {$orderBy} limit $begin, $pageSize";

        //dd($sql);
        return DB::connection('robot')->select($sql);
    }



    /*
    * 获取用户的群主列表
     */
    public static function getYCAdminGroupList($condition, $begin = 0, $pageSize = 10) {
        $where = self::getYCAdminGroupListWhere($condition);

        $sql = "SELECT g.mid,r.id,count(DISTINCT(g.id)) as groupCount,r.status,m.type";
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " LEFT JOIN sline_community_group_member m ON m.mid = g.mid";
        $sql .= " WHERE  {$where}";
        $sql .= " group by g.mid limit $begin, $pageSize";
//        dd($sql);
        return DB::connection('robot')->select($sql);
    }

    /*
    * 获取用户的群主列表
     */
    public static function getAdminGroupListV2($condition, $begin = 0, $pageSize = 10) {
        $where = self::getAdminGroupListWhereV2($condition);
        $orderBy = self::getAdminGroupListOrderByV2($condition);
        $orderWhere = self::getAdminGroupListOrderWhereV2($condition);

        $sql = "SELECT g.mid as groupMid,g.robot_status,w.wxid_type,w.bind_mid,w.mid as rmid,w.deleted_at,";
        $sql .= " count(DISTINCT(g.id)) as groupCount,sum(o.orderAmountRoom) orderAmount ,o.channel,";
        $sql .= " sum(o.orderNumRoom) orderNum ,g.mobile as adminName,scoc.groupCommission,";
        $sql .= "sum(o.estCommissionRoom) estCommission ";//
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select room_id,sum(actual_price) orderAmountRoom,channel,count(id) orderNumRoom,good_type,";
        $sql .= "sum(TRUNCATE(CASE WHEN good_type = 1 THEN ( goods_price_member - goods_price_buy - goods_coupon_average ) / 4 else commission / 4  END ,2)) estCommissionRoom  ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx w ON w.wxid = g.wxid";
        $sql .= " LEFT JOIN (select member_id,sum(amount) as groupCommission";
        $sql .= " from ";
        $sql .= " sline_community_new_commission where is_delete=1 group by member_id) scoc";
        $sql .= " ON scoc.member_id =g.mid ";
        $sql .= " WHERE  {$where}";
        $sql .= " group by g.mid {$orderBy} limit $begin, $pageSize";

        return DB::connection('robot')->select($sql);
    }



    /*
    * 获取用户的群主列表
     */
    public static function getAdminGroupListV3($condition) {
        $where = self::getAdminGroupListWhereV3($condition);
        $orderWhere = self::getAdminGroupListOrderWhereV3($condition);


        $sql = "SELECT g.mid as groupMid,g.robot_status,w.wxid_type,w.bind_mid,w.mid as rmid,w.deleted_at,";
        $sql .= " count(DISTINCT(g.id)) as groupCount,sum(o.orderAmountRoom) orderAmount ,o.channel,";
        $sql .= " sum(o.orderNumRoom) orderNum ,g.mobile as adminName,scoc.groupCommission,";
        $sql .= "TRUNCATE(sum(o.orderAmountRoom) / sum(o.orderNumRoom) , 0) orderAvgAmount,";
        $sql .= "sum(o.estCommissionRoom) estCommission ";//
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select room_id,sum(actual_price) orderAmountRoom,channel,count(id) orderNumRoom,good_type,";
        $sql .= "sum(TRUNCATE(CASE WHEN good_type = 1 THEN ( goods_price_member - goods_price_buy - goods_coupon_average ) / 4 else commission / 4  END ,2)) estCommissionRoom  ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx w ON w.wxid = g.wxid";
        $sql .= " LEFT JOIN (select member_id,sum(amount) as groupCommission";
        $sql .= " from ";
        $sql .= " sline_community_new_commission where is_delete=1 group by member_id) scoc";
        $sql .= " ON scoc.member_id =g.mid ";
        $sql .= " WHERE  {$where}";


        return DB::connection('robot')->selectOne($sql);
    }


    public static function getAdminGroupListOrderWhere($condition)
    {
        $where = 'pay_status = 1 ';
        if (isset($condition['timeType']) && $condition['timeType'] == 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d H:i:s', $condition['beginTime']));
                $end = strtotime(date('Y-m-d H:i:s', $condition['endTime']));
                if($begin==$end){
                    $end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                }else{
                    $end = strtotime(date('Y-m-d H:i:s', $condition['endTime']));
                }
                $where .= "pay_time between {$begin} and {$end}";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d H:i:s', $condition['beginTime']));
                $where .= "pay_time >= {$begin}";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $end = strtotime(date('Y-m-d H:i:s', $condition['endTime']));
                $where .= "pay_time <= {$end}";
            }
        }
        return $where;
    }

    public static function getAdminGroupListOrderWhereV2($condition)
    {
        $where = 'pay_status = 1 ';
        if (isset($condition['timeType']) && $condition['timeType'] == 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d H:i:s', $condition['beginTime']));
                $end = strtotime(date('Y-m-d H:i:s', $condition['endTime']));
                if($begin==$end){
                    $end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                }else{
                    $end = strtotime(date('Y-m-d H:i:s', $condition['endTime']));
                }
                $where .= "pay_time between {$begin} and {$end}";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $begin = strtotime(date('Y-m-d H:i:s', $condition['beginTime']));
                $where .= "pay_time >= {$begin}";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                $end = strtotime(date('Y-m-d H:i:s', $condition['endTime']));
                $where .= "pay_time <= {$end}";
            }
        }
        return $where;
    }


    public static function getAdminGroupListOrderWhereV3($condition)
    {
        $where = 'pay_status = 1 ';
        if (isset($condition['timeType']) && $condition['timeType'] == 'order') {
            if (isset($condition['beginTime']) && isset($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$begin = strtotime(date('Y-m-d 00:00:00', $condition['beginTime']));
                $begin = strtotime($condition['beginTime']);
                //$end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                $end = strtotime($condition['endTime']);
                $where .= "pay_time between {$begin} and {$end}";
            } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$begin = strtotime(date('Y-m-d 00:00:00', $condition['beginTime']));
                $begin = strtotime($condition['beginTime']);
                $where .= "pay_time >= {$begin}";
            } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
                $where = $where ? $where . ' and ' : $where;
                //$end = strtotime(date('Y-m-d 23:59:59', $condition['endTime']));
                $end = strtotime($condition['endTime']);
                $where .= "pay_time <= {$end}";
            }
        }
        return $where;
    }

    /*
     * 8066
    * 获取用户的群主列表
     */
    public static function getAdminGroupListCount($condition) {
        $where = self::getAdminGroupListWhere($condition);
        $orderWhere = self::getAdminGroupListOrderWhere($condition);
        $sql = "SELECT count(*) as cnt";
        $sql .= " FROM (SELECT count(*) as groupCount";
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select * ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx w ON w.wxid = g.wxid";
        $sql .= " LEFT JOIN (select member_id";
        $sql .= " from ";
        $sql .= " sline_community_new_commission where is_delete=1 group by member_id) scoc";
        $sql .= " ON scoc.member_id =g.mid ";
        $sql .= " WHERE  {$where}";
        $sql .= " group by g.mid ) n";

        return DB::select($sql);
    }


    /*
     * 8066
    * 获取用户的群主列表
     */
    public static function getYCAdminGroupListCount($condition) {
        $where = self::getYCAdminGroupListWhere($condition);
        $sql = "SELECT count(*) as cnt";
        $sql .= " FROM (SELECT count(*) as groupCount";
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN sline_community_white_list_wx r ON r.wxid = g.wxid";
        $sql .= " WHERE  {$where}";
        $sql .= " group by g.mid ) n";
        $result = DB::selectOne($sql);
        return $result ? $result->cnt : 0;
    }


    /*
     * 8066
    * 获取用户的群主列表
     */
    public static function getAdminGroupListCountV2($condition) {
        $where = self::getAdminGroupListWhereV2($condition);
        $orderWhere = self::getAdminGroupListOrderWhereV2($condition);
        $sql = "SELECT count(*) as cnt";
        $sql .= " FROM (SELECT count(*) as groupCount";
        $sql .= " FROM sline_community_member_info_by_appid g";
        $sql .= " LEFT JOIN (select * ";
        $sql .= " from ";
        $sql .= config('community.oldMallOrderGoods')." where {$orderWhere} group by room_id) o";
        $sql .= " ON g.id = o.room_id ";
        $sql .= " LEFT JOIN sline_community_white_list_wx w ON w.wxid = g.wxid";
        $sql .= " LEFT JOIN (select member_id";
        $sql .= " from ";
        $sql .= " sline_community_new_commission where is_delete=1 group by member_id) scoc";
        $sql .= " ON scoc.member_id =g.mid ";
        $sql .= " WHERE  {$where}";
        $sql .= " group by g.mid ) n";

        return DB::select($sql);
    }


    /*
    * 获取用户的群主实际分佣
     */
    public static function getAdminGroupTrueAmount($ids,$member_id = 0) {

        $sql = "select sum(amount) as commission";
        $sql.= " FROM sline_community_new_commission c";
        $sql.= " LEFT JOIN sline_community_mall_order_goods o on o.ordersn = c.order_sn";
        $sql.= " LEFT JOIN sline_community_member_info_by_appid g on g.id = o.room_id";
        $sql.= " where c.member_id = {$member_id} and g.id in ({$ids})" ;

        $result =  DB::connection('robot')->selectOne($sql);

        return empty($result->commission)?0:$result->commission;
    }


    /*
    * 获取用户的机器人列表
    */
    public static function getRobotList($condition, $pageSize) {
        $fields = ['id', 'name', 'wxid'];
        $where = [];
        if (isset($condition['mid']) && !empty($condition['mid'])) {
            $where['mid'] = $condition['mid'];
        }
        return DB::connection('robot')->table('member_info_by_appid')
            ->select($fields)
            ->where($where)
            ->groupBy('wxid')
            ->paginate($pageSize)
            ->toArray();
    }

    /*
    * 获取用户的机器人列表
    */
    public static function getGroupIds($wxId) {
        $fields = ['id'];
        $where['wxid'] = $wxId;
        return DB::connection('robot')->table('member_info_by_appid')
            ->select($fields)
            ->where($where)
            ->get()->toArray();
    }

    /*
    * 获取用户的机器人列表
    */
    public static function getGroupMemberList($param, $pageSize) {
        $where = ['romm_id' => $param['roomId'], 'is_delete' => 0];
        $fields = ['id', 'romm_id','wxid', 'nickname', 'head_img', 'sex', 'country', 'province', 'city', 'integral_account', 'wx_alias', 'room_nickname', 'created_at'];
        $query = DB::connection('robot')->table('member_weixin_info')->select($fields)->where($where);
        if (isset($param['nickname']) && !empty($param['nickname'])) {
            $query = $query->where('nickname','like','%'.$param['nickname'].'%');
        }
        return $query->paginate($pageSize)->toArray();
    }

    /*
    * 获取用户的机器人列表
    */
    public static function searchGroupMemberList($nickname, $pageSize) {
        $fields = ['nickname', 'head_img', 'sex', 'country', 'province', 'city', 'wx_alias', 'room_nickname', 'created_at'];
        return DB::connection('robot')->table('member_weixin_info')
            ->select($fields)
            ->where('nickname','like','%'.$nickname.'%')
            ->paginate($pageSize)->toArray();
    }

    /*
     * 有效订单
     */
    public static function getOrderNum($param)
    {
        $where['room_id'] = $param['roomId'];
        //已支付的有效订单
        $where['pay_status'] = 1;
//        $query = DB::connection('mysql_yuelvhui')->table('mall_order_goods');
        $query = DB::connection('robot')->table('mall_order_goods');
        if (is_array($param['roomId'])) {
            $query = $query->whereIn('room_id', $param['roomId']);
        } else {
            $query = $query->where($where);
        }
        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $query = $query->whereBetween('created_at', [$param['beginTime'], $param['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }

    /*
     * 浏览次数商品数量
     */
    public static function getBrowseGoodsNum($condition = []) {
        $where['romm_id'] = $condition['roomId'];
        $query = DB::connection('robot')->table('room_activation');
        if (is_array($condition['roomId'])) {
            $query = $query->whereIn('romm_id', $condition['roomId']);
        } else {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }


    /*
     * 浏览次数人数数量
     */
    public static function getBrowseUserNum($condition = []) {
        $where['romm_id'] = $condition['roomId'];
        $query = DB::connection('robot')->table('goods_browse');
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (is_array($condition['roomId'])) {
            $query = $query->whereIn('romm_id', $condition['roomId']);
        } else {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }

    /*
     * 消息数量
     */
    public static function getMessageTotalNum($condition = []) {
        $where = ['is_send' => 1];
        $where['room_wxid'] = $condition['roomWxId'];
        $query = DB::connection('robot')->table('report_msg');
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }


    /*
     * 素材条数
     */
    public static function getFodderTotalNum($condition = []) {
        $where['room_wxid'] = $condition['roomWxId'];
        $query = DB::connection('robot')->table('send_room_reload');
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }

    /*
     * 链接点击量
     */
    public static function getClickTotalNum($condition = []) {
        $where['romm_id'] = $condition['roomId'];
        $query = DB::connection('robot')->table('room_activation');
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }


    /*
     * 获取群的用户数量
     */
    public static function getGroupUserCount($condition = [], $sex = '') {
        $where = [];
        if (!empty($sex)) {
            if ($sex == 'man') {
                $where['sex'] = 1;
            } else {
                $where['sex'] = 0;
            }
        }
        $where['delete_time'] = 0;
        $query = DB::connection('robot')->table('member_weixin_info');
        if (isset($condition['roomId']) && !empty($condition['roomId'])) {
            if (is_array($condition['roomId'])) {
                $query = $query->whereIn('romm_id', $condition['roomId']);
            } else {
                $where['romm_id'] = $condition['roomId'];
                $query = $query->where($where);
            }
        } else {
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where['mid'] = $condition['mid'];
            }
        }
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $query = $query->whereBetween('created_at', [$condition['beginTime'], $condition['endTime']]);
        }else if(isset($condition['beginTime'])&&empty($condition['endTime'])){
            $query = $query->Where('created_at','>=', $condition['beginTime']);
        }else if(isset($condition['endTime'])&&empty($condition['beginTime'])){
            $query = $query->Where('created_at','<=',$condition['endTime']);
        }
        return $query->count();
    }


    /*
     * 获取群的用户数量
     */
    public static function getYCloudGroupUserCount($roomId, $sex = '') {
        $where = [];
        if (!empty($sex)) {
            if ($sex == 'man') {
                $where['sex'] = 1;
            } else {
                $where['sex'] = 0;
            }
        }
        $where['delete_time'] = 0;
        $query = DB::connection('robot')->table('member_weixin_info');
        $where['romm_id'] = $roomId;
        if (!empty($where)) {
            $query = $query->where($where);
        }
        return $query->count();
    }

    /*
    * 获取群的用户数量
    */
    public static function getGroupUserCountByRobot($condition = [], $sex = '') {
        if (isset($condition['wxId']) && !empty($condition['wxId'])) {
            $where['wxid'] = $condition['wxId'];
        }
        if (!empty($sex)) {
            if ($sex == 'man') {
                $where['sex'] = 1;
            } else {
                $where['sex'] = 0;
            }
        }
        $query = DB::connection('robot')->table('member_weixin_info');
        if (!empty($where)) {
            $query = $query->where($where);
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $beginTime = date('Y-m-d 00:00:00', $condition['beginTime']);
            $endTime = date('Y-m-d 00:00:00', $condition['endTime']);
            $query = $query->whereBetween('created_at', [$beginTime, $endTime]);
        }
        return $query->distinct('mid')->count();
    }

    /*
     * 获取群的用户数量
     */
    public static function getRobotName($wxId) {
        $where = ['id' => $wxId];
        return DB::connection('robot')->table('white_list_wx')->where($where)->first();
    }

    /*
       * 获取当前群绑定的群主信息
       */
    public static function getBindGroupMidData($condition = []) {

        $sql = "select    scm.name,scm.mobile,scm.nick_name as truename,scm.nick_name as nickname,scm.code_number,scm.mid  
               from sline_community_member_info_by_appid   as scm  where scm.id={$condition['id']}";
        return DB::connection('robot')->selectOne($sql);

    }

    /**
     * 根据手机号获去详细信息
     * @param array $condition
     * @return mixed
     */
    public static function getMemberInfo($condition = []) {
        $sql = "select    sm.mobile,sm.truename,sm.nickname,sic.code_number,sm.mid  from   yuelvhui.sline_member   as sm  
               left join  yuelvhui.sline_invitation_code   as sic   on sm.mid=sic.mid
               where sm.mobile={$condition['mobile']} ";
        return DB::connection('robot')->selectOne($sql);
    }

    /**
     * 更新群的 mid
     */
    public  static  function  updatedGroupMid($mid,$id,$code_number='',$nick_name,$mobile)
    {
        $bind_mid_time= date('Y-m-d H:i:s',time());
        $sql = "update sline_community_member_info_by_appid set mid=?,bind_mid_time=?,code_number=?,nick_name=?,mobile=?  where id = ?";
        return DB::connection('robot')->update($sql, [$mid, $bind_mid_time,$code_number,$nick_name,$mobile,$id]);
    }


    /**
     * 更新群的 mid
     */
    public  static  function  bindYCloudGroupMid($roomId, $update)
    {
        $update['bind_mid_time'] = date('Y-m-d H:i:s',time());
        $where = [
            'id' => $roomId
        ];
        return DB::connection('robot')->table('member_info_by_appid')->where($where)->update($update);
    }


    /**
     * 更新群的 mid
     */
    public  static  function  bindYCloudRobot($roomId, $update)
    {
        $where = [
            'id' => $roomId
        ];
        return DB::connection('robot')->table('white_list_wx')->where($where)->update($update);
    }

    /**
     *
     */
    public  static function getGroupData($id)
    {
        $where = ['id' => $id];
        return DB::connection('robot')->table('member_info_by_appid')->select(['id','mid','member_count'])->where($where)->first();
    }

    /**
     * 当前数据
     * @param $room_id
     * @return int
     */
    public  static function getSurplusDays($room_id){
        $sql = "select count(*) count_1 from sline_community_invitation_group where room_id={$room_id} and group_flag>0";

        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count_1 : 0;
    }

    /**
     * 获取群主分佣明细
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommission($param){

        $where = self::getGroupAdminCommissionWhere($param);
        $orderBy = self::getGroupAdminCommissionBy($param);

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $sql = "select c.order_sn,c.amount as true_amount,o.buyer_phone,c.good_type,o.actual_price,c.channel,o.pay_time,m.mobile,m.nickname,o.goods_name,o.goods_cover_image";

        $sql .=",TRUNCATE(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) / 4 / 100 ELSE o.commission / 4/ 100 END ,2) lirun";

        $sql .=" FROM sline_community_new_commission c";

        $sql .=" LEFT JOIN sline_community_mall_order_goods o on o.ordersn = c.order_sn";

        $sql .=" LEFT JOIN yuelvhui.sline_member m on m.mid = c.member_id";

        $sql .=" where {$where} {$orderBy} limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }


    /**
     * 获取群主每天分佣
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionEveryDay($param){

        $where = self::getGroupAdminCommissionWhereV2($param);
        $orderBy = self::getGroupAdminCommissionBy($param);

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $sql = "select DATE_FORMAT(FROM_UNIXTIME(o.pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as day,sum(c.amount) as true_amount,o.channel,g.mobile,g.mid,g.nick_name as nickname";

        $sql .=",count(distinct(o.id)),sum(TRUNCATE(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) / 100 ELSE o.commission / 100 END ,2)) as lirun";

        $sql .=" FROM sline_community_mall_order_goods o";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on g.id = o.room_id and o.parent_id=g.mid";

        $sql .=" LEFT JOIN sline_community_new_commission c on c.member_id = g.mid and  o.ordersn = c.order_sn";

        $sql .=" where {$where} group by g.mid,day {$orderBy} limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }


    /**
     * 获取悦淘群主每天分佣
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionEveryDayForYT($param){

        $where = self::getGroupAdminCommissionWhereV3($param);
        $orderBy = self::getGroupAdminCommissionByV3($param);

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $sql = "SELECT b.amount as lirun,m.nick_name as nickname,b.order_no as order_no,m.mobile,b.created_at as day,b.status,b.channel FROM sline_community_bonus_becommitted b";
        $sql.= " LEFT JOIN sline_community_group_member m on m.mid=b.mid";
        $sql.= " WHERE b.status<2 and b.identity<6 $where $orderBy limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }

    /**
     * 获取悦淘群主每天总分佣
     * @param $param
     * @return array
     */
    public static function getGroupAdminTotalCommissionEveryDayForYTCount($param){

        $where = self::getGroupAdminCommissionWhereV3($param);

        $sql = "SELECT SUM(b.amount) as lirun FROM sline_community_bonus_becommitted b";
        $sql.= " LEFT JOIN sline_community_group_member m on m.mid=b.mid";
        $sql.= " WHERE b.status<2 $where";

        $result = DB::selectOne($sql);

        return !empty($result->lirun)?$result->lirun:0;

    }

    /**
     * 获取悦淘群主每天分佣
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionEveryDayForYTCount($param){

        $where = self::getGroupAdminCommissionWhereV3($param);

        $sql = "SELECT count(*) as count_1 FROM sline_community_bonus_becommitted b";
        $sql.= " LEFT JOIN sline_community_group_member m on m.mid=b.mid";
        $sql.= " WHERE b.status<2 $where";

        $result = DB::selectOne($sql);

        return !empty($result->count_1)?$result->count_1:0;

    }


    /**
     * 获取群主每天分佣
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionEveryDayCount($param){

        $where = self::getGroupAdminCommissionWhereV2($param);

        $sql = "select count(*) as count_1";

        $sql .=" FROM sline_community_mall_order_goods o";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on g.id = o.room_id and o.parent_id=g.mid";

        $sql .=" LEFT JOIN sline_community_new_commission c on c.member_id = g.mid and  o.ordersn = c.order_sn";

        $sql .=" where {$where} group by g.mid,DATE_FORMAT(FROM_UNIXTIME(o.pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d')  ";

        $result = DB::select($sql);

        return count($result);

    }



    /**
     * 获取群主每天分佣
     * @param $param
     * @return array
     */
    public static function getGroupAdminTotalCommissionEveryDayCount($param){

        $where = self::getGroupAdminCommissionWhereV2($param);

        $sql = "select sum(TRUNCATE(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) / 100 ELSE o.commission / 100 END ,2)) as lirun";
//        $sql = "select sum(TRUNCATE(CASE WHEN o.channel <> 4 THEN ";
//        $sql .= "(CASE WHEN o.good_type = 1 THEN ";
//        $sql .= "( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) / 100  / 4 ELSE o.commission / 100 / 4 END) ";
//        $sql .= "ELSE (CASE WHEN o.good_type <> 1 THEN ";
//        $sql .= "( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) / 100 ELSE o.commission / 100 END) END ,2)) as lirun";

        $sql .=" FROM sline_community_mall_order_goods o";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on g.id = o.room_id and o.parent_id=g.mid";

        $sql .=" LEFT JOIN sline_community_new_commission c on c.member_id = g.mid and  o.ordersn = c.order_sn";

        $sql .=" where {$where}";

        $result = DB::selectOne($sql);

        return !empty($result->lirun)?$result->lirun:0;

    }

    /**
     * 获取群主分佣明细
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionWhere($param){

        $where = " g.is_delete=0 and g.mid>0";

        if(isset($param['mobile'])&&!empty($param['mobile'])){

            $where .=" and m.mobile = ".$param['mobile'];
        }

        if(isset($param['startTime'])&&$param['startTime']!=''){

            $where .=" and o.pay_time>".$param['startTime'];
        }

        if(isset($param['endTime'])&&$param['endTime']!=''){

            $param['endTime'] = $param['startTime']==$param['endTime']?$param['endTime']+3600:$param['endTime'];

            $where .=" and o.pay_time<".$param['endTime'];
        }

        if(isset($param['channel'])&&!empty($param['channel'])){

            $where .=" and o.channel = ".$param['channel'];
        }

        if(isset($param['good_type'])&&!empty($param['good_type'])){

            $where .=" and o.good_type = ".$param['good_type'];
        }

        if(isset($param['member_id'])&&!empty($param['member_id'])){

            $where .=" and c.member_id = ".$param['member_id'];
        }

        if(isset($param['order_sn'])&&!empty($param['order_sn'])){

            $where .=" and c.order_sn = ".$param['order_sn'];
        }

        return $where;

    }

    /**
     * 获取群主分佣明细
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionWhereV2($param){

        $where = " g.is_delete=0 and g.mid>0";

        if(isset($param['mobile'])&&!empty($param['mobile'])){

            $where .=" and g.mobile = ".$param['mobile'];
        }

        if(isset($param['startTime'])&&$param['startTime']!=''){

            $where .=" and o.pay_time>".$param['startTime'];
        }

        if(isset($param['endTime'])&&$param['endTime']!=''){

            $param['endTime'] = $param['startTime']==$param['endTime']?$param['endTime']+3600:$param['endTime'];

            $where .=" and o.pay_time<".$param['endTime'];
        }

        if(isset($param['channel'])&&!empty($param['channel'])){

            $where .=" and o.channel = ".$param['channel'];
        }

        if(isset($param['good_type'])&&!empty($param['good_type'])){

            $where .=" and o.good_type = ".$param['good_type'];
        }

        if(isset($param['member_id'])&&!empty($param['member_id'])){

            $where .=" and g.mid = ".$param['member_id'];
        }

        if(isset($param['order_sn'])&&!empty($param['order_sn'])){

            $where .=" and o.ordersn = ".$param['order_sn'];
        }

        return $where;

    }


    /**
     * 获取群主分佣明细
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionWhereV3($param){

        $where = "";

        if(isset($param['mobile'])&&!empty($param['mobile'])){

            $where .=" and m.mobile = ".$param['mobile'];
        }

        if(isset($param['startTime'])&&$param['startTime']!=''){

            $where .=" and b.created_at>'".date("Y-m-d H:i:s",$param['startTime'])."'";
        }

        if(isset($param['endTime'])&&$param['endTime']!=''){

            $param['endTime'] = $param['startTime']==$param['endTime']?$param['endTime']+3600:$param['endTime'];

            $where .=" and b.created_at<'".date("Y-m-d H:i:s",$param['endTime'])."'";
        }

        if(isset($param['channel'])&&!empty($param['channel'])){

            $where .=" and b.channel = ".$param['channel'];
        }

        if(isset($param['good_type'])&&!empty($param['good_type'])){

            $where .=" and b.product_type = ".$param['good_type'];
        }

        if(isset($param['member_id'])&&!empty($param['member_id'])){

            $where .=" and b.mid = ".$param['member_id'];
        }

        if(isset($param['order_sn'])&&!empty($param['order_sn'])){

            $where .=" and b.order_no = ".$param['order_sn'];
        }

        return $where;

    }


    /**
     * 获取群主分佣明细排序
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionBy($param){

        $order = " o.pay_time";

        $by = " desc";

        $array = ['lirun','pay_time','true_amount','actual_price'];

        if(isset($param['order'])&&in_array($param['order'], $array)){

            $order = " ".$param['order'];
        }

        if(isset($param['by'])&&$param['by']==2){

            $by = " asc";
        }

        return " order by ".$order.$by;

    }


    /**
     * 获取群主分佣明细排序
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionByV3($param){

        $order = " day";

        $by = " desc";

        $array = ['lirun','true_amount'];

        if(isset($param['order'])&&in_array($param['order'], $array)){

            $order = " ".$param['order'];

            if($param['order']=='true_amount'){

                $order = " b.status,lirun";
            }
        }

        if(isset($param['by'])&&$param['by']==2){

            $by = " asc";
        }

        return " order by ".$order.$by;

    }



    /**
     * 获取群主分佣明细
     * @param $param
     * @return array
     */
    public static function getGroupAdminCommissionCount($param){

        $where = self::getGroupAdminCommissionWhere($param);

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $sql = "select count(*) as count_1";

        $sql .=" FROM sline_community_new_commission c";

        $sql .=" LEFT JOIN sline_community_mall_order_goods o on o.ordersn = c.order_sn";

        $sql .=" LEFT JOIN yuelvhui.sline_member m on m.mid = c.member_id";

        $sql .=" where {$where}";

        $result = DB::selectOne($sql);

        return isset($result->count_1)?$result->count_1:0;

    }


    /**
     * 按日期获取社群总数
     *
     * @param $param
     * @return array
     */
    public static function getVipGroupByTime($param)
    {

        $where = "";

        $where = "is_delete = 0";

        if (isset($param['beginTime']) && isset($param['endTime'])) {
            $param['beginTime'] = strtotime($param['beginTime']);
            $param['endTime'] = strtotime($param['endTime']);
            $where .= " and created_time between '{$param['beginTime']}' and '{$param['endTime']}' ";
        }

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .= " and platform_identity='{$param['platform_identity']}'";
        }

        $sql = "select count(*) as count_1 from sline_community_vip_group_log where {$where}";
        $result = DB::connection('robot')->selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;

    }


    /**
     * 获取群自定义标签列表
     *
     * @param $param
     * @return array
     */
    public static function getGroupTagsList($param){


        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $where = "";

        if(isset($param['categoryName'])&&!empty($param['categoryName'])){

            $where = " and name like '%".$param['categoryName']."%'";
        }

        $sql = "select * from sline_community_group_tag where is_delete=0 {$where} order by sort desc limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);
    }


    /**
     * 获取群自定义标签列表
     *
     * @param $param
     * @return array
     */
    public static function getGroupTagsCount($param){

        $where = "";

        if(isset($param['categoryName'])&&!empty($param['categoryName'])){

            $where = " and name like '%".$param['categoryName']."%'";
        }

        $sql = "select count(*) as count_1 from sline_community_group_tag where is_delete=0 {$where}";

        $result = DB::selectOne($sql);

        return !empty($result->count_1)?$result->count_1:0;
    }

    /**
     * 查询标签信息
     *
     * @param $param
     * @return array
     */
    public static function getGroupTagInfo($param){

        $where = "";

        if(isset($param['categoryName'])&&!empty($param['categoryName'])){

            $where = " and name = '".$param['categoryName']."'";
        }       

        $sql = "select * from sline_community_group_tag where is_delete=0 {$where}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);       

    }
    /**
     * 添加标签信息
     *
     * @param $param
     * @return array
     */
    public static function addGroupTags($param){

        return DB::table('group_tag')->insertGetId($param);
    }

    /**
     * 获取标签信息
     *
     * @param $param
     * @return array
     */
    public static function getTagInfo($param){

        $tagInfo =  DB::table('group_tag')->where('id',$param['id'])->get();

        return $tagInfo;
    }



    /**
     * 编辑标签信息
     *
     * @param $param
     * @return array
     */
    public static function editGroupTags($param){

        return DB::table('group_tag')->where('id',$param['id'])->update($param);
    }

}