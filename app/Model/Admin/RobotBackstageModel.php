<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RobotBackstageModel extends Model{

    /*
     * 服务项目列表
     */
    public static function getServiceList()
    {

        return DB::table('service')
            -> where('is_delete',0)
            -> orderBy('sort')
            -> get()
            -> toArray();
    }



    /*
     * 服务项目添加
     */
    public static function create($param = [])
    {
        return DB::table('service')
            -> insert($param);
    }

    /*
     * 服务项目修改
     */
    public static function updateService($param = [])
    {   
        return DB::table('service')
            -> where('id',$param['id'])
            -> update($param);
    }


    /*
     * 服务项目删除
     */
    public static function delService($param)
    {

        $data['is_delete'] = 1;

        return DB::table('service')
            -> where('id',$param['id'])
            -> update($data);        
    }


    public static function getBuyGroupOrderList($param,$page = 1,$pageSize = 10)
    {   
        $page = ($page-1)*10;
        $where = "";
        if(isset($param['mobile'])&&!empty($param['mobile']))
        {
            $where .=" and m.mobile like '%".$param['mobile']."%'";
        }
        $sql = "select o.*,m.nickname,m.mobile from sline_community_buy_info o";
        $sql.= " left join sline_community_user m on m.mid=o.mid";
        $sql.= " where o.pay_status=1 $where limit $page,$pageSize";

        return DB::select($sql);
    }


    public static function getBuyGroupOrderCount($param)
    {   
        $where = "";
        if(isset($param['mobile'])&&!empty($param['mobile']))
        {
            $where .=" and m.mobile like '%".$param['mobile']."%'";
        }
        $sql = "select count(*) count from sline_community_buy_info o";
        $sql.= " left join sline_community_user m on m.mid=o.mid";
        $sql.= " where o.pay_status=1 $where";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->count:0;
    }
}