<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\UserModel as AUserModel;
use Illuminate\Support\Facades\DB;
use App\Models\WhiteListWx;

class GroupManageModel extends Model
{

    /*
     * 查询审核列表
     */
    public static function getGroupVerifyList($param, $page = 1, $pageSize = 10)
    {

        $where = self::getCommunityInvitationGroupWhere($param);

        $page = ($page - 1) * $pageSize;

        $sql = "select g.*,g.phone as mobile from sline_community_invitation_group g";
        if (isset($param['mobile']) && $param['mobile'] > 0) {
            $where .= " and g.phone like '%" . $param['mobile'] . "%'";

        }
        $sql .= " where g.examine_status > 0 $where order by g.id desc limit $page,$pageSize";

        return DB::select($sql);
    }

    /*
     * 查询审核列表总条数
     */
    public static function getGroupVerifyListCount($param)
    {

        $where = self::getCommunityInvitationGroupWhere($param);
        $sql = "select count(*) count from sline_community_invitation_group g";
        if (isset($param['mobile']) && $param['mobile'] > 0) {
            $where .= " and g.phone like '%" . $param['mobile'] . "%'";

        }

        $sql .= " where g.examine_status > 0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }


    public static function getCommunityInvitationGroupCountInfo($param)
    {

        $where = self::getCommunityInvitationGroupCountWhere($param);
        $sql = "select * from sline_community_invitation_group_member_count where deleted_at = 0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }


    public static function addCommunityInvitationGroupCount($data)
    {
        return DB::table("invitation_group_member_count")->insert($data);
    }

    public static function upCommunityInvitationGroupCount($where, $data)
    {
        return DB::table("invitation_group_member_count")->where($where)->update($data);
    }


    public static function getMemberShipInfo($param)
    {
        $where = '';

        if (isset($param['mid']) || !empty($param['mid'])) {
            $where .= " and mid = {$param['mid']}";
        }

        if ($where) {
            $sql = "select * from yuelvhui.sline_member_ship where delete_time = 0 {$where}";
            $result = DB::selectOne($sql);
        }

        return !empty($result) ? $result : '';
    }

    public static function addCommunityInvitationGroupCountInfo($data)
    {
        return DB::table("invitation_group_member_count_info")->insert($data);
    }


    public static function getCommunityInvitationGroupCountWhere($param = [])
    {
        $where = '';

        if (isset($param['id']) || !empty($param['id'])) {
            $where .= " and id = {$param['id']}";
        }

        if (isset($param['mid']) || !empty($param['mid'])) {
            $where .= " and mid = {$param['mid']}";
        }

        return $where;
    }


    /**
     * 更改审核群状态
     * @param timestamp
     * @return string
     */
    public static function upCommunityInvitationGroup($where, $data)
    {

        return DB::table("invitation_group")->where('id', $where)->update($data);

    }

    /*
     * 审核群详情
     * @param Array
     */
    public static function getGroupVerifyInfo($param, $field = "*")
    {
        $where = self::getCommunityInvitationGroupWhere($param);
        $sql = "select $field from sline_community_invitation_group g where g.examine_status > 0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }

    /*
     * 审核群详情
     * @param Array
     */
    public static function getGroupVerifyInfoV1($param, $field = "*")
    {

        $where = '';

        if (isset($param['id']) || !empty($param['id'])) {
            $where .= " and id = {$param['id']}";
        }

        $sql = "select $field from sline_community_invitation_group where examine_status > 0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }

    /*
 * 获取用户订单总数
 */
    public static function getOrderTotalNumGroupDate($condition = [], $format = 'day')
    {
        $where = self::getOrderTotalNumGroupWhere($condition);
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }
        $sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(o.created_at), '{$format}') as date,COUNT(*) as orderNum,SUM(actual_price) as orderAmount FROM " . config('community.oldMallOrderGoods') . " o";
        $sql .= " LEFT JOIN sline_community_member_info_by_appid g on o.room_id = g.id and o.room_id>0";
        $sql .= " LEFT JOIN sline_community_white_list_wx r on r.wxid = g.wxid";
        //$sql .= " where {$where} group by date {$having} {$orderBy} limit $begin,$pageSize";
        $sql .= " where {$where} group by date";
        return DB::select($sql);
    }


    public static function getOrderTotalNumGroupWhere($condition)
    {
        //订单支付, 有效订单
        $where = 'o.pay_status = 1 ';
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = strtotime($condition['beginTime']);
            $end = strtotime($condition['endTime']);
            $where .= "o.pay_time between {$begin} and {$end}";
        }
        if (isset($condition['channel']) && !empty($condition['channel'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= " o.channel = {$condition['channel']}";
        }
        if (isset($condition['goodType']) && !empty($condition['goodType'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= " o.good_type = {$condition['goodType']}";
        }
        if (isset($condition['wxAliasString']) && !empty($condition['wxAliasString'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= " r.wxid  in ({$condition['wxAliasString']})";
        }
        return $where;
    }


    public static function getRobotGroupInfoGraphWhere($condition)
    {
        //订单支付, 有效订单
        $where = 'is_delete = 0 ';
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = $condition['beginTime'];
            $end = $condition['endTime'];
            $where .= "g.created_at between '{$begin}' and '{$end}'";
        }
//        if(isset($condition['channel']) && !empty($condition['channel'])){
//            $where = $where ? $where . ' and ' : $where;
//            $where.=  " o.channel = {$condition['channel']}";
//        }
//        if(isset($condition['goodType']) && !empty($condition['goodType'])){
//            $where = $where ? $where . ' and ' : $where;
//            $where.=  " o.good_type = {$condition['goodType']}";
//        }
//        if(isset($condition['wxAliasString']) && !empty($condition['wxAliasString'])){
//            $where = $where ? $where . ' and ' : $where;
//            $where .= " r.wxid  in ({$condition['wxAliasString']})";
//        }
        return $where;
    }


    /*
     * 补全统计日期中没有数据的日期
     */
    public static function completionDateArray($param, $dataKeys, $orderTotalData)
    {
        #计算日期内天数
        $stimestamp = $param['beginTime'];
        $etimestamp = $param['endTime'];
        if ($param['format'] == 'day') {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 86400;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
            }
        } else {
            #计算日期段内有多少天
            $days = ($etimestamp - $stimestamp) / 3600;
            #保存每天日期
            $date = array();
            for ($i = 0; $i <= $days; $i++) {
                //$date[] = date('Y-m-d H', $stimestamp + (3600 * $i));
                $date[] = date('H', $stimestamp + (3600 * $i));
            }
        }
        $data = [];
        #循环补全日期
        foreach ($date as $key => $val) {
            if ($param['format'] != 'day') {
                $val = intval($val);
            }
            foreach ($dataKeys as $v) {
                $data[$val][$v] = 0;
                foreach ($orderTotalData as $item) {
                    if ($item['date'] == $val) {
                        $data[$val][$v] = $item[$v];
                    }
                }
                if ($v == 'orderAmount') {
                    $data[$val][$v] = sprintf("%.2f", $data[$val][$v] / 100);
                }
            }
        }
        return $data;
    }

    /*
     * 审核群搜索
     * @param Array
     */
    public static function getCommunityInvitationGroupWhere($param = [])
    {
        $where = '';

        if (isset($param['id']) || !empty($param['id'])) {
            $where .= " and g.id = {$param['id']}";
        }

        if (isset($param['type']) || !empty($param['type'])) {
            $where .= " and g.type = {$param['type']}";
        }

        if (isset($param['examineStatus']) || !empty($param['examineStatus'])) {
            $where .= " and g.examine_status = {$param['examineStatus']}";
        }

        if (isset($param['platform_identity']) || !empty($param['platform_identity'])) {
            $where .= " and g.platform_identity = {$param['platform_identity']}";
        }

        if (isset($param['startTime']) || !empty($param['startTime'])) {
            $where .= " and g.addtime >= " . strtotime($param['startTime']);
        }

        if (isset($param['endTime']) || !empty($param['endTime'])) {
            $where .= " and g.addtime <= " . strtotime($param['endTime']);
        }

        if (isset($param['twice']) || !empty($param['twice'])) {
            $where .= " and g.upgrade_status > 0";
        }


        return $where;
    }


    /*
     * 群列表
     * @param Array
     */
    public static function getCommunityList($page, $pageSize, $param = [])
    {
        $page = ($page - 1) * $pageSize;

        $where = "";

        if (isset($param['groupName'])) {

            $where .= " and g.name like '%" . $param['groupName'] . "%' ";
        }

        if (isset($param['wxid']) && !empty($param['wxid'])) {
            $where .= " and wx.wxid ='" . $param['wxid'] . "'";
        }

        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= " and wx.platform_identity ='{$param['platform_identity']}'";
        }
        if (isset($param['mid']) && !empty($param['mid'])) {
            $where .= " and wx.bind_mid =" . $param['mid'] . "";
        }


        $sql = "select g.*,wx.id as rid,wx.wxid_type as wxid_type,wx.mid as wxmid,wx.bind_mid as bind_mid,count(o.room_id) count_1,wx.platform_identity from sline_community_member_info_by_appid g";
        $sql .= " left join " . config('community.oldMallOrderGoods') . " o on o.room_id = g.id and o.pay_status=1 and o.room_id>0";
        $sql .= " left join sline_community_white_list_wx wx on wx.wxid = g.wxid";
        $sql .= " where g.is_delete = 0  $where group by g.id order by count_1  desc limit $page,$pageSize";

        return DB::select($sql);
    }

    /*
    * 群主信息列表
    * @param Array
    */
    public static function getHouTaiGroupOwnerList($page = 1, $pageSize = 10, $param = [])
    {
        $begin = ($page - 1) * $pageSize;
        $where = self::getHouTaiAdminGroupListWhere($param);
        $orderBy = self::getHouTaiAdminGroupListOrderBy($param);
        $sql = "SELECT g.id,g.room_wxid,r.wx_alias,g.mid,g.robot_status,g.tag_id,r.order_by as rid,r.wxid_type,r.bind_mid,r.mid as rmid,r.deleted_at, count(*) as groupCount,group_concat(g.id) as ids,sum(o.actual_price) orderAmount,count(o.room_id) orderNum";//
        $sql .= " FROM `sline_community_member_info_by_appid` as g left join " . config('community.oldMallOrderGoods') . " o on o.room_id = g.id left join sline_community_white_list_wx as r on g.wxid = r.wxid where {$where} group by g.mid {$orderBy} limit $begin, $pageSize";
        return DB::connection('robot')->select($sql);
    }


    public static function getHouTaiAdminGroupListWhere($condition)
    {
        $where = 'g.mid <> 0';
        if (!empty($condition['name'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like %{$condition['name']}%";
        }
        if (isset($condition['roomId']) && !empty($condition['roomId']) && count($condition['roomId'])) {
            $where = $where ? $where . ' and ' : $where;
            $roomId = implode(',', $condition['roomId']);
            $where .= "g.id in ({$roomId})";
        }
        if (isset($condition['tag_id']) && !empty($condition['tag_id']) && count($condition['tag_id'])) {
            $where = $where ? $where . ' and ' : $where;
            $tagId = implode(',', $condition['tag_id']);
            $where .= "g.tag_id in ({$tagId})";
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at between '{$begin}' and '{$end}'";
        } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $where .= "g.created_at >= '{$begin}'";
        } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at <= '{$end}'";
        }
        if (isset($condition['user_type']) && $condition['user_type'] >= 2) {
            if (!empty($condition['wx_alias'])) {
                $where = $where ? $where . ' and ' : $where;
                $where .= "r.wx_alias = {$condition['wx_alias']}";
            } else {
                $arr = [];
                foreach ($condition['wxIds'] as $v) {
                    $arr[] = $v;
                }
                if (!empty($arr)) {
                    $where = $where ? $where . ' and ' : $where;
                    $wxIds = implode("','", $arr);
                    $where .= "g.wxid in ('{$wxIds}')";
                }
            }
        } else {
            if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
                $where = $where ? $where . ' and ' : $where;
                $where .= "g.mid = {$condition['searchMid']}";
            } else {
                if (isset($condition['mid']) && !empty($condition['mid'])) {
                    $where = $where ? $where . ' and ' : $where;
                    $where .= "g.mid = {$condition['mid']}";
                }
            }
        }
        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['searchMid']}";
        }
        return $where;
    }


    public static function getHouTaiAdminGroupListOrderBy($condition)
    {
        $orderBy = '';
        if (isset($condition['orderNum']) && !empty($condition['orderNum']) && in_array($condition['orderNum'], ['asc', 'desc'])) {
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if (isset($condition['orderAmount']) && !empty($condition['orderAmount']) && in_array($condition['orderAmount'], ['asc', 'desc'])) {
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }
        if (isset($condition['orderBy']) && $condition['orderBy'] == 2) {
            $orderBy = $orderBy ? $orderBy . ", groupCount asc" : " order by groupCount asc";
        } else {

            $orderBy = $orderBy ? $orderBy . ", groupCount desc" : " order by groupCount desc";
        }
        return $orderBy;
    }


    /*
     * 群主信息总数
     * @param Array
     */
    public static function getHouTaiGroupOwnerCountV1($param = [])
    {

        $where = self::getHouTaiAdminGroupListWhere($param);
        $sql = "SELECT count(*) as cnt FROM (SELECT g.*";
        $sql .= " FROM `sline_community_member_info_by_appid` as g left join " . config('community.oldMallOrderGoods') . " o on o.room_id = g.id left join sline_community_white_list_wx as r on g.wxid = r.wxid where {$where} group by mid) as a";
        return DB::connection('robot')->select($sql);
    }


    /*
     * 群主信息列表
     * @param Array
     */
    public static function getGroupOwnerList($page = 1, $pageSize = 10, $param = [])
    {
        $page = ($page - 1) * $pageSize;

        $where = self::getHouTaiGroupOwnerWhere($param);

        $sql = "select g.*,wx.id as rid,wx.wxid_type as wxid_type,wx.mid as wxmid,count(*) count,wx.bind_mid,wx.platform_identity from sline_community_member_info_by_appid g";
        $sql .= " left join sline_community_white_list_wx wx on wx.wxid = g.wxid";
        $sql .= " where g.is_delete = 0 $where group by g.mid order by g.id limit $page,$pageSize";


        return DB::select($sql);
    }

    /*
     * 群主信息总数
     * @param Array
     */
    public static function getGroupOwnerCountV1($param = [])
    {

        $where = self::getHouTaiGroupOwnerWhere($param);

        $sql = "select g.mid from sline_community_member_info_by_appid g";
        $sql .= " left join sline_community_white_list_wx wx on wx.wxid = g.wxid";
        $sql .= " where g.is_delete = 0 $where group by g.mid";


        $result = DB::select($sql);

        return count($result);
    }


    public static function getHouTaiGroupOwnerWhere($param)
    {
        $where = "";

        if (isset($param['mobile']) && !empty($param['mobile'])) {
            $where .= " and g.mobile like '%" . $param['mobile'] . "%' ";
        }

        if (isset($param['wxid']) && !empty($param['wxid'])) {
            $where .= " and wx.wxid ='" . $param['wxid'] . "'";
        }

        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= " and wx.platform_identity ='{$param['platform_identity']}'";
        }
        if (isset($param['mid']) && !empty($param['mid'])) {
            $where .= " and wx.bind_mid = " . $param['mid'] . "";
        }
        return $where;
    }

    public static function getlimitgroupCount($mid = 0)
    {
        $nowTime = time();

        $sql = "select sum(service_number) count_1 from sline_community_buy_info where deleted_at = 0 and duetime_at<$nowTime and mid = $mid ";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;
    }


    /*
     * 消息列表
     * @param Array
     */
    public static function getMessageList($param = [], $page = 1, $pageSize = 10)
    {
        $page = ($page - 1) * $pageSize;
        $sql = "select * from sline_community_send_reload r";
        $sql .= " left join sline_community_send_info i on i.random_str=r.random_str";
        $sql .= " where r.deleted_at=0 order by r.created_at desc limit $page,$pageSize";

        return DB::select($sql);
    }

    /*
     * 消息列表总数量
     * @param Array
     */
    public static function getMessageCount($roomids = "", $stattime = "", $endTime = "", $mid = 0, $sup_id = 0, $source = '')
    {

        $where = "";

        if ($roomids != "") {
            $where .= "  and r.room_id in ($roomids)";
        }

        if ($stattime != "") {

            $where .= " and r.created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and r.created_at<'$endTime'";
        }

        $sql = "select count(*) count_1 from sline_community_send_room_reload r";

        if ($mid > 0) {

            if ($sup_id > 0) {
                $array = DB::table("white_list_wx")
                    ->where("bind_mid", $mid)
                    ->pluck("wxid")
                    ->toArray();

            } else {

                $array = DB::table("white_list_wx")
                    ->where("mid", $mid)
                    ->where("wxid_type", 1)
                    ->pluck("wxid")
                    ->toArray();

            }

            if (count($array) < 0) {

                return 0;
            }

            $wxids = "'" . implode("','", $array) . "'";

            $sql .= " left join sline_community_member_info_by_appid g on g.id = r.room_id";

            $where .= "  and g.wxid in ($wxids)";
        }

        if ($source != "") {
            $sql .= " left join sline_community_send_info s on s.random_str = r.random_str";
            $where .= "  and s.platform_identity = '" . $source . "'";
        }

        $sql .= " where r.deleted_at=0 $where";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;
    }

    /*
     * 消息列表总数量
     * @param Array
     */
    public static function getMessageCountByTimes($roomids = "", $stattime = "", $endTime = "")
    {

        $where = "";

        if ($roomids != "") {
            $where .= "  and room_id in ($roomids)";
        }

        $sql = "select count(*) as count_1,DATE_FORMAT(created_at,'%Y-%m-%d') as times from sline_community_send_room_reload ";
        $sql .= " where deleted_at=0 and created_at>'$stattime' and created_at<'$endTime' GROUP BY DATE_FORMAT(created_at,'%Y-%m-%d') $where";

        $result = DB::select($sql);

        return $result;
    }


    /*
     * 群总数
     * @param Array
     */
    public static function getGroupMemberCount($roomWxId = 0)
    {
        if (empty($roomWxId)) {
            $query = DB::connection('robot')->table('member_weixin_info');
            return $query->where(['room_id' => $roomWxId])->count();
        }
        return 0;
    }

    /*
     * 群总数
     * @param Array
     */
    public static function getCommunityCount($wxid = "", $today = 0, $mid = 0, $param = [], $stattime = "", $endTime = "")
    {
        $where = "";

        if ($wxid != "") {

            $where .= " and g.wxid = '$wxid'";
        }
        if ($today == 1) {

            $todayTime = date("Y-m-d", time());

            $where .= " and DateDiff('$todayTime',g.created_at)=0";
        }

        if ($mid > 0) {
            $where .= " and g.mid = $mid";
        }

        if (isset($param['groupName'])) {

            $where .= " and g.name like '%" . $param['groupName'] . "%' ";
        }

        if ($stattime != "") {

            $where .= " and g.bind_mid_time>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and g.bind_mid_time<'$endTime'";
        }

        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= " and wx.platform_identity ='{$param['platform_identity']}'";
        }

        if (isset($param['source']) && !empty($param['source'])) {
            $where .= " and wx.platform_identity ='{$param['source']}'";
        }

        if (isset($param['mid']) && !empty($param['mid'])) {
            $where .= " and wx.bind_mid =" . $param['mid'] . "";
        }

        $sql = "select count(*) count from sline_community_member_info_by_appid g";
        $sql .= " left join sline_community_white_list_wx wx on wx.wxid = g.wxid";
        $sql .= " where g.is_delete = 0 {$where} ";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count : 0;

    }

    /*
     * 群中男女比例占比
     * @param Array
     */
    public static function getErveySexCount($romm_id = 0)
    {

        $sql = "select count(*) count,sum(w.sex=1)/count(*) manCount,sum(w.sex=2)/count(*) womanCount from sline_community_member_weixin_info w";

        $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = w.room_wxid ";

        $sql .= " where w.delete_time = 0  and g.id = $romm_id";

        return DB::selectOne($sql);

    }


    /*
     * 机器人列表
     * @param Array
     */
    public static function getRobotList($page = 1, $pageSize = 10, $param = [])
    {
        $pageSize = 10;
        $page = ($page - 1) * $pageSize;
        $where = "";
        if (isset($param['wxNumber'])) {
            $where .= " and r.wx_alias like '%" . $param['wxNumber'] . "%'";
        }
        $orderBy = ' order by order_by asc';
        if ($param['bind_master'] == 1) {
            $orderBy = ' order by bind_mid_num asc';
        } else if ($param['bind_master'] == 2) {
            $orderBy = ' order by bind_mid_num desc';
        }
        if ($param['bind_group_number'] == 1) {
            $orderBy = ' order by bind_group_num asc';
        } else if ($param['bind_group_number'] == 2) {
            $orderBy = ' order by bind_group_num desc';
        }
        if ($param['orderCount'] == 1) {
            $orderBy = ' order by orderCount asc';
        } else if ($param['orderCount'] == 2) {
            $orderBy = ' order by orderCount desc';
        }
        if ($param['orderPrice'] == 1) {
            $orderBy = ' order by orderPrice asc';
        } else if ($param['orderPrice'] == 2) {
            $orderBy = ' order by orderPrice desc';
        }

        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= " and r.platform_identity ='{$param['platform_identity']}'";
        }
        if (isset($param['mid']) && !empty($param['mid'])) {
            $where .= " and r.bind_mid =" . $param['mid'] . "";
        }

        $sql = "select  count(o.room_id) as orderCount,sum(o.actual_price) as orderPrice,r.id,r.wx_alias,
               r.deleted_at,r.wxid,r.bind_mid,r.wx_name,r.mid,r.bind_mid_num,r.bind_group_num,r.order_by,r.mid,r.wxid_type,r.platform_identity   
               from sline_community_white_list_wx   as r  
               left join sline_community_user as u on r.mid = u.mid
               left join sline_community_member_info_by_appid g on g.wxid=r.wxid  and g.mid>0
               left join " . config('community.oldMallOrderGoods') . " o on o.room_id=g.id   and o.room_id>0 and o.pay_status=1
               where 
               r.deleted_at = 0  {$where}  group by r.id  {$orderBy} limit $page,10";
        return DB::select($sql);
    }


    /*
     * 机器人总量
     * @param Array
     */
    public static function getRobotCount($param = [], $platform = '')
    {
        $where = "";

        if (isset($param['wxNumber'])) {
            $where .= " and wx_alias like '%" . $param['wxNumber'] . "%'";
        }


        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= " and platform_identity ='{$param['platform_identity']}'";
        }
        if (isset($param['mid']) && !empty($param['mid'])) {
            $where .= " and bind_mid =" . $param['mid'] . "";
        }
        if (isset($platform) && !empty($platform)) {
            $where .= " and platform =" . $platform . "";
        }


        $sql = "select count(*) count_1 from sline_community_white_list_wx where deleted_at = 0 {$where}";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;
    }

    /*
     * 获取绑定机器人总量
     * @param Array
     */
    public static function getBindRobotCount()
    {

        $sql = "select count(*) from sline_community_member_info_by_appid where is_delete=0 group by wxid";

        return count(DB::select($sql));
    }

    /*
     * 获取群主数量
     * @param Array
     */
    public static function getGroupOwnerCount($param = [], $stattime = "", $endTime = "")
    {

        $where = "and mid <> 0";

        if (isset($param['wxid']) && !empty($param['wxid'])) {

            $where .= " and wxid = '" . $param['wxid'] . "'";
        }

        if ($stattime != "") {

            $where .= " and created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and created_at<'$endTime'";
        }

        $sql = "select count(*) from sline_community_member_info_by_appid where is_delete = 0 $where group by mid";

        return count(DB::select($sql));

    }


    /*
     * 获取总GMV
     * @param Array
     */
    public static function getTotalGMV($groupid = 0)
    {

        $where = "";

        if ($groupid != 0) {

            $where = " and room_id in ($groupid)";
        } else {

            $where = " and room_id>0";
        }

        $sql = "select sum(actual_price) totalmoney from " . config('community.oldMallOrderGoods') . " where pay_status=1 $where";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->totalmoney / 100 : 0;
    }

    /*
     * 根据用户mid获取用户昵称
     * @param Array
     */
    public static function getNickNameByMid($mid = 0)
    {
        return DB::table("user")
            ->where("mid", $mid)
            ->pluck("name")
            ->first();
    }


    /*
     * 获取总GMV与订单数量
     * @param Array
     */
    public static function getTotalGMV_V1($groupid = 0, $stattime = "", $endTime = "", $source = '')
    {

        $where = "";

        if ($groupid != 0) {

            $where .= " and room_id in ($groupid)";
        } else {

            $where .= " and room_id>0";
        }

        if ($stattime != "") {

            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d')>='$stattime'";
        }

        if ($endTime != "") {

            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d')<'$endTime'";
        }
        if ($source != "") {
            $channel = AUserModel::getChannelDataByPlatform($source);
            $where .= " and channel=" . $channel;
        }

        $sql = "select sum(actual_price) totalmoney,count(*) count_1 from " . config('community.oldMallOrderGoods') . " where pay_status=1 $where";

        return DB::selectOne($sql);
    }


    /*
    * 获取总GMV
    * @param Array
    */
    public static function getEveryDayTotalGMV($groupid = 0, $stattime = "", $endTime = "", $source = "")
    {

        $where = "";

        if ($groupid != 0) {

            $where .= " and room_id in ($groupid)";
        } else {

            $where = " and room_id>0";
        }

        if ($endTime != "") {
            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d')<'$endTime'";
        }

        if ($source != "") {
            $channel = AUserModel::getChannelDataByPlatform($source);
            $where .= " and channel=" . $channel;
        }

        $sql = "select sum(actual_price) as  totalmoney,count(*) as count_1,DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times from 
" . config('community.oldMallOrderGoods') . "  where pay_status=1 $where and FROM_UNIXTIME(pay_time,'%Y-%m-%d')>='$stattime'  GROUP BY DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d
%H:%i:%s'),'%Y-%m-%d')";

        return DB::select($sql);

    }


    /*
    * 获取总用户数根据时间
    * @param Array
    */
    public static function getEveryDayTotalmember($groupid = "", $stattime = "", $endTime = "", $mid = 0)
    {

        $where = "";

        $sql = "select count(*) as count_1,DATE_FORMAT(w.created_at,'%Y-%m-%d') as times from sline_community_member_weixin_info w";

        if ($groupid != "" && $mid == 0) {

            $where = " and g.id in ($groupid)";

            $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = w.room_wxid ";
        }

        if ($mid > 0) {

            $where = " and g.mid=$mid";

            $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = w.room_wxid ";
        }

        $sql .= " where w.created_at>'$stattime' and w.created_at<'$endTime' $where  GROUP BY DATE_FORMAT(w.created_at,'%Y-%m-%d')";

        return DB::select($sql);

    }

    public static function getBonusByMid($mid = 0, $groupid = 0)
    {
        if ($mid == 0 || $groupid == 0) {

            return 0;
        }

        $sql = "select sum(b.amount) totalmoney from " . config('community.oldMallOrderGoods') . " o";

        $sql .= " left join yuelvhui.sline_member_bonus_becommitted b on b.order_no=o.ordersn_son";

        $sql .= " where o.pay_status=1 and b.mid=$mid and o.room_id in ($groupid)";

        $result = DB::selectOne($sql);

        return $result == null ? $result->totalmoney : 0;
    }


    //获取群用户数量
    public static function getUserNumber($today = 0, $groupid = "", $mid = 0, $stattime = "", $endTime = "")
    {
        $where = "";

        if ($today == 1) {

            $todayTime = date("Y-m-d", time());

            $where .= " and DateDiff('$todayTime',w.created_at)=0";
        }

        $sql = "select count(*) count_1 from sline_community_member_weixin_info w ";

        if ($groupid != "" && $mid == 0) {

            $where .= " and g.id in ($groupid)";

            $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = w.room_wxid ";
        }

        if ($mid > 0) {

            $where .= " and g.mid = $mid";
            $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = w.room_wxid ";
        }

        if ($stattime != "") {

            $where .= " and w.created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and w.created_at<'$endTime'";
        }

        $sql .= " where w.delete_time=0 $where";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;

    }

    //获取机器人信息
    public static function getRobotInfo($id)
    {
        $sql = " select wxid from sline_community_white_list_wx where id=$id";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->wxid : null;


    }

    //获取群id
    public static function getRoomIds($wxid)
    {
        $array = DB::table("member_info_by_appid")
            ->where("wxid", $wxid)
            ->pluck("id")
            ->toArray();
        return implode(",", $array);
    }

    /*
     * 获取普通用户总GMV
     * @param Array
     */
    public static function getUserOneGMV($mid = 0, $stattime = "", $endTime = "")
    {

        $where = "";

        if ($stattime != "") {

            $where .= " and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')<'$endTime'";
        }

        $sql = "select sum(o.actual_price) totalmoney,count(*) count_1 from  sline_community_member_info_by_appid g";

        $sql .= " left join  " . config('community.oldMallOrderGoods') . " o on g.id = o.room_id";

        $sql .= " where g.is_delete = 0 and o.room_id>0 $where and o.pay_status=1 and g.mid = $mid";

        return DB::selectOne($sql);
    }

    /*
    * 获取普通用户总GMV根据时间
    * @param Array
    */
    public static function getUserEveryDayTotalGMV($mid = 0, $stattime = "", $endTime = "")
    {


        $sql = "select sum(o.actual_price) as  totalmoney,count(*) as count_1,DATE_FORMAT(FROM_UNIXTIME(o.pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times from 
" . config('community.oldMallOrderGoods') . " o ";

        $sql .= " left join sline_community_member_info_by_appid g on g.id = o.room_id";

        $sql .= " where o.pay_status=1 and o.room_id>0 and g.mid = $mid and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')>'$stattime' and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')<'$endTime'  GROUP BY DATE_FORMAT(FROM_UNIXTIME(o.pay_time,'%Y-%m-%d
%H:%i:%s'),'%Y-%m-%d')";


        return DB::select($sql);

    }

    public static function getgroupmamangeRobot($mid)
    {

        $sql = "select count(*) count_1 from sline_community_white_list_wx where mid = $mid and wxid_type = 1";

        $result = DB::selectOne($sql);

        return empty($result) ? 0 : $result->count_1;
    }

    /*
     * 获取社群管理员GMV
     * @param Array
     */
    public static function getgroupmamangeGMV($mid, $stattime = "", $endTime = "", $sup_id = 0)
    {


        if ($sup_id > 0) {
            $array = DB::table("white_list_wx")
                ->where("bind_mid", $mid)
                ->pluck("wxid")
                ->toArray();

        } else {

            $array = DB::table("white_list_wx")
                ->where("mid", $mid)
                ->where("wxid_type", 1)
                ->pluck("wxid")
                ->toArray();

        }

        if (count($array) < 0) {

            return 0;
        }

        $wxids = "'" . implode("','", $array) . "'";

        $field = "";

        $where = "";

        if ($stattime != "") {

            $field = ",DATE_FORMAT(FROM_UNIXTIME(o.pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times";

            $where = " and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')>'$stattime' and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')<'$endTime'  GROUP BY DATE_FORMAT(FROM_UNIXTIME(o.pay_time,'%Y-%m-%d
%H:%i:%s'),'%Y-%m-%d')";
        }

        $sql = "select sum(o.actual_price) totalmoney,count(*) count_1 $field from " . config('community.oldMallOrderGoods') . " o";

        $sql .= " left join sline_community_member_info_by_appid g on g.id = o.room_id";

        $sql .= " where g.is_delete = 0 and o.pay_status=1 and g.wxid in ($wxids) $where";

        if ($stattime != "") {

            return DB::select($sql);

        } else {

            return DB::selectOne($sql);
        }


    }


    /*
     * 获取社群管理员GMV
     * @param Array
     */
    public static function getgroupmamangeGMVV2($mid, $stattime = "", $endTime = "", $sup_id)
    {


        $array = DB::table("white_list_wx")
            ->where("mid", $mid)
            ->where("wxid_type", 1)
            ->pluck("wxid")
            ->toArray();

        if (count($array) < 0) {

            return 0;
        }

        $wxids = "'" . implode("','", $array) . "'";

        $where = " and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')>'$stattime' and FROM_UNIXTIME(o.pay_time,'%Y-%m-%d')<'$endTime' ";

        $sql = "select sum(o.actual_price) totalmoney,count(*) count_1 from " . config('community.oldMallOrderGoods') . " o";

        $sql .= " left join sline_community_member_info_by_appid g on g.id = o.room_id";

        $sql .= " where g.is_delete = 0 and o.pay_status=1 and g.wxid in ($wxids) $where";

        return DB::selectOne($sql);
    }


    /*
     * 获取社群管理员群数量
     * @param Array
     */
    public static function getgroupmamangeCount($type = 1, $mid = 0, $stattime = "", $endTime = "", $sup_id = 0)
    {

        $where = "";

        if ($stattime != "") {

            $where .= " and g.created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and g.created_at<'$endTime'";
        }

        if ($sup_id > 0) {

            $where .= " and w.bind_mid=$mid";

            $mid = $sup_id;
        }


        $sql = "select count(*) count_1 from sline_community_member_info_by_appid g";

        $sql .= " left join sline_community_white_list_wx w on w.wxid = g.wxid";

        $sql .= " where g.is_delete = 0 $where and w.wxid_type = $type and w.mid = $mid";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;
    }


    /*
     * 获取社群管理员群主数量
     * @param Array
     */
    public static function getgroupownmamangeCount($type = 1, $mid = 0, $stattime = "", $endTime = "", $sup_id = 0)
    {

        $where = "";

        if ($stattime != "") {

            $where .= " and g.created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and g.created_at<'$endTime'";
        }

        if ($sup_id > 0) {

            $where .= " and w.bind_mid=$mid";

            $mid = $sup_id;
        }


        $sql = "select count(*) count_1 from sline_community_member_info_by_appid g";

        $sql .= " left join sline_community_white_list_wx w on w.wxid = g.wxid";

        $sql .= " where g.is_delete = 0 and w.wxid_type = $type and w.mid = $mid $where group by g.mid";

        return count(DB::select($sql));

    }

    /*
     * 获取机器人专属用户群主数量
     * @param Array
     */
    public static function getgroupownCount($groupid = '', $mid = 0, $stattime = "", $endTime = "")
    {

        $where = "";

        if ($groupid != "") {

            $where .= " and g.id in ($groupid)";

        }
        if ($mid > 0) {

            $where .= " and g.mid=$mid";

        }

        if ($stattime != "") {

            $where .= " and created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and created_at<'$endTime'";
        }


        $sql = "select count(*) count_1 from sline_community_member_info_by_appid g";

        $sql .= " where g.is_delete = 0 $where group by g.mid";

        return count(DB::select($sql));
    }

    /*
     * 获取社群管理员群用户总数量
     * @param Array
     */
    public static function getgroupownmamangeMemberCount($mid, $stattime = "", $endTime = "", $sup_id = 0)
    {
        if ($sup_id > 0) {
            $array = DB::table("white_list_wx")
                ->where("bind_mid", $mid)
                ->pluck("wxid")
                ->toArray();

        } else {

            $array = DB::table("white_list_wx")
                ->where("mid", $mid)
                ->where("wxid_type", 1)
                ->pluck("wxid")
                ->toArray();

        }


        if (count($array) < 0) {

            return 0;
        }

        $where = "";

        if ($stattime != "") {

            $where .= " and m.created_at>'$stattime'";
        }

        if ($endTime != "") {

            $where .= " and m.created_at<'$endTime'";
        }


        $wxids = "'" . implode("','", $array) . "'";

        $sql = "select count(*) count_1 from sline_community_member_weixin_info m";

        $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = m.room_wxid";

        $sql .= " where m.delete_time = 0 $where  and g.wxid in ($wxids) ";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;
    }

    /*
    * 获取社群管理员群用户总数量根据时间
    * @param Array
    */
    public static function getMamangeEveryDayTotalmember($mid = "", $stattime = "", $endTime = "", $sup_id)
    {

        if ($sup_id > 0) {
            $array = DB::table("white_list_wx")
                ->where("bind_mid", $mid)
                ->pluck("wxid")
                ->toArray();

        } else {

            $array = DB::table("white_list_wx")
                ->where("mid", $mid)
                ->where("wxid_type", 1)
                ->pluck("wxid")
                ->toArray();

        }

        if (count($array) < 0) {

            return [];
        }

        $wxids = "'" . implode("','", $array) . "'";

        $where = "";

        $sql = "select count(*) as count_1,DATE_FORMAT(w.created_at,'%Y-%m-%d') as times from sline_community_member_weixin_info w";

        $sql .= " left join sline_community_member_info_by_appid g on g.room_wxid = w.room_wxid ";

        $sql .= " where w.created_at>'$stattime.' and w.created_at<'$endTime' and g.wxid in ($wxids) $where  GROUP BY DATE_FORMAT(w.created_at,'%Y-%m-%d')";

        return DB::select($sql);

    }


    /*
    * 获取直播排名列表
    * @param Array
    */
    public static function getLiveRankList($stattime = "", $endTime = "", $order = "", $page = 1, $pageSize = 10, $param = [])
    {

        $page = ($page - 1) * $pageSize;

        $where = "";

        if ($stattime != "") {
            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')>'$stattime'";
        }

        if ($endTime != "") {
            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')<'$endTime'";
        }

        $orderBy = isset($param['orderBy']) && !empty($param['orderBy']) ? $param['orderBy'] : 0;
        switch ($orderBy) {
            case 1://订单数量desc
                $order = "order by order_count desc";
                break;
            case 2://订单数量asc
                $order = "order by order_count asc";
                break;
            case 3://订单金额desc
                $order = "order by order_money desc";
                break;
            case 4://订单金额asc
                $order = "order by order_money asc";
                break;
            default:
                $order = "order by order_money desc";
                break;
        }
        // $order = "order by order_money";

        // if($order=="orderCount")
        // {
        //     $order = "order by order_count";
        // }

        // $by = "desc";

        $sql = "SELECT live_people_id,count(*) order_count,sum(actual_price) order_money  from yuelvhui.sline_mall_order ";

        $sql .= " where pay_status=1 and live_people_id>0 $where GROUP BY live_people_id $order limit $page,$pageSize";

        return DB::select($sql);
    }

    /*
    * 获取直播排名总数
    * @param Array
    */
    public static function getLiveRankCount($stattime = "", $endTime = "")
    {

        $where = "";

        if ($stattime != "") {
            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')>'$stattime'";
        }

        if ($endTime != "") {
            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')<'$endTime'";
        }

        $sql = "SELECT live_people_id,count(*) order_count,sum(actual_price) order_money from yuelvhui.sline_mall_order ";

        $sql .= " where pay_status=1 and live_people_id>0 $where GROUP BY live_people_id";

        return count(DB::select($sql));
    }

    /*
     * 小悦机器人管理总群数量
     * @param Array
     */
    public static function getRobotGroupInfo($stattime = "", $endTime = "", $page = 1, $pageSize = 10)
    {

        $page = ($page - 1) * $pageSize;

        $where = "";

        if ($stattime != "") {
            $where .= " and created_at>'$stattime'";
        }

        if ($endTime != "") {
            $where .= " and created_at<'$endTime'";
        }

        $sql = "select  DATE_FORMAT(created_at,'%Y-%m-%d') as days,DATE_FORMAT(created_at,'%H') as hours,count(*) count_1   from sline_community_member_info_by_appid";

        $sql .= " where is_delete = 0 $where group by  DATE_FORMAT(created_at,'%Y-%m-%d %H') order by DATE_FORMAT(created_at,'%Y-%m-%d %H') desc limit $page,$pageSize";

        return DB::select($sql);

    }


    /*
     * 小悦机器人管理总群数量
     * @param Array
     */
    public static function getRobotGroupInfoGraph($condition = [], $format = 'day')
    {
        $where = self::getRobotGroupInfoGraphWhere($condition);
        if ($format == 'day') {
            $format = '%Y-%m-%d';
        } else {
            //$format = '%Y-%m-%d %H';
            $format = '%H';
        }

        $sql = "select  DATE_FORMAT(g.created_at, '{$format}') as date, count(1) as groupNum   from sline_community_member_info_by_appid as g";

        $sql .= " where {$where} group by date";

        //dd($sql);
        return DB::select($sql);

    }


    /*
     * 小悦机器人管理总群数量
     * @param Array
     */
    public static function getRobotGroupCount($stattime = "", $endTime = "")
    {
        $where = "";

        if ($stattime != "") {
            $where .= " and created_at>'$stattime'";
        }

        if ($endTime != "") {
            $where .= " and created_at<'$endTime'";
        }

        $sql = "select  DATE_FORMAT(created_at,'%Y-%m-%d') as days,DATE_FORMAT(created_at,'%H') as hours,count(*) count_1   from sline_community_member_info_by_appid";

        $sql .= " where is_delete = 0 $where group by  DATE_FORMAT(created_at,'%Y-%m-%d %H') order by DATE_FORMAT(created_at,'%Y-%m-%d %H') desc";

        return count(DB::select($sql));
    }

    /*
     * 获取商品统计
     * @param Array
     */
    public static function getProductStatisticsList($param = [], $page = 1, $pageSize = 10)
    {

        $page = ($page - 1) * $pageSize;

        $where = self::getProductStatisticswhere($param);
        $clickWhere = self::getProductClickWhere($param);

        $order = "order by totalmoney desc";

        $orderBy = isset($param['orderBy']) ? $param['orderBy'] : 1;

        switch ($orderBy) {
            case 1:
                $order = "order by totalmoney desc";
                break;
            case 2:
                $order = "order by totalmoney asc";
                break;
            case 3:
                $order = "order by goodsNumber desc";
                break;
            case 4:
                $order = "order by goodsNumber asc";
                break;
            case 5:
                $order = "order by liren desc";
                break;
            case 6:
                $order = "order by liren asc";
                break;
            case 7:
                $order = "order by unitPrice desc";
                break;
            case 8:
                $order = "order by unitPrice asc";
                break;
            default:
                # code...
                break;
        }
        if (isset($param['clickNum']) && !empty($param['clickNum']) && in_array($param['clickNum'], ['asc', 'desc'])) {
            $order = "order by clickNum {$param['clickNum']}";
        }

        // $sql  = "SELECT sum(o.actual_price) totalmoney,sum(o.goods_num) as goodsNumber,sum(o.actual_price)/sum(o.goods_num) as unitPrice,count(o.id) as orderCount,";
        // $sql .= " o.goods_id,o.goods_price_member,o.goods_num,o.actual_price,o.goods_price_orig,o.commission,";
        // $sql .= " o.goods_name,o.goods_cover_image,o.goods_price_buy,TRUNCATE(o.goods_price_member-o.goods_price_buy-o.goods_coupon_average,2)/100 liren,";
        // $sql .= " TRUNCATE((o.goods_price_member-o.goods_price_buy-o.goods_coupon_average),2)/100 zLiRun,o.good_type,o.channel  FROM sline_community_mall_order_goods o";
        // $sql .= " left join sline_community_member_info_by_appid g on o.room_id = g.id";
        // $sql .= " left join sline_community_white_list_wx   r on r.wxid = g.wxid";

        // $sql .=" where   room_id>0 and pay_status=1 $where group by goods_id $order limit $page,$pageSize";


        $sql = "SELECT sum(o.actual_price) totalmoney,sum(o.goods_num) as goodsNumber,sum(o.actual_price)/sum(o.goods_num) as unitPrice,count(o.id) as orderCount,";
        $sql .= " o.goods_id,c.clickNum,o.goods_price_member,o.goods_num,o.actual_price,o.goods_price_orig,o.commission,";
        $sql .= " o.goods_name,o.goods_cover_image,o.goods_price_buy, TRUNCATE(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) ELSE o.commission END ,2) liren,";
        $sql .= " TRUNCATE(sum(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) ELSE o.commission END ),2) zLiRun,o.good_type,o.channel  FROM sline_community_mall_order_goods o";
        $sql .= " left join sline_community_member_info_by_appid g on o.room_id = g.id";
        $sql .= " LEFT JOIN (select sum(count_num) as clickNum,goods_id";
        $sql .= " from ";
        $sql .= " sline_community_goods_browse WHERE {$clickWhere} group by goods_id) c";
        //$sql .= " sline_community_goods_browse group by goods_id) c";
        $sql .= " ON c.goods_id = o.goods_id ";
        $sql .= " left join sline_community_white_list_wx   r on r.wxid = g.wxid";

        $sql .= " where   room_id>0 and pay_status=1 $where group by goods_id $order limit $page,$pageSize";


        return DB::select($sql);

    }



    /*
     * 获取商品统计
     * @param Array
     */
    public static function getProductStatisticsListV2($param = [], $page = 1, $pageSize = 10)
    {

        $page = ($page - 1) * $pageSize;

        $where = self::getProductStatisticswhereV2($param);
        $clickWhere = self::getProductClickWhereV2($param);

        $order = "order by totalmoney desc";

        $orderBy = isset($param['orderBy']) ? $param['orderBy'] : 1;

        switch ($orderBy) {
            case 1:
                $order = "order by totalmoney desc";
                break;
            case 2:
                $order = "order by totalmoney asc";
                break;
            case 3:
                $order = "order by goodsNumber desc";
                break;
            case 4:
                $order = "order by goodsNumber asc";
                break;
            case 5:
                $order = "order by liren desc";
                break;
            case 6:
                $order = "order by liren asc";
                break;
            case 7:
                $order = "order by unitPrice desc";
                break;
            case 8:
                $order = "order by unitPrice asc";
                break;
            default:
                # code...
                break;
        }
        if (isset($param['clickNum']) && !empty($param['clickNum']) && in_array($param['clickNum'], ['asc', 'desc'])) {
            $order = "order by clickNum {$param['clickNum']}";
        }

        // $sql  = "SELECT sum(o.actual_price) totalmoney,sum(o.goods_num) as goodsNumber,sum(o.actual_price)/sum(o.goods_num) as unitPrice,count(o.id) as orderCount,";
        // $sql .= " o.goods_id,o.goods_price_member,o.goods_num,o.actual_price,o.goods_price_orig,o.commission,";
        // $sql .= " o.goods_name,o.goods_cover_image,o.goods_price_buy,TRUNCATE(o.goods_price_member-o.goods_price_buy-o.goods_coupon_average,2)/100 liren,";
        // $sql .= " TRUNCATE((o.goods_price_member-o.goods_price_buy-o.goods_coupon_average),2)/100 zLiRun,o.good_type,o.channel  FROM sline_community_mall_order_goods o";
        // $sql .= " left join sline_community_member_info_by_appid g on o.room_id = g.id";
        // $sql .= " left join sline_community_white_list_wx   r on r.wxid = g.wxid";

        // $sql .=" where   room_id>0 and pay_status=1 $where group by goods_id $order limit $page,$pageSize";


        $sql = "SELECT sum(o.actual_price) totalmoney,sum(o.goods_num) as goodsNumber,sum(o.actual_price)/sum(o.goods_num) as unitPrice,count(o.id) as orderCount,";
        $sql .= " o.goods_id,c.clickNum,o.goods_price_member,o.goods_num,o.actual_price,o.goods_price_orig,o.commission,r.platform_identity,";
        $sql .= " o.goods_name,o.goods_cover_image,o.goods_price_buy, TRUNCATE(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) ELSE o.commission END ,2) liren,";
        $sql .= " TRUNCATE(sum(CASE WHEN o.good_type = 1 THEN ( o.goods_price_member - o.goods_price_buy - o.goods_coupon_average ) ELSE o.commission END ),2) zLiRun,o.good_type,o.channel  FROM sline_community_mall_order_goods o";
        $sql .= " left join sline_community_member_info_by_appid g on o.room_id = g.id";
        $sql .= " LEFT JOIN (select sum(count_num) as clickNum,goods_id";
        $sql .= " from ";
        $sql .= " sline_community_goods_browse WHERE {$clickWhere} group by goods_id) c";
        //$sql .= " sline_community_goods_browse group by goods_id) c";
        $sql .= " ON c.goods_id = o.goods_id ";
        $sql .= " left join sline_community_white_list_wx   r on r.wxid = g.wxid";

        $sql .= " where   room_id>0 and pay_status=1 $where group by goods_id $order limit $page,$pageSize";


        return DB::select($sql);

    }

    /*
     * 获取商品统计
     * @param Array
     */
    public static function getProductStatisticsCount($param = [])
    {

        $where = self::getProductStatisticswhere($param);

        $sql = "SELECT sum(actual_price) totalmoney,sum(goods_num) as goodsNumber,goods_price_member,goods_price_orig FROM  " . config('community.oldMallOrderGoods') . " o";
        $sql .= " left join `sline_community_member_info_by_appid` g on o.room_id = g.id";
        $sql .= " left join sline_community_white_list_wx r on r.wxid = g.wxid";
        //$sql .= " left join sline_community_user u on u.mid=g.mid ";
        $sql .= " where room_id>0 and pay_status=1 $where group by goods_id";

        return count(DB::select($sql));
    }


    /*
     * 获取商品统计
     * @param Array
     */
    public static function getProductStatisticsCountV2($param = [])
    {

        $where = self::getProductStatisticswhereV2($param);

        $sql = "SELECT sum(actual_price) totalmoney,sum(goods_num) as goodsNumber,goods_price_member,goods_price_orig FROM  " . config('community.oldMallOrderGoods') . " o";
        $sql .= " left join `sline_community_member_info_by_appid` g on o.room_id = g.id";
        $sql .= " left join sline_community_white_list_wx r on r.wxid = g.wxid";
        //$sql .= " left join sline_community_user u on u.mid=g.mid ";
        $sql .= " where room_id>0 and pay_status=1 $where group by goods_id";

        return count(DB::select($sql));
    }


    /*
    * 获取商品统计搜索
    * @param Array
    */
    public static function getProductClickWhere($condition = [])
    {
        $where = '1=1 ';
        if (isset($condition['goods_type']) && !empty($condition['goods_type'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "goods_type = {$condition['goods_type']}";
        }
        if (isset($condition['startTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = $condition['startTime'];
            $end = $condition['endTime'];
            $where .= "created_at between '{$begin}' and '{$end}'";
        } else if (isset($condition['startTime']) && empty($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = $condition['startTime'];
            $where .= "created_at >= '{$begin}'";
        } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $end = $condition['endTime'];
            $where .= "created_at <= '{$end}'";
        }

        return $where;
    }


    /*
    * 获取商品统计搜索
    * @param Array
    */
    public static function getProductClickWhereV2($condition = [])
    {
        $where = '1=1 ';
        if (isset($condition['goods_type']) && !empty($condition['goods_type'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "goods_type = {$condition['goods_type']}";
        }
        if (isset($condition['startTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = $condition['startTime'];
            $end = $condition['endTime'];
            $where .= "created_at between '{$begin}' and '{$end}'";
        } else if (isset($condition['startTime']) && empty($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = $condition['startTime'];
            $where .= "created_at >= '{$begin}'";
        } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $end = $condition['endTime'];
            $where .= "created_at <= '{$end}'";
        }

        return $where;
    }

    /*
    * 获取商品统计搜索
    * @param Array
    */
    public static function getProductStatisticswhere($param = [])
    {

        $where = "";


        if (isset($param['startTime']) && $param['startTime'] != "") {

            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')>'" . $param['startTime'] . "'";
        }

        if (isset($param['adminId']) && !empty($param['adminId'])) {

            $where .= " and r.bind_mid = {$param['adminId']}";
        }

        if (isset($param['endTime']) && $param['endTime'] != "") {
            $param['endTime'] = date("Y-m-d H:i:s", strtotime($param['endTime']) + 3600 * 24);

            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')<'" . $param['endTime'] . "'";
        }

        if (isset($param['channel']) && $param['channel'] > 0) {
            $where .= " and o.channel = {$param['channel']}";
        }

        if (isset($param['goods_type']) && $param['goods_type'] > 0) {
            $where .= " and o.good_type = {$param['goods_type']}";
        }

        if (isset($param['platform']) && !empty($param['platform'])) {
            $where .= " and r.platform = {$param['platform']}";
        }

        return $where;
    }


    /*
    * 获取商品统计搜索
    * @param Array
    */
    public static function getProductStatisticswhereV2($param = [])
    {

        $where = "";


        if (isset($param['startTime']) && $param['startTime'] != "") {

            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')>'" . $param['startTime'] . "'";
        }

        if (isset($param['adminId']) && !empty($param['adminId'])) {

            $where .= " and r.bind_mid = {$param['adminId']}";
        }

        if (isset($param['roomId']) && !empty($param['roomId'])) {

            $where .= " and o.room_id = '{$param['roomId']}'";
        }

        if (isset($param['endTime']) && $param['endTime'] != "") {
            $param['endTime'] = date("Y-m-d H:i:s", strtotime($param['endTime']) + 3600 * 24);

            $where .= " and FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s')<'" . $param['endTime'] . "'";
        }

        if (isset($param['channel']) && $param['channel'] > 0) {
            $where .= " and o.channel = {$param['channel']}";
        }

        if (isset($param['goods_type']) && $param['goods_type'] > 0) {
            $where .= " and o.good_type = {$param['goods_type']}";
        }

        if (isset($param['platform']) && !empty($param['platform'])) {
            $where .= " and r.platform = {$param['platform']}";
        }

        return $where;
    }


    /**
     * 获取群主列表
     * @param $param
     * @param $begin
     * @param $pageSize
     * @param $wxAliasString
     */
    public static function getLeaderListStatisticsList($param, $begin, $pageSize, $wxAliasString)
    {
        $where = self::getAdminGroupListWhere($param);
        $orderBy = self::getAdminGroupListOrderBy($param);
        $sql = "SELECT g.id,g.mobile as adminName,g.room_wxid,r.wx_alias,g.mid,g.robot_status,r.order_by as rid,r.wxid_type,r.bind_mid,r.mid as rmid,r.deleted_at, count(*) as groupCount,group_concat(g.id) as ids,sum(o.actual_price) orderAmount,count(o.room_id) orderNum, r.platform";//
        $sql .= " FROM `sline_community_member_info_by_appid` as g left join " . config('community.oldMallOrderGoods') . " o on o.room_id = g.id";
        if (isset($param['platform']) && $param['platform'] == 2) {
            $sql .= " left join sline_community_white_list_wx as r on g.enterprise_robot_serial_no = r.enterprise_robot_serial_no";
        } else {
            $sql .= " left join sline_community_white_list_wx as r on g.wxid = r.wxid";
        }
        $sql .= " where {$where} group by g.mid {$orderBy} limit $begin, $pageSize";
        return DB::connection('robot')->select($sql);
    }

    /**
     * 群的总数量
     * @param $param
     */
    public static function  getgetLeaderListStatisticsCount($condition)
    {
        $where = self::getAdminGroupListWhere($condition);
        $sql = "SELECT count(*) as cnt FROM (SELECT g.*";
        $sql .= " FROM `sline_community_member_info_by_appid` as g 
        left join " . config('community.oldMallOrderGoods') . "  o on o.room_id = g.id
        left join sline_community_white_list_wx as r on g.wxid = r.wxid where {$where} group by mid) as a";
        return DB::connection('robot')->select($sql);
    }

    /**
     * 获取渠道wxid
     * @param array $param
     * @return array
     */
    public static function getRobotChannelWxIds($param = [])
    {
        $getSourceWxidSql = "select  scw.wxid  from  sline_community_user  as scm  
                             left  join  sline_community_channel_data as scc  on scc.platform_identity= scm.platform_identity
                             left join sline_community_white_list_wx as scw on scm.mid=scw.mid  
                             where scc.id={$param['id']} and scw.deleted_at=0 and scm.delete_at=0  ";
        return DB::select($getSourceWxidSql);
    }

    /**
     * @param $condition
     * @return string
     */
    public static function getAdminGroupListWhere($condition)
    {
        $where = 'g.mid <> 0 and g.is_delete = 0';
        if (!empty($condition['name'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like %{$condition['name']}%";
        }
        if (isset($condition['roomId']) && !empty($condition['roomId']) && count($condition['roomId'])) {
            $where = $where ? $where . ' and ' : $where;
            $roomId = implode(',', $condition['roomId']);
            $where .= "g.id in ({$roomId})";
        }
        if (isset($condition['tag_id']) && !empty($condition['tag_id']) && count($condition['tag_id'])) {
            $where = $where ? $where . ' and ' : $where;
            $tagId = implode(',', $condition['tag_id']);
            $where .= "g.tag_id in ({$tagId})";
        }
        if (isset($condition['beginTime']) && isset($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at between {$begin} and {$end}";
        } else if (isset($condition['beginTime']) && empty($condition['endTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $begin = date('Y-m-d 00:00:00', $condition['beginTime']);
            $where .= "g.created_at >= {$begin}";
        } else if (isset($condition['endTime']) && empty($condition['beginTime'])) {
            $where = $where ? $where . ' and ' : $where;
            $end = date('Y-m-d 00:00:00', $condition['endTime']);
            $where .= "g.created_at <= {$end}";
        }
//        if ($condition['user_type'] >= 2) {
//            if(!empty($condition['wx_alias'])){
//                $where = $where ? $where . ' and ' : $where;
//                $where .= "r.wx_alias = {$condition['wx_alias']}";
//            }else{
//                $arr=[];
//                foreach($condition['wxIds'] as $v){
//                    $arr[] = $v;
//                }
//                if(!empty($arr)){
//                    $where = $where ? $where . ' and ' : $where;
//                    $wxIds = implode("','", $arr);
//                    $where .= "g.wxid in ('{$wxIds}')";
//                }
//            }
//        } else {
        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.mid = {$condition['searchMid']}";
        } else {
            if (isset($condition['mid']) && !empty($condition['mid'])) {
                $where = $where ? $where . ' and ' : $where;
                $where .= "g.mid = {$condition['mid']}";
            }
        }
        if (isset($condition['platform_identity']) || !empty($condition['platform_identity'])) {
            $where .= " and r.platform_identity = '{$condition['platform_identity']}'";
        }
        if (isset($condition['platform']) && $condition['platform'] !== '') {
            $where .= " and r.platform = '{$condition['platform']}'";
        }
//        }
//        if (isset($condition['searchMid']) && !empty($condition['searchMid'])) {
//            $where = $where ? $where . ' and ' : $where;
//            $where .= "g.mid = {$condition['searchMid']}";
//        }
        return $where;
    }

    /**
     * @param $condition
     * @return string
     */
    public static function getAdminGroupListOrderBy($condition)
    {
        $orderBy = '';
        if (isset($condition['orderNum']) && !empty($condition['orderNum']) && in_array($condition['orderNum'], ['asc', 'desc'])) {
            $orderBy .= "order by orderNum {$condition['orderNum']}";
        }
        if (isset($condition['orderAmount']) && !empty($condition['orderAmount']) && in_array($condition['orderAmount'], ['asc', 'desc'])) {
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }
        if (isset($condition['orderBy']) && $condition['orderBy'] == 2) {
            $orderBy = $orderBy ? $orderBy . ", groupCount asc" : " order by groupCount asc";
        } else {

            $orderBy = $orderBy ? $orderBy . ", groupCount desc" : " order by groupCount desc";
        }
        return $orderBy;
    }

    /**
     * 数据统计
     * @param $startTime
     * @param $endTime
     * @param $memberInfo
     * @param $bindMid
     */
    public static function getGroupActivityStatistics($startTime, $endTime, $memberInfo, $wxAliasString)
    {
        $getSourceWxidSql = "select sum(msg_num) as  msg_count_num,sum(source_num) as source_count_num,
                               sum(link_num) as link_count_num,sum(order_num) as order_count_num,
                               sum(member_num) as member_count_num,count(1) as group_count_num,date  from  sline_community_activity_statistics 
                               where  wxid in($wxAliasString) and  date>= '{$startTime}' and  date<='{$endTime}' 
                               group by date";
        return DB::select($getSourceWxidSql);
    }


    public static function getGroupActivityStatisticsToday($memberInfo, $wxAliasString)
    {
        $getSourceWxidSql = "select sum(msg_num) as  msg_count_num,sum(source_num) as source_count_num,
                               sum(link_num) as link_count_num,sum(order_num) as order_count_num,
                               sum(member_num) as member_count_num,count(1) as group_count_num,date  from  sline_community_activity_statistics 
                               where  wxid in($wxAliasString)";
        return DB::select($getSourceWxidSql);
    }

    /**
     * 消息数量
     * @param $wxAliasString
     * @param $today
     * @return array
     */
    public static function  msgCountNum($wxAliasString, $today)
    {
        $where1 = '';
        if (!empty($today)) {
            $where1 = "  and  created_at>'$today' ";
        }
        $sql = "SELECT count(1) as count_num FROM sline_community_report_msg   where wxid in($wxAliasString) {$where1} ";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count_num : [];
    }

    /**
     * 消息发群数量
     * @param $wxAliasString
     * @param $today
     * @return array
     */
    public static function  msgSourceNum($wxAliasString, $today)
    {
        $where1 = '';
        if (!empty($today)) {
            $where1 = "  and  created_at>'$today' ";
        }
        $sql = "SELECT count(1) as count_num FROM sline_community_send_room_reload   where wxid in($wxAliasString) {$where1} ";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count_num : [];
    }

    /**
     *点击数量
     * @param $wxAliasString
     * @param $today
     * @return array
     */
    public static function  msgLinkNum($wxAliasString, $today)
    {
        $where1 = '';
        if (!empty($today)) {
            $where1 = "  and  scr.created_at>'$today' ";
        }
        $sql = "SELECT count(1) as count_num FROM sline_community_room_activation  as scr 
                left join sline_community_member_info_by_appid   scm on scr.romm_id=scm.id
                where scm.wxid in($wxAliasString) {$where1} ";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count_num : [];

    }

    /**
     * @param $wxAliasString
     * @param $today
     * @return array
     */
    public static function  msgOrderNum($wxAliasString, $today)
    {
        $where1 = '';
        if (!empty($today)) {
            $today1 = strtotime($today);
            $where1 = "  and  sco.pay_time>'$today1' ";
        }
        $sql = "SELECT count(1) as count_num FROM sline_community_mall_order_goods  as sco 
                left join sline_community_member_info_by_appid   scm on sco.room_id=scm.id
                where scm.wxid in($wxAliasString) {$where1} ";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count_num : [];

    }

    /**
     * @param $wxAliasString
     * @param $today
     * @return array]
     */
    public static function  msgMemberNum($wxAliasString, $today)
    {
        $where1 = '';
        if (!empty($today)) {
            $where1 = "  and  scw.created_at>'$today' ";
        }
        $sql = "SELECT count(1) as count_num FROM sline_community_member_weixin_info  as scw 
                left join sline_community_member_info_by_appid   scm on scw.room_wxid=scm.room_wxid
                where scm.wxid in($wxAliasString) {$where1} ";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count_num : [];

    }

    /**
     * 群数量
     * @param $wxAliasString
     * @param $today
     * @return array
     */
    public static function  msgGroupNum($wxAliasString, $today)
    {
        $sql = "SELECT count(1) as count_num FROM  sline_community_member_info_by_appid scm 
                where scm.wxid in($wxAliasString)";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count_num : [];
    }

    /**
     * 获取绑定微信的机器人总数（新）
     *
     * @param string $platform_identity 渠道ID（可选）
     * @return Int
     */
    public static function getBindRobotCounts($platform_identity = '')
    {
        $data = WhiteListWx::where('deleted_at', '=', 0)
            ->where('wxid', '!=', '')
            ->where('mid', '!=', '');
        if (!empty($platform_identity)) {
            $data->where('platform_identity', $platform_identity);
        }
        return $data->count();
    }


}