<?php


namespace App\Model\Admin;


use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    //微信群用户信息数据表
    protected $table = 'sline_community_member_weixin_info';

    const PAGE_SIZE = 15;

}