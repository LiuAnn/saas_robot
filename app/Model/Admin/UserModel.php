<?php
namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserModel extends Model{

    /*
     * 查询用户列表
     */
    public static function getUserList($param,$page = 1,$pageSize = 10)
    {   

        $where = self::getCommunityUserWhere($param);

        $page = ($page-1)*$pageSize;

        $sql = "select * from sline_community_user where examine_status=0 $where limit $page,$pageSize";

        return DB::select($sql);
    }


    public static function getUserAdminList($param,$page=1,$pageSize = 10,$member=[])
    {

        $where = self::getCommunityUserAdminWhere($param);

        $where1 = "";

        if(isset($param['platform_identity'])&&$param['platform_identity']!="")
        {
            $where1 = " and u.platform_identity='".$param['platform_identity']."'";
        }

        if(isset($param['platform'])&&$param['platform']!="")
        {
            $where1 = " and w.platform='".$param['platform']."'";
        }

        if(isset($param['team_mids'])&&$param['team_mids'])
        {

            $where1 = " and u.mid in (".$param['team_mids'].") ";
        }

        $page = ($page-1)*$pageSize;

        $limit = " limit $page,$pageSize";

        $orderBy = " order by orderAmount desc";

        if(isset($param['orderBy']))
        {

            switch ($param['orderBy']) {
                case 1:
                    $orderBy = " order by orderAmount desc";
                    break;
                case 2:
                    $orderBy = " order by orderAmount asc";
                    break;
                case 3:
                    $orderBy = " order by orderCount desc";
                    break;
                case 4:
                    $orderBy = " order by orderCount asc";
                    break;
                case 5:
                    $orderBy = " order by groupCount desc";
                    break;
                case 6:
                    $orderBy = " order by groupCount asc";
                    break;
                case 7:
                    $orderBy = " order by memberCount desc";
                    break;
                case 8:
                    $orderBy = " order by memberCount asc";
                    break;
                case 9:
                    $limit = " ";
                    break;
                case 10:
                     $limit = " ";
                    break;
                case 11:
                     $limit = " ";
                    break;
                case 12:
                     $limit = " ";
                    break;
                default:
                     $limit = " ";
                    break;
            }

        }



        $sql  = "select u.name,sum(DISTINCT(g.member_count)) as memberCount,u.mid,count(DISTINCT g.id) as groupCount,count(o.id) as orderCount,sum(o.actual_price) orderAmount from sline_community_user u";
        $sql .= " left join sline_community_white_list_wx w on u.mid = w.bind_mid and w.status=0";
        if (isset($param['platform']) && $param['platform'] == 2) {
            $sql .= " left join sline_community_member_info_by_appid g on w.enterprise_robot_serial_no = g.enterprise_robot_serial_no and g.mid>0";
        } else {
            $sql .= " left join sline_community_member_info_by_appid g on w.wxid = g.wxid and g.mid>0";
        }
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id = g.id and o.pay_status=1 and o.room_id>0 $where";
        $sql .= " where u.sup_id > 0 and u.user_type = 2  and u.delete_at=0 $where1  GROUP BY u.mid $orderBy $limit ";

        return DB::select($sql);
    }

    public static function getInfoByMid($param,$mid)
    {
        $where = self::getCommunityUserAdminWhere($param);

        $sql  = "select w.id as robot_id,w.wx_name,sum(g.member_count) as memberCount,count(DISTINCT g.id) as groupCount,count(o.room_id) as orderCount,sum(o.actual_price) orderAmount from sline_community_white_list_wx w";
        $sql .= " left join sline_community_member_info_by_appid g on w.wxid = g.wxid and g.mid>0";
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id = g.id and o.pay_status=1 and o.room_id>0 $where";
        $sql .= " where w.bind_mid=$mid and w.status=0  GROUP BY w.id ";
        
        return DB::select($sql); 

    }

    public static function getUserAdminCount($param,$member=[])
    {

        //$where = self::getCommunityUserAdminWhere($param);
        $where1 = "";

        if(isset($param['platform_identity'])&&$param['platform_identity']!="")
        {
            $where1 = " and u.platform_identity='".$param['platform_identity']."'";
        }


        if(isset($param['team_mids'])&&$param['team_mids'])
        {

            $where1 = " and u.mid in (".$param['team_mids'].") ";
        }

        $sql = "select count(*) count_1 from sline_community_user u where u.sup_id > 0 and u.user_type = 2 and u.delete_at=0 $where1 ";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->count_1:0;
    }

    public static function getCommunityUserAdminWhere($param)
    {
        $where = "";

        if(isset($param['startTime'])&&$param['startTime']!="")
        {
            $where .=" and o.pay_time>'".$param['startTime']."'"; 
        }

        if(isset($param['endTime'])&&$param['endTime']!="")
        {
            $where1 =" and o.pay_time<'".$param['endTime']."'";

            if(isset($param['startTime'])&&isset($param['endTime'])&&$param['startTime']==$param['endTime'])
            {   
                $param['endTime'] = $param['endTime']+86400;

                $where1 =" and o.pay_time<'".$param['endTime']."'";
            }

            $where = $where.$where1;       
        }


        return $where;
    }


    public static function getAdminRobotList($mid=0)
    {

        return DB::table("white_list_wx")
        ->where("bind_mid",$mid)
        ->where("status",0)
        ->pluck("wx_name");
    }


    public static function getMemberMobile($mid)
    {
        return DB::connection('mysql_yuelvhui')->table('member')->where('mid', $mid)->value('mobile');
    }


    public static function getMemberNickname($mid)
    {
        return DB::connection('mysql_yuelvhui')->table('member')->where('mid', $mid)->value('nickname');
    }

    public static function getOrderListByTime($param=[],$mid=0,$timeList = [])
    {
        $where = self::getCommunityUserAdminWhere($param);

        $sql  = "select DATE_FORMAT(FROM_UNIXTIME(pay_time), '%Y-%m-%d') as payTime,count(o.room_id) as orderCount from sline_community_white_list_wx w";
        $sql .= " left join sline_community_member_info_by_appid g on w.wxid = g.wxid and g.mid>0";
        $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id = g.id and o.pay_status=1 and o.room_id>0 $where";
        $sql .= " where w.bind_mid=$mid and w.status=0 GROUP BY payTime order by payTime desc limit 30";
        
        $result = DB::select($sql);


        $orderList = [];

        foreach ($timeList as $key => $value) {
            
            $orderList[$key]['payTime'] = $value;
            $orderList[$key]['orderCount'] = 0;

            foreach ($result as $k => $v) {
                
                if($v->payTime==$value)
                {
                    $orderList[$key]['orderCount'] = $v->orderCount;
                }
            }
        }

        return $orderList;

    }


    public static function getUserSendInfoWhere($param)
    {   

        $where = "";

        if(isset($param['startTime'])&&$param['startTime']!="")
        {
            $where .= " and s.created_at > '".date("Y-m-d H:i:s",$param['startTime'])."'";
        }

        if(isset($param['endTime'])&&$param['endTime']!="")
        {   
            if($param['endTime']==$param['startTime'])
            {
                $where .= " and s.created_at < '".date("Y-m-d H:i:s",$param['endTime']+86400)."'";
            }else{

                $where .= " and s.created_at < '".date("Y-m-d H:i:s",$param['endTime'])."'";
            }
            
        }

        return $where;

        
    }

    public static function getUserSendInfoWhereV2($param)
    {   

        $where = "";

        if(isset($param['startTime'])&&$param['startTime']!="")
        {
            $where .= " and r.created_at > '".date("Y-m-d H:i:s",$param['startTime'])."'";
        }

        if(isset($param['endTime'])&&$param['endTime']!="")
        {   
            if($param['endTime']==$param['startTime'])
            {
                $where .= " and r.created_at < '".date("Y-m-d H:i:s",$param['endTime']+86400)."'";
            }else{

                $where .= " and r.created_at < '".date("Y-m-d H:i:s",$param['endTime'])."'";
            }
            
        }

        return $where;

        
    }

    public static function getUserSendInfo($param=[],$mid=0)
    {   
        $where = self::getUserSendInfoWhere($param);

        $sql = "select count(s.id) as relodeCount from sline_community_white_list_wx w ";
        $sql.= " left join sline_community_send_reload s on s.wxid=w.wxid $where";
        $sql.= " where w.deleted_at=0 and w.bind_mid=$mid";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->relodeCount:0;
    }

    public static function getUserSendInfoV2($param=[],$robot_id=0)
    {   
        $where = self::getUserSendInfoWhere($param);

        $sql = "select count(s.id) as relodeCount from sline_community_white_list_wx w ";
        $sql.= " left join sline_community_send_reload s on s.wxid=w.wxid $where";
        $sql.= " where w.deleted_at=0 and w.id=$robot_id";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->relodeCount:0;
    }

    public static function getUserSendGroupInfo($param=[],$mid=0)
    {   
        $where = self::getUserSendInfoWhereV2($param);

        $sql = "select count(DISTINCT(r.room_id)) as sendGroupCount from sline_community_white_list_wx w ";
        $sql.= " left join sline_community_send_room_reload r on r.wxid = w.wxid $where";
        $sql.= " where w.deleted_at=0 and w.bind_mid=$mid";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->sendGroupCount:0;
    }

    public static function getUserSendGroupInfoV2($param=[],$robot_id=0)
    {   
        $where = self::getUserSendInfoWhereV2($param);

        $sql = "select count(DISTINCT(r.room_id)) as sendGroupCount from sline_community_white_list_wx w ";
        $sql.= " left join sline_community_send_room_reload r on r.wxid = w.wxid $where";
        $sql.= " where w.deleted_at=0 and w.id=$robot_id";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->sendGroupCount:0;
    }

    /*
     * 查询用户总数量
     */
    public static function getUserCount($param,$page = 1,$pageSize = 10)
    {

        $where = self::getCommunityUserWhere($param);

        $page = ($page-1)*$pageSize;

        $sql = "select count(*) count_1 from sline_community_user where  examine_status=0 $where";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->count_1:0;
    }

    public static function getCommunityUserWhere($param)
    {

        $where = "";

        if(isset($param['mobile'])&&$param['mobile']>0)
        {
            $where.=" and mobile like '%".$param['mobile']."%'";
        }  

        if(isset($param['type'])||!empty($param['type']))
        {
            $where.=" and user_type = {$param['type']}";
        }      
        if(isset($param['platform_identity']) && !empty($param['platform_identity'])){
            $where.=" and platform_identity = {$param['platform_identity']}";
        }

        return $where;

    }

    /*
     * 查询用户信息
     */
    public static function getUserInfo($userid)
    {

        $sql = "select * from sline_community_user where id = $userid";

        return DB::selectOne($sql);
    }

    /*
     * 查询用户信息
     */
    public static function getUserInfoBymid($userid)
    {

        $sql = "select * from sline_community_user where mid = $userid";

        return DB::selectOne($sql);
    }

    /*
     * 查询用户信息
     */
    public static function getUserInfoByPhone($mobile)
    {

        $sql = "select * from sline_community_user where mobile = $mobile";

        return DB::selectOne($sql);
    }

    /*
     * 赠送群插入记录
     */
    public static function insetGiveLog($param)
    {

        return DB::table("give_group_log")->insert($param);
    }


    /*
     * 插入白名单记录
     */
    public static function insetwxwhitelist($param)
    {

        return DB::table("white_list_wx")->insert($param);
    }




    /*
     * 群助理微信表
     */
    public static function insetassistant($param)
    {

        return DB::table("assistant")->insert($param);
    }



    public static function getExclusiveRobotList($param=[],$page=1,$pageSize=10)
    {   
        $where = "";
        if(isset($param['mobile'])&&!empty($param['mobile']))
        {
            $where = " where phone like '%".$param['mobile']."%' ";
        }
        $page = ($page-1)*$pageSize;
        $sql = "select * from sline_community_exclusive_application $where limit $page,$pageSize";

        return DB::select($sql);
    }

    public static function getExclusiveRobotinfo($id)
    {      

        return DB::table("exclusive_application")
            ->where('id',$id)
            ->first();
    }

    public static function updateExclusiveRobotinfo($id,$param)
    {

        return DB::table("exclusive_application")->where("id",$id)->update($param);
    }

    public static function updateusertype($id,$param)
    {

        return DB::table("user")->where("mid",$id)->update($param);
    }

    public static function getAdminUserName($id)
    {
        if($id<1)return "";


        return DB::table('admin')
            ->where('admin_user_id',$id)
            ->pluck('admin_user_name')
            ->first();
    }

    public static function getExclusiveRobotCount($param=[])
    {   
        $where = "";

        if(isset($param['mobile'])&&!empty($param['mobile']))
        {
            $where = " where phone like '%".$param['mobile']."%' ";
        }

        $sql = "select count(*) count_1 from sline_community_exclusive_application $where";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->count_1:0;
    }

    //获取专属用户专属群ID
    public static function getRoomIdsByWxid($mid=0)
    {

        $sql = "select g.id from sline_community_member_info_by_appid g";

        $sql .= " left join sline_community_white_list_wx w on w.wxid = g.wxid";

        $sql .=" where w.mid = $mid and w.deleted_at=0 and g.is_delete = 0";

        return json_decode(json_encode(DB::select($sql)) , true);
    }

    //根据mid获取群id列表
    public static function getRoomIdsBymid($mid=0)
    {

        $sql = " select id from sline_community_member_info_by_appid where mid = $mid";

        return json_decode(json_encode(DB::select($sql)) , true);
    }


    //根据platform_identity获取所属平台
    public static function getChannelDataByPlatform($platform_identity="")
    {
        $sql = "select id from sline_community_channel_data where platform_identity='".$platform_identity."'";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->id:0;
    }


}