<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SectioynModel extends Model {
    /*
     * 获取部门信息
     */
    public static function getSectioyn()
    {
        $get = self::newDb()
            -> table('ylh_sectioyn')
            -> where(['deleted_at' => null])
            -> get()
            -> toArray();
        return !empty($get) ?$get : null;
    }

    public static function newDb()
    {
        return DB::connection('statistics');
    }
}