<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class AdminOperationLogModel extends Model
{
    protected $table = 'admin_log';

    const PAGE_SIZE = 15;

    public static function insertAdminOperation($insert)
    {
        return self::insert($insert);
    }

}
