<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/8
 * Time: 19:49
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StrategyArticleModel extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'strategy_article';

    protected $primaryKey = 'id';
    protected $guarded = [];

    public static function ylhDb()
    {
        return DB::connection('mysql');
    }

    public static function getStrategyArticleList($data=[])
    {
        $where = self::getStrategyArticleListWhere($data);

        $sql    = "select * from sline_community_strategy_article WHERE deleted_at=0 {$where} order by strategy_category_id desc,id asc limit ?, ?";
        $result = self::ylhDb()->select($sql, [$data["offset"], $data["display"]]);

        return $result;
    }

    public static function getStrategyArticleListWhere($param = [])
    {
        $where='';

        if (isset($param['strategy_category_id'])) {
            if (is_array($param['strategy_category_id'])) {
                $categoryIds = "'".implode($param['strategy_category_id'],"','")."'";
                $where .= " and strategy_category_id in ({$categoryIds})";
            } else {
                $where .= " and strategy_category_id = {$param['strategy_category_id']}";
            }
        }
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= " AND `platform_identity` = '" . $param['platform_identity'] . "'";
        }

        return $where;
    }

    public static function getStrategyArticleListCount($param)
    {
        $where  = self::getStrategyArticleListWhere($param);
        $sql    = "select count(*) count from sline_community_strategy_article WHERE deleted_at=0 {$where}";
        $result = self::ylhDb()->selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    public static function moveStrategyArticle($update, $param)
    {
        return self::ylhDb()->table('strategy_article')->where(['platform_identity' => $param['platform_identity']])
            ->whereIn('id', $param['id'])->update($update);
    }


    public static function getArticleCountByCategory($strategy_category_id, $param)
    {
        return self::ylhDb()->table('strategy_article')->where([
            'strategy_category_id' => $strategy_category_id,
            'platform_identity' => $param['platform_identity'],
            'deleted_at' => 0
        ])->count();
    }
}