<?php
/**
 * Created by PhpStorm.
 * User: redis  发送
 * Date: 2020/5/13
 * Time: 22:23
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use  App\Model\Mall\YanModel;
use  App\Service\VoucherBag\CouponService;
class PullTaskRedisModel extends Model
{

    /**
     *把将要发送 消息  存redis
     */
    public static function getNoRedisSendInfo()
    {
        set_time_limit(0);// 通过set_time_limit(0)可以让程序无限制的执行下去
        ini_set('memory_limit','512M'); // 设置内存限制
        //获取消息
        $todayTime = date('Y-m-d', time()-86400);
        $randomStrSql = "select random_str  from sline_community_send_info where  deleted_at=0 and mid not in ('921696','425637','77034','1042004','481553','1057359','1146044','1041600','1065455','1138548','460622','704524','688863','684603','32726','599151','450389','1046342','1174002','309010000898')  and status=0 and is_send=1 and robot_send_status=0 
                     and is_redis=0 and created_at>'{$todayTime}' and   platform_identity='7323ff1a23f0bf1cda41f690d4089353' group by random_str  limit 0,10";
        $randomStrData = DB::select($randomStrSql, []);
        Log::info('getNoRedisSendInfo  rand  randomStrSql  Data'.json_encode($randomStrData));
        if(!empty($randomStrData)){
            foreach($randomStrData as  $randomStr){
                $sql = "  select  id,type,typeid,content,random_str,short_url,link_type,tag_ids,platform_identity,mid,cid,link_title,link_desc,link_img_url,is_redis,up_flag,is_more_product   from sline_community_send_info  
                     where random_str='{$randomStr->random_str}'  and  status = 0  and  deleted_at=0  and  is_send=1  and robot_send_status=0  and  is_redis=0 and created_at>'{$todayTime}' ";
                $pushData = DB::select($sql, []);
                //如果有消息 进行处理
                Log::info('getNoRedisSendInfo  rand  randomStr  pushData'.json_encode($pushData));
                if (!empty($pushData)) {
                    $up_flag    = $pushData[0]->up_flag;
                    $tag_ids    = empty($pushData[0]->tag_ids) ? 0 : $pushData[0]->tag_ids;
                    $random_str = $pushData[0]->random_str;
                    $wxidflag=[];
                    Log::info('getNoRedisSendInfo  rand randomStr '.$random_str.' tag_ids ' .$tag_ids );

//                    Log::info('update   send_info   random_str  is_redis 1  up_flag 0' .$random_str);
//                    DB::table("send_info")->where("random_str", $random_str)->update(['is_redis'=>1,'up_flag'=>0]);

                    if (empty($tag_ids) &&$pushData[0]->mid==167624 ) {
                        $sql1 = 'select  id,wxid,bind_mid    from  sline_community_white_list_wx  where   status=0 and deleted_at=0 and wxid_type=1 ';
                        $result1 = DB::select($sql1, []);

                        foreach ($result1 as $v) {
                            $redisNewKey = $v->wxid . $random_str . config('community.redisKeystr');
                            $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                            $wxidflag[$v->wxid]=$up_flag;
                            if ($tobeCount > 0&&$wxidflag[$v->wxid]==1) {
                                for ($i = 1; $i <= $tobeCount; $i++) {
                                    Redis::lpop($redisNewKey);
                                }
                                $tobeCount=0;
                                self::getDeleteReload($random_str, $v->wxid);
                                $wxidflag[$v->wxid]=0;
                            }
                            $id = self::getWxSendReload($random_str, $v->wxid);  //当前微信号有没有发送
                            if (empty($tobeCount) && empty($id)) {
                                $roomMidData = self::getGroupOwerIdCodeNumCopy($v->wxid);
                                foreach ($roomMidData as $room_data) {
                                    $codeNumber = empty($room_data->code_number) ? config('community.codeNumber') : $room_data->code_number;
                                    $msg_list = self::getSendData($pushData,$codeNumber,$room_data->id,$room_data->mid);
                                    if (!empty($msg_list)) {
                                        $rpush = ['room_wxid' => $room_data->room_wxid, 'room_id' => $room_data->id, 'msg_list' => $msg_list];
                                        if (!empty($rpush)) {
                                            $is = Redis::rpush($redisNewKey, json_encode($rpush));
                                            Log::info('save  data  to  redis ' . $random_str . $v->wxid . $is . json_encode($rpush));
                                        }
                                    }
                                }
                                self:: addResidSendInfo($v->wxid,$random_str,$pushData);
                            }
                        }
                    } else if(empty($tag_ids) &&$pushData[0]->mid!=167624 ){
                        $sql1 = "select  id,wxid,bind_mid    from  sline_community_white_list_wx  where   status=0 and deleted_at=0 and wxid_type=1  and bind_mid={$pushData[0]->mid}";
                        $result1 = DB::select($sql1, []);
                        Log::info('save push bind  random_str  ' . $pushData[0]->random_str.json_encode([$result1]) );
                        foreach ($result1 as $v) {
                            $redisNewKey = $v->wxid . $random_str .config('community.redisKeystr');
                            $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                            $wxidflag[$v->wxid]=$up_flag;
                            if ($tobeCount > 0&&$wxidflag[$v->wxid]==1) {
                                for ($i = 1; $i <= $tobeCount; $i++) {
                                    Redis::lpop($redisNewKey);
                                }
                                $tobeCount=0;
                                self::getDeleteReload($random_str, $v->wxid);
                                $wxidflag[$v->wxid]=0;
                            }
                            $id = self::getWxSendReload($random_str, $v->wxid);  //当前微信号有没有发送
                            Log::info('save push bind  random_str  ' . $pushData[0]->random_str.json_encode([$redisNewKey,$tobeCount,$id]) );
                            if (empty($tobeCount) && empty($id)){
                                $roomMidData = self::getGroupOwerIdCodeNumCopy($v->wxid);
                                Log::info('save push bind  random_str  ' . $pushData[0]->random_str.json_encode([$tobeCount,$id,$roomMidData]) );
                                foreach ($roomMidData as $room_data) {
                                    $codeNumber = empty($room_data->code_number) ? config('community.codeNumber') : $room_data->code_number;
                                    //消息数据组合
                                    $msg_list = self::getSendData($pushData,$codeNumber,$room_data->id,$room_data->mid);
                                    if (!empty($msg_list)) {
                                        $rpush = ['room_wxid' => $room_data->room_wxid, 'room_id' => $room_data->id, 'msg_list' => $msg_list];
                                        if (!empty($rpush)) {
                                            $is = Redis::rpush($redisNewKey, json_encode($rpush));
                                            Log::info('save  data  to  redis ' .  json_encode([$random_str, $v->wxid , $is ,$rpush]));
                                        }
                                    }
                                }
                                //redis  存储存储记录表
                                self:: addResidSendInfo($v->wxid,$random_str,$pushData);
                            }
                        }
                    }else {
                        if($pushData[0]->mid==167624){
                            $where =  " and scw.mid={$pushData[0]->mid}" ;
                        }else if($pushData[0]->mid==61855){
                            $where =  " and scw.mid={$pushData[0]->mid}" ;
                        }else{
                            $where =  " and scw.bind_mid={$pushData[0]->mid}" ;
                        }
                        $sqlNorma = "select   scm.room_wxid,scm.id,scm.mid,sic.code_number,scm.wxid    from  sline_community_member_info_by_appid    as scm 
                            left join  sline_community_white_list_wx as scw   on scm.wxid=scw.wxid
                            left join yuelvhui. sline_invitation_code  as sic on sic.mid=scm.mid
                            where   scm.is_delete=0   and scm.robot_status=1 and 
                             scm.tag_id in (".$tag_ids.") and scm.tag_id!=11 and  scw.status=0 and  scw.deleted_at=0  and  scw.wxid_type=1 {$where}";
                        $returnData = DB::select($sqlNorma, []);
                        $wxidCounData =$wxidData=$wxidflag=[];
                        Log::info('save push bind count  tag_ids  ' . $pushData[0]->random_str.json_encode([count($returnData)]));
                        if(!empty($returnData)){
                            $wxidRoomCountSql = "select  count(1) as countNum,scw.wxid    from  sline_community_member_info_by_appid    as scm 
                            left join  sline_community_white_list_wx as scw   on scm.wxid=scw.wxid
                            where   scm.is_delete=0   and scm.robot_status=1 and 
                             scm.tag_id in(".$tag_ids.") and  scw.status=0 and  scw.deleted_at=0  and  scw.wxid_type=1 {$where} group by scw.wxid" ;
                            $CountData = DB::select($wxidRoomCountSql, []);
                            if(!empty($CountData)){
                                foreach($CountData as $wxidCount){
                                    $wxidCounData[$wxidCount->wxid.$random_str] = $wxidCount->countNum;
                                    $wxidflag[$wxidCount->wxid.$random_str] = $up_flag;
                                }
                            }
                            Log::info('save  data  to  redis tag_id CountData '.$random_str.json_encode($wxidCounData).json_encode($wxidflag));
                            foreach ($returnData as $normaData) {
                                $redisNewKey = $normaData->wxid . $random_str . config('community.redisKeystr');
                                $wxidData[$normaData->wxid ]=$normaData->wxid;
                                $tobeCount = count(Redis::lrange($redisNewKey, 0, -1));
                                if ($tobeCount>0&&$wxidflag[$normaData->wxid.$random_str]==1) {
                                    Log::info('save  data  to  redis tag_id  tobecount '.$random_str.$tobeCount. ' tobecount'.$wxidCounData[$normaData->wxid.$random_str]);
                                    for ($i = 1; $i <= $tobeCount; $i++) {
                                        Redis::lpop($redisNewKey);
                                    }
                                    $tobeCount=0;
                                    self::getDeleteReload($random_str, $normaData->wxid);
                                    $wxidflag[$normaData->wxid.$random_str]=0;
                                }
                                $id = self::getWxSendReload($random_str, $normaData->wxid);  //当前微信号有没有发送
                                if (empty($id)) {
                                    $codeNumber = empty($normaData->code_number) ? config('community.codeNumber') : $normaData->code_number;
                                    //消息数据组合

                                    $msg_list = self::getSendData($pushData,$codeNumber,$normaData->id,$normaData->mid);
                                    if (!empty($msg_list)) {
                                        $rpush = ['room_wxid' => $normaData->room_wxid, 'room_id' => $normaData->id, 'msg_list' => $msg_list];
                                        if (!empty($rpush)) {
                                            $is = Redis::rpush($redisNewKey, json_encode($rpush));
                                            Log::info( 'tag_id '.$is .'save  data  to  redis tag_id'.$random_str.$normaData->wxid.json_encode($rpush));
                                        }
                                    }
                                }
                            }
                        }
                        if(!empty($wxidData)){
                            //redis  存储存储记录表
                            foreach($wxidData as $wxid){
                                self:: addResidSendInfo($wxid,$random_str,$pushData);
                            }
                        }
                    }
                    Log::info('update   send_info   random_str  is_redis 1  up_flag 0' .$random_str);

                    DB::table("send_info")->where("random_str", $random_str)->update(['is_redis'=>1,'up_flag'=>0]);
                }else{
                    Log::info('save  data  no data  ');
                }
            }
        }
    }

    /**
     * 大人短链  获取带参数的链接
     * @param $postData
     * @return bool|string
     * @throws \Exception
     */
    public  static function  getDaRenLinkUrl($postData){
//        $postData = ['url' =>'https://m.yuelvdaren.com/DKWxZ3','room_id'=>1,'group_mid'=>22,'froms'=>1];  demo
        sleep(2);
        $result = curlPostCommon('https://gateway.daren.tech/open/v1/open/v1/home/getShorturl', $postData);
        $returnData = json_decode($result,true);
        Log::info( 'Daren  link   url '.$postData['url'].' url '.json_encode($returnData));
        if($returnData['code']==200){
            return $returnData['data']['shortUrl'];
        }else{
            return $postData['url'];
        }
    }


    public static function getSendMiniAppXmlDataByContent($baseXml, $queryData = []){
        $xmlData = xmlToArray($baseXml);
        $pagePath = $xmlData['appmsg']['weappinfo']['pagepath'];
        if (!empty($pagePath)) {
            $xmlData['appmsg']['weappinfo']['pagepath'] .= (stripos($pagePath, '?') !== false ? '&' : '?') . http_build_query($queryData);
        } else {
            unset($xmlData['appmsg']['weappinfo']['pagepath']);
        }
        return arr2xml($xmlData);
    }

    /**
     * 大人链接转连
     * @param $pushData
     * @param $roomId
     * @param $qzmid
     * @return array
     */
    public static function getSendDaRenData($pushData,$roomId,$qzmid, $codeNumber = ''){
            $msg_list =[];
            $more_product_text = "";
            foreach ($pushData as $vmsg) {
                if ($vmsg->type == 1 && !empty($vmsg->content)) {
                    $shortUrl = '';
                    if (!empty($vmsg->short_url)) {
                        $shortUrl = '👉' . self::getDaRenLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                        if (empty($shortUrl)) {
                            DB::table('send_info')->where('random_str', $vmsg->random_str)->update(['is_send' => 0]);
                            break;
                        }
                    }
                    if (empty($vmsg->content) && empty($shortUrl)) {
                        continue;
                    }
                    if($pushData[0]->is_more_product==1){

                        $more_product_text = $more_product_text.'
'.str_replace('?', '', $vmsg->content).$shortUrl;
                    }else{
                        $msg_list[] = [
                            'msg_type' => 1,
                            'msg' => str_replace('?', '', $vmsg->content) . $shortUrl,
                        ];
                    }
                } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                    if (empty($vmsg->content)) {
                        continue;
                    }
                    $msg_list[] = [
                        'msg_type' => 3,
                        'msg' => $vmsg->content,
                    ];
                } else if ($vmsg->type == 3 && !empty($vmsg->content)) {
                    if (empty($vmsg->content)) {
                        continue;
                    }
                    $msg_list[] = [
                        'msg_type' => 43,
                        'video_url' => $vmsg->content,
                    ];
                } else if ($vmsg->type == 4 && !empty($vmsg->content)) {
                    if (empty($vmsg->content)) {
                        continue;
                    }
                    $shortUrl = '👉' . self::getDaRenLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                    if(empty($shortUrl)){
                        DB::table('send_info')->where('random_str',$vmsg->random_str)->update(['is_send'=>0]);
                        break;
                    }
                    $msg_list[] = [
                        'msg_type' => 1,
                        'msg' => '👉 \r\n' . $shortUrl,
                    ];
                } else if ($vmsg->type == 5 &&!empty($qzmid)&&$vmsg->cid>0) {
                    $shortUrl = '👉' . self::getDaRenLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                    $msg_list[] =[
                        'msg_type'=>49,
                        'link_url'=>$shortUrl,
                        'link_title'=>$vmsg->link_title,
                        'link_desc'=>$vmsg->link_desc,
                        'link_img_url'=>$vmsg->link_img_url
                    ];
                } else if ($vmsg->type == 8 && !empty($vmsg->content)) {
                    $miniAppQueryData = [
                        'room_id' => $roomId,
                        'reCode' => $codeNumber,
                        'code_number' => $codeNumber
                    ];
                    $newXml = self::getSendMiniAppXmlDataByContent($vmsg->content, $miniAppQueryData);
                    $msg_list[] = [
                        "msg_type" => 4901,
                        "cover_url" => $vmsg->link_img_url,
                        "raw_msg" => $newXml
                    ];
                }
            }
        if($pushData[0]->is_more_product==1&&$more_product_text!=""){
            $msg_list1 =[
                'msg_type'=>1,                          
                'msg'=> $more_product_text,                                                                             
            ];
            array_unshift($msg_list,$msg_list1);                   
        }
        return $msg_list;
    }



    /**
     * 大人链接转连
     * @param $pushData
     * @param $roomId
     * @param $qzmid
     * @return array
     */
    public static function getZhidingData($pushData,$roomId,$qzmid, $codeNumber = ''){
        $msg_list =[];
        $more_product_text = "";
        foreach ($pushData as $vmsg) {
            if ($vmsg->type == 1 && !empty($vmsg->content)) {
                $shortUrl = '';
                if (!empty($vmsg->short_url)) {
                    $shortUrl = '👉' . self::getZhidingLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                    if (empty($shortUrl)) {
                        DB::table('send_info')->where('random_str', $vmsg->random_str)->update(['is_send' => 0]);
                        break;
                    }
                }
                if (empty($vmsg->content) && empty($shortUrl)) {
                    continue;
                }
                if($pushData[0]->is_more_product==1){

                    $more_product_text = $more_product_text.'
'.str_replace('?', '', $vmsg->content).$shortUrl;
                }else{
                    $msg_list[] = [
                        'msg_type' => 1,
                        'msg' => str_replace('?', '', $vmsg->content) . $shortUrl,
                    ];
                }
            } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 3,
                    'msg' => $vmsg->content,
                ];
            } else if ($vmsg->type == 3 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 43,
                    'video_url' => $vmsg->content,
                ];
            } else if ($vmsg->type == 4 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $shortUrl = '👉' . self::getZhidingLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                if(empty($shortUrl)){
                    DB::table('send_info')->where('random_str',$vmsg->random_str)->update(['is_send'=>0]);
                    break;
                }
                $msg_list[] = [
                    'msg_type' => 1,
                    'msg' => '👉 \r\n' . $shortUrl,
                ];
            } else if ($vmsg->type == 8 && !empty($vmsg->content)) {
                $miniAppQueryData = [
                    'room_id' => $roomId,
                    'reCode' => $codeNumber,
                    'code_number' => $codeNumber
                ];
                $newXml = self::getSendMiniAppXmlDataByContent($vmsg->content, $miniAppQueryData);
                $msg_list[] = [
                    "msg_type" => 4901,
                    "cover_url" => $vmsg->link_img_url,
                    "raw_msg" => $newXml
                ];
            }
        }
        if($pushData[0]->is_more_product==1&&$more_product_text!=""){
            $msg_list1 =[
                'msg_type'=>1,
                'msg'=> $more_product_text,
            ];
            array_unshift($msg_list,$msg_list1);
        }
        return $msg_list;
    }


    /**
     * 直订短链  获取带参数的链接
     * @param $postData
     * @return bool|string
     * @throws \Exception
     */
    public  static function  getZhidingLinkUrl($postData){
        Log::info( 'Zhiding  link   url post param'.json_encode($postData));
        sleep(2);
        $result = curlPostCommon('https://zhidingapi.zhiding365.com/api/getRobotLongUrl', $postData);
        $returnData = json_decode($result,true);
        Log::info( 'Zhiding  link   url '.$postData['url'].' url '.json_encode($returnData));
        if($returnData['code']==200){
            return $returnData['data']['shortUrl'];
        }else{
            return $postData['url'];
        }
    }

    /**
     * 迈图发送群消息
     * @param $pushData
     * @param $roomId
     * @param $qzmid
     * @return array
     * @throws \Exception
     */
    public static function  getMaituData($pushData,$roomId,$qzmid, $codeNumber = ''){
        $msg_list =[];
        $more_product_text = "";
        foreach ($pushData as $vmsg) {
            if ($vmsg->type == 1 && !empty($vmsg->content)) {
                $shortUrl = '';
                if (!empty($vmsg->short_url)) {
                    $shortUrl = '👉' . self::getMaituLinkUrl(['shortUrl' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                    if (empty($shortUrl)) {
                        DB::table('send_info')->where('random_str', $vmsg->random_str)->update(['is_send' => 0]);
                        break;
                    }
                }
                if (empty($vmsg->content) && empty($shortUrl)) {
                    continue;
                }
                if($pushData[0]->is_more_product==1){

                    $more_product_text = $more_product_text.'
'.str_replace('?', '', $vmsg->content).$shortUrl;
                }else{
                    $msg_list[] = [
                        'msg_type' => 1,
                        'msg' => str_replace('?', '', $vmsg->content) . $shortUrl,
                    ];
                }
            } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 3,
                    'msg' => $vmsg->content,
                ];
            } else if ($vmsg->type == 3 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 43,
                    'video_url' => $vmsg->content,
                ];
            } else if ($vmsg->type == 4 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $shortUrl = '👉' . self::getMaituLinkUrl(['shortUrl' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                if(empty($shortUrl)){
                    DB::table('send_info')->where('random_str',$vmsg->random_str)->update(['is_send'=>0]);
                    break;
                }
                $msg_list[] = [
                    'msg_type' => 1,
                    'msg' => '👉 \r\n' . $shortUrl,
                ];
            } else if ($vmsg->type == 8 && !empty($vmsg->content)) {
                $miniAppQueryData = [
                    'room_id' => $roomId,
                    'reCode' => $codeNumber,
                    'code_number' => $codeNumber
                ];
                $newXml = self::getSendMiniAppXmlDataByContent($vmsg->content, $miniAppQueryData);
                $msg_list[] = [
                    "msg_type" => 4901,
                    "cover_url" => $vmsg->link_img_url,
                    "raw_msg" => $newXml
                ];
            }
        }
        if($pushData[0]->is_more_product==1&&$more_product_text!=""){
            $msg_list1 =[
                'msg_type'=>1,
                'msg'=> $more_product_text,
            ];
            array_unshift($msg_list,$msg_list1);
        }
        return $msg_list;
    }


    /**
     * 迈图短链  获取带参数的链接
     * @param $postData
     * @return bool|string
     * @throws \Exception
     */
    public  static function  getMaituLinkUrl($postData){
        $result = curlPostCommon('https://maitu-web.yuebei.vip/open/v1/getShortUrlAppendParams', $postData);
        $returnData = json_decode($result,true);
        Log::info( 'Maitu  link   url '.$postData['shortUrl'].' url '.json_encode($returnData));
        if($returnData['code']==200){
            return $returnData['data']['short_url'];
        }else{
            return $postData['shortUrl'];
        }
    }


    /**
     * 获取迈图自营商品订单
     * @param $postData
     * @return bool|string
     * @throws \Exception
     */
    public static function getMaituOrderData(){

        $sql = " select * from pf_order.order_info m ";
        $sql.= " left join pf_order.order_sub_info s on s.order_no = m.order_no";
        $sql.= " left join pf_order.order_mall_info f on f.order_sub_no = s.order_sub_no";
        $sql.= " where m.room_id>0 and m.mch_id = 252 ";


        $result = DB::select($sql);

        return json_decode(json_encode($result),true);
    }
    /**
     * 迈图发送群消息
     * @param $pushData
     * @param $roomId
     * @param $qzmid
     * @return array
     * @throws \Exception
     */
    public static function  getLinjutuanData($pushData,$roomId,$qzmid,$codeNumber = ''){
        $msg_list =[];
        $more_product_text = "";
        foreach ($pushData as $vmsg) {
            if ($vmsg->type == 1 && !empty($vmsg->content)) {
                $shortUrl = '';
                if (!empty($vmsg->short_url)) {
                    $shortUrl = '👉' . self::getLinjutuanLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                    if (empty($shortUrl)) {
                        DB::table('send_info')->where('random_str', $vmsg->random_str)->update(['is_send' => 0]);
                        break;
                    }
                }
                if (empty($vmsg->content) && empty($shortUrl)) {
                    continue;
                }
                if($pushData[0]->is_more_product==1){

                    $more_product_text = $more_product_text.'
'.str_replace('?', '', $vmsg->content).$shortUrl;
                }else{
                    $msg_list[] = [
                        'msg_type' => 1,
                        'msg' => str_replace('?', '', $vmsg->content) . $shortUrl,
                    ];
                }
            } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 3,
                    'msg' => $vmsg->content,
                ];
            } else if ($vmsg->type == 3 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 43,
                    'video_url' => $vmsg->content,
                ];
            } else if ($vmsg->type == 4 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $shortUrl = '👉' . self::getLinjutuanLinkUrl(['url' => $vmsg->short_url,'room_id'=>$roomId,'group_mid'=>$qzmid,'order_froms'=>1]);
                if(empty($shortUrl)){
                    DB::table('send_info')->where('random_str',$vmsg->random_str)->update(['is_send'=>0]);
                    break;
                }
                $msg_list[] = [
                    'msg_type' => 1,
                    'msg' => '👉 \r\n' . $shortUrl,
                ];
            } else if ($vmsg->type == 8 && !empty($vmsg->content)) {
                $miniAppQueryData = [
                    'room_id' => $roomId,
                    'reCode' => $codeNumber,
                    'code_number' => $codeNumber
                ];
                $newXml = self::getSendMiniAppXmlDataByContent($vmsg->content, $miniAppQueryData);
                $msg_list[] = [
                    "msg_type" => 4901,
                    "cover_url" => $vmsg->link_img_url,
                    "raw_msg" => $newXml
                ];
            }
        }
        if($pushData[0]->is_more_product==1&&$more_product_text!=""){
            $msg_list1 =[
                'msg_type'=>1,
                'msg'=> $more_product_text,
            ];
            array_unshift($msg_list,$msg_list1);
        }
        return $msg_list;
    }


    /**
     * 迈图短链  获取带参数的链接
     * @param $postData
     * @return bool|string
     * @throws \Exception
     */
    public  static function  getLinjutuanLinkUrl($postData){
        $result = curlPostCommon('https://neighbour.yuetao.vip/api/stall/addUrlRooms', $postData);
        $returnData = json_decode($result,true);
        Log::info( 'Maitu  link   url '.$postData['url'].' url '.json_encode($returnData));
        if($returnData['code']==200){
            return $returnData['data']['short_url'];
        }else{
            return $postData['url'];
        }
    }

    /**
     * 发送的素材整合
     * @param $pushData
     * @param $codeNumber
     * @param int $roomId
     * @return array
     */
    public  static  function  getSendData($pushData,$codeNumber,$roomId=0,$qzmid=0){
        if(isset($pushData[0]->platform_identity)&&$pushData[0]->platform_identity=='89dafaaf53c4eb68337ad79103b36aff'){
             return   self::getSendDaRenData($pushData,$roomId,$qzmid, $codeNumber);
        }else if(isset($pushData[0]->platform_identity)&&$pushData[0]->platform_identity=='75f712af51d952af3ab4c591213dea13'){
             return   self::getZhidingData($pushData,$roomId,$qzmid, $codeNumber);
        }else if(isset($pushData[0]->platform_identity)&&$pushData[0]->platform_identity=='183ade9bc5f1c4b52c16072362bb21d1'){
             return   self::getMaituData($pushData,$roomId,$qzmid, $codeNumber);
        }else if(isset($pushData[0]->platform_identity)&&$pushData[0]->platform_identity=='c23ca372624d226e713fe1d2cd44be35'){
            return   self::getLinjutuanData($pushData,$roomId,$qzmid,$codeNumber);
        }
        $msg_list =[];
        $more_product_text = "";
        foreach ($pushData as $vmsg) {
            if (($vmsg->type == 1 && !empty($vmsg->content))||($vmsg->type == 6 && !empty($vmsg->content)) || ($vmsg->type == 7 && !empty($vmsg->content))) {
                $shortUrl = '';
                if (!empty($vmsg->short_url)) {
                    $keyParseData = parse_url($vmsg->short_url);
                    $shareCode = substr($keyParseData['path'], 1, 6);
                    $longData = YanModel::getProductLongUrl($shareCode);
                    if(!empty($longData)){
                        $parseData = parse_url($longData);
                        if($parseData['path']=='/page/JD.html'){//京东联盟
                            $longInfoUrl = explode('?', $longData);
                            $queryData = self::convertUrlQuery($longInfoUrl[1]);
                            $queryData['codeNumber'] = $codeNumber;
                            $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                            $queryData['order_from']= 1;
                            $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                            $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                        }else if($parseData['path']=='/page/PDD.html'){
                            $longInfoUrl = explode('?', $longData);
                            $queryData = self::convertUrlQuery($longInfoUrl[1]);
                            $queryData['codeNumber'] = $codeNumber;
                            $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                            $queryData['order_from']= 1;
                            $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                            $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                        }else if($parseData['path']=='/index-zhibo-yuetao.html'){
                            $longInfoUrl = explode('?', $longData);
                            $queryData = self::convertUrlQuery($longInfoUrl[1]);
                            $queryData['codeNumber'] = $codeNumber;
                            $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                            $queryData['share_from']= 3;
                            $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                            $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                        }else{
                            $longInfoUrl = explode('?', $longData);
                            $queryData = self::convertUrlQuery($longInfoUrl[1]);
                            $queryData['codeNumber'] = $codeNumber;
                            $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                            $queryData['share_from']= 3;
                            $queryData['robot_send']= 1;
                            $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                            $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                        }
                    }
                    if(empty($shortUrl)){
                        DB::table('send_info')->where('random_str',$vmsg->random_str)->update(['is_send'=>0]);
                        break;
                    }
                }
                if (empty($vmsg->content) && empty($shortUrl)) {
                    continue;
                }
                if($pushData[0]->is_more_product==1){

                    $more_product_text = $more_product_text.'
'.str_replace('?', '', $vmsg->content).$shortUrl;
                }else{
                    $msg_list[] = [
                        'msg_type' => 1,
                        'msg' => str_replace('?', '', $vmsg->content) . $shortUrl,
                    ];
                }
            } else if ($vmsg->type == 2 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 3,
                    'msg' => $vmsg->content,
                ];
            } else if ($vmsg->type == 3 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $msg_list[] = [
                    'msg_type' => 43,
                    'video_url' => $vmsg->content,
                ];
            } else if ($vmsg->type == 4 && !empty($vmsg->content)) {
                if (empty($vmsg->content)) {
                    continue;
                }
                $keyParseData = parse_url($vmsg->content);
                $shareCode = substr($keyParseData['path'], 1, 6);
                $longData = YanModel::getProductLongUrl($shareCode);
                if(!empty($longData)){
                    $parseData = parse_url($longData);
                    if($parseData['path']=='/page/JD.html'){//京东联盟
                        $longInfoUrl = explode('?', $longData);
                        $queryData = self::convertUrlQuery($longInfoUrl[1]);
                        $queryData['codeNumber'] = $codeNumber;
                        $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                        $queryData['order_from']= 1;
                        $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                        $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                    }else if($parseData['path']=='/page/PDD.html'){
                        $longInfoUrl = explode('?', $longData);
                        $queryData = self::convertUrlQuery($longInfoUrl[1]);
                        $queryData['codeNumber'] = $codeNumber;
                        $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                        $queryData['order_from']= 1;
                        $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                        $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                    }else if($parseData['path']=='/index-zhibo-yuetao.html'){
                        $longInfoUrl = explode('?', $longData);
                        $queryData = self::convertUrlQuery($longInfoUrl[1]);
                        $queryData['codeNumber'] = $codeNumber;
                        $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                        $queryData['share_from']= 3;
                        $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                        $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                    }else{
                        $longInfoUrl = explode('?', $longData);
                        $queryData = self::convertUrlQuery($longInfoUrl[1]);
                        $queryData['codeNumber'] = $codeNumber;
                        $queryData['room_id'] = isset($roomId) ? $roomId: 0;
                        $queryData['share_from']= 3;
                        $queryData['robot_send']= 1;
                        $newUrl = self::combineURL($longInfoUrl[0], $queryData);
                        $shortUrl = '👉' . CouponService::getShortURrl(['url' => $newUrl]);
                    }
                }else{
                    $shortUrl = $vmsg->content;
                }
                if(empty($shortUrl)){
                    DB::table('send_info')->where('random_str',$vmsg->random_str)->update(['is_send'=>0]);
                    break;
                }
                $msg_list[] = [
                    'msg_type' => 1,
                    'msg' => '👉 \r\n' . $shortUrl,
                ];
            } else if ($vmsg->type == 5 &&!empty($qzmid)&&$vmsg->cid>0) {
                if($vmsg->typeid==5){
                    $link_url = "https://single.yuetao.vip/coupons/detailActivity.html?codeNumber=".$codeNumber."&cid=".$vmsg->cid."&parentId=".$qzmid."&room_id=".$roomId."&from=groupmessage1&isappinstalled=0";
                }else{
                    $link_url = "https://single.yuetao.vip/coupons/detailGoods.html?codeNumber=".$codeNumber."&cid=".$vmsg->cid."&parentId=".$qzmid."&room_id=".$roomId."&from=groupmessage2&isappinstalled=0";
                }
                $msg_list[] =[
                    'msg_type'=>49,
                    'link_url'=>$link_url,
                    'link_title'=>$vmsg->link_title,
                    'link_desc'=>$vmsg->link_desc,
                    'link_img_url'=>$vmsg->link_img_url
                ];
            } else if ($vmsg->type == 8 && !empty($vmsg->content)) {
                $miniAppQueryData = [
                    'room_id' => $roomId,
                    'reCode' => $codeNumber,
                    'code_number' => $codeNumber
                ];
                $newXml = self::getSendMiniAppXmlDataByContent($vmsg->content, $miniAppQueryData);
                $msg_list[] = [
                    "msg_type" => 4901,
                    "cover_url" => $vmsg->link_img_url,
                    "raw_msg" => $newXml
                ];
            }
        }
        if (isset($pushData[0])) {
            if ($pushData[0]->is_more_product == 1 && $more_product_text != "") {
                $msg_list1 = [
                    'msg_type' => 1,
                    'msg' => $more_product_text,
                ];

                array_unshift($msg_list, $msg_list1);
            }
        }
        return $msg_list;
    }

    /**
     * 删除记录
     * @param $random_str
     * @param $wxid
     * @return int
     */
    public static function getDeleteReload($random_str, $wxid)
    {
        $result =DB::delete("delete from sline_community_send_reload where wxid='{$wxid}' and random_str = '{$random_str}' ");
        return $result;
    }
    /**
     * 查询发送记录
     * @param $random_str
     * @param $wxid
     * @return array
     */
    public static function getWxSendReload($random_str, $wxid)
    {
        $sql = 'select  id,robot_send_status   from sline_community_send_reload  where  random_str = ?  and wxid= ? ';
        $result = DB::select($sql, [$random_str, $wxid]);
        return !empty($result) ? $result[0] : [];
    }

    /**
     * 获取机器人管理的群
     * @param $wxid
     * @return array
     */
    public static function getGroupOwerIdCodeNumCopy($wxid)
    {
        $sql = "select scm.room_wxid,scm.id,scm.mid,sic.code_number from sline_community_member_info_by_appid   as scm 
             left join yuelvhui. sline_invitation_code  as sic on sic.mid=scm.mid
             where  scm.wxid=? and scm.is_delete=0 and tag_id!=11 order by  scm.mid desc , scm.id asc ";
        $result = DB::select($sql, [$wxid]);
        return !empty($result) ? $result : [];
    }


    /**
     * 解析path 参数
     * @param $query
     * @return array
     */
    public static function convertUrlQuery($query)
    {
        $queryParts = explode('&', $query);
        $params = array();
        foreach ($queryParts as $param) {
            $item = explode('=', $param);
            $params[$item[0]] = $item[1];
        }
        return $params;
    }


    /**
     * 参数拼接
     * @param $baseURL
     * @param $keysArr
     * @return string
     */
    public static function combineURL($baseURL, $keysArr)
    {
        $combined = $baseURL . "?";
        $valueArr = array();

        foreach ($keysArr as $key => $val) {
            $valueArr[] = "$key=$val";
        }

        $keyStr = implode("&", $valueArr);
        $combined .= ($keyStr);

        return $combined;
    }

    /**
     * 白名单数据更细
     */
    public  static  function  WhiteListDataTask()
    {
        $sql = " select  wxid  from  sline_community_white_list_wx ";
        $result = DB::select($sql);
        foreach($result as $v){
            //绑定群主数量
            $sqlcount =    "select count(*) as count_num from ( 
                  select count(1),mid,wxid   from sline_community_member_info_by_appid 
                  where wxid='{$v->wxid}' and mid>0 and is_delete=0   group by mid) as p" ;
            $resultcount =  DB::selectOne($sqlcount);
//            var_dump($resultcount->count_num);
            DB::table('white_list_wx')->where('wxid',$v->wxid)->update(['bind_mid_num'=>$resultcount->count_num]);
            //机器人群数量
            $sqlGroupCount =    "select count(1) as countNum,mid,wxid   from sline_community_member_info_by_appid 
                  where wxid='{$v->wxid}' and is_delete=0  and mid>0 " ;
            $resultGroupCount =  DB::selectOne($sqlGroupCount);
//            var_dump($resultGroupCount->countNum);
            DB::table('white_list_wx')->where('wxid',$v->wxid)->update(['bind_group_num'=>$resultGroupCount->countNum]);
           //机器人销售额
            $sqlOrderCount =    "select  sum(actual_price) as  all_order_sum  from  sline_community_mall_order_goods as scmo
                         left join   sline_community_member_info_by_appid as scm  on scmo.room_id=scm.id
                         where scm.wxid='{$v->wxid}'  " ;
            $OrderCount =  DB::selectOne($sqlOrderCount);
            DB::table('white_list_wx')->where('wxid',$v->wxid)->update(['all_order_sum'=>$OrderCount->all_order_sum]);
        }
    }


    /**
     * 添加数据
     * @param $data
     * @return bool
     */
    public static function addResidSendInfo($wxid,$random_str,$data) {
        $insertData = [
            'wxid'=>$wxid,
            'random_str'=> $random_str,
            'status'=> 0,
            'created_at'=> date('Y-m-d  H:i:s' ,time()),
            'json_data'=> json_encode($data),
        ];
        //redis  存储存储记录表
        DB::table("random_str_redis")->insert($insertData);
    }

    /**
     * 更改共同群数量
     */
    public  static function CommonGroupNumRobotChange()
    {
        $sql = "select count(1) as count_num  from sline_community_robot_friend  ";
        $resultCountNum= DB::selectOne($sql, []);
        $countNum = empty($resultCountNum) ? 0 : $resultCountNum->count_num;

        if($countNum>0){
            $countPage = ceil($countNum/200);
            for($i=1;$i<=$countPage;$i++) {
                $friendData = self::  getWeixinDataRobot(['offset' => ($i - 1) * 200, 'display' => 200]);
                if (!empty($friendData)) {
                    foreach ($friendData as $v) {
                       CommunityModel::updateRobotCommonGroupNum($v->wxid, $v->robot_wxid, $v->id);
                    }
                }
            }
        }
    }

    /**
     * 数据统计
     * @param $param
     * @return array
     */
    public  static function getWeixinDataRobot($param){
        $sql = "select wxid,robot_wxid,id from sline_community_robot_friend limit {$param['offset']},{$param['display']} ";
        $resultMsgInfo= DB::select($sql, []);
        return  $resultMsgInfo;
    }
    /**
     * 更新共同群数量
     * @param $wxid
     * @param $robot_wxid
     * @param $id
     * @return int
     */
    public static function updateRobotCommonGroupNum($wxid,$robot_wxid,$id){

        $sql    = "select count(1) as count_num  from `sline_community_member_weixin_info`  where  room_wxid in 
                  (select room_wxid  from `sline_community_member_weixin_info`   where  wxid=? )  and wxid=? ";
        $result =DB::selectOne($sql,[$wxid,$robot_wxid]);

        $commonNum = !empty($result) ? $result->count_num : 0;
        if($commonNum>0){
            return DB::table('robot_friend')->where('id', $id)->update(['common_group_num'=>$commonNum]);
        }
        return $commonNum;
    }
    
    /**
     * 订单数据入库
     * @param $id
     * @param $table
     * @param $data
     */
    public  static function insertOrderData($table, $data = [])
    {
        return DB::table($table)->insertGetId($data);
    }

    /**
     * 获取订单最后recordId
     */
    public static function getOrderInfoId($where)
    {
        $data = DB::table('mall_order_goods')
            ->where($where)
            ->orderBy('id', 'asc')
            ->value('id');
        return $data ?: 0;
    }


    /**
     * 获取佣金
     */
    public static function getCommissionInfo($where)
    {
        $data = DB::table('new_commission')
            ->where($where)
            ->select('amount')
            ->orderBy('id', 'desc')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }

    /**
     * 获取待处理的新增群数据
     */
    public static function getNewGroupList()
    {
        $beginTime = date("Y-m-d",strtotime("-6 day")) . ' 00:00:00';
        $endTime = date("Y-m-d",strtotime("-5 day")) . ' 00:00:00';
        $where = "created_at BETWEEN '{$beginTime}' and '{$endTime}' AND execute_status = 1";
        $sql = "SELECT id,room_id,wxid,room_wxid";
        $sql .= " FROM ylh_robot.sline_community_leave_judge where {$where}";
        $sql .= " ORDER BY id ASC limit 100";
        $data = DB::select($sql);
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }

    /**
     * 获取待处理的新增群数据
     */
    public static function updateLeaveJudgeInfo($where, $update)
    {
        if (empty($where)) {
            return 0;
        }
        $newGroupMemberCount = DB::table('leave_judge')->where($where)->update($update);
        return $newGroupMemberCount ?: 0;
    }

    /**
     * 获取待处理的新增群数据
     */
    public static function getNewGroupMemberCount($groupInfo)
    {
        $roomId = $groupInfo['room_id'] ?? 0;
        if (empty($roomId)) {
            return 0;
        }
        $newGroupMemberCount = DB::table('member_weixin_info')->where(['romm_id' => $roomId])->count();
        return $newGroupMemberCount ?: 0;
    }

    /**
     * 查看普通用户三方实际到账佣金
     */
    public static function getCpsMemberCommissionInfo($where)
    {

        $data = DB::connection('mysql_yuelvhui')->table('member_bonus')
            ->where($where)
            ->select('*')
            ->orderBy('id', 'desc')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }


    /**
     * 查看普通用户三方实际到账佣金
     */
    public static function getCpsMemberIntegralCommissionInfo($where)
    {

        $data = DB::connection('mysql_yuelvhui')->table('member_integral_record')
            ->where($where)
            ->select('*')
            ->orderBy('id', 'desc')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }


    /**
     * 获取订单最后未做返佣记录的主键ID
     */
    public static function getOrderCommission($recordId)
    {
        $limitTime = strtotime("-1 month");
        $where = "pay_time < {$limitTime} and is_commission = 0";
        $sql = "SELECT id,record_id,room_id,member_id,ordersn,ordersn_son,good_type,ordersn_jd,ordersn_son_jd,";
        $sql .= "channel,good_type,created_at";
        $sql .= " FROM ylh_robot.sline_community_mall_order_goods where {$where}";
        $sql .= " ORDER BY id ASC limit {$recordId},100";

        $data = DB::select($sql);
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }


    /**
     * 获取订单最后未做返佣记录的主键ID
     */
    public static function UpdateMallOrderIsCommission($order_id)
    {
        $data = DB::table('mall_order_goods')
            ->where(['id' => $order_id])
            ->update(['is_commission' => 1]);
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }

    /**
     * 获取订单最后未做返佣记录的主键ID
     */
    public static function getCommissionByOrderSn($order_sn)
    {
        $where = "orderNo = '{$order_sn}'";
        $sql = "SELECT id,mid as member_id,amount";
        $sql .= " FROM yuelvhui.sline_member_bonus where {$where}";
        $sql .= " limit 10";

        $data = DB::select($sql);
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }

    /**
     * 获取订单最后未做返佣记录的主键ID
     */
    public static function getIntegralByOrderSn($order_sn)
    {
        $where = "order_sn = '{$order_sn}'";
        $sql = "SELECT id,mid as member_id,integral";
        $sql .= " FROM yuelvhui.sline_member_integral_record where {$where}";
        $sql .= " limit 10";

        $data = DB::select($sql);
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }

    /**
     * 获取订单最后recordId
     */
    public static function getOrderInfo($where)
    {
        $data = DB::table('mall_order_goods')
            ->where($where)
            ->select('record_id', 'is_syn', 'id','order_status','commission')
            ->orderBy('record_id', 'desc')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }
    /**
     * 获取悦淘订单数据
     */
    public static function getYtOrderList($recordId = 0, $offset = 0, $limit = 10)
    {
        $data = DB::connection('mysql_yuelvhui')->table('mall_order_goods')
            ->where([['record_id', '>', $recordId], ['room_id', '>', 0], 'pay_status' => 1])
            ->select('*')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('record_id', 'asc')
            ->get();
        return $data ? $data : [];
    }
     /**
     * 获取悦淘订单数量
     */
    public static function getYtOrderTotal($recordId = 0)
    {
        $data = DB::connection('mysql_yuelvhui')->table('mall_order_goods')
            ->where([['record_id', '>', $recordId], ['room_id', '>', 0], 'pay_status' => 1])
            ->select('*')
            ->count();
        return $data;
    }

    /**
     * 获取悦淘直播间id
     */
    public static function getYtZhiboOrderId($ordersn = '')
    {
        $data = DB::connection('mysql_yuelvhui')->table('mall_order')
            ->where(['ordersn' => $ordersn])
            ->select('live_people_id','order_source')
            ->first();;
            $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }


    public static function updateOrderData($ordersn, $data = []){
        return DB::table('mall_order_goods')->where('ordersn', $ordersn)->update($data);
    }
    /**
     * 更新日志信息
     * @param $data
     * @return int
     */
    public static function upOrderLog($logId = 0, $data = []){
        return DB::table('order_log')->where('id', $logId)->update($data);
    }
    /**
     * 获取订单最小的recoredId
     */
    public static function getMinRecordId($where)
    {
        $data = DB::table('mall_order_goods')
            ->where($where)
            ->select('record_id')
            ->orderBy('record_id', 'asc')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];

    }
    /**
     * 更新订单信息
     * @param $data
     * @return int
     */
    public static function upOrderInfo($orderId = 0, $data = []){
        return DB::table('mall_order_goods')->where('id', $orderId)->update($data);
    }
    /**
     * 获取机器人微信群列表
     */
    public static function getWxGroupList($recordId = 0, $offset = 0, $limit = 10)
    {
        $data = DB::table('white_list_wx as wlx')
            ->leftJoin('member_info_by_appid as miba', 'miba.wxid', '=', 'wlx.wxid')
            ->where(['miba.is_delete' => 0, 'wlx.status' => 0])
            ->select('miba.room_wxid', 'miba.wxid')
            ->get();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }

    /**
     * 获取所有的正常群
     * @param $data
     * @return int
     */
    public static function getDataByWxid($wxid = '')
    {
        $data = DB::table('white_list_wx')
            ->where(['wxid' => $wxid ])
            ->select('wxid', 'bind_mid', 'mid')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }
    /**
     * 通过wxid获取所有群
     * @param $data
     * @return int
     */
    public static function getRoomList($time = '')
    {
        $data = DB::table('member_info_by_appid as miba')
            ->leftJoin('white_list_wx as wlx', 'miba.wxid', '=', 'wlx.wxid')
            ->where(['miba.is_delete' => 0])
            ->orWhere(['miba.is_delete' => 1, ['miba.deleted_at', '>=', $time]])
            ->select('miba.id', 'miba.wxid', 'miba.room_wxid','wlx.bind_mid','wlx.mid')
            ->get();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }
    /**
     * 数据入库
     * @param $id
     * @param $table
     * @param $data
     */
    public  static function insertData($table, $data = [])
    {
        return DB::table($table)->insertGetId($data);
    }

    /**
     * 获取群维度相关统计信息
     * @param $data
     * @return int
     */
    public static function getActivityStatistics($mid = 0, $bindMid = 0, $wxid = '', $roomWxid = '', $date = '')
    {
        $data = DB::table('activity_statistics')
            ->where(['mid' => $mid, 'bind_mid' => $bindMid, 'wxid' => $wxid, 'room_wxid' => $roomWxid, 'date' => $date])
            ->select('*')
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data ? $data : [];
    }
     /**
     * 更新统计信息
     * @param $data
     * @return int
     */
    public static function updateInfo($table = '', $id = 0, $data = []){
        return DB::table($table)->where('id', $id)->update($data);
    }
    /**
     * 返回社群订单数量
     * @param $data
     * @return int
     */
    public static function getOrderNum($roomId = 0, $time = '', $endTime = ''){
        $total = DB::table('mall_order_goods')
            ->where(['room_id' => $roomId, ['created_at', '>=', $time], ['created_at', '<=', $endTime]])
            ->count();
        return $total ? $total : 0;
    }
    /**
     * 返回消息数量
     * @param $data
     * @return int
     */
    public static function getMsgNum($roomWxId = '', $time = '', $endTime = ''){
        $total = DB::table('report_msg')
            ->where(['room_wxid' => $roomWxId, ['created_at', '>=', $time], ['created_at', '<=', $endTime]])
            ->count();
        return $total ? $total : 0;
    }
    /**
     * 返回素材数量
     * @param $data
     * @return int
     */
    public static function getSourceNum($roomId = 0, $time = '', $endTime = ''){
        $total = DB::table('send_room_reload')
            ->where(['room_id' => $roomId, ['created_at', '>=', $time], ['created_at', '<=', $endTime]])
            ->count();
        return $total ? $total : 0;
    }
    /**
     * 返回链接点击数量
     * @param $data
     * @return int
     */
    public static function getLinkClickNum($roomId = 0, $time = '', $endTime = ''){
        $total = DB::table('room_activation')
            ->where(['romm_id' => $roomId, ['created_at', '>=', $time], ['created_at', '<=', $endTime]])
            ->count();
        return $total ? $total : 0;
    }
     /**
     * 返回群成员数量
     * @param $data
     * @return int
     */
    public static function getMemberNum($roomId = 0){
        $total = DB::table('member_weixin_info')
            ->where(['romm_id' => $roomId, 'is_delete' => 0])
            ->count();
        return $total ? $total : 0;
    }    
    /**
     * 数据统计
     * @param $param
     * @return array
     */
    public  static function getDrOrderTotal($recordId = 0){
        // $sql = "select count(*) as count from daren_mall.mall_order where order_id > {$recordId} and pay_status = 1 and room_id > 0";
        $sql = 'select count(*) as count from daren_mall.mall_order as o
                left join daren_mall.mall_order_goods as og on og.ordersn = o.ordersn
                where o.order_id > '.$recordId.' and o.room_id > 0 and o.pay_status = 1 order by o.order_id asc';
        $res= DB::selectOne($sql);
        return !empty($res) ? $res->count : 0;
    }
    /**
     * 获取悦淘订单数据
     */
    public static function getDrOrderList($recordId = 0, $offset = 0, $limit = 10)
    {
        $sql = 'select og.*, o.room_id, o.froms as orderFrom, o.order_id , o.source from daren_mall.mall_order as o
                left join daren_mall.mall_order_goods as og on og.ordersn = o.ordersn
                where o.order_id > '.$recordId.' and o.room_id > 0 and o.pay_status = 1 order by o.order_id asc limit '.$limit.' offset '.$offset.'';
        
        $res = DB::select($sql);

        return $res;
    }

    /**
     * 获取机器人管理的群
     * @param $wxid
     * @return array
     */
    public static function  GetRoomByTagIdWxid($wxid,$tag_ids)
    {
        $where = '';
        if(!empty($tag_ids)){
            $where =  " and scm.tag_id in($tag_ids)" ;
        }
        $sql = "select scm.room_wxid,scm.id,scm.mid,sic.code_number,scm.tag_id from sline_community_member_info_by_appid   as scm 
             left join yuelvhui. sline_invitation_code  as sic on sic.mid=scm.mid
             where  scm.wxid=? and scm.is_delete=0 and tag_id!=11   $where  order by scm.order_num desc,scm.id asc, scm.mid desc  ";
        $result = DB::select($sql, [$wxid]);
        return !empty($result) ? $result : [];
    }

    /**
     * 获取素材
     * @param $randomStr
     * @return array
     */
    public static function  getPullToRoomData($randomStr,$tag_ids=''){

        $where = "";

        if($tag_ids!=""){
            $where =  " and (find_in_set(".$tag_ids.",tag_ids) or find_in_set(0,tag_ids))";
        }
        $sql = "  select  id,type,typeid,content,random_str,short_url,link_type,tag_ids,platform_identity,mid,cid,link_title,link_desc,link_img_url,is_redis,up_flag,is_more_product   from sline_community_send_info  
                     where random_str='{$randomStr}' {$where} and  status = 0  and  deleted_at=0  and  is_send=1  and robot_send_status=0   ";
        $pushData = DB::select($sql, []);
         return $pushData;
    }

    /**
     * @param $randomStr
     * @return array
     */
    public static function  getPullToRoomMsgCount($randomStr){
        $sql = "  select  count(1) as count_num  from sline_community_send_info  
                     where random_str=?  and  status = 0  and  deleted_at=0  and  is_send=1  and robot_send_status=0    ";
        $result = DB::selectOne($sql, [$randomStr]);
        return $result->count_num>0?$result->count_num:0;
    }

    /**
     * @param $randomStr
     */
    public static function  updateSendInfoByRandomStr($randomStr)
    {
       return    DB::table("send_info")->where("random_str", $randomStr)->update(['is_redis'=>1,'up_flag'=>0]);
    }

   /**
    * 获取符合当前时间的素材
    */

   public static  function  getWillToRoomMas($wxid){
       $uni_Time = time()-14400;
       $today_time =  $todayTime =date('Y-m-d H:i:s',$uni_Time);
       $sql = "select random_str  from sline_community_white_list_wx as scw  
             left join  sline_community_send_info as  scs on scw.bind_mid=scs.mid 
             where  scw.wxid='$wxid'    and  scs.is_redis=1 and  
             (scs.created_at>'$today_time' or  (is_timing_send=1 and timing_send_time !=0 and unix_timestamp(now())>timing_send_time and  timing_send_time>$uni_Time)  )
            group by random_str ";
       $result = DB::select($sql);
       $data = json_decode(json_encode($result), true);
       return $data ? $data : [];
   }

    /**
     * 群的详细信息
     * @return array|mixed
     */
   public  static function   getSendToGroupInfo($room_wxid){
       $data = DB::table('member_info_by_appid')
           ->where(['room_wxid' => $room_wxid ])
           ->select('mid', 'code_number', 'mobile','id','tag_id')
           ->first();
       $data = json_decode(json_encode($data), true);
       return $data ? $data : [];
   }


    /**
     * 更新发送的顺序
     */

    public static  function  updateRoomOrderNum(){
        $sql = " UPDATE sline_community_member_info_by_appid AS scm  SET  order_num = (select count(*) FROM sline_community_mall_order_goods  WHERE room_id=scm.id)";
        $result = DB::update($sql);
        $data = json_decode(json_encode($result), true);
        return $data ? $data : [];
    }

    /**
     *获取悦呗所有数据
     */
    public static function yueBeiHistoryAllData()
    {
        $sql = " select   scm.mid,count(1) as group_num,b.*   from   sline_community_member_info_by_appid  scm left join 
       (select  count(1) as order_num,sum(scmo.actual_price) as sumPrice,scmi.mid from sline_community_mall_order_goods    scmo left join sline_community_member_info_by_appid  scmi on scmo.room_id=scmi.id where  scmo.created_at<1590940800   and   scmo.created_at>0 and scmi.mid>0   group by  scmi.mid )  b
        on b.mid=scm.mid  where scm.created_at < '2020-06-01'  and  scm.mid>0  group by  scm.mid";
        $result = DB::select($sql);
        $data = json_decode(json_encode($result), true);
        return $data ? $data : [];
    }

    /**
     * 更新数据
     * @return array|mixed
     */
    public static  function  updateWorkerData($data){
        $result=  DB::connection('mysql_worker')->table("worker")->where("mid", $data['mid'])->update(['group_number'=>$data['group_num'],'group_savos'=>$data['sumPrice']]);
        $data = json_decode(json_encode($result), true);
        return $data ? $data : [];
    }

    /**
     * 查询用户是否是大床或者分公司
     * @param $mid
     * @return array
     */
    public static function isSetMidYueBei($mid){
        if($mid>0){
            $sql = " select id from  data_worker where mid={$mid}  ";
            $result = DB::connection('mysql_worker')->selectOne($sql);
            $data = json_decode(json_encode($result), true);
            return $data ? $data['id'] : [];
        }
       return [];
    }


    /**
     * 根据room_id获取群主mid
     * @param $mid
     * @return string
     */
    public static function getMidByRoomId($room_id = 0){
        if($room_id>0){
            $sql = " select mid from  sline_community_member_info_by_appid where id={$room_id}  ";
            $result = DB::selectOne($sql);
            $data = json_decode(json_encode($result), true);
            return $data ? $data['mid'] : 0;
        }
       return 0;
    }


    public static function getHotelOrderList($param){

        $sql  = "select o.room_id,o.member_id,o.order_no,o.pay_amount,o.total_amount,h.hotel_name,o.pay_time,o.pay_status,o.order_status,o.cancel_time,h.hotel_id,h.room_name,h.hotel_pic,o.create_time,o.commission_amount,o.update_time from pf_order.order_info o ";
        $sql .= " left join pf_order.order_sub_info s on o.order_no=s.order_no";
        $sql .= " left join pf_order.order_hotel_info h on h.order_sub_no=s.order_sub_no";
        $sql .= " where o.room_id>0 and o.order_type=301 and o.pay_status=10 and o.sys_source='yuecheng'";

        $result = DB::connection('center_order')->select($sql);
        //$result = DB::select($sql);

        $data = json_decode(json_encode($result), true);

        return !empty($data) ? $data : [];
    }

}