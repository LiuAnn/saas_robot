<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/1
 * Time: 14:32
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class GroupGoodsDataModel extends Model
{

    /**
     * 群激活首页团特卖商品
     */
    public static function ActivationGroupGoodsDataIndex()
    {
        return [
            'title' => '团特卖',
            'titleDesc' => '群内专享补贴  赚群管理奖励',
            'goodsData' => [
                [
                    'goods_id' => 363695,
                    'goods_img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2019-12-18/11/yuelvhuiy90Q9R5Z2d1576640481.jpg',
                    'goods_name' => '群内专享价',
                    'price' => 8.8,
                    'earn_desc' => '赚',
                    'earn_price' => 12.5,
                ],
                [
                    'goods_id' => 363696,
                    'goods_img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2019-12-18/11/yuelvhuiy90Q9R5Z2d1576640481.jpg',
                    'goods_name' => '群内专享价',
                    'price' => 8.8,
                    'earn_desc' => '赚',
                    'earn_price' => 12.5,
                ],
            ]
        ];
    }



}