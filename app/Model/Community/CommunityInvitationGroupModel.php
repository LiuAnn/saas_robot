<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2018/11/20
 * Time: 下午5:59
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommunityInvitationGroupModel extends Model
{

    //指定表名
    protected $table = 'invitation_group';

    //指定主键字段
    protected $primaryKey = 'id';

    //关闭 Laravel 自动管理列
    public $timestamps = false;

    //设置和名单，为空则表明任何字段都可插入数据库
    protected  $guarded = [];

    /**
     * 通过群id获取群信息
     * @param $number
     * @return mixed
     */
    public static function getGroupInfoByGroupId($number)
    {
        return CommunityInvitationGroupModel::select('id as groupId', 'group_number as groupNo', 'group_name as groupName', 'assistant_wx as assistantWx', 'tutor_wx as tutorWx', 'images', 'examine_status as status')->where('id', $number)->first();
    }


    /**
     * 通过群id获取群信息
     * @param $number
     * @return mixed
     */
    public static function getGroupInfoByMid($params)
    {
        $mid = $params['mid'];
        $source = isset($params['source']) ? $params['source'] : '';
        $where = [
            'mid' => $mid,
            'type' => 1
        ];
        if (!empty($source)) {
            $where['platform_identity'] = $source;
        }
        return CommunityInvitationGroupModel::select('id as groupId', 'group_number as groupNo', 'group_name as groupName', 'assistant_wx as assistantWx', 'tutor_wx as tutorWx', 'images', 'examine_status as status')
            ->where($where)
            ->orderBy('id', 'desc')->first();
    }

    /**
     * 通过群编号获取群信息
     * @param $number
     * @return mixed
     */
    public static function getGroupInfoByGroupNumber($number)
    {
        return CommunityInvitationGroupModel::where('group_number', $number)->first();
    }

    /**
     * 根据用户id查询未审核的群 如果有不重新生成
     * @param $mid
     * @return mixed
     */
    public static function getGroupInfoByMidAndStatus($mid, $status, $type)
    {
        return CommunityInvitationGroupModel::where(['mid' => $mid, 'examine_status' => $status, 'type' => $type])->first();
    }

    /**
     * 根据用户id查询未审核的群 如果有不重新生成
     * @param $mid
     * @return mixed
     */
    public static function getExamineStepByGroupId($where)
    {
        return CommunityInvitationGroupModel::where($where)->orderBy('id', 'desc')->value('examine_step');
    }

    /**
     * 根据用户id审核表id查询审核数据是否存在
     * @param $mid
     * @return mixed
     */
    public static function getInvitationGroupInfo($mid,$groupId)
    {

        return CommunityInvitationGroupModel::where(['id' => $groupId, 'mid' => $mid, 'examine_status' => 2])->first();
    }


    /**
     * 查询是否存在待审核状态数据
     * @param $where
     * @return mixed
     */
    public static function getExistRecord($where)
    {
        return CommunityInvitationGroupModel::where($where)->count();
    }



    /**
     * 查询是否存在待审核状态数据
     * @param $where
     * @return mixed
     */
    public static function getExistRecordInfo($where)
    {
        $result = CommunityInvitationGroupModel::where($where)->first();
        return $result ? json_decode(json_encode($result), true) : [];
    }

    /**
     * 查询是否存在待审核状态数据
     * @param $where
     * @return mixed
     */
    public static function getYCloudRobotInfo($where)
    {
        return DB::table('white_list_wx')->where($where)->value('wx_alias');
    }


    /**
     * 插入数据
     * @param $data
     * @return mixed
     */
    public static function createData($data)
    {
        return CommunityInvitationGroupModel::insertGetId($data);
    }

    /**
     * 更新数据
     * @param $data
     * @return mixed
     */
    public static function updateDataById($id, $data)
    {
        return CommunityInvitationGroupModel::where(['id' => $id])->update($data);
    }

    /**
     * 更新数据
     * @param $data
     * @return mixed
     */
    public static function updateDataByIdMid($id,$mid, $data)
    {
        return CommunityInvitationGroupModel::where(['id' => $id,'mid'=>$mid])->update($data);
    }


    /**
     * 更新数据
     * @param $data
     * @return mixed
     */
    public static function updateDataByIdMidParam($mid,$source, $data)
    {
        return CommunityInvitationGroupModel::where(['mid'=>$mid, 'platform_identity' => $source])
            ->orderBy("id","DESC")->limit(1)->update($data);
    }

    /**
     * 获取用户的直推群列表
     * @param $params
     * @return mixed
     */
    public static function getGroupListByMid($params)
    {
        return CommunityInvitationGroupModel::select('id as groupId','room_id as roomId','upgrade_status as statusV2','upgrade_desc as desc','group_number as groupNo', 'group_name as groupName', 'examine_status as status', 'assistant_wx as assistantWx', 'examine_step')
            ->where([['mid', $params['mid']], ['examine_status', '>', 0], ['examine_status', '<', 3], ['type', $params['source']]])
            ->orderBy('id', 'desc')
            ->paginate($params['pageSize']);
    }

    /**
     * @param $source
     * @return int
     */
    public static function changeSourceIsSet($source){
        $sql = "select id   from sline_community_user    where platform_identity='{$source}' ";
        $res  = DB::select($sql);
        return !empty($res)?$res[0]->id:0;
    }

    /**
     * @param $source
     * @return int
     */
    public static function changeSourceIsSetV2($source){
        $sql = "select mid   from sline_community_user where sup_id = 0 and platform_identity='{$source}' ";
        $res  = DB::select($sql);
        return !empty($res)?$res[0]->mid:0;
    }

    /**
     * 获取用户的间推群列表
     * @param $params
     * @return mixed
     */
    public static function getGroupListByMidArr($params)
    {
        return CommunityInvitationGroupModel::select('id as groupId', 'group_number as groupNo', 'group_name as groupName', 'examine_status as status', 'assistant_wx as assistantWx', 'examine_step')
            ->where(['examine_status' => 2, 'type' => $params['source']])
            ->whereIn('mid', $params['midArr'])
            ->orderBy('id', 'desc')
            ->paginate($params['pageSize']);
    }

    /**
     * 获取未分配的群助理微信
     * @return mixed
     */
    public static function getLeastUserGroupAssistant($type)
    {   

        return DB::table('assistant')->where(['type'=>$type,'platform_identity'=>'7323ff1a23f0bf1cda41f690d4089353'])->orderBy('count_num', 'asc')->first();
    }

    //获取专属机器人微信号

    public static function getExclusiveUserGroupAssistant($type,$platform_identity,$mid)
    {
        return DB::table('assistant')->where(['type'=>$type,'platform_identity'=>$platform_identity,'exclusive_mid'=>$mid,'deleted_at'=>0])->orderBy('count_num', 'asc')->first();
    }


    /**
     * @return Model|null|object|static
     */
   public  static function  getLeastUserGroupDarenAssistant($source){
       return DB::table('assistant')->where(['platform_identity'=>$source,'type'=>1])->orderBy('count_num', 'asc')->first();
   }


    /**
     * 获取未分配的群助理微信
     * @return mixed
     */
    public static function getLeastUserGroupAssistantV2($mid = 0)
    {   
        return DB::table('assistant')->where('mid',$mid)->orderBy('count_num', 'asc')->first();

    }

    /**
     * 获取专属机器人的
     * @param $mid
     * @return array|null|\stdClass
     */
    public static function getLeastUserGroupAssistantByMid($mid)
    {
        return DB::table('assistant')->where('mid', $mid)->orderBy('count_num', 'asc')->first();
    }
    /**
     * 修改群助理微信状态
     * @return mixed
     */
    public static function updateUserGroupAssistant($id)
    {
        return DB::table('assistant')->where('id', $id)->increment('count_num');
    }

    /**
     * 通过短链code获取长链接
     * @param $code
     * @return array|null|\stdClass
     */
    public static function getLongUrlByShortUrlCode($code)
    {
        return DB::connection('mysql_ylh')->table('long_short_url')->where('short_url_code', $code)->first();
    }

    /**
     * 通过短链code获取长链接
     * @param $code
     * @return array|null|\stdClass
     */
    public static function getLongUrlByShortUrlCodeV2($code)
    {
        return DB::connection('mysql_malls')->table('long_short_url')->where('short_url_code', $code)->first();
    }


    /**
     * 获取最新的一条数据
     * @param $mid
     * @return mixed
     */
    public static function getLastDataByMid($mid, $type,$Source)
    {
        return CommunityInvitationGroupModel::where([['mid', $mid], ['examine_status', '>', 0], ['type', $type], ['platform_identity', $Source]])->orderBy('id', 'desc')->first();
    }


    /**
     * 获取最新的一条数据
     * @param $mid
     * @return mixed
     */
    public static function getLastDataByMidV2($mid, $source)
    {
        if ($source) {
            return DB::connection('robot')->table('invitation_group')->where([['mid', $mid], ['examine_status', '>', 0], ['type', 1]])->orderBy('id', 'desc')->first();
        }else{
            return CommunityInvitationGroupModel::where([['mid', $mid], ['examine_status', '>', 0]])->orderBy('id', 'desc')->first();
        }
    }

    /**
     * 获取用户提交过审核的导师微信
     * @param $mid
     * @return mixed
     */
    public static function getToturWxByMid($mid)
    {
        return CommunityInvitationGroupModel::where([['mid', $mid], ['examine_status', '>', 0]])
            ->distinct()->pluck('tutor_wx')->toArray();
    }

    /**
     * 获取新人上手类型文章
     * @return \Illuminate\Support\Collection
     */
    public static function getRaidersNewList($Source,$type)
    {
        return DB::table('strategy_article')
            ->select('title', 'content', 'button_type as buttonType', 'tutor_wechat as copyText', 'is_video as isVideo', 'video_url as videoUrl')
            ->where(['strategy_category_id' => $type, 'deleted_at' => 0, 'platform_identity' => $Source])
            ->orderBy('sort', 'desc')
            ->get();

    }

    /**
     * 获取新人上手类型文章
     * @return \Illuminate\Support\Collection
     */
    public static function getRaidersNewListV2($Source)
    {
        return DB::table('strategy_article')
            ->select('title', 'content', 'button_type as buttonType', 'tutor_wechat as copyText', 'is_video as isVideo', 'video_url as videoUrl')
            ->where(['strategy_category_id' => 1, 'deleted_at' => 0, 'platform_identity' => $Source])
            ->orderBy('sort', 'desc')
            ->get();
    }



    /**
     * 获取指定的类型列表
     * @param array $typeIds
     * @param $pageIndex
     * @param $pageSize
     * @param array $except
     * @return mixed
     */
    public static function raidersYCloudStudyArticle(array $typeIds, $pageIndex, $pageSize,array $except=[])
    {

        //$query = ArticleNewModel::where('is_online', self::ON_LINE)->whereIn('type_id',$typeIds)->whereNotIn('aid',$except)->where('mid',$mid);
        $query = DB::connection('mysql_yuelvhui')->table('article_new')->where('is_online', 1)
            ->whereIn('type_id',$typeIds)->whereNotIn('aid',$except);
        //查询总数
        $tempQuery = clone $query;
        $data['totalRaw'] = $tempQuery->count();
        //计算页数
        $data['totalPage'] = ceil($data['totalRaw']/$pageSize);
        $data['pageIndex'] = $pageIndex;
        $data['pageSize'] = $pageSize;
        //当前页数据
        $models = $query->skip(($pageIndex-1)*$pageSize)
            ->take($pageSize)
            ->orderBy('sort','desc')
            ->orderBy('created_at','desc')
            ->get();
        $data['list'] = $models ? $models->toArray() : [];
        return $data;
    }
    /**
     * 获取申请通过的群
     * @param $mid
     * @return mixed
     */
    public static function getPassGroupByMid($mid, $type)
    {
        return CommunityInvitationGroupModel::where([['mid', $mid], ['examine_status', 2], ['type', $type]])->count();
    }


    /**
     * 获取申请通过的群
     * @param $mid
     * @return mixed
     */
    public static function getPassGroupByMidV2($mid, $source)
    {
        if ($source) {
            return DB::connection('robot')->table('invitation_group')->where([['mid', $mid], ['examine_status', 2], ['type', 1]])->count();
        }else{
            return CommunityInvitationGroupModel::where([['mid', $mid], ['examine_status', 2]])->count();
        }
    }

    /**
     * 获取邀请海报内容
     * @return \Illuminate\Support\Collection
     */
    public static function invitePosterList($source,$type)
    {
        //, 'invite_exclusive_img as posterImg'
        return DB::table('strategy_article')
            ->select('desc as content', 'invite_img as image')
            ->where(['strategy_category_id' =>$type, 'deleted_at' => 0, 'platform_identity' => $source])
            ->orderBy('sort', 'desc')
            ->get();
    }

    /**
     * 获取邀请海报内容
     * @return \Illuminate\Support\Collection
     */
    public static function invitePosterYCloudList()
    {
        return DB::connection('mysql_yuelvhui')->table('community_strategy_article')
            ->select('desc as content', 'invite_img as image', 'invite_exclusive_img as posterImg')
            ->where(['strategy_category_id' => 4, 'deleted_at' => 0])
            ->orderBy('sort', 'desc')
            ->get();
    }

    /**
     * 获取用户所有群列表
     * @param $params
     * @return mixed
     */
    public static function getAllGroupListByMidAndOrderSn($params)
    {
        return CommunityInvitationGroupModel::select('id as groupId', 'group_number as groupNo', 'group_name as groupName', 'examine_status as examineStatus', 'assistant_wx as assistantWx', 'tutor_wx as tutorWx')
            ->where(['mid' => $params['mid'], 'ordersn' => $params['ordersn']])
            ->orderBy('id', 'desc')->get();
    }

    /**
     * 批量添加数据
     * @param $data
     * @return mixed
     */
    public static function insertData($data)
    {
        return CommunityInvitationGroupModel::insert($data);
    }

    /**
     * 获取订单信息
     * @param $ordersn
     * @return array|null|\stdClass
     */
    public static function getOrderInfo($ordersn)
    {
        return DB::table('buy_info')->where('ordersn', $ordersn)->first();
    }


    /**
     * 群助理分类列表
     * @param $params
     * @return mixed
     */
    public static function getAllGroupCategory($source)
    {
        return DB::table('strategy_category') ->select('name as title ', 'id as labelId')->where(['platform_identity'=> $source,'deleted_at'=>0])  ->orderBy('id', 'asc') ->get();
    }


    /**
     * @param $typeIds
     * @param $pageIndex
     * @param $pageSize
     * @param $source
     * @return array
     */
    public static function getArticleListByTypeIds($typeIds, $pageIndex, $pageSize,$source)
    {
        $list = self::getAdvancedDataList($typeIds, $pageIndex, $pageSize,$source);
        $totalRaw = self::getGroupVerifyListCount($typeIds,$source);
        $totalPage = $totalRaw>0?ceil($totalRaw/$pageSize):0;

        //TODO  暂时富文本转化  纯文本
        foreach ($list as &$value) {
            //$value->text_content = $value->content;
            $content = htmlspecialchars_decode($value->content);
            $content = str_replace(" ", "", $content);
            $content = str_replace("\n", "", $content);
            $arr=array(
                "&nbsp;",
                "&ensp;",
                "&emsp;",
                "&thinsp;",
                "&zwnj;",
                "&zwj;",
                "&ldquo;",
                "&rdquo;",
                "&quot;",
                "&acute;",
                "&uml;",
                "&mdash;"
            );
            $content = str_replace($arr, "", $content);
            $value->content = strip_tags($content);
        }

        $pageIndex = $pageIndex;
        $pageSize = $pageSize;
        return   [
            'totalRaw' =>$totalRaw,
            'totalPage' => $totalPage,
            'pageIndex' =>$pageIndex,
            'pageSize' => $pageSize,
            'list' => $list,
        ];

    }


    /*
    * 查询审核列表
    */
    public static function getAdvancedDataList($typeIds,$page = 1,$pageSize = 10,$source)
    {
        $page = ($page-1)*$pageSize;
        $sql = "select id as aid,invite_img as coverImg,title,created_at,content  from sline_community_strategy_article    where platform_identity='{$source}' and deleted_at = 0 and  strategy_category_id={$typeIds}  order by sort  desc limit $page,$pageSize";
        return DB::select($sql);
    }


    public static function getArticleInfoId($typeId,$id,$source){
        $sql = "select id as aid,invite_img as coverImg,title,content,created_at  from sline_community_strategy_article    where platform_identity='{$source}'  and  strategy_category_id={$typeId} and id={$id}";
        return DB::select($sql);


    }


    /*
     * 获取总数量
     */
    public static function getGroupVerifyListCount($typeIds,$source)
    {
        $sql    = "select count(1) as countNum from sline_community_strategy_article    where platform_identity='{$source}'  and  strategy_category_id={$typeIds} ";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->countNum : 0;
    }


    /**
     * 获取用户提交过审核的导师微信
     * @param $mid
     * @return mixed
     */
    public static function getTagNameByTagIds($tagIds)
    {
        return DB::table('tag')->whereIn('id', $tagIds)
            ->distinct()->pluck('tag_name')->toArray();
    }


    /**
     * 获取用户提交过审核的导师微信
     * @param $mid
     * @return mixed
     */
    public static function getTagNameByTagId($tagId)
    {
        return DB::table('tag')->where(['id' => $tagId])
            ->pluck('tag_name')->first();
    }


    public static function getGroupTagList($param=[],$page=1,$pageSize=10)
    {   
        $page = ($page-1)*$pageSize;

        $where = self::getGroupTagWhere($param);

        $sql = "select * from sline_community_tag";
        $sql.= " where is_detete=0 $where order by sort,id asc limit $page,$pageSize";

        return DB::select($sql);
    }

    public static function getGroupTagCount($param=[])
    {   

        $where = self::getGroupTagWhere($param);

        $sql = "select count(*) count_1 from sline_community_tag";
        $sql.= " where is_detete=0 $where";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }

    public static function getGroupTagWhere($param)
    {

        $where = "";

        if(isset($param['platform_identity'])&&!empty($param['platform_identity']))
        {
            $where .= " and platform_identity='".$param['platform_identity']."'";  
        }

        if(isset($param['tag_name'])&&!empty($param['tag_name']))
        {
            $where .=" and tag_name like '%".$param['tag_name']."%' ";
        }

        return $where;
    }


    /*
     * 添加群标签
     */
    public static function addGroupTag($data)
    {
        return DB::table('tag')->insert($data);
    } 

    /*
     * 修改群标签
     */
    public static function updateGroupTag($id,$data)
    {
        return DB::table('tag')->where('id',$id)->update($data);
    }   

    /*
     * 获取群标签信息
     */

    public static function getTagInfo($param=[])
    {

        $where = "";

        if(isset($param['tag_name'])&&!empty($param['tag_name']))
        {
            $where=" and tag_name = '".$param['tag_name']."'";
        }

        if(isset($param['tag_id'])&&!empty($param['tag_id']))
        {
            $where=" and id = ".$param['tag_id'];
        }

        $sql = "select * from sline_community_tag where is_detete=0 $where ";

        return DB::selectOne($sql);
    }

    public static function getGroupCountByTagID($tagId)
    {

        $sql = "select count(*) count_1 from sline_community_member_info_by_appid where tag_id = $tagId and is_delete=0";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }

    /*
     * 修改群标签
     */
    public static function updateGroup($id,$data)
    {
        return DB::table('member_info_by_appid')->where('id',$id)->update($data);
    }

    /*
     * 获取优惠券列表
     */
    public static function getCouponList($param,$page,$pageSize)
    {

        $page = ($page-1)*$pageSize;

        $where  = self::getCouponListWhere($param);

        $sql = "select id,name,isopen,type,samount,totalnumber,send_num,amount,isnever,starttime,endtime,modules,antedate,use_num,typeid,son_modules,state,channel_type,typeid  from yuelvhui.sline_coupon";
    
        $sql .= " where isdel=0 and is_give=1 and typeid in(3,5) and isopen=1 $where order by id desc limit $page,$pageSize";

        $result = DB::select($sql);

        return $result;
    }

    /*
     * 获取优惠券列表
     */
    public static function getCouponCount($param)
    {

        $where  = self::getCouponListWhere($param);

        $sql = "select count(*) as count_1 from yuelvhui.sline_coupon";
    
        $sql .= " where isdel=0 and is_give=1 and typeid=3  and isopen=1 $where";


        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }

    /*
     * 获取优惠券详情
     */
    public static function getCouponInfo($param)
    {

        $where  = self::getCouponListWhere($param);

        $sql = "select * from yuelvhui.sline_coupon";
    
        $sql .= " where isdel=0 and is_give=1 and isopen=1 $where";

        $result = DB::selectOne($sql);

        return $result;
    }

    /*
     * 修改优惠券库存
     */    
    public static function updateCouponTotalNum($id = 0, $number)
    {
        $sql = "update yuelvhui.sline_coupon set totalnumber = totalnumber - ?, send_num = send_num + ? where id = ?";
        return DB::update($sql, [$number, $number, $id]);
    }

   /*
     * 发放优惠券
     */  
    public static function insertCoupon($data)
    {
        return DB::connection('mysql_yuelvhui')->table("member_coupon")->insert($data);
    }

    public static function insertCouponReload($data)
    {
        return DB::table("send_coupon_reload")->insert($data);
    }


    /*
     * 获取优惠券列表条件
     */
    public static function getCouponListWhere($param = [])
    {

        $where = "";

        if(isset($param['id'])&&$param['id']>0)
        {
            $where.=" and id=".$param['id'];
        }

        if(isset($param['channel_type']))
        {
            $where.=" and channel_type=".$param['channel_type'];
        }

        if(isset($param['coupon_name'])&&!empty($param['coupon_name']))
        {
            $where.=" and name like '%".$param['coupon_name']."%'";
        }

        return $where;
    }

    /**
     * @param $source
     * @return int
     */
    public static function getChannelDataBySource($source)
    {
        $sql = "select * from sline_community_channel_data where platform_identity='{$source}' and is_delete=0";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : 0;
    }


    /**
     *  根据mid 获取上级的parentid
     * @param $code
     * @return array|null|\stdClass
     */
    public static function getParentIdByMid($mid)
    {
        return DB::connection('mysql_yuelvhui')->table('member')->where('mid', $mid)->first();
    }


    /**
     * 总的GMV
     * @param $source
     * @return int
     */
    public static function getBindMidGmv($source,$wx_alias)
    {
        $sql = "select *  from sline_community_white_list_wx  where platform_identity='{$source}'  and  wx_alias='{$wx_alias}' and deleted_at=0";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : 0;
    }

    /**
     * 总的群数量
     * @param $source
     * @return int
     */
    public static function getBindGroupNum($mid)
    {
        $sql = "select count(1) as group_num,group_concat(id) as ids  from  sline_community_member_info_by_appid  where mid={$mid} and is_delete=0";
        $result = DB::selectOne($sql);
        return !empty($result) ? json_decode(json_encode($result),true) : [];
    }

    /**
     * 总的群数量
     * @param $source
     * @return int
     */
    public static function getBindGroupAllGmv($roomIds)
    {
        $beforeTime = time()-2592000;

        $sql = "select  sum(actual_price)  as allGroupgmv from  sline_community_mall_order_goods  where room_id in(".$roomIds.") and pay_time>=".$beforeTime;
        $result = DB::selectOne($sql);
        return !empty($result) ? json_decode(json_encode($result),true) : [];
    }

}