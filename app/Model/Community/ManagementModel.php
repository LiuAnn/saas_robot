<?php

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;

class ManagementModel extends Model
{
    // 指定表名
    protected $table = 'community_member_weixin_info';

    /**
     * 关联活跃度表
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @author  liujiaheng
     * @desc    []
     * @created 2020/4/7 5:17 下午
     */
    public function activity()
    {
        return $this->hasMany('App\Model\Community\GoodsBrowseModel', 'mid', 'mid');
    }

    /**
     * 关联VIP
     * @return mixed
     * @author  liujiaheng
     * @created 2020/4/7 6:36 下午
     */
    public function vip()
    {
        return $this->hasOne('App\Model\Member\MemberModel', 'mid', 'mid')->select(['mid', 'card_type']);
    }

    /**
     * 获取群用户列表
     * @param $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author  liujiaheng
     * @created 2020/4/7 5:51 下午
     */
    public static function queryMemberList($params)
    {
        return self::with(['vip', 'activity' => function ($query) use ($params) {
            // 筛选关联数据中当前群的数据
            $query->where(['romm_id' => $params['groupId']]);
            // 指定获取字段
            $query->select(['mid', 'romm_id', 'count_num', 'created_at']);
        }])
            ->leftJoin('member', 'community_member_weixin_info.mid', '=', 'member.mid')
            ->where(function ($query) use ($params) {
                // 群ID
                $query->where(['romm_id' => $params['groupId']]);
                // 会员身份
                if (isset($params['isPlus'])) {
                    if ($params['isPlus']) {
                        $query->where(['member.card_type' => 1]);
                    } else {
                        $query->where(['member.card_type' => 0]);
                    }
                }
            })
            ->select(['community_member_weixin_info.mid', 'romm_id',
                         'head_img', 'community_member_weixin_info.nickname'])
            ->paginate(isset($params['pageSize']) ? $params['pageSize'] : 20);
    }

    /**
     * 查询群成员总数
     * @param $params
     * @param $type
     * @return mixed
     * @author  liujiaheng
     * @created 2020/4/8 11:07 上午
     */
    public static function queryPeopleNumber($params, $type = 0)
    {
        return self::where(function ($query) use ($params, $type) {
            // 群ID
            $query->where(['romm_id' => $params['groupId']]);
            // 时间
            if (isset($params['date']) && $type) {
                $query->where('created_at', '>', "{$params['date']} 00:00:00");
                $query->where('created_at', '<', "{$params['date']} 23:59:59");
            }
        })->count();
    }

    /**
     * 群详细信息
     * @param $params
     * @return mixed
     * @author  liujiaheng
     * @created 2020/4/8 11:30 上午
     */
    public static function queryGroupInfo($params)
    {
        return self::where(['romm_id' => $params['groupId']])
            ->select(['room_nickname', 'head_img', 'nickname', 'room_wxid',])
            ->first();
    }

}
