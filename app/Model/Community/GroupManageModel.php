<?php
namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GroupManageModel extends Model{

    const GOOD_TYPE_ARRAY = [
        ['id' => 1, 'name'=> '自营'],
        ['id' => 2, 'name'=> '京东'],
        ['id' => 3, 'name'=> '拼多多'],
        ['id' => 5, 'name'=> '美团']
    ];


    /**
     * 返回对应的微信号
     * @param $member
     * @return int
     */
    public static function  getWxAlias($member){
        if ($member['user_type'] == 1) {
            $sql = "select  group_concat(assistant_wx)  as wxAlias  from sline_community_invitation_group where mid ={$member['mid']}  and  assistant_wx!='' group by assistant_wx ";
        } else {
            $sql = "select  group_concat(wx_alias)  as wxAlias  from sline_community_white_list_wx where mid ={$member['mid']}  and wx_alias!='' ";
        }
        $result = DB::select($sql);

        return !empty($result) ? $result->wxAlias : 0;
   }


    /*
     * 查询审核列表
     */
    public static function getGroupVerifyList($param,$page = 1,$pageSize = 10,$member,$wxAliasString)
    {

        if($member['user_type']==1){
            $param['mid']=$member['mid'];
        }else{
            $param['platform_identity']= $member['platform_identity'];
        }
        if(isset($param['startTime'])&&!empty($param['startTime']))
        {
            $param['startTime'] = strtotime($param['startTime']);
        }
        if(isset($param['endTime'])&&!empty($param['endTime']))
        {
            $param['endTime'] = strtotime($param['endTime']);
        }
        $where = self::getCommunityInvitationGroupWhere($param);
//        if(!isset($param['startTime']) && !isset($param['endTime']) && !isset($param['robot_wx'])) {
//            if (!isset($param['examineStatus']) && !isset($param['group_number'])) {
//                if (empty($member['sup_id'])) {
//                    $where .= " and scig.examine_status >= 0 or (assistant_wx = '' and examine_status < 2)";
//                }
//            } elseif ((isset($param['examineStatus']) && $param['examineStatus'] <= 1)) {
//                $where .= " or (scig.examine_status = {$param['examineStatus']} and assistant_wx = '')";
//            }
//        }

        $page = ($page-1)*$pageSize;
        $sql = "select scig.*,phone as mobile from sline_community_invitation_group  as  scig";
        $sql .= " where  1=1 {$where}   order by scig.id desc limit $page,$pageSize";
        return DB::select($sql);
    }


    /*
    * 二次 查询审核列表
    */
    public static function getGroupVerifyListTwice($param,$page = 1,$pageSize = 10,$member,$wxAliasString)
    {

        if($member['user_type']==1){
            $param['mid']=$member['mid'];
        }else{
            $param['platform_identity']= $member['platform_identity'];
        }

        $where = self::getCommunityInvitationGroupWhereV2($param);
        // if(!isset($param['startTime']) && !isset($param['endTime']) && !isset($param['robot_wx'])) {
        //     if (!isset($param['examineStatus']) && !isset($param['group_number'])) {
        //         if (empty($member['sup_id']) && empty($param['twice'])) {
        //             $where .= " and scig.examine_status >= 0 or (assistant_wx = '' and examine_status < 2)";
        //         }
        //     } elseif ((isset($param['examineStatus']) && $param['examineStatus'] <= 1)) {
        //         $where .= " or (scig.examine_status = {$param['examineStatus']} and assistant_wx = '')";
        //     }
        // }

        $page = ($page-1)*$pageSize;
        $sql = "select scig.* from sline_community_invitation_group  as  scig ";
        $sql .= " where  1=1 {$where}   order by scig.id desc limit $page,$pageSize";
        return DB::select($sql);
    }


    /*
     * 查询审核列表总条数
     */
    public static function getGroupVerifyListCount($param,$member,$wxAliasString)
    {
        if($member['user_type']==1){
            $param['mid']=$member['mid'];
        }
        $param['platform_identity'] = $member['platform_identity'];
        if(isset($param['startTime'])&&!empty($param['startTime']))
        {
            $param['startTime'] = strtotime($param['startTime']);
        }
        if(isset($param['endTime'])&&!empty($param['endTime']))
        {
            $param['endTime'] = strtotime($param['endTime']);
        }
        $where  = self::getCommunityInvitationGroupWhere($param);
//        if(!isset($param['startTime']) && !isset($param['endTime'])) {
//            if (!isset($param['examineStatus']) && !isset($param['group_number']) && !isset($param['robot_wx'])) {
//                if (empty($member['sup_id']) && empty($param['twice'])) {
//                    $where .= " and scig.examine_status >= 0 or (assistant_wx = '' and examine_status < 2)";
//                }
//            } elseif ((isset($param['examineStatus']) && $param['examineStatus'] <= 1)) {
//                $where .= " or (scig.examine_status = {$param['examineStatus']} and assistant_wx = '')";
//            }
//        }
        $sql    = "select count(*) count from sline_community_invitation_group as scig ";
        $sql    .= " where 1=1  {$where} ";
//        Log::info('getGroupVerifyListCount' . $sql);
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }



    //二次审核 总数量
    public static function getGroupVerifyListCountTwice($param,$member,$wxAliasString)
    {
        if($member['user_type']==1){
            $param['mid']=$member['mid'];
        }
        $param['platform_identity'] = $member['platform_identity'];

        $where  = self::getCommunityInvitationGroupWhereV2($param);
        if(!isset($param['startTime']) && !isset($param['endTime'])) {
            if (!isset($param['examineStatus']) && !isset($param['group_number']) && !isset($param['robot_wx'])) {
                if (empty($member['sup_id']) && empty($param['twice'])) {
                    $where .= " and scig.examine_status >= 0 or (assistant_wx = '' and examine_status < 2)";
                }
            } elseif ((isset($param['examineStatus']) && $param['examineStatus'] <= 1)) {
                $where .= " or (scig.examine_status = {$param['examineStatus']} and assistant_wx = '')";
            }
        }
        $sql    = "select count(*) count from sline_community_invitation_group as scig ";
        $sql    .= " where 1=1  {$where} ";
//        Log::info('getGroupVerifyListCount' . $sql);
        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }

    /**
     * 获取用户的直推群列表
     * @param $params
     * @return mixed
     */
    public static function getGroupCountByMid($mid = 0,$type = 1)
    {   
        $where = "";

        if($type == 1)
        {
            $where .= "and examine_status = 2";
        }

        if($type == 2)
        {
            $where .= "and upgrade_status = 2";
        }

        $sql = "select count(*) count_1 from sline_community_invitation_group where type = 1 and mid = $mid $where"; 

        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count_1 : 0;        
    }

    /**
     * 查看用户是否为小创
     * @param $mid
     * @return mixed
     */
    public static function isWork($mid = 0)
    {   
        $sql = "select count(*) count_1 from  yuelvhui.sline_member_work where mid = $mid ";

        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count_1 : 0;                
    }


    /**
     * 查看当前用户级别
     * @param $mid
     * @return mixed
     */
    public static function getUserIdentity($mid = 0)
    {   
        $sql = "select member_type,card_type from  yuelvhui.sline_member where mid = $mid ";

        return DB::selectOne($sql);
 
    }


    /**
     * 处理已是会员的成长值
     * @param $mid
     * @return array
     */
    public static function getOrderToMember($mid)
    {
        $sql = " select actual_price from sline_mall_order where pay_status = 1 and is_member_goods = 1 and mid = ? ";
        return DB::connection("mysql_yuelvhui")->select($sql,[$mid]);
    }

    /**
     * 查看当前用户级别
     * @param $mid
     * @return mixed
     */
    public static function getGrowthValue($mid = 0)
    {   
        $sql = "select sum(growth_value) as count_1 from  yuelvhui.sline_member_growth where mid = $mid ";

        $result = DB::selectOne($sql);

        return  $result->count_1;    
    }

    /**
     * 修改用户级别
     * @param $mid
     * @param $data
     * @return mixed
     */
    public static function updateMemberIdentity($mid=0,$data=[])
    {
        return  DB::connection('mysql_yuelvhui')->table("member")->where("mid",$mid)->update($data);

    }

    /**
     * 用户升级添加用户成长值
     * @param $mid
     * @param $growth_value
     * @return mixed
     */
    public static function addslineMemberGrowth($mid = 0,$growth_value = 200,$type=1)
    {
        $end_date = 0;

        switch ($type) {
            case 1:
                    $end_date = strtotime("+3 months");

                break;
            
            default:
                # code...
                break;
        }

        $data = [
            "mid"=>$mid,
            "growth_value"=>$growth_value,
            "end_time"=>$end_date,
            "created_at"=>time(),
            "updated_at"=>time(),
            "type"=>1
        ];

        return  DB::connection('mysql_yuelvhui')->table("member_growth")->insert($data);

    }

    /**
     * 用户升级添加记录
     * @param $mid
     * @param $growth_value
     * @return mixed
     */
    public static function addslineMemberExperienceCard($mid = 0,$type)
    {   

        $end_date = 0;

        switch ($type) {
            case 1:
                
                    $end_date = strtotime("+3 months");

                break;
            
            default:
                # code...
                break;
        }

        $data = [
            "mid"=>$mid,
            "created_at"=>time(),
            "end_date"=>$end_date
        ];

        return  DB::connection('mysql_yuelvhui')->table("member_experience_card")->insert($data);

    }

    public static function getCommunityInvitationGroupCountInfo($param)
    {
        $where  = self::getCommunityInvitationGroupCountWhere($param);
        $sql    = "select * from sline_community_invitation_group_member_count as scig where  scig.deleted_at = 0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }


    public static function addCommunityInvitationGroupCount($data) {
        return DB::table("invitation_group_member_count")->insert($data);
    }

    public static function upCommunityInvitationGroupCount($where,$data) {
        return DB::table("invitation_group_member_count")->where($where)->update($data);
    }


    public static function getMemberShipInfo($param)
    {
        $where='';

        if(isset($param['mid'])||!empty($param['mid']))
        {
            $where.=" and mid = {$param['mid']}";
        }

        if($where)
        {
            $sql    = "select * from yuelvhui.sline_member_ship where delete_time = 0 {$where}";
            $result = DB::selectOne($sql);
        }

        return !empty($result) ? $result : '';
    }

    public static function addCommunityInvitationGroupCountInfo($data) {
        return DB::table("invitation_group_member_count_info")->insert($data);
    }


    public static function getCommunityInvitationGroupCountWhere($param = [])
    {
        $where='';

        if(isset($param['id'])||!empty($param['id']))
        {
            $where.=" and id = {$param['id']}";
        }

        if(isset($param['mid'])||!empty($param['mid']))
        {
            $where.=" and mid = {$param['mid']}";
        }

        return $where;
    }


    /**
     * 更改审核群状态
     * @param timestamp
     * @return string
     */
    public static function upCommunityInvitationGroup($where,$data) {

        return DB::table("invitation_group")->where('id',$where)->update($data);

    }

    /*
     * 审核群详情
     * @param Array
     */
    public static function getGroupVerifyInfo($param,$field = "*")
    {
        $where  = self::getCommunityInvitationGroupWhere($param);
        $sql    = "select $field from sline_community_invitation_group scig where scig.examine_status >= 0 {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }


    /**
     *获取所有微信号
     * @param $mid
     * @return mixed|string
     */
    public static function getAllRobotWxAlias($mid, $sup_id, $type=0) {
        if($type!=1){
            if ($sup_id == 0) {
                $sql    = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where mid in ({$mid})  and wx_alias!=''   and   deleted_at=0 ";
            } else {
                $sql    = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where bind_mid in ({$mid})  and wx_alias!=''   and deleted_at=0 ";
            }
        }else{
            if ($sup_id == 0) {
                $sql    = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where bind_mid in ({$mid})  and wx_alias!=''   and  deleted_at=0 ";
            } else {
                $sql = "select  id,assistant_wx as wx_alias,wx_name,wxid  from sline_community_invitation_group where mid in ({$mid})  and  assistant_wx <> '' group by assistant_wx ";
            }
        }
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }


    /**
     *获取所有微信号对外
     * @param $platform_identity 平台标识
     * @param $mid  
     * @param $type  用户类型 1 小管理员 对应 bind_mid 2 渠道管理员 对应 mid 3 专属机器人管理员 对应 exclusive_mid
     * @return mixed|string
     */
    public static function getAllRobotWxAliasV2($platform_identity,$mid=0,$type=0) {

        $where = "";

        switch ($type) {
            case 1:
                $where = " mid = ".$mid;
                break;
            case 2:
                $where = " bind_mid = ".$mid;
                break;
            case 3:
                $where = " exclusive_mid = ".$mid;
                break;
            default:
                return [];
                break;
        }

        $where .= " and platform_identity='".$platform_identity."'";

        $sql = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where {$where} and wx_alias!='' and  deleted_at=0 ";

        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }



    /**
     *获取所有微信号
     * @param $mid
     * @return mixed|string
     */
    public static function getRobotWxAlias($mid, $sup_id, $type=0) {
        if($type!=1){
            if ($sup_id == 0) {
                $sql    = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where mid in ({$mid})  and wx_alias!='' and   wxid_type=1  and   deleted_at=0 ";
            } else {
                $sql    = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where bind_mid in ({$mid})  and wx_alias!='' and  wxid_type=1  and deleted_at=0 ";
            }
        }else{
            if ($sup_id == 0) {
                $sql    = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx where bind_mid in ({$mid})  and wx_alias!='' and  wxid_type=1  and  deleted_at=0 ";
            } else {
                $sql = "select  id,assistant_wx as wx_alias,wx_name,wxid  from sline_community_invitation_group where mid in ({$mid})  and  assistant_wx <> '' group by assistant_wx ";
            }
        }
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }



    /**
     *获取所有微信号
     * @param $mid
     * @return mixed|string
     */
    public static function getYCRobotWxAlias($mid) {
        $sql = "select  id,wx_alias,wx_name,wxid  from sline_community_white_list_wx";
        $sql .= " where exclusive_mid = {$mid}  and wx_alias!='' and deleted_at=0 ";
        //$sql .= " where exclusive_mid = {$mid}  and wx_alias!='' and  wxid_type=1  and deleted_at=0 ";
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }


    public static function getRobotExclusiveWxAlias($mid, $sup_id, $type=0) {
        if($type!=1){
            if ($sup_id == 0) {
                $sql    = "select  id,wx_alias,wx_name,wxid,all_order_sum,created_at  from sline_community_white_list_wx where   wxid_type=3  and    mid in ({$mid})  and wx_alias!='' and  deleted_at=0 ";
            } else {
                $sql    = "select  id,wx_alias,wx_name,wxid,all_order_sum,created_at  from sline_community_white_list_wx where   wxid_type=3  and    bind_mid in ({$mid})  and wx_alias!='' and  deleted_at=0 ";
            }
        }else{
            if ($sup_id == 0) {
                $sql    = "select  id,wx_alias,wx_name,wxid,all_order_sum,created_at  from sline_community_white_list_wx where  wxid_type=3  and     bind_mid in ({$mid})  and wx_alias!='' and  deleted_at=0 ";
            } else {
                $sql = "select  id,assistant_wx as wx_alias,wx_name,wxid  from sline_community_invitation_group where mid in ({$mid})  and  assistant_wx <> '' group by assistant_wx ";
            }
        }
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     *获取所有微信号
     * @param $mid
     * @return mixed|string
     */
    public static function getHouTaiRobotWxAlias() {
        $sql    = "select  wx_alias  from sline_community_white_list_wx where wx_alias!='' and  deleted_at=0 ";
        $result = DB::select($sql);
        return !empty($result) ? $result : '';
    }



    /**
     *获取所有微信号
     * @param $mid
     * @return mixed|string
     */
    public static function getRobotWxAliasNew($mid,$sup_id,$type=0) {
        if($type!=1){
            if($sup_id==0){
                $sql    = "select  wx_alias  from sline_community_white_list_wx where mid = {$mid}  and wx_alias!='' and  deleted_at=0 ";
            }else{
                $sql    = "select  wx_alias  from sline_community_white_list_wx where bind_mid = {$mid} and wx_alias!='' and  deleted_at=0 ";
            }
        }else{
            $sql    = "select  assistant_wx as wx_alias  from sline_community_invitation_group where mid = {$mid} and  assistant_wx <> '' group by assistant_wx ";
        }
        $result = DB::select($sql);
        return !empty($result) ? $result : '';
    }

    /**
     * 统计待提交 待审核  审核通过  审核拒绝
     * @param $wxAliasString
     * @param $member
     * @return array|string
     */
    public static function getNumStatus($wxAliasString, $member, $param)
    {
        $where1 = '';

        if ($member['user_type'] == 1) {
            $where1 .= "   mid in(" . $member['mid'] . ") ";
        }else{
//            $where1 .= "(type=1 AND assistant_wx in(" . $wxAliasString . ")) or (assistant_wx = '' and examine_status < 2)";
            $where1 .= "   type=1 ";
        }
        if(isset($param['platform_identity'])&&!empty($param['platform_identity']))
        {
            $where1.=" and platform_identity = '{$param['platform_identity']}'";
        }
        if(isset($param['startTime'])&&!empty($param['startTime']))
        {
            $where1.=" and addtime >= ".strtotime($param['startTime']);
        }

        if(isset($param['endTime'])&&!empty($param['endTime']))
        {
            $where1.=" and addtime <= ".strtotime($param['endTime']);
        }
        if(isset($param['robot_wx'])||!empty($param['robot_wx']))
        {
            if(strstr($param['robot_wx'], ',')) {
                $where1 .= " and  assistant_wx in ({$param['robot_wx']})";
            } else {
                $where1 .= " and  assistant_wx in ('{$param['robot_wx']}')";
            }
        }
        $sql = "select ";
        $sql .= "sum(case when examine_status=0  then 1 else 0 end )  tobesubNum,";
        $sql .= "sum(case when examine_status=1  then 1 else 0 end )  pendNum,";
        $sql .= "sum(case when examine_status=2  then 1 else 0 end )  adoptNum,";
        $sql .= "sum(case when examine_status=3  then 1 else 0 end )  rejectNum,";
        $sql .= "count(distinct(mid)) applyTotalNum ";
        $sql .= "from sline_community_invitation_group where   {$where1}";
        $result = DB::select($sql);
        return !empty($result) ? $result : '';
    }

    /**
     * 统计二次审核待提交 待审核  审核通过  审核拒绝
     * @param $wxAliasString
     * @param $member
     * @return array|string
     */
    public static function getNumStatusV2($wxAliasString, $member)
    {
        $where1 = '';
//        if (!empty($wxAliasString)) {
//            $where1 .= " and  assistant_wx in(" . $wxAliasString . ") ";
//        }
        if ($member['user_type'] == 1) {
            $where1 .= "   mid in(" . $member['mid'] . ") ";
        }else{
            $where1 .= "(type=1 AND assistant_wx in(" . $wxAliasString . ")) or (assistant_wx = '' and upgrade_status < 2)";
        }
        if (isset($member['twice']) && !empty($member['twice'])) {
            $where1 .= "   upgrade_status>0  ";
        }
        $sql = "select 
                    sum(case when upgrade_status=0  then 1 else 0 end )  tobesubNum,
                    sum(case when upgrade_status=1  then 1 else 0 end )  pendNum,
                    sum(case when upgrade_status=2  then 1 else 0 end )  adoptNum,
                    sum(case when upgrade_status=3  then 1 else 0 end )  rejectNum
                   from sline_community_invitation_group where   {$where1}";
        $result = DB::select($sql);
        return !empty($result) ? $result : '';
    }



    /*
     * 审核群搜索
     * @param Array
     */
    public static function getCommunityInvitationGroupWhere($param = [])
    {
        $where='';
        if(isset($param['id'])||!empty($param['id']))
        {
            $where.=" and scig.id = {$param['id']}";
        }
        if(isset($param['mobile'])||!empty($param['mobile']))
        {
            $where.="  and ( scig.phone={$param['mobile']}   or  scig.phone like '%{$param['mobile']}%' )";
        }
        if(isset($param['group_name'])||!empty($param['group_name']))
        {
            $where.="  and scig.group_name like '%{$param['group_name']}%'";
        }

        if(isset($param['type'])||!empty($param['type']))
        {
            $where.=" and scig.type = {$param['type']}";
        }

        if(isset($param['examineStatus'])||!empty($param['examineStatus']))
        {
            $where.=" and scig.examine_status = {$param['examineStatus']}";
        }


        if(isset($param['startTime'])||!empty($param['startTime']))
        {
            $where.=" and scig.addtime >= ".$param['startTime'];
        }

        if(isset($param['endTime'])||!empty($param['endTime']))
        {
            $where.=" and scig.addtime <= ".$param['endTime'];
        }

        if(isset($param['upgradeStatus'])||!empty($param['upgradeStatus']))
        {
            $where.=" and scig.upgrade_status = {$param['upgradeStatus']}";
        }

        if(isset($param['startTimeV1'])||!empty($param['startTimeV1']))
        {
            $where.=" and scig.upgrade_addtime >= ".$param['startTimeV1'];
        }

        if(isset($param['endTimeV1'])||!empty($param['endTimeV1']))
        {
            $where.=" and scig.upgrade_addtime <= ".$param['endTimeV1'];
        }


        if(isset($param['mid'])||!empty($param['mid']))
        {
            $where.=" and scig.mid = {$param['mid']}";
        }

        if(isset($param['robot_wx'])||!empty($param['robot_wx']))
        {
            if(strstr($param['robot_wx'], ',')) {
                $where .= " and  (scig.assistant_wx in ({$param['robot_wx']}) or scig.assistant_wx is null  )";
            } else {
                $where .= " and  scig.assistant_wx in ('{$param['robot_wx']}')";
            }
        }

        if(isset($param['group_number'])||!empty($param['group_number']))
        {
            $where.=" and scig.group_number like '%{$param['group_number']}%'";
        }

        if(isset($param['platform_identity'])&&!empty($param['platform_identity']))
        {
            $where.=" and scig.platform_identity = '{$param['platform_identity']}'";
        }

        if(isset($param['twice'])&&!empty($param['twice']))
        {
            $where.=" and scig.upgrade_status > 0";
        }

        if(isset($member['tag_id'])&&$param['tag_id']){
             $where.=" and scig.tag_id= {$param['tag_id']}";
        }

        return $where;
    }



    /*
     * 审核群搜索
     * @param Array
     */
    public static function getCommunityInvitationGroupWhereV2($param = [])
    {
        $where='';
        if(isset($param['id'])||!empty($param['id']))
        {
            $where.=" and scig.id = {$param['id']}";
        }
        if(isset($param['mobile'])||!empty($param['mobile']))
        {
            $where.="  and ( scig.mobile={$param['mobile']}   or  scig.mobile like '%{$param['mobile']}%' )";
        }
        if(isset($param['group_name'])||!empty($param['group_name']))
        {
            $where.="  and scig.group_name like '%{$param['group_name']}%'";
        }

        if(isset($param['type'])||!empty($param['type']))
        {
            $where.=" and scig.type = {$param['type']}";
        }

        if(isset($param['examineStatus'])||!empty($param['examineStatus']))
        {
            $where.=" and scig.examine_status = {$param['examineStatus']}";
        }


        if(isset($param['startTime'])||!empty($param['startTime']))
        {
            $where.=" and scig.addtime >= ".$param['startTime'];
        }

        if(isset($param['endTime'])||!empty($param['endTime']))
        {
            $where.=" and scig.addtime <= ".$param['endTime'];
        }

        if(isset($param['upgradeStatus'])||!empty($param['upgradeStatus']))
        {
            $where.=" and scig.upgrade_status = {$param['upgradeStatus']}";
        }

        if(isset($param['startTimeV1'])||!empty($param['startTimeV1']))
        {
            $where.=" and scig.upgrade_addtime >= ".$param['startTimeV1'];
        }

        if(isset($param['endTimeV1'])||!empty($param['endTimeV1']))
        {
            $where.=" and scig.upgrade_addtime <= ".$param['endTimeV1'];
        }


        if(isset($param['mid'])||!empty($param['mid']))
        {
            $where.=" and scig.mid = {$param['mid']}";
        }

        if(isset($param['robot_wx'])||!empty($param['robot_wx']))
        {
            if(strstr($param['robot_wx'], ',')) {
                $where .= " and  (scig.assistant_wx in ({$param['robot_wx']}) or scig.assistant_wx is null  )";
            } else {
                $where .= " and  scig.assistant_wx in ({$param['robot_wx']})";
            }
        }

        if(isset($param['group_number'])||!empty($param['group_number']))
        {
            $where.=" and scig.group_number like '%{$param['group_number']}%'";
        }

        if(isset($param['twice'])&&!empty($param['twice']))
        {
            $where.=" and scig.upgrade_status > 0";
        }

        if(isset($member['tag_id'])&&$param['tag_id']){
             $where.=" and scig.tag_id= {$param['tag_id']}";
        }

        return $where;
    }


    /*
     * 群列表
     * @param Array
     */
    public static function getCommunityList($page,$pageSize)
    {
        $page = ($page-1)*$pageSize;
        $sql = "select g.*,wx.id as rid from sline_community_member_info_by_appid g";
        $sql .= " left join sline_community_white_list_wx wx on wx.wxid = g.wxid";
        $sql .= " where is_delete = 0 limit $page,$pageSize";

        return DB::select($sql);
    }


    /*
     * 群主信息列表
     * @param Array
     */
    public static function getGroupOwnerList($page=1,$pageSize=10,$param = [])
    {   
        $page = ($page-1)*$pageSize;

        $where = "";

        if(isset($param['mobile'])&&!empty($param['mobile']))
        {
            $where = " and g.mobile like '%".$param['mobile']."%' ";
        }

        $sql = "select g.*,wx.id as rid,count(*) count from sline_community_member_info_by_appid g";
        $sql .= " left join sline_community_white_list_wx wx on wx.wxid = g.wxid";
        $sql .= " where g.is_delete = 0 $where group by g.mid limit $page,$pageSize";

        return DB::select($sql);
    }

    /*
     * 群主信息总数
     * @param Array
     */
    public static function getGroupOwnerCountV1($param = [])
    {   

        $where = "";

        if(isset($param['mobile'])&&!empty($param['mobile']))
        {
            $where = " and g.mobile like '%".$param['mobile']."%' ";
        }

        $sql = "select g.mid from sline_community_member_info_by_appid g";
        $sql .= " where g.is_delete = 0 $where group by g.mid";

        $result = DB::select($sql);

        return count($result);
    }


    public static function getlimitgroupCount($mid=0)
    {   
        $nowTime = time();

        $sql = "select sum(service_number) count_1 from sline_community_buy_info where deleted_at = 0 and duetime_at<$nowTime and mid = $mid ";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count_1 : 0;
    }


    /*
     * 消息列表
     * @param Array
     */
    public static function getMessageList($param=[],$page=1,$pageSize=10)
    {
        $page = ($page-1)*$pageSize;
        $sql  = "select * from sline_community_send_reload r";
        $sql .= " left join sline_community_send_info i on i.random_str=r.random_str";
        $sql .= " where r.deleted_at=0 order by r.created_at desc limit $page,$pageSize";

        return DB::select($sql);
    }

    /*
     * 消息列表总数量
     * @param Array
     */
    public static function getMessageCount($param=[])
    {

        $sql  = "select count(*) count_1 from sline_community_send_reload r";
        $sql .= " where r.deleted_at=0";

        $result = DB::selectOne($sql);   

        return !empty($result) ? $result->count_1 : 0; 
    }

    /*
     * 群总数
     * @param Array
     */
    public static function getCommunityCount($wxid = "")
    {   
        $where = "";

        if($wxid!=""){

            $where = " and wxid = '$wxid'";
        }

        $sql = "select count(*) count from sline_community_member_info_by_appid where is_delete = 0 $where ";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->count : 0;

    }

    /*
     * 群中男女比例占比
     * @param Array
     */
    public static function getErveySexCount($romm_id=0)
    {

        $sql = "select count(*) count,sum(sex=1)/count(*) manCount,sum(sex=2)/count(*) womanCount from sline_community_member_weixin_info where delete_time = 0  and romm_id = $romm_id";

        return DB::selectOne($sql);   

    }



    /*
     * 机器人列表
     * @param Array
     */
    public static function getRobotList($page = 1,$pageSize = 10)
    {

        $page = ($page-1)*$pageSize;

        $sql = "select * from sline_community_white_list_wx where deleted_at = 0 limit $page,$pageSize";

        return DB::select($sql);
    }


    /*
     * 机器人总量
     * @param Array
     */
    public static function getRobotCount($page = 1,$pageSize = 10)
    {

        $page = ($page-1)*$pageSize;

        $sql = "select count(*) count_1 from sline_community_white_list_wx where deleted_at = 0";

        $result = DB::selectOne($sql);   

        return !empty($result) ? $result->count_1 : 0; 
    }

    /*
     * 获取绑定机器人总量
     * @param Array
     */
    public static function getBindRobotCount()
    {

        $sql = "select count(*) from sline_community_member_info_by_appid where is_delete=0 group by wxid";

        return count(DB::select($sql));
    }

    /*
     * 获取群主数量
     * @param Array
     */
    public static function getGroupOwnerCount($param = [])
    {      

        $where = "";

        if(isset($param['wxid'])&&!empty($param['wxid'])){

            $where = " and wxid = '".$param['wxid']."'";
        }

        $sql = "select count(*) from sline_community_member_info_by_appid where is_delete = 0 $where group by mid";

        return count(DB::select($sql));

    }


    /*
     * 获取总GMV
     * @param Array
     */
    public static function getTotalGMV($groupid=0)
    {

        $where = "";

        if($groupid!=0){

            $where = " and room_id in ($groupid)";
        }else{

            $where = " and room_id>0";
        }

        $sql = "select sum(actual_price) totalmoney from  ".config('community.oldMallOrderGoods')."  where pay_status=1 $where";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->totalmoney/100:0;
    }

     /*
     * 获取总GMV
     * @param Array
     */
     public static function getEveryDayTotalGMV($groupid=0,$stattime = "")
     {

        $where = "";

        if($groupid!=0){

            $where = " and room_id in ($groupid)";
        }else{

            $where = " and room_id>0";
        }

        $sql = "select sum(actual_price) as  totalmoney,DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times from
".config('community.oldMallOrderGoods')."  where pay_status=1 and room_id>0 and FROM_UNIXTIME(pay_time,'%Y-%m-%d')>$stattime  GROUP BY DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d
%H:%i:%s'),'%Y-%m-%d')";


        return DB::select($sql);

     }   

    public static function getBonusByMid($mid=0,$groupid=0)
    {   
        if($mid==0||$groupid==0)
        {

            return 0;
        }

        $sql = "select sum(b.amount) totalmoney from ".config('community.oldMallOrderGoods')." o";

        $sql .= " left join yuelvhui.sline_member_bonus_becommitted b on b.order_no=o.ordersn_son";

        $sql .= " where o.pay_status=1 and b.mid=$mid and o.room_id in ($groupid)";

        $result = DB::selectOne($sql);

        return $result==null?$result->totalmoney:0;
    }


    //获取群用户数量
    public static function getUserNumber($today = 0,$groupid="")
    {   
        $where = "";

        if($today==1){

            $todayTime = date("Y-m-d",time());

            $where .= " and DateDiff('$todayTime',created_at)=0";
        }

        if($groupid!=""){

            $where .= " and romm_id in ($groupid)";
        }

        $sql = "select count(*) count_1 from sline_community_member_weixin_info where delete_time=0 $where ";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->count_1:0;

    }

    //获取机器人信息
    public static function getRobotInfo($id)
    {
        $sql = " select wxid from sline_community_white_list_wx where id=$id";

        $result = DB::selectOne($sql);

        return !empty($result)?$result->wxid:null;       

    }

    //获取群id
    public static function getRoomIds($wxid)
    {
        $array =  DB::table("member_info_by_appid")
            ->where("wxid",$wxid)
            ->pluck("id")
            ->toArray();
        return implode(",",$array);
    }

    public static function newDb()
    {
        return DB::connection('robot');
    }
    /**
     * 获取总数量
     */
    public  static function getSendGroupNumByLinkTypeCount($param)
    {
        $where = ' where 1=1 ';
        $where1 = self::getLinkTypeWhere($param);
        $sql = "select 
                 sum(case when link_type=1  then 1 else 0 end )  zhiboNum, 
                 sum(case when link_type=2  then 1 else 0 end )  listNum,
                 sum(case when link_type=3  then 1 else 0 end )  zhibogoodsNum, 
                 sum(case when link_type=4  then 1 else 0 end )  putongNum,  
                DATE_FORMAT(created_at,'%Y%m%d%H')  as datehours , DATE_FORMAT(created_at,'%H')  as hour ,
                DATE_FORMAT(created_at,'%Y-%m-%d')  
                as date from     `sline_community_send_room_reload` {$where} {$where1} group by datehours  order by  created_at desc  limit {$param['offset']},{$param['display']}";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 获取数据
     */
    public  static function getSendGroupNumByLinkType($param)
    {

        $where = ' where 1=1 ';
        $where1 = self::getLinkTypeWhere($param);
        $sql = " select count(1) as count_num from (select  DATE_FORMAT(created_at,'%Y%m%d%H')  as datehours , DATE_FORMAT(created_at,'%H')  as hour ,DATE_FORMAT(created_at,'%Y%m%d')  
                 as date from     `sline_community_send_room_reload` {$where} {$where1}  group by datehours) as t";
        $result = self::newDb()->selectOne($sql);
        return !empty($result) ? $result->count_num : 0;
    }



    public static function getLinkTypeWhere($param = [])
    {
        $where ='';
        $endTime = empty($param['sendEndTime']) ?'': date('Y-m-d H:i:s',(strtotime($param['sendEndTime'])+3500));
        //是否有时间筛选
        if (isset($param['sendStartTime']) && isset($param['sendStartTime'])) {
            $where = " and created_at >= '{$param['sendStartTime']}' and created_at <= '{$endTime}' ";
        } else if (isset($param['sendStartTime'])) {
            $where = " and created_at >='{$param['sendStartTime']}'";
        } else if (isset($param['sendEndTime'])) {
            $where = " and created_at <= '{$endTime}' ";
        }else{
            $where = "  ";
        }
        return $where;
    }

    /**
     * 获取可食用的标签
     * @param $platform_identity
     * @return array
     */
    public  static  function  getTagData($platform_identity)
    {
        $sql = "select id,tag_name  from  sline_community_tag where platform_identity=? and is_detete=0 ";
        $result =self::newDb() ->select($sql,[$platform_identity]);
        return $result;
    }


    /**
     * @param $table
     * @param $updateData
     * @return int
     */
    public static function upGroupTagGroupData($table,$updateData)
    {
        return DB::table($table)->where('room_id', $updateData['room_id'])->update($updateData);

    }

    /**
     * @param $roomId
     * @return array
     */
    public static function getGroupInfo($roomId)
    {
        $sql = " select id from sline_community_invitation_group where room_id=?";
        $result = DB::selectOne($sql,[$roomId]);
        return !empty($result)?$result->id:[];
    }
    /**
     * 获取群的基础信息
     * @param $roomId
     * @return array|mixed
     */
    public static function getGroupTagInfo($roomId)
    {
        $sql = " select id,mid,name,tag_id,order_num from sline_community_member_info_by_appid where id=?";
        $result = DB::selectOne($sql,[$roomId]);
        return !empty($result)?$result:[];
    }
    /**
     * 更新标签
     * @param $table
     * @param $updateData
     * @return int
     */
    public static function upGroupTagInfo($table,$updateData)
    {
        return DB::table($table)->where('id', $updateData['id'])->update($updateData);

    }

    public  static  function  getTagDataName($tag_id)
    {
        $sql = "select id,tag_name  from  sline_community_tag where id=? and is_detete=0 ";
        $result =self::newDb() ->selectOne($sql,[$tag_id]);
        return !empty($result) ? $result->tag_name :'';
    }

    /**
     * 订单列表数据
     * @param $wheredata
     * @param $member
     */
    public  static   function getOrderListData($wheredata,$member)
    {

        $where = ' where  smog.pay_time>0 and smog.pay_status=1 and scu.user_type=2 ';
        $fields = '';
        if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
            $fields = ' ,smog.good_type ';
        }
        if(!empty($wheredata['goodType'])) {
            $where.=  " and  smog.good_type = ".$wheredata['goodType']."";
        }   
        if(!empty($wheredata['groupName'])){
            $where.=  "  and  scm.name like '%".$wheredata['groupName']."%'";
        }
        if(!empty($wheredata['goodsName'])){
            $where.=  "  and  smog.goods_name like '%".$wheredata['goodsName']."%'";
        }
        if(!empty($wheredata['wxAliasString'])){
            $where.=  "  and  scm.wxid  in (".$wheredata['wxAliasString'].")";
        }
        if(!empty($wheredata['groupMid'])){
            $where.=  "  and  scm.mid = ".$wheredata['groupMid']."";
        }
        if(!empty($wheredata['buyerPhone'])){
            $where.=  " and  smog.buyer_phone like '%".$wheredata['buyerPhone']."%'";
        }
        if(!empty($wheredata['orderFrom'])){
            $where.=  " and  smog.order_from = ".$wheredata['orderFrom']."";
        }
        if(!empty($wheredata['channel'])){
            $where.=  " and  smog.channel = ".$wheredata['channel']."";
        }
        if(isset($wheredata['startTime'])&&$wheredata['startTime']!="")
        {   
            $where .=" and smog.created_at>".strtotime($wheredata['startTime']);
        }
        if(isset($wheredata['ordersn'])&&$wheredata['ordersn']!="")
        {   
            $where .=" and smog.ordersn=".$wheredata['ordersn'];
        }
        if(isset($wheredata['room_id'])&&$wheredata['room_id']!="")
        {   
            $where .=" and smog.room_id=".$wheredata['room_id'];
        }
        if(isset($wheredata['endTime'])&&$wheredata['endTime']!="")
        {   
            if($wheredata['endTime']==$wheredata['startTime'])
            {
                $wheredata['endTime'] =strtotime($wheredata['endTime'])+3600*24;
            }else{
                $wheredata['endTime'] =strtotime($wheredata['endTime']);
            }
            
            $where .=" and smog.created_at<".$wheredata['endTime'];
        }
        $orderBy = self::getOrderListDataOrderBy($wheredata);
//        订单列表，包含字段：购买人昵称（点击跳转好友列表详情）、购买手机号、商品名称、实付金额、下单时间、群名称（点击跳转群统计详情）、所属机器人（点击跳转机器人统计详情）、所属管理员；
//2.1、列表筛选条件：商品名称&群名称（文本框模糊查询）、机器人&管理员（下拉框）；
        $getListSql=" select smog.record_id,smog.ordersn,smog.order_from,smog.channel,smog.goods_name,smog.actual_price,smog.created_at,scm.name as groupName,scm.id as roomId,scw.wx_name,scm.wxid,";
        $getListSql.=" smog.buyer_nickname as nickname ,smog.buyer_phone as mobile ,scu.name{$fields}";
        $getListSql.=" from  ".config('community.oldMallOrderGoods')."   as smog";
        $getListSql.=" left join  sline_community_member_info_by_appid   as scm  on smog.room_id=scm.id";
        $getListSql.=" left join  sline_community_white_list_wx   as   scw  on scw.wxid=scm.wxid";
        $getListSql.=" left join  sline_community_user as  scu  on  scu.mid=scw.bind_mid";
        $getListSql.=" {$where} {$orderBy} limit {$wheredata['offer']},{$wheredata['pageSize']}";

        Log::info('订单列表的结果查询--' . $getListSql);
        $result = DB::select($getListSql);
        return $result;

    }


    /**
     * 订单列表数据
     * @param $wheredata
     * @param $member
     */
    public  static   function getOrderGMVData($wheredata,$member)
    {

        $where = ' where  smog.pay_time>0 and smog.pay_status=1 and scu.user_type=2 ';
        $fields = '';
        if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
            $fields = ' ,smog.good_type ';
        }
        if(!empty($wheredata['goodType'])) {
            $where.=  " and  smog.good_type = ".$wheredata['goodType']."";
        }
        if(!empty($wheredata['groupName'])){
            $where.=  "  and  scm.name like '%".$wheredata['groupName']."%'";
        }
        if(!empty($wheredata['goodsName'])){
            $where.=  "  and  smog.goods_name like '%".$wheredata['goodsName']."%'";
        }
        if(!empty($wheredata['wxAliasString'])){
            $where.=  "  and  scm.wxid  in (".$wheredata['wxAliasString'].")";
        }
        if(!empty($wheredata['groupMid'])){
            $where.=  "  and  scm.mid = ".$wheredata['groupMid']."";
        }
        if(!empty($wheredata['buyerPhone'])){
            $where.=  " and  smog.buyer_phone like '%".$wheredata['buyerPhone']."%'";
        }
        if(!empty($wheredata['orderFrom'])){
            $where.=  " and  smog.order_from = ".$wheredata['orderFrom']."";
        }
        if(!empty($wheredata['channel'])){
            $where.=  " and  smog.channel = ".$wheredata['channel']."";
        }
        if(isset($wheredata['startTime'])&&$wheredata['startTime']!="")
        {
            $where .=" and smog.created_at>".strtotime($wheredata['startTime']);
        }
        if(isset($wheredata['ordersn'])&&$wheredata['ordersn']!="")
        {
            $where .=" and smog.ordersn=".$wheredata['ordersn'];
        }
        if(isset($wheredata['room_id'])&&$wheredata['room_id']!="")
        {
            $where .=" and smog.room_id=".$wheredata['room_id'];
        }
        if(isset($wheredata['endTime'])&&$wheredata['endTime']!="")
        {
            if($wheredata['endTime']==$wheredata['startTime'])
            {
                $wheredata['endTime'] =strtotime($wheredata['endTime'])+3600*24;
            }else{
                $wheredata['endTime'] =strtotime($wheredata['endTime']);
            }

            $where .=" and smog.created_at<".$wheredata['endTime'];
        }
        //$orderBy = self::getOrderListDataOrderBy($wheredata);
//        订单列表，包含字段：购买人昵称（点击跳转好友列表详情）、购买手机号、商品名称、实付金额、下单时间、群名称（点击跳转群统计详情）、所属机器人（点击跳转机器人统计详情）、所属管理员；
//2.1、列表筛选条件：商品名称&群名称（文本框模糊查询）、机器人&管理员（下拉框）；
        $getListSql=" select SUM(smog.actual_price) as totalGMV";
        $getListSql .= " from  ".config('community.oldMallOrderGoods')."   as smog";
        $getListSql .= " left join  sline_community_member_info_by_appid   as scm  on smog.room_id=scm.id";
        $getListSql .= " left join  sline_community_white_list_wx   as   scw  on scw.wxid=scm.wxid";
        $getListSql .= " left join  sline_community_user as  scu  on  scu.mid=scw.bind_mid";
        $getListSql .= " {$where}";

        $result = DB::selectOne($getListSql);
        return !empty($result) ? $result->totalGMV : 0;
    }



    /**
     * 订单列表数据
     * @param $wheredata
     * @param $member
     */
    public  static   function getOrderListDataV2($wheredata)
    {

        $where = ' where  smog.pay_time>0 and smog.pay_status=1 and scu.user_type=2 ';
        $fields = '';
        if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
            $fields = ' ,smog.good_type ';
        }
        if(!empty($wheredata['goodType'])) {
            $where.=  " and  smog.good_type = ".$wheredata['goodType']."";
        }
        if(!empty($wheredata['groupName'])){
            $where.=  "  and  scm.name like '%".$wheredata['groupName']."%'";
        }
        if(!empty($wheredata['goodsName'])){
            $where.=  "  and  smog.goods_name like '%".$wheredata['goodsName']."%'";
        }
        if(!empty($wheredata['wxAliasString'])){
            $where.=  "  and  scm.wxid  in (".$wheredata['wxAliasString'].")";
        }
        if(!empty($wheredata['groupMid'])){
            $where.=  "  and  scm.mid = ".$wheredata['groupMid']."";
        }
        if(!empty($wheredata['buyerPhone'])){
            $where.=  " and  smog.buyer_phone like '%".$wheredata['buyerPhone']."%'";
        }
        if(!empty($wheredata['orderFrom'])){
            $where.=  " and  smog.order_from = ".$wheredata['orderFrom']."";
        }
        if(!empty($wheredata['channel'])){
            $where.=  " and  smog.channel = ".$wheredata['channel']."";
        }
        if(isset($wheredata['startTime'])&&$wheredata['startTime']!="")
        {
            $where .=" and smog.created_at>".strtotime($wheredata['startTime']);
        }
        if(isset($wheredata['ordersn'])&&$wheredata['ordersn']!="")
        {
            $where .=" and smog.ordersn=".$wheredata['ordersn'];
        }
        if(isset($wheredata['roomId'])&&$wheredata['roomId']!="")
        {
            $where .=" and smog.room_id=".$wheredata['roomId'];
        }
        if(isset($wheredata['endTime'])&&$wheredata['endTime']!="")
        {
            if($wheredata['endTime']==$wheredata['startTime'])
            {
                $wheredata['endTime'] =strtotime($wheredata['endTime'])+3600*24;
            }else{
                $wheredata['endTime'] =strtotime($wheredata['endTime']);
            }

            $where .=" and smog.created_at<".$wheredata['endTime'];
        }
        $orderBy = self::getOrderListDataOrderBy($wheredata);
//        订单列表，包含字段：购买人昵称（点击跳转好友列表详情）、购买手机号、商品名称、实付金额、下单时间、群名称（点击跳转群统计详情）、所属机器人（点击跳转机器人统计详情）、所属管理员；
//2.1、列表筛选条件：商品名称&群名称（文本框模糊查询）、机器人&管理员（下拉框）；
        $getListSql = " select smog.record_id,smog.ordersn,smog.order_from,smog.channel,smog.goods_name,smog.actual_price,smog.created_at,scm.name as groupName,scm.id as roomId,scw.wx_name,scm.wxid,";
        $getListSql .= " smog.buyer_nickname as nickname ,smog.buyer_phone as mobile ,smog.pay_type, scu.name{$fields}";
        $getListSql .= " from  ".config('community.oldMallOrderGoods')."   as smog";
        $getListSql .= " left join  sline_community_member_info_by_appid   as scm  on smog.room_id=scm.id";
        $getListSql .= " left join  sline_community_white_list_wx   as   scw  on scw.wxid=scm.wxid";
        $getListSql .= " left join  sline_community_user as  scu  on  scu.mid=scw.bind_mid";
        $getListSql .= " {$where} {$orderBy} limit {$wheredata['offer']},{$wheredata['pageSize']}";


        $result = DB::select($getListSql);
        return $result;

    }

    public static function getOrderListDataOrderBy($condition)
    {
        $orderBy = '';
        if(isset($condition['groupNameOrderBy'])&&!empty($condition['groupNameOrderBy'])&&in_array($condition['groupNameOrderBy'], ['asc', 'desc'])){
            $orderBy .= " GROUP BY smog.record_id,groupName order by groupName {$condition['groupNameOrderBy']}";
        }
        if(isset($condition['orderAmount'])&&!empty($condition['orderAmount'])&&in_array($condition['orderAmount'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderAmount {$condition['orderAmount']}" : "order by orderAmount {$condition['orderAmount']}";
        }
        if(isset($condition['orderMember'])&&!empty($condition['orderMember'])&&in_array($condition['orderMember'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", orderMember {$condition['orderMember']}" : "order by orderMember {$condition['orderMember']}";
        }
        if(isset($condition['groupMemberNum'])&&!empty($condition['groupMemberNum'])&&in_array($condition['groupMemberNum'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", groupMemberNum {$condition['orderAmount']}" : "order by groupMemberNum {$condition['groupMemberNum']}";
        }
        if(isset($condition['rebate'])&&!empty($condition['rebate'])&&in_array($condition['rebate'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", c.rebate {$condition['rebate']}" : "order by c.rebate {$condition['rebate']}";
        }
        if(isset($condition['rebateNew'])&&!empty($condition['rebateNew'])&&in_array($condition['rebateNew'], ['asc', 'desc'])){
            $orderBy = $orderBy ? $orderBy . ", rebateNew {$condition['rebateNew']}" : "order by rebateNew {$condition['rebateNew']}";
        }
        if($orderBy=='')
        {
            $orderBy .= "order by smog.created_at desc";
        }
        return $orderBy;
    }

    /**
     * 订单总数量
     * @param $wheredata
     * @param $member
     */
    public static function getOrderListCount($wheredata,$member)
    {
        $where = ' where  smog.pay_time>0 and smog.pay_status=1 and scu.user_type=2';
        $fields = '';
        if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
            $fields = ' ,smog.good_type ';
        }
        if(!empty($wheredata['goodType'])) {
            $where.=  " and  smog.good_type = ".$wheredata['goodType']."";
        }   
        if(!empty($wheredata['groupName'])){
            $where.=  " and  scm.name like '%".$wheredata['groupName']."%'";
        }
        if(!empty($wheredata['goodsName'])){
            $where.=  " and  smog.goods_name like '%".$wheredata['goodsName']."%'";
        }
        if(!empty($wheredata['wxAliasString'])){
            $where.=  " and  scm.wxid  in (".$wheredata['wxAliasString'].")";
        }
        if(!empty($wheredata['groupMid'])){
            $where.=  "  and  scm.mid = ".$wheredata['groupMid']."";
        }
        if(!empty($wheredata['buyerPhone'])){
            $where.=  " and  smog.buyer_phone like '%".$wheredata['buyerPhone']."%'";
        }
        if(!empty($wheredata['orderFrom'])){
            $where.=  " and  smog.order_from = ".$wheredata['orderFrom']."";
        }
        if(!empty($wheredata['channel'])){
            $where.=  " and  smog.channel = ".$wheredata['channel']."";
        }
        if(isset($wheredata['startTime'])&&$wheredata['startTime']!="")
        {   
            $where .=" and smog.created_at>".strtotime($wheredata['startTime']);
        }
        if(isset($wheredata['ordersn'])&&$wheredata['ordersn']!="")
        {   
            $where .=" and smog.ordersn=".$wheredata['ordersn'];
        }
        if(isset($wheredata['room_id'])&&$wheredata['room_id']!="")
        {   
            $where .=" and smog.room_id=".$wheredata['room_id'];
        }
        if(isset($wheredata['endTime'])&&$wheredata['endTime']!="")
        {   
            if($wheredata['endTime']==$wheredata['startTime'])
            {
                $wheredata['endTime'] =strtotime($wheredata['endTime'])+3600*24;
            }else{
                $wheredata['endTime'] =strtotime($wheredata['endTime']);
            }
            
            $where .=" and smog.created_at<".$wheredata['endTime'];
        }
        $getListSql=" select  count(*) as orderNumTotal from ".config('community.oldMallOrderGoods')." as smog";
        $getListSql.=" left join  sline_community_member_info_by_appid   as scm  on smog.room_id=scm.id";
        $getListSql.=" left join  sline_community_white_list_wx   as   scw  on scw.wxid=scm.wxid";
        $getListSql.=" left join  sline_community_user as  scu  on  scu.mid=scw.bind_mid";
        $getListSql.=" {$where} ";

        Log::info('订单列表的总条数查询--' . $getListSql);
        $result = DB::selectOne($getListSql);
        return !empty($result) ? $result->orderNumTotal : 0;
    }


    /**
     * 订单总数量
     * @param $wheredata
     * @param $member
     */
    public static function getOrderListCountV2($wheredata)
    {
        $where = ' where  smog.pay_time>0 and smog.pay_status=1 and scu.user_type=2 ';
        $fields = '';
        if(config('community.oldMallOrderGoods') == ' sline_community_mall_order_goods '){
            $fields = ' ,smog.good_type ';
        }
        if(!empty($wheredata['goodType'])) {
            $where.=  " and  smog.good_type = ".$wheredata['goodType']."";
        }
        if(!empty($wheredata['groupName'])){
            $where.=  " and  scm.name like '%".$wheredata['groupName']."%'";
        }
        if(!empty($wheredata['goodsName'])){
            $where.=  " and  smog.goods_name like '%".$wheredata['goodsName']."%'";
        }
        if(!empty($wheredata['wxAliasString'])){
            $where.=  " and  scm.wxid  in (".$wheredata['wxAliasString'].")";
        }
        if(!empty($wheredata['groupMid'])){
            $where.=  "  and  scm.mid = ".$wheredata['groupMid']."";
        }
        if(!empty($wheredata['buyerPhone'])){
            $where.=  " and  smog.buyer_phone like '%".$wheredata['buyerPhone']."%'";
        }
        if(!empty($wheredata['orderFrom'])){
            $where.=  " and  smog.order_from = ".$wheredata['orderFrom']."";
        }
        if(!empty($wheredata['channel'])){
            $where.=  " and  smog.channel = ".$wheredata['channel']."";
        }
        if(isset($wheredata['startTime'])&&$wheredata['startTime']!="")
        {
            $where .=" and smog.created_at>".strtotime($wheredata['startTime']);
        }
        if(isset($wheredata['ordersn'])&&$wheredata['ordersn']!="")
        {
            $where .=" and smog.ordersn=".$wheredata['ordersn'];
        }
        if(isset($wheredata['roomId'])&&$wheredata['roomId']!="")
        {
            $where .=" and smog.room_id=".$wheredata['roomId'];
        }
        if(isset($wheredata['endTime'])&&$wheredata['endTime']!="")
        {
            if($wheredata['endTime']==$wheredata['startTime'])
            {
                $wheredata['endTime'] =strtotime($wheredata['endTime'])+3600*24;
            }else{
                $wheredata['endTime'] =strtotime($wheredata['endTime']);
            }

            $where .=" and smog.created_at<".$wheredata['endTime'];
        }
        $getListSql = " select  count(*) as orderNumTotal from ".config('community.oldMallOrderGoods')." as smog";
        $getListSql .= " left join  sline_community_member_info_by_appid   as scm  on smog.room_id=scm.id";
        $getListSql .= " left join  sline_community_white_list_wx   as   scw  on scw.wxid=scm.wxid";
        $getListSql .= " left join  sline_community_user as  scu  on  scu.mid=scw.bind_mid";
        $getListSql .= " {$where} ";

        $result = DB::selectOne($getListSql);
        return !empty($result) ? $result->orderNumTotal : 0;
    }

    /**
     *获取所有微信号
     * @param $mid
     * @return mixed|string
     */
    public static function getRobotWxid($mid,$sup_id,$type=0) {
        if($type!=1){
            if($sup_id==0){
                $sql    = "select  wxid  from sline_community_white_list_wx where mid = {$mid}  and wx_alias!='' and  deleted_at=0 ";
            }else{
                $sql    = "select  wxid  from sline_community_white_list_wx where bind_mid = {$mid} and wx_alias!='' and  deleted_at=0 ";
            }
        }else{
            $sql    = "select  assistant_wx as wx_alias  from sline_community_invitation_group where mid = {$mid} and  assistant_wx <> '' group by assistant_wx ";
        }
        $result = DB::select($sql);
        return !empty($result) ? $result : '';
    }

    /**
     * 优惠券列表
     * @param $param
     * @param $page
     * @param $pageSize
     */
    public  static function getCouponList($param,$page,$pageSize)
    {
        $where = ' where smc.sourse>0 ';
        if(isset($param['startTime'])){
             $where.=  " and  smc.addtime  >= ".$param['startTime'] ;
        }
        if(isset($param['endTime'])){
            $where.=  " and  smc.addtime  <=".$param['endTime'] ;
        }
        $whereWxid='';
        if(isset($param['wxAliasString'])&&!empty($param['wxAliasString'])){

            $whereWxid=  "  where wxid in(".$param['wxAliasString'].") ";
        }

        if(isset($param['couponName'])){
            $where.=  " and  sc.name like '%{$param['couponName']}%' ";
        }
        $page = ($page-1)*$pageSize;
        $listDataSql = "SELECT sc.id,sc.name,ntsmc.* FROM (";
        $listDataSql .= "SELECT smc.cid,  SUM(case when sourse=1  then 1 else 0 end )  sendNum,";
        $listDataSql .= " SUM(case when sourse=2  then 1 else 0 end )  receiveNum,";
        $listDataSql .= " SUM(case when  sourse=1  and usetime>0  then 1 else 0 end )  adoptNum";
        $listDataSql .= " FROM  yuelvhui.sline_member_coupon    as smc FORCE INDEX (`mid`)";
        $listDataSql .= " LEFT  JOIN  yuelvhui.sline_coupon    as sc  on smc.cid=sc.id";
        $listDataSql .= " {$where}  and smc.mid in ";
        $listDataSql .= " (select mid  from sline_community_member_info_by_appid FORCE INDEX (`wxid`)";
        $listDataSql .= " {$whereWxid}) group by smc.cid";
        $listDataSql .= ") as ntsmc LEFT JOIN yuelvhui.sline_coupon as sc on ntsmc.cid = sc.id";
        $listDataSql .= " group by sc.id limit $page,$pageSize ";
        Log::info('getGroupVerifyListCount' . $listDataSql);
        $result = DB::select($listDataSql);
        return $result;
    }


    /**
     * 优惠券列表
     * @param $param
     * @param $page
     * @param $pageSize
     */
    public  static function getCouponCount($param)
    {
        $where = ' where smc.sourse>0 ';
        if(isset($param['startTime'])){
            $where.=  " and  smc.addtime  >= ".$param['startTime'] ;
        }
        if(isset($param['endTime'])){
            $where.=  " and  smc.addtime  <=".$param['endTime'] ;
        }
        $whereWxid='';
        if(isset($param['wxAliasString'])&&!empty($param['wxAliasString'])){
            $whereWxid=  "  where wxid in(".$param['wxAliasString'].") ";
        }
        if(isset($param['couponName'])){
            $where.=  " and  sc.name like '%{$param['couponName']}%' ";
        }
        $listDataSqlCount = "select
        count(1) as total
       from (select  count(1) as count_num
          from  yuelvhui.sline_member_coupon    as smc  
          left  join  yuelvhui.sline_coupon    as sc  on smc.cid=sc.id  
          {$where}  and smc.mid in  (select mid   from
          (select mid  from sline_community_member_info_by_appid  {$whereWxid})as t) group by sc.id ) as u";
        $result = DB::select($listDataSqlCount);
        return !empty($result) ? $result[0]->total : 0;

    }


    /**
     * 获取群订单列表
     * @param $param
     * @param $page
     * @param $pageSize
     */
    public  static function getGroupOrderListData($param,$page,$pageSize)
    {
        $where =self::getGroupOrderListWhere($param);
        $page = ($page-1)*$pageSize;
        $room_id = $param['room_id']?$param['room_id']:0;

        $listSql = "select o.* from  ".config('community.oldMallOrderGoods')." o ";

        if(isset($param['channel'])&&$param['channel']==1){

            $listSql.= " left join yuelvhui.sline_member_work w on o.member_id = w.mid ";
        }

        $listSql.= " where o.room_id=$room_id $where ";

        if(isset($param['is_page'])&&$param['is_page']==1){

            $listSql .=" limit $page,$pageSize";
        }

        $result = DB::select($listSql);
        return !empty($result) ? $result:[];

    }

    /**
     * 获取群订单列表总条数
     * @param $param
     * @param $page
     * @param $pageSize
     */
    public  static function getGroupOrderListCount($param)
    {
        $where =self::getGroupOrderListWhere($param);
        $room_id = $param['room_id']?$param['room_id']:0;

        $getlistCount = "select count(1) as total from  ".config('community.oldMallOrderGoods')." o ";

        if(isset($param['channel'])&&$param['channel']==1){

            $getlistCount.= " left join yuelvhui.sline_member_work w on o.member_id = w.mid ";
        }

        $getlistCount.= " where  o.room_id=$room_id $where";

        $result = DB::select($getlistCount);

        return !empty($result) ? $result[0]->total : 0;

    }


    public static function getGroupOrderListWhere($param)
    {

        $where = "";

        if(isset($param['channel'])&&$param['channel']>0)
        {

            $where .=" and o.channel=".$param['channel'];

        }

        if(isset($param['channel'])&&$param['channel']==1&&isset($param['mid']))
        {

            $where .=" and (w.mid is null or (o.member_id = ".$param['mid']." and w.mid>0 )) ";
        }

        if(isset($param['startTime'])&&!empty($param['startTime']))
        {

            $where .=" and o.pay_time>".$param['startTime'];
        }

        if(isset($param['endTime'])&&!empty($param['endTime']))
        {   

            if($param['endTime']==$param['startTime'])
            {
                $param['endTime'] = $param['startTime']+3600*24;
            }

            $where .=" and o.pay_time<".$param['endTime'];
        }

        return $where;
    }


    public static function getUserParentIdByMid($mid=0)
    {
        $sql = "select parent_id from yuelvhui.sline_member where mid=$mid";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->parent_id : 0;
    }


    public static  function  getUserParentIdByMidNew($mid=0)
    {
        $sql = " select parent_id,mobile,nickname from yuelvhui.sline_member where mid=$mid";

        $result = DB::selectOne($sql);
        $returnData = json_decode(json_encode($result),true);
        return !empty($returnData) ? $returnData :[];
    }

    public static function getChannelIdByPlatform($platform_identity)
    {
        $sql = "select id from sline_community_channel_data where platform_identity='".$platform_identity."'";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result->id : 0;
    }

}