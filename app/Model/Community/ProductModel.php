<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/27
 * Time: 20:30
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class ProductModel  extends Model
{


    public static function getGoods($offset = 0, $limit = 10, $param = [])
    {
        $andWhere = self::getGoodsAndWhere($param);

        $order = "order by id desc";

        if (isset($param['sorttype'])&&$param['sorttype']>0) {
            
            switch ($param['sorttype']) {
                case 1: //按照佣金从低到高排序
                    
                    if($param['sort']==1)
                    {

                        $order = "order by gSku.product_distribution asc";

                        
                    }else{

                        $order = "order by gSku.product_distribution desc";

                    }

                    break;

                case 2: //按照佣金从高到低排序
                    
                    

                    break;
                
                case 3: //按照价格从低到高排序
                    
                    if($param['sort']==1)
                    {

                        $order = "order by gSku.vip_price asc";

                        
                    }else{
                        
                        $order = "order by gSku.vip_price desc";
                        
                    }

                    break;

                case 4: //按照折扣比例排序
                    

                    if($param['sort']==1)
                    {

                        $order = "order by gSku.discount asc";

                        
                    }else{
                        
                        $order = "order by gSku.discount desc";
                        
                    }
                    

                    break;
                default:
                    # code...
                    break;
            }
        }

        $sql      = "select p.id,p.channel_id,p.middle_product_id,p.distribution_id,p.name, p.create_time, p.product_coupon, p.product_discount, p.state,p.spu_id,p.spl_id,p.is_check,temp.activity_name,temp.act_id,gSku.discount  from ylh_mall.mall_product p";
        $sql     .= " left join (select group_concat(ma.activity_name) activity_name,product_id,ma.id act_id from ylh_mall.mall_product_activities mpa ";
        $sql     .= " left join ylh_mall.mall_activities ma on mpa.activity_id = ma.id  where mpa.deleted_at = 0 group by mpa.product_id) temp on temp.product_id = p.id";
        $sql     .= " left join ylh_mall.mall_supplier ms on p.spl_id = ms.id ";
        $sql     .= " left join (select min(vip_price) as vip_price,buy_price,state as sku_state,(vip_price-buy_price) as product_distribution,FORMAT((vip_price/price)*100,2) as discount,product_id from ylh_mall.mall_product_sku group by product_id) gSku on gSku.product_id = p.id";
        $sql     .= " where  p.id > 0 and gSku.sku_state=1  $andWhere  $order  limit $offset, $limit";

        return DB::select($sql, [$offset, $limit]);
    }

    public static function getGoodsCount($param = [])
    {
        $andWhere = self::getGoodsAndWhere($param);
        $sql      = "select count(*) total  from ylh_mall.mall_product p";
        $sql     .= " left join (select group_concat(ma.activity_name) activity_name,product_id,ma.id act_id from ylh_mall.mall_product_activities mpa ";
        $sql     .= " left join ylh_mall.mall_activities ma on mpa.activity_id = ma.id  where mpa.deleted_at = 0 group by mpa.product_id) temp on temp.product_id = p.id";
        $sql     .= " left join (select min(vip_price) as vip_price,product_id from ylh_mall.mall_product_sku group by product_id) gSku on gSku.product_id = p.id";
        $sql     .= " left join ylh_mall.mall_supplier ms on p.spl_id = ms.id  where  p.id > 0 " . $andWhere;
        return DB::selectOne($sql)->total;
    }

    public static function getGoodsAndWhere($param = [])
    {
        $andWhere = " and  p.state=1 ";

        if (isset($param['is_live'])) {
            $andWhere .= ' and p.is_live = ' . $param['is_live'];
        }

        if (isset($param['is_integral'])) {
            $andWhere .= ' and p.is_integral_goods = ' . $param['is_integral'];
        }


        if(isset($param['maxPrice'])&&$param['maxPrice']>0)
        {
            $andWhere .= ' and gSku.vip_price < ' . $param['maxPrice']*100;
        }

        if(isset($param['minPrice'])&&$param['minPrice']>0)
        {
            $andWhere .= ' and gSku.vip_price > ' . $param['minPrice']*100;
        }

        if (isset($param['distribution_id'])) {
            $andWhere .= ' and p.distribution_id = ' . $param['distribution_id'];
        }


        if (isset($param['goodName'])) {
            $andWhere .= " and (p.name like '%{$param['goodName']}%' or ms.supplier_name like '%{$param['goodName']}%') ";
        }

        if (isset($param['bandId'])) {
            $andWhere .= " and (p.category like '%{$param['bandId']}%') ";
        }


        $andWhere .= ' and p.channel_id != ' . 1;


        if (isset($param['is_check'])&&$param['is_check']!=null) {
            $andWhere .= ' and p.is_check = ' . $param['is_check'];
        }else{
            $andWhere .= ' and p.is_check = 3' ;
        }


        // if(isset($param['skuId']))
        // {
        //     $skuInfo = ProductSkuModel::getAllSkuInfo($param['skuId']);

        //     if(!empty($skuInfo))
        //     {
        //         $andWhere .= ' and p.id = '.$skuInfo->product_id;
        //     }
        // }



        return $andWhere;
    }


    /**
     * 自营一级分类
     * @return array
     */
    public static function getChannelOneCategoryList()
    {
        $sql = "select id,name,parent_id,channel_id from ylh_mall.mall_category where delete_time=0 and  channel_id = 0 and parent_id = 0 order by sort asc";
        return DB::select($sql);
    }


    public static function getNewSkuId($productId = 0)
    {
        $productSkuModel = DB::connection('mysql_malls')->table("product_sku")->where('product_id', $productId)->orderby('vip_price','asc')->where('state',1)
            ->first([
                'id as skuId',
                'id as skuId',
                'buy_price',
                'vip_price',
                'price',
                'official_price',
                'product_id',
                'inventory',
                'product_sale_value_id',
                'product_sale_value_name'
            ]);

        if (empty($productSkuModel)) {
            return [];
        }
        return $productSkuModel;
    }


    /**
     * 根据id获取供应商信息
     * @param int $splid
     * @return array|mixed
     */
    public static function getSupplierInfoById($splid = 0)
    {
        if (!$splid) {
            return [];
        }
        $sql = "select id,supplier_name,supplier_nickname,supplier_people,supplier_mobile,supplier_remarks from mall_supplier where id =" . $splid;
        return DB::connection('mysql_malls')->selectOne($sql);

    }


    public static function getNewSkus($productId = 0)
    {
        $productSkuModel = DB::connection('mysql_malls')->table("product_sku")->where('product_id', $productId)->where('state', 1)
            ->get([
                'id as skuId',
                'buy_price',
                'vip_price',
                'price',
                'official_price',
                'product_id',
                'inventory',
                'product_sale_value_id',
                'product_sale_value_name'
            ]);
        if (empty($productSkuModel)) {
            return [];
        }
        return $productSkuModel;
    }

    public static function getDistributionInfo($id = 0)
    {
        $productSkuModel = DB::connection('mysql_malls')->table("distribution")->where('id',$id)->where('deleted_at', 0)
            ->first([
                'distribution_level2'
            ]);
        return empty($productSkuModel)?0:$productSkuModel->distribution_level2;
    }

    public static function getGoodExistByGoodId($id = 0)
    {
        $where = [
            'id' => $id,
            'is_sell' => 1,
            'state' => 1
        ];
        return DB::connection('mysql_malls')->table("product")->where($where)
            ->count();
    }

    public static function getDistributionInfoByGoodsId($id=0)
    {
        $sql  = " select d.distribution_level2 from ylh_mall.mall_product p";
        $sql .= " left join ylh_mall.mall_distribution d on d.id=p.distribution_id";
        $sql .= " where p.id=$id";

        $result = DB::connection('mysql_malls')->selectOne($sql);

        return empty($result)?0:$result->distribution_level2;
    }


    public static function getConverImage($productId = 0)
    {
        $productImageModel = DB::connection('mysql_malls')->table("product_image")->where('yn', 1)->where('is_primary', 1)->where('product_id', $productId)
            ->first(['path']);
        return empty($productImageModel) ? '' : $productImageModel->path;
    }


    public static function getNewSuggestImage($productId = 0)
    {
        $productImageModel = DB::connection('mysql_malls')->table("product_image")->where('yn', 1)->where('product_id', $productId)
            ->get(['path']);

        if (empty($productImageModel)) {
            return [];
        }

        $productImageModel = $productImageModel->toArray();

        return array_filter(array_column($productImageModel, 'path'));
    }


    public static function getSaleInfo($saleValueId = 0)
    {
        $productSaleValueModel = DB::connection('mysql_malls')->table("product_sale_value")->where('id', $saleValueId)->first(['product_sale_key_id', 'sale_image_path']);

        if (empty($productSaleValueModel)) {
            return [];
        }
        return $productSaleValueModel;
    }

    public static function getSkuNum($param = [])
    {
        $andWhere = self::getGoodsAndWhere($param);
        $sql      = "select count(*) total from mall_product_sku sku left join mall_product p on sku.product_id=p.id "
            . " left join (select group_concat(ma.activity_name) activity_name,product_id,ma.id act_id from mall_product_activities mpa left join"
            . " mall_activities ma on mpa.activity_id = ma.id  where mpa.deleted_at = 0 group by mpa.product_id) temp on temp.product_id = p.id"
            . " left join mall_supplier ms on p.spl_id = ms.id "
            . " where  p.id > 0 and sku.state=1 $andWhere";
        return DB::connection('mysql_malls')->selectOne($sql)->total;

    }

    /**
     * 获取上架商品总数
     * @param array $param
     * @return mixed
     */
    public static function getOnlineNum($param = [])
    {
        $andWhere = self::getGoodsAndWhere($param);
        $sql      = "select count(*) total  from mall_product p"
            . " left join (select group_concat(ma.activity_name) activity_name,product_id,ma.id act_id from mall_product_activities mpa left join"
            . " mall_activities ma on mpa.activity_id = ma.id  where mpa.deleted_at = 0 group by mpa.product_id) temp on temp.product_id = p.id"
            . " left join mall_supplier ms on p.spl_id = ms.id where p.id > 0 and p.state = 1  " . $andWhere;
        return DB::connection('mysql_malls')->selectOne($sql)->total;
    }

    /**
     * 获取下架商品总数
     * @param array $param
     * @return mixed
     */
    public static function getDownNum($param = [])
    {
        $andWhere = self::getGoodsAndWhere($param);
        $sql      = "select count(*) total  from mall_product p"
            . " left join (select group_concat(ma.activity_name) activity_name,product_id,ma.id act_id from mall_product_activities mpa left join"
            . " mall_activities ma on mpa.activity_id = ma.id  where mpa.deleted_at = 0 group by mpa.product_id) temp on temp.product_id = p.id"
            . " left join mall_supplier ms on p.spl_id = ms.id  where  p.id > 0 and p.state = 0  " . $andWhere;
        return DB::connection('mysql_malls')->selectOne($sql)->total;
    }

    /**
     *
     * @param $distributionId
     * @return mixed
     */
    public static function getDistributions($distributionId)
    {
        $sql      = "select distribution_type,distribution_first,distribution_two,
               distribution_share,distribution_company,distribution_self,distribution_level0,distribution_level1,distribution_level2               
                from mall_distribution  where  id={$distributionId} and deleted_at=0";
        return DB::connection('mysql_malls')->selectOne($sql);

    }


    public  static  function  getSalelifeCount($type){
        $sql      = "select count(1) as count_num  from  sline_community_mall_order_goods where good_type={$type} ";
        $result = DB::selectOne($sql);
        return $result->count_num>0?$result->count_num:0;
    }


    public  static  function  getSaleNum($gid){
        $sql      = "select count(1) as count_num  from  sline_community_mall_order_goods where goods_id={$gid} ";
        $result = DB::selectOne($sql);
        return $result->count_num>0?$result->count_num:0;
    }


    public static function insertTbkLongUrl($param){

        return DB::table('tbk_longurl')->insert($param);
    }

    public static function getTbkLongUrl($gid=0){

        if(empty($gid)||$gid<1)
            return '';

        $result =  DB::table('tbk_longurl')->where('gid',$gid)->orderby('id','desc')->value('longUrl');

        return empty($result)?'':$result;
    }

    public static function upGoodsPriceData($goods_id = 0, $data = []){
        return DB::connection('robot')->table('mall_order_goods')->where('goods_id', $goods_id)->update($data);
    }


    /**
     * 获取总的条数
     * @param $type
     * @return int
     */
    public  static  function  getNomemberPrice($type){
        $sql      = "select count(1) as count_num  from  
(select count(1)  from  sline_community_mall_order_goods where good_type={$type} and goods_price_member=0 group by goods_id) as p ";

        $result = DB::selectOne($sql);
        return $result->count_num>0?$result->count_num:0;
    }

    /**
     * 获取数据
     * @param $type
     * @param $offet
     * @param $limit
     * @return mixed
     */
    public  static  function  getNomemberPriceData($type,$offet,$limit){
        $sql      = "select   goods_id,good_type,actual_price  from  sline_community_mall_order_goods where good_type={$type} and goods_price_member=0.00  group by goods_id limit {$offet},{$limit} ";
        $result = DB::select($sql);
        return  json_decode(json_encode($result),true);
    }


    /**
     * 获取cps混合活动列表
     * @param $type
     * @param $offet
     * @param $limit
     * @return mixed
     */
    public  static  function  getCpsActiveList($para){
        $page = ($para['page']-1)*$para['pageSize'];
        $pageSize = $para['pageSize'];

        $where = "";

        if(isset($para['goodName'])&&$para['goodName']!=''){

            $where  = " and  activity_name = {$para['goodName']}";
        }
        $sql      = "select * from ylh_mall.mall_activities where activity_type=2 and deleted_at = 0 {$where} order by id desc limit $page,$pageSize ";
        $result = DB::select($sql);
        return  json_decode(json_encode($result),true);
    }

    /**
     * 获取活动列表
     * @param $type
     * @param $offet
     * @param $limit
     * @return mixed
     */
    public  static  function  getActiveList($para){

        $page = ($para['page']-1)*$para['pageSize'];
        $pageSize = $para['pageSize']?$para['pageSize']:10;

        $where = "";

        if(isset($para['activeName'])&&$para['activeName']!=''){

            $where  = " and  active_name like '%".$para['activeName']."%'";
        }

        if(isset($para['platform_identity'])&&$para['platform_identity']!=''){

            $where .= " and platform_identity = '".$para['platform_identity']."' ";
        }

        $sql = "select * from sline_community_active where is_delete = 0 {$where} order by id desc limit $page,$pageSize ";
        

        $result = DB::select($sql);
        
        return  json_decode(json_encode($result),true);
    }

    /**
     * 获取活动列表条数
     * @param $type
     * @param $offet
     * @param $limit
     * @return mixed
     */
    public  static  function  getActiveListCount($para){

        $where = "";

        if(isset($para['activeName'])&&$para['activeName']!=''){

            $where  = " and  active_name like '%".$para['activeName']."%'";
        }

        if(isset($para['platform_identity'])&&$para['platform_identity']!=''){

            $where .= " and platform_identity = '".$para['platform_identity']."' ";
        }

        $sql      = "select count(*) as count_1 from sline_community_active where is_delete = 0 {$where} ";
        
        $result = DB::selectOne($sql);
        
        return  isset($result->count_1)?$result->count_1:0;
    }

    /**
     * 活动数据添加
     */
    public static function activeDataInsert($para){

        return DB::table('active')->insertGetId($para);
    }

    /**
     * 活动商品数据添加
     */
    public static function activeProductDataInsert($para){

        return DB::table('active_goods')->insertGetId($para);
    }

    /**
     * 活动商品数据修改
     */
    public static function updateActive($para){

        return DB::table('active')->where('id',$para['id'])->update($para);
    }
    /**
     * 获取活动详情
     */
    public static function getActiveInfo($para){

        $sql = "select * from sline_community_active where is_delete=0 and  id=".$para['id'];

        $result = DB::selectOne($sql);

        return  json_decode(json_encode($result),true);
    }

    public static function getActiveProductList($id){

        $sql = "select * from sline_community_active_goods where is_delete=0 and  active_id=".$id." order by sort desc";

        $result = DB::select($sql);

        return  json_decode(json_encode($result),true);

    }
    
    public static function getActiveProductInfo($param,$id){

        $sql = "select * from sline_community_active_goods where active_id=".$id." and good_id=".$param['goods_id'];

        $result = DB::selectOne($sql);

        return  json_decode(json_encode($result),true);

    }    

    public static function activeProductDataUpdate($para){

        return DB::table('active_goods')->where('id',$para['id'])->update($para);

    }

    public static function updateActiveGoods($para){

        return DB::table('active_goods')->where('active_id',$para['active_id'])->update($para);

    }
}