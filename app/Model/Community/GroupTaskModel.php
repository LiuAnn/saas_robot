<?php

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GroupTaskModel extends Model
{
    /**
     * 批量添加数据
     * @param $data
     * @return mixed
     */
    public static function insertData($data)
    {
        return self::insert($data);
    }

    /**
     * 添加团长任务数据
     * @param $data
     * @return mixed
     */
    public static function insertTaskData($data)
    {
        return DB::table('group_task')->insertGetId($data);
    }

    /**
     * 编辑团长任务数据
     * @param $data
     * @return mixed
     */
    public static function updateTaskData($data)
    {
        return DB::table('group_task')->where('id',$data['id'])->update($data);
    }


    /**
     * 添加团长任务商品数据
     * @param $data
     * @return mixed
     */
    public static function insertGoodData($data)
    {
        return DB::table('group_task_goods')->insert($data);
    }

    /**
     * 编辑团长任务商品数据
     * @param $data
     * @return mixed
     */
    public static function updateGoodData($data)
    {   
        return DB::table('group_task_goods')->where('id',$data['id'])->update($data);
    }


    /**
     * 编辑团长任务商品数据
     * @param $data
     * @return mixed
     */
    public static function updateGoodDataForTaskId($data)
    {   
        return DB::table('group_task_goods')->where('task_id',$data['task_id'])->update($data);
    }


    /**
     * 获取团长任务单条商品数据
     * @param $data
     * @return mixed
     */
    public static function getGoodDataForTaskId($where)
    {   
        $where['is_del'] = 0;
        $result = DB::table('group_task_goods')->where($where)->first();
        return json_decode(json_encode($result),true);
    }

    /**
     * 获取任务详情
     * @param $data
     * @return mixed
     */
    public static function getTaskInfo($where=[])
    {   
        $where['is_del'] = 0;
        return DB::table('group_task')->where($where)->first();
    }


    /**
     * 获取任务商品详情
     * @param $data
     * @return mixed
     */
    public static function getTaskGoodsInfo($where=[])
    {   
        $where['is_del'] = 0;
        $result =  DB::table('group_task_goods')->where($where)->get();

        return json_decode(json_encode($result),true);
    }


    /**
     * 获取任务列表
     * @param $data
     * @return mixed
     */
    public static function getTaskList($param)  
    {   


        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $where = "";

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .= " and platform_identity = '".$param['platform_identity']."'";
        }

        if(isset($param['beginTime'])&&!empty($param['beginTime'])){

            $where .= " and begin_time>".strtotime($param['beginTime']);
        }
        if(isset($param['examine'])&&!empty($param['examine'])){

            $where .= " and examine=".$param['examine'];
        }
        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .= " and end_time<".strtotime($param['endTime']);
        }
        if(isset($param['bonusMoney'])&&!empty($param['bonusMoney'])){

            $where .= " and bonus_money=".$param['bonusMoney']*100;
        }
        if(isset($param['taskStatus'])&&!empty($param['taskStatus'])){

            $where .= " and task_status=".$param['taskStatus'];
        }
        if(isset($param['updatetaskStatus'])&&!empty($param['updatetaskStatus'])){

            $where .= " and task_status>0";
        }
        $sql = "select * from sline_community_group_task where is_del=0 {$where} limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }


    /**
     * 获取任务列表总条数
     * @param $data
     * @return mixed
     */
    public static function getTaskCount($param)  
    {  

        $where = "";

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .= " and platform_identity = '".$param['platform_identity']."'";
        }

        if(isset($param['beginTime'])&&!empty($param['beginTime'])){

            $where .= " and begin_time>".strtotime($param['beginTime']);
        }
        if(isset($param['examine'])&&!empty($param['examine'])){

            $where .= " and examine=".$param['examine'];
        }
        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .= " and end_time<".strtotime($param['endTime']);
        }

        if(isset($param['bonusMoney'])&&!empty($param['bonusMoney'])){

            $where .= " and bonus_money=".$param['bonusMoney']*100;
        }
        if(isset($param['taskStatus'])&&!empty($param['taskStatus'])){

            $where .= " and task_status=".$param['taskStatus'];
        }
        if(isset($param['updatetaskStatus'])&&!empty($param['updatetaskStatus'])){

            $where .= " and task_status>0";
        }
        $sql = "select count(*) as count_1 from sline_community_group_task where is_del=0  {$where}";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;

    }

    /**
     * 获取团长任务完成度
     * @param $data
     * @return mixed
     */
    public static function groupTaskRelation($where=[])
    {      
        $where['is_del'] = 0;
        $result =  DB::table('group_task_relation')->where($where)->first();
        return json_decode(json_encode($result),true);
    }


    public static function getGroupTaskList($param){

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $where = "";
        if(isset($param['taskId'])&&!empty($param['taskId'])){

            $where .= " and task_id=".$param['taskId']; 
        }

        $sql = "select * from sline_community_group_task_relation where is_del=0 {$where} limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }



    public static function getGroupTaskListV1($param){


        $where = "";

        if(isset($param['taskId'])&&!empty($param['taskId'])){

            $where .= " and task_id=".$param['taskId']; 
        }

        $sql = "select * from sline_community_group_task_relation where is_del=0 {$where}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }


    public static function getGoodsListByGoodsId($goodsId,$platform_identity){

        $sql = " select id,begin_time,end_time from sline_community_group_task where id in (select task_id from sline_community_group_task_goods where good_id = {$goodsId} and is_del=0) and is_del=0 and platform_identity='{$platform_identity}'";
    
        $result = DB::select($sql);

        return json_decode(json_encode($result),true);
    }

    public static function getProductIdsbyTaskId($taskId){

        $where['task_id'] = $taskId;
        $where['is_del'] = 0;

        $result = DB::table('group_task_goods')->where($where)->pluck('good_id');

        return json_decode(json_encode($result),true);
    }

    /**
     * 获取任务列表总条数
     * @param $data
     * @return mixed
     */
    public static function getGroupTaskCount($param)  
    {  

        $where = "";

        if(isset($param['taskId'])&&!empty($param['taskId'])){

            $where .= " and task_id=".$param['taskId']; 
        }

        $sql = "select count(*) as count_1 from sline_community_group_task_relation where is_del=0 {$where}";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;

    }

    /**
     * 获取团长任务商品完成详情
     * @param $data
     * @return mixed
     */
    public static function getGroupTaskGoodsInfo($where=[])
    {   
        $where['is_del'] = 0;
        $result =  DB::table('group_task_goods_relation')->where($where)->get();

        return json_decode(json_encode($result),true);
    }


    /**
     * 获取团长任务商品完成详情
     * @param $data
     * @return mixed
     */
    public static function getGroupTaskGoodsInfoByGoodsId($where=[])
    {   
        $where['is_del'] = 0;
        $result =  DB::table('group_task_goods_relation')->where($where)->first();

        return json_decode(json_encode($result),true);
    }


    /**
     * 获取任务列表
     * @param $data
     * @return mixed
     */
    public static function getTaskListForMid($param)  
    {   


        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $where = "";

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .= " and t.platform_identity = '".$param['platform_identity']."'";
        }

        if(isset($param['examine'])&&!empty($param['examine'])){

            $where .= " and t.examine=".$param['examine'];
        }

        if(isset($param['taskStatus'])&&!empty($param['taskStatus'])){

            $where .= " and t.task_status=".$param['taskStatus'];
        }


        if(isset($param['updatetaskStatus'])&&!empty($param['updatetaskStatus'])){

            $where .= " and t.task_status>0";
        }

        $sql = "select * from sline_community_group_task where is_del=0 {$where} limit {$page},{$pageSize}";

        $sql = "select t.*,r.task_status as user_task_status from sline_community_group_task t ";
        $sql .= " left join sline_community_group_task_relation r on r.task_id=t.id";
        $sql .= " where t.is_del=0 {$where} group by t.id order by t.task_status,r.task_status desc  limit {$page},{$pageSize}";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);

    }


    /**
     * 添加团长任务进度数据
     * @param $data
     * @return mixed
     */
    public static function insertTaskRelationData($data)
    {
        return DB::table('group_task_relation')->insertGetId($data);
    }


    /**
     * 修改长任务进度数据
     * @param $data
     * @return mixed
     */
    public static function updateTaskRelationInfo($where=[])
    {   
        $where['is_del'] = 0;
        return DB::table('group_task_relation')->where(['id'=>$where['id']])->update($where);
    }

    /**
     * 添加团长任务商品进度数据
     * @param $data
     * @return mixed
     */
    public static function insertGoodsTaskRelationData($data)
    {
        return DB::table('group_task_goods_relation')->insertGetId($data);
    }


    /**
     * 修改团长任务商品完成详情
     * @param $data
     * @return mixed
     */
    public static function updateGroupTaskGoodsInfo($where=[])
    {   
        $where['is_del'] = 0;
        return DB::table('group_task_goods_relation')->where(['id'=>$where['id']])->update($where);
    }


    public static function updateGroupTaskStatus($platform_identity,$task_status=0){

        $now_time = time();
        $now_status = $task_status;
        $update_status = $task_status+1;
        if($update_status==1){
            $where  = " and begin_time<{$now_time}";
        }else{
            $where  = " and end_time<{$now_time}";
        }

        if(isset($platform_identity)&&!empty($platform_identity)){

            $where .= " and platform_identity = '".$platform_identity."'";
        }

        $where = [];
        $where['is_del'] = 0;
        $where['examine'] = 1;
        $where['task_status'] = $now_status;

        if(isset($platform_identity)&&!empty($platform_identity)){

            $where['platform_identity']= $platform_identity;
        }



        $sql = DB::table('group_task')->where($where);

        if($update_status==1){

            $sql = $sql->where("begin_time","<",$now_time);

        }else{

            $sql = $sql->where("end_time","<",$now_time);
        }

        $update['task_status'] = $update_status;
        $update['updated_time'] = $now_time;

        return $sql->update($update);

    }

}
