<?php

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AutoGroupCreateModel extends Model
{
    CONST GROUP_LIST_KEY = 'auto_group_create';
    CONST INVITE_FRIEND_KEY = 'invite_friend_group';
    CONST LEAVE_GROUP_KEY = 'leave_group_key';
    CONST UPDATE_GROUP_NAME_KEY = 'update_group_name';

    protected $table = 'auto_group_create';


    /**
     * 批量添加数据
     * @param $data
     * @return mixed
     */
    public static function insertData($data)
    {
        return self::insertGetId($data);
    }


    public static function getGroupInfoByWhere($where)
    {
        $query = self::where($where)->first();
        return $query ? $query->toArray() : [];
    }


    public static function getGroupInfoNotNull()
    {
        $query = self::where(['robot_send_status' => 0])->where('member_count', '>', '1')->first();
        return $query ? $query->toArray() : [];
    }

    public static function getMemberGmvByIds($memberIds)
    {
        $sql = "select TRUNCATE(sum(actual_price)/100, 2) GMV, count(1) cnt";
        //$sql = "select sum(actual_price) GMV";
        $sql .= " from  ".config('community.oldMallOrderGoods')." ";
        $sql .= " where member_id in ($memberIds) and pay_status = 1";
        $result = DB::selectOne($sql);
        return $result;
    }

    public static function getMemberIdsByWXID($wxId)
    {
        return DB::table('robot_friend')->whereIn('wxid', $wxId)->pluck('mid')->toArray();
    }

    public static function getToCreateGroupNum()
    {
        //机器人未创建
        $count = self::where(['robot_send_status' => 0])->where('member_count', '>', '1')->count();
        return $count ? $count : 0;
    }
}
