<?php

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;

class FriendsIntoGroupModel extends Model
{
    protected $table = 'friends_into_group';


    /**
     * 批量添加数据
     * @param $data
     * @return mixed
     */
    public static function insertData($data)
    {
        return self::insert($data);
    }
}
