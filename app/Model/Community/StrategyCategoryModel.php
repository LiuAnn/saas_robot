<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/8
 * Time: 18:56
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StrategyCategoryModel extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'strategy_category';

    protected $primaryKey = 'id';
    protected $guarded = [];

    public static function ylhDb()
    {
        return DB::connection('mysql');
    }

    public static function getStrategyCategoryList($data=[])
    {
        $where = self::getStrategyCategoryListWhere($data);

        if (!empty($data['limit']) && $data['limit']=='all')
        {
            $sql    = "select * from sline_community_strategy_category WHERE deleted_at=0 {$where} ";
            $result = self::ylhDb()->select($sql);
        }else{
            $sql    = "select * from sline_community_strategy_category WHERE deleted_at=0 {$where} order by id desc limit ?, ?";
            $result = self::ylhDb()->select($sql, [$data["offset"], $data["display"]]);
        }

        return $result;
    }

    public static function getStrategyCategoryListWhere($param = [])
    {
        $where='';
        if (isset($param['name']) && !empty($param['name'])) {
            $where = "AND `name` like '%" . $param['name'] . "%' ";
        }
        if (isset($param['platform_identity']) && !empty($param['platform_identity'])) {
            $where .= "AND `platform_identity` = '" . $param['platform_identity'] . "'";
        }
        return $where;
    }

    public static function getStrategyCategoryListCount($param)
    {
        $where  = self::getStrategyCategoryListWhere($param);
        $sql    = "select count(*) count from sline_community_strategy_category WHERE deleted_at=0 {$where}";
        $result = self::ylhDb()->selectOne($sql);
        return !empty($result) ? $result->count : 0;
    }
}