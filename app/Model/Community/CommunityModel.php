<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/27
 * Time: 20:30
 */

namespace App\Model\Community;
use App\Service\Admin\AdminIndexService;
use App\Service\Member\MemberService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class CommunityModel  extends Model
{
    //指定表名
    protected $table = 'data_info';
    //指定主键字段
    protected $primaryKey = 'id';
    //关闭 Laravel 自动管理列
    public $timestamps = false;

    //设置和名单，为空则表明任何字段都可插入数据库
    protected  $guarded = [];
    /**
     * 新增记录
     * @param $data
     * @return int
     */
    public static function insertFileIndesData($data) {
        return DB::table('community_data_info')->insertGetId([
            'action'       => $data['action'],//'操作名称'
            'appid'      => $data['appid'],//'appid'
            'client_session'        => $data['client_session'],//'client_session'
            'file_index'    => $data['file_index'],//file_index
            'msg_id'        => $data['msg_id'],//msg_id
            'msg_type'         => $data['msg_type'],//msg_type
            'room_wxid'       => $data['room_wxid'],//room_wxid
            'wxid_from'       => $data['wxid_from'],//wxid_from
            'wxid_to'       => $data['wxid_to'],//wxid_to
            'created_at'     => date('Y-m-d H:i:s',time()),
        ]);
    }
    /**
     * 修改记录  上传记录
     * @param $data
     * @return int
     */
    public static function updateOrder($data)
    {
        return DB::table('data_info')->where('file_index', $data['file_index'])->update($data);
    }
    /**
     * 获取文件的路径
     * 用来判断文件是否被上传过
     */
    public static function getOrderListByMemberId($file_index)
    {
        $sql = 'select  path_info  from sline_community_data_info where  file_index = ?  ';
        $result = DB::select($sql, [$file_index]);
        return !empty($result) ? $result[0] : [];
    }
    /**
     * 文件是否存在
     * @param $body
     * @return array|int
     */
    public static function fileIsSetServer($body)
    {
        $data = self::getOrderListByMemberId($body['data']['msg']['file_index']);
        if (empty($data)) {
            $insertData = [
                'action' => $body['action'],//'操作名称'
                'appid' => $body['appid'],//'appid'
                'client_session' => $body['client_session'],//'client_session'
                'file_index' => $body['data']['msg']['file_index'],//file_index
                'msg_id' => $body['data']['msg']['msg_id'],//msg_id
                'msg_type' => $body['data']['msg']['msg_type'],//msg_type
                'room_wxid' => $body['data']['msg']['room_wxid'],//room_wxid
                'wxid_from' => $body['data']['msg']['wxid_from'],//wxid_from
                'wxid_to' => $body['data']['msg']['wxid_to'],//wxid_to
                'wxid' => $body['wxid'],//wxid_to
            ];
            //文件记入记录
            $data = self::insertFileIndesData($insertData);
        }
        return $data;
    }
    /**
     * 数据入库
     * @param $table
     * @param $data
     * @return bool
     */
    public static function insertTableData($table,$data) {
        return DB::table($table)->insert($data);
    }
    /**
     * 数据入库 返回值id
     * @param $table
     * @param $data
     * @return int
     */
    public static function insertGetId($table,$data) {

        return DB::table($table)->insertGetId($data);
    }
    /**
     * id
     */
    public static function getRoomMemberId($appid,$room_wxid)
    {
        $sql = 'select  id  from sline_community_member_info_by_appid where room_wxid= ? ';
        $result = DB::select($sql, [$room_wxid]);
        return !empty($result) ? $result[0]->id: [];
    }

    /**
     * id
     */
    public static function getRoomMemberMId($appid,$room_wxid)
    {
        $sql = 'select  mid,id,is_delete  from sline_community_member_info_by_appid where room_wxid= ? ';
        $result = DB::select($sql, [$room_wxid]);
        return !empty($result) ? $result[0]: [];
    }
    /**
     * 更新群的消息
     * @param $data
     * @return int
     */
    public static function updateMemberTableData($data){
        return DB::table('member_info_by_appid')->where(['id'=> $data['id'],'wxid'=>$data['wxid']])->update($data);
    }

    public static function updateWxidMemberTableData($data){
        return DB::table('member_info_by_appid')->where(['id'=> $data['id']])->update($data);
    }
    /**
     * 更新群的消息
     * @param $id, $update
     * @return int
     */
    public static function updateAutoCreateGroupData($id, $update){
        return DB::table('auto_group_create')->where(['id' => $id])->update($update);
    }


    /**
     * 微信用户信息存储
     * @param $id
     * @param $table
     * @param $data
     */
    public  static function insertWxUserInfo($id,$room_wxid,$table, $data)
    {
        $arr['romm_id'] =$id;
        $arr['room_wxid'] =$room_wxid;
        array_walk($data, function (&$value, $key, $arr) {
            $value = array_merge($value, $arr);
        }, $arr);
        return DB::table($table)->insert($data);
    }

    /**
     * 微信用户信息更新
     * @param $id
     * @param $table
     * @param $data
     */
//    public  static function updateWxUserInfo($isId,$id,$room_wxid,$table,$data)
//    {
        // $arr['romm_id'] =$id;
        // $arr['room_wxid'] =$room_wxid;
        // array_walk($data, function (&$value, $key, $arr) {
        //     $value = array_merge($value, $arr);
        // }, $arr);

        // foreach ($isId as $k => $v) {
        //     foreach ($arr as $keys => $vi) {
        //         if($vi['wxid']==$v->wxid)
        //         {
        //             DB::table($table)->where('id',$v->id)->update($vi);
        //         }
        //     }
        // }
//    }
    /**
     * 获取微信群是否存在该微信用户
     * @param $id
     * @param $table
     * @param $data
     */
    public  static function  getRoomWxUser($wxid = '', $room_wxid = '', $isSetId = 0)
    {
        $sql = 'select id from sline_community_member_weixin_info where wxid=? and room_wxid=? and romm_id=? ';
        $result = DB::select($sql, [$wxid, $room_wxid, $isSetId]);
        return !empty($result) ? $result[0]->id : [];
    }

    public static function updateWxUserInfo($mwxUserId = 0 , $upUser = []){
        return DB::table('member_weixin_info')->where('id', $mwxUserId)->update($upUser);
    }
    /**
     * 获取的群成员信息入库
     * @param $action
     * @param $appid
     * @param $wxid
     * @param $data
     * @return int|mixed
     * @throws \Throwable
     */
    public static function insertRoomMemberInfo($action, $appid, $wxid, $data)
    {

        $insertData = [];
        foreach ($data['room_data_list'] as $key => $rooInfo) {
            DB::transaction(function () use ($action, $appid, $wxid, $rooInfo) {
                $id=0;
                $isSetId = self::getRoomMemberId($appid, $rooInfo['room_wxid']);
                if (!empty($isSetId)&&!empty($rooInfo['head_img'])&&!empty($rooInfo['name'])) {
                    $updatedData=[];
                    $updatedData['id'] = $isSetId;
                    $updatedData['action'] = $action;
                    $updatedData['appid'] = $appid;
                    $updatedData['wxid'] = $wxid;
                    $updatedData['room_wxid'] = $rooInfo['room_wxid'];
                    $updatedData['name'] = $rooInfo['name'];
                    $updatedData['owner_wxid'] = $rooInfo['owner_wxid'];
                    $updatedData['head_img'] = $rooInfo['head_img'];
                    $updatedData['is_delete'] = 0;
                    $updatedData['member_count'] = $rooInfo['member_count'];
                    $updatedData['memberInfo_list'] = json_encode($rooInfo['memberInfo_list']);
                    $updatedData['updated_at'] = date('Y-m-d H:i:s', time());

                    $id = self::updateMemberTableData($updatedData);
                    //判断 群room_wxid  是否
                    $allRoomWxUserList = self:: getRoomWxAllUser($isSetId, $rooInfo['room_wxid']);
                    if(empty($allRoomWxUserList)){
                        self::insertWxUserInfo($isSetId, $rooInfo['room_wxid'], 'member_weixin_info', $rooInfo['memberInfo_list']);
                    }else{
                        foreach ($rooInfo['memberInfo_list'] as $key => $value) {
                            $wxUserInfoId = self::getRoomWxUser($value['wxid'], $rooInfo['room_wxid'], $isSetId);
                            if(empty($wxUserInfoId)){
                                $insertUser = [];
                                $insertUser['romm_id'] = $isSetId;
                                $insertUser['room_wxid'] = $rooInfo['room_wxid'];
                                $insertUser['wxid'] = $value['wxid'];
                                $insertUser['wx_alias'] = $value['wx_alias'];
                                $insertUser['room_nickname'] = $value['room_nickname'];
                                $insertUser['nickname'] = $value['nickname'];
                                $insertUser['head_img'] = $value['head_img'];
                                $insertUser['sex'] = $value['sex'];
                                $insertUser['country'] = $value['country'];
                                $insertUser['province'] = $value['province'];
                                $insertUser['city'] = $value['city'];
                                $id = self::insertGetId('member_weixin_info', $insertUser);
                                continue;
                            }
                            $upUser = [];
                            $upUser['wx_alias'] = $value['wx_alias'];
                            $upUser['room_nickname'] = $value['room_nickname'];
                            $upUser['nickname'] = $value['nickname'];
                            $upUser['head_img'] = $value['head_img'];
                            $upUser['sex'] = $value['sex'];
                            $upUser['country'] = $value['country'];
                            $upUser['province'] = $value['province'];
                            $upUser['is_delete'] = 0;
                            $upUser['city'] = $value['city'];
                            self::updateWxUserInfo($wxUserInfoId, $upUser);
                        }
                        
                    }
                } else if(!empty($rooInfo['head_img'])&&!empty($rooInfo['name'])){
                    $insertInfo=[];
                    $insertInfo['action'] = $action;
                    $insertInfo['appid'] = $appid;
                    $insertInfo['wxid'] = $wxid;
                    $insertInfo['room_wxid'] = $rooInfo['room_wxid'];
                    $insertInfo['name'] = $rooInfo['name'];
                    $insertInfo['owner_wxid'] = $rooInfo['owner_wxid'];
                    $insertInfo['head_img'] = $rooInfo['head_img'];
                    $insertInfo['member_count'] = $rooInfo['member_count'];
                    $insertInfo['memberInfo_list'] = json_encode($rooInfo['memberInfo_list']);
                    $insertInfo['created_at'] = date('Y-m-d H:i:s', time());

                    $id = self::insertGetId('member_info_by_appid', $insertInfo);
                    if ($id > 0) {
                        self::bindingRoomId($rooInfo['name'], $id);
                        self::insertWxUserInfo($id, $rooInfo['room_wxid'], 'member_weixin_info', $rooInfo['memberInfo_list']);
                    }
                }
                $insertData[] = $id;
            });
        }
        if (count($data['room_data_list']) == count($insertData)) {
            return true;
        } else {
            return false;
        }

    }
    /**
     * 更新用户登录表
     * @param string $str
     * @param $id
     * @return string
     */
    public static   function bindingRoomId($str='',$id){
        $str=trim($str);
        if(empty($str)){return '';}
        $reg='/(\d{8}(\.\d+)?)/is';//匹配数字的正则表达式
        preg_match_all($reg,$str,$result);
        if(is_array($result)&&!empty($result)&&!empty($result[1])&&!empty($result[1][0])){
            //根据群编号 获取 mid
            $midData =  self::getActivationNumMidNew($result[0][0]); //获取群编号mid
            Log::info('userBehavior mid id app ' .json_encode([$id,$str,$result[0][0],$midData]));
            if(!empty($midData)){
                $update_data = ['id'=>$id,'mid'=>$midData->mid,'bind_mid_time' => date('Y-m-d H:i:s', time())];
                if($midData->tag_id>0){
                    $update_data['tag_id']=$midData->tag_id;
                }

                if($midData->platform_identity=='7323ff1a23f0bf1cda41f690d4089353'&&$midData->mid>0){
                    $yuetaoData =   self::getMemberInfo($midData->mid);
                    $update_data['code_number']=$yuetaoData->code_number;
                    $update_data['mobile']=$yuetaoData->mobile;
                }else if($midData->platform_identity=='89dafaaf53c4eb68337ad79103b36aff'&&$midData->mid>0){
                    $daren =AdminIndexService::getDarenMidData($midData->mid);
                    $update_data['code_number'] =!isset($daren['codeNumber'])?'':$daren['codeNumber'];
                    $update_data['mobile'] =!isset($daren['mobile'])?'':$daren['mobile'];
                }

                $isSetMid = self::isSetMid($id);

                if($isSetMid!=$midData->mid){

                    //MemberService::sendIsRuleGroup($id,8);
                    
                    self::upByAppIdMid($update_data); //更新群对应得mid
                    ////更新群主信息开始
                    if($midData->platform_identity=='7323ff1a23f0bf1cda41f690d4089353'&&$midData->mid>0){
                        //MemberService::sendIsRuleGroup($id,2);
                        MemberService::newSendIsRuleGroup($id);
                    }
                    ///更新群主信息结束

                }

            }
            self::upGroupInfoByGroupNumber(['group_number'=>$result[0][0],'room_id'=>$id]);
        }
        return '';
    }


    /**
     * 是否存在mid
     * @param string $str
     * @param $id
     * @return string
     */
    public static   function isSetMid($id){

        $sql = 'select mid from  sline_community_member_info_by_appid where id=?  ';
        $result = DB::select($sql, [$id]);
        return !empty($result) ? $result[0]->mid : 0;        

    }


        /**
         * 根据手机号获去详细信息
         * @param array $condition
         * @return mixed
         */
        public static function getMemberInfo($mid =67) {
            $sql = "select    sm.mobile,sm.truename,sm.nickname,sic.code_number,sm.mid  from   yuelvhui.sline_member   as sm  
                   left join  yuelvhui.sline_invitation_code   as sic   on sm.mid=sic.mid
                   where sm.mid={$mid} ";
            return DB::connection('robot')->selectOne($sql);
        }

    // 判断的群号是否存在
    public static function  isSetBuyNumber($str='')
    {
        $str=trim($str);
        if(empty($str)){return '';}
        $reg='/(\d{8}(\.\d+)?)/is';//匹配数字的正则表达式
        preg_match_all($reg,$str,$result);
        $room_id = [];
        if(is_array($result)&&!empty($result)&&!empty($result[1])&&!empty($result[1][0])){
            //根据群编号 获取 mid
            $room_id =  self::getActivationNumMid($result[0][0]); //获取群编号mid
        }
        return $room_id;
    }




    /**
     * 通过群编号更信息
     * @param $number
     * @return mixed
     */
    public static function upGroupInfoByGroupNumber($data)
    {
        return DB::table('invitation_group')->where('group_number', $data['group_number'])->update($data);
    }

    /**
     * @param $mid
     * @param $staus
     * @return array
     */
    public static function getActivationNumMid($group_number)
    {
        $sql = 'select mid,room_id  from  sline_community_invitation_group   where group_number=?  ';
        $result = DB::select($sql, [$group_number]);
        return !empty($result) ? $result[0]->room_id : [];
    }


    public static function getActivationNumMidNew($group_number)
    {
        $sql = 'select mid,room_id,tag_id,platform_identity  from  sline_community_invitation_group   where group_number=? and  examine_status=2';
        $result = DB::select($sql, [$group_number]);
        return !empty($result) ? $result[0] : [];
    }
    /**
     * 通过群编号更信息
     * @param $number
     * @return mixed
     */
    public static function upByAppIdMid($data)
    {
        return DB::table('member_info_by_appid')->where('id', $data['id'])->update($data);
    }
    /**
     * 用户登陆wehup记录
     * @param $table
     * @param $app
     * @param $wxid
     * @param $data
     */
    public static function insertMemberLoginInfo($table,$app, $wxid, $data,$type=1)
    {
        $insertData['appid']= $app;
        $insertData['wxid']= $wxid;
        $insertData['machine_id']= $data['machine_id'];
        $insertData['client_version']= $data['client_version'];
        if($type==1){
            $insertData['province']= empty($data['province'])?'':$data['province'];
            $insertData['city']=  empty($data['city'])?'':$data['city'];
            $insertData['head_img']= $data['head_img'];
            $insertData['nickname']= $data['nickname'];
            $insertData['nonce']= $data['nonce'];
            $insertData['wx_alias']= $data['wx_alias'];
            $insertData['sex']= $data['sex'];
            $insertData['type']= 1;
        }else{
            $insertData['type']= 2;
        }
        if(!empty($insertData)){
            return    self::insertGetId($table,$insertData);
        }
    }

    /**
     * report_contact(上报当前好友列表和群列表)  目前处理群的
     * @param $action
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public  static function  insertReportContactRoomMemberInfo($action, $appid, $wxid, $data)
    {

        $insertData = [];
        if(!empty($data['group_list'])&&isset($data['group_list'])){

            $robotInfo = self::getWxidBindMid($wxid);
            $tag_id = 0;
            if(!empty($robotInfo)&&isset($robotInfo->wxid_type)&&$robotInfo->wxid_type==3){

                $tag_id = 11;

            }

            foreach ($data['group_list'] as $key => $rooInfo) {

                DB::transaction(function () use ($action, $appid, $wxid, $data, $rooInfo ,$tag_id) {
                    //当群成员完全

                    if (count(explode('@', $rooInfo['room_wxid'])) == 2) {
                        $isSetId = self::getRoomMemberMId($appid, $rooInfo['room_wxid']);
                        if (!empty($isSetId)&&!empty($rooInfo['name'])) {

                            Log::info('getreport_contactInfo    report_contact update new_room'.$wxid.$rooInfo['name']);

                            $updatedData=[];
                            $updatedData['id'] = $isSetId->id;
                            $updatedData['action'] = $action;
                            $updatedData['appid'] = $appid;
                            $updatedData['wxid'] = $wxid;
                            $updatedData['room_wxid'] = $rooInfo['room_wxid'];
                            $updatedData['name'] = $rooInfo['name'];
                            $updatedData['owner_wxid'] = $rooInfo['owner_wxid'];
                            $updatedData['head_img'] = $rooInfo['head_img'];
                            $updatedData['member_count'] = $rooInfo['member_count'];
                            $updatedData['memberInfo_list'] = json_encode($rooInfo['member_wxid_list']);
                            $updatedData['is_delete'] = 0;
                            $updatedData['updated_at'] = date('Y-m-d H:i:s', time());
                            $res = self::updateWxidMemberTableData($updatedData);
                            if ($res&&!empty($rooInfo['name'])&&empty($isSetId->mid)) {
                                self::bindingRoomId($rooInfo['name'], $isSetId->id);
                            }
                        } else {

                            if(!empty($rooInfo['name'])){

                                Log::info('getreport_contactInfo    report_contact add new_room'.$wxid.$rooInfo['name']);

                                $insertInfo=[];
                                $insertInfo['action'] = $action;
                                $insertInfo['appid'] = $appid;
                                $insertInfo['wxid'] = $wxid;
                                $insertInfo['room_wxid'] = $rooInfo['room_wxid'];
                                $insertInfo['name'] = $rooInfo['name'];
                                $insertInfo['tag_id'] = $tag_id;
                                $insertInfo['owner_wxid'] = $rooInfo['owner_wxid'];
                                $insertInfo['head_img'] = $rooInfo['head_img'];
                                $insertInfo['member_count'] = $rooInfo['member_count'];
                                $insertInfo['memberInfo_list'] = json_encode($rooInfo['member_wxid_list']);
                                $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
                                $id = self::insertGetId('member_info_by_appid', $insertInfo);
                                if($rooInfo['member_count']>=360){
                                    self::addVipGroupLog($id,$wxid,1);
                                }
                                if ($id > 0) {
                                    self::bindingRoomId($rooInfo['name'], $id);
                                }
                            }
                        }
                        $returnData[] = $rooInfo['room_wxid'];
                    }
                });
            }
        }
//        if (!empty($data['group_list']) && isset($data['group_list'])) {
//            foreach ($data['group_list'] as $key => $rooInfo) {
//                $insertInfo = [];
//                $insertInfo['action'] = $action;
//                $insertInfo['appid'] = $appid;
//                $insertInfo['wxid'] = $wxid;
//                $insertInfo['room_wxid'] = $rooInfo['room_wxid'];
//                $insertInfo['name'] = $rooInfo['name'];
//                $insertInfo['owner_wxid'] = $rooInfo['owner_wxid'];
//                $insertInfo['head_img'] = $rooInfo['head_img'];
//                $insertInfo['member_count'] = $rooInfo['member_count'];
//                $insertInfo['memberInfo_list'] = json_encode($rooInfo['member_wxid_list']);
//                $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
//                self::insertGetId('member_info_by_appid_new', $insertInfo);
//            }
//        }


        //机器人好友数据
        if(isset($data['friend_list'])&&!empty($data['friend_list'])){
            $platform_identity = self:: getPlatformIdentity($wxid);
            foreach($data['friend_list'] as $userInfo){
                //判断当前机器人的好友列表是否包含 当前好友
                $isFriendData = self::getRobotFriend($userInfo['wxid'],$wxid);
                if(empty($isFriendData)){
                    $insertfriendData = [
                        'robot_wxid'=>$wxid,
                        'wxid'=>$userInfo['wxid'],
                        'platform_identity'=>$platform_identity,
                        'wx_alias'=>$userInfo['wx_alias'],
                        'nickname'=>$userInfo['nickname'],
                        'head_img'=>$userInfo['head_img'],
                        'sex'=>$userInfo['sex'],
                        'country'=>$userInfo['country'],
                        'province'=>$userInfo['province'],
                        'city'=>$userInfo['city'],
                    ];
                    self::insertTableData('robot_friend',$insertfriendData);
                }else{
                    $upfriendData = [
                        'id'=>$isFriendData,
                        'robot_wxid'=>$wxid,
                        'wxid'=>$userInfo['wxid'],
                        'platform_identity'=>$platform_identity,
                        'wx_alias'=>$userInfo['wx_alias'],
                        'nickname'=>$userInfo['nickname'],
                        'head_img'=>$userInfo['head_img'],
                        'sex'=>$userInfo['sex'],
                        'country'=>$userInfo['country'],
                        'province'=>$userInfo['province'],
                        'city'=>$userInfo['city'],
                    ];
                    self::upRobotFriend('robot_friend',$upfriendData);
                }
            }
        }
        return true;

    }
    //更新好友列表
    public static function upRobotFriend($table,$data)
    {
        return DB::table($table)->where('id', $data['id'])->update($data);
    }
    //查看好友记录是否存在
    public static function getRobotFriend($wxid,$robot_wxid)
    {
        $sql = 'select  id  from sline_community_robot_friend  where  wxid = ? and robot_wxid= ? ';
        $result = DB::select($sql, [$wxid,$robot_wxid]);
        return !empty($result) ? $result[0]->id: [];
    }
    //平台标识
    public  static  function getPlatformIdentity($wxid){
        $sql = 'select  scu.platform_identity   from sline_community_white_list_wx as scw 
               left join  sline_community_user as scu on scw.mid=scu.mid    where  wxid = ?  ';
        $result = DB::selectOne($sql, [$wxid]);
        return !empty($result) ? $result->platform_identity: '';
    }
    /**
     * 上报成员信息变化
     * @param $action
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function reportContactUpdateData($action, $appid, $wxid, $data)
    {
        $insertData = [];
        foreach ($data['update_list'] as $key => $rooInfo) {
            DB::transaction(function () use ($action, $appid, $wxid, $data, $rooInfo) {
                $id = 0;
                if (strpos($rooInfo['wxid'], '@chatroom')) {
                    //更新群的信息
                    $isSetId = self::getRoomMemberId($appid, $rooInfo['wxid']);
                    if (!empty($isSetId)&&!empty($rooInfo['name'])) {
                        $updatedData=[];
                        $updatedData['id'] = $isSetId;
                        $updatedData['action'] = $action;
                        $updatedData['appid'] = $appid;
                        $updatedData['wxid'] = $wxid;
                        $updatedData['room_wxid'] = $rooInfo['wxid'];
                        $updatedData['name'] = $rooInfo['name'];
                        $updatedData['is_delete'] = 0;
                        $updatedData['head_img'] = $rooInfo['head_img'];
                        if (array_key_exists('owner_wxid', $rooInfo)) {
                            $updatedData['owner_wxid'] = $rooInfo['owner_wxid'];
                            $updatedData['member_count'] = $rooInfo['member_count'];
                            MemberService::newSendIsRuleGroup($isSetId);
                            // if($updatedData['member_count']>=80){

                            //     MemberService::sendIsRuleGroup($isSetId,7);

                            // }else{

                            //     MemberService::sendIsRuleGroup($isSetId,8);
                            // }
                        }
                        $updatedData['updated_at'] = date('Y-m-d H:i:s', time());
                        $id = self::updateMemberTableData($updatedData);
                        if ($id&&$isSetId>0) {
                            self::bindingRoomId($rooInfo['name'], $isSetId);
                        }
                    } else {
                        if(!empty($rooInfo['name'])){
                            $insertInfo=[];
                            $insertInfo['action'] = $action;
                            $insertInfo['appid'] = $appid;
                            $insertInfo['wxid'] = $wxid;
                            $insertInfo['room_wxid'] = $rooInfo['wxid'];
                            $insertInfo['name'] = $rooInfo['name'];
                            $insertInfo['owner_wxid'] = $rooInfo['owner_wxid'];
                            $insertInfo['head_img'] = $rooInfo['head_img'];
                            $insertInfo['member_count'] = $rooInfo['member_count'];
                            $insertInfo['created_at'] = date('Y-m-d H:i:s', time());
                            $id = self::insertGetId('member_info_by_appid', $insertInfo);
                            if ($id > 0) {
                                self::bindingRoomId($rooInfo['name'], $id);
                            }
                        }
                    }
                }else{
                    //更新好友的所有信息
                    $updateWxInfo['wxid'] =  $rooInfo['wxid'];
                    $updateWxInfo['wx_alias'] =  isset($rooInfo['wx_alias'])?$rooInfo['wx_alias']:'';
                    $updateWxInfo['nickname'] =  $rooInfo['nickname'];
                    $updateWxInfo['head_img'] =  $rooInfo['head_img'];
                    $updateWxInfo['sex'] =  $rooInfo['sex'];
                    $updateWxInfo['country'] =  $rooInfo['country'];
                    $updateWxInfo['province'] =  $rooInfo['province'];
                    $updateWxInfo['city'] =  $rooInfo['city'];
                    self::changeWXInfoByWxid($updateWxInfo);
                }
                $insertData[] = $id;
            });
        }
        return true;
    }

    /**
     * 群成员信息更新
     * @param $action
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function  report_room_member_update($action, $appid, $wxid, $data)
    {
        $room_id = self::getIdByRoomId($data['room_wxid']);
        if(!empty($room_id)){
            $mem_count = self::getNewMemCount($room_id);
           // self::upByAppIdMid(['id'=>$room_id,'member_count'=>$mem_count,'wxid'=>$wxid,'is_delete'=>0]);
            self::upByAppIdMid(['id'=>$room_id,'appid'=>$appid,'wxid'=>$wxid,'is_delete'=>0]);
        }
        return  [];
    }


    /**
     * 上报群成员变化
     * @param $action
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public static function reportRoomMemberChange($action, $appid, $wxid, $data)
    {
        return DB::transaction(function () use ($action, $appid, $wxid, $data) {
            $room_id = self::getIdByRoomId($data['room_wxid']);

            if($data['flag']==1&&!empty($room_id)){
                if(!empty($data['increase_member_list'])){
                    foreach($data['increase_member_list'] as $v){
                        $WXid =  self::isSetWeiXinInfoRoom(['wxid'=>$v['wxid'],'room_wxid'=>$data['room_wxid'],'room_id'=>$room_id]);
                        if(empty($WXid)){

                            $insertWX['room_wxid'] = $data['room_wxid'];
                            $insertWX['wxid'] = $v['wxid'];
                            $insertWX['romm_id']=$room_id;
                            $insertWX['wx_alias']=$v['wx_alias'];
                            $insertWX['room_nickname']=$v['room_nickname'];
                            $insertWX['nickname']=$v['nickname'];
                            $insertWX['head_img']=$v['head_img'];
                            $insertWX['sex']=$v['sex'];
                            $insertWX['country']=$v['country'];
                            $insertWX['province']=$v['province'];
                            $insertWX['city']=$v['city'];
                            $insertWX['inviter']=isset($v['inviter'])?$v['inviter']:'';
                            $insertWX['is_delete']=0;
                            self::insertGetId('member_weixin_info',$insertWX);

                            $mem_count = self::getNewMemCountV2($room_id);
                            $mem_count = $mem_count+1;
                            if($mem_count==360){

                                self::addVipGroupLog($room_id,$wxid,1);
                            }
                            self::upByAppIdMid(['id'=>$room_id,'appid'=>$appid,'member_count'=>$mem_count,'wxid'=>$wxid,'is_delete'=>0]);

                        }else{
                           $updataWX['id']=$WXid->id;
                           $updataWX['romm_id']=$room_id;
                           $updataWX['room_nickname']=$v['room_nickname'];
                           $updataWX['wx_alias']=$v['wx_alias'];
                           $updataWX['nickname']=$v['nickname'];
                           $updataWX['head_img']=$v['head_img'];
                           $updataWX['sex']=$v['sex'];
                           $updataWX['country']=$v['country'];
                           $updataWX['province']=$v['province'];
                           $updataWX['city']=$v['city'];
                           $updataWX['inviter']= isset($v['inviter'])?$v['inviter']:'';
                           $updataWX['is_delete']=0;
                           self::changeWeixinInfoByWxid($updataWX);

                           if($WXid->is_delete==1){

                            $mem_count = self::getNewMemCountV2($room_id);
                            $mem_count = $mem_count+1;
                            if($mem_count==359){
                                self::addVipGroupLog($room_id,$wxid,2);
                            }
                            self::upByAppIdMid(['id'=>$room_id,'appid'=>$appid,'member_count'=>$mem_count,'wxid'=>$wxid,'is_delete'=>0]);     
                                                   
                           }                     
                        }
                    }
                }

                //self::upByAppIdMid(['id'=>$room_id,'wxid'=>$wxid,'is_delete'=>0]);

            }else if($data['flag']==0&&!empty($room_id)){

                if(!empty($data['wxid_list'])){
                    foreach($data['wxid_list'] as $v){
                       $WXid =  self::isSetWeiXinInfoRoom(['wxid'=>$v,'room_wxid'=>$data['room_wxid'],'room_id'=>$room_id]);
                       Log::info('reportRoomMemberChange delete member'.json_encode($WXid));
                       if(!empty($WXid)){
                           $updataWX['id']=$WXid->id;
                           $updataWX['is_delete']=1;
                           $updataWX['delete_time']=time();
                           self::changeWeixinInfoByWxid($updataWX);
                       }
                    }
                }
                $mem_count = self::getNewMemCountV2($room_id);
                self::upByAppIdMid(['id'=>$room_id,'appid'=>$appid,'member_count'=>$mem_count>0?$mem_count-1:0,'wxid'=>$wxid,'is_delete'=>0]);
                //self::upByAppIdMid(['id'=>$room_id,'wxid'=>$wxid,'is_delete'=>0]);
            }

            $insertData = [];
            $insertData['action'] = $action;
            $insertData['appid']  = $appid;
            $insertData['wxid']   = $wxid;
            $insertData['room_wxid']  = $data['room_wxid'];
            $insertData['owner_wxid'] = $data['owner_wxid'];
            $insertData['wxid_list']  = json_encode($data['wxid_list']);
            $insertData['flag']        = json_encode($data['flag']);
            $insertData['created_at'] = date('Y-m-d H:i:s', time());
            $id = self::insertGetId('member_change', $insertData);
            if($id){
                return true;
            }
            return false;
        });
    }

    /**
     *添加人数超过360群日志
     * @return array
     */
    public static function addVipGroupLog($room_id,$wxid,$type=0){

        $vipGroupLog = self::getVipGroupLog($room_id);
        $platform_identity = self:: getPlatformIdentity($wxid);
        if($type == 1){

            if(!empty($vipGroupLog)){

                if($vipGroupLog->is_delete==1){

                    $updateData = [];
                    $updateData['platform_identity'] = $platform_identity;
                    $updateData['wxid'] = $wxid;
                    $updateData['is_delete'] = 0;
                    $updateData['created_time'] = time();


                    return DB::table('vip_group_log')->where('room_id', $room_id)->update($updateData);
                }
            }else{

                $insertData = [];
                $insertData['room_id'] = $room_id;
                $insertData['platform_identity'] = $platform_identity;
                $insertData['wxid'] = $wxid;
                $insertData['is_delete'] = 0;
                $insertData['created_time'] = time(); 

                return DB::table('vip_group_log')->insert($insertData);              
            }

            

        }else{

            if(!empty($vipGroupLog)){

                if($vipGroupLog->is_delete==0){

                    $updateData = [];
                    $updateData['platform_identity'] = $platform_identity;
                    $updateData['wxid'] = $wxid;
                    $updateData['is_delete'] = 1;
                    $updateData['del_time'] = time();


                    return DB::table('vip_group_log')->where('room_id', $room_id)->update($updateData);
                }
            }else{

                $insertData = [];
                $insertData['room_id'] = $room_id;
                $insertData['platform_identity'] = $platform_identity;
                $insertData['wxid'] = $wxid;
                $insertData['is_delete'] = 1;
                $insertData['del_time'] = time();
                $insertData['created_time'] = time(); 

                return DB::table('vip_group_log')->insert($insertData);              
            }            


        }

    }

    public static function getVipGroupLog($room_id){

        $sql = "select * from sline_community_vip_group_log where room_id=?";
        $result = DB::selectOne($sql, [$room_id]);

        return !empty($result) ? $result: [];
    }


    public  static function getNewMemCount($id)
    {
        $getNoUserRoomSql= 'select count(*) as count_num  from sline_community_member_weixin_info  where romm_id=?  and is_delete=0  ';
        $result = DB::selectOne($getNoUserRoomSql, [$id]);
        return !empty($result) ? $result->count_num: 0;
    }

    public  static function getNewMemCountV2($id)
    {
        $getNoUserRoomSql= 'select member_count from sline_community_member_info_by_appid  where id=? ';
        $result = DB::selectOne($getNoUserRoomSql, [$id]);
        return !empty($result) ? $result->member_count: 0;
    }

    /**
     *查看当前好友是否已上报
     * @return array
     */
    public  static function isSetWeiXinInfoRoom($data)
    {
        $getNoUserRoomSql= 'select  id,wxid,is_delete    from  sline_community_member_weixin_info  where wxid=? and room_wxid=? and romm_id = ? ';
        $result = DB::selectOne($getNoUserRoomSql, [$data['wxid'],$data['room_wxid'],$data['room_id']]);
        return !empty($result) ? $result: [];
    }

    /**
     * 获取没有上报好友信息的群room
     * @return array
     */
    public  static function reportNoWeixinUserRoom()
    {
        $getNoUserRoomSql= 'select  sciba.room_wxid   from  sline_community_member_info_by_appid    as sciba  where (
                          select count(1) as num from sline_community_member_weixin_info  as scmli   
                          where scmli.romm_id =sciba.id) = 0';
        $result = DB::select($getNoUserRoomSql, []);
        return !empty($result) ? $result[0]->room_wxid: [];
    }
    /**
     *查看当前好友是否已上报
     * @return array
     */
    public  static function reportNoWeixinUserRoomIs($id,$room_wxid)
    {
        $getNoUserRoomSql= 'select  id,wxid    from  sline_community_member_weixin_info  where romm_id=? and room_wxid=? ';

        $result = DB::select($getNoUserRoomSql, [$id,$room_wxid]);
        return !empty($result) ? $result[0]->id: [];
    }
     /**
     *查出所有该群下所有的群成员
     * @return array
     */
    public  static function getRoomWxAllUser($id,$room_wxid)
    {
        $getNoUserRoomSql= 'select id,wxid from sline_community_member_weixin_info  where romm_id=? and room_wxid=? ';

        $result = DB::select($getNoUserRoomSql, [$id,$room_wxid]);
        return !empty($result) ? $result: [];
    }
    /**
     * 获取  好友  id
     * @return array
     */
    public  static function getWeixinInfoByWxid($wxid)
    {
        $getNoUserRoomSql= 'select  id    from  sline_community_member_weixin_info  where  wxid=? ';
        $result = DB::select($getNoUserRoomSql, [$wxid]);
        return !empty($result) ? $result[0]->id: [];
    }

    /**
     * 更新信息
     * @return array
     */
    public  static function changeWeixinInfoByWxid($wxData)
    {
        return DB::table('member_weixin_info')->where('id', $wxData['id'])->update($wxData);

    }


    /**
     * 更新信息
     * @return array
     */
    public  static function changeWXInfoByWxid($wxData)
    {
        return DB::table('member_weixin_info')->where('wxid', $wxData['wxid'])->update($wxData);

    }


    /**
     *查看当前群是否已上报
     * @return array
     */
    public  static function reportRoomWxId($room_wxid)
    {
        $getNoUserRoomSql= 'select  count(*) as countNum    from  sline_community_member_weixin_info  where  room_wxid=? ';
        $result = DB::select($getNoUserRoomSql, [$room_wxid]);
        return !empty($result) ? $result[0]->countNum: [];
    }


    /**
     * @param $room_wxid
     * @return array
     */
    public static function  getIdByRoomId($room_wxid )
    {
        $getIdSql = 'select   id   from  sline_community_member_info_by_appid  where room_wxid=?  ';
        $result = DB::select($getIdSql, [$room_wxid]);
        return !empty($result) ? $result[0]->id: [];
    }
    /**
     * 把刚入群的好友存上
     * @param $appid
     * @param $wxid
     * @param $data
     * @return bool
     */
    public static function  inserReportUserInfo($appid, $wxid, $data)
    {
        foreach($data['room_list'] as  $room_wxid)
        {
            //查询当前用户下 room_id 是否已存在  $data['room_list']   所在的群room_wxid
            $room_id =   self::getIdByRoomId($room_wxid);
            if(!empty($room_id)){
                $arr['romm_id'] =$room_id;
                $arr['room_wxid'] =$room_wxid;
                $arr['wxid'] =$data['wxid'];
                $arr['wx_alias'] =$data['wx_alias'];
                $arr['nickname'] =$data['nickname'];
                $arr['head_img'] =$data['head_img'];
                $arr['sex'] =$data['sex'];
                $arr['country'] =$data['country'];
                $arr['province'] =$data['province'];
                $arr['city'] =$data['city'];
                $arr['room_nickname'] ='';
                return DB::table('member_weixin_info')->insert($arr);
            }
        }
    }
    /**
     * 更新qrcode 信息登录
     * @param $data
     * @return int
     */
    public static function  communityMemberQrcodeUpdate($table,$key,$data){
        return DB::table($table)->where("$key", $data[$key])->update($data);
    }

    /**
     * 获取机器人创建群的群昵称
     */
    public static function  getAutoCreateGroupName(){
        $where = [
            'robot_send_status' => 1,
            'group_name_update' => 0
        ];
        return DB::table('auto_group_create')
            ->where($where)->orderBy('id', 'asc')->first();
    }

    /**
     * @param $groupName
     * @return mixed
     */
    public static function  saveLeaveJudge($insertInfo){
        $judgeNum = config('community.countNumV2') ?? 50;
        $insertData = [];
        $insertData['room_id'] = $insertInfo['id'];
        $insertData['name'] = $insertInfo['name'];
        $insertData['wxid'] = $insertInfo['wxid'];
        $insertData['room_wxid'] = $insertInfo['room_wxid'];
        $insertData['member_count'] = $insertInfo['member_count'];
        if ($insertData['member_count'] >= $judgeNum) {
            $insertData['execute_status'] = 2;
        }
        return DB::table('leave_judge')->insertGetId($insertData);
    }


    /**
     * @param $groupName
     * @return mixed
     */
    public static function  isSetGroupNameId($groupName){
        $getNoUserRoomSql="select  id   from  sline_community_invitation_group  where group_name='{$groupName}'";
        $result = DB::selectOne($getNoUserRoomSql, []);

        return  !empty($result) ? $result->id: [];
    }

    /**
     * 获取qr_session_id
     * @return array
     */
    public static function  getAddQrSessionId(){
        $uniqid = md5(uniqid(microtime(true),true));
        $arr['qrcode_session_id'] =$uniqid;
        $arr['created_at'] =date('Y-m-d H:i:s',time());
        DB::table('member_qrcode')->insert($arr);
        return  ['task_id'=>'7','qrcode_session_id'=>$uniqid];
    }
    /**
     * 获取最后的 qrcode生成记录
     * @return array
     */
    public static function getMemberQrcodeId(){
        $getNoUserRoomSql= 'select  id,created_at,login_id   from  sline_community_member_qrcode  order by id desc  limit 1 ';
        $result = DB::select($getNoUserRoomSql, []);
        $returnData = !empty($result) ? $result[0]: [];
        if(!empty($returnData)){
            $returnData=[];
        }
        return $returnData;
    }
    /**
     * 当机器人被拉入新群 要拿到所有信息
     * @param $appid
     * @param $wxid
     * @param $data
     * @return array
     */
    public static function  userReportNewRoom($appid, $wxid, $data)
    {
        // 判断当前wxid 与 和群 room是否已存在
        $isSetId = self::getRoomMemberId($appid, $data['wxid']);

        $action = 'report_new_room';
        if(empty($isSetId)){
           DB::transaction(function () use ($action, $appid, $wxid, $data) {
                $insertInfo=[];
                $insertInfo['action']    =$action;
                $insertInfo['appid']     = $appid;
                $insertInfo['wxid']      = $wxid;
                $insertInfo['room_wxid'] = $data['wxid'];
                $insertInfo['name']      = $data['name'];
                $insertInfo['owner_wxid'] = $data['owner_wxid'];
                $insertInfo['head_img']    = $data['head_img'];
                $insertInfo['memberInfo_list'] = json_encode($data['memberInfo_list']);
                $insertInfo['member_count']     = count($data['memberInfo_list']);
                $insertInfo['created_at']       = date('Y-m-d H:i:s', time());
                $id = self::insertGetId('member_info_by_appid', $insertInfo);
                if ($id > 0) {
                    self::bindingRoomId($data['name'], $id);
                    //保存新增群到新增群判断, 五天内不满足建群人数,机器人自动退群
                    $platform_identity = self:: getPlatformIdentity($wxid);
                    if ($platform_identity == '7323ff1a23f0bf1cda41f690d4089353') {
                        $insertInfo['id'] = $id;
                        self::saveLeaveJudge($insertInfo);
                    }
                    //更新审核表的room_id
                    $dataUpdate['group_name'] = $data['name'];
                    $dataUpdate['room_id'] = $id;
                    $res1 = self::isSetGroupNameId($data['name']);
                    if (!empty($res1)) {
                        self::communityMemberQrcodeUpdate('invitation_group', 'group_name', $dataUpdate);
                    }
                    self::insertWxUserInfo($id, $data['wxid'], 'member_weixin_info', $data['memberInfo_list']);
//                    $newRoomDataKey = config('community.new_room_redis_key');
//                    Redis::rpush($newRoomDataKey, json_encode(['wxid'=>$wxid,'room_wxid'=>$data['wxid']])); //加入 待发送的 群队列值
                }
            });
        }else{
            $updatedData=[];
            $updatedData['id'] = $isSetId;
            $updatedData['action'] = $action;
            $updatedData['appid'] = $appid;
            $updatedData['wxid'] = $wxid;
            $updatedData['room_wxid'] = $data['wxid'];
            $updatedData['name'] = $data['name'];
            $updatedData['head_img'] = $data['head_img'];
            $updatedData['owner_wxid'] = $data['owner_wxid'];
            $updatedData['member_count'] = count($data['memberInfo_list']);
            $updatedData['is_delete'] = 0;
            $updatedData['updated_at'] = date('Y-m-d H:i:s', time());
            self::updateMemberTableData($updatedData);
            //查看当前 微信room
            $id = self::reportNoWeixinUserRoomIs($isSetId, $data['wxid']);
            if (empty($id)) {
                self::insertWxUserInfo($isSetId, $data['wxid'], 'member_weixin_info', $data['memberInfo_list']);
            }
        }

        //差看群昵称是否存在, 机器人自动创建群,群昵称不存在, 找到群昵称, 同事下达修改群昵称任务
        if (empty($data['name'])) {
            $autoCreateGroupInfo = self::getAutoCreateGroupName();
            if ($autoCreateGroupInfo) {
                $data['name'] = $newGroupName = $autoCreateGroupInfo->name;
                $id = $autoCreateGroupInfo->id;
                $autoCreateUpdateData = [];
                $autoCreateUpdateData['room_wxid'] = $data['wxid'];
                $autoCreateUpdateData['group_name_update'] = 1;
                //下达修改群昵称任务
                $update_group_name_key = $wxid . AutoGroupCreateModel::UPDATE_GROUP_NAME_KEY;
                $updateGroupNameData = [];
                $updateGroupNameData['room_wxid'] = $data['wxid'];
                $updateGroupNameData['room_name'] = $newGroupName;
                Redis::rpush($update_group_name_key, json_encode($updateGroupNameData)); //加入队列值
                self::updateAutoCreateGroupData($id, $autoCreateUpdateData);
            }
        }
    }
    //获取当前微信号对应的群 号
    public static function getWXRoomId($appid,$room_wxid)
    {
        $sql = 'select  room_wxid  from sline_community_member_info_by_appid where wxid= ?';
        $result = DB::select($sql, [$room_wxid]);
        return !empty($result) ? $result: [];
    }
    /**
     * 发送的消息
     * @return array
     */
    public  static function  getPushMsgData($manageMid,$wxid)
    {
        $todayTime =date('Y-m-d H:i:s',time()-14400);
        $sql =" select  id,type,typeid,content,random_str,short_url,link_type,tag_ids,cid,link_title,link_desc,link_img_url,is_more_product,platform_identity,exclusive_mid  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and robot_send_status=0 and  is_timing_send=0  and mid={$manageMid} and exclusive_mid=0 and created_at>'{$todayTime}'  order by id desc limit 0,1)
                  as t) and   status = 0  and deleted_at=0  and is_send=1 and is_timing_send=0  and robot_send_status=0  and mid={$manageMid} and created_at>'{$todayTime}' " ;
        $result = DB::select($sql, [$todayTime,$todayTime]);
        return !empty($result) ? $result: [];
    }

    /**
     * 发送的消息
     * @return array
     */
    public  static function  getPushMsgDataTag($manageMid,$wxid)
    {
        $todayTime =date('Y-m-d H:i:s',time()-14400);
        $sql =" select  id,type,content,random_str,short_url,link_type,tag_ids,is_more_product,platform_identity,exclusive_mid  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and robot_send_status=0  and is_redis=1 and  is_timing_send=0  and mid={$manageMid}  and exclusive_mid=0 and created_at>'{$todayTime}'  order by id desc limit 0,1)
                  as t) and   status = 0  and deleted_at=0  and is_redis=1   and is_send=1 and is_timing_send=0  and robot_send_status=0  and mid={$manageMid} and created_at>'{$todayTime}' " ;
        $result = DB::select($sql, [$todayTime,$todayTime]);
        return !empty($result) ? $result: [];
    }
    /**
     * 定时发送的消息
     * @return [type] [description]
     */
    public  static function  getTimePushMsgData($manageMid,$wxid)
    {
        $todayTime = time()-14400;
        $sql = " select  id,type,typeid,content,random_str,short_url,link_type,tag_ids,cid,link_title,link_desc,link_img_url,is_more_product,platform_identity,exclusive_mid  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and is_timing_send=1 and robot_send_status=0 and mid={$manageMid} and unix_timestamp(now())>timing_send_time and timing_send_time>?  order by timing_send_time desc limit 0,1)
                  as t) and   status = 0  and deleted_at=0 and is_send=1  and robot_send_status=0 and is_timing_send=1 and timing_send_time !=0 and unix_timestamp(now())>timing_send_time and mid={$manageMid} and timing_send_time>? " ;
        $result = DB::select($sql, [$todayTime,$todayTime]);
        return !empty($result) ? $result: [];
    }

    /**
     * 定时发送的消息
     * @return [type] [description]
     */
    public  static function  getTimePushMsgDataTag($manageMid,$wxid)
    {
        $todayTime = time()-14400;
        $sql = " select  id,type,content,random_str,short_url,link_type,tag_ids,platform_identity,is_more_product,exclusive_mid  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and is_timing_send=1 and robot_send_status=0  and is_redis=1  and mid={$manageMid}  and exclusive_mid=0 and unix_timestamp(now())>timing_send_time and timing_send_time>?  order by timing_send_time desc limit 0,1)
                  as t) and   status = 0 and deleted_at=0 and is_send=1  and robot_send_status=0 and is_timing_send=1 and is_redis=1   and timing_send_time !=0  and unix_timestamp(now())>timing_send_time and mid={$manageMid} and timing_send_time>? " ;
        $result = DB::select($sql, [$todayTime,$todayTime]);
        return !empty($result) ? $result: [];
    }



    /**
     * 专属机器人 定时发送的消息
     * @return [type] [description]
     */
    public  static function  getExcluPushMsgDataTag($manageMid,$wxid)
    {
        $todayTime = time()-7200;
        $sql = " select  id,type,content,random_str,short_url,link_type,tag_ids,platform_identity,is_more_product,exclusive_mid  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and is_timing_send=1 and robot_send_status=0  and is_redis=1  and exclusive_mid={$manageMid} and unix_timestamp(now())>timing_send_time and timing_send_time>?  order by timing_send_time desc limit 0,1)
                  as t) and   status = 0 and deleted_at=0 and is_send=1  and robot_send_status=0 and is_timing_send=1 and is_redis=1   and timing_send_time !=0  and unix_timestamp(now())>timing_send_time and exclusive_mid={$manageMid} and timing_send_time>? " ;
        $result = DB::select($sql, [$todayTime,$todayTime]);
        return !empty($result) ? $result: [];
    }

    /**
     * @param $manageMid
     * @param $wxid
     * @return array
     */
    public  static function  getExcluTimePushMsgData($manageMid,$wxid)
    {
        $todayTime = time()-7200;
        $sql = " select  id,type,content,random_str,short_url,link_type,tag_ids,cid,link_title,link_desc,link_img_url,is_more_product,platform_identity,exclusive_mid  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and is_timing_send=1 and robot_send_status=0 and exclusive_mid={$manageMid} and unix_timestamp(now())>timing_send_time and timing_send_time>?  order by timing_send_time desc limit 0,1)
                  as t) and   status = 0  and deleted_at=0 and is_send=1  and robot_send_status=0 and is_timing_send=1 and timing_send_time !=0 and unix_timestamp(now())>timing_send_time and exclusive_mid={$manageMid} and timing_send_time>? " ;
        $result = DB::select($sql, [$todayTime,$todayTime]);
        return !empty($result) ? $result: [];
    }

    /**
     * 获取当前微信号是否已发送
     * @param $random_str
     * @param $wxid
     * @return array
     */
    public static function getWxSendReload($random_str,$wxid)
    {
        $sql = 'select  id,robot_send_status   from sline_community_send_reload  where  random_str = ?  and wxid= ? ';
        $result = DB::select($sql, [$random_str,$wxid]);
        return !empty($result) ? $result[0]: [];
    }

    /**
     * 总的条数
     * @param $random_str
     * @param $wxid
     * @return array
     */
    public static function getWxSendCount($random_str)
    {
        $sql1 = 'select  count(1) as countNum   from sline_community_send_reload  where  random_str = ? ';
        $result1 = DB::select($sql1, [$random_str]);
        return !empty($result1) ? $result1[0]->countNum: [];
    }

    /**更新发送记录
     * @param $data
     * @return int
     */
    public static function updateSendData($data)
    {
        return DB::table('send_reload')->where('random_str', $data['random_str'])->update($data);
    }


    /**更新发送记录
     * @param $data
     * @return int
     */
    public static function updateSendInfo($data)
    {
        return DB::table('send_info')->where('random_str', $data['random_str'])->update($data);
    }
    /**
     *
     * @param $wxid
     * @param $data
     * @return array
     */
    public static function isWhiteData($wxid,$data)
    {
        if(!empty($data['wx_alias'])){
            $sql1 = 'select  id    from  sline_community_white_list_wx  where  wx_alias = ? and wxid=? and status=0';
            $result1 = DB::select($sql1, [$data['wx_alias'],$wxid]);
        }else {
            $sql1 = 'select  id    from  sline_community_white_list_wx  where   wxid=? and status=0';
            $result1 = DB::select($sql1, [$wxid]);
        }
        return !empty($result1) ? $result1[0]->id: [];
    }

    /**
     * 判断是否申请了专属机器人
     * @param $wxid
     * @param $data
     * @return array
     */
    public static  function isExclusiveData($wxid,$data){
        $sql1 = 'select  id,platform_identity,manage_phone,mid,user_wx   from  sline_community_exclusive_application  where  user_wx = ? ';
        $result1 = DB::selectOne($sql1, [$data['wx_alias']]);
        return !empty($result1) ? json_decode(json_encode($result1),true): [];
    }

    /**
     * @param $platform_identity
     * @return array|mixed
     */
    public static  function getOrderBydata($platform_identity){
        $sql1 = 'select  id,order_by,wx_name   from  sline_community_white_list_wx  where  platform_identity = ? ';
        $result1 = DB::selectOne($sql1, [$platform_identity]);
        return !empty($result1) ? json_decode(json_encode($result1),true): [];
    }

    /**
     *上级管理者
     */
    public   static function getBindMid($manage_phone,$platform_identity){
        $sql1 = 'select  id,mid,sup_id   from  sline_community_user  where  mobile = ?  and platform_identity=?';
        $result1 = DB::selectOne($sql1, [$manage_phone,$platform_identity]);
        return !empty($result1) ? json_decode(json_encode($result1),true): [];
    }



    /**
     * 获取唯一的群标识
     * @return [type] [description]
     */
    public  static function  getPushMsgRoomIdBuyMid($wxid,$roomids)
    {
        if(empty($roomids)){
            $sql = "select room_wxid  from sline_community_member_info_by_appid where mid={$wxid}  and is_delete=0";
        }else{
            $sql = "select room_wxid  from sline_community_member_info_by_appid where mid={$wxid}  and is_delete=0";
        }
        $result = DB::select($sql, []);
        return !empty($result) ? $result: [];
    }

    /**
     * @param $mid
     */
    public static function getCodeMember($mid)
    {
        $sql = 'select code_number from sline_invitation_code  where  mid=? ';
        $res =DB::connection('mysql_yulvhui')->select($sql,[$mid]);
        return empty($res) ? 0 : $res[0]->code_number;
    }

    /**
     * 获取服务的 定时信息
     * @param $wxid
     * @return array
     */
    public static  function  getServiceTimePushMsgData($wxid)
    {
        //获取所有的群 和 mid
        $sql = "select scmiba.mid,group_concat(room_wxid) as mid_roomids,group_concat(scmiba.id) as roomids,code_number,wxid  from sline_community_member_info_by_appid   as scmiba 
                left  join  sline_community_user  as  scu  on scmiba.mid=scu.mid
                where scmiba.wxid=?  and scmiba.is_delete=0 group by scmiba.mid   ";
        $result = DB::select($sql, [$wxid]);
        $returnData =[];
        //升序 查出第一批未发送的定时消息
        foreach($result as $roomInfo){
            //获取符合条件的消息
            $sql = 'select  id,type,content,random_str,short_url,room_ids  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and mid=?  and robot_send_status=0 and  timing_send_time !=0 and unix_timestamp(now())>timing_send_time  order by id asc limit 0,1)
                  as t) and   status = 0   and is_send=1  and robot_send_status=0  and mid=? and  timing_send_time !=0 and unix_timestamp(now())>timing_send_time';
            $resultMsgInfo= DB::select($sql, [$roomInfo->mid,$roomInfo->mid]);

            if(!empty($resultMsgInfo)){
                $roomData = explode(',',$roomInfo->mid_roomids);
                if(empty($resultMsgInfo[0]->room_ids)){
                    $roomids = explode(',',$roomInfo->roomids);
                    foreach($roomData as $key=> $room_wxid)
                    {
                        $returnData[] = ['code_number'=>$roomInfo->code_number,'random_str'=>$resultMsgInfo[0]->random_str,'room_id'=>$roomids[$key],'room_wxid'=>$room_wxid,'msg_info'=>json_encode($resultMsgInfo)];
                    }
                }else{
                    $sendIds = explode(',',$resultMsgInfo[0]->room_ids);
                    $roomids = explode(',',$roomInfo->roomids);
                    foreach($sendIds as $idInfo){
                        $key = array_search($idInfo,$roomids);
                        if($key>=0){
                            $room_wxid = $roomData[$key];
                            $returnData[] = ['code_number'=>$roomInfo->code_number,'random_str'=>$resultMsgInfo[0]->random_str,'room_id'=>$idInfo,'room_wxid'=>$room_wxid,'msg_info'=>json_encode($resultMsgInfo)];
                        }
                    }
                }
            }
        }
        return $returnData;
    }

    /**
     * 获取即刻消息
     * @param $wxid
     * @return array
     */
    public static  function  getServicePushMsgData($wxid)
    {
        //获取所有的群 和 mid
        $sql = "select scmiba.mid,group_concat(room_wxid) as mid_roomids,group_concat(scmiba.id) as roomids,code_number,wxid  from sline_community_member_info_by_appid   as scmiba 
                left  join  sline_community_user  as  scu  on scmiba.mid=scu.mid
                where scmiba.wxid=?  and scmiba.is_delete=0 group by scmiba.mid   ";
        $result = DB::select($sql, [$wxid]);
        $returnData =[];
        //升序 查出第一批未发送的定时消息
        foreach($result as $roomInfo){
            //获取符合条件的消息
            $sql = 'select  id,type,content,random_str,short_url,room_ids  from sline_community_send_info  where random_str in
                  (select t.random_str from
                  (select random_str  from sline_community_send_info where status=0 and is_send=1  and mid=?  and robot_send_status=0 and  timing_send_time =0  order by id asc limit 0,1)
                  as t) and   status = 0   and is_send=1  and robot_send_status=0  and mid=? and  timing_send_time =0 ';
            $resultMsgInfo= DB::select($sql, [$roomInfo->mid,$roomInfo->mid]);
            if(!empty($resultMsgInfo)){

                $roomData = explode(',',$roomInfo->mid_roomids);
                $sendIds = explode(',',$roomInfo->room_ids);
                if(empty($resultMsgInfo[0]->room_ids)){
                    foreach($roomData as $keyids=>$room_wxid)
                    {
                        $key = array_search($room_wxid,$roomData);
                        $returnData[] = ['code_number'=>$roomInfo->code_number,'room_id'=>$sendIds[$key],'random_str'=>$resultMsgInfo[0]->random_str,'room_wxid'=>$room_wxid,'msg_info'=>json_encode($resultMsgInfo)];
                    }
                }else{
                    $sendIds = explode(',',$resultMsgInfo[0]->room_ids);
                    $roomids = explode(',',$roomInfo->roomids);
                    foreach($sendIds as $idInfo){
                        $key = array_search($idInfo,$roomids);
                        if($key>=0){
                            $room_wxid = $roomData[$key];
                            $returnData[] = ['code_number'=>$roomInfo->code_number,'room_id'=>$idInfo,'random_str'=>$resultMsgInfo[0]->random_str,'room_wxid'=>$room_wxid,'msg_info'=>json_encode($resultMsgInfo)];
                        }
                    }
                }
            }
        }
        return $returnData;
    }

    /**
     * 更新消息
     */
    public  static function changeServicePushMsgData($random_str,$wxid){
        self:: updateSendInfo(['random_str'=>$random_str,'robot_send_status'=>1,'is_send'=>2]);
        self::insertGetId('send_reload',['random_str'=>$random_str,'wxid'=>$wxid]);
    }


    /**
     *  查看当前机器人绑定的 用户关系
     * @param $wxid
     * @return mixed
     */
    public static function  getWxidBindMid($wxid)
    {
        $sql = " select mid,bind_mid,is_send_by_ass,exclusive_mid,wxid_type   from  sline_community_white_list_wx   where wxid='{$wxid}' and  deleted_at=0";
        $result = DB::selectOne($sql);
        return $result;
    }


    /**
     *
     * @param $data
     * @return int
     */
    public static function updateSendInfoByRandomtr($wheredata,$data)
    {
        return DB::table('send_info')->where('random_str', $wheredata['random_str'])->update($data);
    }

    public static function updateSendInfoByRandomtrSend($wheredata,$data)
    {
        return DB::table('send_reload')->where('random_str', $wheredata['random_str'])->where('wxid', $data['wxid'])->update($data);
    }
    /**
     * @param $wxid
     */
    public static function getNotReportRoom($wxid){
        $sql = "select  count(1) as count_num,scmi.room_wxid,scmi.created_at 
               from  sline_community_member_info_by_appid  as scmi  
              left join  sline_community_member_weixin_info as  scmw 
              on scmw.room_wxid=scmi.room_wxid
              where scmi.wxid=? and  scmi.deleted_at=0   group by scmi.room_wxid HAVING count(1) <5";
        $resultMsgInfo= DB::select($sql, [$wxid]);
        return $resultMsgInfo;
    }

    /**
     * 新增好友
     * @param $appid
     * @param $wxid
     * @param $data
     */
    public  static function addFriendRequest($appid, $wxid, $data){
        $insertData = [
            'appid'=>$appid,
            'wxid'=>$wxid,
            'v1'=>$data['v1'],
            'v2'=>$data['v2'],
            'notice_word'=>$data['notice_word'],
            'raw_msg'=>$data['raw_msg'],
        ];
        self::insertTableData('member_add_request',$insertData);
    }
    //查询有重复的数据
    public static function getDeleteTableRoomId(){
        $sql = "select count(1) as count_num, wxid,room_wxid   from  sline_community_member_weixin_info group by wxid,room_wxid having  count_num>1  limit 10";
        $resultMsgInfo= DB::select($sql, []);
        return $resultMsgInfo;
    }
    //查询重复数据对应的群id
    public  static function getDeleteRoom($wxid,$room_wxid){
        $sql = "select *  from sline_community_member_weixin_info  where wxid=? and room_wxid=?";
        $resultMsgInfo= DB::select($sql, [$wxid,$room_wxid]);
        return $resultMsgInfo;
    }

    public  static function geRoomMemberApp($room_id,$room_wxid){
        $sql = "select *  from  sline_community_member_info_by_appid  where id=? and room_wxid=?";
        $resultMsgInfo= DB::select($sql, [$room_id,$room_wxid]);
        return $resultMsgInfo;
    }

    public  static function geRoomWeiXinMemberApp($room_id){
        $sql = "delete from sline_community_member_weixin_info where  romm_id=?";
        $resultMsgInfo= DB::select($sql, [$room_id]);
        return $resultMsgInfo;
    }

    public  static function geRobotFriendNumber($wxid){
        $sql = "select count(1) as  count_num    from sline_community_robot_friend where  robot_wxid=?";
        $resultMsgInfo= DB::select($sql, [$wxid]);
        return  $resultMsgInfo[0]->count_num>0?$resultMsgInfo[0]->count_num:[];
    }

    public  static function geRobotAppidFriendNumber($wxid){
        $sql = "select count(1) as  count_num    from sline_community_member_info_by_appid  where  wxid=?";
        $resultMsgInfo= DB::select($sql, [$wxid]);
        return  $resultMsgInfo[0]->count_num>0?$resultMsgInfo[0]->count_num:[];
    }

    public  static function getWeixinDataRobot($param){
        $sql = "select wxid,robot_wxid,id from sline_community_robot_friend limit {$param['offset']},{$param['display']} ";
        $resultMsgInfo= DB::select($sql, []);
        return  $resultMsgInfo;
    }


    public static function updateRobotCommonGroupNum($wxid,$robot_wxid,$id){
        $sql    = "select count(1) as count_num  from `sline_community_member_weixin_info`  where  room_wxid in 
                  (select room_wxid  from `sline_community_member_weixin_info`   where  wxid=? )  and wxid=? ";
        $result =DB::selectOne($sql,[$wxid,$robot_wxid]);
        $commonNum = !empty($result) ? $result->count_num : 0;
        if($commonNum>0){
            return DB::table('robot_friend')->where('id', $id)->update(['common_group_num'=>$commonNum]);
        }
        return $commonNum;
    }
    /**
     * 关键词回复
     * @param $dataMsg
     * @param $wxid
     * @return array
     */
    public static  function geRobotReplyData($dataMsg,$wxid, $roomWxid){
        $sql  = " select sck.id,sck.title,sck.content,sck.type  from sline_community_message_keyword as sck ";
        $sql  .= " left join sline_community_member_info_by_appid  as scm  on FIND_IN_SET(scm.tag_id,sck.tag_id)  where scm.room_wxid=?";
        $sql  .= " and sck.keyword = ? and sck.deleted_at=0 and sck.platform_identity in";
        $sql  .= " ( select t.platform_identity  from   (  select scu.platform_identity  from sline_community_user  as scu ";
        $sql  .= " left  join  sline_community_white_list_wx  as scw  on  scu.mid=scw.mid  where  scw.wxid=? ) as t  )   ";
        $resultMsgInfo= DB::select($sql, [$roomWxid, $dataMsg, $wxid]);
        return  $resultMsgInfo;
    }

    /**
     * 详细内容返回
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function geRobotReturnData($msgId,$wxid){
        $sql  = " select scm.id,scm.title,scm.content  from sline_community_message_reply  as scm 
                   left join sline_community_message_keyword as sck on scm.keyword_id=sck.id  
                   where  scm.id =? and sck.deleted_at=0 and scm.deleted_at=0 and scm.platform_identity in
                   ( select t.platform_identity  from   (  select scu.platform_identity  from sline_community_user  as scu  
                     left  join  sline_community_white_list_wx  as scw  on  scu.mid=scw.mid  where  scw.wxid=? ) as t  )   ";
        $resultMsgInfo= DB::selectOne($sql, [$msgId,$wxid]);
        return     !empty($resultMsgInfo) ? $resultMsgInfo->content :'';
    }


    public static function geRobotFriendReturnDataByWxid($wxid)
    {

        $sql  = " select id,image,content,type  from  sline_community_into_group_reply where tag_id like '%".$wxid."%' and reply_address
=1 and deleted_at = 0";
        $resultMsgInfo= DB::select($sql);
        return     !empty($resultMsgInfo) ? $resultMsgInfo:[];

    }

    /**
     * 获取记录关键词的关键词列表,遍历匹配记录
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function getKeywordList($room_wxid, $wxid){
//        $sql  = " select rk.id,rk.keyword,scm.id as room_id,rk.platform_identity from  sline_community_record_keyword  as rk ";
//        $sql  .= " left join sline_community_member_info_by_appid  as scm ";
//        $sql  .= " on FIND_IN_SET(scm.tag_id,rk.tag_id)  where scm.room_wxid= '{$room_wxid}' AND rk.deleted_at = 0";
//        $sql  .= " AND rk.platform_identity = (select `platform_identity` FROM sline_community_white_list_wx";
//        $sql  .= " WHERE wxid = '{$wxid}')";
        $sql  = " select rk.id,rk.keyword,rk.platform_identity from  sline_community_record_keyword  as rk ";
        $sql  .= " where rk.deleted_at = 0";
        $sql  .= " AND rk.platform_identity = (select `platform_identity` FROM sline_community_white_list_wx";
        $sql  .= " WHERE wxid = '{$wxid}')";
        $resultMsgInfo= DB::select($sql);
        return !empty($resultMsgInfo) ? json_decode(json_encode($resultMsgInfo), true) : [];
    }


    /**
     * 获取记录关键词的关键词列表,遍历匹配记录
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function getRoomIdByRoomWxId($room_wxid){

        return DB::table('member_info_by_appid')->where(['room_wxid' => $room_wxid])->value('id');
    }


    /**
     * 记录关键词触发次数加一
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function incrRecordKeyword($id){
        return DB::table('record_keyword')->where(['id' => $id])->increment('touch_off_num');
    }


    /**
     * 获取记录关键词数据
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function getRecordTail($condition){
        $where = [
            'room_id' => $condition['room_id'],
            'keyword_id' => $condition['keyword_id'],
            'created_at' => $condition['created_at'],
            'platform_identity' => $condition['platform_identity'],
        ];
        return DB::table('record_keyword_detail')->where($where)->first();
    }


    /**
     * 好友的表示回复
     * @param $msgId
     * @param $wxid
     * @return array
     */
    public  static function geRobotFriendReturnData($msgId,$room_wxid){
        $sql  = " select sci.id,image,content,type  from  sline_community_into_group_reply  as sci 
                  left join sline_community_member_info_by_appid  as scm  on FIND_IN_SET(scm.tag_id,sci.tag_id)  where sci.reply_address = 0 and scm.room_wxid=? and sci.id=?";
        $resultMsgInfo= DB::select($sql, [$room_wxid,$msgId]);
        return     !empty($resultMsgInfo) ? $resultMsgInfo:[];
    }

    /**
     * 获取新增好友的回复
     * @param $msgId
     * @param $room_wxid
     * @return array
     */
    public  static function geNewFriendReturnData($room_wxid, $platform_identity){
        $sql  = " select sci.id,sci.title,sci.content  from  sline_community_into_group_reply  as sci ";
        $sql  .= " left join sline_community_member_info_by_appid  as scm  on";
        $sql  .= " FIND_IN_SET(scm.tag_id,sci.tag_id)";
        $sql  .= " where sci.reply_address = 0 and sci.deleted_at = 0 and sci.platform_identity = ? and  scm.room_wxid=?";
        $resultMsgInfo= DB::select($sql, [$platform_identity, $room_wxid]);
        return     !empty($resultMsgInfo) ? $resultMsgInfo:[];
    }


    /**
     * 获取新增好友的回复
     * @param $msgId
     * @param $room_wxid
     * @return array
     */
    public  static function geNewFriendReturnDataV1($room_wxid, $platform_identity,$msg){
        $sql  = " select sci.id,sci.title,sci.content,sci.type  from  sline_community_message_keyword  as sci ";
        $sql  .= " left join sline_community_member_info_by_appid  as scm  on";
        $sql  .= " FIND_IN_SET(scm.tag_id,sci.tag_id)";
        $sql  .= " where sci.deleted_at = 0 and sci.platform_identity = ? and  scm.room_wxid=? and sci.keyword= ?";
        $resultMsgInfo= DB::select($sql, [$platform_identity, $room_wxid,$msg]);
        return     !empty($resultMsgInfo) ? $resultMsgInfo:[];
    }


    /**
     * 当前消息发送的数量
     * @param $rand_str
     * @return int
     */
    public static function getSendMsgRoomCountNew($rand_str,$wxid){
        $sql    = "select count(1) as count_num  from `sline_community_send_room_reload`  where  random_str=?  and wxid=?";
        $result =DB::selectOne($sql,[$rand_str,$wxid]);
        $commonNum = !empty($result) ? $result->count_num : 0;
        return $commonNum;
    }

    /**
     * @param $wxid
     * @param $tag_ids
     * @return array
     */
    public static function getShouldSendRoomCountNew($wxid, $tag_ids)
    {
        $sql = "select  count(1) as count_num  from sline_community_member_info_by_appid   as scm 
             where  scm.wxid='{$wxid}' and scm.is_delete=0  and  scm.tag_id in (".$tag_ids.")";

        Log::info('room    getShouldSendRoomCountNew ' .json_encode([$sql]));
        $result = DB::selectOne($sql, []);
        $commonNum = !empty($result) ? $result->count_num : 0;
        return $commonNum;
    }
    /**
     * @param $wxid
     * @param $tag_ids
     * @return array
     */
    public static function getShouldSendRoomCount()
    {
        $sql = "select  count(1) as count_num  from sline_community_member_info_by_appid   as scm 
             where   scm.is_delete=0  ";
        $result = DB::selectOne($sql, []);
        $commonNum = !empty($result) ? $result->count_num : 0;
        return $commonNum;
    }



    /**
     * 当前消息发送的数量
     * @param $rand_str
     * @return int
     */
    public static function getSendMsgRoomCount($wxid){
        $sql    = "select count(1) as count_num  from `sline_community_send_room_reload`  where   wxid=?";
        $result =DB::selectOne($sql,[$wxid]);
        $commonNum = !empty($result) ? $result->count_num : 0;
        return $commonNum;
    }
    /**
     * 修改记录  上传记录
     * @param $data
     * @return int
     */
    public static function updateWixdRandomtrRoom($wheredata,$data)
    {
        return DB::table('send_reload')->where('random_str', $wheredata['random_str'])->update($data);
    }
    /**
     * 机器人退出或者被T出群 软删除所有成员
     * @param $data
     * @return int
     */
    public static function upRoomWxMemberInfo($roomWxId = 0, $data = [])
    {
        return DB::table('member_weixin_info')->where('room_wxid', $roomWxId)->update($data);
    }


    /**
     * 当前消息发送的数量
     * @param $rand_str
     * @return int
     */
    public static function getSendRoomCount($rand_str,$room_wxid){
        $sql    = "select count(1) as count_num  from `sline_community_send_room_reload`  where  random_str=?  and room_wxid=?";
        $result =DB::selectOne($sql,[$rand_str,$room_wxid]);
        $commonNum = !empty($result) ? $result->count_num : 0;
        return $commonNum;
    }


    /**
     * 总的GMV
     * @param $source
     * @return int
     */
    public static function getWhileBindMidGmv($wxid)
    {
        $sql = "select  exclusive_mid,all_order_sum,count(1) as group_sum  from sline_community_white_list_wx   as scw 
                left join  sline_community_member_info_by_appid  as scm on  scw.wxid=scm.wxid
                where scw.wxid='{$wxid}'  and  scw.deleted_at=0 and  scm.is_delete=0 ";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : 0;
    }
}