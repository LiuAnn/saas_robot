<?php

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;

class GroupInfoModel extends Model
{
    // 指定表名
    protected $table = 'community_member_info_by_appid';

    /**
     * 用户群列表
     * @param $params
     * @return mixed
     * @author  liujiaheng
     * @created 2020/4/8 4:08 下午
     */
    public static function queryGroupList($params)
    {
        return self::where(function ($query) use ($params) {
            // 指定用户
            $query->where(['mid' => $params['mid']]);
            // 是否已经激活
            if (isset($params['type'])) {
                $query->where(['is_activation' => $params['type']]);
            }
        })
            ->select(['head_img', 'name', 'cliecked_num', 'member_count', 'id'])
            ->get();
    }

}
