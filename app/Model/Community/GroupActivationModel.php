<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/3/31
 * Time: 22:07
 */

namespace App\Model\Community;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupActivationModel extends Model
{

    /**
     * 激活数量 未激活数量
     * @param $mid
     * @param $staus
     * @return array
     */
    public static function getActivationNum($mid, $staus)
    {
        $sql = ' select  count(1) as countNum  from sline_community_member_info_by_appid   where  mid =?  and  is_activation=?  ';
        $result = DB::select($sql, [$mid, $staus]);
        return !empty($result) ? $result[0]->countNum : [];
    }

    /**
     * 获取 群数据
     * @param $mid
     * @param $staus
     * @param $page
     * @param $pageSize
     */
    public static function getActivationGroupData($mid, $staus, $page, $pageSize)
    {
        $sql = "select scm.id as room_id,scm.room_wxid,scm.name,scm.head_img,scm.member_count   from sline_community_member_info_by_appid  as scm 
                  where  scm.mid =?  and   scm.is_activation=?  order by scm.id  desc  limit ?, ?";
        $result = DB::select($sql, [$mid, $staus, $page, $pageSize]);
        return !empty($result) ? $result : [];
    }

    /**
     * 统计浏览数量
     */
    public static function browseCountNum($romm_id)
    {
        $data_time = date("Y-m-d");
        $sql = "select count(1) as countNum  from sline_community_user_browse where romm_id =?  and created_at>$data_time";
        $result = DB::select($sql, [$romm_id]);
        return !empty($result) ? $result[0]->countNum : [];
    }

    /**
     * 获取基础用户的卡类型、昵称、头像信息
     * @param $uid
     * @return mixed
     */
    public static function getBriefInfo($uid)
    {
        $sql = "select card_type,nickname,litpic,jointime from yuelvhui.sline_member  where mid =?  ";
        $result = DB::select($sql, [$uid]);
        return !empty($result) ? (array)$result[0] : [];
    }

    /**
     * 获取会员等级信息
     * @param $uid
     * @return array
     */
    public static function getMemberLevelInfo($uid)
    {
        $sql = " select sl.name,sl.icon from yuelvhui.sline_level sl left join yuelvhui.sline_member_level sm on sl.id = sm.level_id ";
        $sql .= " where sm.mid = ? ";
        $res = DB::select($sql, [$uid]);
        return empty($res) ? [] : $res;
    }

    /**
     * 获取群的基本信息
     * @param $mid
     * @param $roomId
     * @param string $field
     * @return array
     */
    public static function getGroupInfoByRoomId($mid, $roomId, $field = "*")
    {
        $sql = ' select  ' . $field . '   from sline_community_member_info_by_appid  as scm where  scm.mid =?  and   scm.id=?  ';
        $result = DB::select($sql, [$mid, $roomId]);
        return !empty($result) ? $result[0] : [];
    }

    /**
     * 获取群激活的数量
     * @param $roomId
     * @return array
     */
    public static function getActivationNumberCount($roomId)
    {
        $sql = 'select  count(*) as countNum  from  sline_community_user_browse  where romm_id= ? and goods_id=0 ';
        $result = DB::select($sql, [$roomId]);
        return !empty($result) ? $result[0]->countNum : [];
    }
    /**
     * 获取激活该群的人的基本信息
     * @param $roomId
     * @return array
     */
    public static function getActivationData($roomId)
    {
        $sql = "select  CASE sm.litpic  WHEN  ''  THEN  'http://image.yuelvhui.com/pubfile/2018/07/17/line_1531821979.png'  else sm.litpic END as headPic   from  sline_community_user_browse  as scu 
                left join yuelvhui.sline_member as sm  on scu.mid=sm.mid
                where romm_id= ?  order by  scu.id asc limit 10";
        $result = DB::select($sql, [$roomId]);
        return !empty($result) ?$result:[];
    }


    /**
     * 获取机器人管理的微信对应的群主id
     * @param $roomId
     * @return array
     */
    public static function getGroupOwerIdCodeNum($field,$wxid)
    {
        $sql = ' select  ' . $field . '   from sline_community_member_info_by_appid  as scm 
                   left join  sline_community_invitation_group   as scg on scm.id=scg.room_id
                   left join yuelvhui.sline_invitation_code  as sic on sic.mid=scg.mid 
                    where  scm.wxid =? and  scg.mid>0 ';
        $result = DB::select($sql, [$wxid]);
        return !empty($result) ? $result : [];
    }


    public static function getGroupOwerIdCodeNumByRoomId( $field,$room_id)
    {
        $sql = ' select  ' . $field . '   from sline_community_member_info_by_appid  as scm 
                   left join yuelvhui. sline_invitation_code  as sic on sic.mid=scm.mid 
                    where  scm.id =? and  scm.mid>0 and scm.is_delete=0 ';
        $result = DB::select($sql, [$room_id]);
        return !empty($result) ? $result[0] : [];
    }

    /**
     * @param $wxid
     * @return array
     */
    public static function getGroupOwerIdCodeNumCopy($wxid)
    {
        $sql ="select scm.room_wxid,scm.id,scm.mid,sic.code_number from sline_community_member_info_by_appid   as scm 
             left join yuelvhui. sline_invitation_code  as sic on sic.mid=scm.mid
             where  scm.wxid=? and scm.is_delete=0 and tag_id!=11 order by   scm.order_num desc ,scm.id asc,scm.mid desc  ";
        $result = DB::select($sql, [$wxid]);
        return !empty($result) ? $result : [];
    }

    public static function getGroupOwerIdCodeNumNew($wxid,$tag_ids)
    {
        $sql ="select scm.room_wxid,scm.id,scm.mid,sic.code_number from sline_community_member_info_by_appid   as scm 
             left join yuelvhui. sline_invitation_code  as sic on sic.mid=scm.mid
             where  scm.wxid=? and scm.is_delete=0  and  scm.tag_id in ({$tag_ids})";
        $result = DB::select($sql, [$wxid]);
        return !empty($result) ? $result : [];
    }

}