<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/4/14
 * Time: 21:18
 */

namespace App\Model\RobotOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RobotModel   extends Model
{
    /**
     * 初始化数据
     * @return \Illuminate\Database\Connection
     */
    public static function newDb()
    {
        return DB::connection('robot');
    }
    /*
     * 查询售卖的服务
     */
    public static function getRobotServiceList()
    {
        $sql = "SELECT id,service_name,service_price as pay_price,FORMAT(service_price/100,0) as service_price,group_number,remark,service_num,service_unit,CASE service_unit WHEN 1 THEN '年' WHEN 2 THEN '月'  ELSE '日' END as unit
              FROM sline_community_service  WHERE is_delete=0  order by sort  desc";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * 获取购买记录
     * @param $mid
     * @return array
     */
    public static function getUserOrderByMid($mid=65013)
    {
        $sql = "SELECT count(1) as buy_num
              FROM sline_community_buy_info  where mid={$mid} and pay_status=1";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->buy_num : [];
    }

    /*
     * 查询售卖的服务详细
     */
    public static function getServiceInfoById($service_id)
    {
        $sql = "SELECT id,service_name,service_price,group_number,remark,group_number ,CASE service_unit WHEN 1 THEN '年' WHEN 2 THEN '月'  ELSE '日' END as unit
              FROM sline_community_service  WHERE is_delete=0  and id={$service_id}";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0] : [];
    }

    /*
    * 查询售卖的服务记录
    */
    public static function getRobotInfoById($serviceId)
    {
        $sql = "SELECT id,service_name,service_price,group_number,remark ,CASE service_unit WHEN 1 THEN '年' WHEN 2 THEN '月'  ELSE '日' END as unit
              FROM sline_community_service  WHERE is_delete=0  and id={$serviceId}";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0] : [];
    }

    /*
        * 查询售卖的服务
        */
    public static function getRobotServiceOrderReload($mid)
    {
        $sql = "SELECT id,mid,ordersn,service_name,FORMAT(service_price/100,0) as service_price,service_number,remark
              FROM sline_community_buy_info  where mid={$mid}";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * @param array $data
     * @return int
     */
    public static function addOrderInfo($table,$data=[])
    {
        return self::newDb()-> table($table)->insertGetId($data);
    }
    /**
     * 订单入库
     * @param $insertData
     */
   public static function  insertOrder($insertData){
      return  self::addOrderInfo('buy_order',$insertData);
   }

    /**
     * 详细
     * @param $insertData
     * @return int
     */
    public static function  insertOrderInfo($insertData){
        return  self::addOrderInfo('buy_info',$insertData);
    }
    /**
     * 获取订单详细
     * @param $order_sn
     * @param $mid
     * @return array
     */
   public static function getOrderInfo($order_sn,$mid){
       $sql = "SELECT *  FROM sline_community_buy_order  where  order_sn={$order_sn} and mid={$mid}";
       $result = self::newDb() -> select($sql);
       return !empty($result) ? $result[0] : [];
   }


    /**
     * 获取订单详细
     * @param $order_sn
     * @param $mid
     * @return array
     */
    public static function getPayOrderInfo($order_sn){
        $sql = "SELECT *  FROM sline_community_buy_order  where  order_sn={$order_sn} ";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0] : [];
    }
    /**
     *  获取查看信息
     * @param $order_sn
     * @param $mid
     * @return array
     */
    public static function getChakanMid($mid,$room_id){
        $sql = "SELECT id,count_num  FROM  sline_community_room_browse  where  mid={$mid} and romm_id={$room_id}";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0] : [];
    }
    /**
     *  获取查看信息
     * @param $order_sn
     * @param $mid
     * @return array
     */
    public static function getUserChakanMid($mid,$room_id,$goods_id,$channel,$goods_type,$robot_send){
        $sql = "SELECT id,count_num  FROM  sline_community_goods_browse  where  mid={$mid} and romm_id={$room_id} and goods_id={$goods_id} and channel={$channel} and  goods_type={$goods_type} and  robot_send={$robot_send}";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0] : [];
    }


    public static function updateSee($table,$key,$data){
        return DB::table($table)->where("$key", $data[$key])->update($data);
    }

    /**
     * 拿到机器人数据
     * @param $param
     * @param $page
     * @param $pageSize
     * @param $member
     * @return array
     */
    public static function getRobotListData($param,$page,$pageSize,$member,$type=1)
    {
        $where = empty($param['wx_alias'])? '': "  and   wx_alias like  '%".$param['wx_alias']."%'" ;
        $midS = "('" . implode("','", $param['midS']) . "')";
        $orderBy=' order by order_by asc';
        if($member['user_type']==1){
            if($param['bind_master']==1){
                $orderBy = ' order by scwlw.bind_mid_num asc' ;
            }else if($param['bind_master']==2){
                $orderBy = ' order by scwlw.bind_mid_num desc' ;
            }
            if($param['bind_group_number']==1){
                $orderBy = ' order by scwlw.bind_group_num asc' ;
            }else if($param['bind_group_number']==2){
                $orderBy = ' order by scwlw.bind_group_num desc' ;
            }

            $sql = "SELECT   count(o.room_id) as orderCount,sum(o.actual_price) as orderPrice,scwlw.id,scwlw.wx_alias,scwlw.deleted_at,scmiba.wxid,scwlw.mid,scwlw.bind_mid,scwlw.bind_mid_num,scwlw.bind_group_num,scwlw.order_by  FROM  sline_community_member_info_by_appid  as scmiba";
            $sql.=" left join  sline_community_white_list_wx  scwlw  on scmiba.wxid=scwlw.wxid";
            $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id=scmiba.id and o.room_id>0 and o.pay_status=1 ";
            $sql.=" where scmiba.mid in {$midS}  {$where}  group by scmiba.wxid  {$orderBy} limit $page,$pageSize";
        }else if($member['user_type']>=2){
            if($param['bind_master']==1){
                $orderBy = ' order by r.bind_mid_num asc' ;
            }else if($param['bind_master']==2){
                $orderBy = ' order by r.bind_mid_num desc' ;
            }
            if($param['bind_group_number']==1){
                $orderBy = ' order by r.bind_group_num asc' ;
            }else if($param['bind_group_number']==2){
                $orderBy = ' order by r.bind_group_num desc' ;
            }
            if($param['orderCount']==1){
                $orderBy = ' order by orderCount asc' ;
            }else if($param['orderCount']==2){
                $orderBy = ' order by orderCount desc' ;
            }
            if($param['orderPrice']==1){
                $orderBy = ' order by orderPrice asc' ;
            }else if($param['orderPrice']==2){
                $orderBy = ' order by orderPrice desc' ;
            }

            if (empty($member['sup_id'])) {
                $sql  = " SELECT   count(o.room_id) as orderCount,sum(o.actual_price) as orderPrice,r.id,r.wx_alias,
 r.deleted_at,r.wxid,r.bind_mid,u.name,r.wx_name,r.mid,r.bind_mid_num,r.bind_group_num,r.order_by  FROM  sline_community_white_list_wx as r ";
                $sql .=" left join sline_community_user as u on r.mid = u.mid ";
                $sql .= " left join sline_community_member_info_by_appid g on g.wxid=r.wxid ";
                $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id=g.id and o.room_id>0 and o.pay_status=1 ";
                $sql .=" where r.wxid_type={$type}    {$where}   and r.mid in {$midS} group by r.id {$orderBy} limit $page,$pageSize";
            } else {
                $sql  = " SELECT   count(o.room_id) as orderCount,sum(o.actual_price) as orderPrice,r.id,r.wx_alias,r.deleted_at,r.wxid,bind_mid,u.name,r.wx_name,r.mid,r.bind_mid_num,r.bind_group_num,r.order_by  FROM  sline_community_white_list_wx as r"; 
                $sql .= " left join sline_community_user as u on r.mid = u.mid ";
                $sql .= " left join sline_community_member_info_by_appid g on g.wxid=r.wxid";
                $sql .= " left join ".config('community.oldMallOrderGoods')." o on o.room_id=g.id and o.room_id>0 and o.pay_status=1 ";
                $sql .= " where r.wxid_type={$type}   {$where}   and r.bind_mid in {$midS} group by r.id {$orderBy}  limit $page,$pageSize";
            }

        }
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * 拿到机器人数据
     * @param $param
     * @param $page
     * @param $pageSize
     * @param $member
     * @return array
     */
    public static function getNewRobotListData($condition, $page, $pageSize)
    {
        $begin = ($page - 1) * $pageSize;
        $orderBy = ' order by order_by asc';
        $where = self::getYCloudRobotListData($condition);
        //userId YCloud系统的用户ID
        $sql = " SELECT r.id,r.wx_alias,r.wxid,r.wx_name,r.exclusive_mid,r.bind_mid_num,r.bind_group_num,r.status";
        $sql .= "  FROM  sline_community_white_list_wx as r ";
        $sql .= " where {$where}  group by r.id {$orderBy} limit $begin,$pageSize";

        $result = self::newDb()->select($sql);
        return !empty($result) ? $result : [];
    }


    /**
     * 通过机器人ID获取机器人信息
     * @param $id
     * @return array|mixed
     */
    public static function fetchRobotInfoById($id)
    {
        $where = [
            'id' => $id
        ];
        $robotInfo = self::newDb()->table('white_list_wx')->where($where)->first();
        return $robotInfo ? json_decode(json_encode($robotInfo), true) : [];
    }


    /**
     * 通过机器人ID获取机器人信息
     * @param $id
     * @return array|mixed
     */
    public static function fetchGroupInfoById($id)
    {
        $where = [
            'id' => $id
        ];
        $robotInfo = self::newDb()->table('member_info_by_appid')->where($where)->first();
        return $robotInfo ? json_decode(json_encode($robotInfo), true) : [];
    }


    /**
     * 修改机器人信息
     * @param $id
     * @param $update
     * @return int
     */
    public static function updateRobotInfoById($id, $update)
    {
        $where = [
            'id' => $id
        ];
        return self::newDb()->table('white_list_wx')->where($where)->update($update);
    }


    /**
     * 修改机器人信息
     * @param $id
     * @param $update
     * @return int
     */
    public static function updateGroupInfoById($id, $update)
    {
        $where = [
            'id' => $id
        ];
        return self::newDb()->table('member_info_by_appid')->where($where)->update($update);
    }


    public static function getYCloudRobotListData($condition)
    {
        $where='r.deleted_at = 0 ';

        if(isset($condition['userId'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " r.exclusive_mid = {$condition['userId']}";
        }

        if(isset($condition['wxAlias']) && !empty($condition['wxAlias'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= " r.wx_alias like '%{$condition['wxAlias']}%'";
        }

        if(isset($condition['platform_identity']) && !empty($condition['platform_identity'])){
            $where = $where ? $where . ' and ' : $where;
            $where .= "r.platform_identity = '{$condition['platform_identity']}'";
        }
        return $where;
    }


    public static function getRobotAdminName($bindIds)
    {
        if (empty($bindIds)) {
            return [];
        }
        return self::newDb()->table('user')->whereIn('mid', $bindIds)->pluck('name', 'mid');
    }


    public static function insertRobotInfo($insertData)
    {
        return self::newDb()->table('white_list_wx')->insertGetId($insertData);
    }


    /**
     * @param $param
     * @param $member
     * @param $type   1  社群   3 专属
     * @return array
     */
    public static function getRobotCount($param,$member,$type=1){
        $where = empty($param['wx_alias'])?'': "  and   wx_alias like  '%".$param['wx_alias']."%'" ;
        $midS = "('" . implode("','", $param['midS']) . "')";
        if($member['user_type']==1){
            $sql = "SELECT  count(1) as countNum  FROM  sline_community_member_info_by_appid  as scmiba
                    left join  sline_community_white_list_wx  scwlw  on scmiba.wxid=scwlw.wxid
                    where scmiba.mid in {$midS}   {$where}  group by scmiba.wxid  ";
        }else if($member['user_type']>=2){
            if (empty($member['sup_id'])) {
                $sql = "SELECT  count(1) as countNum    FROM  sline_community_white_list_wx  where wxid_type={$type}     {$where}  and   deleted_at=0  and mid in {$midS}  ";
            } else {
                $sql = "SELECT  count(1) as countNum    FROM  sline_community_white_list_wx  where wxid_type={$type}     {$where}  and   deleted_at=0  and bind_mid in {$midS} ";
            }
        }
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->countNum : [];
    }

    /**
     * @param $param
     * @param $member
     * @param $type   1  社群   3 专属
     * @return array
     */
    public static function getNewRobotCount($condition){
        $where = self::getYCloudRobotListData($condition);
        $sql = "SELECT  count(1) as cnt   FROM  sline_community_white_list_wx r";
        $sql .= "  where  {$where}";
        $result = self::newDb()->selectOne($sql);
        return $result ? $result->cnt : 0;
    }

    /**
     * 绑定群主
     * @param $wxid
     * @param $member
     * @return array
     */
    public static function bindMasterNumber($wxid,$member)
    {
        if($member['user_type']==1){
            $sql = "SELECT count(*)  as countNum FROM (SELECT  count(1) as countNum    FROM  sline_community_member_info_by_appid  where  wxid='{$wxid}' and mid='{$member['mid']}' group by mid ) a  ";
        }else if($member['user_type']>=2){
            $sql = "SELECT count(*)   as countNum FROM (SELECT  count(1) as countNum    FROM  sline_community_member_info_by_appid  where  wxid='{$wxid}' and mid>0 group by mid ) a  ";
        }
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->countNum : [];
    }



    public static function robotManagerName($mid)
    {
        $sql = "select name   from  sline_community_user where mid= {$mid} ";
        $result = self::newDb() -> selectOne($sql);
        return !empty($result) ? $result : [];
    }
    /**
     * 绑定群数量
     * @param $wxid
     * @param $member
     * @return array
     */
    public static function bindGroupNumber($wxid,$member){
        if($member['user_type']==1){
            $sql = "SELECT  count(1) as countNum   FROM  sline_community_member_info_by_appid  where mid={$member['mid']}   and wxid='{$wxid}'   ";
        }else if($member['user_type']>=2){
            $sql = "SELECT  count(1) as countNum    FROM  sline_community_member_info_by_appid  where  wxid='{$wxid}'  ";
        }
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->countNum : [];

    }
    /**
     * 绑定群数量
     * @param $wxid
     * @param $member
     * @return integer
     */
    public static function bindNewGroupNumber($wxid){
        $sql = "SELECT  count(1) as cnt    FROM  ";
        $sql .= "sline_community_member_info_by_appid  where  wxid='{$wxid}' ";
        $result = self::newDb() -> selectOne($sql);
        return $result ? $result->cnt : 0;

    }
    //已绑定群主的群数量
    public static function bindGroupNumberMid($wxid,$member){
        if($member['user_type']==1){
            $sql = "SELECT  count(1) as countNum   FROM  sline_community_member_info_by_appid  where mid={$member['mid']}   and wxid='{$wxid}'   ";
        }else if($member['user_type']>=2){
            $sql = "SELECT  count(1) as countNum    FROM  sline_community_member_info_by_appid  where  wxid='{$wxid}' and mid>0  ";
        }
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->countNum : [];

    }


    /**
     * 总的订单总额
     * @param $data
     * @param $member
     * @return array
     */
    public static function getRoomOrderData($data,$member)
    {
        $where='';
        if (isset($data['beginTime']) && isset($data['endTime'])&&!empty($data['beginTime'])&&!empty($data['endTime'])) {
            $where  =  "  and  pay_time>={$data['beginTime']} and  pay_time<=  {$data['endTime']}" ;
        }else if(isset($data['beginTime'])&&empty($data['endTime'])&&!empty($data['beginTime'])){
            $where  =  "  and  pay_time>={$data['beginTime']}";
        }else if(isset($data['endTime'])&&empty($data['beginTime'])&&!empty($data['endTime'])){
            $where  =  "  and  pay_time<={$data['endTime']}";
        }
        if($member['user_type']==1){
            $sql = " select sum(actual_price) as coun_amount from  sline_community_member_info_by_appid  as scmiba
                     left join  ".config('community.oldMallOrderGoods')."  as smog  on scmiba.id=smog.room_id
                    where scmiba.mid={$member['mid']}  and smog.pay_status=1 and scmiba.wxid='{$data['wxid']}'  {$where}";
        }else if($member['user_type']>=2){
            $sql = "select sum(actual_price)  as coun_amount from  sline_community_member_info_by_appid  as scmiba
                     left join  ".config('community.oldMallOrderGoods')."  as smog  on scmiba.id=smog.room_id
                    where  scmiba.wxid='{$data['wxid']}' and smog.pay_status=1   {$where}";
        }
        $result = self::newDb() -> select($sql);

        return !empty($result) ? $result[0]->coun_amount : [];
    }


    /**
     * 总的订单总额
     * @param $data
     * @param $member
     * @return array
     */
    public static function getRoomOrderCount($data,$member)
    {
        $where='';
        if (isset($data['beginTime']) && isset($data['endTime'])&&!empty($data['beginTime'])&&!empty($data['endTime'])) {
            $where  =  "  and  pay_time>={$data['beginTime']} and  pay_time<=  {$data['endTime']}" ;
        }else if(isset($data['beginTime'])&&empty($data['endTime'])&&!empty($data['beginTime'])){
            $where  =  "  and  pay_time>={$data['beginTime']}";
        }else if(isset($data['endTime'])&&empty($data['beginTime'])&&!empty($data['endTime'])){
            $where  =  "  and  pay_time<={$data['endTime']}";
        }
        if($member['user_type']==1){
            $sql = " select sum(1) as coun_amount from  sline_community_member_info_by_appid  as scmiba
                     left join  ".config('community.oldMallOrderGoods')."  as smog  on scmiba.id=smog.room_id
                    where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}' and smog.pay_status=1   {$where}";
        }else if($member['user_type']>=2){
            $sql = " select sum(1)  as coun_amount from  sline_community_member_info_by_appid  as scmiba
                     left join ".config('community.oldMallOrderGoods')."  as smog  on scmiba.id=smog.room_id
                    where  scmiba.wxid='{$data['wxid']}'  and smog.pay_status=1    {$where}";
        }

//        Log::info('getEveryDayTotalOrderCount' .  json_encode($sql));
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->coun_amount : [];
    }

    /**
     * 群数量
     * @param $data
     * @param $member
     * @return array
     */
    public static function getRoomCountNum($data,$member,$type=0)
    {
        $where='';
        if($member['user_type']==1){
            if($type==1){
                $tadayTime = date('Y-m-d' ,time());
                $where ="  and  scmiba.created_at>'{$tadayTime}' ";
            }else{
                if (isset($data['beginTimeDate']) && isset($data['endTimeDate']) &&!empty($data['beginTimeDate'])&&!empty($data['beginTimeDate'])) {
                    $where  =  "  and  scmiba.created_at>='{$data['beginTimeDate']}' and  scmiba.created_at<='{$data['endTimeDate']}' " ;
                }else if(isset($data['beginTimeDate'])&&empty($data['endTimeDate'])&&!empty($data['beginTimeDate'])){
                    $where  =  "  and  scmiba.created_at>='{$data['beginTime']}'";
                }else if(isset($data['endTimeDate'])&&empty($data['beginTimeDate'])&&!empty($data['beginTimeDate'])){
                    $where  =  "  and  scmiba.created_at<='{$data['endTimeDate']}'";
                }
            }
            $sql = " select count(1) as coun_amount from  sline_community_member_info_by_appid  as scmiba where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}' {$where} ";
        }else if($member['user_type']>=2){
            if($type==1){
                $tadayTime = date('Y-m-d' ,time());
                $where =" and  scmiba.created_at>'{$tadayTime}' ";
            }else{
                if (isset($data['beginTimeDate']) && isset($data['endTimeDate']) &&!empty($data['beginTimeDate'])&&!empty($data['beginTimeDate'])) {
                    $where  =  "  and  scmiba.created_at>='{$data['beginTimeDate']}' and  scmiba.created_at<='{$data['endTimeDate']}' " ;
                }else if(isset($data['beginTimeDate'])&&empty($data['endTimeDate'])&&!empty($data['beginTimeDate'])){
                    $where  =  "  and  scmiba.created_at>='{$data['beginTime']}'";
                }else if(isset($data['endTimeDate'])&&empty($data['beginTimeDate'])&&!empty($data['beginTimeDate'])){
                    $where  =  "  and  scmiba.created_at<='{$data['endTimeDate']}'";
                }
            }
            $sql = " select count(1)  as coun_amount  from  sline_community_member_info_by_appid  as scmiba   where  scmiba.wxid='{$data['wxid']}' {$where} ";
        }
//        Log::info('getEveryDayTotalOrder num Count' .  json_encode($sql));
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->coun_amount : [];
    }

    /**
     * 总的用户数
     * @param $data
     * @param $member
     */
    public  static function getUserCountNum($data,$member,$type=0){
        $where='';
        if($member['user_type']==1){
            if($type==1){
                $tadayTime = date('Y-m-d' ,time());
                $where =" and  wxinfo.created_at>='{$tadayTime}' ";
            }else{
                if (isset($data['beginTimeDate']) && isset($data['endTimeDate'])&&!empty($data['beginTimeDate'])&&!empty($data['endTimeDate'])) {
                    $where  =  "  and  wxinfo.created_at>='{$data['beginTimeDate']}' and  wxinfo.created_at<='{$data['endTimeDate']}' " ;
                }else if(isset($data['beginTimeDate'])&&empty($data['endTimeDate'])&&!empty($data['beginTimeDate'])){
                    $where  =  "  and  wxinfo.created_at>='{$data['beginTime']}'";
                }else if(isset($data['endTimeDate'])&&empty($data['beginTimeDate'])&&!empty($data['endTimeDate'])){
                    $where  =  "  and  wxinfo.created_at<='{$data['endTimeDate']}'";
                }
            }

            $sql = " select  count(1) as coun_amount from  sline_community_member_info_by_appid  as scmiba 
                     left join sline_community_member_weixin_info  as wxinfo on scmiba.room_wxid= wxinfo.room_wxid
                     where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}' {$where} ";
        }else if($member['user_type']>=2){
            if($type==1){
                $tadayTime = date('Y-m-d' ,time());
                $where ="  and  weixin.created_at>='{$tadayTime}' ";
            }else{
                if (isset($data['beginTimeDate']) && isset($data['endTimeDate'])&&!empty($data['beginTimeDate'])&&!empty($data['endTimeDate'])) {
                    $where  =  "  and weixin.created_at>='{$data['beginTimeDate']}' and  weixin.created_at<='{$data['endTimeDate']}' " ;
                }else if(isset($data['beginTimeDate'])&&empty($data['endTimeDate'])&&!empty($data['beginTimeDate'])){
                    $where  =  "  and  weixin.created_at>='{$data['beginTime']}'";
                }else if(isset($data['endTimeDate'])&&empty($data['beginTimeDate'])&&!empty($data['endTimeDate'])){
                    $where  =  "  and  weixin.created_at<='{$data['endTimeDate']}'";
                }
            }
             $sql = " select  count(1)  as coun_amount from  sline_community_member_weixin_info as weixin  where  weixin.room_wxid in (
            (select t.room_wxid   from
            (select room_wxid  from sline_community_member_info_by_appid  where  wxid='{$data['wxid']}' )as t
            ))   {$where}";
        }

//        Log::info('get  count  user  count_amount' .  json_encode($sql));
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->coun_amount : [];
    }


    /*
    * 获取总GMV
    * @param Array
    */
    public static function getEveryDayTotalGMV($data,$member,$stattime= "")
    {
//        if($member['user_type']==1){
//
//            $sql = " select group_concat(id) as room_ids  from  sline_community_member_info_by_appid  as scmiba where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}'  ";
//        }else if($member['user_type']>=2){
//
//            $sql = " select  group_concat(id) as room_ids  from  sline_community_member_info_by_appid  as scmiba   where  scmiba.wxid='{$data['wxid']}'  ";
//        }
//        $result = self::newDb() -> select($sql);
//
//        $where = "";
//        if(!empty($result[0]->room_ids)){
//            $where .= " and room_id in (". trim($result[0]->room_ids,',').")";
//        }else{
//            return [];
//        }

        if (isset($data['beginTime']) && isset($data['endTime'])&&!empty($data['beginTime'])&&!empty($data['endTime'])) {
            $where1  =  "  and  smog.pay_time>={$data['beginTime']} and  smog.pay_time<=  {$data['endTime']}" ;
        }else{
            $stattime = strtotime($stattime);
            $where1 =  " and  smog.pay_time >= {$stattime}";
        }
//        $sql = "select sum(actual_price)/100  as totalmoney,DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times from
//              yuelvhui.sline_mall_order_goods where pay_status=1 {$where}  {$where1}
//              GROUP BY DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d')";


        if($member['user_type']==1){
            $sql = " select sum(1) as coun_amount from  sline_community_member_info_by_appid  as scmiba
                     left join  ".config('community.oldMallOrderGoods')."  as smog  on scmiba.id=smog.room_id
                    where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}' and smog.pay_status=1   {$where1}";
        }else if($member['user_type']>=2){
            $sql = " select sum(actual_price)/100  as totalmoney,DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times  
                  from  sline_community_member_info_by_appid  as scmiba
                     left join  ".config('community.oldMallOrderGoods')."  as smog  on scmiba.id=smog.room_id
                    where  scmiba.wxid='{$data['wxid']}'  and smog.pay_status=1    {$where1} GROUP BY DATE_FORMAT(FROM_UNIXTIME(pay_time,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d')";
        }

//        Log::info('getEveryDayTotalGMV' .  json_encode($sql));
        $result = self::newDb() ->select($sql);

        return $result;
    }



    /*
    * 补全统计日期中没有数据的日期
    */
    public static function completionDate($param, $endGmvdata, $enduserdata,$enduseradddata)
    {
        #计算日期内天数
        $stimestamp = $param['beginTime']+86400;
        $etimestamp = $param['endTime'];
        #计算日期段内有多少天
        $days = ($etimestamp - $stimestamp) / 86400;
        #保存每天日期
        $date = array();
        for ($i = 0; $i <= $days; $i++) {
            $date[] = date('Y-m-d', $stimestamp + (86400 * $i));
        }
        $data1=[];
        $data2=[];
        $data3=[];
        #循环补全日期
        foreach ($date as $key => $val) {
            $data1[$val] = [
                'times' => $val, //时间
                'totalmoney' => sprintf("%.2f",0) //订单金额
            ];
            $data2[$val] = [
                'times' =>$val , //时间
                'count_1' => 0
            ];

            $data3[$val] = [
                'times' => $val, //时间
                'count_add' => 0 //发群消息数量
            ];
            foreach ($endGmvdata as $item1 => $value) {
                if ($val == $item1) {
                    $data1[$val]['totalmoney'] = sprintf("%.2f",$value);
                }
            }
            foreach ($enduserdata as $item2 => $value) {
                if ($val == $item2) {
                    $data2[$val]['count_1'] = $value;
                }
            }
            foreach ($enduseradddata as $item3 => $value) {
                if ($val == $item3) {
                    $data3[$val]['count_add'] = $value;
                }
            }
        }
        return ['data1'=>$data1,'data2'=>$data2,'data3'=>$data3];
    }

    /**
     * @param $data
     * @param $member
     * @param string $stattime
     */
    public static function  getEveryDayTotalUser($data,$member,$stattime= ""){

        if($member['user_type']==1){

            $sql = " select group_concat(room_wxid) as room_wxids  from  sline_community_member_info_by_appid  as scmiba where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}'  ";
        }else if($member['user_type']>=2){

            $sql = " select  group_concat(room_wxid) as room_wxids  from  sline_community_member_info_by_appid  as scmiba   where  scmiba.wxid='{$data['wxid']}'  ";
        }
        $result = self::newDb() -> select($sql);
        Log::info('getEveryDayTotalUser1' .  json_encode($result));
        $where ='';
        $roomData = explode(',',$result[0]->room_wxids);
        foreach( $roomData as $v){
            $where .=  "'".$v."'," ;
        }
        $where2='';
        if(!empty($where)){
            $where2= " and room_wxid in ( "  .trim($where,',')." )";
        }
        if (isset($data['beginTimeDate']) && isset($data['endTimeDate'])&&!empty($data['beginTimeDate'])&&!empty($data['endTimeDate'])) {
            $where1  =  "   created_at>='{$data['beginTimeDate']}' and  created_at<='{$data['endTimeDate']}' " ;
        }else{
            $where1 =  "    created_at>='{$stattime}' " ;
        }

        $sql = "select count(*) as count_1,DATE_FORMAT(created_at,'%Y-%m-%d') as times from
          sline_community_member_weixin_info   where {$where1}  {$where2}   GROUP BY DATE_FORMAT(created_at,'%Y-%m-%d')";
//        Log::info('getEveryDayTotalUser' .  json_encode($sql));
        return DB::select($sql);

    }

    /**
     * 统计新增数量
     * @param $data
     * @param $member
     * @param $startTime
     */
    public  static function getUserAddSendInfoData($data,$member,$stattime="")
    {
        if($member['user_type']==1){

            $sql = " select group_concat(room_wxid) as room_wxids  from  sline_community_member_info_by_appid  as scmiba where scmiba.mid={$member['mid']}   and scmiba.wxid='{$data['wxid']}'  ";
        }else if($member['user_type']>=2){

            $sql = " select  group_concat(room_wxid) as room_wxids  from  sline_community_member_info_by_appid  as scmiba   where  scmiba.wxid='{$data['wxid']}'  ";
        }
        $result = self::newDb() -> select($sql);
        Log::info('getEveryDayTotalUser1' .  json_encode($result));
        $where ='';
        $roomData = explode(',',$result[0]->room_wxids);
        foreach( $roomData as $v){
            $where .=  "'".$v."'," ;
        }
        $where2='';
        if(!empty($where)){
            $where2= " and room_wxid in ( "  .trim($where,',')." )";
        }
        if (isset($data['beginTimeDate']) && isset($data['endTimeDate'])&&!empty($data['beginTimeDate'])&&!empty($data['endTimeDate'])) {
            $where1  =  "   created_at>='{$data['beginTimeDate']}' and  created_at<='{$data['endTimeDate']}' " ;
        }else{
            $where1 =  "    created_at>='{$stattime}' " ;
        }

        $sql = "select count(*) as count_add,DATE_FORMAT(created_at,'%Y-%m-%d') as times from
          sline_community_send_room_reload   where {$where1}  {$where2}   GROUP BY DATE_FORMAT(created_at,'%Y-%m-%d')";
//        Log::info('getUserAddData User' .  json_encode($sql));
        return DB::select($sql);
    }


    /**获取最后一次登陆时间
     * @param $wxid
     */
    public static  function lastloginTime($wxid){
        $sql = " select login_time  from  sline_community_member_login_info  where wxid='{$wxid}' and type=1  order by id desc limit 1";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->login_time : '';
    }

    /**获取最后一次拉取任务时间
     * @param $wxid
     */
    public static  function lastWorkTime($wxid){
        $sql = " select created_at as login_time  from  sline_community_action_log_one  where wxid='{$wxid}' order by id desc limit 1";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->login_time : '';
    }


    /**
     * @param $wxid
     */
    public static  function getNoBingdWxData($data,$mid){
        $sql = " select  wx_name as wx_alias,id  from  sline_community_white_list_wx   where mid='{$mid}'  and status=0   order by order_by asc";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }


    public static  function  isBandWxData($wxids,$mid){
        $sql = " select wx_alias,id  from  sline_community_white_list_wx   where mid='{$mid}'   and id in({$wxids})  and status=0 and  bind_mid>0  order by id desc";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }

    public static function  isBandWxDataMid($wxids,$mid,$myMid){
        $sql = " select  group_concat(id) as bind_ids  from  sline_community_white_list_wx   where  mid={$mid} and  status=0 and   bind_mid={$myMid} ";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->bind_ids :'';
    }


    public static function getNoUserData($param, $mid)
    {
        $where = '';
        if (!empty($param["name"])) {
            $where = " and scu.name like '%" . $param["name"] . "%'";
        }
        $sql = " select   scu.id,scu.mid,scu.code_number,scu.mobile,scu.name,scur.role_name,scu.created_time,scgm.group_name   from  sline_community_user   as scu";
        $sql .= " left join sline_community_user_role_front as scur on scu.front_role_id=scur.role_id";
        $sql .= " left join sline_community_group_staff_manage  as scgs  on scgs.bind_mid=scu.mid";
        $sql .= " left join sline_community_group_manage  as scgm  on scgs.group_id=scgm.id";

        $sql .= " where scu.sup_id='{$mid}'  and  scu.delete_at=0  {$where} order by scu.id desc limit {$param['offset']},{$param['display']}";
        $result = self::newDb()->select($sql);
        return !empty($result) ? $result : [];
    }

    public static function getUserDataCount($param, $mid)
    {
        $where = '';
        if (!empty($param["name"])) {
            $where = " and scu.name like '%" . $param["name"] . "%'";
        }
        $sql = " select   count(1) as count_num  from  sline_community_user   as scu
                  where scu.sup_id='{$mid}'  and  scu.delete_at=0   {$where} ";
        $result = self::newDb()->select($sql);
        return !empty($result) ? $result[0]->count_num :0;
    }


    public static  function  getBindWxDataByMid($mid){
        $sql = " select count(1) as count_num  from  sline_community_white_list_wx   where bind_mid='{$mid}'  and status=0";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->count_num : 0;
    }

    public static function updateUserDataByid ($table,$where,$updata){
        return DB::table($table)->where('mid',$where['mid'])->where('id',$where['id'])->update($updata);
    }


    public static function updateRobotbindMidUpdate($table,$data,$updata){
        return DB::table($table)->where('mid',$data['mid'])->whereIn('id',array_values($data['updateIds']))->update($updata);
    }

    public static  function  getBindWxDataByMidData($mid){
        $sql = " select count(1) as count_num  from  sline_community_white_list_wx   where  mid='{$mid}'  and status=0";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result[0]->count_num : 0;
    }


   public  static  function  updateWhiteBindMidNum(){
       $sql = " select  wxid  from  sline_community_white_list_wx ";
       $result = self::newDb() -> select($sql);
        foreach($result as $v){
            $sqlcount =    "select count(*) as count_num from ( 
                  select count(1),mid,wxid   from sline_community_member_info_by_appid 
                  where wxid='{$v->wxid}' and mid>0 group by mid) as p" ;
            $resultcount = self::newDb() -> selectOne($sqlcount);
            DB::table('white_list_wx')->where('wxid',$v->wxid)->update(['bind_mid_num'=>$resultcount->count_num]);
        }
   }

    /**
     * 获取所有的微信
     * @return array|int
     */
    public static  function  getBindWxData(){
        $sql = " select wxid  from  sline_community_white_list_wx" ;
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : 0;
    }

    public static function getSendDataOld()
    {
        $sql = " select random_str,id   from  sline_community_send_info where created_at>'2020-05-19 19:16:51'  and is_redis=1 group by random_str" ;
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : 0;
    }


    public static function getRobotInfoByWhere($where)
    {
        $memberInfo = self::newDb()->table('white_list_wx')
            ->where($where)->first();
        return $memberInfo ? json_decode(json_encode($memberInfo), true) : [];
    }

    /**
     *  获取查看信息
     * @param $order_sn
     * @param $mid
     * @return array
     */
    public static function getRoomIdByMid($mid,$source){
        $sql = "SELECT  scm.id  FROM  sline_community_member_info_by_appid   as scm
                left join sline_community_white_list_wx as scw on scm.wxid=scw.wxid     
            where  scm.mid={$mid}  and  scw.platform_identity='{$source}'  order by  scm.id  desc limit 1";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     *  获取查看信息
     * @param $order_sn
     * @param $mid
     * @return array
     */
    public static function getMidByRoomId($roomId,$source){
        $sql = "SELECT  scm.mid  FROM  sline_community_member_info_by_appid   as scm
                left join sline_community_white_list_wx as scw on scm.wxid=scw.wxid     
            where  scm.id={$roomId}  and  scw.platform_identity='{$source}'  order by  scm.id  desc limit 1";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * 悦呗获取悦淘专属机器人列表
     * @param $param
     * @return array
     */
    public static function getRobotSendGoodList($param=[], $page = 1, $pageSize = 10){
        $begin = ($page-1)*$pageSize;
        $where = self::getRobotSendGoodListWhere($param);
        $sql  = "select id,product_id,sku_id,product_name,product_price,product_type,product_vipPrice,";
        $sql .= "commission_rate,goods_sort,small_goods_img,goods_status,create_time";
        $sql .= " from `sline_community_autosend_cps` WHERE {$where} ";
        $sql .= "  order by id desc  limit $begin,$pageSize";
        return DB::select($sql);
    }

    public static function getRobotSendGoodListWhere($param)
    {
        $where='deleted_at = 0 ';
        if(isset($param['platform_identity']) && !empty($param['platform_identity']))
        {
            $where = $where ? $where . ' and ' : $where;
            $where.="platform_identity = '{$param['platform_identity']}'";
        }
        if(isset($param['goods_status']) && !empty($param['goods_status']))
        {
            $where = $where ? $where . ' and ' : $where;
            $where.="goods_status = {$param['goods_status']}";
        }
        return $where;
    }

    /**
     * 统计数据数量
     */
    public static function getRobotSendGoodListCount($param)
    {
        $where = self::getRobotSendGoodListWhere($param);
        $sql  = "select count(1) as cnt from `sline_community_autosend_cps`";
        $sql .= " WHERE {$where}";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->cnt : 0;
    }


    /**
     * 悦呗获取悦淘专属机器人列表
     * @param $param
     * @return array
     */
    public static function getRoomIdsByGroupAdmin($param=[]){
        //$param['platform_identity']
        $sql = "SELECT  id  FROM  sline_community_member_info_by_appid as g";
        $sql .= " WHERE g.mid = {$param['mid']} AND `is_delete` = 0 AND g.wxid in";
        $sql .= " (SELECT wxid FROM `sline_community_white_list_wx`";
        $sql .= " WHERE `platform_identity` = '{$param['platform_identity']}' AND `deleted_at` = 0)";
        $result = self::newDb() -> select($sql);
        return !empty($result) ? json_decode(json_encode($result), true) : [];
    }

    /**
         * 悦呗获取悦淘专属机器人列表
         * @param $param
         * @return array
         */
    public static function getExclusiveRobotList($param=[]){

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $where  = self::getExclusiveRobotListWhere($param);

        $orderBy  = self::getExclusiveGroupOrderByV1($param);

        $sql  = "SELECT m.mobile,m.truename,w.wxid,w.wx_name,w.created_at,count(distinct(g.id)) as group_num,sum(distinct(g.member_count)) as member_count,count(o.id) as order_num,sum(o.actual_price) as order_amount";

        $sql .=" FROM sline_community_white_list_wx w";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on g.wxid = w.wxid";

        $sql .=" LEFT JOIN sline_community_mall_order_goods o on o.room_id = g.id";

        $sql .=" LEFT JOIN yuelvhui.sline_member m on m.mid = w.exclusive_mid";

        $sql .=" where w.status = 0 and w.wxid_type=3 {$where} group by w.id {$orderBy} limit $page,$pageSize";

        $result = self::newDb() -> select($sql);

        return !empty($result) ? json_decode(json_encode($result), true) : [];

    }


    /**
     * 悦呗获取悦淘专属机器人列表总数
     * @param $param
     * @return array
     */
    public static function getExclusiveRobotCount($param=[]){

        $where  = self::getExclusiveRobotListWhere($param);

        $sql  = "SELECT count(*) as count_1";

        $sql .=" FROM sline_community_white_list_wx w";

        $sql .=" LEFT JOIN yuelvhui.sline_member m on m.mid = w.exclusive_mid";

        $sql .=" where w.status = 0 and w.wxid_type=3 {$where}";

        $result = self::newDb() -> selectOne($sql);

        return !empty($result->count_1) ? $result->count_1 : 0 ;

    }



    /**
     * 悦呗获取悦淘专属机器人列表总数
     * @param $param
     * @return array
     */
    public static function getExclusiveRobotDataCount($param=[]){

        $where  = self::getExclusiveRobotListWhere($param);

        $sql  = "SELECT count(distinct(g.id)) as group_num,sum(distinct(g.member_count)) as member_count,count(o.id) as order_num,sum(o.actual_price) as order_amount";

        $sql .=" FROM sline_community_white_list_wx w ";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on g.wxid = w.wxid";

        $sql .=" LEFT JOIN sline_community_mall_order_goods o on o.room_id = g.id";

        $sql .=" LEFT JOIN yuelvhui.sline_member m on m.mid = w.exclusive_mid";

        $sql .=" where w.status = 0 and w.wxid_type=3 and g.is_delete = 0  {$where}";

        $result = self::newDb() -> selectOne($sql);

        return !empty($result) ? json_decode(json_encode($result), true) : [];

    }



    public static function getExclusiveRobotListWhere($param=[]){

        $where = "";

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .=" and w.platform_identity = '".$param['platform_identity']."'";
        }

        if(isset($param['mobile'])&&!empty($param['mobile'])){

            $where .= " and m.mobile='".$param['mobile']."' or m.truename='".$param['mobile']."'";
        }

        if(isset($param['robot_id'])&&!empty($param['robot_id'])){

            $where .= " and w.id=".$param['robot_id'];
        }

        if(isset($param['startTime'])&&!empty($param['startTime'])){

            $where .=" and w.created_at>'".date("Y-m-d H:i:s",$param['startTime'])."'";
        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .=" and w.created_at<'".date("Y-m-d H:i:s",$param['endTime'])."'";
        }

        return $where;
    }


    public static function getExclusiveRobotListForSelect($param=[]){


        $where = "";

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .=" and w.platform_identity = '".$param['platform_identity']."'";
        }


        $sql  = "SELECT wx_name,id as robot_id";

        $sql .=" FROM sline_community_white_list_wx w ";

        $sql .=" where w.status = 0 and w.wxid_type=3 {$where} ";

        $result = self::newDb() -> select($sql);

        return !empty($result) ? json_decode(json_encode($result), true) : [];

    }


    public static function getOrderAmountByMid($param=[]){

        $where = self::getGroupNumByMidWhere($param,1);

        $sql  = "SELECT sum(o.actual_price) as amount ";

        $sql .=" FROM sline_community_mall_order_goods o";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on o.room_id = g.id";

        $sql .=" LEFT JOIN sline_community_white_list_wx w on w.wxid=g.wxid";

        $sql .=" where w.status=0 and  g.is_delete = 0 {$where}";

        $result = self::newDb() -> selectOne($sql);

        return empty($result->amount)?0:$result->amount;
    }


    public static function getGroupNumByMid($param=[]){

        $where = self::getGroupNumByMidWhere($param,2);

        $sql  = "SELECT  count(g.id) as groupNum ";

        $sql .=" FROM sline_community_member_info_by_appid g";

        $sql .=" LEFT JOIN sline_community_white_list_wx w on w.wxid=g.wxid";

        $sql .=" where w.status=0 and  g.is_delete = 0 {$where}";

        $result = self::newDb() -> selectOne($sql);

        return empty($result->groupNum)?0:$result->groupNum;
    }


    public static function getRobotNumByMid($param=[]){

        $where = "";

        if(isset($param['mid'])&&$param['mid']!=null){

            $where .=" and w.exclusive_mid = ".$param['mid'];
        }

        if(isset($param['startTime'])&&!empty($param['startTime'])){

            $where .=" and w.created_at>'".date("Y-m-d H:i:s",$param['startTime'])."'";
        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .=" and w.created_at<'".date("Y-m-d H:i:s",$param['endTime'])."'";
        }


        $sql  = "SELECT  count(w.id) as robotNum ";

        $sql .=" FROM sline_community_white_list_wx w";

        $sql .=" where w.status=0 and w.wxid_type=3 {$where}";

        $result = self::newDb() -> selectOne($sql);

        return empty($result->robotNum)?0:$result->robotNum;
    }


    public static function getGroupNumByMidWhere($param,$type=1){

        $where = "";

        if(isset($param['mid'])&&$param['mid']!=null){

            $where .=" and g.mid = ".$param['mid'];
        }


        if(isset($param['startTime'])&&!empty($param['startTime'])){

            if($type==1){

                $where .=" and o.pay_time>".$param['startTime'];

            }else{

                $where .=" and g.created_at>'".date("Y-m-d H:i:s",$param['startTime'])."'";
            }
        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

            if($type==1){

                $where .=" and o.pay_time<".$param['endTime'];

            }else{
                
                $where .=" and g.created_at<'".date("Y-m-d H:i:s",$param['endTime'])."'";
            }
        }

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){


            $where .=" and w.platform_identity = '".$param['platform_identity']."'";
        }

        return $where;

    }


    public static function getExclusiveGroupList($param){

        $page = ($param['page']-1)*$param['pageSize'];
        $pageSize = $param['pageSize'];

        $where = self::getExclusiveGroupWhere($param);
        $orderBy = self::getExclusiveGroupOrderBy($param);

        $sql  = " SELECT w.wx_name,g.nick_name,g.mobile,g.created_at as created_time,g.member_count,g.name as group_name,count(o.id) as order_num,sum(o.actual_price) as order_amount";

        $sql .=" FROM sline_community_member_info_by_appid g ";

        $sql .=" LEFT JOIN sline_community_mall_order_goods o on o.room_id = g.id";

        $sql .=" LEFT JOIN sline_community_white_list_wx w on w.wxid = g.wxid";

        $sql .=" where g.is_delete = 0 and w.status = 0  {$where}  group by g.id {$orderBy} limit $page,$pageSize";


        $result = self::newDb() -> select($sql);

        return !empty($result) ? json_decode(json_encode($result), true) : [];

    }


    public static function getExclusiveGroupWhere($param){

        $where = "";


        if(isset($param['mobile'])&&$param['mobile']!=null){

            $where .=" and g.mobile = ".$param['mobile'];
        }

        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

            $where .=" and w.platform_identity = '".$param['platform_identity']."'";
        }


        if(isset($param['startTime'])&&!empty($param['startTime'])){

            $where .=" and g.created_at>'".date("Y-m-d H:i:s",$param['startTime'])."'";
        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .=" and g.created_at<'".date("Y-m-d H:i:s",$param['endTime'])."'";
        }

        return $where;

    }


    public static function getExclusiveGroupOrderBy($param){

        $by = " created_time";

        $order = " desc";

        if(isset($param['orderType'])&&$param['orderType']==2){

            $order = " asc";
        }

        $orderByNeed = ['created_time','group_num','order_num','order_amount'];

        if(isset($param['orderBy'])&&in_array($param['orderBy'],$orderByNeed)){

            $by = " ".$param['orderBy'];
        }

        return " order by ".$by.$order;

    }


    public static function getExclusiveGroupOrderByV1($param){

        $by = " created_at";

        $order = " desc";

        if(isset($param['orderType'])&&$param['orderType']==2){

            $order = " asc";
        }

        $orderByNeed = ['created_at','group_num','order_num','order_amount'];

        if(isset($param['orderBy'])&&in_array($param['orderBy'],$orderByNeed)){

            $by = " ".$param['orderBy'];
        }

        return " order by ".$by.$order;

    }



    public static function getExclusiveGroupCount($param){

        $where = self::getExclusiveGroupWhere($param);

        $sql  = " SELECT count(o.id) as order_num,sum(o.actual_price) as order_amount";

        $sql .=" FROM sline_community_mall_order_goods o ";

        $sql .=" LEFT JOIN sline_community_member_info_by_appid g on o.room_id = g.id";

        $sql .=" LEFT JOIN sline_community_white_list_wx w on w.wxid = g.wxid";

        $sql .=" where g.is_delete = 0 and w.status = 0 {$where}";

        $result = self::newDb() -> selectOne($sql);

        return !empty($result) ? json_decode(json_encode($result), true) : [];

    }


    public static function getExclusiveGroupData($param){

        $where = self::getExclusiveGroupWhere($param);

        $sql  = " SELECT count(g.id) as group_num,sum(g.member_count) as member_count";

        $sql .=" FROM sline_community_member_info_by_appid g ";

        $sql .=" LEFT JOIN sline_community_white_list_wx w on w.wxid = g.wxid";

        $sql .=" where g.is_delete = 0 and w.status = 0 {$where}";

        $result = self::newDb() -> selectOne($sql);

        return !empty($result) ? json_decode(json_encode($result), true) : [];

    }

    /**
     * 获取商品分享点击率
     *
     * @param $goods_id
     * @param $good_type
     * @param $channel
     * @return int
     */
    public static function getClickNum($goods_id, $good_type, $channel)
    {
        if (empty($goods_id)) {
            return 0;
        }
        $sql = 'select sum(count_num) as count from sline_community_goods_browse count where goods_type =' . $good_type . ' and channel = ' . $channel . ' and goods_id = ' . $goods_id;
        $data = DB::selectOne($sql);
        return !isset($data->count) ? 0 : $data->count;
    }

}