<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/6/7
 * Time: 11:39
 */

namespace App\Model\RobotOrder;


class MondodbModel
{
    #mongo测试方法
     #查询
    public  static  function mongoList(){
        $model = \DB::connection('db_mongodb')->collection('test'); #链接mongodb的test集合
        $res = $model->where('name', 'root1')->get(); #获取集合所有文档
        return $res;

    }
   #添加
    public  static function mongoAdd(){
        $model = \DB::connection('db_mongodb')->collection('test'); #链接mongodb的test集合
        $data = ['name'=>'root1','age'=>18,'status'=>0];
        $res = $model->insert($data); #插入集合文档，数组形式传递参数
         return $res;
    }
    #编辑
    public static function mongoEdit(){
        $model = \DB::connection('db_mongodb')->collection('test'); #链接mongodb的test集合
        $data = ['name'=>'root','age'=>18,'status'=>1];
        $where = ['name'=>'root'];
        $res = $model->update($data,$where); #更新集合文档，数组形式传递参数
       return $res;
    }
    #删除
    public static function mongoDel(){
        $model = \DB::connection('db_mongodb')->collection('test'); #链接mongodb的test集合
        $res = $model->where(['name'=>'rad'])->delete(); #根据条件删除文档
         return $res;
    }

}