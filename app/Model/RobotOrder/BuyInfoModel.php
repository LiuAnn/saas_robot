<?php

namespace App\Model\RobotOrder;

use Illuminate\Database\Eloquent\Model;

class BuyInfoModel extends Model
{
    protected $table = 'buy_info';

    protected $connection = 'robot';

    protected $primaryKey = 'id';

    /**
     * 根据单号获取详情
     * @param $orderSn
     * @return mixed
     * @author  liujiaheng
     * @created 2020/4/17 4:37 上午
     */
    public static function queryInfoByOrderSn($orderSn)
    {
        return self::where(['ordersn' => $orderSn])->first();
    }

}
