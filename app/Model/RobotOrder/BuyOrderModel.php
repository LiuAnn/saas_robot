<?php

namespace App\Model\RobotOrder;

use Illuminate\Database\Eloquent\Model;

class BuyOrderModel extends Model
{
    protected $table = 'buy_order';

    protected $connection = 'robot';

    protected $primaryKey = 'order_id';

    /**
     * 根据单号获取详情
     * @param $orderSn
     * @return mixed
     * @author  liujiaheng
     * @created 2020/4/17 4:37 上午
     */
    public static function queryInfoByOrderSn($orderSn)
    {
        return self::where(['order_sn' => $orderSn])->first();
    }

}
