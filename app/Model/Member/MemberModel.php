<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2018/12/18
 * Time: 下午3:28
 */

namespace App\Model\Member;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MemberModel extends Model {

    /**
     * 用户列表
     * @param Array
     * @return Array
     */
    public static function getList($param) {
        $where = self::getWhere($param);

        $sql = "select m.mid, m.mobile, m.nickname, m.truename, m.card_date, m.card_type, m.jointime,"
            . "m.money, m.balance, m.cash,"
            . "m.occupied_amount, m.rollouted_occupied_amount, m2.mobile pMobile, m2.truename pName,"
            . "s.total, s.available, s.consum, s.expire, m.sex "
            . "from sline_member m left join sline_member m2 on m2.mid = m.parent_id "
            . "left join sline_integral_statis s on m.mid = s.mid "
            . "where m.mobile is not null {$where} order by m.mid desc limit ?, ?";
        $result = DB::select($sql, [$param["offset"], $param["display"]]);
        return $result;
    }

    /**
     * 获取用户总数
     * @param Array
     * @return Array
     */
    public static function getListCount($param) {
        $where = self::getWhere($param);

        $where = empty($where) ? '' : "where  m.mobile is not null {$where}";
        $sql = "select count(m.mid) count "
            . "from sline_member m $where";
        $result = DB::select($sql, [$param["offset"], $param["display"]]);
        return !empty($result) ? $result[0]->count : 0;
    }

    /**
     * 获取用户信息详情
     * @param Array
     * @return Array
     */
    public static function getDetail($memberId) {
        $sql = "select m.mid, m.mobile, m.nickname, m.truename, m.card_date, m.card_type, m.jointime,"
            . "m.occupied_amount, m.rollouted_occupied_amount, m.cardid_identify cardid, "
            . "m.province, m.city, m.district, m.address, m.sex, "
            . "m2.mobile pMobile, m2.truename pName,"
            . "s.total, s.available, s.consum, s.expire "
            . "from sline_member m left join sline_member m2 on m2.mid = m.parent_id "
            . "left join sline_integral_statis s on m.mid = s.mid "
            . "where m.mid = ?";
        $result = DB::select($sql,[$memberId]);
        return !empty($result) ? $result[0] : [];
    }

    /**
     * @Notes: 获取sline_member_ship中的关系path
     * @Date:2019/12/26 16:01
     * @param $memberId
     * @return array|mixed
     */
    public static function getRelationsPath($memberId) {
        $sql = "select path from sline_member_ship where mid = ? ";
        $result = DB::selectOne($sql,[$memberId]);
        return !empty($result) ? $result : [];
    }


    /**
     * @Notes: 获取sline_member_work中的关系
     * @Date:2019/12/26 16:01
     * @param $memberPath
     * @return array|mixed
     */
    public static function getRelationList($memberPath) {
        $sql = "select mobile,truename,stock_number,first_number,type,first_number"
            . " from sline_member_work "
            . "where mid in({$memberPath}) order by field(mid,{$memberPath})";
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * @Notes: 获取sline_member_bonus中的sline_member_work
     * @Date:2019/12/26 16:01
     * @param $memberId
     * @return array|mixed
     */
    public static function getBrokerage($memberId) {
        $sql = "select mb.amount,mb.identity_type,mw.type,mw.mobile,mw.truename,mw.stock_number,mw.first_number "
            ." from sline_member_bonus mb left join sline_member_work mw on mb.mid = mw.mid "
            ." where  mb.type = 1 and mb.fid = ? ";
        $result = DB::select($sql,[$memberId]);
        return !empty($result) ? $result : [];
    }
    /**
     * 获取用户的优惠券信息
     * @param Array
     * @return Array
     */
    public static function getMemberCoupon($memberId) {
        $sql = "select mc.cid, c.name, c.amount, count(mc.cid) total, IFNULL(mc3.unnum, 0) unnum, IFNULL(mc2.usenum, 0) usenum, end_dates "
            . "from sline_member_coupon mc "
            . "left join sline_coupon c on c.id = mc.cid "
            . "left join (select cid, count(cid) usenum from sline_member_coupon where mid = ? and usenum = 1 group by cid) mc2 on mc2.cid = mc.cid "
            . "left join (select cid, count(cid) unnum from sline_member_coupon where mid = ? and usenum = 0 group by cid) mc3 on mc3.cid = mc.cid "
            . "where mc.mid = ? group by mc.cid";
        $result = DB::select($sql, [$memberId, $memberId, $memberId]);
        return !empty($result) ? $result : [];
    }

    /**
     * 用户的酒店订单
     * @param Array
     * @return Array
     */
    public static function getHotelOrderListByMemberid($memberId) {
        $sql = "select ho.ordersn, payFormNo, hotel_name, price, (sale_price * sale_count) sale,"
            . "mo.cmoney, (actual_pay / 100) act, arrival_date, paytime, hotel_id "
            . "from sline_hotel_order ho left join sline_member_order_coupon mo on ho.ordersn = mo.ordersn "
            . "where pay_status = 5 and mid = ?";
        $result = DB::select($sql, [$memberId]);
        return !empty($result) ? $result : [];
    }

    /**
     * 获取用户所属分公司
     * @param int
     * @return Array
     */
    public static function getCompanyByMemeberId($memberId) {
        $sql = "select y.companyName, y.companyPeople from sline_member m "
            . "left join ylh_company.sline_company_message y on m.company_id = y.company_id "
            . "where m.mid = ?";
        $result = DB::select($sql, [$memberId]);
        return !empty($result) ? $result[0] : [];
    }

    /**
     * 根据日期获取日常用户数据统计
     * @param int
     * @param int
     * @return obj | []
     */
    public static function getMemberDataByDate($offset, $display) {
        $sql = "select date_format(from_unixtime(jointime), '%Y-%m-%d') date1, count(*) count, ifnull(m2.count, 0) cCount,"
            . "ifnull(m3.count, 0) vCount, ifnull(m4.count, 0) yCount from sline_member m left join ( "
            . "select date_format(from_unixtime(card_date), '%Y-%m-%d') date2, count(*) count from sline_member m2 group by date2 order by date2 desc limit {$offset}, {$display}) m2 on m2.date2 = date_format(from_unixtime(jointime), '%Y-%m-%d') "
            . "left join (select date_format(from_unixtime(jointime), '%Y-%m-%d') date3, count(*) count from sline_member m3 where `virtual` = 2 group by date3 order by date3 desc limit {$offset}, {$display}"
            . ") m3 on m3.date3 = date_format(from_unixtime(jointime), '%Y-%m-%d') left join ( "
            . "select date_format(from_unixtime(jointime), '%Y-%m-%d') date4, count(*) count from sline_member m4 where `virtual` = 1 group by date4 order by date4 desc limit {$offset}, {$display} "
            . ") m4 on m4.date4 = date_format(from_unixtime(jointime), '%Y-%m-%d') "
            . "group by date1 order by date1 desc limit {$offset}, {$display}";
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * 奖金列表的数据
     * @param Array
     * @return int
     */
    public static function getBonusListCount($param) {
        $where  = self::getWhere($param);
        $sql    = "select count(*) count from sline_member m where mobile is not null {$where}";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count : 0;
    }

    /**
     * 用户的奖金列表
     */
    public static function getBonusList($param, $offset, $display) {
        $where = self::getWhere($param);
        $sql = "select mid, mobile, nickname, truename, money, balance, cash from sline_member m "
            . "where mobile is not null {$where} order by mid desc limit {$offset}, {$display}";
        return DB::select($sql);
    }

    /**
     * 提现申请数量
     */
    public static function getCashListCount() {
        $sql    = "select count(*) count from sline_member_cash";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count : 0;
    }

    /**
     * 提现申请
     */
    public static function getCashList($offset, $display) {
        $sql = 'select c.mid, billNo, m.truename, c.mobile, cardid, m.money, '
            . 'bonus.amount bMount, c.amount, is_identify_check, createTime from sline_member_cash c '
            . 'left join sline_member m on m.mid = c.mid left join (select mid, sum(amount) amount '
            . 'from sline_member_bonus group by mid) bonus on bonus.mid = c.mid order by id desc limit ?, ?';
        $result = DB::select($sql, [$offset, $display]);
        return $result;
    }

    /**
     * 根据某个日期获取线上开卡量
     * 只要统计线上支付的购卡订单，就可以统计出某个如期下的线上开卡量
     */
    public static function getOlineCardCount($date) {
        $data_start = $date.' 00:00:00';
        $data_end   = $date.' 23:59:59';
        $sql    = "select count(*) as count from sline_user_entity_card where is_online = 1 and  bindtime <= '" . $data_end . "' and bindtime >= '" .$data_start."'";
        $result = DB::select($sql);
        return $result[0]->count;
    }

    /**
     * 根据某个日期获取线上开卡量
     * 只要统计线上支付的购卡订单，就可以统计出某个如期下的线上开卡量
     */
    public static function getDownCardCount($date) {
        $data_start = $date.' 00:00:00';
        $data_end   = $date.' 23:59:59';
        $sql    = "select count(*) as count from sline_user_entity_card where is_online = 2 and  bindtime <= '" . $data_end . "' and bindtime >= '" .$data_start."'";
        $result = DB::select($sql);
        return $result[0]->count;
    }

    /**
     * 获取where条件
     */
    private static function getWhere($param) {
        $where = "";
        /**
         * 注册开始时间
         */
        if(!empty($param["startTime"])) {
            $where .= " and m.jointime >= " . strtotime($param["startTime"]);
        }

        /**
         * 注册结束时间
         */
        if(!empty($param["endTime"])) {
            $where .= " and m.jointime <= " . strtotime($param["endTime"]);
        }

        /**
         * 按照手机号
         */
        if(!empty($param["keyWord"])) {
            $keyWord = strip_tags($param["keyWord"]);
            $where .= " and (m.mobile like '{$keyWord}%' or m.nickName like '{$keyWord}%' or m.truename like '{$keyWord}%' )";
        }

        if(isset($param["cardType"])) {
            $where .= " and m.card_type = "."{$param['cardType']}";
        }

        if(isset($param["cardTypeNum"])) {
            $where .= " and m.card_type = "."{$param['cardTypeNum']}";
        }

        if(isset($param["mid"])) {
            $where .= " and m.mid = "."{$param['mid']}";
        }


        return $where;
    }

    /**
     * 获取where 条件后面的and
     */
    private static function getWhereAnd($string) {
        if(!empty($string)) {
            return " and ";
        }

        return "";
    }

    public static function getWhereListCount($param = [])
    {
        $whereOne='';

        if (isset($param['startTime'])) {
            $whereOne .= ' and '.'unix_timestamp(bindtime) >= ' . strtotime($param['startTime']);
        }

        if (isset($param['endTime'])) {
            $whereOne .= ' and '.'unix_timestamp(bindtime) <= ' . strtotime($param['endTime']);
        }

        $sqlOne = "select count(*) as count from sline_user_entity_card where is_online in (1,2) $whereOne";

        $resultOne = DB::selectOne($sqlOne);


        $cardTypeWhereZone = $param;

        $cardTypeWhereZone['cardTypeNum'] = 0;

        $whereZone = self::getWhere($cardTypeWhereZone);

        $sqlZone = "select count(m.mid) count "
            . "from sline_member m left join sline_member m2 on m2.mid = m.mid "
            . "left join sline_integral_statis s on m.mid = s.mid "
            . "where m.mobile is not null {$whereZone}";
        $resultZone = DB::selectOne($sqlZone);

        $data = [
            'eliteCardNum'=>!empty($resultOne) ? $resultOne->count : 0,
            'noBuyCardNum'=>!empty($resultZone) ? $resultZone->count : 0,
        ];


        return $data;
    }

    /**
     * 会员信息
     * @param Array
     * @return int
     */
    public static function getMermberInfo($param) {
        $where  = self::getWhere($param);
        $sql    = "select * from sline_member m where mobile is not null {$where}";
        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }

    /**
     * 实名认证失败列表
     * @param Array
     * @return Array
     */
    public static function getMemberIdentifyList($param) {
        $where = self::getMemberIdentifyWhere($param);

        $sql = "select m.mid, m.mobile, m.nickname, m.truename,m.cardid,m.cardid_identify,mi.status,mi.id "
                . "from sline_member_identify mi left join sline_member m on mi.mid = m.mid "
                . "where mi.status = 0 {$where} order by m.mid desc limit ?, ?";

        $result = DB::select($sql, [$param["offset"], $param["display"]]);
        return $result;
    }


    /**
     * 实名认证失败总数
     * @param Array
     * @return Array
     */
    public static function getMemberIdentifyListCount($param) {
        $where = self::getMemberIdentifyWhere($param);


        $sql = "select count(*) count "
            . "from sline_member_identify mi left join sline_member m on mi.mid = m.mid "
            . "where mi.status = 0 {$where} ";

        $result = DB::select($sql);
        return !empty($result) ? $result[0]->count : 0;
    }

    /**
     * 获取where条件
     */
    private static function getMemberIdentifyWhere($param) {
        $where = "";

        /**
         * 按照手机号
         */
        if(!empty($param["keyword"])) {
            $keyWord = strip_tags($param["keyword"]);
            $where .= " and (m.mobile like '{$keyWord}%' or m.nickName like '{$keyWord}%' or m.truename like '{$keyWord}%' )";
        }

        if(isset($param["cardType"])) {
            $where .= " and m.card_type = "."{$param['cardType']}";
        }

        if(isset($param["mid"])) {
            $where .= " and mi.mid = "."{$param['mid']}";
        }
        if(isset($param["id"])) {
            $where .= " and mi.id = "."{$param['id']}";
        }

        return $where;
    }

    /**
     * 会员信息
     * @param Array
     * @return int
     */
    public static function getMemberIdentify($param) {
        $where  = self::getMemberIdentifyWhere($param);

        $sql = "select m.mid, m.mobile, m.nickname, m.truename,m.cardid,m.cardid_identify,mi.status,mi.id,mi.front_img,mi.back_img,mi.third_img"
            . " from sline_member_identify mi left join sline_member m on mi.mid = m.mid "
            . " where mi.status = 0 {$where} ";

        $result = DB::selectOne($sql);
        return !empty($result) ? $result : '';
    }


    public static function updateMember($where,$data)
    {
        return DB::table('sline_member')->where($where)->update($data);
    }


    public static function upMemberIdentify($where,$data)
    {
        return DB::table('sline_member_identify')->where($where)->update($data);
    }

    public static function getNewCashListCount($param)
    {
        $where  = self::getNewCashWhere($param);

        $sql = "select count(*) count "
            . "from sline_member_cash mc left join sline_member m on m.mid = mc.mid "
            .'left join (select mid, sum(amount) amount from sline_member_bonus group by mid) bonus on bonus.mid = mc.mid '
            . "where mc.id > 0 {$where} ";
//            . "where mc.status = 2 {$where} ";

        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count : 0;

    }

    public static function getNewCashList($param=[])
    {
        $where  = self::getNewCashWhere($param);

        $sql = "select m.card_type,mc.id,mc.mid,mc.status mcStatus,mc.amount, mc.amount as mcCash,m.money,m.cash,m.balance,mc.mobile, mc.createTime,bonus.amount bMount "
            . "from sline_member_cash mc left join sline_member m on m.mid = mc.mid "
            .'left join (select mid, sum(amount) amount from sline_member_bonus group by mid) bonus on bonus.mid = mc.mid '
            . "where mc.id > 0 {$where} order by mc.id desc limit ?, ?";
//            . "where mc.status = 2 {$where} order by mc.id desc limit ?, ?";

//        $sql = 'select c.mid, billNo, m.truename, c.mobile, cardid, m.money, '
//            . 'bonus.amount bMount, c.amount, is_identify_check, createTime from sline_member_cash c '
//            . 'left join sline_member m on m.mid = c.mid left join (select mid, sum(amount) amount '
//            . 'from sline_member_bonus group by mid) bonus on bonus.mid = c.mid where c.status = 2 order by id desc limit ?, ?';


        $result = DB::select($sql,[$param["offset"], $param["display"]]);

        return !empty($result) ? $result : '';
    }

    public static function getNewCashWhere($param)
    {
        $where = "";

        if (isset($param['startTime'])) {
            $where .= self::getWhereAnd($where) . 'UNIX_TIMESTAMP(mc.createTime) >= ' . strtotime($param['startTime']);
        }

        if (isset($param['endTime'])) {
            $where .= self::getWhereAnd($where) . 'UNIX_TIMESTAMP(mc.createTime) <= ' . strtotime($param['endTime']);
        }


        if (isset($param['mobile'])) {
            $where .= self::getWhereAnd($where) . " (mc.mobile like '{$param["mobile"]}%')";
        }

        if (isset($param['status'])) {
            $where .= self::getWhereAnd($where) . " mc.status = " .$param["status"];
        }


        return !empty($where) ? " and {$where}" : "";
    }

    public static function getCardResult($param=[])
    {
        $sql        = "select * from sline_member_identify_ext where mid = ? limit 1";
        $cardResult = DB::selectOne($sql, [$param['mid']]);

        return !empty($cardResult) ? $cardResult : '';
    }

    public static function getBank($param=[])
    {
        $bankSql    = "select * from sline_member_bankCard_ext where mid = ? limit 1";
        $bankResult = DB::selectOne($bankSql, [$param['mid']]);

        return !empty($bankResult) ? $bankResult : '';
    }

    public static function getMemberInfo($param=[]) {
        $sql    = "select * from yuelvhui.sline_member where mid= ?";
        $result =  DB::selectOne($sql, [$param['mid']]);
        return !empty($result) ? $result : [];
    }

    /**
     * 统计通过拒绝的次数
     * @param array $param
     * @return int
     */
    public static function geReferNumCount($param=[]) {
        $sql    = "select count(1) as count_num  from  sline_community_examine_log  where mid= ? and type=?";
        $result =  DB::select($sql, [$param['mid'],$param['type']]);
        return !empty($result) ? $result[0]->count_num :0;
    }


    /**
     * 获取用户的邀请码
     * @param $memberId
     * @return null
     */
    public static function getInvitationCodeByMember($memberId)
    {
        $sql = "select *  from  yuelvhui.sline_invitation_code where mid = ?";
        $result = DB::select($sql, [$memberId]);
        return !empty($result) ? $result[0]->code_number : null;
    }

    public static function getMemberIdentifyInfo($param=[])
    {
        $where='';
        if (isset($param['statue'])) {
            $where .= " and status = {$param['status']}";
        }

        if (isset($param['mid'])) {
            $where .= " and mid = {$param['mid']}";
        }

        if($where)
        {
            $sql    = "select * from sline_member_identify where id>0 $where";
            $result =  DB::selectOne($sql);
            return !empty($result) ? $result : '';
        }
    }


    public static function getNewCashInfo($param=[])
    {
        $where  = self::getNewCashInfoWhere($param);

        $sql = "select * from sline_member_cash where id > 0 {$where}";

        $result = DB::selectOne($sql);

        return !empty($result) ? $result : '';
    }


    public static function getNewCashInfoWhere($param)
    {
        $where = "";

        if (isset($param['id'])) {
            $where .= self::getWhereAnd($where) . " id =".$param['id'];
        }

        return !empty($where) ? " and {$where}" : "";
    }

    public static function upMemberCash($where,$data)
    {
        return DB::table('sline_member_cash')->where($where)->update($data);
    }

    public static function delIdentifyExt($where)
    {
        return DB::table('sline_member_identify_ext')->where($where)->delete();
    }

    public static function delBankCardExt($where)
    {
        return DB::table('sline_member_bankCard_ext')->where($where)->delete();
    }



    /**
     * 获取下级用户id
     * @param $mid
     * @return \Illuminate\Support\Collection
     */
    public static function getSubordinateIdsByMid($mid, $source)
    {
        if ($source == 1) {
            return DB::connection('mysql_yuelvhui')->table('member')->where('parent_id', $mid)->pluck('mid');
        }
    }


    /**
     * 获取下级用户列表
     * @param $mid
     * @return \Illuminate\Support\Collection
     */
    public static function getLowerMemberList($param, $page, $pageSize = 15)
    {
        $begin = ($page-1)*$pageSize;
        $where = self::getLowerMemberWhere($param);
        $having = self::getLowerMemberHaving($param);
        $sql = "select f.mid,f.nickname,f.wxid,ifnull(cast(sum(o.actual_price)/100 AS decimal(13,2)), 0) as gmv,m.parent_id";
        $sql .= " from ylh_robot.sline_community_robot_friend as f FORCE INDEX (`mid`)";
        $sql .= " left join yuelvhui.sline_member as m";
        $sql .= " on f.mid = m.mid";
        $sql .= " left join ".config('community.oldMallOrderGoods')." as o";
        $sql .= " on f.mid = o.member_id and o.pay_status = 1";
        $sql .= " left join sline_community_white_list_wx as r";
        $sql .= " on r.wxid = f.robot_wxid";
        //TODO 明天关联上下级用户还有gmv搜索
//        $sql .= " left join yuelvhui.sline_member as m";
//        $sql .= " on f.mid = m.mid";
        $sql .= " where {$where} {$having} GROUP BY f.mid limit $begin,$pageSize";
        return DB::select($sql);
    }

    /**
     * 获取下级用户列表
     * @param $mid
     * @return \Illuminate\Support\Collection
     */
    public static function getLowerMemberCount($param)
    {
        $where = self::getLowerMemberWhere($param);
        $having = self::getLowerMemberHaving($param);
        $sql = "select count(distinct(f.mid)) as cnt";
        $sql .= " from ylh_robot.sline_community_robot_friend as f";
        $sql .= " left join yuelvhui.sline_member as m";
        $sql .= " on f.mid = m.mid";
        $sql .= " left join ".config('community.oldMallOrderGoods')." as o";
        $sql .= " on f.mid = o.member_id and o.pay_status = 1";
        $sql .= " left join sline_community_white_list_wx as r";
        $sql .= " on r.wxid = f.robot_wxid";
        $sql .= " where {$where} {$having}";
        return DB::select($sql);
    }


    public static function getLowerMemberWhere($param)
    {
        $where = "1=1";
        if(isset($param['wxid'])&&!empty($param['wxid']))
        {
            $where = $where ? $where . ' and ' : $where;
            $where .="f.robot_wxid='".$param['wxid']."'";
        }
        if(isset($param['upMid'])&&!empty($param['upMid']))
        {
            $where = $where ? $where . ' and ' : $where;
            $where .="m.parent_id='".$param['upMid']."'";
        }
        return $where;
    }


    public static function getLowerMemberHaving($condition)
    {
        $having = 'having ';
        $tail = '';
        $orderGmv = "sum(o.actual_price)/100";
        if (isset($condition['beginGmv']) && isset($condition['endGmv'])) {
            $tail = $tail ? $tail . ' and ' : $tail;
            $tail .= "$orderGmv between {$condition['beginGmv']} and {$condition['endGmv']}";
        } else if (isset($condition['beginGmv']) && empty($condition['endGmv'])) {
            $tail = $tail ? $tail . ' and ' : $tail;
            $tail .= "$orderGmv >= {$condition['beginGmv']}";
        } else if (isset($condition['endGmv']) && empty($condition['beginGmv'])) {
            $tail = $tail ? $tail . ' and ' : $tail;
            $tail .= "$orderGmv <= {$condition['endGmv']}";
        }
        if ($tail) {
            return $having.$tail;
        }
        return '';
    }




    /**
     * 获取下级用户列表
     * @param $mid
     * @return \Illuminate\Support\Collection
     */
    public static function toCreateGroupList($param, $page, $pageSize = 15)
    {
        $begin = ($page-1)*$pageSize;
        $where = self::toCreateGroupWhere($param);
        //$sql = "select f.mid,f.nickname,f.wxid,ifnull(cast(sum(o.actual_price)/100 AS decimal(13,2)), 0) as gmv";
        $sql = "select g.*";
        $sql .= " from";
        $sql .= " sline_community_auto_group_create as g";
        $sql .= " where {$where} order by created_at desc limit $begin,$pageSize";
        return DB::select($sql);
    }

    /**
     * 获取下级用户列表
     * @param $mid
     * @return \Illuminate\Support\Collection
     */
    public static function toCreateGroupCount($param)
    {
        $where = self::toCreateGroupWhere($param);
        //$sql = "select f.mid,f.nickname,f.wxid,ifnull(cast(sum(o.actual_price)/100 AS decimal(13,2)), 0) as gmv";
        $sql = "select count(1) as cnt";
        $sql .= " from";
        $sql .= " sline_community_auto_group_create as g";
        $sql .= " where {$where}";
        return DB::select($sql);
    }

    public static function toCreateGroupWhere($param)
    {
        //机器人已创建不展示
        $where = "g.robot_send_status = 0";
        $where = "";
        if(isset($param['platform_identity'])&&!empty($param['platform_identity'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.platform_identity = '{$param['platform_identity']}'";
        }
        if(isset($param['name'])&&!empty($param['name']))
        {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.name like '%{$param['name']}%'";
        }
        if (isset($condition['beginGmv']) && isset($condition['endGmv'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.GMV between {$param['beginGmv']} and {$param['endGmv']}";
        } else if (isset($param['beginGmv']) && empty($param['endGmv'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.GMV >= {$param['beginGmv']}";
        } else if (isset($param['endGmv']) && empty($param['beginGmv'])) {
            $where = $where ? $where . ' and ' : $where;
            $where .= "g.GMV <= {$param['endGmv']}";
        }
        if(isset($param['groupMid'])&&empty($param['groupMid']))
        {
            $where = $where ? $where . ' and ' : $where;
            $where .="g.mid = '{$param['groupMid']}'";
        }
        return $where;
    }


    public static function updateGroupMemeberInfo($param){

        return DB::table('group_member')->where(['mid'=>$param['mid']])->update($param);

    }


    public static function updateGroupMemeberInfoV1($mid=0,$key='',$money=0,$type){

        if($type==1){

            return DB::table('group_member')->where(['mid'=>$mid])->increment($key,$money);
        }

        if($type==2){

            return DB::table('group_member')->where(['mid'=>$mid])->decrement($key,$money);
        }       

    
    }

    public static function insertMemberInfo($param){

        return DB::table('group_member')->insert($param);

    }


    public static function insertBindgroupRecode($param){

        return DB::table('bind_group_recode')->insert($param);

    }


    public static function getBindgroupInfo($roomid,$mid){

        $result = DB::table("bind_group_recode")->where(['room_id'=>$roomid,'mid'=>$mid])->orderby('id','desc')->first();
        
        return empty($result)?[]:json_decode(json_encode($result),true);       
    }
   

    public static function getLastMemberBonus($mid,$type){

        $result = DB::table("member_bonus")->where(['mid'=>$mid,'group_type'=>$type])->orderby('id','desc')->first();
        
        return empty($result)?[]:$result;
    }


    public static function insertMemberBonus($param){


        return DB::table('member_bonus')->insert($param);
    }


    public static function getGroupMemeberInfo($mid){

        $sql = "select * from sline_community_group_member where mid=$mid";

        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);

    }

    /**
     * 插入用户提现信息
     * @param array $condition
     * @return mixed
     */
    public static function insertMemeberCash($param){


        return DB::table('cash')->insert($param);
    }


    /**
     * 获取用户提现信息
     * @param array $condition
     * @return mixed
     */
    public static function getMemeberCashInfo($billNo){


        return DB::table('cash')->where(['billNo'=>$billNo])->first();
    }


    /**
     * 处理用户提现
     * @param array $condition
     * @return mixed
     */
    public static function updateMemeberCash($param){

        return DB::table('cash')->where(['billNo'=>$param['billNo']])->update($param);
    }


    /**
     * 根据room_id获取群信息
     * @param array $condition
     * @return mixed
     */
    public  static function getGroupData($id)
    {
        $created_at = "2020-09-11 00:00:00";
        $bind_mid_time = "2020-09-11 00:00:00";

        $sql = "select id,mid,member_count from sline_community_member_info_by_appid where id={$id} and created_at>'{$created_at}' and bind_mid_time>'{$bind_mid_time}' ";
    
        $result = DB::selectOne($sql);

        return empty($result)?[]:$result;
    }


    /**
     * 根据room_id获取群信息
     * @param array $condition
     * @return mixed
     */
    public  static function getGroupDataV1($id)
    {
        $created_at = "2020-12-1 00:00:00";
        $bind_mid_time = "2020-12-1 00:00:00";

        $sql = "select id,mid,member_count from sline_community_member_info_by_appid where id={$id} and created_at>'{$created_at}' and bind_mid_time>'{$bind_mid_time}' ";
    
        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }


    /**
     * 查看该群是否发放过奖励
     * @param array $condition
     * @return mixed
     */
    public  static function getRoomBonusInfo($id)
    {  
    
        $sql = "select * from sline_community_bonus_becommitted where room_id={$id} and identity = 7 and status<2";

        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);     
    }


    /**
     * 查看该用户第几次发放建群奖励
     * @param array $condition
     * @return mixed
     */
    public static function getRoomBonusInfoV1($mid)
    {  
    
        $sql = "select * from sline_community_bonus_becommitted where mid={$mid} and identity = 7 and status<2";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);        
    } 


    /**
     * 根据room_id查看该群是否绑定群主没有超过三天
     * @param array $condition
     * @return mixed
     */
    public  static function isRuleGroup($id)
    {
        $bind_mid_time = date("Y-m-d H:i:s",strtotime('-3 days'));

        $sql = "select mid from sline_community_member_info_by_appid where id={$id} and bind_mid_time>'{$bind_mid_time}' and member_count>49 and is_delete=0 ";
    
        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }


    /**
     * 根据room_id获取群信息
     * @param array $condition
     * @return mixed
     */
    public  static function isgRroupMember($mid){

        $sql = "select id from sline_community_member_info_by_appid where mid={$mid} and is_delete = 0 and member_count>49";

        $result = DB::selectOne($sql);

        return empty($result)?[]:$result;
    }



    /**
     * 根据room_id获取群信息
     * @param array $condition
     * @return mixed
     */
    public  static function getGetCashNumber($mid){

        $sql = "SELECT sum(`amount`) as cnt FROM `ylh_robot`.`sline_community_bonus_becommitted` WHERE  `status` = '1' AND mid = {$mid}";

        $result = DB::selectOne($sql);

        return $result->cnt ? $result->cnt / 100 : 0;
    }



    /**
     * 根据room_id获取群信息
     * @param array $condition
     * @return mixed
     */
    public  static function getWithdrawNumber($mid){

        $sql = "SELECT sum(`amount`) as cnt FROM `ylh_robot`.`sline_community_cash` WHERE `status` = 3 AND mid = {$mid}";

        $result = DB::selectOne($sql);

        return $result->cnt ?: 0;
    }


    /**
     * 获取用户群信息
     * @param array $condition
     * @return mixed
     */
    public  static function isGroupMemberV1($mid){

        $sql = "select * from sline_community_member_info_by_appid where mid={$mid} and is_delete = 0 and member_count>49";
    
        $result = DB::selectOne($sql);

        return empty($result)?[]:$result;       
    }


    /**
     * 获取用户群信息
     * @param array $condition
     * @return mixed
     */
    public  static function isGroupMember($mid){

        $sql = "select * from sline_community_member_info_by_appid where mid={$mid} and is_delete = 0";
    
        $result = DB::selectOne($sql);

        return empty($result)?[]:$result;       
    }

    /**
     * 根据mid获取下级有效群主数量
     * @param array $condition
     * @return mixed
     */
    public static function getIvalidLeaderNum($mid,$type=1){

        if($type==1){

            $sql = "select count(*) as count_1 from sline_community_group_member where parent_id={$mid} and group_number>0 and ivalid_group_number>2";

        }else{

            $sql = "select count(*) as count_1 from sline_community_group_member where parent_id={$mid} and group_number>0 ";
        }
        
        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }



    /**
     * 根据mid获取用户详细信息
     * @param array $condition
     * @return mixed
     */
    public static function getMemberInfoByMid($condition = []) {
        $sql = "select    sm.mobile,sm.truename,sm.nickname,sic.code_number,sm.mid,sm.parent_id,sm.litpic  from   yuelvhui.sline_member   as sm  
               left join  yuelvhui.sline_invitation_code   as sic   on sm.mid=sic.mid
               where sm.mid={$condition['mid']} ";
        return DB::connection('robot')->selectOne($sql);

    }

    /**
     * 获取我的下级购买过
     * @param array $condition
     * @return mixed
     */
    public static function getMyUserNumber($mid){

        $jointime = strtotime("2020-12-1");

        $sql = "select member_id from sline_community_mall_order_goods where member_id in(select mid from yuelvhui.sline_member where parent_id={$mid} and jointime>{$jointime}) group by member_id";
        
        $result = DB::select($sql);

        return count($result);
    }

    /**
     * 查看获取过几次最高级别的奖励
     * @param array $condition
     * @return mixed
     */
    public static function getMyUserNumberForTop($mid){


        $sql = "select count(*) as count_1 from sline_community_bonus_becommitted where new_identity=3 and mid=".$mid;
        
        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }

    /**
     * 获取已满三十天的奖金待到账列表
     * @param array $condition
     * @return mixed
     */
    public static function getUserIdentityUpBonusList(){

        $created_at = date("Y-m-d H:i:s",strtotime('-30 days'));

        $sql = "select * from sline_community_bonus_becommitted where  identity=6 and status=0 and channel=1 and created_at<'".$created_at."'";

        $result = DB::select($sql);

        return json_decode(json_encode($result),true);
    }

    /**
     * 获取用户奖金明细
     * @param array $condition
     * @return mixed
     */
    public static function getUserBonusList($mid,$page=1,$pageSize=10){

        $page = ($page-1)*$pageSize;

        $sql = "select * from sline_community_member_bonus where mid=$mid order by id desc limit $page,$pageSize ";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }

    /**
     * 获取用户奖金明细总数
     * @param array $condition
     * @return mixed
     */
    public static function getUserBonusCount($mid){


        $sql = "select count(*) as count_1 from sline_community_member_bonus where mid=$mid";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }


    /**
     * 获取订单详情根据订单号
     * @param array $condition
     * @return mixed
     */
    public static function getOrderInfoByOrdersn($ordersn){

        $created_at = strtotime("2020-10-15 20:00:00");

        $paytime = strtotime("2020-10-15 20:00:00");

        $sql = "select * from sline_community_mall_order_goods where ordersn='{$ordersn}' and pay_time>{$paytime} and created_at>{$created_at}";

        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }

    /**
     * 获取订单详情根据订单号
     * @param array $condition
     * @return mixed
     */
    public static function getOrderInfoByOrdersnV2($ordersn){

        $created_at = strtotime("2020-12-1 20:00:00");

        $paytime = strtotime("2020-12-1 20:00:00");

        $sql = "select * from sline_community_mall_order_goods where ordersn='{$ordersn}' and pay_time>{$paytime} and created_at>{$created_at}";

        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }





    /**
     * 获取订单分佣状态
     * @param array $condition
     * @return mixed
     */
    public static function getBonusBecommittedStatusByOrdersn($ordersn){


        $sql = "select * from sline_community_bonus_becommitted where order_no=$ordersn";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }



    /**
     * 获取商品成长值
     * @param string 商品ID
     * @return mixed
     */
    public static function getProductGrowthValue($id){


        $sql = "select growth_value from ylh_mall.mall_product where id=$id";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->growth_value;
    }

 

    /**
     * 修改分佣状态
     * @param array $condition
     * @return mixed
     */
    public static function updateBonusBecommittedStatus($param){


        return DB::table('bonus_becommitted')->where(['id'=>$param['id']])->update($param);
    }


    /**
     * 获取悦淘7天前未结算佣金订单
     * @param array $condition
     * @return mixed
     */
    public static function getNotReceiveOrder(){

        $created_at = "2020-10-15 20:00:00";

        $where = " and product_type = 1";
        $where.= " and channel = 1";
        $where.= " and created_at>'".$created_at."' ";

        $sql = "select order_no from sline_community_bonus_becommitted where status = 0 $where group by order_no";


        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }


    public static function getYtOrderInfo($order_no){

        $sql = "select receiving_time from yuelvhui.sline_mall_order_goods where ordersn=$order_no";

        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }

    public static function getNotReceiveCpsOrder(){

        $sql = "select ordersn  from sline_community_mall_order_goods where channel = 1 and robot_send = 1 and live_id=0 and  pay_time>1602763200";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }


    /**
     * 添加分佣
     * @param array $condition
     * @return mixed
     */
    public static function insertBonusBecommitted($param){

        return DB::table('bonus_becommitted')->insert($param);
    }


    /**
     * 修改订单状态
     * @param array $condition
     * @return mixed
     */
    public static function updateOrderStatus($param){

       return DB::table('mall_order_goods')->where(['ordersn'=>$param['ordersn']])->update($param);
    }


    /**
     * 获取用户佣金明细
     * @param array $condition
     * @return mixed
     */
    public static function getUserBecommittedList($mid,$page=1,$pageSize=10){

        $page = ($page-1)*$pageSize;

        $sql = "select * from sline_community_bonus_becommitted where mid=$mid order by id desc limit $page,$pageSize ";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }

    /**
     * 获取用户奖金明细总数
     * @param array $condition
     * @return mixed
     */
    public static function getUserBecommittedCount($mid){


        $sql = "select count(*) as count_1 from sline_community_bonus_becommitted where mid=$mid";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
    }


    /**
     * 获取用户收益总条数
     * @param array $condition
     * @return mixed
     */
    public static function getMemeberProfit($param,$identity = 0){

        $where = "mid = {$param['mid']}";

        if(isset($param['startTime'])&&!empty($param['startTime'])){

            $where .= " and created_at>'{$param['startTime']}'";
        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .= " and created_at<'{$param['endTime']}'";
        }

        if(isset($param['status'])){
            $where .= " and status='{$param['status']}'";
        }

        if($identity>0){

            $where .= " and identity=".$identity;
        }

        $sql = "select sum(amount) as total_amount from sline_community_bonus_becommitted where {$where} and status = 0";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->total_amount/100;
    }



    /**
     * 获取用户建群收益总数
     * @param array $condition
     * @return mixed
     */
    public static function getMemeberCreateGroupProfit($param){

        $where = "mid = {$param['mid']}";

        if(isset($param['startTime'])&&!empty($param['startTime'])){

            $where .= " and created_time>'{$param['startTime']}'";
        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

            $where .= " and created_time<'{$param['endTime']}'";
        }

        $sql = "select sum(amount) as total_amount from sline_community_member_bonus where {$where} and type =1";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->total_amount/100;
    }


    /**
     * 获取用户
     * @param array $condition
     * @return mixed
     */
    public static function sendUserFrozenMoney(){

        $created_time =  date("Y-m-d H:i:s",strtotime("-35 days"));

        $sql = "select a.* from sline_community_member_bonus a,(select mid,max(created_time) created_time from sline_community_member_bonus where created_time>'{$created_time}'  group by mid) b where a.mid=b.mid and a.created_time=b.created_time and a.created_time>'{$created_time}'";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }


    public static function getReturnOrderList(){


        $sql = "select order_no from sline_community_bonus_becommitted where identity = 2 and status = 0;";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    }


    /**
     * 群主预估收入
     * @param array $condition
     * @return mixed
     */
    public static function getGrouoMemberMoneyList($param, $page, $pageSize = 10)
    {
        $begin = ($page-1)*$pageSize;

        $order = " groupNum";
        $by = " desc";

        $orderbyList = ['groupNum','bonusAmount','TgOrderCount','TgOrderAmount','ZgOrderCount','cashMoney','drawnMoney','pedingAccountMoney'];

        if(isset($param['order'])&&in_array($param['order'],$orderbyList)){

            $order = $param['order'];
        }

        if(isset($param['by'])){

            $by = $param['by']==1?'desc':'asc';
        }

        $where = "";
        $where2 = "";
        $where3 = "";
        $where4 = "";
        if(isset($param['mobile'])&&$param['mobile']>0){

            $where .= " and g..mobile=".$param['mobile'];
        }

        if(isset($param['beginTime'])&&!empty($param['beginTime'])){

            $where .= " and g.bind_mid_time>'".date("Y-m-d H:i:s",$param['beginTime'])."' and bind_mid_time<'".date("Y-m-d H:i:s",$param['endTime'])."'";
            
            $where2 .= " and p.created_time>'".date("Y-m-d H:i:s",$param['beginTime'])."' and p.created_time<'".date("Y-m-d H:i:s",$param['endTime'])."'";
        
            $where3 .= " and created_at>'".date("Y-m-d H:i:s",$param['beginTime'])."' and created_at<'".date("Y-m-d H:i:s",$param['endTime'])."'";
        
            $where4 .= " and pay_time>".$param['beginTime']." and pay_time<".$param['endTime'];
        }

        $sql = "select m.mid,m.nick_name as nickName,m.group_num as groupNum,sum(p.amount) as bonusAmount,b.TgOrderCount,b1.TgOrderAmount,o.ZgOrderCount,gm.cash_money as cashMoney,gm.drawn_money as drawnMoney,gm.peding_account_money as pedingAccountMoney from ";
        $sql.= " (select g.mid,g.mobile,g.nick_name,count(g.id) as group_num from sline_community_member_info_by_appid  g left join sline_community_white_list_wx w on w.wxid = g.wxid where w.platform_identity ='7323ff1a23f0bf1cda41f690d4089353' and g.is_delete = 0 and g.mid>0 $where group by g.mid) m ";
        $sql.= " LEFT JOIN sline_community_group_member gm on gm.mid = m.mid ";
        $sql.= " left join sline_community_member_bonus  p on p.mid=m.mid and p.type=2 $where2";
        $sql.= " LEFT JOIN (select count(*) as TgOrderCount,mid from sline_community_bonus_becommitted where identity < 4 $where3 group by mid) b ON b.mid = m.mid";
        $sql.= " LEFT JOIN (select sum(amount) as TgOrderAmount,mid from sline_community_bonus_becommitted where identity = 4 $where3 group by mid) b1 ON b1.mid = m.mid ";
        $sql.= " LEFT JOIN (select count(*) as ZgOrderCount,member_id from sline_community_mall_order_goods where channel = 1 $where4 group by member_id) o ON o.member_id = m.mid ";
        $sql.= "  group by m.mid order by $order $by limit $begin,$pageSize";
    
        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
    } 

    /**
     * 群主预估收入
     * @param array $condition
     * @return mixed
     */
    public static function getGrouoMemberMoneyCount($param)
    {

        $where = "";

        if(isset($param['mobile'])&&$param['mobile']>0){

            $where .= " and g.mobile=".$param['mobile'];
        }

        if(isset($param['beginTime'])&&!empty($param['beginTime'])){

            $where .= " and g.bind_mid_time>'".date("Y-m-d H:i:s",$param['beginTime'])."' and g.bind_mid_time<'".date("Y-m-d H:i:s",$param['endTime'])."'";
            
        }

        $sql = "select count(*) as count_1 from(select g.mid,g.mobile,g.nick_name,count(g.id) as group_num from sline_community_member_info_by_appid  g left join sline_community_white_list_wx w on w.wxid = g.wxid where w.platform_identity ='7323ff1a23f0bf1cda41f690d4089353' and g.is_delete = 0 and g.mid>0 $where group by g.mid) m ";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
   
    }

    /**
     * 社群数量
     * @param array $condition
     * @return mixed
     */
    public static function getGrouoTotalCount($param)
    {

        $sql.= "select count(*) as count_1 from sline_community_member_info_by_appid  g left join sline_community_white_list_wx w on w.wxid = g.wxid where w.platform_identity ='7323ff1a23f0bf1cda41f690d4089353' and g.is_delete = 0";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
   
    }

    /**
     * 群主数量
     * @param array $condition
     * @return mixed
     */
    public static function getGrouoMemberTotalCount($param)
    {

        $sql.= "select count(*) as count_1 from(select g.mid,g.mobile,g.nick_name,count(g.id) as group_num from sline_community_member_info_by_appid  g left join sline_community_white_list_wx w on w.wxid = g.wxid where w.platform_identity ='7323ff1a23f0bf1cda41f690d4089353' and g.is_delete = 0 and g.mid>0 group by g.mid) m ";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
   
    }

    /**
     * 站长数量
     * @param array $condition
     * @return mixed
     */
    public static function getGrouoManageTotalCount($param)
    {

        $sql.= "select count(*) as count_1 from sline_community_group_member where type=2 ";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->count_1;
   
    }

    /**
     * 订单量GMV
     * @param array $condition
     * @return mixed
     */
    public static function getGroupOrderCount($param)
    {

        $sql.= "select count(*) as count_1,sum(actual_price)/100 as actual_price from sline_community_mall_order_goods where channel=1 ";

        $result = DB::selectOne($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);
   
    }


    /**
     * 有券订单回滚分佣金额
     * @param array $condition
     * @return mixed
     */    

    public static function callbackOrderBonus(){

        $sql = "select ordersn,goods_coupon_average,goods_id,actual_price,goods_price_buy,goods_num from sline_community_mall_order_goods where ordersn in(select order_no from sline_community_bonus_becommitted where status=0 group by order_no) and goods_coupon_average>0 and pay_time>1601876028 and insert_time<1604646903 and order_three_partner!=4";

        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);       

    }


    public static function getShipByMemberId($mid=0){

        $sql = "select path from yuelvhui.sline_member_ship where mid = $mid";
        $result = DB::selectOne($sql);
        return empty($result)?[]:$result;  
    }


    public static function getAllWork($mids){

        $sql = "select wid, mid from yuelvhui.sline_member_work where mid in($mids) and source = 0  order by field(mid, $mids)";
        $result = DB::select($sql);
        return empty($result)?[]:$result;  
    }    


    /**
     * 查看用户是否为小创
     * @param $mid
     * @return mixed
     */
    public static function isWork($mid = 0)
    {   
        $sql = "select count(*) count_1 from  yuelvhui.sline_member_work where mid = $mid ";

        $result = DB::selectOne($sql);
        return !empty($result) ? $result->count_1 : 0;                
    }


    /**
     * 获取有效群数量
     * @param array $condition
     * @return mixed
     */    

    public static function getIvalidGroupNumber($mid){

        $sql = "select count(*) as count_1 from sline_community_member_info_by_appid  where member_count>80 and bind_mid_time>'2020-09-11 00:00:00' and created_at>'2020-09-11 00:00:00' and mid=$mid ";
        
        $result = DB::selectOne($sql);
        
        return empty($result)?0:$result->count_1;
    }

    /**
     * 根据memberId获取上级用户id
     *
     * @param $member_id
     * @return mixed
     */
    public static function geParentIdByMid($member_id) {
        $sql = "select parent_id from yuelvhui.sline_member where mid={$member_id} ";
        return DB::connection('robot')->selectOne($sql);
    }


    /**
     * 根据memberId获取上级用户id
     *
     * @param $member_id
     * @return mixed
     */
    public static function getZhidingOrderList(){

        $sql = "select id,member_id from sline_community_mall_order_goods where channel=4 and good_type=2 and buyer_phone=0 order by pay_time desc";
        
        $result = DB::select($sql);

        return empty($result)?[]:json_decode(json_encode($result),true);         
    }

    public static function updateZhidingOrderMobile($id,$data){

        return DB::table('mall_order_goods')->where(['id'=>$id])->update($data);
    }
}