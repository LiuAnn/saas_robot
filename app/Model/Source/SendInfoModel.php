<?php

namespace App\Model\Source;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SendInfoModel  extends Model
{
    /**
     * 初始化数据
     * @return \Illuminate\Database\Connection
     */
    public static function newDb()
    {
        return DB::connection('robot');
    }
    /**
     * 获取发消息的群
     * @param $mid
     * @return array
     */
    public static function getMyRoomListByMid($mid)
    {
        $sql    = "select id,name,head_img,member_count  from  sline_community_member_info_by_appid  WHERE is_delete=0 and mid={$mid} order by id desc";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getFodderTypeList($platform_identity)
    {
        $field = "type_id as id, name";
        $where = " and platform_identity = '{$platform_identity}'";
        $sql    = "select {$field} from sline_community_send_info_type WHERE deleted_at=0 {$where}    order by id asc";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getLinkTypeList($platform_identity, $type_id)
    {
        $field = "type_id as id, name";
        $where = " and platform_identity = '{$platform_identity}' and parent_id = {$type_id}";
        $sql    = "select {$field} from sline_community_send_link_type WHERE deleted_at=0 {$where}    order by id asc";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getCommunitySendInfos($field='*',$data=[])
    {
        $where = self::getCommunitySendInfoWhere($data);
        $sql    = "select {$field} from sline_community_send_info WHERE deleted_at=0 {$where}   group by random_str  order by id desc  limit {$data['offset']},{$data['display']}";
        $result =self::newDb() ->select($sql);
        return $result;
    }


    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getCommunitySendInfosV2($field='*',$data=[])
    {
        $where = '';

        $data['sendStartTime'] = isset($data['sendStartTime'])?$data['sendStartTime']:'';
        $data['sendEndTime'] = isset($data['sendEndTime'])?$data['sendEndTime']:'';
        //是否定时  0 未定时  1 定时发送
        if(isset($data['isTimingSend'])&&in_array($data['isTimingSend'],[0,1]))
        {
            $where.=" and is_timing_send='{$data['isTimingSend']}'";
            if(!empty($data['sendStartTime'])) {
                $sendStartTime = strtotime($data['sendStartTime']);
                $where .= " and timing_send_time>{$sendStartTime} ";
            }
            if(!empty($data['sendEndTime'])) {
                $sendEndTime = strtotime($data['sendEndTime']);
                $where .= " and timing_send_time<{$sendEndTime} ";
            }
        }else{
            if(!empty($data['sendStartTime']))
            {
                $where.=" and created_at >'".$data['sendStartTime']."'";
            }

            if(!empty($data['sendEndTime']))
            {
                $where.=" and created_at < '".$data['sendEndTime']."'";
            }
        }
        $where.= " and platform_identity='{$data['platform_identity']}' ";
        if(isset($data['exclusive_mid'])&&$data['exclusive_mid']>0){

            $where.=" and exclusive_mid=".$data['exclusive_mid'];
        }

         //发送状态  0 未发送  1 已发送
        if(isset($data['sendStatus'])&&in_array($data['sendStatus'],[0,1,2]))
        {
            $where.=" and is_send={$param['sendStatus']}";
        }

        $sql    = "select {$field} from sline_community_send_info WHERE deleted_at=0 {$where}   group by random_str  order by id desc  limit {$data['offset']},{$data['display']}";
        $result =self::newDb() ->select($sql);
        return $result;
    }


    /**
     * 统计数据数量
     */
    public static function getCommunitySendInfoCountV2($data)
    {   
         $where = '';

        $data['sendStartTime'] = isset($data['sendStartTime'])?$data['sendStartTime']:'';
        $data['sendEndTime'] = isset($data['sendEndTime'])?$data['sendEndTime']:'';
        //是否定时  0 未定时  1 定时发送
        if(isset($data['isTimingSend'])&&in_array($data['isTimingSend'],[0,1]))
        {
            $where.=" and is_timing_send='{$data['isTimingSend']}'";
            if(!empty($data['sendStartTime'])) {
                $sendStartTime = strtotime($data['sendStartTime']);
                $where .= " and timing_send_time>{$sendStartTime} ";
            }
            if(!empty($data['sendEndTime'])) {
                $sendEndTime = strtotime($data['sendEndTime']);
                $where .= " and timing_send_time<{$sendEndTime} ";
            }
        }else{
            if(!empty($data['sendStartTime']))
            {
                $where.=" and created_at >'".$data['sendStartTime']."'";
            }

            if(!empty($data['sendEndTime']))
            {
                $where.=" and created_at < '".$data['sendEndTime']."'";
            }
        }
        $where.= " and platform_identity='{$data['platform_identity']}' ";
        if(isset($data['exclusive_mid'])&&$data['exclusive_mid']>0){

            $where.=" and exclusive_mid=".$data['exclusive_mid'];
        }

         //发送状态  0 未发送  1 已发送
        if(isset($data['sendStatus'])&&in_array($data['sendStatus'],[0,1,2]))
        {
            $where.=" and is_send={$param['sendStatus']}";
        }

        $sql    = "select count(*) count from (select count(*) count from sline_community_send_info where deleted_at=0 {$where} group by random_str) as e";
        $result =self::newDb() ->select($sql);
        return !empty($result) ? $result[0]->count : 0;
    }

    /**
     * 条件整合
     * @param array $param
     * @return string
     */
    public static function getCommunitySendInfoWhere($param = [])
    {
        $param['sendStartTime'] = isset($param['sendStartTime'])?$param['sendStartTime']:'';
        $param['sendEndTime'] = isset($param['sendEndTime'])?$param['sendEndTime']:'';

        $where='';
        if(isset($param['id'])||!empty($param['id']))
        {
            $where.=" and id = {$param['id']}";
        }
        if(isset($param['randomStr'])||!empty($param['randomStr']))
        {
            $where.=" and random_str = '{$param['randomStr']}'";
        }

        if(isset($param['userId'])&&$param['userId']>0)
        {
            $where.=" and mid=".$param['userId'];
        }else{

            if(isset($param['sup_id'])){
                if($param['sup_id']==0){
                    $where.=" and platform_identity='{$param['platform_identity']}' ";
                }else{
                   $where.=" and (mid={$param['mid']} or mid={$param['sup_id']} ) ";
                }

            }
        }
 
         //发送状态  0 未发送  1 已发送
        if(isset($param['sendStatus'])&&in_array($param['sendStatus'],[0,1,2]))
        {
            $where.=" and is_send={$param['sendStatus']}";
        }
        //是否定时  0 未定时  1 定时发送
        if(isset($param['isTimingSend'])&&in_array($param['isTimingSend'],[0,1]))
        {
            $where.=" and is_timing_send='{$param['isTimingSend']}'";
            if(!empty($param['sendStartTime'])) {
                $sendStartTime = strtotime($param['sendStartTime']);
                $where .= " and timing_send_time>{$sendStartTime} ";
            }
            if(!empty($param['sendEndTime'])) {
                $sendEndTime = strtotime($param['sendEndTime']);
                $where .= " and timing_send_time<{$sendEndTime} ";
            }
        }else{
            if(!empty($param['sendStartTime']))
            {
                $where.=" and created_at >'".$param['sendStartTime']."'";
            }

            if(!empty($param['sendEndTime']))
            {
                $where.=" and created_at < '".$param['sendEndTime']."'";
            }
        }

        return $where;
    }


    public static function getMyUserList($mid=0)
    {
        $sql    = "select name,nickname,mid from sline_community_user where sup_id =$mid and delete_at=0";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 统计数据数量
     */
    public static function getCommunitySendInfoCount($param)
    {
        $where  = self::getCommunitySendInfoWhere($param);
        $sql    = "select count(*) count from (select count(*) count from sline_community_send_info where deleted_at=0 {$where} group by random_str) as e";
         Log::info('素材列表查询sql:'.$sql);
        $result =self::newDb() ->select($sql);
        return !empty($result) ? $result[0]->count : 0;
    }

    /**数据
     * @param array $data
     * @return array
     */
    public static function getCommunitySendInfo2s($data=[])
    {
        $where = self::getCommunitySendInfoWhere($data);
        $sql    = "select * from sline_community_send_info  WHERE deleted_at=0 {$where} order by id desc";
        $result =self::newDb() ->select($sql);
        return $result;
    }


    /**数据
     * @param array $data
     * @return array
     */
    public static function getCommunitySendInfoStr($random_str)
    {

        $sql    = "select *  from sline_community_send_info  WHERE deleted_at=0  and   random_str='{$random_str}' order by id desc";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     *
     * 判断数据
     * @param array $data
     * @return array
     */
    public static function isSetRandomStr($platform_data,$time=0)
    {   
        if($time>0){
            return DB::connection('robot')->table('send_info')->select('*')->distinct('random_str')->where($platform_data)->where('timing_send_time','>=',$time)->count();
        }
        return DB::connection('robot')->table('send_info')->select('*')->distinct('random_str')->where($platform_data)->count();
    }

    /**
     * 添加数据
     * @param $data
     * @return bool
     */
    public static function addAutoSendData($data) {
        return self::newDb()->table("autosend_cps")->insert($data);
    }


    /**
     * 获取
     * @param $data
     * @return bool
     */
    public static function getAutoSendData($where) {
        return self::newDb()->table("autosend_cps")->where($where)->count();
    }

    /**
     * 修改列表商品上下架状态
     * @param $data
     * @return bool
     */
    public static function updateRobotSendGoodStatus($update, $where) {
        return self::newDb()->table('autosend_cps')->where($where)->update($update);
    }

    /**
     * 添加数据
     * @param $data
     * @return bool
     */
    public static function addCommunitySendInfo($data) {
        return self::newDb()->table("send_info")->insert($data);
    }
    /**
     * 更新数据
     * @param $where
     * @param $data
     * @return int
     */
    public static function upCommunitySendInfo($where,$data) {
        return self::newDb()->table("send_info")->where($where)->update($data);
    }

    /**
     * 更新之前查询
     * @param $param
     * @return mixed|string
     */

    public static function getCommunitySendInfo($param)
    {
        $where  = self::getCommunitySendInfoWhere($param);
        $sql    = "select * from sline_community_send_info where deleted_at=0 {$where}";
        $result = self::newDb()->selectOne($sql);
        return !empty($result) ? $result : '';
    }

    /**
     * 根据微信id
     * @param $mid
     */
    public  static function  getWxidByMid($wxid){

        $sql    = "  select *  from  sline_community_member_info_by_appid   where wxid={$wxid} ";
        $result = self::newDb()->selectOne($sql);
        return !empty($result) ? $result : '';
    }

    /**
     *
     */
    public  static function  getIsSendReload($param)
    {
        $sql    = "  select count(1) as countNum from  sline_community_send_reload   where random_str = '{$param['randomStr']}'";
        $result = self::newDb()->selectOne($sql);
        return !empty($result) ? $result->countNum : '';
    }

    public  static function  getIsSendReloadTwo($param)
    {
        $sql    = "  select count(1) as countNum from  sline_community_send_reload  as scs
                      left join   sline_community_white_list_wx as scw  on  scs.wxid=scw.wxid
                     where random_str = '{$param['randomStr']}' and  scw.bind_mid={$param['mid']}";
        $result = self::newDb()->selectOne($sql);
        return !empty($result) ? $result->countNum : '';
    }

    /**
     * @param $condition
     * @return int
     */
     public static function getGroupCountNum($condition)
     {
         $query = DB::connection('robot')->table('member_info_by_appid');
         $arr=[];
         foreach($condition as $v){
             $arr[] = $v;
         }
         if(!empty($arr)){
             $query = $query->whereIn('wxid',$arr );
             $query = $query->where('is_delete',0 );
             return  $query->count();
         }else{
             return  0;
         }

     }

    /**
     * @param $mid
     * @return int
     */
    public static function getGroupCountNumMid($mid)
    {
        $query = DB::connection('robot')->table('member_info_by_appid');
        $query = $query->where('mid','=',$mid );
        return  $query->count();
    }

    /**
     * @param $mid
     * @return int
     */
    public static function getGroupCountByExMid($mid)
    {
        $sql = "select count(*) as count_1 from sline_community_member_info_by_appid g  left join sline_community_white_list_wx w on w.wxid=g.wxid where g.is_delete=0 and w.deleted_at=0 and exclusive_mid={$mid}";
        $result = self::newDb()->selectOne($sql);
        return !empty($result) ? $result->count_1 :0;
    }



    /**
     * @param $randomStr
     */
    public  static function  getSendgGroupNum($randomStr,$condition)
    {
        $query = DB::connection('robot')->table('send_room_reload');
        $query = $query->where('random_str','=',$randomStr );
        $arr=[];
        foreach($condition as $v){
            $arr[] = $v;
        }
        if(!empty($arr)){
            $query = $query->whereIn('wxid',$arr );
            return  $query->count();
        }else{
            return  0;
        }
    }


    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getRobotUserList($data=[])
    {
        $where = self::getRobotUserWhere($data);
        $sql    = "select scr.*,scw.wx_name  from   sline_community_robot_friend  as scr 
                   left join sline_community_white_list_wx  as scw  on scw.wxid=scr.robot_wxid
                   WHERE  scw.deleted_at=0 {$where}  
                   group by  scr.id order by scr.id desc  limit {$data['offset']},{$data['display']}";
        $result =self::newDb() ->select($sql);
        return $result;
    }


    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getFriendRobotList($data=[])
    {
        $where = self::getRobotUserWhere($data);
        $sql    = "select scw.wxid,scw.wx_alias,scw.wx_name from   sline_community_robot_friend  as scr 
                   left join sline_community_white_list_wx  as scw  on scw.wxid=scr.robot_wxid
                   WHERE  scw.deleted_at=0 {$where}  
                   group by  scw.wxid order by scw.order_by  desc  ";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getFriendRobotCount($data=[])
    {
        $where  = self::getRobotUserWhere($data);
        $sql    = "select count(1) as count_num  from   sline_community_robot_friend  as scr 
                   left join sline_community_white_list_wx  as scw  on scw.wxid=scr.robot_wxid
                   WHERE  scw.deleted_at=0 {$where} group by  scw.wxid";
        $result =self::newDb() ->select($sql);
        return !empty($result) ? $result[0]->count_num : 0;
    }

    /**
     * 获取列表
     * @param array $data
     * @return array
     */
    public static function getRobotUserCount($data=[])
    {
        $where  = self::getRobotUserWhere($data);
        $sql    = "select count(1) as count_num  from   sline_community_robot_friend  as scr 
                   left join sline_community_white_list_wx  as scw  on scw.wxid=scr.robot_wxid
                   WHERE  scw.deleted_at=0 {$where} ";
        $result =self::newDb() ->select($sql);
        return !empty($result) ? $result[0]->count_num : 0;
    }
    /**
     * 条件
     * @param array $param
     * @return string
     */
    public static function getRobotUserWhere($param = [])
    {
        $where = ' ';
        if ($param['sup_id'] == 0) {
            $where .= " and scw.mid='{$param['mid']}' ";
        } else {
            $where .= " and scw.bind_mid={$param['mid']}   ";
        }
//        $endTime = !empty($param['sendEndTime'])?  date('Y-m-d H:i:s',strtotime($param['sendEndTime'])+86300):'';
//        //是否有时间筛选
//       if (isset($param['sendStartTime']) && isset($param['sendStartTime'])) {
//            $where .= " and created_at between {$param['sendStartTime']} and  '{$endTime}' ";
//        } else if (isset($param['sendStartTime'])) {
//            $where .= " and created_at >={$param['sendStartTime']}";
//        } else if (isset($param['sendEndTime'])) {
//            $where .= " and created_at <= '{$endTime}' ";
//        }
        if(!empty($param['wxid'])){
            $where .= " and scw.wxid = '{$param['wxid']}'";
        }
        if(!empty($param['nickname'])){
            $where .= " and scr.nickname like '%".$param['nickname']."%' ";
        }
        if(isset($param['sex'])&& in_array($param['sex'],[0,1,2])){
            $where .= " and scr.sex={$param['sex']}";
        }
        if(isset($param['groupNum'])){
            $where .= " and scr.common_group_num={$param['groupNum']}";
        }
        return $where;
    }

    /**
     * 统计共同的群数量
     * @param $wxid
     * @param $robot_wxid
     */
    public  static function  getRobotCommonGroup($wxid,$robot_wxid)
    {
        $sql    = "select count(1) as count_num  from `sline_community_member_weixin_info`  where  room_wxid in 
                  (select room_wxid  from `sline_community_member_weixin_info`   where  wxid=? )  and wxid=? ";
        $result =self::newDb() ->selectOne($sql,[$robot_wxid,$wxid]);
        return !empty($result) ? $result->count_num : 0;
    }

    /**
     * 统计订单数量和金额
     * @param $mid
     * @return int|mixed
     */
    public   static function  orderNumMid($mid){

        $sql = "SELECT count(*) as  order_count,sum(goods_total) as  order_money  from  ".config('community.oldMallOrderGoods')."  where member_id=?  and room_id>0 and pay_status=1 " ;
        $result =self::newDb() ->selectOne($sql,[$mid]);
        return !empty($result) ? $result: 0;
    }

    /**
     * 获取我的群的标签
     * @param $mid
     * @return array|int
     */
    public static  function getMyGroupTagData($mid)
    {
        $sql = "select tag_id,sct.tag_name  from sline_community_member_info_by_appid  as scm 
                  left join sline_community_tag  as sct on scm.tag_id=sct.id
                where  scm.mid=?  and scm.robot_status=1  and scm.is_delete=0 and sct.is_detete=0 " ;
        $result =self::newDb() ->select($sql,[$mid]);
        return !empty($result) ? $result:[];
    }
    /**
     * 获取
     * @param $wxIds
     */
    public static function getMyGroupTagDataByWxid($param)
    {
        if ($param['sup_id'] == 0) {
            $sql = "select tag_id,sct.tag_name  from sline_community_member_info_by_appid  as scm 
                   left join sline_community_white_list_wx   as scw  on scm.wxid=scw.wxid
                   left join sline_community_tag  as sct on scm.tag_id=sct.id
                   where  scw.mid=?  and scm.robot_status=1  and scm.is_delete=0 and sct.is_detete=0  and   scm.tag_id>0 group by  tag_id ";
        } else {
            $sql = "select tag_id,sct.tag_name  from sline_community_member_info_by_appid  as scm 
                   left join sline_community_white_list_wx   as scw  on scm.wxid=scw.wxid
                   left join sline_community_tag  as sct on scm.tag_id=sct.id
                   where  scw.bind_mid=?  and scm.robot_status=1  and scm.is_delete=0 and sct.is_detete=0  and   scm.tag_id>0 group by  tag_id ";
        }
        $result = self::newDb()->select($sql, [$param['mid']]);
        return !empty($result) ? $result : [];
    }


    public static function allUserList()
    {
        $sql    = "select name,mid from sline_community_user where delete_at=0";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 获取我的机器人下的所有群的mid
     * @param $wxIds
     */
    public static function getMyGroupOwnMidByWxid($mid,$tag_id,$sup_id)
    {   
        if($tag_id!=0&&$tag_id!=null)
        {

            $where = " and  scm.tag_id in ($tag_id)";
        }else{
            $where = "";
        }
        if ($sup_id == 0) {
            $sql = "select scm.mid  from sline_community_member_info_by_appid  as scm 
                   left join sline_community_white_list_wx   as scw  on scm.wxid=scw.wxid
                   left join sline_community_tag  as sct on scm.tag_id=sct.id
                   where  scw.mid=$mid  and scm.robot_status=1  and scm.is_delete=0 and sct.is_detete=0 $where  group by  scm.mid ";
        } else {
            $sql = "select scm.mid  from sline_community_member_info_by_appid  as scm 
                   left join sline_community_white_list_wx   as scw  on scm.wxid=scw.wxid
                   left join sline_community_tag  as sct on scm.tag_id=sct.id
                   where  scw.bind_mid=$mid  and scm.robot_status=1  and scm.is_delete=0 and sct.is_detete=0 $where group by  scm.mid ";
        }

        $result = DB::select($sql);

        return !empty($result) ? $result : [];
    }


    public static function getChannelData($mid)
    {
        $sql    = "select mid,platform_identity from sline_community_user where delete_at=0 and mid=?";
        $result =self::newDb() ->selectOne($sql,[$mid]);
        return $result->platform_identity;
    }


    public static function getChannelDataByPlatform($platform_identity)
    {
        $sql    = "select id as channel,channel_name from sline_community_channel_data  where platform_identity=?";
        $result =self::newDb() ->selectOne($sql,[$platform_identity]);
        return $result;
    }



    /**数据
     * @param array $data
     * @return array
     */
    public static function getCommunitySendInfoRandom($RandomStr)
    {
        $sql    = "select * from sline_community_send_info  WHERE  random_str='{$RandomStr}'";
        $result =self::newDb() ->select($sql);
        return $result;
    }

    /**
     * 数据
     * @param $data
     * @return mixed
     */
    public static function delRandomStr($data)
    {
        $wheretime = date('Y-m-d ',(time()-604800));
        $endTime = date('Y-m-d ',(time()-1209800));
        $randomStrSql = "select random_str  from sline_community_send_info where  created_at<='".$wheretime."' and created_at>= '".$endTime."'   group by random_str order by created_at asc   limit {$data['offset']},{$data['display']}";
        $randomStrData = self::newDb() ->select($randomStrSql);
        return json_decode(json_encode($randomStrData),true);
    }

    /**
     * @return mixed
     */
    public  static function getDelCount(){
        $wheretime = date('Y-m-d ',(time()-604800));
        $endTime = date('Y-m-d ',(time()-1209800));
        $randomStrSql    = "select count(*) count_num from (select count(*) count from sline_community_send_info where   created_at<='".$wheretime."' and   created_at>= '".$endTime."'  group by random_str) as e";
        $randomStrCountData = self::newDb() ->selectOne($randomStrSql);
        return json_decode(json_encode($randomStrCountData),true);
    }



}