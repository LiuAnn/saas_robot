<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserModel extends Model
{

    protected static $table_name = "user";

    /**
     * @param $where
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 14:03
     * @method   method   [获取用户登录信息]
     */
    public static function getLoginUser($where){

        return DB::table(self::$table_name)->where($where)->first();
    }

    /**
     * @param $params
     * @return bool
     * @author   liuwenhao
     * @time     2020/4/14 15:20
     * @method   method   [用户注册]
     */
    public static function addUser($params){

        return DB::table(self::$table_name)->insert($params);
    }
    /**
     * @param $params
     * @return bool
     * @author   liuwenhao
     * @time     2020/4/14 15:20
     * @method   method   [用户注册]
     */
    public static function updateUser($params){
        return DB::table(self::$table_name)->where('mid', $params['mid'])->update($params);
    }

    public static function addLoginLog($data,$token,$ip=''){
        $data['token'] = $token;
        $data['ip'] = $ip;
        return DB::table('user_login_info')->insert($data);
    }

    public static function getUserListByName($param = [])
    {
        $fields = [
            'mid', 'name'
        ];
        $query = DB::table(self::$table_name)->select($fields);
        if (isset($param['midS']) && !empty($param['midS'])) {
            $query = $query->whereIn('mid', $param['midS']);
        }
        if (isset($param['userName']) && !empty($param['userName'])) {
            $query = $query->where('name','like','%'.$param['userName'].'%');
        }
        if (!isset($param['pageSize']) || empty($param['pageSize'])) {
            $param['pageSize'] = 100;
        }
        $query = $query->where('name', '!=', null);
        return $query->where(['delete_at' => 0])->paginate($param['pageSize']);
    }

    /*
     * 获取用户及下级的mid 集合
     */
    public static function getDownUserIds($userId)
    {
        return DB::table(self::$table_name)->where(['sup_id' => $userId])->pluck('mid');
    }

    /*
     * 获取用户同级的mid 集合
     */
    public static function getPeerUserIds($userId)
    {
        return DB::table(self::$table_name)->where(['sup_id' => $userId])->pluck('mid');
    }

    /*
     * 获取用户的名称
     */
    public static function getUserNameByMid($mid)
    {
        $adminName = DB::table(self::$table_name)->where(['mid' => $mid])->pluck('name')->first();
        return $adminName ?: '管理员';
    }


    /*
     * 获取用户的名称
     */
    public static function getPlatformIdentityByMid($mid)
    {
        $platform_identity = DB::table(self::$table_name)->where(['mid' => $mid])->pluck('platform_identity')->first();

        switch ($platform_identity) {
            case '7323ff1a23f0bf1cda41f690d4089353':
                $channel_name = '悦淘';
                break;
            case '89dafaaf53c4eb68337ad79103b36aff':
                $channel_name = '大人';
                break;
            case '183ade9bc5f1c4b52c16072362bb21d1':
                $channel_name = '迈图';
                break;
            case '75f712af51d952af3ab4c591213dea13':
                $channel_name = '直订';
                break;
            case 'c23ca372624d226e713fe1d2cd44be35':
                $channel_name = '邻居团';
                break;
            default:
                $channel_name = '悦淘';
                break;
        }
        return $channel_name;
    }

    /*
    * 获取用户的名称
    */
    public static function getMemberNameByMid($mid)
    {
        $adminName = DB::connection('mysql_yuelvhui')->table('sline_member')->where(['mid' => $mid])->pluck('nickname');
        $adminName = json_decode(json_encode($adminName), true);
        if (!empty($adminName) && is_array($adminName)) {
            $adminName = $adminName[0];
        } else {
            $adminName = '暂无';
        }
        return $adminName;
    }

    /*
* 获取用户的名称
*/
    public static function getMemberInfoByMid($mid)
    {
        $memberInfo = DB::connection('mysql_yuelvhui')->table('member')->select(['truename','nickname'])->where(['mid' => $mid])->first();
        $memberInfo = json_decode(json_encode($memberInfo), true);
        return $memberInfo;
    }


    /*
    * 获取用户的信息
    */
    public static function getMemberInfoByWhere($where)
    {
        $memberInfo = DB::connection('mysql_yuelvhui')->table('member')
            ->where($where)->first();
        return $memberInfo ? json_decode(json_encode($memberInfo), true) : [];
    }

    /*
    * 获取用户的名称
     */
    public static function getUserPhoneByMid($mid)
    {
        $adminPhone = DB::connection('mysql_yuelvhui')->table('member')->where(['mid' => $mid])->pluck('mobile');
        $adminPhone = json_decode(json_encode($adminPhone), true);
        if (!empty($adminPhone) && is_array($adminPhone)) {
            $adminPhone = $adminPhone[0];
        } else {
            $adminPhone = '暂无';
        }
        return $adminPhone;
    }

    /*
* 获取用户的名称
 */
    public static function getHouTaiUserPhoneByMid($mid)
    {
        $adminPhone = DB::connection('mysql_yuelvhui')->table('sline_member')->where(['mid' => $mid])->pluck('mobile');
        $adminPhone = json_decode(json_encode($adminPhone), true);
        if (!empty($adminPhone) && is_array($adminPhone)) {
            $adminPhone = $adminPhone[0];
        } else {
            $adminPhone = '暂无';
        }
        return $adminPhone;
    }
}
