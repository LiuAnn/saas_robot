<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class LotteryChanceModel extends Model
{
    protected $connection = "mysql_yuelvhui";

    protected $table = 'member_lottery_chances';

    protected $primaryKey = 'chance_id';

    protected $guarded = [];
}
