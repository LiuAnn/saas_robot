<?php

namespace App\Model\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CouponModel extends Model {

    /**
     * @author   liuwenhao
     * @time     2020/4/14 15:43
     * @method   method   [获取连接信息]
     */
    public static function getConnectDB(){

//        return DB::connection("mysql_open");
        return DB::connection("mysql_yuelvhui");
    }

    /**
     * 优惠券信息
     * @param int $id
     * @return mixed
     */
    public static function getCouponInfoById($id = 0)
    {
        $sql = "select isnever, starttime, endtime, antedate, totalnumber,amount,modules,son_modules from yuelvhui.sline_coupon where id = ?";
        return DB::selectOne($sql, [$id]);
    }


    /**
     * 优惠券数量记录更新
     * @param int $id
     * @param $number
     * @return int
     */
    public static function updateCouponTotalNum($id = 0, $number)
    {
        $sql = "update yuelvhui.sline_coupon set totalnumber = totalnumber - ?, send_num = send_num + ? where id = ?";
        return DB::update($sql, [$number, $number, $id]);
    }

    public static function insertCoupon($data) {
        return self::getConnectDB()->table("member_coupon")->insert($data);
    }
}
