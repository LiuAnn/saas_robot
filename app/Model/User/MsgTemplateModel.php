<?php
/**
 * Created by PhpStorm.
 * User: isdeng
 * Date: 2019/4/16
 * Time: 下午8:26
 */

namespace App\Model\User;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsgTemplateModel extends Model {

    public static function getMessageTemplate($templateId) {
        $sql    = "select * from template where template_id = ?";
        $result = DB::connection("message")->select($sql, [$templateId]);
        return !empty($result) ? $result[0] : [];
    }
}
