<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class LotteryChanceLogModel extends Model
{
    protected $table = 'member_lottery_chances_log';

    protected $primaryKey = 'chance_id';

    protected $guarded = [];

    //关闭 Laravel 自动管理列
    public $timestamps = false;
}
