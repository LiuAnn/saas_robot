<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class InvitationCodeModel extends Model {

    public static function getcompanyByNumber($codeNumber) {
        $sql = "select m.company_id from sline_invitation_code c "
            . "left join sline_member m on c.mid = m.mid where c.code_number = ? limit 1";

        $result = DB::select($sql, [$codeNumber]);
        return !empty($result) ? $result[0]->company_id : 0;
    }


    public static function getMemberByNumber($codeNumber) {
        $sql = "select * from sline_invitation_code c "
            . "left join sline_member m on c.mid = m.mid where c.code_number = ? limit 1";

        $result = DB::select($sql, [$codeNumber]);
        return !empty($result) ? $result[0] : null;
    }
}
