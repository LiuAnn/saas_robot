<?php

namespace App\Model\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MemberBonusModel extends Model
{

    //指定表名
    protected $table = 'member_bonus';

    //指定主键字段
    protected $primaryKey = 'id';

    //关闭 Laravel 自动管理列
    public $timestamps = false;

    public $guarded = [];

    /**
     * 新增
     */
    public static function insert($data)
    {
        return DB::table("member_bonus")->insertGetId([
            "mid"           => $data["mid"],
            "fid"           => $data["fid"],
            "level"         => $data["level"],
            "amount"        => $data["amount"],
            "type"          => $data["type"],
            "productaid"    => $data["productaId"],
            "productName"   => array_key_exists("productName", $data) ? $data["productName"] : "",
            "content"       => array_key_exists("content", $data) ? $data["content"] : "",
            "orderNo"       => array_key_exists("orderNo", $data) ? $data["orderNo"] : "",
            "identity"      => array_key_exists('identity', $data) ? $data["identity"] : 0,
            "identity_type" => array_key_exists('identity_type', $data) ? $data["identity_type"] : 0,
            "createTime"    => date("Y-m-d H:i:s", time()),
            "module"        => array_key_exists('module', $data) ? $data["module"] : 0,
            "channel_id"    => array_key_exists('channel_id', $data) ? $data["channel_id"] : 0,
        ]);
    }

    /**
     * 新增提现记录
     */
    public static function insertCash($data)
    {
        return DB::table("member_cash")->insertGetId($data);
    }

    public static function updateCash($data)
    {
        return DB::table("member_cash")->where("billNo", $data["billNo"])->update($data);
    }

    public static function getBonusList($memberId, $level, $offset = 0, $display = 20)
    {
        $sql = "select sb.fid, nickname, truename, mobile, amount, sb.createTime, litpic ";
        $sql .= "from sline_member_bonus sb ";
        $sql .= "left join sline_member sm on sm.mid = sb.fid ";
        $sql .= "where sb.mid = ? and `level` = ? ";
        $sql .= "order by sb.createTime desc limit ?, ?";

        return DB::select($sql, [$memberId, $level, $offset, $display]);
    }

    public static function getBonusListV2($memberId, $offset = 0, $display = 100)
    {
        $sql = "select sb.fid, sb.type, sb.productaid, sb.level, "
            . "nickname, truename, mobile, amount, sb.createTime, litpic "
            . "from sline_member_bonus sb "
            . "left join sline_member sm on sm.mid = sb.fid "
            . "where sb.mid = ? order by sb.createTime desc limit ?, ?";

        return DB::select($sql, [$memberId, $offset, $display]);
    }

    public static function getBonusListByType($memberId, $type = 1, $offset = 0, $display = 20)
    {
        $sql = "select sb.fid,sb.type,sb.level ,nickname, truename, mobile, amount, sb.createTime, litpic ";
        $sql .= "from sline_member_bonus sb ";
        $sql .= "left join sline_member sm on sm.mid = sb.fid ";
        $sql .= "where sb.mid = ? and `type` = ? ";
        $sql .= "order by sb.createTime desc limit ?, ?";

        return DB::select($sql, [$memberId, $type, $offset, $display]);
    }

    public static function getCashListByMemberId($memberId, $offset = 0, $display = 20)
    {
        $sql = "select amount, name, status, descript, createTime from sline_member_cash where mid = ? and (status = 2 or status = 3 or status = 4) order by createTime desc limit ?, ?";
        return DB::select($sql, [$memberId, $offset, $display]);
    }

    public static function updateCashStatus($billNo, $status)
    {
        $sql = "update sline_member_cash set status = ? where billNo = ?";
        return DB::update($sql, [$status, $billNo]);
    }

    public static function rsyncPay($date)
    {
        $sql    = "select mc.mid, mc.billNo,mc.amount, mc.appId, m.truename_identify truename, mc.name, mc.openid, mc.descript, date_format(createTime, '%Y-%m-%d') day "
            . "from sline_member_cash mc left join sline_member m on mc.mid = m.mid "
            . "where mc.openid != '' and status = 2 and date_format(createTime, '%Y-%m-%d') = '{$date}'";
        $result = DB::select($sql);
        return $result;
    }

    public static function rsyncPayByMemberId($date, $memberId)
    {
        $sql    = "select mc.mid, mc.billNo,mc.amount, mc.appId, m.truename_identify truename, mc.name, mc.openid, mc.descript, date_format(createTime, '%Y-%m-%d') day "
            . "from sline_member_cash mc left join sline_member m on mc.mid = m.mid "
            . "where mc.openid != '' and status = 2 and date_format(createTime, '%Y-%m-%d') = '{$date}' and mc.mid = {$memberId} and apply_type = 1";
        $result = DB::select($sql);
        return $result;
    }

    /**
     * 查询进几天的奖金记录
     */
    public static function getBonusListIntervalByMemberId($memberId, $day)
    {
        $sql = "select m.truename, m.nickname, m.mobile, m.litpic, amount, level, type, b.createTime "
            . "from sline_member_bonus b "
            . "left join sline_member m on b.fid = m.mid "
            . "where date_sub(curdate(), interval {$day} day) <= date(createTime) and b.mid = {$memberId}";
        return DB::select($sql);
    }

    public static function getBonusTotalIntervalByMemberId($memberId, $day)
    {
        $sql    = "select sum(amount) amount from sline_member_bonus "
            . "where date_sub(curdate(), interval {$day} day) <= date(createTime) and mid = {$memberId}";
        $result = DB::select($sql);
        return !empty($result) ? $result[0]->amount : 0;
    }

    /**
     * 根据日期获取用户的佣金明细
     */
    public static function getMemberBonusListByDate($mid, $startTime, $endTime)
    {
        $sql = "select * from sline_member_bonus where mid = {$mid} and createTime <= '{$endTime}'";
        if (!empty($startTime)) {
            $sql .= " and createTime > '{$startTime}'";
        }
        $result = DB::select($sql);
        return !empty($result) ? $result : [];
    }

    /**
     * 获取未执行的提现记录(微信)
     */
    public static function getCashDontUnexecuted($memberId)
    {
        $sql    = "select count(*) count from sline_member_cash where mid = ? and status = 2 and apply_type = 1";
        $result = DB::select($sql, [$memberId]);
        return $result[0]->count;
    }

    /**
     * 获取未执行的提现记录=
     */
    public static function getCashDontUnexecutedForCard($memberId)
    {
        $sql    = "select count(*) count from sline_member_cash where mid = ? and status = 2";
        $result = DB::select($sql, [$memberId]);
        return $result[0]->count;
    }


    /**
     * 获取某个用户在某个日期下级大礼包付款订单数量
     * @param int
     * @param date
     * @return int
     */
    public static function getPackageOrderNoByMemberIdInDate($memberId, $date)
    {
        $sql    = 'select count(*) count from sline_member_bonus where `type` = 1 and mid = ? and date_format(createTime, "%Y-%m-%d") = ?';
        $result = DB::selectOne($sql, [$memberId, $date]);
        return $result->count;
    }

    /**
     * 获取某个用户在某个日期下级大礼包付款订单金额
     * @param int
     * @param date
     * @return int
     */
    public static function getPackageMoneyNoByMemberIdInDate($memberId, $date)
    {
        $sql    = 'select sum(amount) amount from sline_member_bonus where type = 1 and mid = ? and date_format(createTime, "%Y-%m-%d") = ?';
        $result = DB::selectOne($sql, [$memberId, $date]);
        return empty($result->amount) ? 0 : $result->amount;
    }

    /**
     * 获取某个用户在某个月下级大礼包付款订单数量
     * @param int
     * @param date
     * @return int
     */
    public static function getPackageOrderNoByMemberIdInMonth($memberId, $month)
    {
        $sql    = 'select count(*) count from sline_member_bonus where `type` = 1 and mid = ? and date_format(createTime, "%Y-%m") = ?';
        $result = DB::selectOne($sql, [$memberId, $month]);
        return $result->count;
    }

    /**
     * 获取某个用户在某个月下级大礼包付款订单金额
     * @param int
     * @param date
     * @return int
     */
    public static function getPackageMoneyNoByMemberIdInMonth($memberId, $month)
    {
        $sql    = 'select sum(amount) amount from sline_member_bonus where `type` = 1 and mid = ? and date_format(createTime, "%Y-%m") = ?';
        $result = DB::selectOne($sql, [$memberId, $month]);
        return empty($result->amount) ? 0 : $result->amount;
    }

    /**
     * 某个用户下级累计大礼包订单数量
     */
    public static function getPackageOrderNoByMemberId($memberId)
    {
        $sql    = "select count(*) count from sline_member_bonus where `type` = 1 and mid = ?";
        $result = DB::selectOne($sql, [$memberId]);
        return $result->count;
    }

    /**
     * 某个用户下级累计大礼包收益
     */
    public static function getPackageMoneyNoByMemberId($memberId)
    {
        $sql    = "select sum(amount) amount from sline_member_bonus where `type` = 1 and mid = ?";
        $result = DB::selectOne($sql, [$memberId]);
        return empty($result->amount) ? 0 : $result->amount;
    }

    /**
     * 获取某个用户在某个日期下级大礼包付款订单数量
     * @param int
     * @param date
     * @return int
     */
    public static function getSaleOrderNoByMemberIdInDate($memberId, $date)
    {
        $sql    = 'select count(*) count from sline_member_bonus where `type` = 4 and mid = ? and date_format(createTime, "%Y-%m-%d") = ?';
        $result = DB::selectOne($sql, [$memberId, $date]);
        return $result->count;
    }

    /**
     * 获取某个用户在某个日期下级销售付款订单金额
     * @param int
     * @param date
     * @return int
     */
    public static function getSaleMoneyNoByMemberIdInDate($memberId, $date, $module = '')
    {
        $sql = 'select sum(amount) amount from sline_member_bonus where `type` = 4 and mid = ? and date_format(createTime, "%Y-%m-%d") = ?';
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $date]);
        return empty($result->amount) ? 0 : $result->amount;
    }

    /**
     * 获取某个用户在某个月下级销售付款订单数量
     * @param int
     * @param date
     * @return int
     */
    public static function getSaleOrderNoByMemberIdInMonth($memberId, $month, $module = '')
    {
        $sql = 'select count(*) count from sline_member_bonus where `type` = 4 and mid = ? and date_format(createTime, "%Y-%m") = ?';
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $month]);
        return $result->count;
    }

    /**
     * 获取某个用户在某个月下级销售付款订单金额
     * @param int
     * @param date
     * @return int
     */
    public static function getSaleMoneyByMemberIdInMonth($memberId, $month, $module = '')
    {
        $sql = 'select sum(amount) amount from sline_member_bonus where `type` = 4 and mid = ? and date_format(createTime, "%Y-%m") = ?';
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $month]);
        return empty($result->amount) ? 0 : $result->amount;
    }

    /**
     * 某个用户下级累计大礼包订单数量
     * @param int
     * @return int
     */
    public static function getSaleOrderNoByMemberId($memberId)
    {
        $sql    = "select count(*) count from sline_member_bonus where `type` = 4 and mid = ?";
        $result = DB::selectOne($sql, [$memberId]);
        return $result->count;
    }

    /**
     * 某个用户下级累计大礼包收益
     * @param int
     * @return int
     */
    public static function getSaleMoneyNoByMemberId($memberId, $module = '')
    {
        $sql = "select sum(amount) amount from sline_member_bonus where `type` = 4 and mid = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId]);
        return empty($result->amount) ? 0 : $result->amount;
    }

    /**
     * 累计收益订单数量
     */
    public static function getSaleOrderCount($memberId, $type, $module = '')
    {
        $sql = "select count(*) count from sline_member_bonus where orderNo != '' and mid = ? and type = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $type]);
        return $result->count;
    }

    /**
     * 累计获取收益订单
     * @param int
     * @param int
     * @return []
     */
    public static function getSaleOrderList($memberId, $type, $offset, $display, $module = '')
    {
        $sql = "select m.nickname, m.litpic, b.type, b.amount, b.level, b.productName, b.createTime, b.show_status "
            . "from sline_member_bonus b left join sline_member m on m.mid = b.fid "
            . "where orderNo != '' and b.type = ? and b.mid = ? ";
        if (is_numeric($module)) {
            $sql .= " and b.module = '$module'";
        }
        $sql .= "order by id desc limit ?, ?";

        $result = DB::select($sql, [$type, $memberId, $offset, $display]);

        return empty($result) ? [] : $result;
    }

    /**
     * 获得某个日期下的销售订单量
     * @param int
     * @param  int
     */
    public static function getSaleCountInDate($memberId, $type, $date, $module = '')
    {
        $sql = "select count(*) count from sline_member_bonus where orderNo != '' and mid = ? and type = ? and date_format(createTime, '%Y-%m-%d') = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }

        $result = DB::selectOne($sql, [$memberId, $type, $date]);
        return $result->count;
    }

    /**
     * 获得某个月下的销售订单量
     * @param int
     * @param  int
     */
    public static function getSaleCountInMonth($memberId, $type, $month, $module = '')
    {
        $sql = "select count(*) count from sline_member_bonus where orderNo != '' and mid = ? and type = ? and date_format(createTime, '%Y-%m') = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $type, $month]);
        return $result->count;
    }

    /**
     * 获取某个日期收益订单
     * @param int
     * @param int
     * @return []
     */
    public static function getSaleOrderListInDate($memberId, $type, $date, $offset, $display, $module = '')
    {
        $sql = "select m.nickname, m.litpic, b.type, b.amount, b.level, b.productName, b.createTime, b.show_status "
            . "from sline_member_bonus b left join sline_member m on m.mid = b.fid "
            . "where orderNo != '' and b.type = ? and b.mid = ? and date_format(createTime, '%Y-%m-%d') = ? ";
        if (is_numeric($module)) {
            $sql .= " and b.module = '$module'";
        }
        $sql .= "order by id desc limit ?, ?";

        $result = DB::select($sql, [$type, $memberId, $date, $offset, $display]);
        return empty($result) ? [] : $result;
    }

    /**
     * 获取某个日期收益订单
     * @param int
     * @param int
     * @return []
     */
    public static function getSaleOrderListInMonth($memberId, $type, $month, $offset, $display, $module = '')
    {
        $sql = "select m.nickname, m.litpic, b.type,b.amount, b.level, b.productName, b.createTime, b.show_status "
            . "from sline_member_bonus b left join sline_member m on m.mid = b.fid "
            . "where orderNo != '' and b.type = ? and b.mid = ? and date_format(createTime, '%Y-%m') = ? ";
        if (is_numeric($module)) {
            $sql .= " and b.module = '$module'";
        }
        $sql .= "order by id desc limit ?, ?";

        $result = DB::select($sql, [$type, $memberId, $month, $offset, $display]);
        return empty($result) ? [] : $result;
    }

    /**
     * 读取未提现记录
     */
    public static function getMemberCashByMemberId($memberId)
    {
        $sql    = "select * from sline_member_cash where status = 2 and mid = ? order by id asc limit 1";
        $result = DB::selectOne($sql, [$memberId]);
        return empty($result) ? [] : $result;
    }

    /**
     *
     */
    public static function getMemberBonusCountByOrder($orderNo)
    {
        $sql     = "select count(*) count from sline_member_bonus where orderNo = ?";
        $orderNo = $orderNo . "";
        $result  = DB::selectOne($sql, [$orderNo]);
        return $result->count;
    }


    public static function getTotalAmountByMemberIdInMonth($memberId, $month)
    {
        $sql    = "select ifnull(sum(amount), 0) amount from sline_member_cash where mid = ? and (status = 2 or status = 3) and date_format(createTime, '%Y-%m') = ?";
        $result = DB::selectOne($sql, [$memberId, $month]);
        return $result->amount;
    }

    /**
     * 获取用户所有（1：采购结算 4:商品结算）分佣订单
     */
    public static function getAllBonusByType($memberId, $type)
    {
        $sql = "select amount,module,show_status from sline_member_bonus where orderNo != '' and mid = ? and type = ? ";
//        echo $sql;
        $result = DB::select($sql, [$memberId, $type]);
        return empty($result) ? [] : $result;
    }

    /**
     * 累计获取收益订单
     * @param int
     * @param int
     * @return []
     */
    public static function getSettlementOrderList($memberId, $type, $offset, $display, $module = '')
    {
        $sql = "select m.nickname, m.litpic, b.amount, b.product_name, b.created_at, b.status "
            . "from sline_member_bonus_becommitted b left join sline_member m on m.mid = b.fid "
            . "where order_no != '' and b.mid = ? ";
        if (is_numeric($module)) {
            $sql .= " and b.module = '$module'";
        }
        $sql    .= " order by id desc limit ?, ?";
        $result = DB::select($sql, [$memberId, $offset, $display]);
        return empty($result) ? [] : $result;
    }

    /**
     * 获得某个日期下的待结算佣金销售订单量
     * @param int
     * @param  int
     */
    public static function getSettlementCountInDate($memberId, $type, $date, $module = '')
    {
        $sql = "select count(*) count from sline_member_bonus_becommitted where order_no != '' and mid = ? and date_format(created_at, '%Y-%m-%d') = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $date]);
        return $result->count;
    }

    /**
     * 累计待收益订单数量
     */
    public static function getSettlementOrderCount($memberId, $type, $module = '')
    {
        $sql = "select count(*) count from sline_member_bonus_becommitted where order_no != '' and mid = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId]);
        return $result->count;
    }

    /**
     * 获得某个月下的待返佣销售订单量
     * @param int
     * @param  int
     */
    public static function getSettlementCountInMonth($memberId, $type, $month, $module = '')
    {
        $sql = "select count(*) count from sline_member_bonus_becommitted where order_no != '' and mid = ? and date_format(created_at, '%Y-%m') = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::selectOne($sql, [$memberId, $month]);
        return $result->count;
    }

    /**
     * 获取某个日期收益订单
     * @param int
     * @param int
     * @return []
     */
    public static function getSettlementListInDate($memberId, $type, $date, $offset, $display, $module = '')
    {
        $sql = "select m.nickname, m.litpic, b.amount, b.product_name, b.created_at, b.status "
            . " from sline_member_bonus_becommitted b left join sline_member m on m.mid = b.fid "
            . "where order_no != '' and b.mid = ? and date_format(created_at, '%Y-%m-%d') = ? ";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $sql    .= "order by id desc limit ?, ?";
        $result = DB::select($sql, [$memberId, $date, $offset, $display]);
        return empty($result) ? [] : $result;
    }

    /**
     * 获取某个日期收益订单
     * @param int
     * @param int
     * @return []
     */
    public static function getSettlementOrderListInMonth($memberId, $type, $month, $offset, $display, $module = '')
    {
        $sql = "select m.nickname, m.litpic,b.amount, b.product_name, b.created_at, b.status"
            . " from sline_member_bonus_becommitted b left join sline_member m on m.mid = b.fid "
            . "where order_no != '' and b.mid = ? and date_format(created_at, '%Y-%m') = ? ";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }

        $sql    .= "order by id desc limit ?, ?";
        $result = DB::select($sql, [$memberId, $month, $offset, $display]);
        return empty($result) ? [] : $result;
    }

    public static function getSettlementMoneyNoByMemberIdInDate($memberId, $time, $module = '')
    {
        $sql = "select amount,status from sline_member_bonus_becommitted where mid = ? and order_no != '' and date_format(created_at, '%Y-%m-%d') = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::select($sql, [$memberId, $time]);
        return empty($result) ? [] : $result;
    }

    public static function getSettlementMoneyByMemberIdInMonth($memberId, $time, $module = '')
    {
        $sql = "select amount,status from sline_member_bonus_becommitted where mid = ? and order_no != '' and date_format(created_at, '%Y-%m-%d') = ?";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::select($sql, [$memberId, $time]);
        return empty($result) ? [] : $result;
    }

    public static function getSettlementMoneyNoByMemberId($memberId, $module = '')
    {
        $sql = "select amount,status from sline_member_bonus_becommitted where mid = ? and order_no != ''";
        if (is_numeric($module)) {
            $sql .= " and module = '$module'";
        }
        $result = DB::select($sql, [$memberId]);
        return empty($result) ? [] : $result;
    }

    public static function getBonusInDate($memberId, $type, $time, $module)
    {
        $sql = 'select amount,show_status from sline_member_bonus where mid = ? and  `type` = ? and date_format(createTime, "%Y-%m-%d") = ?';
        if (is_numeric($module) && $type == 4) {
            $sql .= " and module = '$module'";
        }
        $result = DB::select($sql, [$memberId, $type, $time]);
        return empty($result) ? [] : $result;
    }

    public static function getBonusInMonth($memberId, $type, $time, $module)
    {
        $sql = 'select amount,show_status from sline_member_bonus where mid = ? and `type` = ? and date_format(createTime, "%Y-%m") = ?';
        if (is_numeric($module) && $type == 4) {
            $sql .= " and module = '$module'";
        }
        $result = DB::select($sql, [$memberId, $type, $time]);
        return empty($result) ? [] : $result;
    }

    public static function getBonus($memberId, $type, $module)
    {
        $sql = 'select amount,show_status from sline_member_bonus where mid = ? and `type` = ?';
        if (is_numeric($module) && $type == 4) {
            $sql .= " and module = '$module'";
        }
        $result = DB::select($sql, [$memberId, $type]);
        return empty($result) ? [] : $result;
    }

}

?>
