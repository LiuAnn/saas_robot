<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CompanyModel extends Model {

    /**
     * 根据分公司ID获取信息
     */
    public static function getCompanyByCompanyId($companyId) {
        $sql    = "select * from sline_company_message where company_id = ?";
        $result = DB::connection("company")->select($sql, [$companyId]);
        return !empty($result) ? $result[0] : [];
    }
}
