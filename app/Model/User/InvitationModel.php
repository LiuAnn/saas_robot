<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class InvitationModel extends Model {



    /**
     * @author   liuwenhao
     * @time     2020/4/14 15:43
     * @method   method   [获取连接信息]
     */
    protected static function getConnectDB(){

//        return DB::connection("mysql_open");
        return DB::connection("mysql_yuelvhui");
    }
    /**
     * 新增推荐码
     */
    public static function insertInvit($data) {
        return self::getConnectDB()->table("invitation_code")->insertGetId($data);
    }
}
