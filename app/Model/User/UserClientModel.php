<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Service\User\LotteryChanceServiceImpl;

class UserClientModel extends Model {

    protected static $table_name = "member";

    const PERIODS20190107 = 'PERIODS2019_01_07';

    /**
     * @author   liuwenhao
     * @time     2020/4/14 15:43
     * @method   method   [获取连接信息]
     */
    public static function getConnectDB(){

//        return DB::connection("mysql_open");
        return DB::connection("mysql_yuelvhui");
    }

    /**
     * @param $where
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 15:42
     * @method   method   [获取用户信息]
     */
    public static function getLoginUser($where){

        return self::getConnectDB()
                    ->table(self::$table_name)
                    ->select("mid" , "mobile" , "nickname")
                    ->where($where)
                    ->first();
    }


    /**
     * @param $where
     * @return
     * @author   liuwenhao
     * @time     2020/4/14 16:38
     * @method   method   [获取codeNumber]
     */
    public static function getCodeNumber($where){

        return self::getConnectDB()
                    ->table("invitation_code")
                    ->select("code_number as codeNumber")
                    ->where($where)
                    ->first();
    }


    /**
     * 新增用户信息
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public static function insertMember($data)
    {

        return DB::transaction(function()use($data){

            Log::info('悦淘用户注册-1',[array_key_exists("companyId", $data)]);

            //添加记录
            $id = self::getConnectDB()->table("member")->insertGetId([
                "parent_id" => $data["parent_id"],
                "mobile"    => $data["mobile"],
                "pwd"       => $data["password"],
                "nickname"  => $data["nickname"],
                "area_code" => array_key_exists("area_code", $data) ? $data["area_code"] : '86',
                "country"   => array_key_exists("country", $data) ? $data["country"] : '中国',
                "jointime"  => $data["jointime"],
                "money"     => 0,
                "balance"   => 0,
                "company_id"=> array_key_exists("companyId", $data) ? $data["companyId"] : 0,
                "channel_id"=> array_key_exists("channelId", $data) ? $data["channelId"] : 0,
                "virtual"   => array_key_exists("virtual", $data) ? $data["virtual"] : 1,
                "application_id"=>array_key_exists("applicationId",$data) ? $data['applicationId']:  0
            ]);

            //添加path记录
            self::getConnectDB()->table('member_ship')->insert([
                'mid'           => $id,
                'path'          => self::createPath($data['parent_id']),
                'create_time'   => time(),
                'update_time'   => time()
            ]);

//            //增加抽奖机会
//            if($data['parent_id']){
//                // 添加抽奖机会
//                LotteryChanceServiceImpl::increase($data['parent_id'],1, self::PERIODS20190107);
//            }

            return $id;
        });
    }



    /**
     * @param $parentId
     * @return int|string
     * @throws \Exception
     */
    protected static function createPath($parentId)
    {
        if(!$parentId){
            return '0';
        }

        //查询父级是否存在
        $parentModel = self::getConnectDB()->table('member_ship')
            ->where('mid',$parentId)
            ->first();

        if(!$parentModel){
            throw new \Exception('数据结构异常');
        }

        return $parentModel->path.','.$parentId;
    }
}
