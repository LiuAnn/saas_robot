<?php
/**
 * Created by PhpStorm.
 * User: cxy
 * Date: 2020/12/16
 * Time: 16:25
 */

namespace App\Model\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class IndexModel  extends Model
{

    public static function InsertThreeProductCategory($para){

        return DB::table('three_product_category')->insert($para);

    }


    /**
     * 获取用户总收益
     * @param array $condition
     * @param string $identity 0 所有入账 0-100以内 各个业务  100 分佣所得
     * @return mixed
     */
    public static function getMemeberProfitByMobile($param,$identity = 0){

    	if(!isset($param['mobile'])||empty($param['mobile'])){

    		return 0;
    	}

        $where = "and mobile = {$param['mobile']}";

        if(isset($param['status'])&&$param['status']==1){

        	$where .= " and b.status=".$param['status'];
        }

        if(isset($param['startTime'])&&!empty($param['startTime'])){

        	if(isset($param['status'])&&$param['status']==1){

        		$where .= " and b.updated_at>'{$param['startTime']}'";

        	}else{

        		$where .= " and b.created_at>'{$param['startTime']}'";
        	}

        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

        	if(isset($param['status'])&&$param['status']==1){

        		$where .= " and b.updated_at>'{$param['endTime']}'";

        	}else{

        		$where .= " and b.created_at>'{$param['endTime']}'";
        	}

        }

        if($identity>0&&$identity<100){

            $where .= " and b.identity=".$identity;
        }

         if($identity==100){

            $where .= " and b.identity<6";
        }       

        $sql = "select sum(amount) as total_amount from sline_community_bonus_becommitted b";
        $sql.=" left join sline_community_group_member m on m.mid=b.mid where b.status<2 {$where}";

        $result = DB::selectOne($sql);

        return empty($result)?0:$result->total_amount/100;
    }


    /**
     * 获取用户总收益
     * @param array $condition
     * @param string $identity 0 所有入账 0-100以内 各个业务  100 分佣所得
     * @return mixed
     */
    public static function getMemeberProfitInfo($param,$identity = 0){

    	if(!isset($param['mobile'])||empty($param['mobile'])){

    		return 0;
    	}

        $where = "mobile = {$param['mobile']}";
        $select = ",DATE_FORMAT(FROM_UNIXTIME(created_at,'%Y-%m-%d %H:%i:%s'),'%Y-%m-%d') as times";

        if(isset($param['status'])&&$param['status']==1){

        	$where .= " and b.status=".$param['status'];
        }

        if(isset($param['startTime'])&&!empty($param['startTime'])){

        	if(isset($param['status'])&&$param['status']==1){
        		$select = ",DATE_FORMAT(FROM_UNIXTIME(updated_at,'%Y-%m-%d %H:%i:%s'),'%m-%d') as times";
        		$where .= " and b.updated_at>'{$param['startTime']}'";

        	}else{

        		$where .= " and b.created_at>'{$param['startTime']}'";
        	}

        }

        if(isset($param['endTime'])&&!empty($param['endTime'])){

        	if(isset($param['status'])&&$param['status']==1){

        		$where .= " and b.updated_at>'{$param['endTime']}'";

        	}else{

        		$where .= " and b.created_at>'{$param['endTime']}'";
        	}

        }

        if($identity>0&&$identity<100){

            $where .= " and b.identity=".$identity;
        }

         if($identity==100){

            $where .= " and b.identity<6";
        }       

        $sql = "select sum(amount) as total_amount {$select} from sline_community_bonus_becommitted b";
        $sql.=" left join sline_community_group_member m on m.mid=b.mid where b.status<2 {$where} group times";

        $result = DB::select($sql);

        $list = json_decode(json_encode($result));

        $data = [];

        foreach ($list as $key => $value) {
        	
        	$data[$key]['days'] = $value['times'];
        	$data[$key]['Amount'] = $value['total_amount']/100;

        }	

        return [];
    }


    /**
     * 获取用户总所有群的群ID
     * @param array $param
     * @param ...
     * @return string
     */
    public static function getMemberRoomIds($param){

    	$where = " g.is_delete=0";

    	if(isset($param['mobile'])&&$param['mobile']!=''){

    		$where .=" and g.mobile=".$param['mobile']."";
    	}

    	if(isset($param['platform_identity'])&&!empty($param['platform_identity'])){

    		$where .=" and w.platform_identity='".$param['platform_identity']."'";
    	}

    	$sql = "select g.id from sline_community_member_info_by_appid g ";
    	$sql.= " left join sline_community_white_list_wx w on w.wxid=g.wxid";
    	$sql.= " where {$where}";

        $result = DB::select($sql);

        $ids = json_decode(json_encode($result));

    	return implode(",", $ids);
    }

    /**
     * 获取用户当前群里的总订单数量与总GMV
     * @param string $roomIds
     * @param ...
     * @return array
     */
    public static function getOrderCount($roomIds=''){

    	if($roomIds==''){
    		return 0;
    	}

    	$sql = "select count(*) as orderCount,sum(amount) as orderAmount from sline_community_mall_order_goods where room_id in ('".$roomIds."')";

    	$result = DB::selectOne($sql);

    	return json_decode(json_encode($result));
    }

    /**
     * 获取用户当前群里的最后一条订单的商品名称与下单时间
     * @param string $roomIds
     * @param ...
     * @return array
     */
    public static function getLastOrderInfo($roomIds=''){

    	if($roomIds==''){
    		return 0;
    	}

    	$sql = "select goods_name,pay_time from sline_community_mall_order_goods where room_id in ('".$roomIds."') order by id desc limit 1";

    	$result = DB::selectOne($sql);

    	return json_decode(json_encode($result));
 	
    }

    /**
     * 获取用户当前群里各个商品类型的订单量
     * @param string $roomIds
     * @param ...
     * @return array
     */
    public static function getOrderInfoByGoodType($roomIds=''){


    	if($roomIds==''){
    		return 0;
    	}

    	$sql = "select count(*) as count_1,good_type from sline_community_mall_order_goods where room_id in ('".$roomIds."') group by good_type";

    	$result = DB::select($sql);

    	return json_decode(json_encode($result));

    }

    /**
     * 获取用户当前群里的下单人数
     * @param string $roomIds
     * @param ...
     * @return array
     */
    public static function getBuyMemberCount($roomIds=''){

    	if($roomIds==''){
    		return 0;
    	}

    	$sql = "select count(DISTINCT(member_id)) as count_1 from sline_community_mall_order_goods where room_id in ('".$roomIds."')";

    	$result = DB::selectOne($sql);

    	return empty($result)?0:$result->count_1;

    }

    /**
     * 根据手机号获取群主各个渠道的群数量
     * @param string $mobile
     * @param ...
     * @return array
     */
    public static function getAllGroupInfoByMobile($mobile){


    	$sql = "select count(g.mid) as groupNum,c.channel_name,c.platform_identity from sline_community_member_info_by_appid g ";
    	$sql.=" left join sline_community_white_list_wx w on w.wxid=g.wxid";
    	$sql.=" left join sline_community_channel_data c on c.platform_identity=w.platform_identity";
    	$sql.=" where w.deleted_at=0 and g.is_delete=0 and mobile={$mobile} group by c.platform_identity";

    	$result = DB::select($sql);

    	return json_decode(json_encode($result),true);
    }


    /**
     * 获取渠道列表
     * @param string $mobile
     * @param ...
     * @return array
     */
    public static function getChannelData($mobile){

    	$sql = "select * from sline_community_channel_data where is_delete=0";

    	$result = DB::select($sql);

    	return json_decode(json_encode($result),true);
    }

    /**
     * 获取用户当前渠道的群列表
     * @param array $param
     * @param string $select
     * @return array
     */
    public static function getGroupListByMobile($param,$select="*"){

    	if(!isset($param['mobile'])||empty($param['mobile'])){

    		return [];
    	}

    	$where = " group_mobile = ".$param['mobile'];

    	if(isset($param['channel_identity'])&&!empty($param['channel_identity'])){

    		$where.= " and channel_identity='".$param['channel_identity']."'";
    	}



    	$sql = "select {$select} from sline_community_sync_group where deleted_at=0 {$where}";

    	$result = DB::select($sql);

    	return json_decode(json_encode($result),true);
    }

    /**
     * 群推荐商品表数据添加
     * @param array $param
     * @param .....
     * @return array
     */
    public static function insertRecommendGoods($param){

    	return DB::table("recommend_goods")->insert($param);
    }

}