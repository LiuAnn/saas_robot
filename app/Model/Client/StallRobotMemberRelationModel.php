<?php
/**
 * Created by PhpStorm.
 * User: cxy
 * Date: 2020/12/16
 * Time: 16:25
 */

namespace App\Model\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class StallRobotMemberRelationModel extends Model {

    /**
     * 修改推送状态
     * @access
     * @param $where
     * @param $params
     * @return int
     * @version 1.0
     * @author liuwenhao
     */
    public static function updatePushStatus($where , $params){

        $result = self::getConnectionDB()
            ->table(self::$table_name)
            ->where($where)
            ->update($params);

        return $result;
    }

    /**
     * 获取助理开关状态
     */
    public static function getPushStatus($mid){
        $where['mid'] = $mid;
        $result = Db::table("robot_member_relation")
            ->where($where)
            ->get();
        return json_decode( json_encode( $result),true);
//        return empty($result) ? 0 :$result->;
    }

    /**
     * 获取是否开启推送状态
     * @access
     * @param $where
     * @return mixed
     * @author liuwenhao
     * @version 1.0
     */
    public static function getFirstInfo($where){

        $result = Db::table("robot_member_relation")
            ->where($where)
            ->first();
        return json_decode(json_encode($result) , true);
    }

    /**
     * 添加一条数据
     * @access
     * @param $params
     * @return bool
     * @author liuwenhao
     * @version 1.0
     */
    public static function insertData($params){

        $result = self::getConnectionDB()
            ->table(self::$table_name)
            ->insert($params);
        return $result;
    }


    /**
     * 条件查询
     * @access
     * @param $where
     * @return mixed
     * @author liuwenhao
     * @version 1.0
     */
    public static function getWhereProduct($where){

        $result = DB::table("stall_assistant")
            ->where($where)
            ->first();

        return json_decode(json_encode($result) , true);
    }


    public static function getCount($where){

        $result = DB::table("stall_assistant")
            ->where($where)
            ->count();

        return $result;
    }


    /**
     * 获取悦淘群群主邀请码
     * @param $wxid
     * @return array
     */
    public static function getGroupOwerIdCodeNum($mid)
    {
        $sql = "select code_number from yuelvhui. sline_invitation_code mid=? ";
        $result = DB::selectOne($sql, [$mid]);
        return !empty($result) ? $result->code_number : 0;
    }



    /**
     * 查询推送时间最新的一条
     * @access
     * @author 
     * @version 1.0
     */
    public static function sline_community_stall_assistant($where){

        $sql = "select * from mall_stall_assistant";
        $sql .= " where mid = " . $where['mid'];
        $sql .= " and push_status = " . $where['push_status'];
        $sql .= " and delete_time = " . $where['delete_time'];
        $sql .= " order by push_start_time desc limit 1";

        $result = DB::select($sql);

        $result = array_map("get_object_vars" , $result);

        return $result;
    }

    /**
     * 添加推送商品
     * @access
     * @param $data
     * @return bool
     * @author 
     * @version 1.0
     */
    public static function insertProduct($data){

        $result = DB::table("stall_assistant")->insert($data);
        return $result;
    }

    /**
     * 获取单条商品
     * @access
     * @param $where
     * @return mixed
     * @author 
     * @version 1.0
     */
    public static function getFirstProduct($where){

        $result = DB::table("stall_assistant")
            ->where($where)
            ->first();
        return json_decode(json_encode($result) , true);
    }

    /**
     * 删除推送商品
     * @access
     * @param $where
     * @return mixed
     * @author 
     * @version 1.0
     */
    public static function delProduct($where){

        $result = DB::table("stall_assistant")
            ->where($where)
            ->update(['delete_time' => time()]);

        return json_decode(json_encode($result) , true);
    }


    /**
     * 更新是否推送成功状态
     * @access
     * @param $where
     * @param $params
     * @return int
     * @version 1.0
     * @author 
     */
    public static function updateProductStatus($where , $params){

        $result = DB::table("stall_assistant")
            ->where($where)
            ->update($params);

        return $result;
    }


    /**
     * 获取未推送列表
     * @access
     * @author 
     * @version 1.0
     */
    public static function findNoPushList(){

        $sql = "select * from sline_community_stall_assistant";

        $sql .= " where push_status = 0 and push_start_time < " . time();

        $result = DB::select($sql);

        $result = array_map("get_object_vars" , $result);

        return $result;
    }


    public static function getGoodsInfoByGoodsId($params)
    {
        $result = DB::table("recommend_goods")
            ->where($params)
            ->first();

        return json_decode(json_encode($result) , true);

    }


}