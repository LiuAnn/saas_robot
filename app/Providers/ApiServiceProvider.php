<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        \Response::macro('success', function ($data = []) {
            $returnData = [
                'code' => 200,
                'msg' => '成功',
                'data' => $data,
            ];
            return response()->json($returnData);
        });

        \Response::macro('fail', function ($code, $data = null) {
            $returnData = [
                'code' => $code,
                'msg' => '',
                'data' => []
            ];
            if (!empty($data) && is_string($data)) {
                //这里为了兼容原来的写法特殊
                $returnData = [
                    'code' => $code,
                    'msg' => $data,
                    'data' => []
                ];
            }
            if(!$returnData['msg']){
                $errorCodes = config('errorCode.code');
                $returnData['msg'] = $errorCodes[$code] ?? '';
            }
            return response()->json($returnData);
        });

    }



    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
