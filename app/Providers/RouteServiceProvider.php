<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();

        $this->mapMemberApiRoutes();
        $this->mapAdminApiRoutes();
        $this->mapUserRoutes();
        $this->mapWebRobotRoutes();
        $this->mapWebCommuntiyRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
    /**
     * 会员卡路由
     */
    protected function mapMemberApiRoutes() {
        Route::prefix("api/member")
            ->namespace($this->namespace)
            ->group(base_path("routes/member.php"));
    }

    /*
     * 管理员路由
     */
    protected function mapAdminApiRoutes(){
        Route::prefix("admin")
            ->namespace($this->namespace)
            ->group(base_path("routes/admin.php"));
    }

    /**
     * Define the "robot" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRobotRoutes(){
        Route::prefix("api/robot")
            ->namespace($this->namespace)
            ->group(base_path("routes/robot.php"));
    }
    /**
     * Define the "community" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebCommuntiyRoutes(){
        Route::prefix("api/community")
            ->namespace($this->namespace)
            ->group(base_path("routes/community.php"));
    }

    //用户
    protected function mapUserRoutes(){
        Route::prefix("api/user")
            ->namespace($this->namespace)
            ->group(base_path("routes/user.php"));
    }

}
