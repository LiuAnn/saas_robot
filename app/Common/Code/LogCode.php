<?php
/**
 * Created by PhpStorm.
 * User: EDZ
 * Date: 2020/3/22
 * Time: 20:00
 */
namespace App\Common\Code;

class LogCode {

    //操作类型
    const CREATE_TYPE = 1; //创建
    const UPDATE_TYPE = 2; //修改
    const DELETE_TYPE = 3; //删除

    //修改数据类型
    const INFO_ARRAY = 1; //一维数组
    const LIST_ARRAY = 2; //二维数组

    //修改或添加主键类型
    const LIVE_CODE  = 1; //视频直播活动
    const GOODS_CODE = 2; //商品


}