<?php
/**
 * Created by PhpStorm.
 * User: EDZ
 * Date: 2020/3/22
 * Time: 19:57
 */
namespace App\Common\Log;

use App\Common\Code\LogCode;
use Illuminate\Support\Facades\DB;

class OperateLog {

    public static function saveLog($memberId = 0,$logArr = array()) {
        if (!$memberId) {
            return ['code' => 400 , 'msg' => '操作人必须存在'];
        }

        if (empty($logArr['path']) || empty($logArr['type']) || empty($logArr['message'])) {
            return ['code' => 400 , 'msg' => '参数不能为空'];
        }

        if ($logArr['type'] == LogCode::CREATE_TYPE || $logArr['type'] == LogCode::UPDATE_TYPE) {
            if (!isset($logArr['input']) || empty($logArr['input'])) {
                return ['code' => 400 , 'msg' => '新增请填写参数'];
            }
        }

        if ($logArr['type'] == LogCode::UPDATE_TYPE) {
            if (!isset($logArr['changeData'])) {
                return ['code' => 400 ,'msg' => '修改请填写修改内容'];
            }
        }

        $logData = [
            'admin_user_id'  => $memberId,
            'path'           => $logArr['path'],
            'input'          => json_encode($logArr['input']),
            'created_time'   => time(),
            'type'           => $logArr['type'],
            'change_data'    => json_encode($logArr['changeData']),
            'message'        => $logArr['message'],
            'data_type'      => $logArr['dataType'],
            'relevance_id'   => empty($logArr['relevanceId']) ? 0 : $logArr['relevanceId'],
            'relevance_type' => empty($logArr['relevanceType']) ? 0 : $logArr['relevanceType'],
        ];

        $adminLog = DB::connection('mysql')->table('sline_admin_log')->insert($logData);
        if ($adminLog) {
            return ['code' => 200 , 'msg' => '添加日志成功'];
        } else {
            return ['code' => 200 , 'msg' => '添加失败'];
        }

    }

    /**
     * @Notes: 检测修改的参数
     * @Author: Yzk
     * @Date:2020/3/23 16:56
     * @param array $mark
     * @param array $oldMark
     * @param int $id
     * @return array
     */
    public static function checkUpdate($mark = array(), $oldMark = array(),$id = 0) {

        if (empty($mark)) {
            return ['code' => 400 ,'msg' => '参数为空'];
        }

        $edit = array();
        if (!empty($oldMark)) {
            foreach ($mark as $key => $value) {
                if (isset($oldMark[$key])) {
                    if ($value != $oldMark[$key]) {
                        $edit[] = $key.': 将  '.$oldMark[$key].' 修改为 : ' . $value . ' -----修改主键-----'.$id.'--';
                    }
                } else {
                    $edit[] = '字段 : ' . $key.' 值为 : ' .$value . ' -----修改主键-----'.$id.'--';
                }
            }
        } else {
            $edit[] = '子数据新增，新增子数据标识 : ' . $id;
        }


        return $edit;

    }

    public static function getLogList($data = [],$offset = 0, $size = 10) {
        $where = '1=1';

        if (isset($data['admin_user_id']) && !empty($data['admin_user_id'])) {
            $where .= " and admin_user_id = {$data["admin_user_id"]}";
        }

        if (isset($data['start_time']) && !empty($data['start_time'])) {
            $where .= " and created_time > {$data['start_time']}";
        }

        if (isset($data['end_time']) && !empty($data['end_time'])) {
            $where .= " and created_time < {$data['end_time']}";
        }

        if (isset($data['type']) && !empty($data['type'])) {
            $where .= " and type = {$data['type']}";
        }

        if (isset($data['relevance_id']) && !empty($data['relevance_id'])) {
            $where .= " and relevance_id = {$data['relevance_id']}";
        }

        if (isset($data['relevance_type']) && !empty($data['relevance_type'])) {
            $where .= " and relevance_type = {$data['relevance_type']}";
        }

        if (isset($data['message']) && !empty($data['message'])) {
            $where .= " and message like '%{$data['message']}%''";
        }

        $sql = "select id,admin_user_id,path,created_time,type,message,data_type,relevance_id,relevance_type from "
             . "sline_admin_log where {$where} limit ?,?";

        $adminLogs = DB::connection('mysql')->select($sql,[$offset,$size]);

        return $adminLogs;

    }

    public static function getLogCount($data = []) {
        $where = '1=1';

        if (isset($data['admin_user_id']) && !empty($data['admin_user_id'])) {
            $where .= " and admin_user_id = {$data["admin_user_id"]}";
        }

        if (isset($data['start_time']) && !empty($data['start_time'])) {
            $where .= " and created_time > {$data['start_time']}";
        }

        if (isset($data['end_time']) && !empty($data['end_time'])) {
            $where .= " and created_time < {$data['end_time']}";
        }

        if (isset($data['type']) && !empty($data['type'])) {
            $where .= " and type = {$data['type']}";
        }

        if (isset($data['relevance_id']) && !empty($data['relevance_id'])) {
            $where .= " and relevance_id = {$data['relevance_id']}";
        }

        if (isset($data['relevance_type']) && !empty($data['relevance_type'])) {
            $where .= " and relevance_type = {$data['relevance_type']}";
        }

        if (isset($data['message']) && !empty($data['message'])) {
            $where .= " and message like '%{$data['message']}%''";
        }

        $sql = "select count(*) as count from sline_admin_log where {$where}";

        $adminLogs = DB::connection('mysql')->selectOne($sql);

        return empty($adminLogs->count) ? 0 : $adminLogs->count;

    }

}
