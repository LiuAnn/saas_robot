<?php

/*
 * 判断是否时间戳
 */
function is_timestamp($timestamp) {
    if (!is_numeric($timestamp)) return false;
    if(strtotime(date('Y-m-d H:i:s',$timestamp)) == $timestamp) {
        return $timestamp;
    } else return false;
}

/*
 * curlPost提交数据
 */
function curlPost($url,$data=array()){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        // POST数据

        curl_setopt($ch, CURLOPT_POST, 1);

        // 把post的变量加上

        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;

 }


/**
 * 以get访问模拟访问
 * @param string $url 访问URL
 * @param array $query GET数
 * @param array $options
 * @throws Exception
 * @return bool|string
 */
function curlGet($url, $query = [], $options = [])
{
    $options['query'] = $query;
    return doRequest('get', $url, $options);
}

/**
* 以post访问模拟访问
* @param string $url 访问URL
* @param array $data POST数据
* @param array $options
    * @throws Exception
* @return bool|string
    */
function curlPostCommon($url, $data = [], $options = [])
{
    $options['data'] = $data;
    return doRequest('post', $url, $options);
}

/**
 * CURL模拟网络请求
 * @param string $method 请求方法
 * @param string $url 请求方法
 * @param array $options 请求参数[headers,data,ssl_cer,ssl_key]
 * @throws Exception
 * @return bool|string
 */
function doRequest($method, $url, $options = [])
{
    $curl = curl_init();
    // GET参数设置
    if (!empty($options['query'])) {
        $url .= (stripos($url, '?') !== false ? '&' : '?') . http_build_query($options['query']);
    }
    // CURL头信息设置
    if (!empty($options['headers'])) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $options['headers']);
    }
    // POST数据设置
    if (strtolower($method) === 'post') {
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($options['data']));
    }
    // 证书文件设置
    if (!empty($options['ssl_cer'])) {
        if (file_exists($options['ssl_cer'])) {
            curl_setopt($curl, CURLOPT_SSLCERTTYPE, 'PEM');
            curl_setopt($curl, CURLOPT_SSLCERT, $options['ssl_cer']);
        } else {
            throw new Exception("Certificate files that do not exist. --- [ssl_cer]");
        }
    }
    // 证书文件设置
    if (!empty($options['ssl_key'])) {
        if (file_exists($options['ssl_key'])) {
            curl_setopt($curl, CURLOPT_SSLKEYTYPE, 'PEM');
            curl_setopt($curl, CURLOPT_SSLKEY, $options['ssl_key']);
        } else {
            throw new Exception("Certificate files that do not exist. --- [ssl_key]");
        }
    }
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    list($content, $status) = [curl_exec($curl), curl_getinfo($curl), curl_close($curl)];
    return (intval($status["http_code"]) === 200) ? $content : false;
}

/*
 * 中台数据接口请求
 */
 function requestByPost($url, $data)
{
    $microtime = time() * 1000;
    $sysId     = 251;
    $sysPwd    = "b54b5f4f9fa046bc881c52e8014c43e9";

    $str     = "{$sysId}.{$microtime}.{$sysPwd}";
    $sign    = md5($str);
    $headers = [
        "Content-Type: application/json",
        "Authorization:  Sys {$sysId}.{$microtime}.{$sign}"
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);    // 提交地址
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($ch, CURLOPT_HEADER, 0); // 设置头文件的信息作为数据流输出
    curl_setopt($ch, CURLOPT_POST, 1); // POST
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);
    $result = json_decode($output, true);
    return $result;
}


/*
 * 获取产品类型名称根据商品类型ID
 */
function getGoodTypeNameByGoodType($goodType=1){

               switch ($goodType) {
                   case '1':
                       $goodsTypeName = '自营';
                       break;
                   case '2':
                       $goodsTypeName = '京东';
                       break;
                   case '3':
                       $goodsTypeName = '拼多多';
                       break;
                   case '4':
                       $goodsTypeName = '唯品会';
                       break;
                   case '5':
                       $goodsTypeName = '美团';
                       break;
                   case '7':
                       $goodsTypeName = '淘宝';
                       break;
                   case '8':
                       $goodsTypeName = '苏宁';
                       break;
                   case '9':
                       $goodsTypeName = '品牌折扣券';
                       break;
                   default:
                       $goodsTypeName = '其他';
                       break;
               }


            return $goodsTypeName;

}

/*
 * 获取渠道类型名称根据渠道类型ID
 */
function getChannelNameChannel($channel=1){

                switch ($channel) {
                    case '1':
                        $channelName = '悦淘';
                        break;
                    case '2':
                        $channelName = '大人';
                        break;
                    case '3':
                        $channelName = '迈图';
                        break;
                    case '4':
                        $channelName = '直订';
                        break;
                    default:
                        $channelName = '其他';
                        break;
                }


                return $channelName;
    
}


function xmlToArray($xml){
    //禁止引用外部xml实体
    libxml_disable_entity_loader(true);

    $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);

    $val = json_decode(json_encode($xmlstring),true);

    return $val;
}


/**
 * 数组转XML内容
 * @param array $data
 * @return string
 */
function arr2xml($data)
{
    //return "<xml>" . _arr2xml($data) . "</xml>";
    return '<?xml version="1.0"?><msg>' . _arr2xml($data) . '</msg>';
}

/**
 * XML内容生成
 * @param array $data 数据
 * @param string $content
 * @return string
 */
function _arr2xml($data, $content = '')
{
    foreach ($data as $key => $val) {
//        dd($data);
        $replaceStatus = 0;
//        if (key($val) == '@attributes') {
//            dd($val);
//        }
        if (in_array($key, ['username','appid','weappiconurl','pagepath','shareId','sharekey','sharename'])) {
            $replaceStatus = 1;
        }
        is_numeric($key) && $key = 'item';
        if ($key == '@attributes') {
            foreach ($val as $k=>$v) {
                $content .= ' ' . $k . '=' . "'" . $v . "'";
            }
            $content .= '>';
            continue;
        }
        $content .= "<{$key}>";
        if (is_array($val) || is_object($val)) {
//            $content .= _arr2xml($val);
            if (key($val) == '@attributes') {
                $content = substr($content, 0, -1);
                $content .= _arr2xml($val);
            } else {
                if (empty($val) && $key != 'emoticonmd5' && $key != 'pagepath') {
                    $content  = substr($content, 0, -1);
                    $content .= ' />';
                    continue;
                }
                $content .= _arr2xml($val);
            }
        } elseif (is_string($val) && $replaceStatus) {
            $content .= '<![CDATA[' . preg_replace("/[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]/", '', $val) . ']]>';
        } else {
            if ($key == 'url') {
                $val = str_replace("&","&amp;",$val);
            }
            $content .= $val;
        }

        $content .= "</{$key}>";
    }
    return $content;
}