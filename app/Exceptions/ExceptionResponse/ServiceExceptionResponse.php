<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */
namespace App\Exceptions\ExceptionResponse;

use Illuminate\Http\Request;

/**
 * 业务异常处理类
 */
class ServiceExceptionResponse extends ExceptionResponseAbstract
{
    public function __construct(\Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);
    }

    public function response()
    {
        $response['code'] = $this->getErrorCode('service_error_code');

        $response['message'] = $this->exception->getMessage();

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('service_error'));
    }
}
