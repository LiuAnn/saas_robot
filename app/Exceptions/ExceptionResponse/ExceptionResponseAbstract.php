<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */

namespace App\Exceptions\ExceptionResponse;

use \Exception;
use Illuminate\Http\Request;

/**
 * Class ExceptionResponseAbstract
 * @package App\Exceptions\ExceptionResponse
 * 系统异常处理类父类
 */
abstract class ExceptionResponseAbstract
{
    protected $exception = null;

    protected $request = null;

    protected $comHttpCode = [];

    protected $moduleName = [];

    //protected $moduleCodes = [];

    public function __construct(Exception $exception, Request $request)
    {
        $this->exception = $exception;

        $this->request = $request;

        $this->comHttpCode = Config('errors.com_http_code');

        $request->route() ? $this->moduleName = trim($request->route()->getPrefix(),'/') : '';

    }

    protected function getComHttpCode($type)
    {
        return array_get($this->comHttpCode,$type);
    }

    protected function getErrorCode($type)
    {
        if($this->moduleName && array_get(Config("errors.modules.{$this->moduleName}",[]), $type)){
            return array_get(Config("errors.modules.{$this->moduleName}",[]), $type);
        }else{
            return array_get(Config("errors.modules.common",[]), $type);
        }
    }

    abstract protected function response();

}
