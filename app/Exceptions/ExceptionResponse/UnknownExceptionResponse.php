<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */
namespace App\Exceptions\ExceptionResponse;

use Illuminate\Http\Request;

/**
 * Class UnknownExceptionResponse
 * @package App\Exceptions\ExceptionResponse
 * 未知异常处理类
 */
class UnknownExceptionResponse extends ExceptionResponseAbstract
{
    public function __construct(\Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);
    }

    public function response()
    {
        $response['code'] = $this->getErrorCode('unknown_error_code');

        $response['message'] = env('APP_DEBUG') === false ? '未知错误' :
            $this->exception->getMessage();

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('unknown_error'));
    }
}
