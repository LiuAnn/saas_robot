<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */

namespace App\Exceptions\ExceptionResponse;
use Exception;
use Illuminate\Http\Request;

/**
 * 身份验证失败异常处理类
 * Class AuthorizationExceptionResponse
 * @package App\Exceptions
 */
class AuthorizationExceptionResponse extends ExceptionResponseAbstract
{
    public function __construct(Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);
    }

    public function response()
    {
        //自定义错误代码
        $response['code'] = $this->getErrorCode('ertification_fail_code');

        //自定义错误提示信息
        $response['messages'] = '登录身份验证失败，登录会话过期或登录身份不合法';

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('ertification_fail'));

    }
}
