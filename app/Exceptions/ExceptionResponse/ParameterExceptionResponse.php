<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */

namespace App\Exceptions\ExceptionResponse;

use Exception;
use Illuminate\Http\Request;

/**
 * Class ParameterExceptionResponse
 * @package App\Exceptions\ExceptionResponse
 * 接口参数异常处理类
 */
class ParameterExceptionResponse extends ExceptionResponseAbstract
{
    //路由名称
    protected $routerName = null;

    //错误验证信息
    protected $errorMsg = null;

    //初始化验证异常处理类
    public function __construct(Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);

        $this->routerName = $request->route()->getName();

        $this->errorMsg = json_decode($exception->getMessage(), true);

    }

    //拼接错误信息结构
    final public function response()
    {
        //自定义错误代码
        $response['code'] = $this->getErrorCode('parameters_error_code');

        //自定义错误提示信息
        $response['messages'] = '参数验证失败';

        //错误详情
        $response['errors'] = [];
        foreach ($this->errorMsg as $key => $errors) {
            foreach ($errors as $error) {
                $subError = [
                    'locationType' => 'parameter',
                    'location' => $key,
                    'reason' => 'invalidParameter',
                    'message' => $error,
                ];
                $response['errors'][] = $subError;
            }
        }

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('validate_error'));
    }

}
