<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */
namespace App\Exceptions\ExceptionResponse;

use Illuminate\Http\Request;

/**
 * Class MethodNotAllowedHttpExceptionResponse
 * @package App\Exceptions\ExceptionResponse
 * http请示method不受支持异常处理类
 */
class NotFoundHttpExceptionExceptionResponse extends ExceptionResponseAbstract
{
    public function __construct(\Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);
    }

    public function response()
    {
        $response['code'] = $this->getErrorCode('unknown_error_code');

        $response['message'] = '请求地址错误';

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('not_found'));
    }


}
