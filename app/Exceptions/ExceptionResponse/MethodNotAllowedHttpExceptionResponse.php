<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */
namespace App\Exceptions\ExceptionResponse;

use Illuminate\Http\Request;

/**
 * Class MethodNotAllowedHttpExceptionResponse
 * @package App\Exceptions\ExceptionResponse
 * http请示method不受支持异常处理类
 */
class MethodNotAllowedHttpExceptionResponse extends ExceptionResponseAbstract
{
    public function __construct(\Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);
    }

    public function response()
    {
        $response['code'] = $this->getErrorCode('unsupport_method_code');

        $response['message'] = 'http请求method不受支持';

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('unsupport_method'));
    }


}
