<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */

namespace App\Exceptions\ExceptionResponse;

use \Exception;
use Illuminate\Support\Facades\Request;

/**
 * Class ModelNotFoundExtendExceptionResponse
 * @package App\Exceptions\ExceptionResponse
 * 模型不存在异常处理类
 */
class ModelNotFoundExtendExceptionResponse
{
    protected $exception = null;

    public function __construct(Exception $exception)
    {
        $this->exception = $exception;
    }

    public function response()
    {
        //获取自定义错误代码
        $response['code'] = Config('errors.modules.common.model_not_found_code');

        //自定义错误提示信息
        $response['messages'] = $this->exception->getMessage();

        return response()->json($response)
            ->setStatusCode(Config('errors.com_http_code.service_error'));
    }
}
