<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */
namespace App\Exceptions\ExceptionResponse;

use Illuminate\Http\Request;

/**
 * Class RequestBodyVerificationExceptionResponse
 * @package App\Exceptions\ExceptionResponse
 * 接口参数格式异常处理类
 */
class RequestBodyVerificationExceptionResponse extends ExceptionResponseAbstract
{
    public function __construct(\Exception $exception, Request $request)
    {
        parent::__construct($exception, $request);
    }

    public function response()
    {
        $response['code'] = $this->getErrorCode('unsupport_paramter_formatter_code');

        $response['message'] = '不可接受的传入参数格式';

        return response()->json($response)
            ->setStatusCode($this->getComHttpCode('unsupport_paramter_formatter'));
    }


}
