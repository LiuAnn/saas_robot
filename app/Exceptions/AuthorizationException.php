<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/25
 * Time: 19:02
 */

namespace App\Exceptions;

/**
 * 身份验证失败异常类
 * Class AuthorizationException
 * @package App\Exceptions
 */
class AuthorizationException extends \Exception
{
    public function __construct($msg = '')
    {
        parent::__construct($msg);
    }

}
