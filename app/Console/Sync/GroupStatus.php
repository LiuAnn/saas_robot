<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 21/01/14
 * Time: 下午09:07
 * Describe : 同步群状态
 */

namespace App\Console\Sync;

use App\Service\Sync\GroupService;
use Illuminate\Console\Command;

class GroupStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setGroupStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步群状态';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public static function handle()
    {
        (new GroupService())::syncGroupStatus();
    }
}
