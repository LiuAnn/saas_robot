<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/11/29
 * Time: 上午11:43
 * Describe : 同步群统计相关95%的内容
 */

namespace App\Console\Sync;

use App\Service\Sync\GroupService;
use Illuminate\Console\Command;

class Group extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncGroup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步群统计相关95%的内容';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new GroupService())::setSyncByGroup();
    }
}
