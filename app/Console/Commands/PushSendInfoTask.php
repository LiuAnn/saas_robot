<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/16
 * Time: 下午02:21
 * Describe: 发送队列中的消息
 */
namespace App\Console\Commands;

use App\Service\Platform\SendInfoService;
use Illuminate\Console\Command;

class PushSendInfoTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pushSendInfoTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '发送队列中的消息';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Describe:  从队列拉数据--探鲸
     * @return mixed
     */
    public static function handle()
    {
        SendInfoService::sendToBePushByRedis();
    }
}
