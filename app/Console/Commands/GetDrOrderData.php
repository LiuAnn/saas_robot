<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use  App\Service\Community\PullTaskRedisDataService;
class GetDrOrderData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetDrOrderData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PullTaskRedisDataService::insertDrOrderData(1);
        PullTaskRedisDataService::insertDrOrderData(2);
        PullTaskRedisDataService::insertDrOrderData(3);       
    }
}
