<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;


use  App\Service\Community\PullTaskRedisDataService;

class GetJdOrderData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetJdOrderData{--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * date 自定义拉取订单的开始时间（最好不要超过当前时间7天，格式：2020-10-13）
     * @return mixed
     */
    public function handle()
    {
        //悦淘拉取京东订单
        $date = $this->option('date') ? $this->option('date') : '';
        PullTaskRedisDataService::insertTripartiteOrderData(2, 0, $date);
    }
}
