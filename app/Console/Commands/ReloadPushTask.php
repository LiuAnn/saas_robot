<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/18
 * Time: 下午02:21
 * Describe: 补发消息
 */
namespace App\Console\Commands;

use App\Service\Platform\SendInfoService;
use Illuminate\Console\Command;

class ReloadPushTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reloadPushTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '补发消息';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Describe:  从发送记录中拉数据--探鲸
     * @return mixed
     */
    public static function handle()
    {
        SendInfoService::reloadPushTask();
    }
}
