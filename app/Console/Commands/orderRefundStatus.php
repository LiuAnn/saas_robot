<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Service\Mall\MallOrderService;


class orderRefundStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orderRefundStatus:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'yuelvhui and middle orderRefundStatus change';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = date('Y-m-d H:i:s',time());
        Log::info("orderGoodsRefundStatusUpdate : {$time}");
        MallOrderService::updateOrderGoodsRefundStatus();
    }
}
