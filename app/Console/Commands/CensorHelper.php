<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 21/01/07
 * Time: 下午06:45
 * Describe: 同步敏感词
 */
namespace App\Console\Commands;

use App\Service\MessageCenter\CensorHelperService;
use Illuminate\Console\Command;

class CensorHelper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censorHelper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '同步敏感词列表';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Describe:  同步敏感词列表
     * @return mixed
     */
    public static function handle()
    {
        CensorHelperService::setCensorHelper();
    }
}
