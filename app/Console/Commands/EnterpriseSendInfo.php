<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/11/20
 * Time: 下午03:08
 * Describe: 企业微信外部群消息推送
 */

namespace App\Console\Commands;

use App\Models\SendReload;
use App\Models\SendRoomReload;
use App\Service\Platform\EnterpriseService;
use Illuminate\Console\Command;
use App\Models\SendInfo as Send_Info;
use App\Models\MemberInfoByAppID;
use App\Models\WhiteListWx;
use App\Models\User;

class EnterpriseSendInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enterpriseSendInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '企业微信外部群消息推送';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public static function handle()
    {
        error_log('时间：' . date('Y-m-d H:i:s'), 3, storage_path('logs/enterpriseSendInfo.log'));

        $send_info = Send_Info::where('is_send_enterprise', 0)
            ->where('is_send', 1)
            ->orderBy('id', 'ASC')
            ->limit(50)
            ->get();

        if (empty($send_info)) {
            return false;
        }

        $push_data = [
            'msg_num' => 1,
            'msg_type' => '',
            'msg_content' => '',
            'voice_time' => 0,
            'href' => '',
            'title' => '',
            'desc' => ''
        ];


        foreach ($send_info as $key => $val) {
            // 先判断是否现在需要发送
            if ($val['is_timing_send'] == 0 || $val['timing_send_time'] < time()) {
                // 看发送的用户身份，在选择下面的机器人
                $user_info = User::select('id', 'user_type', 'sup_id')
                    ->where('mid', $val['mid'])
                    ->where('user_type', 2)
                    ->where('delete_at', 0)
                    ->first();
                if (empty($user_info)) {
                    continue;
                }

                $where = [];
                if ($user_info['sup_id'] > 0) {
                    $where = [
                        'bind_mid' => $val['mid']
                    ];
                } elseif ($user_info['sup_id'] == 0) {
                    $where = [
                        'mid' => $val['mid']
                    ];
                }

                //看那些用户需要发送
                $push_user_list = WhiteListWx::select('id', 'enterprise_robot_serial_no')
                    ->where($where)
                    ->where('deleted_at', 0)
                    ->where('platform', 2)
                    ->get();

//                error_log('要发的用户：' . json_encode($push_user_list) . "\n" . '时间：' . date('Y-m-d H:i:s'), 3, storage_path('logs/enterpriseSendInfo.log'));

                if (empty($push_user_list)) {
                    continue;
                }

                foreach ($push_user_list as $user => $user_val) {
                    $push_group_list = MemberInfoByAppID::select('id', 'enterprise_group_serial_no')
                        ->where('enterprise_robot_serial_no', $user_val['enterprise_robot_serial_no'])
                        ->where('is_delete', 0)
                        ->whereIn('tag_id', [$val['tag_ids']])
                        ->where('enterprise_group_serial_no', '!=', '')
                        ->get();

                    if (empty($push_group_list)) {
                        continue;
                    }

//                    error_log('要发的群：' . json_encode($push_group_list) . "\n" . '时间：' . date('Y-m-d H:i:s'), 3, storage_path('logs/enterpriseSendInfo.log'));

                    foreach ($push_group_list as $push_key => $push_val) {
                        $push_data['msg_type'] = Send_Info::$enterprise_type[$val['type']];
                        // 文本
                        if ($val['type'] == 1) {
                            $content = rtrim($val->content) . '👉' . $val['short_url'];
                            $push_data['msg_content'] = $content;
                        }
                        // 图文
                        if ($val['type'] == 2) {
                            $push_data['msg_content'] = $val->content;
                        }
                        // 视频
                        if ($val['type'] == 3) {
                            /**
                             * 视频图和时长目前没有，先用悦淘的logo
                             */
                            $push_data['msg_content'] = '';
                            $push_data['voice_time'] = 10;
                            $push_data['href'] = $val->content;
                        }
                        // 直播间
                        if ($val['type'] == 4) {
                            $content = rtrim($val->content) . '👉' . $val['short_url'];
                            $push_data['msg_content'] = $content;
                        }
                        // 优惠券
//                        if ($val['type'] == 5) {
//                            $push_data['msg_content'] = $val->content;
//                        }
                        //
                        if ($val['type'] == 6) {
                            $content = rtrim($val->content) . '👉' . $val['short_url'];
                            $push_data['msg_content'] = $content;
                        }
                        // cps活动
                        if ($val['type'] == 7) {
                            $content = rtrim($val->content) . '👉' . $val['short_url'];
                            $push_data['msg_content'] = $content;
                        }
                        if ($val['type'] == 8) {
                            $push_data['msg_content'] = $content;
                        }

//                        //查看当前微信号有没有推送
                        $send_reload = SendReload::where('random_str', $val['random_str'])
                            ->where('enterprise_robot_serial_no', $user_val['enterprise_robot_serial_no'])
                            ->first();
                        if (empty($send_reload)) {
                            SendReload::create(
                                [
                                    'random_str' => $val['random_str'],
                                    'wxid' => '',
                                    'enterprise_robot_serial_no' => $user_val['enterprise_robot_serial_no']
                                ]);
                            $count = SendRoomReload::where('random_str', $val['random_str'])
                                ->where('enterprise_group_serial_no', $push_val['enterprise_group_serial_no'])
                                ->count();
                            if ($count == 0) {
                                SendRoomReload::create(
                                    [
                                        'random_str' => $val['random_str'],
                                        'link_type' => $val['link_type'],
                                        'wxid' => '',
                                        'room_wxid' => '',
                                        'room_id' => $push_val['id'],
                                        'enterprise_robot_serial_no' => $user_val['enterprise_robot_serial_no'],
                                        'enterprise_group_serial_no' => $push_val['enterprise_group_serial_no']
                                    ]
                                );
                            }
                        }
                        //推送消息
                        $push = EnterpriseService::sendInfo($user_val['enterprise_robot_serial_no'], $push_val['enterprise_group_serial_no'], $push_data);
                        if ($push['code'] != 1){
                            error_log('Error：' . json_encode($push) . "\n" . '时间：' . date('Y-m-d H:i:s'), 3, storage_path('logs/enterpriseSendInfo.log'));
                            continue;
                        }
                        error_log('发送的内容ID：' . json_encode($val['id']) . "\n" . '时间：' . date('Y-m-d H:i:s'), 3, storage_path('logs/enterpriseSendInfo.log'));
                    }
                }
                //全发完了修改状态
                Send_Info::where('id', $val['id'])->update(['is_send_enterprise' => 1]);
            }
        }
    }
}
