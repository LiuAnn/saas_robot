<?php
/**
 * Created by PhpStorm.
 * User: Elkan
 * Date: 20/12/16
 * Time: 下午02:21
 * Describe: 群素材插入到队列脚本
 */
namespace App\Console\Commands;

use App\Service\Platform\SendInfoService;
use Illuminate\Console\Command;

class SendInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '群素材插入到队列脚本';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Describe:  素材发送队列--探鲸
     * @return mixed
     */
    public static function handle()
    {
        SendInfoService::getToBeSentList();
    }
}
