<?php

namespace App\Console;

use  App\Service\Source\LotteryDrawService;
use  App\Service\Community\PullTaskRedisDataService;
use  App\Service\Member\MemberService;
use  App\Service\Community\GroupTaskService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\GetPullTaskData::class,    // 添加任务类
        \App\Console\Commands\TestMyTask::class,         // 添加任务类
        \App\Console\Commands\CommondMsgCache::class,    // 添加任务类
        \App\Console\Commands\CommondMsgRoomCache::class,    // 添加任务类
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command('inspire')->hourly();
        $schedule->command('GetPullTaskData')->everyFiveMinutes();//素材定时任务
        $schedule->command('WhiteListDataTask')->hourly();   //白名单中绑定的群数量 群主数量
        $schedule->command('CommonGroupNumRobot')->hourly(); //共同群数量
        $schedule->command('LeaveRoomJudge')->hourly(); //机器人群人数五天判断


        $schedule->command('GetYtOrderData')->everyFiveMinutes();  //悦淘自营订单
        $schedule->command('GetJdOrderData')->everyThirtyMinutes();   //悦淘京东
        $schedule->command('GetPddOrderData')->everyThirtyMinutes();  //获取拼多多

        $schedule->command('SupplementJdOrderData')->dailyAt('10:02');  //订单补充
        $schedule->command('SupplementJdOrderData')->dailyAt('18:02');  //订单补充
        $schedule->command('SupplementJdOrderData')->dailyAt('00:02');  //订单补充

        $schedule->command('SupplementYtOrderData')->dailyAt('00:05');   //订单补充
//        $schedule->command('SynJdOrderPriceData')->weeklyOn(0,'00:20');

//        $schedule->command('SupplementPddOrderData')->dailyAt('00:10');   //京东、拼多多补单
        $schedule->command('GroupMemberData')->dailyAt('00:30');//群成员更新
        $schedule->command('GroupMangerData')->dailyAt('01:00');//活跃度统计
        $schedule->command('GetDrOrderData')->everyFiveMinutes();

//        $schedule->command('GroupMemberData')->dailyAt();//群成员更新
//         $schedule->command('GroupMangerData')->dailyAt('13:00'); //每天13:00运行任务 更新群信息
//         $schedule->command('TestMyTask')->everyMinute();
        //机器人缓存生成



       try{
           // 团长任务更改奖金发送状态
           $schedule->call(function() {
               GroupTaskService::updateMoneySendStatus(['platform_identity'=>'c23ca372624d226e713fe1d2cd44be35']);
           })->everyFiveMinutes();
       }catch (\Exception $exception){
           Log::error('GroupTaskService::updateMoneySendStatus error'.$exception->getMessage());
       }


       try{
           // 团长任务同步订单订单
           $schedule->call(function() {
               GroupTaskService::addGroupTaskRelation(['platform_identity'=>'c23ca372624d226e713fe1d2cd44be35']);
           })->everyFiveMinutes();
       }catch (\Exception $exception){
           Log::error('GroupTaskService::addGroupTaskRelation error'.$exception->getMessage());
       }


       try{
           // 团长任务同步订单订单
           $schedule->call(function() {
               GroupTaskService::updateGroupTaskStatus(['platform_identity'=>'c23ca372624d226e713fe1d2cd44be35']);
           })->everyMinute();
       }catch (\Exception $exception){
           Log::error('GroupTaskService::updateGroupTaskStatus error'.$exception->getMessage());
       }


        try{
            // 刘帅分佣统计
            $schedule->call(function() {
                PullTaskRedisDataService::getOrderCommission();
                //})->hourly();
            })->everyFiveMinutes();
        }catch (\Exception $exception){
            Log::error(' PullTaskRedisDataService::getOrderCommission error'.$exception->getMessage());
        }


        try{
            // 素材消息类型为优惠券时给群主发送优惠券
            $schedule->call(function() {
                LotteryDrawService::sendCouponToGroupOwn();
            })->everyFiveMinutes();
        }catch (\Exception $exception){
            Log::error('LotteryDrawService::sendCouponToGroupOwn error'.$exception->getMessage());
        }


        try{
            // 添加发送素材的
            $schedule->call(function() {
                PullTaskRedisDataService::addNewRoomDataToSend();
            })->hourly();
        }catch (\Exception $exception){
            Log::error(' LotteryDrawService::addNewRoomDataToSend  error'.$exception->getMessage());
        }


        try{
            // 拉取悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {
                PullTaskRedisDataService::insertTripartiteOrderData(4);

                PullTaskRedisDataService::insertTripartiteOrderData(5);

                PullTaskRedisDataService::insertTripartiteOrderData(6);

                PullTaskRedisDataService::insertTripartiteOrderData(8);

                PullTaskRedisDataService::insertTripartiteOrderData(9);
            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::insertTripartiteOrderData getOrderList error'.$exception->getMessage());
        }


        try{
            // 拉取大人三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::insertTripartiteOrderData(2,1);

                PullTaskRedisDataService::insertTripartiteOrderData(3,1);

            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::insertTripartiteOrderData getOrderList error'.$exception->getMessage());
        }


        try{
            // 拉取悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(2);

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(3);

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(4);

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(5);

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(6);

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(8);

                PullTaskRedisDataService::insertTripartiteOrderDataProfit(9);
            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::insertTripartiteOrderDataProfit getOrderProfit error'.$exception->getMessage());
        }


        try{
            // 拉取悦淘三方唯品会 苏宁 美团 品牌折扣券等退款订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::insertCancelOrderData(2);

                PullTaskRedisDataService::insertCancelOrderData(3);

                PullTaskRedisDataService::insertCancelOrderData(4);

                PullTaskRedisDataService::insertCancelOrderData(5);

                PullTaskRedisDataService::insertCancelOrderData(6);

                PullTaskRedisDataService::insertCancelOrderData(8);

                PullTaskRedisDataService::insertCancelOrderData(9);

            })->dailyAt('23:45');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::insertCancelOrderData getOrderList error'.$exception->getMessage());
        }
        //

        try{
            // 拉取悦淘三方唯品会 苏宁 美团 品牌折扣券等佣金 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::synTripartiteOrderCommissionData(2);

            })->dailyAt('23:20');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::synTripartiteOrderCommissionData getOrderList error'.$exception->getMessage());
        }

        try{
            // 补单悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::supplementTripartiteOrderData(4);

            })->dailyAt('00:35');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::supplementTripartiteOrderData(4) error'.$exception->getMessage());
        }

        try{
            // 补单悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::supplementTripartiteOrderData(5);

            })->dailyAt('00:45');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::supplementTripartiteOrderData(5) error'.$exception->getMessage());
        }


        try{
            // 补单悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::supplementTripartiteOrderData(6);

            })->dailyAt('00:50');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::supplementTripartiteOrderData(6) error'.$exception->getMessage());
        }

        try{
            // 补单悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::supplementTripartiteOrderData(8);

            })->dailyAt('00:40');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::supplementTripartiteOrderData(8) error'.$exception->getMessage());
        }


        try{
            // 补单悦淘三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团  7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::supplementTripartiteOrderData(9);

            })->dailyAt('00:55');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::supplementTripartiteOrderData(9) error'.$exception->getMessage());
        }

        //素材缓存数据清除  spz
        try{
            $schedule->call(function() {
                PullTaskRedisDataService::delRandomStrCache();
            })->dailyAt('01:35');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::delRandomStrCache error'.$exception->getMessage());
        }


        try{
            // 拉取直订三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::insertTripartiteOrderData(2,9);

                PullTaskRedisDataService::insertTripartiteOrderData(9,9);

                //拉取直订本地生活订单  直订 4 
                PullTaskRedisDataService::insertLocalLifeOrderData(4);

                //拉取本地生活电影票订单 
                PullTaskRedisDataService::insertMovieOrderData();

                //拉取本地生活酒店订单 
                PullTaskRedisDataService::insertHotelOrderData(['channel_id'=>4]);

            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::insertTripartiteOrderData(2,9) getOrderList error'.$exception->getMessage());
        }


        try{
            // 拉取迈图三方唯品会 苏宁 美团 品牌折扣券订单 商品类型  1自营  2京东 3拼多多 4 唯品会 5 美团 6 淘宝 7 肯德基 8 苏宁 9 品牌折扣券
            $schedule->call(function() {

                PullTaskRedisDataService::insertTripartiteOrderData(2,3);

                PullTaskRedisDataService::insertTripartiteOrderData(3,3);

                PullTaskRedisDataService::insertTripartiteOrderData(4,3);

            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::insertTripartiteOrderData(2,3) getOrderList error'.$exception->getMessage());
        }



        try{
            // 拉取悦淘商品真实佣金
            $schedule->call(function() {

                PullTaskRedisDataService::synTripartiteOrderTrueCommissionData(0,1);  //拉取悦淘普通用户实际分佣

                PullTaskRedisDataService::synTripartiteOrderTrueCommissionData(1,1);  //拉取悦淘创客用户实际分佣

                PullTaskRedisDataService::synTripartiteOrderTrueCommissionData(1,2);  //拉取大人用户实际分佣

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(2);

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(3);

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(4);

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(5);

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(6);

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(8);

                PullTaskRedisDataService::synTripartiteCpsOrderTrueCommissionData(9);

            })->dailyAt('01:30');
        }catch (\Exception $exception){
            Log::error('PullTaskRedisDataService::synTripartiteOrderTrueCommissionData getOrderCommission error'.$exception->getMessage());
        }


        try{
            //发放用户冻结奖金
            $schedule->call(function() {

                MemberService::sendUserFrozenMoney();

            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('MemberService::sendUserFrozenMoney error'.$exception->getMessage());
        }


        try{
            
            $schedule->call(function() {

                //发放角色升级时获得的奖励
                MemberService::sendUserIdentityUpBonus();

                //已收货订单分佣
                MemberService::getNotReceiveOrder();

            })->everyThirtyMinutes();
        }catch (\Exception $exception){
            Log::error('MemberService::getNotReceiveOrder error'.$exception->getMessage());
        }

        //探鲸微信发送消息
        $schedule->command('sendInfo')
            ->everyMinute();

        //探鲸企业微信发送消息
        $schedule->command('enterpriseSendInfo')
            ->everyMinute();

        //群消息同步
        $schedule->command('syncGroup')
            ->everyThirtyMinutes();

        //探鲸微信素材队列发送
        $schedule->command('pushSendInfoTask')
            ->everyMinute();

        //素材队列补发消息
        $schedule->command('reloadPushTask')
            ->everyMinute();

        //风控敏感词同步 每天晚上23点执行一次
        $schedule->command('censorHelper')
            ->dailyAt('23:00');

        # 同步删除的群状态到新统计表
        $schedule->command('setGroupStatus')
            ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        $this->load(__DIR__.'/Sync');

        require base_path('routes/console.php');
    }
}
