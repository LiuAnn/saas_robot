<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */
    'default' => env('DB_CONNECTION', 'mysql'),
    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */
    'connections' => [

        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix'   => '',
        ],
        //小悦悦
        'mysql'       => [
            'driver'    => 'mysql',
            'read' =>[
                'host'      => env('DB_READ_HOST', '127.0.0.1'),//'172.21.0.118',
                'password'  => env('DB_PASSWORD', ''),
            ],
            'write' => [
                'host'      => env('DB_HOST', '127.0.0.1'),
                'password'  => env('DB_PASSWORD', ''),
            ],
            'port'      => env('DB_PORT', '3306'),
            'database'  => env('DB_DATABASE', 'forge'),
            'username'  => env('DB_USERNAME', 'forge'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => 'sline_community_',
            'strict'    => false,
            'engine'    => null,
        ],
        //小悦悦
        'robot'       => [
            'driver'    => 'mysql',
            'read' =>[
                'host'      => env('DB_READ_HOST', '127.0.0.1'),//'172.21.0.118',
                'password'  => env('DB_PASSWORD', ''),
            ],
            'write' => [
                'host'      => env('DB_HOST', '127.0.0.1'),
                'password'  => env('DB_PASSWORD', ''),
            ],
            'port'      => env('DB_PORT', '3306'),
            'database'  => env('DB_DATABASE', 'forge'),
            'username'  => env('DB_USERNAME', 'forge'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => 'sline_community_',
            'strict'    => false,
            'engine'    => null,
        ],
        //yuelvhui
        'mysql_yuelvhui' => [
            'driver'    => 'mysql',
            'read' =>[
                'host'      => env('DB_READ_HOST', '127.0.0.1'),//'172.21.0.118',
                'password'  => env('DB_YUELVHUI_PASSWORD', ''),
            ],
            'write' => [
                'host'      => env('DB_YUELVHUI_HOST', '127.0.0.1'),
                'password'  => env('DB_YUELVHUI_PASSWORD', ''),
            ],
            'port'      => env('DB_YUELVHUI_PORT', '3306'),
            'database'  => env('DB_YUELVHUI_DATABASE', 'forge'),
            'username'  => env('DB_YUELVHUI_USERNAME', 'forge'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => 'sline_',
            'strict'    => false,
            'engine'    => null,
        ],
       //商城

        'mysql_malls'  => [
            'driver'    => 'mysql',
            'read' =>[
                'host'      => env('DB_READ_HOST', '127.0.0.1'),//'172.21.0.118',
                'password'  => env('DB_MALL_PASSWORD', ''),
            ],
            'write' => [
                'host'      => env('DB_MALL_HOST', '127.0.0.1'),
                'password'  => env('DB_MALL_PASSWORD', ''),
            ],

            'port'      => env('DB_MALL_PORT',''),
            'database'  => env('DB_MALL_DATABASE',''),
            'username'  => env('DB_MALL_USERNAME',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => 'mall_',
            'strict'    => false,
            'engine'    => null,
        ],
        //悦呗
        'mysql_worker'  => [
            'driver'    => 'mysql',
            'read' =>[
                'host'      => env('DB_READ_HOST', '127.0.0.1'),//'172.21.0.118',
                'password'  => env('DB_YUELVHUI_WORKER_PASSWORD', ''),
            ],
            'write' => [
                'host'      => env('DB_YUELVHUI_WORKER_HOST', '127.0.0.1'),
                'password'  => env('DB_YUELVHUI_WORKER_PASSWORD', ''),
            ],
            'port'      => env('DB_YUELVHUI_WORKER_PORT',''),
            'database'  => env('DB_YUELVHUI_WORKER_DATABASE',''),
            'username'  => env('DB_YUELVHUI_WORKER_USERNAME',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => 'data_',
            'strict'    => false,
            'engine'    => null,
        ],
        //mongodb
        'db_mongodb'  => [
            'driver'    => 'mongodb',
            'host'      => env('DB_MONGO_HOST'),
            'port'      => env( 'DB_MONGO_PORT'),
            'database'  => env('DB_MONGO_DATABASE'),
            'username'  => env('DB_MONGO_USERNAME'),
            'password'  => env('DB_MONGO_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => 'mall_',
            'strict'    => false,
            'engine'    => null,
        ],
        //中台订单中心
        'center_order'  => [
            'driver'    => 'mysql',
            'host'      => env('DB_ORDER_CENTER_HOST', '127.0.0.1'),//'172.21.0.118',
            'password'  => env('DB_ORDER_CENTER_PASSWORD', ''),
            'port'      => env('DB_ORDER_CENTER_PORT',''),
            'database'  => env('DB_ORDER_CENTER_DATABASE',''),
            'username'  => env('DB_ORDER_CENTER_USERNAME',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => 'order_',
            'strict'    => false,
            'engine'    => null,
        ],
         //待用
        'mysql_open'   => [
            'driver'    => 'mysql',
            'read' =>[
                'host'      => env('DB_READ_HOST', '127.0.0.1'),//'172.21.0.118',
                'password'  => env('DB_OPEN_PASSWORD', ''),
            ],
            'write' => [
                'host'       => env('DB_OPEN_HOST', '127.0.0.1'),
                'password'  => env('DB_OPEN_PASSWORD', ''),
            ],
            'host'      => env('DB_OPEN_HOST'),
            'port'      => env('DB_OPEN_PORT'),
            'database'  => env('DB_OPEN_DATABASE'),
            'username'  => env('DB_OPEN_USERNAME'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => 'sline_',
            'strict'    => false,
            'engine'    => null,
        ],

        // 核心库，不对外开放
        'mysql_kernel' => [
            'driver'    => 'mysql',
            'host'      => env('DB_KERNEL_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB_KERNEL_DATABASE'),
            'username'  => env('DB_KERNEL_USERNAME'),
            'password'  => env('DB_KERNEL_PASSWORD'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'strict'    => false,
            'engine'    => null,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

        'session' => [
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', 6379),
            'database' => 2,
        ],

    ],

];
