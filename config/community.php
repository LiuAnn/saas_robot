<?php

//群成员人数
$countNum = 80;
$countNumV2 = 50;
$yTCountNumV2 = 80;
$daRenCountNum = 50;
$zhiDingCountNum = 50;

return [
    'groupName' => '悦淘优选折扣群',
    'daRenGroupName' => '大人优选折扣群',
//    $secretkey = config('community.oldMallOrderGoods');
//    'oldMallOrderGoods' => ' yuelvhui.sline_mall_order_goods ',
    'oldMallOrderGoods' => ' sline_community_mall_order_goods ',
    'countNum' => $countNum,
    'countNumV2' => $countNumV2,
    'daRenCountNum' => $daRenCountNum,
    'zhiDingCountNum' => $zhiDingCountNum,
    //当前机器总人数     当12个微信号都发送了 会把当前信息对应的机器人发送状态更新  spz
    'groupCount' =>12,
    //需要修改成自己的secretkey，在登录网页获取
    'appid'=>[
        '1612cff9b39bff0b' =>'kquOPQSUW14!',
        '91b2f476922e89a6' =>'bcgqrzCEWX9$'
    ],
    'groupType'=>[
        '7323ff1a23f0bf1cda41f690d4089353' => 1,// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
        '75f712af51d952af3ab4c591213dea13' => 1,// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
        '89dafaaf53c4eb68337ad79103b36aff' =>3,// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
    ],
    'groupNameData'=>[
        '7323ff1a23f0bf1cda41f690d4089353' =>'悦淘优选折扣群',// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
        '89dafaaf53c4eb68337ad79103b36aff' =>'大人优选折扣群',// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
        '75f712af51d952af3ab4c591213dea13' =>'直订优选折扣群',// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
        '2cb4a91bce63da9862f72559d4c463f5' =>'微小店折扣群'// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
        //'183ade9bc5f1c4b52c16072362bb21d1' =>'直订优选折扣群'// 1 对应社群   2  对应购买服务（已在使用） 3 对应大人专属机器人
    ],

    //导师微信号
    'tutorWx' => [
        '7323ff1a23f0bf1cda41f690d4089353' => [
            [
                'tutorWx' => 'yuetao04',
                'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-10/14/yuelvhui45Vzz9QAhe1586501259.jpeg',
                'nickname' => '悦淘班主任-西西',
                'mobile' => '18518575202',
            ]
        ],
        '89dafaaf53c4eb68337ad79103b36aff' => [
            [
                'tutorWx' => 'daren012345',
                'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-09/16/yuelvhuiiZ0Qhn4NKS1591690005.jpg',
                'nickname' => '春小义',
                'mobile' => '1851857570',
            ]
        ],
        '75f712af51d952af3ab4c591213dea13' => [
            [
                'tutorWx' => 'zht1514520',
                'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-09/16/yuelvhuiiZ0Qhn4NKS1591690005.jpg',
                'nickname' => '直订导师',
                'mobile' => '1851857570',
            ]
        ],
        '2cb4a91bce63da9862f72559d4c463f5' => [
            [
                'tutorWx' => 'DrunkBlind',
                'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-09/16/yuelvhuiiZ0Qhn4NKS1591690005.jpg',
                'nickname' => 'YCloud导师',
                'mobile' => '18811104887',
            ]
        ],
//        '183ade9bc5f1c4b52c16072362bb21d1' => [
//            [
//                'tutorWx' => 'zht1514520',
//                'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-09/16/yuelvhuiiZ0Qhn4NKS1591690005.jpg',
//                'nickname' => '直订导师',
//                'mobile' => '1851857570',
//            ]
//]
    ],
    'barandData'=>[
        '悦淘',
        '旅悦',
        '大人'
    ],
    'wxid_tag_data'=>[
        'wxid_ye8f77o0b0gm22',//王荣宁  35号
        'wxid_t1s8qhrli8ju22',//55号
    ],
    'insert_to_redis_random'=>'inranstr',
    'insert_to_redis_to_room'=>'inredisroom',
    'redisNewKeystr'=>'redisNewstr',
    'bind_mid_new'=>[921696,425637,77034,1042004,481553,1057359,1146044,1041600,1065455,1138548,921696,460622,704524,688863,684603,32726,599151,450389,1046342,1174002,309010000898],
    'bind_mid_wxid'=>['wxid_8z0h8r9yq2q922','q6077045','wxid_wgfguvpw0xsx22','wxid_clyhacx5rgp622',
        'wxid_5ehzmnlv3d2p22','wxid_6pimvvhnnygc22','wxid_68hgzw2rk8j012','wxid_10l6vxxk2np22','wxid_nw0yi628jzta22',
        'wxid_yyvn72htf8dv22','wxid_1pbbqtr31hno22','wxid_wv0b467lsujt21','wxid_ib3rxip0wy5j22','wxid_kkdfj9urh2pa22','wxid_zgfxrh5d3e7v22','wxid_t2203f9q1oa622','wxid_va0n6o4iy15n22','wxid_qqiezwxugrzj22',
        'wxid_qhgz0kjmgfri22',
        'wxid_njwii0pbff6p12','wxid_hz7po27v3cx822','chenbing933366','wxid_lio09a0ro9xh22','wxid_apws1pid5kq922','wxid_g15avkft3xd422','wxid_i56flb96w01622','wxid_ix4jfdoe6s0612','wxid_s250tt9x10ob22',//吕
        'wxid_0mtax4ud1j2i22','wxid_iwo36a2bcqf622','wxid_6zr4xjnp1di122','wxid_e022edmdr1j412',//青
        'wxid_8z6qyulqneyn22','wxid_jzbq0i9wmrqv22','wxid_z6r1irdzcb6w22','wxid_wzcb6pu3q2x412','wxid_0ovw7ji4hwrr22','wxid_jgnx15dbbuxa22','wxid_fp8gqhmwojqs22','wxid_8082270815612','wxid_ga7be3zrjlc222',//马
        'wxid_5o34l26iuy6z22','wxid_l2fa69rnk4tx22','wxid_h85coqy13tls22','wxid_ym0vr2xn34jr22','wxid_im2uk4447gil22','wxid_c83hq454lih822','wxid_60b4gexhxxmo22','wxid_bhvbxf9jp3gw22','wxid_wloo94agjxlc22','wxid_0iloz3styxir12',//玉东
        'wxid_ap72ncsjihdb22','wxid_vg1huv4kkoen22','wxid_fr9v09dbowaj22','wxid_mz9v3h24jagp12','wxid_34b94c2w3djn22','wxid_u5j1y4rddxms22','wxid_i98mpgsrgl4422','wxid_0n3z3kaa3p2522','wxid_c8z6oqi4i9e422','wxid_gdjxuks4udh222',
        'wxid_csj5lievhswl22','wxid_0ok12f5idlq222','wxid_mradeesiw9s412','wxid_dhdy6m6n04ws21',//玉东
        'wxid_jtxs826nu5m522','wxid_t40o066gz5j422',//直订
         'wxid_lccpdgixbozq22',//刘翠晴
        'wxid_qovqo8hqgv6f22','wxid_h30vvelj3niu22',//大茅 1 7
        'wxid_328vfbgnj5kt22','wxid_8a1nmi6vaajn22',//大茅 2 8
         'wxid_95jg3j66l5fk22',//迈途
         'wxid_3v1trscwma8r22','wxid_r5kjeadttthi22','wxid_mr1ah7mi5w1w12','wxid_vah713r1b87f22','xiaomai193772',
        'wxid_ral8okhefqft22','wxid_ue988yzcpa1w22','lvtouniubb',
        'wxid_17fxe33fuo2l22',//大茅 专属
        'wxid_zvf6jjia8bse22',//赵德帅  29
        'wxid_sosj7x6jvlw522',
        'wxid_bmr3q81kxnhf12','wxid_kf5o5t3mog8422','wxid_mttdubcibo6m22','wxid_b4q6cxxvao3122','wxid_e1j4nv1qex0622','wxid_crgv05rxkc0022',//韩
        'wxid_tqkgdzs5xza922','wxid_c298guoko9zq22','wxid_ber5cfeuuywi22','wxid_y5hpt6uwoebz22',//翠
        'wxid_leuoie83s13l22',//liuqing
        'wxid_s5rhm2ya64bt22','wxid_3ghbi098jx6z22','wxid_suo25py3o5bm22','lijuhe19801219','wxid_i914hjqtrkl222','wxid_zw8q6dah8fen22','wxid_n2fsq8d5fyms22',//yangwei
        'wxid_lefr6532fzn722','wxid_dtypcs4i1lzm22',//君
        'wxid_q368xy76nyme22', 'wxid_k9qrxw5ahc9722', //大人4号//大人1号
        'wxid_1opft97ocnih22','wxid_xp9hrsmtf21922', //大人3号//大人1号
        'wxid_zypyzko0hpkq22', //全欣鹏


    ],
    //专属机器人460622
    'bind_exclu_wxid'=>[
        'GUO15555181087','yuetao19987481789','yuetao18332029858','yutao13112707395',
        'fjf17088','yhp1632983774','RW371985','tiandao0504','yuetao18306226433',
        'huibaoren','yuetao13182875771','wxid_8z0h8r9yq2q922','chenbing933366',
        'wxid_qhgz0kjmgfri22',
        'wxid_iwo36a2bcqf622','wxid_6zr4xjnp1di122','wxid_e022edmdr1j412',
        'wxid_wzcb6pu3q2x412','wxid_z6r1irdzcb6w22','wxid_0ovw7ji4hwrr22',
        'wxid_5o34l26iuy6z22','wxid_l2fa69rnk4tx22','wxid_h85coqy13tls22','wxid_ym0vr2xn34jr22','wxid_im2uk4447gil22','wxid_c83hq454lih822','wxid_ap72ncsjihdb22','wxid_c8z6oqi4i9e422','wxid_wloo94agjxlc22','wxid_0iloz3styxir12',
        'wxid_vg1huv4kkoen22','wxid_fr9v09dbowaj22','wxid_mz9v3h24jagp12','wxid_34b94c2w3djn22','wxid_u5j1y4rddxms22','wxid_i98mpgsrgl4422','wxid_60b4gexhxxmo22','wxid_0n3z3kaa3p2522','wxid_bhvbxf9jp3gw22','wxid_gdjxuks4udh222',
        'wxid_csj5lievhswl22','wxid_0ok12f5idlq222','wxid_mradeesiw9s412','wxid_dhdy6m6n04ws21',//玉东

        'wxid_jtxs826nu5m522', //直订
        'wxid_95jg3j66l5fk22',//迈途
        'wxid_3v1trscwma8r22','wxid_r5kjeadttthi22','wxid_mr1ah7mi5w1w12','wxid_vah713r1b87f22','xiaomai193772','wxid_ral8okhefqft22','wxid_ue988yzcpa1w22','lvtouniubb',
        'wxid_17fxe33fuo2l22',//大茅
        'wxid_t40o066gz5j422',//直订
        'wxid_bmr3q81kxnhf12','wxid_kf5o5t3mog8422','wxid_mttdubcibo6m22','wxid_b4q6cxxvao3122','wxid_e1j4nv1qex0622','wxid_crgv05rxkc0022',//韩
        'wxid_tqkgdzs5xza922','wxid_c298guoko9zq22','wxid_ber5cfeuuywi22','wxid_y5hpt6uwoebz22',//翠
        'wxid_leuoie83s13l22',//liuqing
        'wxid_s5rhm2ya64bt22','wxid_3ghbi098jx6z22','wxid_suo25py3o5bm22','lijuhe19801219','wxid_i914hjqtrkl222','wxid_zw8q6dah8fen22','wxid_n2fsq8d5fyms22',//yangwei
        'wxid_apws1pid5kq922','wxid_g15avkft3xd422','wxid_i56flb96w01622','wxid_ix4jfdoe6s0612','wxid_s250tt9x10ob22',//吕
        'wxid_lefr6532fzn722','wxid_dtypcs4i1lzm22',//君
        'wxid_jgnx15dbbuxa22','wxid_fp8gqhmwojqs22','wxid_8082270815612','wxid_ga7be3zrjlc222',//马
        'wxid_zypyzko0hpkq22', //全欣鹏
    ],

    'add_room_wxid'=>'addsendroom',//当有新群创建的时候 消息进行添加到队列
    //更新悦淘路径
    'changeByRandomStr'=>[
        '7323ff1a23f0bf1cda41f690d4089353'=>'https://gateway.yuetao.vip/shop/v1/stall/callbackAssistantPush',
        '75f712af51d952af3ab4c591213dea13'=>'https://zhidingapi.zhiding365.com/apiH5/stall/callbackAssistantPush',
        '183ade9bc5f1c4b52c16072362bb21d1'=>'https://maitu-web.yuebei.vip/mall/stall/callbackAssistantPush',
    ],
    //不进行任何操作YT20208-31 |               | wxid_1b9uxn9qgyjx22 |
    'bind_no_method'=>[
         'wxid_1b9uxn9qgyjx22',
         'wxid_qfe2u8ap097822',//晨晨
         'wxid_nmdfkfb58mcl22'//韩艳波
    ],



    //专属机器人配置
    'exclusive_small_num'=>10,
    'exclusive_big_num'=>50,
    'exclusive_order_sum'=>1000000,
    //创建的新群素材发送
    'new_room_redis_key'=>'new_room_redis_key',


    // 机器人分享链接  默认的邀请码
    'codeNumber' => '8260930',
    'redisKeystr' => 'redis',
    'community_whitelist'=>[
        'wxid_clyhacx5rgp622','q6077045','wxid_njwii0pbff6p12','wxid_0mtax4ud1j2i22','wxid_328vfbgnj5kt22','wxid_5ehzmnlv3d2p22','wxid_qovqo8hqgv6f22',
        'wxid_6pimvvhnnygc22','wxid_h30vvelj3niu22','wxid_7q4m0290cz5w12','wxid_8a1nmi6vaajn22','wxid_d9676lygzazg12'
   ],
    //wxid_68hgzw2rk8j012   九号机器人给大人了
    'service_whitelist'=>[
        'wxid_clyhacx5rgp622'
   ],

    //示例图片
    'upgradeContent' => [
        ["1.新拉或已有一个超".$countNum."人待微信推广群，群名称修改"],
        ["也可添加导师微信咨询，导师微信号："],
        ["2.上传两张截图（截图一：显示出群主和群人数 截图二：显示出群名称）"],
        ["3.上传提交后，工作时间一天内会有审核结果。"]
    ],
    //大人示例图片
    'daRenUpgradeContent' => [
        ["1.新拉或已有一个超".$daRenCountNum."人待微信推广群，群名称修改"],
        ["也可添加导师微信咨询，导师微信号："],
        ["2.上传两张截图（截图一：显示出群主和群人数 截图二：显示出群名称）"],
        ["3.上传提交后，工作时间一天内会有审核结果。"]
    ],
    //直订示例图片
    'zhiDingUpgradeContent' => [
        ["1.新拉或已有一个超".$zhiDingCountNum."人待微信推广群，群名称修改"],
        ["也可添加导师微信咨询，导师微信号："],
        ["2.上传两张截图（截图一：显示出群主和群人数 截图二：显示出群名称）"],
        ["3.上传提交后，工作时间一天内会有审核结果。"]
    ],

    //群示例图片
    'exampleImages' => [
        "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-09/21/yuelvhuixhQRhydaHm1586438113.png",
        "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-15/21/yuelvhuiC0dNkQtFFR1586957562.png"
    ],

    //大人群示例图片
    'daRenExampleImages' => [
        "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-23/16/yuelvhui7me2nnToSw1592901390.jpg",
        "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-23/16/yuelvhuit0i1D01w9g1592901468.jpg"
    ],

    //导师信息
    'tutorInfo' => [
        [
            'tutorWx' => 'yuetao04',
            'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-10/14/yuelvhui45Vzz9QAhe1586501259.jpeg',
            'nickname' => '悦淘班主任-西西',
            'mobile' => '18518575202',
        ],[
            'tutorWx' => 'YlhDT3',
            'avatar' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-10/14/yuelvhuiraMj1IQdaB1586501285.jpeg',
            'nickname' => '悦淘班主任-大甜',
            'mobile' => '18518575722',
        ]
    ],

    //申请群助理文案  全部插件是统一的
    'applyCopyWriting' => [
        "userNum" => $countNum,
        'oneStep' => [
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/19/yuelvhuiTBWSdqQYYY1586344630.png",
            "content" => "注意* 
            1、开通时务必按照要求来修改群名称 
            2、开通助理后可再改为自己的群名称",
        ],
        'twoStep' => [
            "title" => "*务必正确按群人数选择，否则无法成功开通助理*",
            "content" => "微信群人数大于".$countNum."人可以申请正式使用。 
            务必群人数满".$countNum."人，否则群助理会自动关闭。",
        ],
        'threeStep' => [
            [
                "title" => "您需要完成两步：",
                "content" => [
                    [
                        'text' => '1、复制以上微信添加助理为好友',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/19/yuelvhuirGknDMEaVI1586345038.png',
                    ],[
                        'text' => '2、把助理拉到您的微信群，并务必@它',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/19/yuelvhuijlRagcg1MF1586345115.png',
                    ]
                ]
            ],[
                "title" => "申请时注意：",
                "content" => " 1、每个群开通智能助理务必单独申请。 
                2、此助理微信号不可用于其他人或微信群。",
            ],[
                "title" => "完成以上操作后：",
                "content" => "1、客服工作时间内，一般会在2个小时内为您开通，助理会在群里自动发品。
                2、如果助理2小时内仍未自动发品，请联系悦淘在线客服解决。
                客服工作时间：早8:00——晚23:00。
",
            ]
        ],
    ],


    //申请群助理文案  全部插件是统一的
    'applyCopyWritingV2' => [
        "userNum" => $yTCountNumV2,
        'oneStep' => [
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/17/yuelvhuicyN1PD57Fj1596361952.png",
            "content" => "注意*<br>
            1、开通时务必按照要求来修改群名称<br>2、开通助理后可再改为自己的群名称<br>3、修改群名称记得返回小程序",
        ],
        'twoStep' => [
            "title" => "*务必正确按群人数选择，否则无法成功开通助理*",
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhuit6xx0yvhFf1596363110.png",
            "content" => "微信群人数大于".$yTCountNumV2."人可以申请正式使用。 
            务必群人数满".$yTCountNumV2."人，否则群助理会自动关闭。",
        ],
        'threeStep' => [
            [
                "title" => "您需要完成两步：",
                "content" => [
                    [
                        'text' => '1、复制以上微信添加助理为好友',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhui0KxfCNjlFB1596363378.png',
                    ],[
                        'text' => '2、把助理拉到您的微信群，并务必@它',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhuiPjWXQbSSmX1596363424.png',
                    ]
                ]
            ],[
                "title" => "申请时注意：",
                "content" => " 1、每个群开通智能助理务必单独申请。 
                2、此助理微信号不可用于其他人或微信群。",
            ],[
                "title" => "完成以上操作后：",
                "content" => "1、客服工作时间内，一般会在2个小时内为您开通，助理会在群里自动发品。
                2、如果助理2小时内仍未自动发品，请联系悦淘在线客服解决。
                客服工作时间：早8:00——晚23:00。
",
            ]
        ],
    ],


    //申请群助理文案  全部插件是统一的
    'YCloudApplyCopyWriting' => [
        "userNum" => $countNumV2,
        'oneStep' => [
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2021-01-18/13/yuelvhuiBwkseHPL1X1610949516.jpg",
            "content" => "注意*<br>
            1、首先要创建一个微信群<br>2、开通助理后可再改为自己的群名称<br>3、修改群名称记得返回小程序",
        ],
        'twoStep' => [
            "title" => "*务必正确按群人数选择，否则无法成功开通助理*",
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2021-01-18/13/yuelvhuiMjQ9Ocvtqb1610949538.png",
            "content" => "微信群人数大于".$countNumV2."人可以申请正式使用。 
            务必群人数满".$countNumV2."人，否则群助理会自动关闭。",
        ],
        'threeStep' => [
            [
                "title" => "您需要完成两步：",
                "content" => [
                    [
                        'text' => '1、复制以上微信添加助理为好友',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhui0KxfCNjlFB1596363378.png',
                    ],[
                        'text' => '2、把助理拉到您的微信群，并务必@它',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhuiPjWXQbSSmX1596363424.png',
                    ]
                ]
            ],[
                "title" => "申请时注意：",
                "content" => " 1、每个群开通智能助理务必单独申请。 
                2、此助理微信号不可用于其他人或微信群。",
            ],[
                "title" => "完成以上操作后：",
                "content" => "1、客服工作时间内，一般会在2个小时内为您开通，助理会在群里自动发品。
                2、如果助理2小时内仍未自动发品，请联系悦淘在线客服解决。
                客服工作时间：早8:00——晚23:00。
",
            ]
        ],
    ],


    //申请群助理文案  全部插件是统一的
    'zhiDingApplyCopyWriting' => [
        "userNum" => $countNumV2,
        'oneStep' => [
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/17/yuelvhuicyN1PD57Fj1596361952.png",
            "content" => "注意*<br>
            1、开通时务必按照要求来修改群名称<br>2、开通助理后可再改为自己的群名称<br>3、修改群名称记得返回小程序",
        ],
        'twoStep' => [
            "title" => "*务必正确按群人数选择，否则无法成功开通助理*",
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhuit6xx0yvhFf1596363110.png",
            "content" => "微信群人数大于".$countNumV2."人可以申请正式使用。 
            务必群人数满".$countNumV2."人，否则群助理会自动关闭。",
        ],
        'threeStep' => [
            [
                "title" => "您需要完成两步：",
                "content" => [
                    [
                        'text' => '1、复制以上微信添加助理为好友',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhui0KxfCNjlFB1596363378.png',
                    ],[
                        'text' => '2、把助理拉到您的微信群，并务必@它',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-08-02/18/yuelvhuiPjWXQbSSmX1596363424.png',
                    ]
                ]
            ],[
                "title" => "申请时注意：",
                "content" => " 1、每个群开通智能助理务必单独申请。 
                2、此助理微信号不可用于其他人或微信群。",
            ],[
                "title" => "完成以上操作后：",
                "content" => "1、客服工作时间内，一般会在2个小时内为您开通，助理会在群里自动发品。
                2、如果助理2小时内仍未自动发品，请联系悦淘在线客服解决。
                客服工作时间：早8:00——晚23:00。
",
            ]
        ],
    ],


    //申请群助理文案  全部插件是统一的
    'daRenApplyCopyWriting' => [
        "userNum" => $daRenCountNum,
        'oneStep' => [
            "img" => "https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-06-23/16/yuelvhuiWIsz0zV8Gl1592901505.jpg",
            "content" => "注意* 
            1、开通时务必按照要求来修改群名称 
            2、开通助理后可再改为自己的群名称",
        ],
        'twoStep' => [
            "title" => "*务必正确按群人数选择，否则无法成功开通助理*",
            "content" => "微信群人数大于".$daRenCountNum."人可以申请正式使用。 
            务必群人数满".$daRenCountNum."人，否则群助理会自动关闭。",
        ],
        'threeStep' => [
            [
                "title" => "您需要完成两步：",
                "content" => [
                    [
                        'text' => '1、复制以上微信添加助理为好友',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/19/yuelvhuirGknDMEaVI1586345038.png',
                    ],[
                        'text' => '2、把助理拉到您的微信群，并务必@它',
                        'img' => 'https://yuetao-1300766538.cos.ap-beijing.myqcloud.com/yuetao/image/2020-04-08/19/yuelvhuijlRagcg1MF1586345115.png',
                    ]
                ]
            ],[
                "title" => "申请时注意：",
                "content" => " 1、每个群开通智能助理务必单独申请。 
                2、此助理微信号不可用于其他人或微信群。",
            ],[
                "title" => "完成以上操作后：",
                "content" => "1、客服工作时间内，一般会在2个小时内为您开通，助理会在群里自动发品。
                2、如果助理2小时内仍未自动发品，请联系悦淘在线客服解决。
                客服工作时间：早8:00——晚23:00。
",
            ]
        ],
    ]















];
