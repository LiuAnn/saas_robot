<?php

/**
 * 定义本系统的错误代码和错误信息
 */
return [

    /**
     * HTTP响应代码（通用）
     */
    'com_http_code' => [
        //请求成功
        'success'=>200,

        //参数验证失败
        'validate_error'=>400,

        //请求地址错误
        'not_found'=>404,

        //http请求method不受支持
        'unsupport_method' => 405,

        //不可接受的传入参数格式
        'unsupport_paramter_formatter' => 406,

        //权限不足
        'permission_deny'=>407,

        //登录身份或token不能为空
        'identity_mission' => 401,

        //登录身份验证失败，登录会话过期或登录身份不合法
        'ertification_fail' => 403,

        //未知错误或者服务器错误
        'unknown_error' => 500,

        //代码抛出业务异常
        'service_error' =>400,

    ],

    /**
     * 定义各个模块的通用错误代码和详细错误代码
     */
    'modules' => [
        //通用错误代码
        'common' => [
            //参数校验失败(通用)
            'parameters_error_code' => 32001001,

            //http请求method不受支持(通用)
            'unsupport_method_code' => 32009001,

            //不可接受的传入参数格式
            'unsupport_paramter_formatter_code'=>32009001,

            //未知异常(通用)
            'unknown_error_code' => 32009001,

            //未知异常(通用)
            'service_error_code' => 32009001,

            //权限受限
            'permission_deny_code'=>32009001,

            //登录身份验证失败
            'ertification_fail_code'=>32008002,

            //数据不存在
            'model_not_found_code'=>32002001,

        ],

        //以路由前缀来标识模块
        'distrbuter/line' => [],

    ],

];
