<?php

$startTime = strtotime("2018-10-01");
return [
    "xiaoyue"                 => [
        "mchId"    => 230,
        "payToken" => "dc1f571fb9e2c528d3edeef865d16008"
    ],
    "manage"                 => [
        'wxid_clyhacx5rgp622'=>167624,
        'q6077045'=>167624,
        'wxid_njwii0pbff6p12'=>167624,
        'wxid_0mtax4ud1j2i22'=>167624,
        'wxid_328vfbgnj5kt22'=>167624,
        'wxid_5ehzmnlv3d2p22'=>167624,
        'wxid_qovqo8hqgv6f22'=>167624,
        'wxid_6pimvvhnnygc22'=>167624,
        'wxid_h30vvelj3niu22'=>167624,
        'wxid_7q4m0290cz5w12'=>167624,
        'wxid_8a1nmi6vaajn22'=>167624,
        'wxid_68hgzw2rk8j012'=>167624,
        'wxid_d9676lygzazg12'=>167624,
    ],

    "manageGroupType"                 => [ 2=>1,1=>2,

    ],
    "baseUrl"               => "http://www.yuetao.vip/",
    "app"                   => [
        "memberTokenKey"         => "app_user_token_mid_",
        "articleKey"             => "app_article_id_",
        "mobelSendKey"           => "app_send_",
        "hotelCityBrand"         => "hotel_brand_by_city_",
        "hotelCityDistricts"     => "hotel_districts_by_city_",
        "hotelCityBusiness"      => "hotel_business_by_city_",
        "hotelCityLandmark"      => "hotel_landmark_by_city_",
        "memberInfo"             => "member_info_id_",
        "mMemberToken"           => "m_member_token_",
        "hotelCityStation"       => "hotel_station_by_city_",
        "hotelCityCollege"       => "hotel_college_by_city_",
        "hotelCityHospital"      => "hotel_hospital_by_city_",
        "hotelDiscountPrice"     => 30,
        "hotelPushPrice"         => 50,
        "hotelDiscountTotal"     => 100,
        "hotelDiscountHourTotal" => 10,//每小时10间夜
        "hotelMinPrice"          => 80,//80起优惠20
        "hotelMaxPrice"          => 150,//100以上优惠30
        "cardParentReward"       => 100,  // 直推奖励
        "cardSuperReward"        => 50,  // 间接奖励,
        "registerBonus"          => 0,
        "defaultTemplateId"      => "300005",
        "interTemplateId"        => "300006",
        'mtCityidHotelidJson' => 'meituan_cityid_hotelid_json',//美团价格日历redis键
        'xcCityidHotelidJson' => 'xiecheng_cityid_hotelid_json',//携程价格日历redis键
        'xcfailHotelinfoList' => 'xiecheng_fail_hotelinfo_list',//携程获取酒店信息失败的redis键
    ],
    "global"                => [
        "defaultAvatar" => "https://image.yuelvhui.com/pubfile/2018/07/17/line_1531821979.png",
        "start"         => "https://image.yuelvhui.com/pubfile/2019/01/03/line_1546448137.jpg",
        "card299"       => env("APP_URL") . "/images/index/e-2.png",
        "cardPrice"     => 399,
        "cardPrice2"    => 558,
        "learderPrice"  => 998,
        "startTime"     => $startTime,
        "indexCard"     => "https://www.yuelvhui.com/phone/public/images/app/card/bg1.png",
        "share"         => env("APP_URL") . "/share/xinrenshare.jpg",
        "amanV1"        => env("APP_URL") . "/images/index/l-2.png",
        "logo"          => env("APP_URL") . "/images/index/logo.png",
        "ad400"         => "https://image.yuelvhui.com/pubfile/2018/11/08/line_1541660743.jpg",
        "ad200"         => "https://image.yuelvhui.com/pubfile/2019/01/03/line_1546451266.jpg",
        "ad100"         => "https://image.yuelvhui.com/pubfile/2018/11/08/line_1541660862.jpg",
        "adId"          => 10975,
        "mallBanner"    => "https://image.yuelvhui.com/pubfile/2018/1209/1e8a128433e5d5b110f27c2bcbecfd61.jpeghttps://image.365zhiding.com/pubfile/2018/1130/26765e0789dc8a6aa53b00cb0415b70c.jpg",
        "mallGoodsId"   => 401,
        "mallBanner1"   => "https://image.yuelvhui.com/pubfile/2018/12/18/line_1545109021.jpg",
        "mallGoodsId1"  => 393380,
    ],
    "pay"                   => [
        "sysId"    => 8002,
        "sysPwd"   => "e432675796754f54b448a6d83fa6ada2",
        "mchId"    => 3,
        "payToken" => "f00505b5c8a64741b1ed9200d4825ab8",
    ],
    //玩蜜支付平台参数
    "wm"                    => [
        "mchId"    => 158,
        "payToken" => "347692242a0b4e4e9bcd8a6319f1f731"
    ],
    //悦旅会商城酒店旅游支付平台参数
    "sht"                   => [
        "mchId"    => 210,
        "payToken" => "6d7911ff88c019e4fec5f88e9cea9907"
    ],
    //海风支付平台参数
    "hf"                    => [
        "mchId"    => 164,
        "payToken" => "98e7be5a4e58d423cfe7b7ab464fb03e"
    ],
    "dr"                    => [
        "mchId"    => 232,//5
        "payToken" => '04a5c8a1ad9313410c3eb5a4c4d0f8b0'//"d8de7fa83f1f44bf8146b0efc6c32243"
    ],
    "hd"                    => [
        "mchId"    => 223,
        "payToken" => "79740adb5379313dbbbe96f4a33e2491"
    ],
    "bjapp"                 => [
        "mchId"    => 230,
        "payToken" => "dc1f571fb9e2c528d3edeef865d16008"
    ],
    "hqyl"                  => [
        "mchId"    => 231,
        "payToken" => "5d728e31ba73ae222e50781547d8828b",
    ],
    "ylApp"                 => [
        "mchId"    => 233,
        "payToken" => "0ce9042d0722e64fdf65eefc931255ff",
    ],
    "hdApp"                 => [
        "mchId"    => 234,
        "payToken" => "a221220c10dacf4dd9e8f76631cf0d69",
    ],
    "ydApp"                 => [
        "mchId"    => 235,
        "payToken" => "fe6b36b4d3c5fd822abf7dd94f9c6334",
    ],
    "yd"                    => [//悦店小程序
        "mchId"    => 235,
        "payToken" => "fe6b36b4d3c5fd822abf7dd94f9c6334",
    ],
    "gzh"                   => [//公众号
        "mchId"    => 237,
        "payToken" => "8ededb4f0057478783db999b4d5dbb54",
    ],
    "trains"         => [//中铁管家I火车票机票旅游
        "mchId"    => 239,
        "payToken" => "07af2fe40972486694188557637da61f",
    ],
    "tour"         => [//悦淘|悦旅会免费旅游优品生活 小程序
        "mchId"    => 240,
        "payToken" => "d2869f5c2630404fb0f5c1eba576acb6",
    ],
    "gzhyx"         => [//悦淘优享 公众号
        "mchId"    => 241,
        "payToken" => "jhvgmx8i7m66t0zssgoo7tja2d9pwxjx",
    ],
    "lvyue"         => [//旅悦
        "mchId"    => 239,
        'newMchId' => 233,
        "payToken" => "0ce9042d0722e64fdf65eefc931255ff",
    ],

    "hotel"                 => [
        "elongUser"              => "e61285144d5947f8a518ae6105429a48",
        "elongAppKey"            => "7cc875e309518a67d14d8cb163b14d17",
        "elongSecretKey"         => "34475461d4f8f006f1f0bf15ad06d9d7",
        "hotelDetail"            => "hotel_detail_id_",
        "hotelTitanDetail"       => "hotel_titan_detail_id_",
        "hotelTitanImage"        => "hotel_titan_image_id_",
        "hotelTitanRatePlan"     => "hotel_titan_rate_plan_id_",
        "hotelTitanFixedPrice"   => 0.02,//泰坦云定价
        "hotelRoomDetail"        => "hotel_room_detail_id_",
        "hotelCtripRoomDetail"   => "hotel_ctrip_room_detail_id_",
        "hotelMeituanRoomDetail" => "hotel_meituan_room_detail_id_",
        "hotelRoomImage"         => "hotel_room_image_",
        "hotelRoomInfo"          => "hotel_room_info_",
        "hotCity"                => "hotel_hotel_city",
        "free"                   => "hotel_free_id_",
        "carouselUp"             => "https://image.yuelvhui.com/pubfile/2018/1209/d12091d2066e7cfd06f2a19cc16688f3.jpeg",
        "newCarouselUp"          => "https://image.yuelvhui.com/pubfile/2019/01/24/line_1548338330.jpeg",
        "theme1"                 => "https://image.yuelvhui.com/pubfile/2018/11/16/line_1542376073.jpg",
        "theme2"                 => "https://image.yuelvhui.com/pubfile/2018/11/16/line_1542375898.png",
        "theme3"                 => "https://image.yuelvhui.com/pubfile/2018/11/16/line_1542375962.png",
        "theme5"                 => "https://image.yuelvhui.com/pubfile/2018/11/16/line_1542376014.png",
        "theme6"                 => "https://image.yuelvhui.com/pubfile/2018/11/16/line_1542376035.png",
        "hotelStartDedu"         => "hotel_start_dedu_thirty",
        "hotelListDiscount"      => "hotel_list_discount",
        "samHotelCity"           => "sam_hotel_city",
        "samHotel"               => "https://image.yuelvhui.com/pubfile/2018/12/20/line_1545312725.png",
        "newSamHotel"            => "https://image.yuelvhui.com/pubfile/2018/12/26/line_1545834504.png",
        "shillaHotel"            => "https://image.yuelvhui.com/pubfile/2018/12/20/line_1545312794.png",
        "newShillaHotel"         => "https://image.yuelvhui.com/pubfile/2018/12/26/line_1545834461.png",
        "samListPic"             => "https://image.yuelvhui.com/pubfile/2018/12/27/line_1545897472.png",
        "noticeImage"            => "https://image.yuelvhui.com/pubfile/2019/01/08/line_1546956927.png",
        "mallAd"                 => "https://image.yuelvhui.com/pubfile/2019/01/09/line_1546982085.png",
        "offLineHotels"          => [
            90115354, 90736171, 1903184, 1901191, 92957877, 1901306, 92114322, 92495111, 93544446, 90115125, 91729496,
            91327201, 91313200, 91777590, 90264470, 91321712, 90596867, 1901845, 90026729, 92073729, 31718015, 91679611,
            92176648, 93562864, 91326762, 92516365, 92315718
        ],
        "hotelCtripRatePlan"     => "hotel_ctrip_rate_plan_id_",
        "samImage"               => 'https://image.yuelvhui.com/pubfile/2019/05/12/line_1557649640.png',
    ],
    "iHotel"                => [
        "elongUser"      => "8a8fd8942dc74fa896924095b1057823",
        "elongAppKey"    => "c6534334f78c9a826b3a512dcda35343",
        "elongSecretKey" => "3be842452bae6a1565c1b8af48803b11",
    ],
    "cTrip"                 => [
        "aid"          => 973857,
        "sid"          => 1564499,
        "gatewayUrl"   => "http://openservice.ctrip.com",
        "format"       => "JSON",
        "key"          => "f7b98acaa35049a8bf9f3eb8ff825b58",
        "uuid"         => "3d7f9f0c-8196-4828-bd6e-c5ff72199d77",
        "refreshToken" => "cTrip_refresh_token",
        "accessToken"  => "cTrip_access_token",
        "iCode"        => [
            "getHotelIds"         => "ccc56c891a4d4f4fa4fc78f0aec29d03",
            "getHotelDetails"     => "62a8bb573582435bbe8b361334efe36f",
            "getHotelRoomInfo"    => "39122604bf0e4b33a9569658cf273161",//"c04d1d7b7af449999dba88896fcc0811",
            "getHotelRatePlan"    => "4c15cadb05f14aa9836260c53ab74526",//"14a14763194648f5b9d2427f00b3ca04",
            "checkRoomCanBooking" => "419f7a688e3c45f481d295708b870323",//d67d24cfdc2940cd858f506c708cf0d4
            "ctripOrderCreate"    => "2b0c7e0eebe2467a97be3d284313a129",//"acf5c44749234668ba3c539cc1d26c87",
            "ctripOrderMakeSure"  => "f40c5c71f84745cbbc73238252d9d43c",//"5f3c81bc0c90458ba94bfe2dafea8459",
            "ctripOrderListener"  => "e5e981e2cf9340f99f9cbf79c82668ac",//"61aebb32ae1347c5a274fc9fb4b4f962",
            "ctripOrderDetail"    => "e4a2a8ac057f4b378c9e153ef3f35249",//"5db9d7e805ef4b36a8cf0e57900e118a",
            "ctripCancelOrder"    => "f40a36ac2dfe42518fb883d33fa33391",//"519114d791b94babba31f32eb5d559eb"
            "ctripRefundOrder"    => "740848180ff44c9ead1dcb5815744eb0",
        ]
    ],
    "cTripTest"             => [
        "aid"          => 1,
        "sid"          => 50,
        "gatewayUrl"   => "http://openservice.open.uat.ctripqa.com",
        "format"       => "JSON",
        "key"          => "123456789",
        "uuid"         => "f71dee19-9e09-42d9-a39c-40757d9d76e4",
        "refreshToken" => "cTrip_test_refresh_token",
        "accessToken"  => "cTrip_test_access_token",
        "iCode"        => [
            "getHotelIds"         => "ccc56c891a4d4f4fa4fc78f0aec29d03",//
            "getHotelDetails"     => "62a8bb573582435bbe8b361334efe36f",//
            "getHotelRoomInfo"    => "c04d1d7b7af449999dba88896fcc0811",
            "getHotelRatePlan"    => "14a14763194648f5b9d2427f00b3ca04",
            "checkRoomCanBooking" => "d67d24cfdc2940cd858f506c708cf0d4",
            "ctripOrderCreate"    => "acf5c44749234668ba3c539cc1d26c87",
            "ctripOrderMakeSure"  => "5f3c81bc0c90458ba94bfe2dafea8459",
            "ctripOrderListener"  => "61aebb32ae1347c5a274fc9fb4b4f962",
            "ctripOrderDetail"    => "5db9d7e805ef4b36a8cf0e57900e118a",
            "ctripCancelOrder"    => "519114d791b94babba31f32eb5d559eb",
            "ctripRefundOrder"    => "740848180ff44c9ead1dcb5815744eb0",//
        ]
    ],
    "meiTuan"               => [
        "partnerId"  => 10108,
        "accesskey"  => "10b5bbd537e3c03e35b8f28f74b0227f",
        "secretkey"  => "ba3f380629b3d54cf3278fc05c0d48ac",
        "gatewayUrl" => "https://fenxiao.meituan.com/opdtor/api",
        "version"    => "1.0",
    ],
    "meiTuanTest"           => [
        "partnerId"  => 3,
        "accesskey"  => "3c8862bc4cae512fbc8dc48e231a612b",
        "secretkey"  => "b2e021aba7127c1763ede65019688d4d",
        "gatewayUrl" => "https://fenxiao.meituan.com/opdtor/api",
        "version"    => "1.0",
    ],
    //公众号信息,Z卡
    "share"                 => [
        //"appId" => 'wx48cbc0aa72781551',
        "appId"     => 'wx743a4ed7a700d06c',
        //"appSecret"=> "4b14ffe09326dff26a6a96f8ffa56d0f",
        "appSecret" => "542c2a033c87fd81b3f4e2ebb0176391",
    ],

    //推荐码使用超过10次，获得的积分
    'invitationJiFenNumber' => 50,
    'registerJiFenNumber'   => 10,
    'ncnbHotel'             => [
        'appId'       => 271,
        'securityKey' => '3e61cf51-3a97-48a6-8b24-e1b9e3ba989a',
        'userName'    => 'BK100065',
        'passWord'    => 'D380299254199FDC7A9AC90967C8CCE2',
        'signature'   => 'QksxMDAwNjVFMTBBREMzOTQ5QkE1OUFCQkU1NkUwNTdGMjBGODgzRTNlNjFjZjUxLTNhOTctNDhhNi04YjI0LWUxYjllM2JhOTg5YQ==',
    ],
    "card"                  => [
        "ticket"    => env("APP_URL") . "/images/card/jdmp.png",
        "unTicket"  => env("APP_URL") . "/images/card/un_jdmp.png",
        "traffic"   => env("APP_URL") . "/images/card/jtywx.png",
        "unTraffic" => env("APP_URL") . "/images/card/un_jtywx.png",
        "hotel"     => env("APP_URL") . "/images/card/jdyd.png",
        "unHotel"   => env("APP_URL") . "/images/card/un_jdyd.png",
        "coin"      => env("APP_URL") . "/images/card/bi_600.png",
        "unCoin"    => env("APP_URL") . "/images/card/un_bi_600.png",
        "credit"    => env("APP_URL") . "/images/card/creditCard.png",
        "unCredit"  => env("APP_URL") . "/images/card/un_creditCard.png",
    ],
    "mine"                  => 'mineInfo',
    "mall"                  => [
        'index'                => 'mall_index_cache',
        'newIndex'             => 'mall_newIndex_cache',
        'newTwoIndex'          => 'mall_TwoIndex_cache',
        'newTwosIndex'         => 'mall_TwosIndex_cache',
        'newThreeIndex'        => 'mall_ThreeIndex_cache',
        'newsIndex'            => 'mall_newsIndex_cache',
        'appIndexs'            => 'mall_appIndexs_cache',
        'appNewsIndex'         => 'mall_appIndex_cache',
        'indexNew'             => 'mall_indexNew_cache',
        'newIndexs'            => 'mall_newIndexs_cache',
        'indexNews'            => 'mall_indexNews_cache',
        'JDTypeList'           => 'mall_JDTypeList_cache',
        'jDType'               => 'mall_jDType_cache',
        'JDTypeGoodsList'      => 'JDTypeGoodsList',
        'allEarth'             => 'mall_allEarth_cache',
        'allEarthType'         => 'mall_allEarthType_cache',
        'Remittance'           => 'mall_Remittance_cache',
        'RemittanceList'       => 'mall_RemittanceList_cache',
        'hundredList'          => 'mall_hundredList_cache',
        'detail'               => 'mall_detail_cache',
        'newDetail'            => 'mall_newDetail_cache',
        'expressInfo'          => 'mall_expressInfo_cache',
        'flashEndTime'         => 5,
        'skuIndex'             => 'mall_skuIndex',
        'skuType'              => 'mall_Type',
        'yanType'              => 'yan_Types',
        'skuNewindex'          => 'mall_sku_newIndexs',
        'wyActivity'           => 'mall_wyActivity_cache',
        'topHundred'           => 'mall_topHundred_cache',
        'leftPicture'          => 'https://image.yuelvhui.com/pubfile/2018/11/28/line_1543387552.png',
        'rightPicture'         => 'https://image.yuelvhui.com/pubfile/2018/11/27/line_1543333339.png',
        'leftNewPicture'       => 'https://image.yuelvhui.com/pubfile/2018/11/27/line_1543333358.png',
        'allPicture'           => 'https://image.yuelvhui.com/pubfile/2018/12/24/line_1545656610.jpg',//'https://image.yuelvhui.com/pubfile/2018/12/18/line_1545120197.jpg',
        'mallMiddleImg'        => 'https://image.yuelvhui.com/pubfile/2018/12/06/line_1544081568.png',
        'mallMiddleId'         => '401',
        'memberBuy'            => ['2148'],
        'memberBuyGoods'       => [402894],
        'mallShareGoodsId'     => [393380],
        'mallShareGoodsTypeId' => [2148],
        'memberLimitBuy'       => [
            2148 => 1
        ],
        'purchaseLimit'        => [
            312839 => 1,
            30     => 1,
            393382 => 1,
            402894 => 1,

        ],
        'MallCoupon'           => [
            277595 => 158,
            277604 => 167,
            277610 => 168
        ],
        'limitGoodsBuy'        => 2,
        'limitGoods'           => [369154,369160,363895,363342,379069,379053,379107,379607,379603,346520,379920,380585,380385,380816,381176],//线上开卡7天购买
        'toMemberGoods'        => [277616, 277559, 277945, 53341],
        'picture'              => [
            'banner'         => 'https://image.yuelvhui.com/pubfile/2018/12/21/line_1545323149.jpg',
            'bannerId'       => '393486',
            'left'           => 'https://image.yuelvhui.com/pubfile/2018/12/20/line_1545310614.png',
            'leftNew'        => 'https://image.yuelvhui.com/pubfile/2018/12/20/line_1545310827.png',
            'right'          => 'https://image.yuelvhui.com/pubfile/2018/12/20/line_1545310666.png',
            'under'          => 'https://image.yuelvhui.com/pubfile/2019/01/19/line_1547831877.jpg',//'https://image.yuelvhui.com/pubfile/2019/01/09/line_1546968746.png',
            'backGround'     => 'https://image.yuelvhui.com/pubfile/2018/12/21/line_1545322809.jpg',
            'banner2'        => 'https://image.yuelvhui.com/pubfile/2018/12/28/line_1546012603.jpg',
            'bannerId2'      => '2148',
            'memberLeft'     => 'https://image.yuelvhui.com/pubfile/2019/01/06/line_1546777755.png',
            'memberRight'    => 'https://image.yuelvhui.com/pubfile/2019/01/06/line_1546777785.png',
            'memberNewLeft'  => 'https://image.yuelvhui.com/pubfile/2019/01/18/line_1547824584.png',//'https://image.yuelvhui.com/pubfile/2019/01/06/line_1546777755.png',
            'memberNewRight' => 'https://image.yuelvhui.com/pubfile/2019/01/18/line_1547824668.png',//'https://image.yuelvhui.co
        ],
        'background'           => [
            '2148' => [
                'url'   => 'https://image.yuelvhui.com/pubfile/2019/01/10/line_1547049817.jpg',
                'title' => '会员更优惠'
            ],
            '2150' => [
                'url'   => 'https://image.yuelvhui.com/pubfile/2019/02/27/line_1551200664.jpg',//'https://image.yuelvhui.com/pubfile/2019/01/07/line_1546854952.jpg',
                'title' => 'https://image.yuelvhui.com/pubfile/2019/01/06/line_1546787616.png',
            ]
        ]
    ],
    "hotelRegionType"       => [
        0  => ['typeName' => '城市', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548356.png'],
        1  => ['typeName' => '行政区', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548432.png'],
        2  => ['typeName' => '景区', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548465.png'],
        3  => ['typeName' => '商圈', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548523.png'],
        4  => ['typeName' => '酒店', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548613.png'],
        5  => ['typeName' => '地点', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548649.png'],
        6  => ['typeName' => '医院', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548678.png'],
        7  => ['typeName' => '学校', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883718.png'],
        8  => ['typeName' => '景点', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883764.png'],
        9  => ['typeName' => '地铁站', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883788.png'],
        10 => ['typeName' => '机场/车站', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883806.png'],
        12 => ['typeName' => '品牌', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883863.png'],
        13 => ['typeName' => '集团', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883884.png'],
    ],
    "hotelKeyType"          => [
        0  => ['typeName' => '酒店', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548613.png'],
        3  => ['typeName' => '品牌', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883863.png'],
        4  => ['typeName' => '行政区', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548432.png'],
        5  => ['typeName' => '商圈', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548523.png'],
        6  => ['typeName' => '地点', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548649.png'],
        8  => ['typeName' => '医院', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/26/line_1540548678.png'],
        9  => ['typeName' => '学校', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883718.png'],
        10 => ['typeName' => '景点', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883764.png'],
        11 => ['typeName' => '地铁站', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883788.png'],
        12 => ['typeName' => '机场/车站', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883806.png'],
        13 => ['typeName' => '集团', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883884.png'],
        14 => ['typeName' => '景区', 'iconUrl' => 'https://image.yuelvhui.com/pubfile/2018/10/30/line_1540883764.png']
    ],
    'hotelCityHmt'          => [
        3201 => '香港',
        3301 => '澳门',
        3401 => '台北',
        3402 => '高雄',
        3403 => '台中',
        3404 => '屏东',
        3406 => '基隆',
        3407 => '桃园',
        3408 => '新竹',
        3409 => '苗栗',
        3410 => '彰化',
        3411 => '南投',
        3412 => '云林',
        3413 => '嘉义',
        3414 => '台南',
        3415 => '宜兰',
        3416 => '花莲',
        3417 => '台东',
        3418 => '澎湖',
        3419 => '金门',
        3420 => '马祖',
    ],
    "cardImage"             => [
        "elite"  => [
            "image" => "https://image.yuelvhui.com/pubfile/2018/11/07/line_1541522455.png",
            "price" => 399
        ],
        "leader" => [
            "image" => "https://image.yuelvhui.com/pubfile/2018/11/07/line_1541523772.png",
            "price" => 399,
        ]

    ],

    "skuErrorCode"            => [
        '101' => 'app_key不能为空',
        '102' => 'method参数不能为空',
        '103' => '服务:没有授权',
        '104' => '服务:没有配置映射',
        '105' => '参数不能为空',
        '106' => 'sign参数不能为空',
        '107' => 'sign已存在，重复提交',
        '108' => 'sign签名不正确',
        '109' => 'timestamp参数不能为空',
        '110' => 'timestamp参数格式不对',
        '111' => 'timestamp时间误差为毫秒，大于10分钟',
        '500' => '系统错误'
    ],
    "skuErrorCodeTwo"         => [
        '300' => 'outOrderId不能为空',
        '301' => '客户详细地址不能为空',
        '302' => '客户所属区不能为空',
        '303' => '客户所属市不能为空',
        '304' => '客户电话、移动电话必须至少输入一项',
        '305' => '收件人不能为空',
        '306' => '客户所属省不能为空',
        '307' => '订单商品明细不能为空',
        '308' => '商品商编不能为空',
        '309' => '商品:购买数量必须>0',
        '310' => '商品:价格不正确',
        '311' => '商品:不属于寺库',
        '312' => '商品:不属于大陆商品',
        '313' => '订单创建失败（库存不足，身份证校验失败等）'
    ],
    "skuOrderStatus"          => [
        '100' => '商家待发货',
        '110' => '商家已发货',
        '120' => '商家商品已入库',
        '130' => '订单已确认',
        '140' => '订单已拣货',
        '150' => '已捡货',
        '160' => '已打包',
        '170' => '已发货',
        '180' => '已签收',
        '190' => '已拒签',
        '200' => '已完成',
        '210' => '已取消'

    ],
    "updateSkuOrderEndStatus" => [
        '190', '200', '210'
    ],
    "hotelRefundsStatus"      => [
        31 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '审核通过', 'showType' => 0],
            ['stateName' => '退款中', 'showType' => 0],
            ['stateName' => '已退款', 'showType' => 0],
        ],
        33 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '审核不通过', 'showType' => 2],
            ['stateName' => '退款中', 'showType' => 0],
            ['stateName' => '已退款', 'showType' => 0],
        ],
        35 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '审核通过', 'showType' => 1],
            ['stateName' => '退款中', 'showType' => 0],
            ['stateName' => '已退款', 'showType' => 0],
        ],
        40 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '审核通过', 'showType' => 1],
            ['stateName' => '退款中', 'showType' => 1],
            ['stateName' => '已退款', 'showType' => 0],
        ],
        45 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '已处理但不退款', 'showType' => 1],
            ['stateName' => '退款中', 'showType' => 0],
            ['stateName' => '已退款', 'showType' => 0],
        ],
        47 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '审核通过', 'showType' => 1],
            ['stateName' => '退款中', 'showType' => 1],
            ['stateName' => '退款失败', 'showType' => 1],
        ],
        50 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '审核通过', 'showType' => 1],
            ['stateName' => '退款中', 'showType' => 1],
            ['stateName' => '已退款', 'showType' => 1],
        ],
        60 => [
            ['stateName' => '申请退款', 'showType' => 1],
            ['stateName' => '退款申请超时', 'showType' => 2],
            ['stateName' => '退款中', 'showType' => 0],
            ['stateName' => '已退款', 'showType' => 0],
        ]
    ],
    "hotelRoomStatus"         => [
        0   => [
            ['stateName' => '提交订单', 'showType' => 1],
            ['stateName' => '酒店确认', 'showType' => 0],
            ['stateName' => '入住', 'showType' => 0],
            ['stateName' => '离店', 'showType' => 0],
        ],
        'A' => [
            ['stateName' => '提交订单', 'showType' => 1],
            ['stateName' => '酒店确认', 'showType' => 1],
            ['stateName' => '入住', 'showType' => 0],
            ['stateName' => '离店', 'showType' => 0],
        ],
        'F' => [
            ['stateName' => '提交订单', 'showType' => 1],
            ['stateName' => '酒店确认', 'showType' => 1],
            ['stateName' => '入住', 'showType' => 1],
            ['stateName' => '离店', 'showType' => 0],
        ],
        'C' => [
            ['stateName' => '提交订单', 'showType' => 1],
            ['stateName' => '酒店确认', 'showType' => 1],
            ['stateName' => '入住', 'showType' => 1],
            ['stateName' => '离店', 'showType' => 1],
        ],
    ],
    "hotelRefundsText"        => [
        31 => '申请退款',
        33 => '审核不通过',
        35 => '审核通过',
        40 => '退款中',
        47 => '退款失败',
        50 => '已退款'
    ],
    "hotelOrderText"          => [
        0 => '待支付',
        1 => '已取消',
        2 => '支付完成',
        3 => '已确认',
        4 => '已入住',
        5 => '已离店'
    ],
    "juhe"                    => [
        "mobileApi" => "https://apis.juhe.cn/mobile/get",
        "key"       => "712133792525e75c231c93f2b137e95a"
    ],

    //个人中心活动图片
    "memberActivePics"        => [
        'litPic' => 'https://image.yuelvhui.com/pubfile/2018/11/26/line_1543241506.jpg',
        'bigPic' => 'https://image.yuelvhui.com/pubfile/2018/11/30/line_1543582543.jpg'
    ],

    // 默认上级和分公司
    "default"                 => [
        "parentId"    => 67,
        "companyName" => "环球悦旅会",
        "companyId"   => 28458,
    ],
    "yjyCity"                 => [
        '深圳市', '广州市', '佛山市', '东莞市', '珠海市', '成都市', '重庆市', '长沙市', '宁波市', '南京市', '苏州市', '武汉市', '惠州市'
    ],
    "train"                   => [
        'trainLineCache' => 'trainLineFromToDate'
    ],


    'oneMoney'     => [           //购买商品赠券
        '310255' => '1393-1',
        '310256' => '1392-1',
        '309946' => '520-2',
        '310254' => '992-1',
        '308355' => '540-1',
        '336080' => '598-1',
        '340468' => '642-1',
        '336080' => '681-1',
        '345833' => '703-1',
        '365727' => '1466-1',
        '367761' => '1484-1',
        '367760' => '1483-1',
        '365674' => '1457-1',
        '365669' => '1083-1',
        '367862' => '1493-1',
        '367861' => '1494-1',
        '367860' => '1495-1',
        '368268' => '1522-1',
        '368267' => '1523-1',
        '368266' => '1524-1',

    ],
    'limitJdGoods' => [
        2 => 50,
        3 => 10,
    ],//京东商品限购
    'wallet'       => [
        'hotel'        => ['key' => 'hotelConsumeKey_', 'discount' => 0.2],
        'gas'          => ['key' => 'gasConsumeKey_', 'discount' => 0.2],
        'mobileCharge' => ['key' => 'mobileChargeConsumeKey_', 'discount' => 0.02],
        'videoCharge'  => ['key' => 'videoChargeConsumeKey_', 'discount' => 0.02],
        'couponCharge'  => ['key' => 'couponChargeConsumeKey_', 'discount' => 0.02],
    ],
    'walletType'   => [
        1 => 'hotel',
        2 => 'gas',
        3 => 'videoCharge',
        4 => 'mobileCharge',
        12 => 'couponCharge',
    ],
    'bonusUnique'   => 'bonusUniqueHash_',
    'forbiddenPid'  => [322070],
    'sendGoods'  =>[368328,368329,368264,365738,351502,365732,353780,351444,368264,354125,368877,368876,368875 ,368874,368873,368872,369453,378046,378192,378640,378524,368770,378814,379102,379101,379240,379239,379118,379117,379506,379605,379604,379736,380135,380136,380137,380134,351657,307651,307650,307647,381166],  //白拿商品
    'privateKey' => 'zxcvbnmasdfghjkl',  // aes加密秘钥

    "refundsStatusNew"      => [
        "refundMoney" => [ //仅退款
            31 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '审核通过', 'showType' => 0],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            33 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '审核不通过', 'showType' => 2],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            35 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '审核通过', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            40 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '审核通过', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            45 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '退款驳回', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            47 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '审核通过', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款失败', 'showType' => 2],
            ],
            50 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '审核通过', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 1],
            ],
            60 => [
                ['stateName' => '申请退款', 'showType' => 1],
                ['stateName' => '退款申请超时', 'showType' => 2],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ]
        ],
        "refundMoneyAndGoods" => [ //退货退款
            31 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 0],
                ['stateName' => '商家验收', 'showType' => 0],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            32 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 0],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            34 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 0],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            33 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '审核不通过', 'showType' => 2],
                ['stateName' => '商家验收', 'showType' => 0],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            35 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            40 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            45 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 1],
                ['stateName' => '退款驳回', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 0],
            ],
            47 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款失败', 'showType' => 2],
            ],
            50 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '同意退货', 'showType' => 1],
                ['stateName' => '商家验收', 'showType' => 1],
                ['stateName' => '退款中', 'showType' => 1],
                ['stateName' => '退款成功', 'showType' => 1],
            ],
            60 => [
                ['stateName' => '提交申请', 'showType' => 1],
                ['stateName' => '退款申请超时', 'showType' => 2],
                ['stateName' => '商家验收', 'showType' => 0],
                ['stateName' => '退款中', 'showType' => 0],
                ['stateName' => '退款成功', 'showType' => 0],
            ]
        ]
    ],
];

?>
