<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMallActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_malls')->table('activities', function (Blueprint $table) {
            $table->integer('coupon_type')->default(null)->comment('优惠卷=1 专题=2');
            $table->integer('coupon_id')->default(null)->comment('根据 coupon_type 优惠卷或专题 id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
