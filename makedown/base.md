###数据统计后台基础接口说明

**基础接口地址**

测试地址：  
`http://dev-data-api.yuelvhui.com`

线上地址：  
`https://data-api.yuelvhui.com`

**接口请求验证方式**  
`Bearer id.timestamp.token`  
token的过期时间为60分钟
