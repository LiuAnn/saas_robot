####酒店订单列表  


- 请求地址：  
**hotel/order/list.json** 

- 请求方式：  
**post** 
  
- 请求参数：  
`
{
	"arrivalDate": "2018-12-01",
	"departureDate": "2018-12-02",
	"linkman": "",
	"linktel": "",
	"orderNo": "",
	"roomStatus": "",
	"payStatus": "",
}
`  

- 返回结果：  
`
{
    "code": 200,
    "msg": "来啦~ 老弟~~~",
    "data": {
        "pageTotal": 20,
        "page": 1,
        "list": [
            {
                "createTime": "2018-12-04 08:44:43",
                "orderNo": "11615439130835714",
                "payFormNo": "",
                "arrivalDate": "2018-12-03",
                "hotelName": "天晴国际青年客栈(成都青羊宫店)",
                "linkman": "孙金伟",
                "linktel": "17771945333",
                "roomNum": 1,
                "actualPay": 0,
                "payStatus": 1,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:44:43"
            },
            {
                "createTime": "2018-12-04 08:44:34",
                "orderNo": "11615439130747762",
                "payFormNo": "8037900885",
                "arrivalDate": "2018-12-03",
                "hotelName": "罗城维也纳大酒店",
                "linkman": "邱云辉",
                "linktel": "18176265668",
                "roomNum": 2,
                "actualPay": "27000",
                "payStatus": 5,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:44:34"
            },
            {
                "createTime": "2018-12-04 08:43:29",
                "orderNo": "11615439130091163",
                "payFormNo": "8037900882",
                "arrivalDate": "2018-12-03",
                "hotelName": "衡东银都宾馆",
                "linkman": "张文涛",
                "linktel": "17775628402",
                "roomNum": 1,
                "actualPay": "7500",
                "payStatus": 5,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:43:29"
            },
            {
                "createTime": "2018-12-04 08:42:39",
                "orderNo": "11615439129599367",
                "payFormNo": "8037900881",
                "arrivalDate": "2018-12-03",
                "hotelName": "罗城维也纳大酒店",
                "linkman": "李乐燕",
                "linktel": "18176265668",
                "roomNum": 1,
                "actualPay": "9500",
                "payStatus": 5,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:42:39"
            },
            {
                "createTime": "2018-12-04 08:41:51",
                "orderNo": "11615439129117767",
                "payFormNo": "",
                "arrivalDate": "2018-12-03",
                "hotelName": "罗城维也纳大酒店",
                "linkman": "李乐燕",
                "linktel": "18176265668",
                "roomNum": 1,
                "actualPay": 0,
                "payStatus": 1,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:41:51"
            },
            {
                "createTime": "2018-12-04 08:34:55",
                "orderNo": "11615439124957639",
                "payFormNo": "8037900876",
                "arrivalDate": "2018-12-03",
                "hotelName": "君阅漫居酒店",
                "linkman": "盛佳元",
                "linktel": "15274468688",
                "roomNum": 1,
                "actualPay": "5000",
                "payStatus": 5,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:34:55"
            },
            {
                "createTime": "2018-12-04 08:31:55",
                "orderNo": "11615439123159560",
                "payFormNo": "",
                "arrivalDate": "2018-12-03",
                "hotelName": "尚客优连锁酒店(武胜弘武大道店(锦苑宾馆)",
                "linkman": "姜梅",
                "linktel": "13696171062",
                "roomNum": 1,
                "actualPay": 0,
                "payStatus": 1,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:31:55"
            },
            {
                "createTime": "2018-12-04 08:29:32",
                "orderNo": "11615439121725437",
                "payFormNo": "",
                "arrivalDate": "2018-12-03",
                "hotelName": "好客连锁酒店(武胜假日广场店)",
                "linkman": "姜梅",
                "linktel": "13696171062",
                "roomNum": 1,
                "actualPay": 0,
                "payStatus": 1,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 08:29:32"
            },
            {
                "createTime": "2018-12-04 08:22:56",
                "orderNo": "11615439117761232",
                "payFormNo": "8037900866",
                "arrivalDate": "2018-12-03",
                "hotelName": "君阅漫居酒店",
                "linkman": "盛佳元",
                "linktel": "13973449294",
                "roomNum": 1,
                "actualPay": "5000",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 08:22:56"
            },
            {
                "createTime": "2018-12-04 07:53:43",
                "orderNo": "11615439100234153",
                "payFormNo": "8037900851",
                "arrivalDate": "2018-12-03",
                "hotelName": "汉庭酒店(忻州和平中街店)",
                "linkman": "刘伟",
                "linktel": "15518222727",
                "roomNum": 1,
                "actualPay": "7100",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:53:43"
            },
            {
                "createTime": "2018-12-04 07:40:06",
                "orderNo": "11615439092063530",
                "payFormNo": "8037900843",
                "arrivalDate": "2018-12-03",
                "hotelName": "君阅漫居酒店",
                "linkman": "盛佳元",
                "linktel": "13973449294",
                "roomNum": 1,
                "actualPay": "4000",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:40:06"
            },
            {
                "createTime": "2018-12-04 07:39:29",
                "orderNo": "11615439091691033",
                "payFormNo": "8037900841",
                "arrivalDate": "2018-12-03",
                "hotelName": "霸州壹佰快捷宾馆",
                "linkman": "袁跃彬",
                "linktel": "17729660085",
                "roomNum": 1,
                "actualPay": "5600",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:39:29"
            },
            {
                "createTime": "2018-12-04 07:27:38",
                "orderNo": "11615439084589942",
                "payFormNo": "8037900833",
                "arrivalDate": "2018-12-03",
                "hotelName": "君阅漫居酒店",
                "linkman": "盛佳元",
                "linktel": "13973449294",
                "roomNum": 1,
                "actualPay": "2000",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:27:38"
            },
            {
                "createTime": "2018-12-04 07:17:20",
                "orderNo": "11615439078408032",
                "payFormNo": "8037900815",
                "arrivalDate": "2018-12-03",
                "hotelName": "长沙县星沙树星大酒店",
                "linkman": "李姗姗",
                "linktel": "13875894879",
                "roomNum": 2,
                "actualPay": "17200",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:17:20"
            },
            {
                "createTime": "2018-12-04 07:16:14",
                "orderNo": "11615439077741341",
                "payFormNo": "8037900812",
                "arrivalDate": "2018-12-03",
                "hotelName": "长沙县星沙树星大酒店",
                "linkman": "陈鑫",
                "linktel": "13875894879",
                "roomNum": 2,
                "actualPay": "17200",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:16:14"
            },
            {
                "createTime": "2018-12-04 07:14:29",
                "orderNo": "11615439076698627",
                "payFormNo": "8037900809",
                "arrivalDate": "2018-12-03",
                "hotelName": "长沙县星沙树星大酒店",
                "linkman": "何丽丽",
                "linktel": "13875894879",
                "roomNum": 1,
                "actualPay": "8600",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:14:29"
            },
            {
                "createTime": "2018-12-04 07:13:01",
                "orderNo": "11615439075817233",
                "payFormNo": "",
                "arrivalDate": "2018-12-03",
                "hotelName": "长沙县星沙树星大酒店",
                "linkman": "何丽丽",
                "linktel": "13875894879",
                "roomNum": 2,
                "actualPay": 0,
                "payStatus": 2,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 07:13:01"
            },
            {
                "createTime": "2018-12-04 07:11:17",
                "orderNo": "11615439074776629",
                "payFormNo": "8037900804",
                "arrivalDate": "2018-12-03",
                "hotelName": "君阅漫居酒店",
                "linkman": "盛佳元",
                "linktel": "13973449294",
                "roomNum": 1,
                "actualPay": "5000",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:11:17"
            },
            {
                "createTime": "2018-12-04 07:08:50",
                "orderNo": "11615439073308372",
                "payFormNo": "8037900802",
                "arrivalDate": "2018-12-03",
                "hotelName": "锦江之星(怀来沙城火车站店)",
                "linkman": "王隆基",
                "linktel": "18230023707",
                "roomNum": 1,
                "actualPay": "9600",
                "payStatus": 5,
                "roomStatus": 1,
                "payTime": "2018-12-04 07:08:50"
            },
            {
                "createTime": "2018-12-04 07:06:01",
                "orderNo": "11615439071616321",
                "payFormNo": "",
                "arrivalDate": "2018-12-09",
                "hotelName": "全季酒店(北京通州运河西路店)",
                "linkman": "王刚",
                "linktel": "17610768757",
                "roomNum": 1,
                "actualPay": 0,
                "payStatus": 2,
                "roomStatus": "暂无状态",
                "payTime": "2018-12-04 07:06:01"
            }
        ]
    }
}
`